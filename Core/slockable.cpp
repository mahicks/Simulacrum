/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "slockable.h"

// Mappings between boost and C++11
#ifdef BOOST_MUTEX
#include <boost/thread/mutex.hpp>
#define SCXXEMutex boost::mutex
#else
#include <mutex>
#define SCXXEMutex std::mutex
#endif //BOOST_MUTEX

using namespace Simulacrum;

class Simulacrum::SLockable::SLockablePIMPL {
public:
  SCXXEMutex  LockMutex;
  SCXXEMutex  ReferenceCountMutex;
  int         ReferenceCount;
              SLockablePIMPL() {
    ReferenceCount      = 0;
  }
  virtual    ~SLockablePIMPL() {

  }
};

void SLockable::lock() {
  PrivateData->LockMutex.lock();
}

void SLockable::unlock() {
  PrivateData->LockMutex.try_lock();
  PrivateData->LockMutex.unlock();
}

bool SLockable::try_lock() {
  return PrivateData->LockMutex.try_lock();
}

void SLockable::wait() {
  lock();
  unlock();
}

void SLockable::refIncr() {
  PrivateData->ReferenceCountMutex.lock();
  PrivateData->ReferenceCount++;
  PrivateData->ReferenceCountMutex.unlock();
}

void SLockable::refDecr() {
  PrivateData->ReferenceCountMutex.lock();
  if (PrivateData->ReferenceCount > 0)
    PrivateData->ReferenceCount--;
  PrivateData->ReferenceCountMutex.unlock();
}

int SLockable::refCount() {
  PrivateData->ReferenceCountMutex.lock();
  int retval = PrivateData->ReferenceCount;
  PrivateData->ReferenceCountMutex.unlock();
  return retval;
}

SLockable::SLockable(): PrivateData(new SLockablePIMPL()) {

}

SLockable::SLockable(const SLockable&): PrivateData(new SLockablePIMPL()) {

}

SLockable::~SLockable() {
  wait();
  delete PrivateData;
}

//------------------------------------------------------------------------------

class Simulacrum::SLockGuard::SLockGuardPIMPL {
public:
  SLockable *ParentLock;
};

SLockGuard::SLockGuard(SLockable& lock): PrivateData(new SLockGuardPIMPL()) {
  PrivateData->ParentLock = &lock;
}

SLockGuard::~SLockGuard() {
  if (PrivateData->ParentLock != nullptr)
    PrivateData->ParentLock->unlock();
}

void SLockGuard::unlock() {
  if (PrivateData->ParentLock != nullptr) {
    PrivateData->ParentLock->unlock();
    PrivateData->ParentLock = nullptr;
  }
}
