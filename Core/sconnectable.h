/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Connectable
 * An interface specification to allow components to be 'connected'
 * M. A. Hicks
 */
#ifndef SCONNECTABLE
#define SCONNECTABLE

#include <map>
#include "slockable.h"
#include "definitions.h"

namespace Simulacrum {

  class SIMU_API SConnectable: public SLockable {
  private:
    class SConnectablePIMPL;
    SConnectablePIMPL *PrivateData;
  protected:
    virtual bool addListener(SConnectable&);
    virtual bool removeListener(SConnectable&);
    virtual bool addTarget(SConnectable&);
    virtual bool removeTarget(SConnectable&);
  public:
    typedef unsigned ssignal_t;
    typedef bool     sdepth_t;
                 SConnectable();
    virtual     ~SConnectable();
    virtual void refresh(sdepth_t);
    void         emitRefresh(sdepth_t);
    virtual void signal(ssignal_t,void* = nullptr);
    void         emitSignal(ssignal_t,void* = nullptr);
    virtual void progress(int);
    virtual bool sconnect(SConnectable&);
    virtual bool sconnect(SConnectable&,bool delondiscon);
    virtual bool sdisconnect(SConnectable&);
    virtual void disconnectSources();
    virtual void disconnectListeners();
    virtual void disconnectListeners_Refresh();
    virtual bool isConnected(SConnectable&) const;
    virtual bool isConnected(SConnectable*) const;
    virtual void sconfigure(const std::string&);
    virtual SConnectable&
                 end();
    virtual SConnectable&
                 penum();
    virtual SConnectable&
                 pred();
    virtual SConnectable&
                 succ();
    virtual
    void         deleteOnDisconnect(bool);
    virtual
    bool         deleteOnDisconnect();
    virtual
    void         setName(const std::string&);
    virtual
    const std::string&
                 getName() const;
    const std::map<std::string,double>&
                 parameters();
    const std::map<std::string,std::string>&
                 properties();
    bool         hasProperty (const std::string&) const;
    bool         hasParameter(const std::string&) const;
    void         setProperty (const std::string&, const std::string&);
    void         setParameter(const std::string&, double);
    std::string  getProperty (const std::string&) const;
    double       getParameter(const std::string&) const;
  };

}

#endif
