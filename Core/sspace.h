/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Images:
 * The core classes of the Simulacrum library. Deisgned to store
 * n-dimensional ' images', these classes include SElem sets,
 * images, dimensions and coordinate transformations.
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_IMAGES
#define SIMULACRUM_IMAGES

#include <string>
#include <map>
#include <typeinfo>
#include <functional>
#include "definitions.h"
#include "sprimitives.h"
#include "sconnectable.h"
#include "slockable.h"
#include <Core/LUT/LUT.h>
#include <Core/NNode/nnode.h>

namespace Simulacrum {

/*------------------------------------------------------------------------------
 *                          SElem Collection Classes
 * ---------------------------------------------------------------------------*/

/* A basic selem set, which holds a variable number of selems, and implement
   various computational interface methods. */
class SIMU_API SElemSet {
private:
  class SElemSetPIMPL;
  SElemSetPIMPL *PrivateData;
public:
  typedef unsigned long long int Size;
                      SElemSet();
                      SElemSet(SElemSet::Size);
                      SElemSet(const SElemSet&);
  virtual            ~SElemSet();
  virtual
  void                useData(SElem::DataSource,SElemSet::Size bytes,
                              bool carr=false);
  virtual
  bool                carrData();
  virtual
  SElemSet::Size      size() const;
  virtual
  SElemSet::Size      length() const;
  virtual
  void                setNativeSElemType(SElem *);
  virtual
  SElem*              getNativeSElem() const;
  virtual
  SElem&              exemplar();
  virtual
  SElem::DataSource   SElems(SElemSet::Size);
  virtual
  SElem::DataSource   SElems_Passive(SElemSet::Size);
  virtual
  SElem::DataSource   SElem_Null();
  virtual
  SElem::DataSource   operator[](SElemSet::Size);
  virtual
  void                resize(SElemSet::Size);
  virtual
  void                reset();
  virtual
  void                zeroData();
};

/*------------------------------------------------------------------------------
 *                             SSpace Classes
 * ---------------------------------------------------------------------------*/

typedef unsigned short CONTEXTS_t;
class SIMU_API DataContext {
public:
  SElemSet        ContextData;
  GPLUT           ContextLUT;
};

/* The standard n-dimensional image class -- wraps a selem set and provides
   additional information, coordinates transformations etc etc                */
class SIO;
class SSpaceIterator;
class SIMU_API SSpace : public SConnectable {
  friend class SSpaceIteratorNativeFast;
private:
  class SSpacePIMPL;
  SSpacePIMPL *PrivateData;
protected:
  virtual
  void              getRGBAScanlinesInto(SSpace* target,
                                         SCoordinate::Precision startrow,
                                         SCoordinate::Precision endrow,
                                         SCoordinate::Precision width,
                                         unsigned short  step,
                                         bool mergecontexts,
                                         float opacity,
                                         SCoordinate::Precision startcol   =0,
                                         SCoordinate::Precision endcol     =0,
                                         SCoordinate::Precision pixeoffset =0);
public:
                    SSpace();
                    SSpace(const SCoordinate&);
                    SSpace(SSpace&);
                    SSpace(const SSpace&);
  virtual          ~SSpace();
  virtual
  SSpace*           New   ();
  virtual
  SElemSet::Size
                    calcIndex(const SCoordinate&) const;
  virtual
  int               concatenate(std::vector<SSpace*>&);
  virtual
  SSpace&           operator= (SSpace&);
  virtual
  SSpace&           operator= (const SSpace&);
  virtual
  void              get2DRGBAInto(SSpace&, unsigned short downsample = 0,
                                  bool mergecontexts=true, float opacity= 0.3,
                                  SCoordinate *cropb = nullptr,
                                  SCoordinate *crope = nullptr,
                                  SCoordinate::Precision zoffset = -1);
  virtual
  void              copyInto  (SSpace&, bool withimagedata = true);
  virtual
  bool              resize(const SCoordinate&);
  virtual
  SElem::DataSource SElemData(const SCoordinate&);
  virtual
  SElem::DataSource SElemData_Passive(const SCoordinate&);
  virtual
  SElem::DataSource operator[](const SCoordinate&);
  virtual
  void              SElemInto(const SCoordinate&, SElem& target);
  virtual
  SElem::Ptr        getNativeSElem();
  virtual
  SElem*            getNativeSElemP();
  virtual
  void              setNativeSElemType(SElem*);
  virtual
  void              lockNativeSElemType(bool);
  virtual
  bool              nativeSElemTypeLocked();
  virtual
  const SCoordinate&
                    extent() const;
  const SCoordinate*
                    extentP() const;
  bool              reinterpretExtent(const SCoordinate&);
  virtual
  void              promoteExtent();
  virtual
  void              reset();
  virtual
  SSpace&           getSourceSSpace();
  virtual
  void              setActiveContext(CONTEXTS_t);
  virtual
  CONTEXTS_t        activeContext() const;
  virtual
  CONTEXTS_t        contexts() const;
  virtual
  CONTEXTS_t        newContext();
  virtual
  void              removeContext(CONTEXTS_t);
  virtual
  GPLUT&            LUT();
  virtual
  SElemSet&         selemDataStore();
  virtual
  std::vector<DataContext*>&
                    selemDataContexts();
  GPLUT*            LUTP();
  SElemSet*         selemDataStoreP();
  std::vector<DataContext*>*
                    selemDataContextsP();
  virtual
  void              applyLUT();
  void              autoLUT(int percentile = 90,
                            long long int width  = -1,
                            long long int center = -1,
                            const std::vector< std::pair<float,float> >* 
                                                              histogram = nullptr);
  virtual
  bool              postLoadConfig();
  virtual
  NNode&            informationNode();
  NNode*            informationNodeP();
  virtual
  const
  SVector&          spacing() const;
  const
  SVector*          spacingP() const;
  virtual
  void              setSpacing(const SVector&);
  virtual
  std::string       spacingUnits(long long int index=0) const;
  virtual
  void              setSpacingUnits(const std::string&,long long int index=0);
  virtual
  SPtr<SSpaceIterator>
                    getFastNativeIterator();
  virtual
  SSpaceIterator*   getFastNativeIteratorP();
  virtual
  SVector           toGlobalSpace(const SVector&);
  virtual
  SVector           fromGlobalSpace(const SVector&);
  virtual
  void              setToGlobalSpaceFunction
                               (std::function<SVector(SSpace*,const SVector&)>);
  virtual
  void              setFromGlobalSpaceFunction
                               (std::function<SVector(SSpace*,const SVector&)>);
  virtual
  std::string       globalSpaceID() const;
  virtual
  void              setGlobalSpaceID(const std::string&);
  virtual
  std::string       vectorMeaning(const SVector&, bool strict = true) const;
  virtual
  SVector           meaningVector(const std::string&) const;
  virtual
  void              assignVectorMeaning(const SVector&,const std::string&);
  virtual
  void              resetMeaningVectors();
  virtual
  SCoordinate       toSourceCoords(const SCoordinate&) const;
  virtual
  bool              fromSourceCoords(const SCoordinate& sourcecoord,
                                     SCoordinate& localcoord) const;
};

class SIMU_API SSpaceIterator {
public:
  virtual              ~SSpaceIterator ();
  virtual
  void                  toBegin        () =0;
  virtual
  bool                  isBegin        () =0;
  virtual
  void                  toEnd          () =0;
  virtual
  bool                  isEnd          () =0;
  virtual
  void                  toCoord        (const SCoordinate&) =0;
  virtual
  const SSpaceIterator& operator++     () =0;
  const SSpaceIterator& increment      ();
  virtual
  const SSpaceIterator& operator++     (int);
  virtual
  const SSpaceIterator& operator--     () =0;
  const SSpaceIterator& decrement      ();
  virtual
  const SSpaceIterator& operator--     (int);
  virtual
  bool                  operator==     (const SSpaceIterator&) const;
  virtual
  bool                  operator!=     (const SSpaceIterator&) const;
  virtual
  bool                  operator<      (const SSpaceIterator&) const;
  virtual
  const SSpaceIterator& operator=      (const SSpaceIterator&);
  virtual
  SElem&                operator*      () const =0;
  SElem&                selem          () const;
  virtual
  const
  SCoordinate&          pos            () const =0;
  SElem*                selemp         () const;
  const
  SCoordinate*          posP           () const;
  const
  SCoordinate           sourcePos      () const;
};

class SIMU_API SSpaceIteratorCompat: public SSpaceIterator {
  friend class SSpaceIteratorCompatConstrained;
public:
                        SSpaceIteratorCompat (SSpace&);
                        SSpaceIteratorCompat (const SSpaceIteratorCompat&);
  virtual              ~SSpaceIteratorCompat () override;
  void                  toBegin              () override;
  bool                  isBegin              () override;
  void                  toEnd                () override;
  bool                  isEnd                () override;
  void                  toCoord              (const SCoordinate&) override;
  const SSpaceIterator& operator++           () override;
  const SSpaceIterator& operator--           () override;
  const SSpaceIterator& operator=            (const SSpaceIterator&) override;
  const SSpaceIterator& operator=            (const SSpaceIteratorCompat&);
  SElem&                operator*            () const override;
  const
  SCoordinate&          pos                  () const override;
private:
  class SSpaceIteratorCompatPIMPL;
  SSpaceIteratorCompatPIMPL *PrivateData;
};

class SIMU_API SSpaceIteratorCompatConstrained: public SSpaceIteratorCompat {
public:
                        SSpaceIteratorCompatConstrained(SSpace& source,
                                                       const SCoordinate& base,
                                                 const SCoordinate& constraint);
                       ~SSpaceIteratorCompatConstrained();
  const
  SCoordinate           sourcePos                  () const;
  using SSpaceIteratorCompat::operator=;
private:
  class SSpaceIteratorCompatConstrainedPIMPL;
  SSpaceIteratorCompatConstrainedPIMPL *PrivateDataL;
};

/*------------------------------------------------------------------------------
 *                         ---------END---------
 * ---------------------------------------------------------------------------*/

} //end namespace
#endif //SIMULACRUM_IMAGES

