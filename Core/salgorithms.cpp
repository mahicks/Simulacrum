/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SAlgorithms:
 * Generic algorithms implementation
 * Author: M.A.Hicks
 */

#include "salgorithms.h"
#include <Core/slicer/slicer.h>

using namespace Simulacrum;

class SBucketHistogram::SBucketHistogramPIMPL {
public:
    std::vector< std::pair < float, float > >
                 Histogram;
    SLockable    MergeLock;
    float        BucketRange;
};

SBucketHistogram::SBucketHistogram(): PrivateData(new SBucketHistogramPIMPL()) {
  reset();
}

SBucketHistogram::~SBucketHistogram() {
  delete PrivateData;
}

SAlgorithm* SBucketHistogram::New() {
  return new SBucketHistogram;
}

void SBucketHistogram::init() {
  setInputs   (1);                 // single input space
  setOutputs  (0);                 // no outut space necessary
  setParameter("buckets",128.0);   // set default number of buckets
  setParameter("maximum",65536.0); // set default number of buckets
  setParameter("threshold",0.0);   // set default number of buckets
  setDriver   (driver_t::Input,0); // driven by input, slot 0
  setBlockSize({1});               // assign a pixel-parallel blocksize
}

void SBucketHistogram::preExecute() {
  // configure stores as per parameter
  unsigned bucketnum = (unsigned)getParameter("buckets");
  PrivateData->Histogram.clear();
  PrivateData->Histogram.resize(bucketnum);
  PrivateData->BucketRange = getParameter("maximum") / bucketnum;
  float bucketrcount = PrivateData->BucketRange;
  for (unsigned i=0; i < PrivateData->Histogram.size(); i++) {
    PrivateData->Histogram[i].first = bucketrcount;
    bucketrcount += PrivateData->BucketRange;
  }
}

void SBucketHistogram::kernel(SSpaceIterator *beginc, SSpaceIterator *endc) {
  // local stores
  std::vector< unsigned > buckets(PrivateData->Histogram.size());
  double threshold = getParameter("threshold");
  unsigned bucket;
  // iterate region
  while ( true ) {
    // determine bucket
    SElem::Precision intensity = (**beginc).intensityUnsigned();
    if (intensity >= threshold) {
      bucket = intensity / PrivateData->BucketRange;
      // clamp bucket
      if (bucket >= buckets.size())
        bucket = buckets.size()-1;
      // add to bucket
      buckets[bucket]++;
    }
    // check and increment
    if (*beginc == *endc)
      break;
    else // increment iterator
      (*beginc)++;
  }
  // merge local->global buckets
  PrivateData->MergeLock.lock();
  for (unsigned i=0; i < buckets.size(); i++)
    PrivateData->Histogram[i].second += (float)buckets[i];
  PrivateData->MergeLock.unlock();
}

const std::vector< std::pair< float, float > >&
                                                 SBucketHistogram::histogram() {
  return PrivateData->Histogram;
}

//-----------------------------------------------------------------------------

class SIsoNormZSlices::SIsoNormZSlicesPIMPL {
public:
  std::vector< std::pair<bool,SCoordinate::Precision> > SliceSource;
};

SIsoNormZSlices::SIsoNormZSlices(): PrivateData(new SIsoNormZSlicesPIMPL()) {
  reset();
}

SIsoNormZSlices::~SIsoNormZSlices() {
  delete PrivateData;
}

SAlgorithm* SIsoNormZSlices::New() {
  return new SIsoNormZSlices;
}

void SIsoNormZSlices::init() {
  setInputs   (1);                  // single input space
  setOutputs  (1);                  // single outut space
  setDriver   (driver_t::Output,0); // driven by output, slot 0
  setBlockSize({1});                // assign a pixel-parallel blocksize
}

void SIsoNormZSlices::preExecute() {
  SSpace& inputspace  = input(0);
  SSpace& outputspace = output(0);
  PrivateData->SliceSource.clear();
  if (isApplicable(inputspace)) {
    auto extent    = inputspace.extent();
    auto spacing   = inputspace.spacing();
    auto newextent = extent;
    SVector::Precision addit_slices = SGeom::fabs((extent.z() - ((extent.z() * spacing.z()) / spacing.y())) / extent.z());
    SVector::Precision slice_accum  = 0.0;
    // calculate new slices to be inserted
    for (SCoordinate::Precision z=0; z<extent.z(); z++) {
      PrivateData->SliceSource.push_back(std::make_pair(false,z));
      slice_accum += addit_slices;
      while (slice_accum >= 1.0) {
        PrivateData->SliceSource.push_back(std::make_pair(true,z));
        slice_accum -= 1.0;
      }
    }
    // catch and round-up and residual
    if ((slice_accum >= 0.5) && (PrivateData->SliceSource.size() > 0)) {
      PrivateData->SliceSource.push_back(std::make_pair(true,
                                       PrivateData->SliceSource.back().second));
    }
    // configure the target
    outputspace.setNativeSElemType(inputspace.getNativeSElemP());
    newextent.z(PrivateData->SliceSource.size());
    outputspace.resize(newextent);
  }
}

void SIsoNormZSlices::postExecute() {
  SSpace& inputspace  = input(0);
  SSpace& outputspace = output(0);
  if (isApplicable(inputspace)) {
    auto spacing = inputspace.spacing();
    spacing.z(spacing.y());
    outputspace.setSpacing(spacing);
  }
}

void SIsoNormZSlices::kernel(SSpaceIterator *beginc, SSpaceIterator *endc) {
  if (PrivateData->SliceSource.size() > 0) {
    // variables used throughout algorithm kernel
    SSpace& inputsp = input(0);
    auto    elem    = inputsp.getNativeSElem();
    const
    auto    extent  = output(0).extent();
    auto    pos     = beginc->pos();
    auto    posz    = pos.z();
    auto    srcpos  = pos;
    auto   &slicesc = PrivateData->SliceSource;
    auto    slice   = slicesc[pos.z()];
    auto    slicesz = slicesc.size();
    if (slicesz > 0)  slicesz--;
    SCoordinate interpos;
    // main kernel loop
    while (*beginc != *endc) {
      srcpos = pos;
      posz = pos.z();
      slice  = slicesc[posz];
      srcpos.z(slice.second);
      if (slice.first && ((unsigned)(posz) < slicesz)) {
        interpos = srcpos;
        elem->source(inputsp[interpos]);
        auto y0 = elem->intensity();
        // linear interpolate between to (assumed) equidistant points
        interpos.z(slicesc[posz+1].second);
        elem->source(inputsp[interpos]);
        auto y1 = elem->intensity();
        (**beginc).intensity((y0+y1)/2);
      }
      else {
        elem->source(inputsp[srcpos]);
        **beginc = *elem;
      }
      // increment iterator and position reference
      (*beginc)++;
      pos.tabincrement(extent);
    }
  }
}

bool SIsoNormZSlices::isApplicable(SSpace& src) {
  bool result = false;
  auto extent  = src.extent();
  auto spacing = src.spacing();
  if (
      ( (extent.getDim() == 3) || ((extent.getDim() == 4) && (extent[3] == 1)) )
      &&
      (spacing.getDim() == extent.getDim())
      &&
      (spacing.x() == spacing.y())
      &&
      (spacing.z() > spacing.y())
     ) {
    result = true;
  }
  return result;
}

bool SIsoNormZSlices::makeIsotropicZ(SSpace &target) {
  bool result = false;
  if (isApplicable(target)) {
    SIsoNormZSlices isozcorrect;
    SSpace corrected;
    target.copyInto(corrected,false); // copy over the container information
    isozcorrect({target,corrected});
    target = corrected;
    result = true;
  }
  return result;
}

//-----------------------------------------------------------------------------

class SFilter::SFilterPIMPL {
public:
  sfilter_kernel_t FilterKernel;
};

SFilter::SFilter(sfilter_kernel_t newkern): PrivateData(new SFilterPIMPL()) {
  PrivateData->FilterKernel = newkern;
  reset();
}

SFilter::~SFilter() {
  delete PrivateData;
}

SAlgorithm* SFilter::New() {
  return new SFilter(PrivateData->FilterKernel);
}

void SFilter::init() {
  setInputs   (1);                 // single input space
  setOutputs  (1);                 // single outut space
  setDriver   (driver_t::Input,0); // driven by input, slot 0
  setBlockSize({1});               // assign a pixel-parallel blocksize
}

void SFilter::preExecute() {
  // make sure output has same SElem type as input
  output(0).setNativeSElemType(input(0).getNativeSElemP());
  // make sure output is of same extent as input
  output(0).resize(input(0).extent());
}

void SFilter::kernel(SSpaceIterator *beginc, SSpaceIterator *endc) {
  // setup an additional iterator for the output space
  auto outp = output(0).getFastNativeIterator();
  // align it to the same position as input
  outp->toCoord(beginc->pos());
  // iterate region
  while ( true ) {
    // call kernel function
    PrivateData->FilterKernel(*beginc, **beginc, **outp);
    // check and increment
    if (*beginc == *endc)
      break;
    else { // increment both iterators
      (*beginc)++;
      (*outp)++;
    }
  }
}

