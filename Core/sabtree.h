/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_ABSTREE
#define SIMULACRUM_ABSTREE

/*******************************************************************************
 *                      Simulacrum Abstract Tree
 *
 * An abstract tree specification -- used for XML, DICOM etc.
 *
 ******************************************************************************/

#include <string>
#include <map>
#include <vector>
#include "definitions.h"

namespace Simulacrum {

  class SAbsTreeNode;
  class NNode;
  class DCMTag;
  typedef std::vector<SAbsTreeNode*> SAbsTreeNodeList_t;

  class SIMU_API SAbsTreeNode {
  public:
    virtual std::string               NodeID()=0;
    virtual void                      NodeID(const std::string&);
    virtual std::string               NodeType()=0;
    virtual void                      NodeType(const std::string&);
    virtual unsigned long             NodeSize()=0;
    virtual void                      NodeSize(const std::string&);
    virtual std::string               NodeName()=0;
    virtual void                      NodeName(const std::string&);
    virtual std::string               NodeValue()=0;
    virtual void                      NodeValue(const std::string&);
    virtual std::string               NodeData();
    virtual void                      NodeData(const std::string&);
    virtual unsigned long             NodeChildrenNum(bool strict  = false)=0;
    virtual SAbsTreeNodeList_t        NodeChildren(bool strict     = false,
                                                   bool simpleonly = false)=0;
    virtual bool                      NodePathExists(const std::string&)=0;
    virtual SAbsTreeNode&             NodeByPath(const std::string&)=0;
    virtual SAbsTreeNode&             NodeParent() =0;
    virtual bool                      hasNodeParent() =0;
    virtual SAbsTreeNode&             NodeRoot();
    virtual std::string               NodePath(bool skiptop = false);
    virtual SAbsTreeNode&             NewChild()=0;
    virtual bool                      NodeError()=0;
    virtual bool                      NodeEmph();
    virtual bool                      removeNode()=0;
    virtual void                      detachNode()=0;
    virtual bool                      hasAttribute(const std::string&);
    virtual std::string               getAttribute(const std::string&);
    virtual std::map<std::string,std::string>
                                      getAttributes();
    virtual void                      setAttribute(const std::string&,
                                                   const std::string&);
    virtual void                      copy(SAbsTreeNode&);
    virtual                          ~SAbsTreeNode() {};
    virtual void                      clear()=0;
    virtual void                      clearChildren();
            SPtr<SAbsTreeNode>        query(const std::string&);
    virtual SAbsTreeNode*             queryP(const std::string&);
    virtual SimulacrumLibrary::str_enc
                                      stringEncoding();
  };

  class SIMU_API SAbsTreeNodeConversions {
  public:
    static void                       DCMTagToNNode(DCMTag&,NNode&,
                                                    bool recurse = true);
  };

}
#endif //ABSTREE
