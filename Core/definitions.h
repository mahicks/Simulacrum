/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_DEFINITIONS
#define SIMULACRUM_DEFINITIONS

// used throughout library
#include <string>
#include <sstream>
#include <stdint.h>
#include <memory>

// DSO Exporting, Reference: http://gcc.gnu.org/wiki/Visibility
#define SIMU_API
#ifdef SHAREDLIB
  #ifdef _WIN32
    #undef  SIMU_API
    #define SIMU_API __declspec(dllexport)
  #else
    #undef  SIMU_API
    #define SIMU_API __attribute__ ((visibility ("default")))
  #endif
#endif

namespace Simulacrum {

  class SIMU_API SimulacrumLibrary {
  public:
    static bool init();
    enum str_enc { Latin1, UTF8, Raw, Unknown };
  };

  class SIMU_API SimulacrumInformation {
  public:
    static const std::string getInfoString();
    static const std::string getVersionString();
    static const std::string getAuthors();
    static const std::string getURI();
    static const std::string getLic();
    static const std::string getBuildInformation();
  };

// shared pointers
#ifndef SWIG
  template<class T> using SPtr = std::shared_ptr<T>;
#else
  #define SPtr std::shared_ptr
#endif //SWIG

// Qt Graphics System
#if QT_VERSION >= 0x050000
#define GRAPHICS_BACKEND(x) std::cerr << "WARNING: cannot setGraphicsSystem in QT5" << std::endl
#else
#define GRAPHICS_BACKEND(x) QApplication::setGraphicsSystem(x)
#endif

// CPP defs to strings
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

// Annotate fall-throughs
#if __GNUC__ > 6
#define __FALLTHROUGH__ __attribute__ ((fallthrough));
#else
#define __FALLTHROUGH__
#endif

} //end namespace

#endif //SIMULACRUM_DEFINITIONS
