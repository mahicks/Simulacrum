/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Lockable
 * An interface specification to allow components to be 'locked'
 * M. A. Hicks
 */
#ifndef SLOCKABLE
#define SLOCKABLE

#include "definitions.h"

namespace Simulacrum {

  class SIMU_API SLockable {
  private:
    class SLockablePIMPL;
    SLockablePIMPL *PrivateData;
  public:
    virtual void lock();
    virtual void unlock();
    virtual bool try_lock();
    virtual void wait();
    virtual void refIncr();
    virtual void refDecr();
    virtual int  refCount();
                 SLockable();
                 SLockable(const SLockable&);
    virtual     ~SLockable();
  };

  class SIMU_API SLockGuard {
  private:
    class SLockGuardPIMPL;
    SLockGuardPIMPL *PrivateData;
  public:
                  SLockGuard(SLockable& lock);
    virtual      ~SLockGuard();
    virtual  void unlock();
  };

}

#endif //SLOCKABLE
