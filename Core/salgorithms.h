/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SAlgorithms:
 * Generic algorithms specification
 * Author: M.A.Hicks
 */

#ifndef SALGORITHMS
#define SALGORITHMS

#include "salgorithm.h"
#include <vector>

namespace Simulacrum {

  class SIMU_API SBucketHistogram : public SAlgorithmCPU {
  private:
    class SBucketHistogramPIMPL;
    SBucketHistogramPIMPL *PrivateData;
  public:
                 SBucketHistogram();
    virtual     ~SBucketHistogram();
    virtual
    SAlgorithm*  New             () override;
    virtual
    void         init      () override;
    virtual
    void         preExecute() override;
    virtual
    void         kernel    (SSpaceIterator *beginc, SSpaceIterator *endc) override;
    const std::vector< std::pair < float, float > >&
                 histogram ();
  };

  class SIMU_API SIsoNormZSlices : public SAlgorithmCPU {
  private:
    class SIsoNormZSlicesPIMPL;
    SIsoNormZSlicesPIMPL *PrivateData;
  public:
                 SIsoNormZSlices();
    virtual     ~SIsoNormZSlices();
    virtual
    SAlgorithm*  New       () override;
    virtual
    void         init      () override;
    virtual
    void         preExecute() override;
    virtual
    void         postExecute() override;
    virtual
    void         kernel    (SSpaceIterator *beginc, SSpaceIterator *endc) override;
    static
    bool         isApplicable (SSpace&);
    static
    bool         makeIsotropicZ (SSpace&);
  };

  class SIMU_API SFilter : public SAlgorithmCPU {
  private:
    class SFilterPIMPL;
    SFilterPIMPL *PrivateData;
  public:
    typedef      std::function<void(SSpaceIterator&,SElem&,SElem&)>
                                                              sfilter_kernel_t;
                 SFilter(sfilter_kernel_t);
    virtual     ~SFilter();
    virtual
    SAlgorithm*  New    () override;
    virtual
    void         init   () override;
    virtual
    void         preExecute() override;
    virtual
    void         kernel (SSpaceIterator *beginc, SSpaceIterator *endc) override;
  };

}

#endif // SALGORITHMS
