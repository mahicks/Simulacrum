/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*******************************************************************************
 *                      Simulacrum Abstract Tree
 *
 * An abstract tree implementation -- used for XML, DICOM etc.
 *
 ******************************************************************************/

#include "sabtree.h"
#include <Core/NNode/nnode.h>
#include <Resources/DICOM/dcmtag.h>
#include <Toolbox/SURI/suri.h>
#include <Core/error.h>

#include <set>

using namespace Simulacrum;

void SAbsTreeNode::NodeID(const std::string& ) {

}

void SAbsTreeNode::NodeName(const std::string& ) {

}

void SAbsTreeNode::NodeSize(const std::string& ) {

}

void SAbsTreeNode::NodeType(const std::string& ) {

}

void SAbsTreeNode::NodeValue(const std::string& ) {

}

std::string SAbsTreeNode::NodeData() {
  return NodeValue();
}

void SAbsTreeNode::NodeData(const std::string& src) {
  NodeValue(src);
}

std::string SAbsTreeNode::NodePath(bool skiptop) {
  SURI tmpuri;
  if (hasNodeParent())
    if ( (!skiptop) || NodeParent().hasNodeParent() )
      tmpuri = SURI(NodeParent().NodePath(skiptop));
  tmpuri.addComponentBack(NodeID());
  return tmpuri.getURI(true);
}

bool SAbsTreeNode::NodeEmph() {
  return false;
}

void SAbsTreeNode::copy(SAbsTreeNode& target) {
  NodeID(target.NodeID());
  NodeType(target.NodeType());
  NodeName(target.NodeName());
  NodeValue(target.NodeValue());
  // TODO: recurse on child
}

bool SAbsTreeNode::hasAttribute(const std::string& ) {
  return false;
}

std::map< std::string, std::string > SAbsTreeNode::getAttributes() {
  return std::map< std::string,std::string >();
}

std::string SAbsTreeNode::getAttribute(const std::string& ) {
  return "";
}

void SAbsTreeNode::setAttribute(const std::string& , const std::string& ) {

}

void SAbsTreeNode::clearChildren() {
  SAbsTreeNodeList_t children = NodeChildren(true,true);
  for (unsigned i = 0; i < children.size(); i++)
    children[i]->removeNode();
}

SPtr<SAbsTreeNode> SAbsTreeNode::query(const std::string& qstr) {
  return SPtr<SAbsTreeNode>(queryP(qstr));
}

SAbsTreeNode* SAbsTreeNode::queryP(const std::string&) {
  throw SimulacrumExceptionGeneric
                            ("Querying not implemented for this type of node.");
  return nullptr;
}

SAbsTreeNode& SAbsTreeNode::NodeRoot() {
  SAbsTreeNode *root = this;
  std::set<SAbsTreeNode*> parents = {root};
  while(root->hasNodeParent()) {
    root = &(root->NodeParent());
    if (! parents.insert(root).second) break; // cycle detected
  }
  return *root;
}

SimulacrumLibrary::str_enc SAbsTreeNode::stringEncoding() {
  return SimulacrumLibrary::str_enc::Unknown;
}

/*******************************************************************************
 *                    Simulacrum Tree Conversion Methods
 ******************************************************************************/

void SAbsTreeNodeConversions::DCMTagToNNode(DCMTag& ttag,
                                            NNode& tnode,
                                            bool recurse ) {
  tnode.setName(ttag.NodeName());
  tnode.setAttribute("ID",ttag.NodeID());
  tnode.setAttribute("VR",ttag.NodeType());
  tnode.setData(ttag.NodeValue());
  if (recurse) {
    for (unsigned i=0; i<ttag.getTags().size();i++) {
      NNode *newchild = new NNode();
      DCMTagToNNode(*ttag.getTags()[i],*newchild);
      tnode.addChildNode(newchild);
    }
  }
}
