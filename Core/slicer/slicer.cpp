/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Slicer:
 * The Simulacrum Slicer
 * Takes an 2-3 dimensional image as input and slices tomographically along a
 * specified plane, with the standard SSpace interface.
 * Author: M.A.Hicks
 */

#include "slicer.h"

#include <algorithm>
#include <limits>
#include <map>
#include <Toolbox/SLogger/slogger.h>
#include <Core/error.h>

using namespace Simulacrum;

namespace Simulacrum {
  class SimulacrumSlicerException: public SimulacrumBoundsException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: Image Slicer Exception";
    }
  };
}

class SSlicer::SSlicerPIMPL {
public:
  /* Properties of Slice and calculations */
  SSlicer          *Parent;
  SSpace            NullSpace;
  SCoordinate       SliceExtent;
  SSpace           *SourceImage;
  SPlane            SlicePlane;
  SVector           XAxis, YAxis;
  SPoint            FrontP, BackP;
  unsigned          Depth;
  SPoint            TLC;
  SVector           Spacing;
  SCoordinate       CoordinateSuffix;
  static const
  unsigned          EdgesNum = 12;
  SLine             Edges[EdgesNum];
  static const
  unsigned          FacesNum = 6;
  SPlane            Faces[FacesNum];
  unsigned          Thickness;
  bool              Unsliceable;
  bool              KeepPos;
  void              updateGeometry(bool deep);
  bool              handleDisconnect(SConnectable&);
  void              initEdges();
  void              initSlice();
  bool              isOnSlice(const SVector&) const;
  SVector           sspaceToSlice(const SVector&) const;
  bool              calcExtentIntersects(SLine,SPoint &Front, SPoint &Back);
  void              calcFrontBack();
  inline
  SCoordinate       toDiscreteSpace(const SVector&) const;
  inline SGeom::intersection_res
                    lineFaceFrontIntersection(SLine&);
  void              genPointsLine(std::vector<SElem::DataSource>&,
                                  const SPoint&,const SVector&,unsigned);
  linereduction_t
                    LineRedFunc;
  SSlicerPIMPL(SSlicer* parent) {
    Parent = parent;
    Thickness   = 1;
    LineRedFunc = std::bind(&SSlicer::MaxIPGrey,std::placeholders::_1,
                                                std::placeholders::_2,
                                                std::placeholders::_3,
                                                std::placeholders::_4,
                                                std::placeholders::_5,
                                                std::placeholders::_6,
                                                std::placeholders::_7,
                                                std::placeholders::_8);
    SourceImage = nullptr;
    KeepPos     = false;
    NullSpace.setName("Slicer");
  }
};

SSlicer::SSlicer() : PrivateData(new SSlicerPIMPL(this)) {
  sconnect(PrivateData->NullSpace);
}

SSlicer::SSlicer(SSpace& source) : PrivateData(new SSlicerPIMPL(this)){
  sconnect(source);
}

SSlicer::SSlicer(const SSlicer& target) : SSpace(),
                                          PrivateData(new SSlicerPIMPL(this)) {
  sconnect(*(target.PrivateData->SourceImage));
}

SSlicer::~SSlicer() {
  PrivateData->handleDisconnect(*(PrivateData->SourceImage));
  delete PrivateData;
}

SSpace* SSlicer::New() {
  return new SSlicer();
}

bool SSlicer::resize(const SCoordinate& targsize) {
  bool res = false;
  // throw an error only if asking to actually resize
  if (targsize != extent()) {
    res = true;
    // non-sensical method for this class
    throw SimulacrumSlicerException();
  }
  return res;
}

const SCoordinate& SSlicer::extent() const {
  return PrivateData->SliceExtent;
}

void SSlicer::reset() {
  setFrontSlice();
  setThickness(1);
  setCoordinateSuffix({});
  PrivateData->
            LineRedFunc = std::bind(&SSlicer::MaxIPGrey,std::placeholders::_1,
                                                        std::placeholders::_2,
                                                        std::placeholders::_3,
                                                        std::placeholders::_4,
                                                        std::placeholders::_5,
                                                        std::placeholders::_6,
                                                        std::placeholders::_7,
                                                        std::placeholders::_8);
}

SElem::DataSource SSlicer::SElemData(const SCoordinate& reqcoord) {
  SCoordinate lookup = toSourceCoords(reqcoord);
  SElem::DataSource retval;
  if (PrivateData->SourceImage->extent().withinSpace(lookup))
    retval = PrivateData->SourceImage->SElemData(lookup);
  else
    retval = PrivateData->SourceImage->selemDataStore().SElem_Null();
  return retval;
}

SElem::DataSource SSlicer::SElemData_Passive(const SCoordinate& reqcoord) {
  SCoordinate lookup = toSourceCoords(reqcoord);
  SElem::DataSource retval;
  if (PrivateData->SourceImage->extent().withinSpace(lookup))
    retval = PrivateData->SourceImage->SElemData_Passive(lookup);
  else
    retval = PrivateData->SourceImage->selemDataStore().SElem_Null();
  return retval;
}

void SSlicer::SElemInto(const SCoordinate& reqcoord, SElem& target) {
  SElem *tmppox = getNativeSElemP();
  SElem::DataSource tmpsrc = tmppox->newDataSource();
  tmppox->source(tmpsrc);
  if (PrivateData->Thickness < 2 || PrivateData->LineRedFunc == nullptr) {
    tmppox->source(SElemData_Passive(reqcoord));
    target = *tmppox;
  }
  else {
    PrivateData->LineRedFunc(this,reqcoord.x(),reqcoord.y(),
                             toSourceCoords(reqcoord),
                             PrivateData->SlicePlane.normal(),
                             PrivateData->Thickness,
                             *tmppox,target);
  }
  delete   tmppox;
  delete[] tmpsrc;
}

SElem::Ptr SSlicer::getNativeSElem() {
  return PrivateData->SourceImage->getNativeSElem();
}

SElem* SSlicer::getNativeSElemP() {
  return PrivateData->SourceImage->getNativeSElemP();
}

void SSlicer::setNativeSElemType(SElem* newpox) {
  PrivateData->SourceImage->setNativeSElemType(newpox);
}

SElemSet& SSlicer::selemDataStore() {
  throw SimulacrumSlicerException();
}

std::vector< DataContext* >& SSlicer::selemDataContexts() {
  throw SimulacrumSlicerException();
}

bool SSlicer::sconnect(SConnectable& targ) {
  try {
    SSpace &newsource = dynamic_cast<SSpace&>(targ);
    if (PrivateData->SourceImage != nullptr)
      PrivateData->handleDisconnect(*(PrivateData->SourceImage));
    PrivateData->SourceImage = &newsource;
  }
  catch (std::exception &) { }
  bool didcon = SConnectable::sconnect(targ);
  PrivateData->updateGeometry(true);
  if (!PrivateData->KeepPos)
    setFrontSlice();
  return didcon;
}

bool SSlicer::SSlicerPIMPL::handleDisconnect(SConnectable& targ) {
  if (&targ == SourceImage) {
    SSpace *presource = SourceImage;
    presource->try_lock();
    SourceImage = &NullSpace;
    presource->unlock();
  }
  return Parent->SConnectable::sdisconnect(targ);
}

bool SSlicer::sdisconnect(SConnectable& targ) {
  bool retval = PrivateData->handleDisconnect(targ);
  if (&targ != &PrivateData->NullSpace)
      sconnect(PrivateData->NullSpace);
  return retval;
}

void SSlicer::sconfigure(const std::string& configtr) {
  // x;y;z;x;y;z;x;y;z;XM|m|A;
  std::size_t prev =0;
  std::size_t next;
  std::vector<std::string> components;
  while (prev < configtr.size()) {
    next = configtr.find(";",prev);
    if (next != std::string::npos) {
      std::string valstr = configtr.substr(prev,next-prev);
      components.push_back(valstr);
      prev = next+1;
    }
    else
      break;
  }
  if (components.size() > 8) {
    SVector xaxis(3),yaxis(3),origin(3);
    for (unsigned c=0; c < 3; c++) {
      std::stringstream conv;
      SVector::Precision val;
      conv << components[c];
      conv >> val;
      origin[c] = val;
    }
    for (unsigned c=3; c < 6; c++) {
      std::stringstream conv;
      SVector::Precision val;
      conv << components[c];
      conv >> val;
      xaxis[c-3] = val;
    }
    for (unsigned c=6; c < 9; c++) {
      std::stringstream conv;
      SVector::Precision val;
      conv << components[c];
      conv >> val;
      yaxis[c-6] = val;
    }
    PrivateData->KeepPos = true;
    setPlane(origin,xaxis,yaxis);
    if (components.size() > 9) {
      if (components[9].size() > 1) {
        int lthickness = 1;
        std::stringstream conv;
        switch(components[9][components[9].size()-1]) {
          case 'M':
            setReductionFunction(std::bind(&SSlicer::MaxIPGrey,
                                           std::placeholders::_1,
                                           std::placeholders::_2,
                                           std::placeholders::_3,
                                           std::placeholders::_4,
                                           std::placeholders::_5,
                                           std::placeholders::_6,
                                           std::placeholders::_7,
                                           std::placeholders::_8));
            break;
          case 'm':
            setReductionFunction(std::bind(&SSlicer::MinIPGrey,
                                           std::placeholders::_1,
                                           std::placeholders::_2,
                                           std::placeholders::_3,
                                           std::placeholders::_4,
                                           std::placeholders::_5,
                                           std::placeholders::_6,
                                           std::placeholders::_7,
                                           std::placeholders::_8));
            break;
          case 'A':
            setReductionFunction(std::bind(&SSlicer::AVGIPGrey,
                                           std::placeholders::_1,
                                           std::placeholders::_2,
                                           std::placeholders::_3,
                                           std::placeholders::_4,
                                           std::placeholders::_5,
                                           std::placeholders::_6,
                                           std::placeholders::_7,
                                           std::placeholders::_8));
            break;
          default:
            setReductionFunction(std::bind(&SSlicer::MaxIPGrey,
                                           std::placeholders::_1,
                                           std::placeholders::_2,
                                           std::placeholders::_3,
                                           std::placeholders::_4,
                                           std::placeholders::_5,
                                           std::placeholders::_6,
                                           std::placeholders::_7,
                                           std::placeholders::_8));
            break;
        }
        components[9].resize(components[9].size()-1);
        conv << components[9];
        conv >> lthickness;
        setThickness(lthickness);
      }
    }
  }
}

bool SSlicer::SSlicerPIMPL::isOnSlice(const SVector& sourcepos) const {
  const SVector           posdiff        = (sourcepos - TLC);
  // take into account the possible cosine error due to discrete space rounding
  const SVector::Precision maxcosineerror = 1.0/posdiff.mag();
  SVector::Precision       planelinedotnorm;
  planelinedotnorm =
               (posdiff).unit().dot(SlicePlane.normal());
  return SGeom::fabs(planelinedotnorm) < maxcosineerror;
}

SVector SSlicer::SSlicerPIMPL::sspaceToSlice(const SVector& sspacecoord) const {
  /* Convert a point *known to be on the plane* into the planar X,Y coordinates
   * -> Project along user defined X-axis and Y-axis
   */
  SVector PlanePosition(2);
  SVector PointPlaneVector;
  PointPlaneVector = (sspacecoord-SlicePlane.point());
  PointPlaneVector.setDim(3);
  PlanePosition[0] = PointPlaneVector.dot(XAxis);
  PlanePosition[1] = PointPlaneVector.dot(YAxis);
  return PlanePosition;
}

SCoordinate SSlicer::toSourceCoords(const SCoordinate& reqcoord) const {
  if (PrivateData->Unsliceable)
    return reqcoord;
  else {
    // currently just use a flat plain - but could call other kernels here
    SVector coordres = PrivateData->TLC + (PrivateData->XAxis * reqcoord.x())
                                        + (PrivateData->YAxis * reqcoord.y());
    return PrivateData->toDiscreteSpace(coordres);
  }
}

bool SSlicer::fromSourceCoords(const SCoordinate& sourcecoord,
                               SCoordinate& localcoord) const {
  bool retval = false;
  if ( PrivateData->isOnSlice(sourcecoord) ) {
    retval = true;
    SVector PlanePosition(2);
    SVector PointPlaneVector;
    PointPlaneVector = (SVector(sourcecoord)-PrivateData->TLC);
    PlanePosition[0] = PointPlaneVector.dot(PrivateData->XAxis);
    PlanePosition[1] = PointPlaneVector.dot(PrivateData->YAxis);
    localcoord = PrivateData->toDiscreteSpace(PlanePosition);
  }
  return retval;
}

bool SSlicer::isOnSlice(const SVector& sourcepos) const {
  bool res = false;
  if (sourcepos.getDim()>2)
    res =  PrivateData->isOnSlice(sourcepos);
  return res;
}

void SSlicer::copyInto(SSpace& target, bool withimagedata) {
  if (PrivateData->Unsliceable)
    PrivateData->SourceImage->copyInto(target, withimagedata);
  else {
    SSpace::copyInto(target,false);
    target.resize(extent());
    SElem::Ptr sourcep = getNativeSElem();
    SElem::Ptr targetp = target.getNativeSElem();
    if (extent() == target.extent()) {
      SPtr<SSpaceIterator> srcit = getFastNativeIterator();
      SPtr<SSpaceIterator> tarit = target.getFastNativeIterator();
      srcit->toBegin();
      tarit->toBegin();
      while (!srcit->isEnd()) {
        *(*tarit) = *(*srcit);
        (*srcit)++;
        (*tarit)++;
      }
    }
  }
}

SCoordinate SSlicer::SSlicerPIMPL::toDiscreteSpace(const SVector& floatpos)
                                                                         const {
  SCoordinate res;
  res.clamp_round(floatpos);
  return res;
}

SGeom::intersection_res SSlicer::SSlicerPIMPL::lineFaceFrontIntersection
                                                                (SLine& nline) {
  SGeom::intersection_res res;
  res.intersects = false;
  for (unsigned i=0; i< FacesNum; i++) {
    SGeom::intersection_res nlinei =
                         SGeom::lineplaneintersect(nline,Faces[i]);
    if ( nlinei.intersects && (!nlinei.parallel) ) {
      if ( (nlinei.intersection - nline.start()).unit() == nline.unit() )
        res = nlinei;
    }
  }
  return res;
}

void SSlicer::SSlicerPIMPL::genPointsLine(std::vector<SElem::DataSource> &SElemsList,
                            const SPoint &sourcep, const SVector &dir,
                            unsigned int depth) {
  SPoint      workingpoint   = sourcep;
  SCoordinate workingpointd;
  SCoordinate srcext         = SourceImage->extent();
  for (unsigned i=0; i < depth; i++) {
    workingpointd = toDiscreteSpace(workingpoint);
    if (!srcext.withinSpace(workingpointd))
      break;
    SElemsList.push_back(SourceImage->SElemData_Passive(workingpointd));
    workingpoint += dir;
  }
}

void SSlicer::MaxIPGrey(const SCoordinate::Precision, const SCoordinate::Precision,
                        const SPoint& sourcep,const SVector& dir,
                        unsigned depth, SElem& sourcepox, SElem& targetpox) {
  SPoint       workingpoint  = sourcep;
  SCoordinate  workingpointd;
  const SCoordinate &srcext  = PrivateData->SourceImage->extent();
  bool          enteredspace = false;
  // step along line
  for (unsigned i=0; i < depth; i++) {
    workingpointd.clamp_floor(workingpoint);
    if (srcext.withinSpace(workingpointd)) {
      enteredspace = true;
      sourcepox.source(
                    PrivateData->SourceImage->SElemData_Passive(workingpointd));
      if ( (i==0) || // initial value
           (sourcepox.intensityUnsigned() > targetpox.intensityUnsigned()))
        targetpox = sourcepox;
    }
    else {
      if (enteredspace)
        break;
    }
    workingpoint += dir;
  }
}

void SSlicer::MinIPGrey(const SCoordinate::Precision, const SCoordinate::Precision,
                        const SPoint& sourcep,const SVector& dir,unsigned depth,
                        SElem& sourcepox, SElem& targetpox) {
  SPoint       workingpoint  = sourcep;
  SCoordinate  workingpointd;
  const SCoordinate &srcext  = PrivateData->SourceImage->extent();
  bool          enteredspace = false;
  // step along line
  for (unsigned i=0; i < depth; i++) {
    workingpointd.clamp_floor(workingpoint);
    if (srcext.withinSpace(workingpointd)) {
      enteredspace = true;
      sourcepox.source(
                    PrivateData->SourceImage->SElemData_Passive(workingpointd));
      if ( (i==0) || // initial value
           (sourcepox.intensityUnsigned() < targetpox.intensityUnsigned()))
        targetpox = sourcepox;
    }
    else {
      if (enteredspace)
        break;
    }
    workingpoint += dir;
  }
}

void SSlicer::AVGIPGrey(const SCoordinate::Precision, const SCoordinate::Precision,
                        const SPoint& sourcep,const SVector& dir,unsigned depth,
                        SElem& sourcepox, SElem& targetpox) {
  SElem::Precision intavg         = 0;
  SPoint          workingpoint   = sourcep;
  SCoordinate     workingpointd;
  const SCoordinate    &srcext   = PrivateData->SourceImage->extent();
  unsigned        dcount;
  bool          enteredspace     = false;
  // step along line
  for (dcount=1; dcount <= depth; dcount++) {
    workingpointd.clamp_floor(workingpoint);
    if (srcext.withinSpace(workingpointd)) {
      enteredspace = true;
      sourcepox.source(
                    PrivateData->SourceImage->SElemData_Passive(workingpointd));
      intavg += sourcepox.intensityUnsigned();
    }
    else {
      if (enteredspace)
        break;
    }
    workingpoint += dir;
  }
  targetpox.intensity(intavg/dcount);
}

void SSlicer::FirstOverGrey(const SCoordinate::Precision, const SCoordinate::Precision,
                            const SPoint& sourcep,const SVector& dir,
                            unsigned depth, SElem& sourcepox, SElem& targetpox,
                            SElem::Precision zerolevel, bool doshade,
                                                             SPoint &lightsrc) {
  SPoint                       workingpoint   = sourcep;
  SCoordinate                  workingpointd;
  const SCoordinate           &srcext         =
                                             PrivateData->SourceImage->extent();
  bool                         enteredspace   = false;
  targetpox.clear();
  // step along line
  for (unsigned i=0; i < depth; i++) {
    workingpointd.clamp_floor(workingpoint);
    if (srcext.withinSpace(workingpointd)) {
      enteredspace = true;
      sourcepox.source(
                    PrivateData->SourceImage->SElemData_Passive(workingpointd));
      if (sourcepox.intensityUnsigned() > zerolevel ) {
        if (doshade && i) {
          // shading
          SVector lighttopoint((workingpoint - lightsrc).unit());
          SVector::Precision scale = SGeom::fabs(lighttopoint.dot(dir));
          SElem::Precision shader =
                           PrivateData->SourceImage->LUT().getWLWidth() * scale;
          targetpox.intensity((PrivateData->SourceImage->LUT().clampWLIntensity(
                              ((PrivateData->SourceImage->LUT().getWLCentre() -
                               (PrivateData->SourceImage->LUT().getWLWidth()/2))
                                      + shader )))
                              - sourcepox.zero());
        }
        else {
          targetpox = sourcepox;
        }
        break;
      }
    }
    else {
      if (enteredspace)
        break;
    }
    workingpoint += dir;
  }
}

SElem::Precision SSlicer::triLinearInterpolateIntensity(SSpace& source,
                                                        const SVector &coords){
  SElem::Precision result = 0.0;
  SElem::Ptr native = source.getNativeSElem();
  if (coords.getDim() > 2) {
    SCoordinate basecoord;
    basecoord.clamp_floor(coords);
    basecoord.setDim(coords.getDim());
    if (source.extent().withinSpace(basecoord+SCoordinate({1,1,1}))) {
      SVector         weights;
      double          p000,p100,p010,p001,p101,p011,p110,p111;
      weights = coords - SVector(basecoord);
      double x,y,z;
      x = weights.x();
      y = weights.y();
      z = weights.z();
      // first get points: organised by order of single vector step addition
      native->source(source.SElemData_Passive(basecoord));
      p000 = native->intensityUnsigned();
      basecoord[2] += 1;
      native->source(source.SElemData_Passive(basecoord));
      p001 = native->intensityUnsigned();
      basecoord[1] += 1;
      native->source(source.SElemData_Passive(basecoord));
      p011 = native->intensityUnsigned();
      basecoord[2] -= 1;
      native->source(source.SElemData_Passive(basecoord));
      p010 = native->intensityUnsigned();
      basecoord[0] += 1;
      native->source(source.SElemData_Passive(basecoord));
      p110 = native->intensityUnsigned();
      basecoord[1] -= 1;
      native->source(source.SElemData_Passive(basecoord));
      p100 = native->intensityUnsigned();
      basecoord[2] += 1;
      native->source(source.SElemData_Passive(basecoord));
      p101 = native->intensityUnsigned();
      basecoord[1] += 1;
      native->source(source.SElemData_Passive(basecoord));
      p111 = native->intensityUnsigned();
      // calculate result
      result = (
        ( p000 * (1 - x) * (1 - y) * (1 - z) ) +
        ( p100 * x * (1 - y) * (1 - z) )       +
        ( p010 * (1 - x) * y * (1 - z) )       +
        ( p001 * (1 - x) * (1 - y) * z )       +
        ( p101 * x * (1 - y) * z )             +
        ( p011 * (1 - x) * y * z )             +
        ( p110 * x * y * (1 - z) )             +
        ( p111 * x * y * z )
        );
    }
    else {
      native->source(source.SElemData_Passive(basecoord));
      result = native->intensityUnsigned();
    }
  }
  return result;
}

void SSlicer::getRGBAScanlinesInto(SSpace* target,
                                   SCoordinate::Precision startrow,
                                   SCoordinate::Precision endrow,
                                   SCoordinate::Precision width,
                                   unsigned short  step,
                                   bool mergecontexts,
                                   float opacity,
                                   SCoordinate::Precision startcol,
                                   SCoordinate::Precision endcol,
                                   SCoordinate::Precision) {
  // balance the crop sizes
  if ( startcol % step != 0 )
    startcol -= startcol % step;
  if ( (width - endcol) % step != 0)
    endcol += (width - endcol) % step;
  SPoint ypoint                      = PrivateData->TLC +
                                                (PrivateData->YAxis * startrow);
  SPoint ypoint_incr                 = (PrivateData->YAxis * step);
  SPoint xpoint_incr                 = (PrivateData->XAxis * step);
  SPoint xpoint_preincr              = (PrivateData->XAxis * startcol);
  SCoordinate      SourcePos(3);
  SCoordinate::Precision rowc_out          = ((width/step)*(startrow/step));
  SCoordinate::Precision rowc_out_prestep  = startcol / step;
  SCoordinate::Precision rowc_out_poststep = (width - endcol) / step;
  const SCoordinate &srcext          = PrivateData->SourceImage->extent(); 
  SElem::Ptr sourcep                     = getNativeSElem();
  const bool   singleslice           = (PrivateData->Thickness < 2);
  // currently, only use interpolation on BW items
  const bool   dointerpol            = ((!(PrivateData->XAxis.isOrtho()
                                           && PrivateData->YAxis.isOrtho()))
                                        && PrivateData->SourceImage
                                           ->LUT().useWL());
  GPLUT& locallut                  = PrivateData->SourceImage->LUT();
  RGBAI32SElem targetp(nullptr);
  const uint32_t emptyholder         = 0;
  // initialise an independent SElemStore (used for projections)
  TightSElem avgstore(nullptr);
  unsigned char tmpdata[8]; // size for TightSElem
  avgstore.source(tmpdata);
  avgstore.clear();
  // initialise selem stores for all contexts
  std::vector<SElem*>             additcontexpox;
  std::vector<GPLUT*> additcontexlut;
  for ( CONTEXTS_t c=1;
       (c < PrivateData->SourceImage->contexts()) && mergecontexts;
        c++) {
    additcontexpox.push_back(PrivateData->SourceImage->selemDataContexts()
                               [c]->ContextData.getNativeSElem());
    additcontexlut.push_back
                           (&(PrivateData->SourceImage->selemDataContexts()
                                [c]->ContextLUT));
  }
  // now begin reading values from the slice geometry and writing to the target
  // WARNING: this loop is performance critical for rendering.
  // It has to be efficient and tight. Small modifications can significantly
  // affect performance.
  for (SCoordinate::Precision y = startrow; y <= endrow-step; y+=step) {
    SPoint xpoint = ypoint + xpoint_preincr;
    rowc_out += rowc_out_prestep;
    for (SCoordinate::Precision x = startcol; x <= endcol-step ; x+=step) {
      SourcePos.clamp_floor(xpoint);
      targetp.source(target->selemDataStore().SElems(rowc_out++));
      if (srcext.withinSpace(SourcePos)) {
        if (singleslice) { // single slice
          if (dointerpol) {// interpolate slice in 3D space
            targetp.rgba(locallut.lookupRGBA(
                      triLinearInterpolateIntensity(*(PrivateData->SourceImage),
                                                    xpoint)
                                            )
                        );
          }
          else {           // no interpolation, just clamp straight to pixel
            sourcep->source(PrivateData->SourceImage->
                              SElemData_Passive(SourcePos));
            targetp.rgba(locallut.lookupRGBA(*sourcep));
          }
        }
        else {              // surface function (MIP etc)
          PrivateData->LineRedFunc(this,x,y,xpoint,
                                   PrivateData->SlicePlane.normal(),
                                   PrivateData->Thickness,
                                   *sourcep,avgstore);
          targetp.rgba(locallut.lookupRGBA(avgstore));
        }
        // now merge each additional context, if requested
        for ( CONTEXTS_t c=1; 
             (c < PrivateData->SourceImage->contexts()) && mergecontexts;
              c++) {
          additcontexpox[c-1]->source
                (PrivateData->SourceImage->selemDataContexts()[c]->
                 ContextData.SElems_Passive(
                               PrivateData->SourceImage->calcIndex(SourcePos)));
          SElem::rgb_mix_zerotrans
                                 (targetp,*additcontexpox[c-1],opacity,targetp);
        }
      }
      else {
        // zero the output
        targetp.rgba(emptyholder);
      }
      xpoint += xpoint_incr;
    }
    rowc_out += rowc_out_poststep;
    ypoint   += ypoint_incr;
  }
  // cleanup context selem handlers
  for (CONTEXTS_t c=0; c < additcontexpox.size(); c++)
    delete additcontexpox[c];
}

void SSlicer::get2DRGBAInto(SSpace& target, unsigned short downsample,
                            bool mergecontexts, float opacity,
                            SCoordinate *cropb, SCoordinate *crope,
                            SCoordinate::Precision) {
  if (PrivateData->Unsliceable)
    PrivateData->SourceImage->get2DRGBAInto(target,downsample,mergecontexts,
                                            opacity,cropb, crope);
  else {
    // delegate to to faster handler if just flicking through stack
    if ( (PrivateData->Thickness == 1) &&
         (PrivateData->XAxis == SVector({1.0,0.0,0.0})) &&
         (PrivateData->YAxis == SVector({0.0,1.0,0.0})) &&
         (PrivateData->CoordinateSuffix.getDim() == 0)
       ) {
      PrivateData->SourceImage->get2DRGBAInto(target,downsample,mergecontexts,
                                              opacity,cropb,crope,slice());
    }
    else {
      SSpace::get2DRGBAInto(target,downsample,mergecontexts,opacity,cropb,
                            crope);
    }
  }
}

void SSlicer::SSlicerPIMPL::updateGeometry(bool deep) {
  if (deep) {
    Parent->setCoordinateSuffix({});
    const SCoordinate &sourcecoord = SourceImage->extent();
    // promote 2D SSpace objects to 3D (of depth 1)
    if (sourcecoord.getDim() == 2)
      SourceImage->promoteExtent();
    if(sourcecoord.getDim() < 3 ||
      (sourcecoord.volume() < 4)) {
      DEBUG_OUT("Setting a source image for Slicer with strange dimensions.");
      if (sourcecoord.getDim() > 1) {
        SliceExtent.setDim(2);
        SliceExtent.xy(sourcecoord.x(),sourcecoord.y());
      }
      else
        SliceExtent.reset();
      Unsliceable = true;
    }
    else {
      Unsliceable = false;
      initEdges();
      calcFrontBack();
    }
  }
  initSlice();
}

void SSlicer::refresh(SConnectable::sdepth_t deep) {
  PrivateData->updateGeometry(deep);
  SConnectable::refresh(deep);
}

void SSlicer::setPlane(const SPoint& orig,
                        const SVector& xvec, const SVector& yvec) {
  PrivateData->XAxis = xvec.unit();
  PrivateData->YAxis = yvec.unit();
  SPoint norig = orig;
  norig.setDim(3); // always sanitise to the assumption of 3D source
  PrivateData->SlicePlane = SPlane(norig,
                                  PrivateData->XAxis.cross(PrivateData->YAxis));
  PrivateData->calcFrontBack();
  // interal shallow update
  PrivateData->updateGeometry(false);
}

SPlane SSlicer::getPlane() const {
  return PrivateData->SlicePlane;
}

SVector SSlicer::getXAxis() const {
  return PrivateData->XAxis;
}

SVector SSlicer::getYAxis() const {
  return PrivateData->YAxis;
}

void SSlicer::setThickness(unsigned nthickness) {
  PrivateData->Thickness = nthickness;
}

void SSlicer::setReductionFunction(linereduction_t newredfunc) {
  PrivateData->LineRedFunc = newredfunc;
}

unsigned SSlicer::thickness() const {
  return PrivateData->Thickness;
}

float SSlicer::thicknessSpacing() const {
  float res = thickness();
  SVector planenormal = getPlane().normal();
  if (planenormal.getDim() ==
                          (unsigned)PrivateData->SourceImage->extent().getDim())
    res *= (planenormal * PrivateData->SourceImage->spacing()).mag();
  return res;
}

void SSlicer::sliceForwards() {
  int nslice = slice() + 1;
  if (nslice >= 0 && nslice < (long int)PrivateData->Depth)
    setSlice(nslice);
}

void SSlicer::sliceBackwards() {
  int nslice = slice() - 1;
  if (nslice >= 0 && nslice < (long int)PrivateData->Depth)
    setSlice(nslice);
}

bool SSlicer::SSlicerPIMPL::calcExtentIntersects(SLine normaline,SPoint& Front,
                                                 SPoint& Back){
  // calculate where the plane normal intersects the volume boundary both
  // in front of the slice (depth) and behind the slice (origin)
  bool                    res = false;
  SPoint                  intersects[2];
  int                     fintersects = 0;
  SGeom::intersection_res ires;
  for (unsigned fcount = 0; ((fcount < FacesNum)&& fintersects < 2); fcount++) {
    ires = SGeom::lineplaneintersect(normaline,Faces[fcount]);
    if (ires.intersects && (!ires.parallel)) {
      if (SourceImage->extent().
                              withinSpace(toDiscreteSpace(ires.intersection))) {
        // it's a valid point - inside the space, not face parallel
        intersects[fintersects] = ires.intersection;
        fintersects++;
      }
    }
  }
  if ( fintersects == 2 ) {
    res = true;
    // one of the found intersections is in front of the plane, the other behind
    if ( SGeom::isEqual(
           (intersects[0] - intersects[1]).unit().dot(SlicePlane.normal())
           ,1) 
       ) {
      Front = intersects[0];
      Back  = intersects[1];
    } else {
      Front = intersects[1];
      Back  = intersects[0];
    }
  }
  return res;
}

void SSlicer::SSlicerPIMPL::calcFrontBack() {
  if ( calcExtentIntersects(
       SLine(SlicePlane.point(), SlicePlane.point()+SlicePlane.normal()),
             FrontP,BackP
       )
     )
    Depth = (BackP - FrontP).mag() + 1;
  else {
    Depth = 1;
    FrontP.reset();
    BackP.reset();
  }
}

void SSlicer::setFrontSlice() {
  setPlane(SVector(3).xyz(0,0,0), SVector(3).xyz(1,0,0), SVector(3).xyz(0,1,0));
}

unsigned SSlicer::depth() const {
  if (PrivateData->Unsliceable)
    return 1;
  else
    return PrivateData->Depth;
}

unsigned SSlicer::slice() const {
  if (PrivateData->Unsliceable)
    return 0;
  else
    return SGeom::sround((PrivateData->BackP -
                                        PrivateData->SlicePlane.point()).mag());
}

SCoordinate SSlicer::coordinateSuffix() const {
  return PrivateData->CoordinateSuffix;
}

bool SSlicer::setSlice(unsigned nslice) {
  bool res = true;
  if (nslice <= depth()) {
    PrivateData->SlicePlane.point(PrivateData->BackP +
                                  (PrivateData->SlicePlane.normal()*nslice));
  }
  else {
    int topoint = depth();
    if (slice() != (unsigned)topoint)
      PrivateData->SlicePlane.point(PrivateData->BackP +
                                    (PrivateData->SlicePlane.normal()*topoint));
    res = false;
  }
  PrivateData->updateGeometry(false);
  return res;
}

void SSlicer::setCoordinateSuffix(const SCoordinate& newsuff) {
  PrivateData->CoordinateSuffix = newsuff;
}

void SSlicer::SSlicerPIMPL::initEdges() {
  /* For a 3D object:
   * (0,0,0) - (M,0,0) *
   * (0,0,0) - (0,M,0) *
   * (0,0,0) - (0,0,M) *
   * (0,M,0) - (0,M,M) *
   * (0,M,0) - (M,M,0) *
   * (M,M,0) - (M,0,0) *
   * (M,0,0) - (M,0,M) *
   * (0,0,M) - (M,0,M) *
   * (0,0,M) - (0,M,M) *
   * (M,0,M) - (M,M,M) *
   * (M,M,M) - (M,M,0) *
   * (M,M,M) - (0,M,M) *
   */
  // initialise all of the edge lines
  const SCoordinate &SExtent = SourceImage->extent();
  Edges[0]  = SLine(SPoint(3).xyz(0.0,0.0,0.0),SPoint(3).xyz(SExtent.x()-1,0.0,0.0));
  Edges[0].definite(true);
  Edges[1]  = SLine(SPoint(3).xyz(0.0,0.0,0.0),SPoint(3).xyz(0.0,SExtent.y()-1,0.0));
  Edges[1].definite(true);
  Edges[2]  = SLine(SPoint(3).xyz(0.0,0.0,0.0),SPoint(3).xyz(0.0,0.0,SExtent.z()-1));
  Edges[2].definite(true);
  Edges[3]  = SLine(SPoint(3).xyz(0.0,SExtent.y()-1,0.0),SPoint(3).xyz(0.0,SExtent.y()-1,SExtent.z()-1));
  Edges[3].definite(true);
  Edges[4]  = SLine(SPoint(3).xyz(0.0,SExtent.y()-1,0.0),SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,0.0));
  Edges[4].definite(true);
  Edges[5]  = SLine(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,0.0),SPoint(3).xyz(SExtent.x()-1,0.0,0.0));
  Edges[5].definite(true);
  Edges[6]  = SLine(SPoint(3).xyz(SExtent.x()-1,0.0,0.0),SPoint(3).xyz(SExtent.x()-1,0.0,SExtent.z()-1));
  Edges[6].definite(true);
  Edges[7]  = SLine(SPoint(3).xyz(0.0,0.0,SExtent.z()-1),SPoint(3).xyz(SExtent.x()-1,0.0,SExtent.z()-1));
  Edges[7].definite(true);
  Edges[8]  = SLine(SPoint(3).xyz(0.0,0.0,SExtent.z()-1),SPoint(3).xyz(0.0,SExtent.y()-1,SExtent.z()-1));
  Edges[8].definite(true);
  Edges[9]  = SLine(SPoint(3).xyz(SExtent.x()-1,0.0,SExtent.z()-1),SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1));
  Edges[9].definite(true);
  Edges[10] = SLine(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1),SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,0.0));
  Edges[10].definite(true);
  Edges[11] = SLine(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1),SPoint(3).xyz(0.0,SExtent.y()-1,SExtent.z()-1));
  Edges[11].definite(true);
  /* Initialise all of the faces:
   * Use 3 normals at origin and extent -1
   */
  Faces[0] = SPlane(SPoint(3).xyz(0,0,0),SVector(3).xyz(0,0,1));
  Faces[1] = SPlane(SPoint(3).xyz(0,0,0),SVector(3).xyz(0,1,0));
  Faces[2] = SPlane(SPoint(3).xyz(0,0,0),SVector(3).xyz(1,0,0));
  Faces[3] = SPlane(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1),SVector(3).xyz(0,0,-1));
  Faces[4] = SPlane(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1),SVector(3).xyz(0,-1,0));
  Faces[5] = SPlane(SPoint(3).xyz(SExtent.x()-1,SExtent.y()-1,SExtent.z()-1),SVector(3).xyz(-1,0,0));
}

void SSlicer::SSlicerPIMPL::initSlice() {
  if (!Unsliceable) {
    //*******************************DEBUG**************************************
#ifdef SLICE_DEBUG
    std::cerr << "---------" << std::endl;
    std::cerr << "X-Axis: " << XAxis.toString() << std::endl;
    std::cerr << "Y-Axis: " << YAxis.toString() << std::endl;
    std::cerr << "Point:  " << SlicePlane.point().toString() << std::endl;
#endif
    //**************************************************************************
    std::vector<SPoint> Intersections;
    std::vector<SPoint> Intersections_inplane;
    SCoordinate         Minpos(2), Maxpos(2);
    // find the 4 intersection points
    for (unsigned edge = 0; edge < EdgesNum; edge++) {
      SGeom::intersection_res interres = 
                              SGeom::lineplaneintersect(Edges[edge],SlicePlane);
      if ( interres.intersects ) {
        if ( interres.parallel ) {
          Intersections.push_back(Edges[edge].start());
          Intersections.push_back(Edges[edge].end());
        }
        else {
    //*******************************DEBUG**************************************
#ifdef SLICE_DEBUG
          std::cerr << "Intersection " << Intersections.size() << ": "
                    << std::endl;
          std::cerr << "  Edge: " << Edges[edge].start().toString() << " -> "
                    << Edges[edge].end().toString() << std::endl;
          std::cerr << "  at: " << interres.intersection.toString() 
                    << std::endl;
#endif
    //**************************************************************************
          Intersections.push_back(interres.intersection);
        }
      }
    }
    // remove duplicates
    std::sort( Intersections.begin(), Intersections.end() );
    Intersections.erase( std::unique( Intersections.begin(), 
                                      Intersections.end() ), 
                                      Intersections.end() );
    // now manually catch any remaining stragglers
    for (unsigned i=0; i < Intersections.size(); i++)
      for (unsigned j=i+1; j < Intersections.size(); j++) {
        if (Intersections[i] == Intersections[j] ) {
          Intersections.erase(Intersections.begin()+j);
          i = 0;
          break;
        }
      }
    // check for sanity
    if (Intersections.size() < 3 || Intersections.size() > 6) {
      TLC.reset();
      SliceExtent.reset();
      return;
    }
    // generate in-plane coordinates
    for (unsigned i = 0; i < Intersections.size(); i++)
      Intersections_inplane.push_back(sspaceToSlice(Intersections[i]));
    //*******************************DEBUG**************************************
#ifdef SLICE_DEBUG
    std::cerr << "Intersections found: " << Intersections.size() << std::endl;
    for (unsigned i=0; i< Intersections.size(); i++)
      std::cerr << "Int " << i << ": " << Intersections[i].toString() << " ("
                << Intersections_inplane[i].toString() << ") " << std::endl;
#endif
    //**************************************************************************
    // compute bounding box
    Minpos.x(std::numeric_limits<SCoordinate::Precision>::max());
    Minpos.y(std::numeric_limits<SCoordinate::Precision>::max());
    Maxpos.x(std::numeric_limits<SCoordinate::Precision>::min());
    Maxpos.y(std::numeric_limits<SCoordinate::Precision>::min());
    for (unsigned i = 0; i < Intersections_inplane.size(); i++) {
      if (Intersections_inplane[i].x() < Minpos.x())
        Minpos.x(Intersections_inplane[i].x());
      if (Intersections_inplane[i].x() > Maxpos.x())
        Maxpos.x(Intersections_inplane[i].x());
      if (Intersections_inplane[i].y() < Minpos.y())
        Minpos.y(Intersections_inplane[i].y());
      if (Intersections_inplane[i].y() > Maxpos.y())
        Maxpos.y(Intersections_inplane[i].y());
    }
    //*******************************DEBUG**************************************
#ifdef SLICE_DEBUG
    std::cerr << "LOWER BOUND: " << Minpos.toString() << " (" 
              << (SlicePlane.point()+((XAxis*Minpos.x())+
                   (YAxis*Minpos.y()))).toString() << ")" << std::endl;
    std::cerr << "UPPER BOUND: " << Maxpos.toString() << " (" 
              << (SlicePlane.point()+((XAxis*Maxpos.x())+
                   (YAxis*Maxpos.y()))).toString() << ")" << std::endl;
#endif
    //**************************************************************************
    // set the extent based on the bounding box
    SliceExtent.setDim(2);
    SliceExtent.x((Maxpos.x() - Minpos.x())+1);
    SliceExtent.y((Maxpos.y() - Minpos.y())+1);
    TLC = SlicePlane.point()+((XAxis*Minpos.x())+(YAxis*Minpos.y()));
    // handle a suffix coordinate (for greater dimensional orders)
    if (CoordinateSuffix.getDim() > 0) {
      SVector TLCp(TLC);
      TLCp.append(CoordinateSuffix);
      if (SVector(SourceImage->extent()).withinSpace(TLCp)) {
        TLC = TLCp;
      }
    }
    // set the spacing on the slice based on the source
    Spacing.setDim(2);
    Spacing.xy((XAxis*SourceImage->spacing()).mag(),
               (YAxis*SourceImage->spacing()).mag());
    //*******************************DEBUG**************************************
#ifdef SLICE_DEBUG
    std::cerr << "Extent: " << SourceImage->getExtent().toString() << std::endl
              << "X-Axis: " << XAxis.toString() << std::endl
              << "Y-Axis: " << YAxis.toString() << std::endl
              << "TLC: " << TLC.toString() << std::endl
              << "---------" << std::endl;
#endif
    //**************************************************************************
  }
}

SSpace& SSlicer::getSourceSSpace() {
  return *(PrivateData->SourceImage);
}

void SSlicer::lock() {
  SLockable::lock();
  PrivateData->SourceImage->lock();
}

bool SSlicer::try_lock() {
  bool res;
  res = SLockable::try_lock();
  if (res) { // could lock slicer
    res = PrivateData->SourceImage->try_lock();
    if (!res) // could not lock source
      SLockable::unlock(); // need to release slice lock
  }
  return res;
}

void SSlicer::unlock() {
  PrivateData->SourceImage->unlock();
  SLockable::unlock();
}

void SSlicer::wait() {
  PrivateData->SourceImage->wait();
  SLockable::wait();
}

bool SSlicer::sliceable() {
  return !PrivateData->Unsliceable;
}

SVector SSlicer::toGlobalSpace(const SVector& srccoord) {
  SCoordinate srcsp;
  srcsp.clamp_round(srccoord);
  return PrivateData->SourceImage->toGlobalSpace(toSourceCoords(srcsp));
}

SVector SSlicer::fromGlobalSpace(const SVector& srccoord) {
  return PrivateData->SourceImage->fromGlobalSpace(srccoord);
}

void SSlicer::setToGlobalSpaceFunction
                              (std::function<SVector(SSpace*,const SVector&)>) {
  SLogger::global().addMessage
                    ("SSlicer: Assigning global space translation nonsensical");
}

void SSlicer::setFromGlobalSpaceFunction
                              (std::function<SVector(SSpace*,const SVector&)>) {
  SLogger::global().addMessage
                    ("SSlicer: Assigning global space translation nonsensical");
}

std::string SSlicer::globalSpaceID() const {
  return PrivateData->SourceImage->globalSpaceID();
}

void SSlicer::setGlobalSpaceID(const std::string& ) {
  SLogger::global().addMessage
                             ("SSlicer: Assigning global space ID nonsensical");
}

std::string SSlicer::vectorMeaning(const SVector& avect, bool strict) const {
  std::string resval;
  if (avect.getDim() == 2) {
    resval = PrivateData->SourceImage->vectorMeaning
                                             ( ((PrivateData->XAxis*avect.x()) +
                                (PrivateData->YAxis*avect.y())).unit(), strict);
  }
  return resval;
}

void SSlicer::assignVectorMeaning(const SVector& , const std::string& ) {
  SLogger::global().addMessage("SSlicer: Assigning vector meaning nonsensical");
}

SVector SSlicer::meaningVector(const std::string& meaning) const {
  SVector retval;
  retval = PrivateData->SourceImage->meaningVector(meaning);
  if (retval.getDim() == 3) {
    SVector newretval(2);
    newretval.x( (PrivateData->XAxis * retval).mag() );
    newretval.y( (PrivateData->YAxis * retval).mag() );
    newretval.z( (getPlane().normal() * retval).mag() );
    retval = newretval.unit();
  }
  return retval;
}

void SSlicer::resetMeaningVectors() {
  PrivateData->SourceImage->resetMeaningVectors();
}

//------------------------------------------------------------------------------

SPtr<SSpaceIterator> SSlicer::getFastNativeIterator() {
  return SPtr<SSpaceIterator>(new SSliceIteratorNativeFast(*this));
}

SSpaceIterator* SSlicer::getFastNativeIteratorP() {
  return new SSliceIteratorNativeFast(*this);
}

const SVector& SSlicer::spacing() const {
  const SVector *retval;
  if (PrivateData->Unsliceable)
    retval = &(PrivateData->SourceImage->spacing());
  else
    retval = &(PrivateData->Spacing);
  return *retval;
}

void SSlicer::setSpacing(const SVector& ) {
  throw SimulacrumSlicerException();
}

std::string SSlicer::spacingUnits(long long int index) const {
  return PrivateData->SourceImage->spacingUnits(index);
}

void SSlicer::setSpacingUnits(const std::string&,long long int) {
  throw SimulacrumSlicerException();
}

NNode& SSlicer::informationNode() {
  return PrivateData->SourceImage->informationNode();
}

const std::string& SSlicer::getName() const {
  return PrivateData->SourceImage->getName();
}

void SSlicer::setName(const std::string& nname) {
  PrivateData->SourceImage->setName(nname);
}

GPLUT& SSlicer::LUT() {
  return PrivateData->SourceImage->LUT();
}

void SSlicer::setActiveContext(CONTEXTS_t ncon) {
  PrivateData->SourceImage->setActiveContext(ncon);
}

CONTEXTS_t SSlicer::activeContext() const {
  return PrivateData->SourceImage->activeContext();
}

CONTEXTS_t SSlicer::contexts() const {
  return PrivateData->SourceImage->contexts();
}

CONTEXTS_t SSlicer::newContext() {
  return PrivateData->SourceImage->newContext();
}

void SSlicer::removeContext(CONTEXTS_t removecon) {
  PrivateData->SourceImage->removeContext(removecon);
}

//------------------------------------------------------------------------------

class SSliceIteratorNativeFast::SSliceInteratorPIMPL {
public:
  SSlicer              *SourceSlice;
  const SCoordinate    *SourceExtent, *SourceVolExtent;
  SPoint                YPosition;
  SPoint                Position;
  SCoordinate           SlicePointPosition;
  SElem                *NativeSElem;
  void                  initNativeSElem();
  SSliceInteratorPIMPL(SSlicer& targ): SourceSlice(&targ),
                                       SourceExtent(&targ.extent()),
                              SourceVolExtent(&targ.getSourceSSpace().extent()),
                                       NativeSElem(nullptr) {
    initNativeSElem();
  }
};

SSliceIteratorNativeFast::SSliceIteratorNativeFast(SSlicer& targ):
                                   PrivateData(new SSliceInteratorPIMPL(targ)) {

}

SSliceIteratorNativeFast::SSliceIteratorNativeFast
                                        (const SSliceIteratorNativeFast& targ) {
  this->PrivateData = new SSliceInteratorPIMPL(*(targ.PrivateData->SourceSlice));
  *this = targ;
}

SSliceIteratorNativeFast::~SSliceIteratorNativeFast() {
  if (PrivateData->NativeSElem != nullptr)
    delete PrivateData->NativeSElem;
  delete PrivateData;
}

void SSliceIteratorNativeFast::toBegin() {
  toCoord(SCoordinate(PrivateData->SourceSlice->extent().getDim()));
}

bool SSliceIteratorNativeFast::isBegin() {
  return PrivateData->SlicePointPosition ==
                          SCoordinate(PrivateData->SlicePointPosition.getDim());
}

void SSliceIteratorNativeFast::toEnd() {
  SCoordinate endcoord = PrivateData->SourceSlice->extent();
  for (int i=0; i<endcoord.getDim(); i++)
    endcoord[i] -= 1;
  toCoord(endcoord);
}

bool SSliceIteratorNativeFast::isEnd() {
  return ! PrivateData->SourceExtent->withinSpace
                                              (PrivateData->SlicePointPosition);
}

void SSliceIteratorNativeFast::toCoord(const SCoordinate& reqcoord) {
  PrivateData->YPosition = PrivateData->SourceSlice->PrivateData->TLC +
              PrivateData->SourceSlice->PrivateData->YAxis * reqcoord.y();
  PrivateData->Position = PrivateData->YPosition +
                  (PrivateData->SourceSlice->PrivateData->XAxis * reqcoord.x());
  PrivateData->SlicePointPosition = reqcoord;
}

const SSpaceIterator& SSliceIteratorNativeFast::operator++() {
  if (PrivateData->SlicePointPosition[0] <
                                      ((*(PrivateData->SourceExtent))[0] - 1)) {
    // simple increment
    PrivateData->SlicePointPosition[0]++;
    PrivateData->Position += PrivateData->SourceSlice->PrivateData->XAxis;
  }
  else {
    //end of the row
      // incremement the row if space permits
      PrivateData->SlicePointPosition[0] = 0;
      PrivateData->SlicePointPosition[1] ++;
      PrivateData->YPosition += PrivateData->SourceSlice->PrivateData->YAxis;
      PrivateData->Position   = PrivateData->YPosition;
  }
  return *this;
}

const SSpaceIterator& SSliceIteratorNativeFast::operator--() {
  if (PrivateData->SlicePointPosition[0] > 0) {
    // simple decrement
    PrivateData->SlicePointPosition[0]--;
    PrivateData->Position -= PrivateData->SourceSlice->PrivateData->XAxis;
  }
  else {
    //start of the row
    if (PrivateData->SlicePointPosition[1] > 0) {
      // decremement the row if space permits
      PrivateData->SlicePointPosition[0] = (*(PrivateData->SourceExtent))[1]-1;
      PrivateData->SlicePointPosition[1] --;
      PrivateData->YPosition -= PrivateData->SourceSlice->PrivateData->YAxis;
      PrivateData->Position   = PrivateData->YPosition+
                                (PrivateData->SourceSlice->PrivateData->XAxis *
                                ((*(PrivateData->SourceExtent))[0]-1));
    }
  }
  return *this;
}

const SSpaceIterator& SSliceIteratorNativeFast::operator=
                                                  (const SSpaceIterator& targ) {
  const SSliceIteratorNativeFast *ntarg =
                           dynamic_cast<const SSliceIteratorNativeFast*>(&targ);
  if (ntarg != nullptr) {
    *this = *ntarg;
  }
  return *this;
}

const SSpaceIterator& SSliceIteratorNativeFast::operator=
                                       (const SSliceIteratorNativeFast& ntarg) {
  PrivateData->SourceSlice = ntarg.PrivateData->SourceSlice;
  PrivateData->SourceExtent = &(PrivateData->SourceSlice->extent());
  PrivateData->SourceVolExtent =
                      &(PrivateData->SourceSlice->getSourceSSpace().extent());
  PrivateData->YPosition = ntarg.PrivateData->YPosition;
  PrivateData->Position = ntarg.PrivateData->Position;
  PrivateData->SlicePointPosition = ntarg.PrivateData->SlicePointPosition;
  PrivateData->initNativeSElem();
  return *this;
}

SElem& SSliceIteratorNativeFast::operator*() const {
  SCoordinate discpos =
  PrivateData->SourceSlice->PrivateData->toDiscreteSpace(PrivateData->Position);
  if (PrivateData->SourceVolExtent->withinSpace(discpos)) {
    PrivateData->NativeSElem->source(
        PrivateData->SourceSlice->getSourceSSpace().SElemData_Passive(discpos));
  }
  else {
     PrivateData->NativeSElem->source(
     PrivateData->SourceSlice->getSourceSSpace().selemDataStore().SElem_Null());
  }
  return *(PrivateData->NativeSElem);
}

const SCoordinate& SSliceIteratorNativeFast::pos() const {
  return PrivateData->SlicePointPosition;
}

void SSliceIteratorNativeFast::SSliceInteratorPIMPL::initNativeSElem() {
  if (NativeSElem != nullptr)
    delete NativeSElem;
  NativeSElem = 
               SourceSlice->getSourceSSpace().selemDataStore().getNativeSElem();
}
