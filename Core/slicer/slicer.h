/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Slicer:
 * The Simulacrum Slicer
 * Takes an 2-3 dimensional image as input and slices tomographically along a
 * specified plane, with the standard SSpace interface.
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SLICER
#define SIMULACRUM_SLICER

#include <Core/sspace.h>
#include <vector>
#include <functional>

namespace Simulacrum {

  class SIMU_API SSlicer : public SSpace {
    friend class SSliceIteratorNativeFast;
  private:
    class SSlicerPIMPL;
    SSlicerPIMPL *PrivateData;
  protected:
    void              getRGBAScanlinesInto(SSpace* target,
                                         SCoordinate::Precision startrow,
                                         SCoordinate::Precision endrow,
                                         SCoordinate::Precision width,
                                         unsigned short  step,
                                         bool mergecontexts,
                                         float opacity,
                                         SCoordinate::Precision startcol=0,
                                         SCoordinate::Precision endcol  =0,
                                         SCoordinate::Precision =0)
                                         override;
  public:
    typedef           std::function<void(SSlicer*,
                                         const SCoordinate::Precision xx,
                                         const SCoordinate::Precision yy,
                                         const SPoint&,const SVector&,
                                         unsigned, SElem&,SElem&)>
                      linereduction_t;
                      SSlicer();
                      SSlicer(SSpace&);
                      SSlicer(const SSlicer&);
    virtual          ~SSlicer();
    virtual
    SSpace*           New   () override;
    /* following two methods for interface implemention only */
    bool              resize(const SCoordinate&) override;
    SElem::DataSource SElemData(const SCoordinate&) override;
    SElem::DataSource SElemData_Passive(const SCoordinate&) override;
    void              SElemInto(const SCoordinate&,SElem& target) override;
#ifndef SWIG
    SElem::Ptr        getNativeSElem() override;
#endif //SWIG
    SElem*            getNativeSElemP() override;
    void              setNativeSElemType(SElem*) override;
    virtual
    SElemSet&         selemDataStore() override;
    virtual
    std::vector<DataContext*>&
                      selemDataContexts() override;
    const SCoordinate&
                      extent() const override;
    void              reset() override;
    /* Slice specific methods */
    virtual
    bool              sconnect(SConnectable&) override;
    virtual
    bool              sdisconnect(SConnectable&) override;
    virtual
    void              sconfigure(const std::string&) override;
    virtual
    SCoordinate       toSourceCoords(const SCoordinate&) const override;
    virtual
    bool              fromSourceCoords(const SCoordinate& sourcecoord,
                                       SCoordinate& localcoord) const override;
    bool              isOnSlice(const SVector& sourcepos) const;
    void              setPlane(const SPoint&  orig,
                               const SVector& xvec,
                               const SVector& yvec);
    SPlane            getPlane() const;
    SVector           getXAxis() const;
    SVector           getYAxis() const;
    void              setThickness(unsigned);
    void              setReductionFunction(linereduction_t);
    unsigned          thickness() const;
    float             thicknessSpacing() const;
    void              sliceForwards();
    void              sliceBackwards();
    void              setFrontSlice();
    unsigned          slice() const;
    unsigned          depth() const;
    bool              setSlice(unsigned);
    SCoordinate       coordinateSuffix() const;
    void              setCoordinateSuffix(const SCoordinate&);
    virtual
    void              refresh(SConnectable::sdepth_t) override;
    SSpace&           getSourceSSpace() override;
    void              copyInto(SSpace&, bool withimagedata = true) override;
    void              get2DRGBAInto
                                 (SSpace& target,unsigned short downsample = 0,
                                  bool mergecontexts=true, float opacity=0.3,
                                  SCoordinate *cropb = nullptr,
                                  SCoordinate *crope = nullptr,
                                  SCoordinate::Precision zoffet = -1) override;
    void              lock() override;
    bool              try_lock() override;
    void              unlock() override;
    void              wait() override;
    bool              sliceable();
    void              MaxIPGrey(const SCoordinate::Precision xx,
                                const SCoordinate::Precision yy,
                                const SPoint&,const SVector&,unsigned,SElem&,
                                SElem&);
    void              MinIPGrey(const SCoordinate::Precision xx,
                                const SCoordinate::Precision yy,
                                const SPoint&,const SVector&,unsigned,SElem&,
                                SElem&);
    void              AVGIPGrey(const SCoordinate::Precision xx,
                                const SCoordinate::Precision yy,
                                const SPoint&,const SVector&,unsigned,SElem&,
                                SElem&);
    void              FirstOverGrey(const SCoordinate::Precision xx,
                                    const SCoordinate::Precision yy,
                                    const SPoint&,const SVector&,unsigned,
                                    SElem&,SElem&,SElem::Precision,bool,SPoint&);
    static
    SElem::Precision  triLinearInterpolateIntensity(SSpace& source,
                                                    const SVector &coords);
    SPtr<SSpaceIterator>
                      getFastNativeIterator() override;
    SSpaceIterator*   getFastNativeIteratorP() override;
    virtual
    const
    SVector&          spacing() const override;
    virtual
    void              setSpacing(const SVector&) override;
    virtual
    std::string       spacingUnits(long long int index=0) const override;
    virtual
    void              setSpacingUnits(const std::string&,long long int index=0) override;
    virtual
    NNode&            informationNode() override;
    virtual
    void              setName(const std::string&) override;
    virtual
    const
    std::string&      getName() const override;
    virtual
    GPLUT&            LUT() override;
    virtual
    void              setActiveContext(CONTEXTS_t) override;
    virtual
    CONTEXTS_t        activeContext() const override;
    virtual
    CONTEXTS_t        contexts() const override;
    virtual
    CONTEXTS_t        newContext() override;
    virtual
    void              removeContext(CONTEXTS_t) override;
    virtual
    SVector           toGlobalSpace(const SVector&) override;
    virtual
    SVector           fromGlobalSpace(const SVector&) override;
    virtual
    void              setToGlobalSpaceFunction
                               (std::function<SVector(SSpace*,const SVector&)>) override;
    virtual
    void              setFromGlobalSpaceFunction
                               (std::function<SVector(SSpace*,const SVector&)>) override;
    virtual
    std::string       globalSpaceID() const override;
    virtual
    void              setGlobalSpaceID(const std::string&) override;
    virtual
    std::string       vectorMeaning(const SVector&, bool strict=true) const override;
    virtual
    SVector           meaningVector(const std::string&) const override;
    virtual
    void              assignVectorMeaning(const SVector&,const std::string&) override;
    virtual
    void              resetMeaningVectors() override;
    using SSpace::operator=;
  };

  class SSliceIteratorNativeFast: public SSpaceIterator {
    friend class SSlicer;
  public:
                          SSliceIteratorNativeFast (SSlicer&);
                          SSliceIteratorNativeFast
                                              (const SSliceIteratorNativeFast&);
    virtual              ~SSliceIteratorNativeFast ();
    void                  toBegin              () override;
    bool                  isBegin              () override;
    void                  toEnd                () override;
    bool                  isEnd                () override;
    void                  toCoord              (const SCoordinate&) override;
    const SSpaceIterator& operator++           () override;
    const SSpaceIterator& operator--           () override;
    const SSpaceIterator& operator=            (const SSpaceIterator&) override;
    const SSpaceIterator& operator=            (const SSliceIteratorNativeFast&);
    SElem&                operator*            () const override;
    const
    SCoordinate&          pos                  () const override;
  private:
    class SSliceInteratorPIMPL;
    SSliceInteratorPIMPL *PrivateData;
  };

}

#endif //SIMULACRUM_SLICER
