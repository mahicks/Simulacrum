/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GPWLLUT:
 * Implementation specification for GP Window Level Lookup Tables
 * Author: M.A.Hicks
 */

#include "LUT.h"
#include <Core/error.h>
using namespace Simulacrum;

class GPLUT::GPLUTPIMPL {
public:
  LUTGen_t                LUTGenerator;
  std::vector <luttype>   WLLUT;
  unsigned                WindowWidth;
  unsigned                WindowCentre;
  bool                    UseWL;
  unsigned                OutRes;
  unsigned                BPPIn;
  bool                    UseChroma;
  uint8_t                 Rc,Gc,Bc;
  float                   Gamma;
  bool                    Inverted;
  bool                    Fixed;
  GPLUTPIMPL(): LUTGenerator(nullptr), WindowWidth(0), WindowCentre(0),
                UseWL(false), OutRes(32), BPPIn(16), UseChroma(false), Rc(0),
                Gc(0), Bc(0), Gamma(1.0), Inverted(false), Fixed(false) {

  }
  static double doGamma(float gamma, double inval, long int min, long int max) {
    double retval = inval;
    if (gamma != 1.0) {
      retval = pow(inval, (double)1.0/(double)gamma);
      retval = retval < min ? min : retval;
      retval = retval > max ? max : retval;
    }
    return retval;
  }
  bool initWLLUT(unsigned bitsin, unsigned bitsout,
                                    unsigned centre, unsigned width) {
    if ( bitsin == 0 || bitsout == 0 || width == 0 ) {
      WLLUT.resize(0);
      DEBUG_OUT("WARNING: Window level values specified as zero.");
      return false;
    }
    SElem::Precision MaxLookupVal = (SElem::Precision)pow(2.0,(int)bitsin);
    WLLUT.resize(MaxLookupVal);
    long int lowerBound = (long int)centre - ((long int)width/2);
    long int upperBound = (long int)centre + ((long int)width/2);
    if (lowerBound < 0)
      lowerBound = 0;
    if (upperBound > (long int)WLLUT.size())
      upperBound = WLLUT.size();
    unsigned maxVal     = pow(2.0,(int)bitsout) - 1;
    /* clamp low values to zero */
    for (long int pos = 0; (pos < lowerBound) && (pos<(long int)MaxLookupVal) ; pos++) {
      if (!Inverted)
        WLLUT[pos] = (luttype)0;
      else
        WLLUT[pos] = (luttype)maxVal;
    }
    /* clamp high values to max */
    for (long int pos = upperBound;(pos>=0) && (pos < (long int)WLLUT.size());
          pos++) {
      if (!Inverted)
        WLLUT[pos] = (luttype)maxVal;
      else
        WLLUT[pos] = (luttype)0;
    }
    /* construct the actual LUT */
    double stepFactor = (double)maxVal / (double)width;
    double accum      = 0.0;
    for (long int pos = lowerBound; pos < upperBound; pos++){
      WLLUT[pos] = (luttype)floor(accum+0.5);
      accum+=stepFactor;
    }
    /* perform a post-inversion of the LUT */
    if (Inverted) {
      if (upperBound >= (long int)WLLUT.size())
        upperBound = WLLUT.size() - 1;
      if ((size_t)lowerBound < WLLUT.size() && 
          (size_t)upperBound < WLLUT.size())
        std::reverse(WLLUT.begin()+lowerBound,WLLUT.begin()+upperBound);
    }
    return true;
  }
};

bool GPLUT::genColBlendLUT(std::vector< uint32_t > colors,
                           std::vector< luttype >& LUT, unsigned int bppin,
                           unsigned int centre, unsigned int width,
                           bool inverted) {
  bool result = true;
  if ( bppin == 0 || width == 0 || colors.size() < 2 ) {
    LUT.resize(0);
    DEBUG_OUT("WARNING: Window level values specified as zero.");
    result = false;
  }
  else {
    luttype         MaxLookupVal = colors[colors.size()-1];
    luttype         MinLookupVal = colors[0];
    SElem::Precision lutsize      = (SElem::Precision)pow(2.0,(int)bppin);
    LUT.resize(lutsize);
    long int lowerBound = (long int)centre - ((long int)width/2);
    long int upperBound = (long int)centre + ((long int)width/2);
    SElem::Precision colinterval  = SGeom::sceil(
                     (double)(upperBound-lowerBound)/(double)(colors.size()-1));
    if (lowerBound < 0)
      lowerBound = 0;
    if (upperBound > (long int)LUT.size())
      upperBound = LUT.size();
    /* clamp low values to zero */
    for (long int pos = 0; (pos < lowerBound) &&
         (pos<(long int)MaxLookupVal); pos++) {
      if (!inverted)
        LUT[pos] = MinLookupVal;
      else
        LUT[pos] = MaxLookupVal;
    }
    /* clamp high values to max */
    for (long int pos = upperBound;(pos>=0) && (pos < (long int)LUT.size());
          pos++) {
      if (!inverted)
        LUT[pos] = MaxLookupVal;
      else
        LUT[pos] = MinLookupVal;
    }
    /* construct the actual LUT */
    for (long int pos = lowerBound; pos < upperBound; pos++){
      SElem::Precision interval = (pos-lowerBound) / colinterval;
      float           mixer    =
                     (float)((pos-lowerBound) % colinterval)/(float)colinterval;
      RGBAI32SElem    target((SElem::DataSource)&LUT[pos]);
      RGBAI32SElem    icol1 ((SElem::DataSource)&colors[interval]);
      RGBAI32SElem    icol2 ((SElem::DataSource)&colors[interval+1]);
      target.red  ( ((1.0-mixer)*icol1.red  ()) + (mixer*icol2.red  ()) );
      target.green( ((1.0-mixer)*icol1.green()) + (mixer*icol2.green()) );
      target.blue ( ((1.0-mixer)*icol1.blue ()) + (mixer*icol2.blue ()) );
      target.alpha( 255 );
    }
    /* perform a post-inversion of the LUT */
    if (inverted) {
      if (upperBound >= (long int)LUT.size())
        upperBound = LUT.size() - 1;
      std::reverse(LUT.begin()+lowerBound,LUT.begin()+upperBound);
    }
  }
  return result;
}


GPLUT::GPLUT(): PrivateData(new GPLUTPIMPL()) {
  resetWL();
}

GPLUT::GPLUT(const GPLUT& source): PrivateData(new GPLUTPIMPL()) {
  copyWL(source);
}

GPLUT::~GPLUT() {
  delete PrivateData;
}

bool GPLUT::genLUT(unsigned bppin, unsigned centre, unsigned width) {
  // sanitise window centre
  unsigned centre_san;
  if ( ((long int)centre - (long int)(width/2)) < 0)
    centre_san = (width/2);
  else
    centre_san = centre;
  // set values
  PrivateData->WLLUT.clear();
  PrivateData->WindowCentre = centre_san;
  PrivateData->WindowWidth  = width;
  PrivateData->BPPIn        = bppin;
  if (PrivateData->LUTGenerator != nullptr) {
    PrivateData->OutRes = 32;
    return PrivateData->LUTGenerator(PrivateData->WLLUT,
                                     PrivateData->BPPIn,
                                     PrivateData->WindowCentre,
                                     PrivateData->WindowWidth,
                                     PrivateData->Inverted);
  }
  else {
    PrivateData->OutRes = 8;
    return PrivateData->initWLLUT(PrivateData->BPPIn,
                                  PrivateData->OutRes,
                                  PrivateData->WindowCentre,
                                  PrivateData->WindowWidth);
  }
}

void GPLUT::adjLUT(unsigned centre, unsigned width) {
  if (!PrivateData->Fixed)
    genLUT(PrivateData->BPPIn,centre,width);
}

void GPLUT::setLUTGen(GPLUT::LUTGen_t newgen) {
  PrivateData->LUTGenerator = newgen;
}

GPLUT::LUTGen_t GPLUT::lutGen() {
  return PrivateData->LUTGenerator;
}

void GPLUT::setFixedLUT(const std::vector< luttype >& newlut) {
  PrivateData->WLLUT = newlut;
  PrivateData->Fixed = true;
}

luttype GPLUT::lookup_nocheck(SElem::Precision lookupk) {
  return PrivateData->WLLUT[lookupk];
}

luttype GPLUT::lookup(SElem::Precision lookupkl) {
  SElem::Precision lookupk = lookupkl;
  if (PrivateData->Gamma != 1.0) {
    lookupk = GPLUTPIMPL::doGamma(PrivateData->Gamma,lookupkl,0,
                                  PrivateData->WLLUT.size());
  }
  if (lookupk >= sizeWL()) {
    if (sizeWL() > 0)
      return PrivateData->WLLUT[PrivateData->WLLUT.size()-1];
    else
      return 0;
  }
  return PrivateData->WLLUT[lookupk];
}

uint32_t GPLUT::lookupRGBA(SElem::Precision lookupk) {
  if (PrivateData->OutRes == 8) {
    uint32_t selemdat = 0;
    if (!PrivateData->UseChroma)
      selemdat = RGBA_ONECHAN(lookup(lookupk));
    else {
      RGBAI32SElem selemres((SElem::DataSource)&selemdat);
      float intscal = (float)lookup(lookupk) / (float)255;
      selemres.red  ((uint8_t)((float)PrivateData->Rc * intscal));
      selemres.green((uint8_t)((float)PrivateData->Gc * intscal));
      selemres.blue ((uint8_t)((float)PrivateData->Bc * intscal));
      selemres.alpha(255);
    }
    return selemdat;
  }
  else {
    return lookup(lookupk);
  }
}

uint32_t GPLUT::lookupRGBA(SElem &lookupp) {
  if (useWL()) {
    return lookupRGBA(lookupp.intensityUnsigned());
  }
  else {
    return lookupp.rgba();
  }
}

SElem::Precision  GPLUT::clampWLIntensity(SElem::Precision val) {
  SElem::Precision res;
  if (val >= PrivateData->WLLUT.size())
    res = PrivateData->WLLUT.size()-1;
  else
    res = val;
  return res;
}

SElem::Precision GPLUT::getWLWidth() {
  return PrivateData->WindowWidth;
}

SElem::Precision GPLUT::getWLCentre() {
  return PrivateData->WindowCentre;
}

SElem::Precision GPLUT::bppIn() {
  return PrivateData->BPPIn;
}

SElem::Precision GPLUT::bppOut() {
  return PrivateData->OutRes;
}

SElem::Precision GPLUT::sizeWL() {
  return PrivateData->WLLUT.size();
}

void GPLUT::useWL(bool newuse) {
  PrivateData->UseWL = newuse;
}

bool GPLUT::useWL() {
  return PrivateData->UseWL;
}

void GPLUT::resetWL() {
  useWL(false);
  PrivateData->WLLUT.clear();
  PrivateData->WindowWidth  = 0;
  PrivateData->WindowCentre = 0;
  PrivateData->OutRes       = 0;
  PrivateData->BPPIn        = 0;
  PrivateData->Gamma        = 1.0;
  PrivateData->Fixed        = false;
  setLUTGen(nullptr);
  setChroma(255,255,255);
  useWLChroma(false);
  invertWL(false);
}

GPLUT& GPLUT::copyWL(const GPLUT& target) {
  PrivateData->WLLUT         = target.PrivateData->WLLUT;
  PrivateData->WindowWidth   = target.PrivateData->WindowWidth;
  PrivateData->WindowCentre  = target.PrivateData->WindowCentre;
  PrivateData->UseWL         = target.PrivateData->UseWL;
  PrivateData->OutRes        = target.PrivateData->OutRes;
  PrivateData->BPPIn         = target.PrivateData->BPPIn;
  PrivateData->Gamma         = target.PrivateData->Gamma;
  PrivateData->UseChroma     = target.PrivateData->UseChroma;
  PrivateData->Rc            = target.PrivateData->Rc;
  PrivateData->Gc            = target.PrivateData->Gc;
  PrivateData->Bc            = target.PrivateData->Bc;
  PrivateData->Inverted      = target.PrivateData->Inverted;
  return *this;
}

void GPLUT::setChroma(uint8_t rr, uint8_t gg, uint8_t bb) {
  PrivateData->Rc = rr;
  PrivateData->Gc = gg;
  PrivateData->Bc = bb;
}

void GPLUT::useWLChroma(bool nchroma) {
  PrivateData->UseChroma = nchroma;
}

uint8_t GPLUT::getWLChromaRc() {
  return PrivateData->Rc;
}

uint8_t GPLUT::getWLChromaGc() {
  return PrivateData->Gc;
}

uint8_t GPLUT::getWLChromaBc() {
  return PrivateData->Bc;
}

void GPLUT::setGamma(float ngam) {
  PrivateData->Gamma = ngam;
}

float GPLUT::getGamma() {
  return PrivateData->Gamma;
}

void GPLUT::invertWL(bool ninvert) {
  if (ninvert)
    PrivateData->Inverted = !PrivateData->Inverted;
  else
    PrivateData->Inverted = false;
  genLUT(PrivateData->BPPIn,PrivateData->WindowCentre,PrivateData->WindowWidth);
}
