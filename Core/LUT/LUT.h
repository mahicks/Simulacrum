/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GPWLLUT:
 * Interface specification for GP Window Level Lookup Tables
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_WLLUT
#define SIMULACRUM_WLLUT
#include <Core/sprimitives.h>

#include <math.h>
#include <algorithm>
#include <functional>

namespace Simulacrum {

  typedef uint32_t luttype;
  class SIMU_API GPLUT {
  private:
    class GPLUTPIMPL;
    GPLUTPIMPL *PrivateData;
  public:
    typedef                 std::function<bool(std::vector <luttype>& LUT,
                                               unsigned bppin,unsigned centre,
                                               unsigned width, bool inverted)>
                            LUTGen_t;
                            GPLUT();
                            GPLUT(const GPLUT& source);
    virtual                ~GPLUT();
    bool                    genLUT(unsigned bppin,unsigned centre,
                                   unsigned width);
    void                    adjLUT(unsigned centre, unsigned width);
    void                    setLUTGen(LUTGen_t);
    LUTGen_t                lutGen();
    void                    setFixedLUT(const std::vector <luttype>&);
    luttype                 lookup_nocheck(SElem::Precision lookupk);
    luttype                 lookup(SElem::Precision lookupk);
    uint32_t                lookupRGBA(SElem::Precision lookupk);
    uint32_t                lookupRGBA(SElem &lookupp);
    SElem::Precision        clampWLIntensity(SElem::Precision val);
    SElem::Precision        getWLWidth();
    SElem::Precision        getWLCentre();
    SElem::Precision        bppIn();
    SElem::Precision        bppOut();
    SElem::Precision        sizeWL();
    void                    useWL(bool newuse);
    bool                    useWL();
    void                    resetWL();
    GPLUT&                  copyWL(const GPLUT& target);
    void                    setChroma(uint8_t rr, uint8_t gg, uint8_t bb);
    void                    useWLChroma(bool nchroma);
    uint8_t                 getWLChromaRc();
    uint8_t                 getWLChromaGc();
    uint8_t                 getWLChromaBc();
    void                    setGamma(float ngam);
    float                   getGamma();
    void                    invertWL(bool ninvert = true);
    // built-in reuseable lut generators
    static bool             genColBlendLUT(std::vector<uint32_t> colors,
                                           std::vector <luttype>& LUT,
                                           unsigned bppin,unsigned centre,
                                           unsigned width, bool inverted);
  };
  // for API compat at <V1.1.0
  typedef GPLUT GPWLLUT;
  #define genLinearBWWLLUT genLUT
  #define adjLinearBWWLLUT adjLUT

}

#endif
