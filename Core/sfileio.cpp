/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Implementaion for FileIO
 * Various individual file read/writers (PPM, DICOM ...)
 * Author: M.A.Hicks
 */

#include "sfileio.h"
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SFile/sfile.h>
#include "error.h"

using namespace Simulacrum;

/*------------------------------------------------------------------------------
 *                          General File I/O
 * ---------------------------------------------------------------------------*/

class SIO::SIOPIMPL {
public:
  NNode                      RootNode;
  bool                       DoStop;
  SIOPIMPL(): DoStop(false) {}
};

SIO::SIO(): PrivateData(new SIOPIMPL()) {

}

SIO::~SIO() {
  delete PrivateData;
}

/* read/write methods assume target image is already resized etc, ready to
 * receive direct selem values as data. Stream must already be at correct
 * correct position for reading/writing and opened in binary mode. */
int SIO::readTabulatedData(SSpace& targetimage, std::istream&
                           targetfile, SElem::Precision pixelbytes,
                           bool isRGB, bool isSigned, bool isInterLvd,
                           bool *dostop ) {
  bool *DoStop, blank;
  if (dostop == nullptr) {
    blank = false;
    DoStop = &blank;
  }
  else {
    DoStop = dostop;
  }
  const SCoordinate& spaceconstraint = targetimage.extent();
  SCoordinate spaceincrement         = SCoordinate(spaceconstraint.getDim());
  int retval = 0;
  typedef int64_t selemread;
  selemread tmppox;
  short rgbcount=0;
  if (pixelbytes > sizeof(selemread)) {
    SimulacrumMemException byteslimitexeeded;
    throw byteslimitexeeded;
  }
  SElem::Ptr TargetSElem = targetimage.getNativeSElem();
  int progressval = 0;
  int count       = 0;
  int maxcount    = spaceconstraint.volume();
  // check for a null SSpace
  if (spaceconstraint.volume() < 1) retval = -1;
  else {
    do {
      //calculate a progress - update only on percentage change
      int tprogress = (int)(((float)count /(float)maxcount)*100);
      if (tprogress > (progressval + 5)) {
        targetimage.progress(progressval);
        progressval = tprogress;
      }
      // now do the real work
      TargetSElem->source(targetimage.SElemData(spaceincrement));
      tmppox = 0;
      targetfile.read((char*)&tmppox,pixelbytes);
      if (isRGB && isInterLvd){
        // class interleaved rgb
        switch (rgbcount){
          case 0:
            TargetSElem->clear();
            TargetSElem->red(tmppox);
            rgbcount = 1;
            break;
          case 1:
            TargetSElem->green(tmppox);
            rgbcount = 2;
            break;
          case 2:
            TargetSElem->blue(tmppox);
            if (! spaceincrement.tabincrement(spaceconstraint) ) break;
            rgbcount = 0;
            break;
          default: /*bat-shit insanity occurred*/
            retval = -666;
            break;
        }
      }
      else{
        // not an interleaved RGB
        TargetSElem->isValid(true);
        TargetSElem->isSigned(isSigned);
        if (isSigned)
          switch (pixelbytes) {
            case 1:
              tmppox = signextend<selemread,(8)>(tmppox);
              break;
            case 2:
              tmppox = signextend<selemread,(16)>(tmppox);
              break;
            case 4:
            tmppox = signextend<selemread,(32)>(tmppox);
            break;
            case 8:
              break;
            default:
              retval = 333; // do not sign extend weird bit lengths
              break;
          }
        if (isRGB && !isInterLvd) {
          // rgb, organised into block of r,g & b
          switch (rgbcount){
            case 0:
              TargetSElem->clear();
              TargetSElem->red(tmppox);
              break;
            case 1:
              TargetSElem->green(tmppox);
              break;
            case 2:
              TargetSElem->blue(tmppox);
              break;
            default: /*bat-shit insanity occurred*/
              retval = -666;
              break;
          }
          if (! spaceincrement.tabincrement(spaceconstraint) ) { 
            if (rgbcount > 1)
              break;
            else {
              // move onto the next channel
              rgbcount++;
              spaceincrement = SCoordinate(spaceconstraint.getDim());
            }
          }
        } else {
          // plain old b/w image
          TargetSElem->clear();
          TargetSElem->intensity(tmppox);
          if (pixelbytes == 1) {
            // this is an 8-bit b/w image -- just apply straight to the RGB
            TargetSElem->red(tmppox);
            TargetSElem->green(tmppox);
            TargetSElem->blue(tmppox);
          }
          if (! spaceincrement.tabincrement(spaceconstraint) ) break;
        }
      }
      count++;
    } while ( targetfile.good() && (!(*DoStop)) );
  }
  // reset stop status
  if (*DoStop)
    *DoStop = false;
  return retval;
}

int SIO::writeTabulatedData(SSpace& source, std::ostream& target,
                            SElem::Precision bytes, bool isRGB) {
  int retval = 0;
  const SCoordinate &maxextent = source.extent();
  SCoordinate counter(maxextent.getDim());
  SElem::Ptr SourceSElem = source.getNativeSElem();
  if (maxextent.volume() < 1)
    retval = -1;
  else {
    do {
      SourceSElem->source(source.SElemData_Passive(counter));
      if (isRGB) {
        char channel = SourceSElem->red();
        target.write(&channel,1);
        channel      = SourceSElem->green();
        target.write(&channel,1);
        channel      = SourceSElem->blue();
        target.write(&channel,1);
      }
      else {
        char     channel1 = 0;
        uint16_t channel2 = 0;
        switch (bytes) {
          case 1:
            channel1 = (char)SourceSElem->intensity();
            target.write(&channel1,1);
            break;
          case 2:
            channel2 = (uint16_t)SourceSElem->intensity();
            target.write((char*)&channel2,sizeof(channel2));
            break;
          default:
            retval = 666;
            break;
        }
      }
    } while ( (counter.tabincrement(maxextent)) && (retval == 0) );
  }
  return retval;
}

/* Convert a 'maxval' for a pixel into a byte width. Better to use logs
   of number, but since in reality only a few values will ever be used,
   just use a switch */
unsigned SIO::maxvaltobytewidth(unsigned themaxval) {
  switch (themaxval){
    case 255:
      return 1;
    case 511:
      return 2;
    case 1023:
      return 4;
    case 127:
      throw SimulacrumIOException();
    default:
      throw SimulacrumIOException();
  }
}

std::string SIO::getInfo(const std::string&) {
  std::string tmpinfo;
  if (isValid()) {
    SFile fileinfo(getLocation());
    SURI  uriinfo(getLocation());
    std::stringstream fsize;
    // roudn the size to 1 DP
    if (fileinfo.exists())
      fsize << (static_cast<float>
            (static_cast<int>((fileinfo.size()/1024.0) * 10.0)) / 10.0);
    else
      fsize << 0;

    tmpinfo += "<b>File name:</b><br/>";
    tmpinfo += uriinfo.getComponent(uriinfo.depth()-1);
    tmpinfo += "<br/><b>File size:</b><br/>";
    tmpinfo += fsize.str();
    tmpinfo += " KiB";
  }
  return tmpinfo;
}

SAbsTreeNode& SIO::getRoot() {
  PrivateData->RootNode.setName(resourceType());
  PrivateData->RootNode.setData(getLocation());
  return PrivateData->RootNode;
}

bool SIO::hasArchive() {
  return false;
}

bool SIO::hasSSpace(const std::string&) {
  return isValid();
}

int SIO::loadSSpace(SSpace& targ) {
  labelSSpace(targ,"");
  return 0;
}

int SIO::storeSSpace(SSpace& targ) {
  labelSSpace(targ,"");
  return 0;
}

void SIO::getSSpaceInto(SSpace& targ, const std::string&) {
  loadSSpace(targ);
}

void SIO::putSSpaceInto(SSpace& src, const std::string&) {
  storeSSpace(src);
}

const std::string SIO::getLocationPathID(const std::string&) {
  return getLocation();
}

void SIO::doStop(bool nstop) {
  PrivateData->DoStop = nstop;
}

bool& SIO::stop() {
  return PrivateData->DoStop;
}

void SIO::refresh(SConnectable::sdepth_t) {
  std::string curloc = getLocation();
  clear();
  setLocation(curloc);
}

void SIO::store() {
 // difficult to store an image file without first providing the image
}
