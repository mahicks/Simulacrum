/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Connectable
 * A partial implementation to allow components to be 'connected'
 * M. A. Hicks
 */

#include "sconnectable.h"
#include "error.h"
#include "SPool/SPool.h"

using namespace Simulacrum;

namespace Simulacrum {
  class SimulacrumConnect: public SimulacrumException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: Invalid SConnect";
    }
  };
}

class SConnectable::SConnectablePIMPL {
public:
  bool                                  DeleteOnDisconnect;
  std::map<SConnectable*,SConnectable*> Listeners, ListeningTo;
  std::map<std::string,double>          Parameters;
  std::map<std::string,std::string>     Properties;
  std::string                           Name;
  SConnectablePIMPL(): DeleteOnDisconnect(false) { }
};

SConnectable::SConnectable(): PrivateData(new SConnectablePIMPL()) {

}

SConnectable::~SConnectable() {
  deleteOnDisconnect(false);
  disconnectSources();
  disconnectListeners();
  delete PrivateData;
}

void SConnectable::emitRefresh(SConnectable::sdepth_t rval) {
  std::map<SConnectable*,SConnectable*>::iterator upt;
  for ( upt = PrivateData->Listeners.begin();
        upt != PrivateData->Listeners.end(); upt ++)
    upt->second->refresh(rval);
}

void SConnectable::progress(int prog) {
  std::map<SConnectable*,SConnectable*>::iterator upt;
  for ( upt = PrivateData->Listeners.begin();
        upt != PrivateData->Listeners.end(); upt ++)
    upt->second->progress(prog);
}


bool SConnectable::addListener(SConnectable& newlis) {
  bool result = false;
  if (PrivateData->Listeners.count(&newlis) == 0) {
    PrivateData->Listeners.insert
                      (std::pair<SConnectable*,SConnectable*>(&newlis,&newlis));
    result = true;
  }
  return result;
}

bool SConnectable::removeListener(SConnectable& oldlis) {
  bool retval = false;
  if (PrivateData->Listeners.count(&oldlis)) {
    retval = true;
    PrivateData->Listeners.erase(&oldlis);
  }
  return retval;
}

bool SConnectable::addTarget(SConnectable& newtarg) {
  bool result = false;
  if (PrivateData->ListeningTo.count(&newtarg) == 0) {
    PrivateData->ListeningTo.insert
                    (std::pair<SConnectable*,SConnectable*>(&newtarg,&newtarg));
    result = true;
  }
  return result;
}

bool SConnectable::removeTarget(SConnectable& oldtarg) {
  bool retval = false;
  if (PrivateData->ListeningTo.count(&oldtarg)) {
    retval = true;
    PrivateData->ListeningTo.erase(&oldtarg);
  }
  return retval;
}

void SConnectable::disconnectListeners() {
  std::map<SConnectable*,SConnectable*>::iterator targit;
  std::map<SConnectable*,SConnectable*> tmplist = PrivateData->Listeners;
  PrivateData->Listeners.clear();
  for (targit = tmplist.begin(); targit != tmplist.end(); targit++)
    targit->second->sdisconnect(*this);
}

void SConnectable::disconnectListeners_Refresh() {
  std::map<SConnectable*,SConnectable*> tmplist = PrivateData->Listeners;
  disconnectListeners();
  std::map<SConnectable*,SConnectable*>::iterator targit;
  for (targit = tmplist.begin(); targit != tmplist.end(); targit++)
    targit->second->refresh(true);
}

void SConnectable::disconnectSources() {
  std::map<SConnectable*,SConnectable*>::iterator targit;
  std::map<SConnectable*,SConnectable*> tmplist = PrivateData->ListeningTo;
  PrivateData->ListeningTo.clear(); // (CL) - see below
  for (targit = tmplist.begin(); targit != tmplist.end(); targit++) {
      (*targit->second).refDecr(); // must do this manually since (CL)
    sdisconnect(*targit->second);
  }
}

void SConnectable::sconfigure(const std::string&) {

}

void SConnectable::refresh(SConnectable::sdepth_t rval) {
  emitRefresh(rval);
}

void SConnectable::signal(unsigned int type, void* object) {
  emitSignal(type,object);
}

void SConnectable::emitSignal(unsigned int type, void* object) {
  std::map<SConnectable*,SConnectable*>::iterator upt;
  for ( upt = PrivateData->Listeners.begin();
        upt != PrivateData->Listeners.end(); upt ++)
    upt->second->signal(type,object);
}

bool SConnectable::sconnect(SConnectable& starget) {
  bool didcon = false;
  didcon = addTarget(starget);
  if (didcon) {
    starget.addListener(*this);
    starget.refIncr();
  }
  return didcon;
}

bool SConnectable::sconnect(SConnectable& starget, bool delondiscon) {
  starget.deleteOnDisconnect(delondiscon);
  return sconnect(starget);
}

bool SConnectable::sdisconnect(SConnectable& starget) {
  bool didrem = removeTarget(starget);
  starget.removeListener(*this);
  if (didrem) {
    starget.refDecr();
  }
  // delete the object if we are the last listener
  if ( starget.PrivateData->DeleteOnDisconnect     &&
      (starget.PrivateData->Listeners.size() == 0) &&
      (starget.refCount() == 0) ) {
    starget.disconnectSources();
    delete &starget;
  }
  return didrem;
}

bool SConnectable::isConnected(SConnectable* targ) const {
  return (PrivateData->ListeningTo.count(targ) > 0);
}

bool SConnectable::isConnected(SConnectable& targ) const {
  return isConnected(&targ);  
}

SConnectable& SConnectable::end() {
  SConnectable *retval = this;
  if (&(succ()) != this)
    retval = &(succ().end());
  return *retval;
}

SConnectable& SConnectable::penum() {
  SConnectable *retval = this;
  if (&(succ().succ()) != &(succ()))
    retval = &(succ());
  else
    retval = this;
  return *retval;
}

SConnectable& SConnectable::pred() {
  SConnectable *retval = this;
  if (PrivateData->Listeners.size() > 0)
    retval = PrivateData->Listeners.end()->second;
  return *retval;
}

SConnectable& SConnectable::succ() {
  SConnectable *retval = this;
  if (PrivateData->ListeningTo.size() > 0)
    retval = PrivateData->ListeningTo.begin()->second;
  return *retval;
}

void SConnectable::deleteOnDisconnect(bool dodel) {
  PrivateData->DeleteOnDisconnect = dodel;
}

bool SConnectable::deleteOnDisconnect() {
  return PrivateData->DeleteOnDisconnect;
}

void SConnectable::setName(const std::string& newname) {
  PrivateData->Name = newname;
}

const std::string& SConnectable::getName() const {
  return PrivateData->Name;
}

const std::map< std::string, double >& SConnectable::parameters() {
  return PrivateData->Parameters;
}

const std::map< std::string, std::string >& SConnectable::properties() {
  return PrivateData->Properties;
}

bool SConnectable::hasProperty(const std::string& prop) const {
  return PrivateData->Properties.count(prop);
}

bool SConnectable::hasParameter(const std::string& param) const {
  return PrivateData->Parameters.count(param);
}

void SConnectable::setProperty(const std::string& prop,const std::string& val) {
  PrivateData->Properties[prop] = val;
}

void SConnectable::setParameter(const std::string& param, double val) {
  PrivateData->Parameters[param] = val;
}

std::string SConnectable::getProperty(const std::string& prop) const {
  return PrivateData->Properties.at(prop);
}

double SConnectable::getParameter(const std::string& param) const {
  return PrivateData->Parameters.at(param);
}
