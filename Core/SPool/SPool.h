/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SPool:
 * A simple thread pool, using C++11 (or BOOST) threads
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SPOOL
#define SIMULACRUM_SPOOL

#include <functional>

#include <Core/definitions.h>

namespace Simulacrum {

  class SIMU_API SPool {
  public:
    typedef std::function< void() > SFunction;
    typedef unsigned SDependency_t;
    typedef unsigned long SThreadID_t;
         SPool();
         SPool(int poolsize);
        ~SPool();
    void addJob(SFunction, bool imdispatch = true);
    void addJob(SFunction,
                std::initializer_list<SDependency_t> depends,
                bool imdispatch = true);
    SDependency_t
         addJob_p(SFunction,
                  std::initializer_list<SDependency_t> depends,
                  bool imdispatch = true);
    void wait();
    void setPoolSize(int);
    int  jobs();
    int  poolSize();
    int  activeThreads();
    void dispatcher(SThreadID_t=0, SDependency_t=0);
    static
    void fireAndForget(SPool::SFunction);
  private:
    struct SPoolPimpl;
    SPoolPimpl *PrivateData;
  };

}

#endif //SIMULACRUM_SPOOL
