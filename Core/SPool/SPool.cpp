/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SPool:
 * A simple thread pool, using C++11 (or BOOST) threads
 * Author: M.A.Hicks
 */

#include "SPool.h"
#include <Toolbox/sysInfo/sysInfo.h>
#include <thread>

// Mappings between boost and C++11
#ifdef BOOST_MUTEX // USE BOOST
  #include <boost/thread/condition_variable.hpp>
  #define SCXXECV boost::condition_variable
  #include <boost/thread/mutex.hpp>
  #define SCXXEMutex boost::mutex
  #include <boost/thread/locks.hpp>
  #define SCXXEULock boost::unique_lock
#else // USE CPP11
  #include <condition_variable>
  #define SCXXECV std::condition_variable
  #include <mutex>
  #define SCXXEMutex std::mutex
  #define SCXXEULock std::unique_lock
#endif //BOOST_MUTEX CHECK

#include <vector>
#include <deque>
#include <set>
#include <map>

#include <Core/slockable.h>

using namespace Simulacrum;

namespace Simulacrum {
  typedef std::thread SThread;
  struct QueueItem {
    SPool::SFunction func;
    SPool::SDependency_t provid;
    std::vector<SPool::SDependency_t> deps;
  };
}

struct SPool::SPoolPimpl {
  SCXXEMutex               GenLock;
  long int                 PoolSize;
  SThreadID_t              IDCounter;
  std::map< SThreadID_t,SThread* >
                           ActiveThreadSet;
  std::deque< QueueItem >  JobQueue;
  SCXXECV                  JobsCond;
  std::map<SPool::SDependency_t,SLockable>
                           DepLocks;
  SPool::SDependency_t     DepIDCounter;
};

SPool::SPool() {
  PrivateData = new SPoolPimpl;
  setPoolSize(sysInfo::systemConcurrency());
  PrivateData->DepIDCounter = 1;
  PrivateData->IDCounter = 1;
}

SPool::SPool(int poolsize) {
  PrivateData = new SPoolPimpl;
  setPoolSize(poolsize);
  PrivateData->DepIDCounter = 1;
  PrivateData->IDCounter = 1;
}

SPool::~SPool() {
  wait();
  delete PrivateData;
}

void SPool::addJob(SFunction newjob, bool imdispatch) {
  addJob(newjob,{},imdispatch);
}

void SPool::addJob(SFunction newjob,
                   std::initializer_list<SDependency_t> depends,
                   bool imdispatch) {
  PrivateData->GenLock.lock();
  QueueItem newitem;
  newitem.func = newjob;
  newitem.provid = 0;
  newitem.deps = depends;
  PrivateData->JobQueue.push_back(newitem);
  PrivateData->GenLock.unlock();
  if (imdispatch)
    dispatcher();
}

SPool::SDependency_t SPool::addJob_p(SFunction newjob,
                        std::initializer_list< SPool::SDependency_t > depends,
                        bool imdispatch) {
  PrivateData->GenLock.lock();
  // create the dependency provider locker
  QueueItem newitem;
  newitem.provid = PrivateData->DepIDCounter++;
  PrivateData->DepLocks[newitem.provid].refIncr();
  // add the job
  newitem.func = newjob;
  newitem.deps = depends;
  PrivateData->JobQueue.push_back(newitem);
  PrivateData->GenLock.unlock();
  if (imdispatch)
    dispatcher();
  return newitem.provid;
}

void SPool::wait() {
  // wait for pending jobs (and then block new ones)
  SCXXEULock<SCXXEMutex> ul(PrivateData->GenLock);
  PrivateData->JobsCond.wait(ul,
    [this]()->bool{return (PrivateData->JobQueue.size()==0) &&
                          (PrivateData->ActiveThreadSet.size()==0); }
                            );
}

void SPool::setPoolSize(int newsize) {
  PrivateData->GenLock.lock();
  PrivateData->PoolSize = newsize;
  PrivateData->GenLock.unlock();
}

int SPool::jobs() {
  return PrivateData->JobQueue.size();
}

int SPool::poolSize() {
  return PrivateData->PoolSize;
}

int SPool::activeThreads() {
  return PrivateData->ActiveThreadSet.size();
}


// encapsulate a thread execution and return to the dispatcher after completion
static void worker(SPool *targ, SPool::SFunction work, SPool::SThreadID_t id,
                   SPool::SDependency_t depid) {
  // do work
  work();
  // call dispatcher
  targ->dispatcher(id, depid);
}

// returns true if all extant deplocks are unlocked, otherwise false
// non existant named dependencies do not block
static inline bool isJobReady(std::vector<SPool::SDependency_t> &deps,
                           std::map<SPool::SDependency_t,SLockable>& deplocks) {
  bool result = true;
  for (unsigned int i=0;i < deps.size(); i++) {
    if (deplocks.count(deps[i])) {
      if ((deplocks[deps[i]].refCount() > 0) ) {
        result = false;
        break;
      }
    }
  }
  return result;
}

// handle the scheduling/dispatching of queued threads
void SPool::dispatcher(SThreadID_t cleanup, SDependency_t providc) {
  PrivateData->GenLock.lock();
  if (cleanup != 0) {
    PrivateData->ActiveThreadSet.erase(cleanup);
    if (PrivateData->DepLocks.count(providc))
      PrivateData->DepLocks[providc].refDecr();
  }
  // fill the pool as far as possible
  while ( (PrivateData->ActiveThreadSet.size() < 
                                            (unsigned)PrivateData->PoolSize) &&
          (PrivateData->JobQueue.size() > 0) ) {
    SThread workThread;
    SFunction job = nullptr;
    SDependency_t provid = 0;
    SThreadID_t tid = PrivateData->IDCounter++;
    // add the new work thread to the thread set, for waiting
    PrivateData->ActiveThreadSet[tid] = &workThread;
    // get the next (ready) job
    for (unsigned j=0; j < PrivateData->JobQueue.size(); j++) {
      if (isJobReady(PrivateData->JobQueue[j].deps, PrivateData->DepLocks)) {
        // record the job found
        job = PrivateData->JobQueue[j].func;
        provid = PrivateData->JobQueue[j].provid;
        // remove the job from the queue
        PrivateData->JobQueue.erase(PrivateData->JobQueue.begin() + j);
        break;
      }
    }
    // dispatch the job, if found
    if (job != nullptr) {
      // assign the job work to the thread
      workThread = SThread(std::bind(worker,this,job,tid,provid));
      // allow the thread to run in the background
      workThread.detach();
    }
  }
  if (PrivateData->JobQueue.size()        == 0 && 
      PrivateData->ActiveThreadSet.size() == 0) {
    PrivateData->JobsCond.notify_all();
  }
  PrivateData->GenLock.unlock();
}

void SPool::fireAndForget(SPool::SFunction forgetme) {
  SThread newthread(forgetme);
  newthread.detach();
}
