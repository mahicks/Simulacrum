/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::NTree:
 * A simple and fast class for modelling tree structures and reading/writing
 * from/to disk. Principally designed for XML.
 * Part of Simulacrum, but can be used as a standalone library.
 * Designed to be very lightweight, and allowing library freedom.
 *
 * Author: M.A.Hicks
 */

#include "nnode.h"
#include <Core/error.h>

using namespace Simulacrum;

namespace Simulacrum {
  class NNodeException: public std::exception {
  protected:
    virtual const char* what() const throw(){
      return "NNode: Fatal Exception!";
    }
    bool isFatal(){
      return true;
    }
  };
}

class NNode::NNodePIMPL {
public:
  /*     -------Storage Maps-------
  *  Store name, loaded data and children.
  *  Declared publicly for simplicity.
  */
  NNode*                   Parent;
  NNode*                   Attributes;
  bool                     AttributeContainer;
  bool                     AttributeNode;
  ChildMap                 Children;
  XMLDataStore             Data;
  std::string              Name;
  bool                     SpecialNode;
  bool                     Good;
  static const char*       XMLVersion;
  static const char*       AttributeNodeName;
  int                      LexerComment;
  std::vector<XMLCharType> LexerBuffer;
  /* --=LEXER=--
    * tokens that are returned by lexer
    */
  enum XMLTokens  {XML_STR,OPN_TAG,CLS_TAG,ESC_CHR,END_CHR,ASN_CHR,STR_DLM,
                    WHT_SPC, EOF_MKR, NLL_TKN, SPC_TKN};
  // represent a fetched token
  class XMLToken {
  public:
    int Token;
    unsigned Line, Column;
    // data only valid for string tokens; check before reading
    std::vector<XMLCharType> Data;
    /* State data for Lexer
    * This is bad in a way, but allows for a re-entrant lexer method for
    * multithreaded parsing usage
    */
    XMLCharType tempdata;
    bool        donotread;
    XMLToken(){
      Token        = Line = Column = 0;
      tempdata     = '\0';
      donotread    = false;
    }
  };
  /* --=Parser=--
  * Is not specific to any underlying character to format (but whitespace may
  * need checking)
  */
  enum XMLParseStates {NODE_RDY, NODE_DCL, NODE_ATT, NODE_CNT, ATR_NME,
                      ATR_VAL, ATR_DAT, NODE_END, NODE_ERR, NODE_CLS,
                      NODE_CLI};
  enum NNodeActions   {ADD_NAME, ADD_ATNM, ADD_ATDT, ADD_NODE, ADD_DATA,
                      CLOSE_NODE,CLOSE_INLINE, ERROR_A, NULL_A, END_FLE,
                      MKSP_NODE, INIT_NODE};
  static const unsigned ParseTokens  = 11;
  static const unsigned ParseStates  = 11;
  static const unsigned ParseActions = 10;
  static char* XMLParseStates_str[ParseStates];
  static char* XMLTokens_str     [ParseTokens];
  static char* NULLNAME;
  static bool  InitStatics;
  // Action Table
  static int actiontable[ParseStates][ParseTokens];
  // GOTO Table
  static int gototable  [ParseStates][ParseTokens];
  /* The main parser method.
  * Recursively add NNodes to root node while reading XML file.
  */
  bool readXMLTree(NNode* curr,std::iostream& xmlsource,XMLParseRes& result);
  // a conveniece enum for lexer tokens (currently ASCII-locked!)
  enum XMLChars   {XMLSTRING,OPEN_TAG='<',CLOSE_TAG='>',ESCAPE_CHAR='&',
                  END_CHAR='/',ASSIGN_CHAR='=',STR_DELIM='"',EOF_CHAR='\0',
                  QST_CHAR='?',EXC_CHAR='!',ENDESC_CHAR=';',HYPH_CHAR='-'};
  //               **********The Lexer**************
  // read *one* token from the input file and initialise the target token
  bool getXMLToken(std::iostream& xmlsource, XMLToken& targettoken,
                  bool whitespacesens=false, bool gobble=false);
  /* writeXMLTree - The main XML writer
  * Writes an XML tree to a stream, from the current node and children.
  * No indentation happens here.
  */
  bool writeXMLTree(std::iostream& xmltarget,unsigned bound=0,
                    unsigned depth=0);
  // check if a character is a whitespace character
  static inline bool isWhiteSpace(XMLCharType achar);
  // extend the method above to a string
  static bool isStrWhiteSpace(XMLCharType* thestr, unsigned thelen);
  // translate a character to a token value
  inline int chartotoken(XMLCharType xmlchar);
  // translate a token into an xml character
  inline XMLCharType tokentochar(int atoken);
  static const int   XMLESCCharsNum;
  static XMLCharType XMLESCChars[];
  static char        *XMLESCCharsSeq[];
  XMLCharType escapedToChar(std::iostream& xmlsource);
  bool writeEscapeSequence(NNode::XMLCharType,std::iostream& xmltarget);
  // --------------------------------------------
  /* A method to initialise the tables to default values (NODE_ERR and ERRO_A)
   * and then add the sparse useful actions/states.
   */
  static void init_tables();
  NNodePIMPL() {
    Parent = nullptr;
    // init parsing tables
    init_tables();
    Name               = std::string(NULLNAME);
    Good               = true;
    SpecialNode        = false;
    Attributes         = nullptr;
    LexerComment       = 0;
    AttributeNode      = false;
    AttributeContainer = false;
  }
};

const char* NNode::NNodePIMPL::XMLVersion = "1.0";
const char* NNode::NNodePIMPL::AttributeNodeName = "*Attributes";

NNode::NNode(): PrivateData(new NNodePIMPL()) {

}

NNode::NNode(NNode& target): PrivateData(new NNodePIMPL())  {
  (*this) = target;
}

NNode::NNode(const NNode&): PrivateData(new NNodePIMPL())  {

}

NNode::~NNode(){
  this->clear();
  delete PrivateData;
}

void NNode::clear(){
  PrivateData->Data.clear();
  if (PrivateData->Attributes != nullptr) {
    delete PrivateData->Attributes;
    PrivateData->Attributes = nullptr;
  }
  //each child node needs to be deleted/freed
  for (ChildMap::iterator i = PrivateData->Children.begin();
       i != PrivateData->Children.end(); i++)
    delete i->second;
  PrivateData->Children.clear();
  PrivateData->Name = "Empty";
  PrivateData->Good = true;
  PrivateData->Parent = nullptr;
}

void NNode::detachNode() {
  if (hasParentNode() && (!PrivateData->AttributeContainer)) {
    NNode &lparent = getParentNode();
    for (ChildMap::iterator it = lparent.PrivateData->Children.begin();
         it != lparent.PrivateData->Children.end(); it++)
      if (it->second == this) {
        lparent.PrivateData->Children.erase(it);
        break;
      }
    PrivateData->Parent = nullptr;
  }
}

bool NNode::removeNode() {
  if (!PrivateData->AttributeContainer) {
    detachNode();
    delete this;
    return true;
  }
  else {
    return false;
  }
}

/* isGood() -- report health status of tree below this node
  * Currently just returns 'goodness', but could make more checks.
  */
bool NNode::good() const {
  return PrivateData->Good;
}

NNode::XMLParseRes NNode::loadFromXMLFile(const std::string& xmlfile){
  XMLParseRes  parseresult;
  parseresult.Column    = 0;
  parseresult.Line      = 0;
  parseresult.Token_str = (char*)"Empty.";
  parseresult.State_str = (char*)"Empty.";
  std::fstream xmlfilein;
  xmlfilein.open(xmlfile.c_str(), std::ios::in | std::ios::binary);
  if (!xmlfilein.is_open()){
    parseresult.Messages = "File I/O error.";
    PrivateData->Good = false;
  }
  else {
    // call the parser
    PrivateData->readXMLTree(this,xmlfilein, parseresult);
    xmlfilein.close();
    //set friendly string pointers
    parseresult.Token_str = PrivateData->XMLTokens_str[parseresult.Token];
    parseresult.State_str = PrivateData->XMLParseStates_str[parseresult.State];
  }
  return parseresult;
}

NNode::XMLParseRes NNode::loadFromXML(std::stringstream& xmlsource) {
  XMLParseRes  parseresult;
  parseresult.Column    = 0;
  parseresult.Line      = 0;
  parseresult.Token_str = (char*)"Empty.";
  parseresult.State_str = (char*)"Empty.";
  // call the parser
  PrivateData->readXMLTree(this,xmlsource, parseresult);
  //set friendly string pointers
  parseresult.Token_str = PrivateData->XMLTokens_str[parseresult.Token];
  parseresult.State_str = PrivateData->XMLParseStates_str[parseresult.State];
  return parseresult;
}

bool NNode::storeToXMLFile(const std::string& xmlfile, bool append){
  std::fstream xmlfileout;
  bool writeres = false;
  std::ios_base::openmode oflags = std::ios::out | std::ios::binary;
  if (append)
    oflags = oflags | std::ios::app;
  xmlfileout.open(xmlfile.c_str(), oflags);
  // call the writer
  writeres = PrivateData->writeXMLTree(xmlfileout);
  xmlfileout.close();
  return writeres;
}

bool NNode::storeToXML(std::stringstream& xmltarget) {
  bool writeres = false;
  // call the writer
  writeres = PrivateData->writeXMLTree(xmltarget);
  return writeres;
}

void NNode::printTree(unsigned depth){
  if(!depth) std::cout << "-------------" << std::endl << "NNode Listing"
                        << std::endl << "-------------" << std::endl;
  // indent
  for (unsigned i=0;i<depth;i++)
    std::cout << " ";
  //this node
  std::cout << "NODE=" << this->PrivateData->Name << "(";
  if (PrivateData->Attributes != nullptr)
    for(ChildMap::iterator a =
                         PrivateData->Attributes->PrivateData->Children.begin();
        a!=PrivateData->Attributes->PrivateData->Children.end();a++)
      std::cout << a->first << "=\"" << a->second->getData() << "\":";
  std::cout << ") " << getData();
  if (PrivateData->SpecialNode)
    std::cout << " *";
  std::cout << std::endl;
  // for each child
  for(ChildMap::iterator c = PrivateData->Children.begin();
                                             c!=PrivateData->Children.end();c++)
    c->second->printTree(depth+1);
  if(!depth) std::cout << "-------------" << std::endl;
}

unsigned NNode::childNodeCount(const std::string& getch) {
  std::string getchp = getch;
  normaliseString(getchp);
  return PrivateData->Children.count(getchp);
}

bool NNode::hasChildNode(const std::string& getch) {
  return childNodeCount(getch) > 0;
}


NNode& NNode::getChildNode(const std::string& getch) {
  std::string getchp = getch;
  normaliseString(getchp);
  return *(PrivateData->Children.find(getchp)->second);
}

NNode& NNode::xmlRootNode() {
  NNode *xmlroot = nullptr;
  for (ChildMap::iterator cit = PrivateData->Children.begin();
                                      cit != PrivateData->Children.end(); cit++)
    if (! cit->second->PrivateData->SpecialNode) {
      xmlroot = cit->second;
      break;
    }
  if (xmlroot == nullptr)
    throw NNodeException();
  return *xmlroot;
}

NNode::ChildMap& NNode::getChildNodes() {
  return PrivateData->Children;
}

bool NNode::hasParentNode() {
  return (PrivateData->Parent != nullptr);
}

NNode& NNode::getParentNode() {
  if (PrivateData->Parent == nullptr)
    throw NNodeException();
  else
    return *(PrivateData->Parent);
}

NNode& NNode::addChildNode(NNode* newnode) {
  if (newnode != nullptr) {
    newnode->PrivateData->Parent = this;
    newnode->PrivateData->AttributeNode = PrivateData->AttributeNode;
    PrivateData->
     Children.insert(std::pair<std::string,NNode*>(newnode->getName(),newnode));
  }
  return *newnode;
}

NNode& NNode::addChildNode(NNode& newnode) {
  NNode *newchild = new NNode();
  *newchild = newnode;
  return addChildNode(newchild);
}

NNode& NNode::operator=(NNode& rhs) {
  setName(rhs.getName());
  setData(rhs.getData());
  if (rhs.PrivateData->Attributes != nullptr)
    for ( ChildMap::iterator attr =
                           rhs.PrivateData->Attributes->getChildNodes().begin();
          attr != rhs.PrivateData->Attributes->getChildNodes().end(); attr++ )
      setAttribute(attr->second->getName(),attr->second->getData());
  for ( ChildMap::iterator child = rhs.getChildNodes().begin();
        child != rhs.getChildNodes().end(); child++ ) {
    NNode *newchild = new NNode();
    *newchild = *(child->second);
    addChildNode(newchild);
  }
  return *this;
}

bool NNode::hasAttribute(const std::string& getat) {
  bool res = false;
  if (PrivateData->Attributes != nullptr)
    res = PrivateData->Attributes->hasChildNode(getat);
  return res;
}

std::string NNode::getAttribute(const std::string& getat) {
  std::string attr = "";
  if (PrivateData->Attributes != nullptr)
    attr = PrivateData->Attributes->getChildNode(getat).getData();
  return attr;
}

void NNode::setAttribute(const std::string& atn, const std::string& atv) {
  // initialise attributes, if it does not already exist
  if (PrivateData->Attributes == nullptr) {
    PrivateData->Attributes = new NNode();
    PrivateData->Attributes->setName(NNode::NNodePIMPL::AttributeNodeName);
    PrivateData->Attributes->PrivateData->AttributeContainer = true;
    PrivateData->Attributes->PrivateData->AttributeNode = true;
  }
  if (PrivateData->Attributes->hasChildNode(atn)) {
    NNode *todel = &(PrivateData->Attributes->getChildNode(atn));
    todel->detachNode();
    delete todel;
  }

  NNode* newattr = new NNode();
  newattr->setName(atn);
  newattr->setData(atv);
  PrivateData->Attributes->addChildNode(newattr);
}

const std::string& NNode::getName() const {
  return PrivateData->Name;
}

void NNode::setName(const std::string& newn) {
  if (!PrivateData->AttributeContainer) {
    PrivateData->Name = newn;
    normaliseString(PrivateData->Name);
    if (hasParentNode()) {
      NNode &lparent = getParentNode();
      detachNode();
      lparent.addChildNode(this);
    }
  }
}

std::string NNode::getData() const {
  std::string tmpstr;
  for (unsigned i = 0; i < PrivateData->Data.size(); i++)
    tmpstr.push_back(PrivateData->Data[i]);
  return tmpstr;
}

void NNode::setData(const std::string& newdata) {
  if (!PrivateData->AttributeContainer) {
    data().clear();
    for (unsigned i = 0; i < newdata.length(); i++)
      data().push_back(newdata[i]);
  }
}

NNode::XMLDataStore& NNode::data() {
  return PrivateData->Data;
}

long unsigned int NNode::NodeSize() {
  return PrivateData->Children.size();
}

long unsigned int NNode::NodeChildrenNum(bool strict) {
  unsigned childnum = PrivateData->Children.size();
  if (!strict) {
    childnum++; // for attributes 'node'
  }
  return childnum;
}

SAbsTreeNodeList_t NNode::NodeChildren(bool strict, bool) {
  SAbsTreeNodeList_t tmplist;
  if (!strict) {
    if (PrivateData->Attributes == nullptr && (!PrivateData->AttributeNode)) {
      PrivateData->Attributes = new NNode();
      PrivateData->Attributes->setName(NNode::NNodePIMPL::AttributeNodeName);
      PrivateData->Attributes->PrivateData->AttributeContainer = true;
      PrivateData->Attributes->PrivateData->AttributeNode = true;
    }
    if (PrivateData->Attributes != nullptr)
      tmplist.push_back(PrivateData->Attributes);
  }
  for (ChildMap::iterator ch = PrivateData->Children.begin();
       ch != PrivateData->Children.end(); ch++)
    if (! ch->second->PrivateData->SpecialNode)
      tmplist.push_back(ch->second);
  return tmplist;
}

bool NNode::NodePathExists(const std::string& pathstr) {
  SURI luri(pathstr);
  if (luri.depth() > 0) {
    if (hasChildNode(luri.getComponent(0))) {
      if ( luri.depth() == 1 )
        return true;
      else {
        std::string childnode = luri.getComponent(0);
        luri.deleteComponent(0);
        return getChildNode(childnode).NodePathExists(luri.getURI());
      }
    }
    else
      return false;
  }
  else
    return true;
}

SAbsTreeNode& NNode::NodeByPath(const std::string& pathstr) {
  SURI luri(pathstr);
  if (luri.depth() > 0) {
    std::string tmplookup = luri.getComponent(0);
    if (luri.depth() == 1)
      return getChildNode(tmplookup);
    else {
      luri.deleteComponent(0);
      return getChildNode(tmplookup).NodeByPath(luri.getURI());
    }
  }
  else
    return *this;
}

SAbsTreeNode& NNode::NodeParent() {
  return getParentNode();
}

bool NNode::hasNodeParent() {
  return hasParentNode();
}

bool NNode::NodeError() {
  return !good();
}

std::string NNode::NodeID() {
  return NodeName();
}

void NNode::NodeID(const std::string& newid) {
  setName(newid);
}

std::string NNode::NodeName() {
  return getName();
}

void NNode::NodeName(const std::string& newname) {
  setName(newname);
}

std::string NNode::NodeType() {
  return "String";
}

std::string NNode::NodeValue() {
  return getData();
}

void NNode::NodeValue(const std::string& newdata) {
  setData(newdata);
}

SAbsTreeNode& NNode::NewChild() {
  NNode *newchild = new NNode();
  newchild->setName("NewNode");
  newchild->setData("EmptyData");
  addChildNode(newchild);
  return *newchild;
}

std::map< std::string, std::string > NNode::getAttributes() {
  std::map< std::string, std::string > attrs;
  if (PrivateData->Attributes != nullptr) {
    ChildMap &attrchildnodes = PrivateData->Attributes->PrivateData->Children;
    for (ChildMap::iterator it = attrchildnodes.begin();
         it != attrchildnodes.end(); it++) {
      attrs [it->first] = it->second->getData();
    }
  }
  return attrs;
}

std::string& NNode::normaliseString(std::string& targetstr) {
  for (unsigned i=0; i<targetstr.length(); i++) {
    if (NNode::NNodePIMPL::isWhiteSpace(targetstr[i]))
      targetstr[i] = '_';
  }
  return targetstr;
}

bool NNode::isSpecial() {
  return PrivateData->SpecialNode;
}

/*****************Parser State/Action Transition Table************************
  *
  * (OK, this parser will cheat a little by only using a partial push-down
  * automaton)
  *
  * State\Symbol |   OPEN_TAG   |   CLOSE_TAG   |   END_CHAR      |   ASSIGN_CHAR   |   STR_DELIM   |   WH_SPACE   |   XMLSTRING   |    EOF    |  QST_TKN  |
  * -------------|------------------------------------------------------------------------------------------------------------------------------------------
  * (start state)|  INIT_NODE   |    ERROR_A    |   ERROR_A       |   ERROR_A       |   ERROR_A     |   NULL_A     |   ERROR_A     |  END_FLE  |  ERROR_A  |
  * NODE_RDY     |  NODE_DECL   |               |                 |                 |               |   NODE_RDY   |               |  NODE_END |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |    ERROR_A    |  NULL_A         |   ERROR_A       |   ERROR_A     |   NULL_A     |  ADD_NAME     |  ERROR_A  | MKSP_NODE |
  * NODE_DCL*    |              |               |  NODE_CLS       |                 |               |   NODE_DCL   |  NODE_ATT     |           | NODE_DCL  |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |  CLOSE_NODE   |  ERROR_A        |   ERROR_A       |   ERROR_A     |   ERROR_A    |  ADD_NAME     |  ERROR_A  |  ERROR_A  |
  * NODE_CLS*    |              | NODE_(RDY|CNT)|                 |                 |               |              |  NODE_CLS     |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |   ADD_NODE    |  NULL_A         |   ERROR_A       |   NULL_A      |   NULL_A     |  ADD_ATNM     |  ERROR_A  | MKSP_NODE |
  * NODE_ATT*    |              |   NODE_CNT    |  NODE_CLI       |                 |   NODE_ATT    |   NODE_ATT   |  ATT_NME      |           | NODE_ATT  |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    | CLOSE_INLINE  |  ERROR_A        |   ERROR_A       |   ERROR_A     |   ERROR_A    |  NULL_A       |  ERROR_A  |  ERROR_A  |
  * NODE_CLI     |              | NODE_(RDY|CNT)|                 |                 |               |              |  NODE_CLI     |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   INIT_NODE  |   NULL_A      |   ERROR_A       |   ERROR_A       |   ERROR_A     |   ADD_DATA   |  ADD_DATA     |  ERROR_A  |           |
  * NODE_CNT     |   NODE_DCL   |   NODE_CNT    |                 |                 |               |   NODE_CNT   |  NODE_CNT     |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |   ADD_NODE    |   ERROR_A       |   NULL_A        |   ERROR_A     |   ERROR_A    |   ERROR_A     |  ERROR_A  |  ERROR_A  |
  * ATTR_NME     |              |   NODE_CNT    |                 |   ATTR_VAL      |               |              |               |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |   ERROR_A     |   ERROR_A       |   ERROR_A       |  ADD_ATDT     |   ADD_ATDT   |  ADD_ATDT     |  ERROR_A  |           |
  * ATTR_VAL     |              |               |                 |                 |  ATTR_DAT     |   NODE_ATT   |  NODE_ATT     |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *              |   ERROR_A    |   ERROR_A     |   ERROR_A       |   ERROR_A       |  NULL_A       |   ADD_ATDT   |  ADD_ATDT     |  ERROR_A  |           |
  * ATTR_DAT     |              |               |                 |                 |  NODE_ATT     |   ATTR_DAT   |  ATTR_DAT     |           |           |
  * --------------------------------------------------------------------------------------------------------------------------------------------------------
  *
  *
  ****************************Parser Actions***********************************
  *
  * ADD_NAME:
  *   Add the name of the working node, using the current token.
  *
  * ADD_ATNM:
  *   Add the name of working attribute, using the current token.
  *
  * ADD_ATDT:
  *   Add the data value of the working attribute, using the current token.
  *
  * ADD_NODE:
  *   Add the working node to parent's children, push (pntr) onto the stack.
  *
  * ADD_DATA:
  *   Add the data to the working node, using the current token.
  *
  * CLOSE_NODE:
  *   Close the working node (semantic check of name/token) and pop stack.
  *
  * CLOSE_INLINE:
  *   Close the working node, but no stack pop necessary.
  *
  * ERROR_A:
  *   Report an error (triggered by the transition table above)
  *
  * NULL_A:
  *   Perform no action, change no state.
  *
  * END_FLE:
  *   Perform necessary EOF actions/clean up.
  *
  *****************************************************************************
  */
/* Allocate Statics */
char* NNode::NNodePIMPL::NULLNAME;
char* NNode::NNodePIMPL::XMLParseStates_str[NNode::NNodePIMPL::ParseStates];
char* NNode::NNodePIMPL::XMLTokens_str     [NNode::NNodePIMPL::ParseTokens];
int   NNode::NNodePIMPL::actiontable[NNode::NNodePIMPL::ParseStates][NNode::NNodePIMPL::ParseTokens];
int   NNode::NNodePIMPL::gototable  [NNode::NNodePIMPL::ParseStates][NNode::NNodePIMPL::ParseTokens];
bool  NNode::NNodePIMPL::InitStatics = false;
/* --------------- */
void NNode::NNodePIMPL::init_tables(){
  if (NNode::NNodePIMPL::InitStatics) return;
  DEBUG_OUT("Initilialising statics.");
  NNode::NNodePIMPL::NULLNAME  = (char*)"Empty";
  for (unsigned i1=0;i1<NNode::NNodePIMPL::ParseStates;i1++)
    for (unsigned i2=0;i2<NNode::NNodePIMPL::ParseTokens;i2++){
      NNode::NNodePIMPL::actiontable[i1][i2] = ERROR_A;
      NNode::NNodePIMPL::gototable  [i1][i2] = NODE_ERR;
    }
  //NODE_RDY
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_RDY] = (char*)"NODE_RDY";
  NNode::NNodePIMPL::actiontable[NODE_RDY][OPN_TAG] = INIT_NODE;
  NNode::NNodePIMPL::gototable  [NODE_RDY][OPN_TAG] = NODE_DCL;
  NNode::NNodePIMPL::actiontable[NODE_RDY][WHT_SPC] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_RDY][WHT_SPC] = NODE_RDY;
  NNode::NNodePIMPL::actiontable[NODE_RDY][EOF_MKR] = END_FLE;
  NNode::NNodePIMPL::gototable  [NODE_RDY][EOF_MKR] = NODE_END;
  //NODE_DCL
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_DCL] = (char*)"NODE_DCL";
  NNode::NNodePIMPL::actiontable[NODE_DCL][END_CHR] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_DCL][END_CHR] = NODE_CLS;
  NNode::NNodePIMPL::actiontable[NODE_DCL][XML_STR] = ADD_NAME;
  NNode::NNodePIMPL::gototable  [NODE_DCL][XML_STR] = NODE_ATT;
  NNode::NNodePIMPL::actiontable[NODE_DCL][WHT_SPC] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_DCL][WHT_SPC] = NODE_DCL;
  NNode::NNodePIMPL::actiontable[NODE_DCL][SPC_TKN] = MKSP_NODE;
  NNode::NNodePIMPL::gototable  [NODE_DCL][SPC_TKN] = NODE_DCL;
  
  //NODE_CLS
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_CLS] = (char*)"NODE_CLS";
  NNode::NNodePIMPL::actiontable[NODE_CLS][CLS_TAG] = CLOSE_NODE;
  NNode::NNodePIMPL::gototable  [NODE_CLS][CLS_TAG] = NODE_RDY;
  NNode::NNodePIMPL::actiontable[NODE_CLS][XML_STR] = ADD_NAME;
  NNode::NNodePIMPL::gototable  [NODE_CLS][XML_STR] = NODE_CLS;
  //NODE_CLS
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_CLI] = (char*)"NODE_CLI";
  NNode::NNodePIMPL::actiontable[NODE_CLI][CLS_TAG] = CLOSE_INLINE;
  NNode::NNodePIMPL::gototable  [NODE_CLI][CLS_TAG] = NODE_RDY;
  NNode::NNodePIMPL::actiontable[NODE_CLI][XML_STR] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_CLI][XML_STR] = NODE_CLI;
  //NODE_ATT
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_ATT] = (char*)"NODE_ATT";
  NNode::NNodePIMPL::actiontable[NODE_ATT][CLS_TAG] = ADD_NODE;  // incr. depth
  NNode::NNodePIMPL::gototable  [NODE_ATT][CLS_TAG] = NODE_CNT;
  NNode::NNodePIMPL::actiontable[NODE_ATT][STR_DLM] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_ATT][STR_DLM] = NODE_ATT;
  NNode::NNodePIMPL::actiontable[NODE_ATT][XML_STR] = ADD_ATNM;
  NNode::NNodePIMPL::gototable  [NODE_ATT][XML_STR] = ATR_NME;
  NNode::NNodePIMPL::actiontable[NODE_ATT][WHT_SPC] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_ATT][WHT_SPC] = NODE_ATT;
  NNode::NNodePIMPL::actiontable[NODE_ATT][END_CHR] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_ATT][END_CHR] = NODE_CLI;
  NNode::NNodePIMPL::actiontable[NODE_ATT][SPC_TKN] = MKSP_NODE;
  NNode::NNodePIMPL::gototable  [NODE_ATT][SPC_TKN] = NODE_ATT;
  //NODE_CNT
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_CNT] = (char*)"NODE_CNT";
  NNode::NNodePIMPL::actiontable[NODE_CNT][OPN_TAG] = INIT_NODE;
  NNode::NNodePIMPL::gototable  [NODE_CNT][OPN_TAG] = NODE_DCL;
  NNode::NNodePIMPL::actiontable[NODE_CNT][CLS_TAG] = NULL_A;
  NNode::NNodePIMPL::gototable  [NODE_CNT][CLS_TAG] = NODE_CNT;
  NNode::NNodePIMPL::actiontable[NODE_CNT][WHT_SPC] = ADD_DATA;
  NNode::NNodePIMPL::gototable  [NODE_CNT][WHT_SPC] = NODE_CNT;
  NNode::NNodePIMPL::actiontable[NODE_CNT][XML_STR] = ADD_DATA;
  NNode::NNodePIMPL::gototable  [NODE_CNT][XML_STR] = NODE_CNT;
  //ATTR_NME
  NNode::NNodePIMPL::XMLParseStates_str  [ATR_NME]  = (char*)"ATR_NME";
  NNode::NNodePIMPL::actiontable[ATR_NME][ASN_CHR]  = NULL_A;
  NNode::NNodePIMPL::gototable  [ATR_NME][ASN_CHR]  = ATR_VAL;
  NNode::NNodePIMPL::actiontable[ATR_NME][CLS_TAG]  = ADD_NODE;
  NNode::NNodePIMPL::gototable  [ATR_NME][CLS_TAG]  = NODE_CNT;
  //ATTR_VAL
  NNode::NNodePIMPL::XMLParseStates_str  [ATR_VAL]  = (char*)"ATR_VAL";
  NNode::NNodePIMPL::actiontable[ATR_VAL][STR_DLM]  = ADD_ATDT;
  NNode::NNodePIMPL::gototable  [ATR_VAL][STR_DLM]  = ATR_DAT;
  NNode::NNodePIMPL::actiontable[ATR_VAL][WHT_SPC]  = ADD_ATDT;
  NNode::NNodePIMPL::gototable  [ATR_VAL][WHT_SPC]  = NODE_ATT;
  NNode::NNodePIMPL::actiontable[ATR_VAL][XML_STR]  = ADD_ATDT;
  NNode::NNodePIMPL::gototable  [ATR_VAL][XML_STR]  = NODE_ATT;
  //ATTR_DAT
  NNode::NNodePIMPL::XMLParseStates_str  [ATR_DAT]  = (char*)"ATR_DAT";
  NNode::NNodePIMPL::actiontable[ATR_DAT][STR_DLM]  = NULL_A;
  NNode::NNodePIMPL::gototable  [ATR_DAT][STR_DLM]  = NODE_ATT;
  NNode::NNodePIMPL::actiontable[ATR_DAT][WHT_SPC]  = ADD_ATDT;
  NNode::NNodePIMPL::gototable  [ATR_DAT][WHT_SPC]  = NODE_ATT;
  NNode::NNodePIMPL::actiontable[ATR_DAT][XML_STR]  = ADD_ATDT;
  NNode::NNodePIMPL::gototable  [ATR_DAT][XML_STR]  = NODE_ATT;

  //final string names
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_END]  = (char*)"NODE_END";
  NNode::NNodePIMPL::XMLParseStates_str  [NODE_ERR]  = (char*)"NODE_ERR";
  //init friendly names for tokens
  for (unsigned i=0; i< ParseTokens; i++)
    NNode::NNodePIMPL::XMLTokens_str[i] = (char*)"UnknownToken";
  NNode::NNodePIMPL::XMLTokens_str[XML_STR] = (char*)"String";
  NNode::NNodePIMPL::XMLTokens_str[OPN_TAG] = (char*)"OpenTag";
  NNode::NNodePIMPL::XMLTokens_str[CLS_TAG] = (char*)"CloseTag";
  NNode::NNodePIMPL::XMLTokens_str[ESC_CHR] = (char*)"EscapeChar";
  NNode::NNodePIMPL::XMLTokens_str[END_CHR] = (char*)"EndChar";
  NNode::NNodePIMPL::XMLTokens_str[ASN_CHR] = (char*)"AssignChar";
  NNode::NNodePIMPL::XMLTokens_str[STR_DLM] = (char*)"StringDelim";
  NNode::NNodePIMPL::XMLTokens_str[WHT_SPC] = (char*)"WhiteSpace";
  NNode::NNodePIMPL::XMLTokens_str[EOF_MKR] = (char*)"EOF";
  NNode::NNodePIMPL::XMLTokens_str[NLL_TKN] = (char*)"Null Token";
  NNode::NNodePIMPL::XMLTokens_str[SPC_TKN] = (char*)"QST Token";
  NNode::NNodePIMPL::InitStatics = true;
}

/* The main parser method.
  * Recursively add NNodes to root node while reading XML file.
  */
bool NNode::NNodePIMPL::readXMLTree(NNode *curr,std::iostream& xmlsource,
                                    XMLParseRes& result){
  NNode*              currnode = nullptr; // store a working node
  int                 current_state;   // store current parsing state
  int                 next_state;      // store the next state
  std::vector<NNode*> nnode_stack;     // stack of pointers to NNodes
  XMLToken            xmltoken;        // store a fetched token
  std::string         attribn,attribv; // store attribute names/values
  bool                stopparsing;     // to be set when parsing should halt
  bool                parsingerr;      // error indicator
  bool                whtspcsen;
  bool                tkngobble;
  //initialise state and node stack
  current_state = NODE_RDY;
  next_state    = NODE_ERR;
  stopparsing   = false;
  parsingerr    = false;
  nnode_stack.push_back(curr);
#ifdef DEBUG
        std::cerr << "-----"<< std::endl;
#endif
  // continuously read tokens; break internally under different circumstances
  while(true){
#ifdef DEBUG
    std::cerr << "NNode::Parser: STATE=" << XMLParseStates_str[current_state]
              << std::endl;
#endif
    // set white space sensitivity
    if(current_state == NODE_DCL || current_state == NODE_ATT)
      whtspcsen = true;
    else
      whtspcsen = false;
    // set gobble mode
    if(current_state == ATR_VAL || current_state == NODE_CNT ||
       current_state == ATR_DAT)
      tkngobble = true;
    else
      tkngobble = false;
    getXMLToken(xmlsource,xmltoken,whtspcsen,tkngobble);
    next_state = gototable[current_state][xmltoken.Token];
#ifdef DEBUG
    std::cerr << "NNode::Parser: TOKEN=" << XMLTokens_str[xmltoken.Token]
            << "(" << xmltoken.Line + 1 << "," << xmltoken.Column
            << ")" << std::endl;
#endif
    // fetch an action and switch on it
    switch(actiontable[current_state][xmltoken.Token]){
      // initialise a memory slot for this new node decl
      case INIT_NODE:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "INIT node" << std::endl;
#endif
        if(currnode != nullptr) {delete currnode;}
        currnode = new NNode();
        break;
      // node definition complete, add to stack
      case ADD_NODE:
        if (currnode->PrivateData->SpecialNode)
          goto CLOSE_INLINE_M;
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "Adding node (" << currnode->NodeName() << ")" << std::endl;
#endif
        if(currnode == nullptr) throw NNodeException();
        nnode_stack.back()->addChildNode(currnode);
        nnode_stack.push_back(currnode);
        currnode = nullptr;
        break;
      // new node, add the name
      case ADD_NAME:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "Adding name (" << &xmltoken.Data[0] << ")" << std::endl;
#endif
        currnode->PrivateData->Name = &xmltoken.Data[0];
        break;
      // add attribute name
      case ADD_ATNM:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "Adding attribute name (" << &xmltoken.Data[0] << ")" << std::endl;
#endif
        attribn = &xmltoken.Data[0];
        attribv.clear();
        currnode->setAttribute(attribn,attribv);
        break;
      // attribute data, and add the attribute to the node
      case ADD_ATDT:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "Adding attribute data (" << &xmltoken.Data[0] << ")" << std::endl;
#endif
        attribv = &xmltoken.Data[0];
        if(currnode == nullptr) throw NNodeException();
        currnode->setAttribute(attribn,attribv);
        break;
      case ADD_DATA:
        /* Alternatives:
          * 1) Use only first data token
          * 2) Use only most recent data token
          * 3) Accumulate all data
          * Using 3 here, for now. However, this includes crappy formatting.
          * Accumulate data (since it can appear be interspersed with child
          * node defs, and hence appear more than once in tokenised form).
          */
        nnode_stack.back()->PrivateData->Data.insert
                                   (nnode_stack.back()->PrivateData->Data.end(),
                                        xmltoken.Data.begin(),
                                        xmltoken.Data.end());
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: "
        << "Adding data (" << &xmltoken.Data[0] << ")" << std::endl;
#endif
        break;
      //entry point for an inline closure (node not on stack)
      case CLOSE_INLINE:
        CLOSE_INLINE_M:
        nnode_stack.back()->addChildNode(currnode);
        nnode_stack.push_back(currnode);
        currnode = nullptr;
        __FALLTHROUGH__
      // a node definition is complete
      case CLOSE_NODE:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: Closing Node." << std::endl;
#endif
        // check that names match, if not an in-line close
        if (currnode != nullptr) {
          if(currnode->PrivateData->Name !=
                                         nnode_stack.back()->PrivateData->Name){
            result.Messages += "Node closure string does not match open node. ";
            goto ERROR_A;
          }
          // this node is no longer required
          delete currnode;
          currnode = nullptr;
        }
        nnode_stack.pop_back();
        // and now be super-naughty and change the next state, depending
        // on the height of the stack. It's bad, but saves complexity in the
        // tables and actions. Will probably change this later. MAH.
        if ( nnode_stack.size() > 1 )
          next_state = NODE_CNT;
        else
          next_state = NODE_RDY;
        break;
      case END_FLE:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: End of File" << std::endl;
#endif
        stopparsing = true;
        break;
      case NULL_A:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: Null" << std::endl;
#endif
        break;
      case MKSP_NODE:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: Special Node" << std::endl;
#endif
        currnode->PrivateData->SpecialNode = true;
        break;
      // an error occurred, unhandled action (usually an unexpected token)
      case ERROR_A:
            ERROR_A:
#ifdef DEBUG
        std::cerr << "NNode::Parser::Action: Error action!"<< std::endl;
#endif
        this->Good  = false;
        parsingerr  = true;
        stopparsing = true;
        result.Messages += "Unexpected token (";
        result.Messages += XMLTokens_str[xmltoken.Token];
        result.Messages += "): syntax error. ";
        break;
      default:
        std::cerr << "NNODE::Parser: Something went horribly wrong."
                  << std::endl;
        throw NNodeException();
        break;
    }
#ifdef DEBUG
      std::cerr << "NNode::Parser: GOTO="
                << XMLParseStates_str[gototable[current_state][xmltoken.Token]]
                << std::endl;
      std::cerr << "-----"<< std::endl;
#endif
    //select the next state (but only there was no error)
    if (!parsingerr)
      current_state = next_state;
    result.State  = current_state;
    result.Line   = xmltoken.Line;
    result.Column = xmltoken.Column;
    result.Token  = xmltoken.Token;
    if(stopparsing) break;
  }
  //clean up
  if(currnode != nullptr) delete currnode;
  if(parsingerr) return false; else return true;
}

// Initialise escape chars

const int NNode::NNodePIMPL::XMLESCCharsNum = 4;

NNode::XMLCharType NNode::NNodePIMPL::XMLESCChars[] = 
{        '<',        '>',         '&',          '"'};

char* NNode::NNodePIMPL::XMLESCCharsSeq[] = 

{(char*)"lt",(char*)"gt",(char*)"amp",(char*)"quot"};

// simple method to revert escaped character sequences
// -> should be called with the first character in stream AFTER '&'
NNode::XMLCharType NNode::NNodePIMPL::escapedToChar(std::iostream& xmlsource) {
  static const unsigned MAXLEN = 10;
  std::string           escseq;
  XMLCharType           retchar = ' ';
  // get the sequence, if possible (up to maxlen, or end of stream)
  while (xmlsource.good() && (escseq.size() < MAXLEN) ) {
    char nextchar;
    xmlsource.get(nextchar);
    if ( nextchar == ENDESC_CHAR )
      break;
    escseq.push_back(nextchar);
  }
  // now escseq should contain the escape sequence to be decoded
  // (without the leading '&' or the trailing ';'
  for (int i = 0; i < XMLESCCharsNum; i++) {
    if ( escseq == std::string(XMLESCCharsSeq[i]) )
      retchar = XMLESCChars[i];
  }
  return retchar;
}

// simple method to convert a special character to its escape sequence
// (returns true if escape sequence written)
bool NNode::NNodePIMPL::writeEscapeSequence(NNode::XMLCharType xc,
                                            std::iostream& xmltarget) {
  bool retval = false;
  for (int i = 0; i < XMLESCCharsNum; i++) {
    if ( xc == XMLESCChars[i] ) {
      std::string writesc = std::string("&") + std::string(XMLESCCharsSeq[i])
                          + std::string(";");
      xmltarget.write(writesc.c_str(),writesc.size());
      retval = true;
      break;
    }
  }
  return retval;
}

//               **********The Lexer**************
// read *one* token from the input file and initialise the target token
bool NNode::NNodePIMPL::getXMLToken(std::iostream& xmlsource,
                                    XMLToken& targettoken, bool whitespacesens,
                                    bool gobble){
  /* This method would need to be changed in the future for unicode.
   */
  XMLCharType        &readdata=targettoken.tempdata;
  bool               &donotread=targettoken.donotread;
  unsigned           &line=targettoken.Line;
  unsigned           &column=targettoken.Column;
  bool               endflag = false;
  bool               retstatus = false;
  //reset target token first
  targettoken.Token = NLL_TKN;
  targettoken.Data.clear();
  targettoken.Line = line; targettoken.Column = column;

  // read input characters until first token
  while ((xmlsource.good() || donotread) && !endflag) {
    // this keeps the last read char, if it ended a string (below)
    if (donotread) donotread = false;
    else{
      // read the next character from the source
      xmlsource.read(&readdata,sizeof(readdata));
      if (xmlsource.eof()) break; // last char would be caught here
      column++;
      // add it to the back of the buffer
      if (LexerBuffer.size() > 3)
        LexerBuffer.erase(LexerBuffer.begin());
      LexerBuffer.push_back(readdata);
      // is this a comment start?
      if (  LexerBuffer.size() == 4 &&
           (std::string(&LexerBuffer[0],4) == std::string("<!--")) ) {
        LexerComment++;
      }
      // is this a comment end?
      if (  LexerBuffer.size() == 4 &&
           (std::string(&LexerBuffer[1],3) == std::string("-->")) ) {
        if (LexerComment > 0)
          LexerComment--;
      }
      // ignore text in a comment
      if (LexerComment > 0) {
        readdata = ' ';
      }
    }
    //initialise the target token, and break
    switch (readdata) {
      case ESCAPE_CHAR:
        targettoken.Data.push_back(escapedToChar(xmlsource));
        break;
      case OPEN_TAG:
      case CLOSE_TAG:
      case STR_DELIM:
        if (!targettoken.Data.size())
          targettoken.Token = chartotoken(readdata);
        else{
          // string, keep the char and return string token
          donotread = true;
          targettoken.Token = XML_STR;
        }
        endflag = true;
        break;
      case END_CHAR:
      case ASSIGN_CHAR:
      case QST_CHAR:
      case EXC_CHAR:
        // these characters should be treated as text if we are in gobble mode
        if (!gobble) {
          if (!targettoken.Data.size())
            targettoken.Token = chartotoken(readdata);
          else{
            // string, keep the char and return string token
            donotread = true;
            targettoken.Token = XML_STR;
          }
          endflag = true;
          break;
        }
        __FALLTHROUGH__
      default:
        // should modify this for windows (possibly)
        if (readdata == '\n') {
          line++;
          column = 0;
        }
        // if this call is sensitive on white space
        if (whitespacesens && isWhiteSpace(readdata)) {
            if (targettoken.Data.size()){ //it should mark the end of a string
              donotread = true;
              endflag   = true;
              break;
            }
            else{
              targettoken.Token = WHT_SPC;
              break;
            }
        }
        // should be considered a string -- add to string buffer
        targettoken.Data.push_back(readdata);
    } // end of switch
  } // end of while
  if (targettoken.Token != NLL_TKN) {
    retstatus = true;
  }
  else { // no token was assigned
    // first, catch a string at the end of a file
    if (targettoken.Data.size()){
      if (targettoken.Data.size()>0 &&
          isStrWhiteSpace(&targettoken.Data[0],targettoken.Data.size()))
        targettoken.Token = WHT_SPC;
      else
        targettoken.Token = XML_STR;
      retstatus = true;
    }
    else{ //otherwise, EOF or weird error
      targettoken.Token = EOF_MKR;
      retstatus = false;
    }
  }
  // flag a string as whitespace
  if(targettoken.Token == XML_STR &&
     targettoken.Data.size()>0 &&
     isStrWhiteSpace(&targettoken.Data[0],targettoken.Data.size())) {
    targettoken.Token = WHT_SPC;
  }
  // add a string null terminator
  targettoken.Data.push_back('\0');
  return retstatus;
}

/* writeXMLTree - The main XML writer
  * Writes an XML tree to a stream, from the current node and children.
  * No indentation happens here.
  */
bool NNode::NNodePIMPL::writeXMLTree(std::iostream& xmltarget,unsigned bound,
                         unsigned depth){
  if (bound > 0 && depth > bound) return false;
  if (depth == 0)
    xmltarget << "<?xml version=\"" << XMLVersion << "\"?>" << std::endl;
  // write out this node's preamble
  xmltarget << tokentochar(OPN_TAG) << normaliseString(this->Name);
  // write out all of the attributes of this node
  if (Attributes != nullptr)
    for(ChildMap::iterator a = Attributes->PrivateData->Children.begin();
        a!=Attributes->PrivateData->Children.end();a++)
      xmltarget << tokentochar(WHT_SPC) << a->first << "="
                << tokentochar(STR_DLM) << a->second->getData()
                << tokentochar(STR_DLM);
  // write an in-line node closure if there are no children or data
  if (this->Children.size() == 0 && this->Data.size() == 0)
    xmltarget << tokentochar(WHT_SPC) << tokentochar(END_CHR)
              << tokentochar(CLS_TAG);
  else {
    xmltarget << tokentochar(CLS_TAG);
    // write out data and children
    if ( this->Data.size()>0 &&
        (!isStrWhiteSpace(&this->Data[0],this->Data.size()))) {
      //only get here if data is not whitespace
      for(unsigned i=0;i<this->Data.size();i++)
        if (Data[i] != '\0') {
          // escape the character if necessary
          if (! writeEscapeSequence (Data[i], xmltarget) )
            xmltarget << Data[i];
        }
    }
    for(ChildMap::iterator c = Children.begin();c!=Children.end();c++)
      if (! c->second->PrivateData->SpecialNode)
        c->second->PrivateData->writeXMLTree(xmltarget,bound,depth+1);
    // close the node
    xmltarget << tokentochar(OPN_TAG) << tokentochar(END_CHR) << this->Name
              << tokentochar(CLS_TAG);
  }
  return true;
}

// check if a character is a whitespace character
inline bool NNode::NNodePIMPL::isWhiteSpace(XMLCharType achar){
  const unsigned whtspcesnum                = 5;
  const char    whitespaces[whtspcesnum]    = {' ', '\t', '\n', 13, '\0'};
  for (unsigned i=0; i<whtspcesnum;i++)
    if (achar==whitespaces[i])
      return true;
  return false;
}
// extend the method above to a string
bool NNode::NNodePIMPL::isStrWhiteSpace(XMLCharType* thestr, unsigned thelen){
  for (unsigned i=0;i<thelen;i++){
    if (!isWhiteSpace(thestr[i]))
      return false;
  }
  return true;
}

// translate a character to a token value
inline int NNode::NNodePIMPL::chartotoken(XMLCharType xmlchar){
  switch (xmlchar){
    case OPEN_TAG:
      return OPN_TAG;
    case CLOSE_TAG:
      return CLS_TAG;
    case END_CHAR:
      return END_CHR;
    case ASSIGN_CHAR:
      return ASN_CHR;
    case STR_DELIM:
      return STR_DLM;
    case ESCAPE_CHAR:
      return ESC_CHR;
    case EOF_CHAR:
      return EOF_MKR;
    case QST_CHAR:
    case EXC_CHAR:
      return SPC_TKN;
    default:
      return XML_STR;
  }
}

// translate a token into an xml character
inline NNode::XMLCharType NNode::NNodePIMPL::tokentochar(int atoken){
  switch (atoken){
    case OPN_TAG:
      return OPEN_TAG;
    case CLS_TAG:
      return CLOSE_TAG;
    case ESC_CHR:
      return ESCAPE_CHAR;
    case END_CHR:
      return END_CHAR;
    case ASN_CHR:
      return ASSIGN_CHAR;
    case STR_DLM:
      return STR_DELIM;
    case EOF_MKR:
      return EOF_CHAR;
    case WHT_SPC:
    default:
      return ' ';
  }
}

/*******************************************************************************
 *
 *                                 NNodeResource
 * 
 ******************************************************************************/

class NNodeResource::NNodeResourcePIMPL {
public:
  NNode       RootNode;
  std::string Location;
  bool        Valid;
};

NNodeResource::NNodeResource(): PrivateData(new NNodeResourcePIMPL()) {
  clear();
}

NNodeResource::~NNodeResource() {
  delete PrivateData;
}

std::string NNodeResource::getInfo(const std::string&) {
  return "XML Tree";
}

const std::string& NNodeResource::getLocation() const {
  return PrivateData->Location;
}

const std::string NNodeResource::getLocationPathID(const std::string&) {
  return PrivateData->Location;
}

SAbsTreeNode& NNodeResource::getRoot() {
  PrivateData->RootNode.setName("XML Resource");
  PrivateData->RootNode.setData(getLocation());
  return PrivateData->RootNode;
}

NNode& NNodeResource::getRootNode() {
  return static_cast<NNode&>(getRoot());
}

bool NNodeResource::hasArchive() {
  return 0;
}

bool NNodeResource::hasSSpace(const std::string&) {
  return 0;
}

bool NNodeResource::isValid() const {
  return PrivateData->Valid;
}

void NNodeResource::refresh(SConnectable::sdepth_t) {
  if (PrivateData->Location.length() > 0)
    PrivateData->RootNode.loadFromXMLFile(PrivateData->Location);
  PrivateData->Valid = PrivateData->RootNode.good();
}

void NNodeResource::store() {
  if (PrivateData->Location.length() > 0) {
    bool append = false;
    for (NNode::ChildMap::iterator
         child=PrivateData->RootNode.getChildNodes().begin();
         child != PrivateData->RootNode.getChildNodes().end();
         child++) {
      if (!child->second->isSpecial()) {
        child->second->storeToXMLFile(getLocation(),append);
        append = true;
      }
    }
  }
  PrivateData->Valid = PrivateData->RootNode.good();
}

int NNodeResource::setLocation(const std::string& newloc) {
  PrivateData->Location = newloc;
  std::ifstream formatchk;
  const unsigned magiclen = 50;
  std::string xmlstr;
  xmlstr.resize(magiclen);
  for (unsigned i=0; i<magiclen; i++)
    xmlstr[i] = '\0';
  formatchk.open(getLocation().c_str(), std::ios::in);
  formatchk.read(&xmlstr[0],magiclen);
  // first 5 chars in a 'compliant' xml file should be '<?xml'
  if (xmlstr.find("<?xml") != std::string::npos)
    PrivateData->Valid = true;
  else {
    SFile testloc(PrivateData->Location);
    // allow anything ending in XML
    if ( testloc.exists() && (newloc.length() > 4) &&
         (std::string(&newloc[newloc.length()-4]) == ".xml") )
      PrivateData->Valid = true;
    else
      PrivateData->Valid = false;
  }
  //initialise root node
  getRoot();
  return isValid();
}

void NNodeResource::clear() {
  PrivateData->Location.clear();
  PrivateData->RootNode.clear();
  PrivateData->Valid = false;
}
