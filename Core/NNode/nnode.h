/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::NTree:
 * A simple and fast class for modelling tree structures and reading/writing
 * from/to disk. Principally designed for XML.
 * Part of Simulacrum, but can be used as a standalone library.
 * Designed to be very lightweight, and allowing library freedom.
 *
 * Author: M.A.Hicks
 */

#ifndef SIMULACRUM_NTREE
#define SIMULACRUM_NTREE

#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <fstream>

#include <Core/sresource.h>
#include <Core/sabtree.h>
#include <Core/definitions.h>

namespace Simulacrum {

  class SIMU_API NNode : public SAbsTreeNode {
  public:
    /* Container class typedefs */
    typedef std::unordered_multimap<std::string, NNode*> ChildMap;
    typedef             char XMLCharType;
    typedef std::vector<XMLCharType> XMLDataStore;
    // used to analyse the last parser state
    struct XMLParseRes {
      int State, Token, Line, Column;
      std::string Messages;
      char *State_str, *Token_str;
    };
                  NNode();
                  NNode(NNode&);
                  NNode(const NNode&);
    virtual      ~NNode();
    void          clear() override;
    void          detachNode() override;
    bool          removeNode() override;
    bool          good() const;
    XMLParseRes   loadFromXMLFile(const std::string& xmlfile);
    XMLParseRes   loadFromXML(std::stringstream&);
    bool          storeToXMLFile(const std::string& xmlfile, bool append=false);
    bool          storeToXML(std::stringstream&);
    void          printTree(unsigned depth=0);
    unsigned      childNodeCount(const std::string&);
    bool          hasChildNode(const std::string&);
    NNode&        getChildNode(const std::string&);
    NNode&        xmlRootNode();
    ChildMap&     getChildNodes();
    bool          hasParentNode();
    NNode&        getParentNode();
    NNode&        addChildNode(NNode&);
    NNode&        addChildNode(NNode*);
    NNode&        operator=(NNode&);
    bool          hasAttribute(const std::string&) override;
    std::string   getAttribute(const std::string&) override;
    void          setAttribute(const std::string&,const std::string&) override;
    const
    std::string&  getName() const;
    void          setName(const std::string&);
    std::string   getData() const;
    void          setData(const std::string&);
    XMLDataStore& data();
    /* For ABTree compliance */
    std::string   NodeID() override;
    void          NodeID(const std::string&) override;
    std::string   NodeType() override;
    unsigned long NodeSize() override;
    std::string   NodeName() override;
    void          NodeName(const std::string&) override;
    std::string   NodeValue() override;
    void          NodeValue(const std::string&) override;
    unsigned long NodeChildrenNum(bool strict = false) override;
    SAbsTreeNodeList_t
                  NodeChildren(bool strict = false, bool simpleonly = false) override;
    bool          NodePathExists(const std::string&) override;
    SAbsTreeNode& NodeByPath(const std::string&) override;
    SAbsTreeNode& NodeParent() override;
    bool          hasNodeParent() override;
    bool          NodeError() override;
    SAbsTreeNode& NewChild() override;
    std::map<std::string,std::string>
                  getAttributes() override;
    static
    std::string&  normaliseString(std::string&);
    bool          isSpecial();
  private:
    class NNodePIMPL;
    NNodePIMPL *PrivateData;
  };

/*******************************************************************************
 *
 *                                 NNodeResource
 *
 ******************************************************************************/

  class SIMU_API NNodeResource : public SResource {
  private:
    class NNodeResourcePIMPL;
    NNodeResourcePIMPL *PrivateData;
  public:
                  NNodeResource();
    virtual      ~NNodeResource();
    /* for SResource compliance */
    const std::string&
                  getLocation() const;
    int           setLocation(const std::string&);
    bool          isValid() const;
    SAbsTreeNode& getRoot();
    NNode&        getRootNode();
    void          refresh(SConnectable::sdepth_t deep = false);
    void          store();
    std::string   getInfo(const std::string &path = "");
    bool          hasArchive();
    bool          hasSSpace(const std::string &path = "");
    const std::string
                  getLocationPathID(const std::string &path = "");
    void          clear();
  };

} //end namespace
#endif //SIMULACRUM_NTREE
