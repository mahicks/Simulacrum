/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for FileIO
 * Various individual file read/writers (PPM, DICOM ...)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_FILEIO
#define SIMULACRUM_FILEIO

#include "sspace.h"
#include "sprimitives.h"
#include "sresource.h"
#include "NNode/nnode.h"
#include "definitions.h"

#include <string>
#include <iostream>
#include <fstream>

namespace Simulacrum {

/* Simulation Standard IO Interface  */
class SIMU_API SIO : public SResource {
private:
  class SIOPIMPL;
  SIOPIMPL *PrivateData;
public:
                             SIO();
  virtual                   ~SIO();
  virtual int                setLocation(const std::string&)=0;
  virtual void               changeLocation(const std::string&)=0;
  virtual const SCoordinate& getExtent() =0;
  virtual bool               isValid() const =0;
  virtual const std::string& getLocation() const =0;
  virtual int                loadSSpace(SSpace&);
  virtual int                storeSSpace(SSpace&);
  /* Methods to fulfill the requires of SResource */
  virtual SAbsTreeNode&      getRoot();
  virtual void               refresh(SConnectable::sdepth_t deep = false);
  virtual void               store();
  virtual std::string        getInfo(const std::string &path = "");
  virtual bool               hasArchive();
  virtual bool               hasSSpace(const std::string &path = "");
  virtual void               getSSpaceInto(SSpace&,
                                             const std::string &path = "");
  virtual void               putSSpaceInto(SSpace&,
                                             const std::string &path = "");
  virtual const std::string  getLocationPathID(const std::string &path = "");
  virtual void               doStop(bool);
  virtual bool&              stop();
  /* Generic methods to read data stored in row/column format */
  static int                 readTabulatedData (SSpace&, std::istream&,
                                                SElem::Precision, bool isRGB,
                                                bool isSigned,
                                                bool isInterLvd = true,
                                                bool *dostop=nullptr);
  static int                 writeTabulatedData(SSpace&, std::ostream&,
                                                SElem::Precision, bool isRGB);
  static unsigned            maxvaltobytewidth (unsigned);
  struct imgStreamData {
    int x,y,channels,bpp;
    std::size_t size;
    char *data;
    bool valid;
  };
};

} //end namespace
#endif //SIMULACRUM_FILEIO
