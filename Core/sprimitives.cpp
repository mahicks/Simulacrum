/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Primitives:
 * The basic building blocks of the Simulacrum library, including pixel/voxel
 * containers, collections of pixel/voxels and their interfaces.
 * Author: M.A.Hicks
 */

#include "sprimitives.h"

#include<string>
#include <sstream>
#include <iostream>
#include <math.h>
#include <limits>

using namespace Simulacrum;

/*------------------------------------------------------------------------------
 *                             SElem Classes
 * ---------------------------------------------------------------------------*/

/* The Tight SElem storage class for pixel/voxel elements in all images.
   Uses a compressed data layout in one 64-bit word length value:

   Data:
   |63..56|55..48|47..40|39..32|31..24|23..16|15..08|07..00|
   ---------------------------------------------------------
   |Flags |   Segment   |  Intensity  |   R  |   G  |   B  |

   Flags:
   |  63  |  62  |  61  |  60  |  59  |  58  |  57  |  56  |
   |Valid |------------------------------------------------|

   The following are some macros to make accessing them easier:
*/

#define GET_BLUE(X)      ( X & 0x00000000000000FF)
#define GET_GREEN(X)     ((X & 0x000000000000FF00)>>8)
#define GET_RED(X)       ((X & 0x0000000000FF0000)>>16)
#define GET_INTENSITY(X) ((X & 0x000000FFFF000000)>>24)
#define GET_SEGMENT(X)   ((X & 0x00FFFF0000000000)>>40)
#define GET_VALID(X)     ((X & 0x8000000000000000)>>63)
#define GET_SIGNED(X)    ((X & 0x4000000000000000)>>62)
#define GET_RGB(X)       ( X & 0x0000000000FFFFFF)
#define GET_ALPHA(X)     ((X & 0x00000000FF000000)>>24)

#define SET_BLUE(X,Y)      (X = (X & 0xFFFFFFFFFFFFFF00) | ((Y) & 0x00000000000000FF))
#define SET_GREEN(X,Y)     (X = (X & 0xFFFFFFFFFFFF00FF) | ((Y<<8) & 0x000000000000FF00))
#define SET_RED(X,Y)       (X = (X & 0xFFFFFFFFFF00FFFF) | ((Y<<16) & 0x0000000000FF0000))
#define SET_INTENSITY(X,Y) (X = (X & 0xFFFFFF0000FFFFFF) | ((Y<<24) & 0x000000FFFF000000))
#define SET_SEGMENT(X,Y)   (X = (X & 0xFF0000FFFFFFFFFF) | ((Y<<40) & 0x00FFFF0000000000))
#define SET_VALID(X,Y)     (X = (X & 0x7FFFFFFFFFFFFFFF) | ((Y<<63) & 0x8000000000000000))
#define SET_SIGNED(X,Y)    (X = (X & 0x3FFFFFFFFFFFFFFF) | ((Y<<62) & 0x4000000000000000))
#define SET_RGB(X,Y)       (X = (X & 0xFFFFFFFFFF000000) | ((Y) & 0x0000000000FFFFFF))
#define SET_ALPHA(X,Y)     (X = (X & 0xFFFFFFFF00FFFFFF) | ((Y<<24) & 0x00000000FF000000))

TightSElem::TightSElem(SElem::DataSource newdata) {
  source(newdata);
}

TightSElem::TightSElem(const TightSElem& that): SElem() {
  (*this) = that;
}

const std::string& TightSElem::name() const {
  static const std::string name = "Compound 24RGB-16BW";
  return name;
}

void TightSElem::source(SElem::DataSource newsource) {
  SElemData = (TightSElemInternType*)newsource;
}

SElem::DataSource TightSElem::source() {
  return (SElem::DataSource)SElemData;
}

SElem& TightSElem::rgb
(SElem::Precision rval,SElem::Precision gval,SElem::Precision bval){
  SET_RED(*SElemData, rval);
  SET_GREEN(*SElemData, gval);
  SET_BLUE(*SElemData,bval);
  return (*this);
}

uint32_t TightSElem::rgba() const {
  uint32_t rgbaval = 0xFF000000;
  rgbaval = rgbaval | GET_RGB(*SElemData);
  return rgbaval;
}

SElem& TightSElem::rgba(uint32_t newrgb) {
  SET_RGB(*SElemData,(newrgb & 0x00FFFFFF));
  return (*this);
}


SElem::Precision TightSElem::red() const {
  return GET_RED(*SElemData);
}

SElem& TightSElem::red(SElem::Precision rval){
  SET_RED(*SElemData, (TightSElemInternType)rval);
  return (*this);
}

SElem::Precision TightSElem::green() const {
  return GET_GREEN(*SElemData);
}

SElem& TightSElem::green(SElem::Precision gval){
  SET_GREEN(*SElemData, (TightSElemInternType)gval);
  return (*this);
}

SElem::Precision TightSElem::blue() const{
  return GET_BLUE(*SElemData);
}

SElem& TightSElem::blue(SElem::Precision bval){
  SET_BLUE(*SElemData,(TightSElemInternType)bval);
  return (*this);
}

SElem::Precision TightSElem::intensity() const {
  return GET_INTENSITY(*SElemData);
}

/* Flatten an intensity to unsigned (sign extend if it's signed) */
SElem::Precision TightSElem::intensityUnsigned() const {
  if (isSigned()) {
    SElem::Precision_Signed intense = (int16_t)intensity();
    return (SElem::Precision)(intense + (32768));
  }
  else
    return intensity();
}

/* assume the value in intensity is signed */
SElem::Precision_Signed TightSElem::intensitySigned() {
  return signextend<SElem::Precision_Signed,16>(intensity());
}

SElem& TightSElem::intensity(SElem::Precision ival){
  SET_INTENSITY(*SElemData,(TightSElemInternType)ival);
  return (*this);
}

SElem::Precision TightSElem::segment() const {
  return GET_SEGMENT(*SElemData);
}

SElem& TightSElem::segment(SElem::Precision sval){
  SET_SEGMENT(*SElemData,(TightSElemInternType)sval);
  return (*this);
}

bool TightSElem::isValid() const {
  return GET_VALID(*SElemData);
}

SElem& TightSElem::isValid(bool validity){
  SET_VALID(*SElemData,(TightSElemInternType)validity);
  return (*this);
}

bool TightSElem::isSigned() const {
  return GET_SIGNED(*SElemData);
}

SElem& TightSElem::isSigned(bool validity){
  SET_SIGNED(*SElemData,(TightSElemInternType)validity);
  return (*this);
}

SElem::Precision TightSElem::zero() const {
  if (isSigned())
    return 32768;
  else
    return 0;
}

SElem::Precision TightSElem::size() const {
  return sizeof(TightSElemInternType);
}

/* Special efficient assignment method for TightSElem */

void TightSElem::clear()
{
    (*(this->SElemData)) = 0;
}

TightSElem& TightSElem::operator=(const TightSElem &rhs){
  (*(this->SElemData)) = (*(rhs.SElemData));
  return *this;
}

TightSElem TightSElem::operator*(const TightSElem& rhs)
{
  TightSElem result = *this;
  result*=rhs;
  return result;
}

TightSElem TightSElem::operator+(const TightSElem& rhs)
{
  TightSElem result = *this;
  result+=rhs;
  return result;
}

TightSElem TightSElem::operator-(const TightSElem& rhs)
{
  TightSElem result = *this;
  result-=rhs;
  return result;
}

SElem* TightSElem::New(SElem::DataSource data) {
  return new TightSElem(data);
}

/* Some generic overrides for operator shortcuts */

SElem::DataSource SElem::nullSource() {
  static Data nulldata[] = {0,0,0,0,0,0,0,0};
  return &nulldata[0];
}

SElem::~SElem() {

}

SElem::DataSource SElem::newDataSource() const {
  return new unsigned char[size()];
}

SElem& SElem::rgb(SElem::Precision rval,SElem::Precision gval,
                  SElem::Precision bval) {
  this->red(rval);
  this->green(gval);
  this->blue(bval);
  return (*this);
}

SElem& SElem::rgba(SElem::Precision rval,SElem::Precision gval,
                   SElem::Precision bval,SElem::Precision aval) {
  this->red(rval);
  this->green(gval);
  this->blue(bval);
  this->alpha(aval);
  return (*this);
}

SElem& SElem::rgba(uint32_t newrgb) {
  red  (newrgb & 0x00FF0000);
  green(newrgb & 0x0000FF00);
  blue (newrgb & 0x000000FF);
  return (*this);
}


uint32_t SElem::rgba() const {
  uint32_t rgbaval = 0;
  rgbaval = (red() << 16) | (green()<<8) | blue();
  rgbaval = rgbaval | 0xFF000000;
  return rgbaval;
}

SElem::Precision SElem::alpha() const {
  return 255;
}

SElem& SElem::alpha(SElem::Precision ) {
  return *this;
}

void SElem::clear(){
  this->rgb(0,0,0);
  this->intensity(0);
  this->segment(0);
  this->isValid(false);
}

SElem& SElem::operator= (const SElem &rhs){
  this->rgb(rhs.red(),rhs.green(),rhs.blue());
  this->segment(rhs.segment());
  this->isValid(rhs.isValid());
  this->isSigned(rhs.isSigned());
  this->intensity(rhs.intensity());
  return *this;
}

SElem& SElem::operator+=(const SElem &rhs){
  this->rgb(this->red()+rhs.red(),this->green()+rhs.green(),this->blue()+rhs.blue());
  this->intensity(this->intensity()+rhs.intensity());
  return *this;
}

SElem& SElem::operator-=(const SElem &rhs){
  this->rgb(this->red()-rhs.red(),this->green()-rhs.green(),this->blue()-rhs.blue());
  this->intensity(this->intensity()-rhs.intensity());
  return *this;
}

SElem& SElem::operator*=(const SElem &rhs){
  this->rgb(this->red()*rhs.red(),this->green()*rhs.green(),this->blue()*rhs.blue());
  this->intensity(this->intensity()*rhs.intensity());
  return *this;
}

bool SElem::operator==(const SElem &rhs) const {
  return (this->red()     == rhs.red() &&
          this->green()   == rhs.green() &&
          this->blue()    == rhs.blue() &&
          this->intensity() == rhs.intensity());
}

bool SElem::operator!=(const SElem &rhs) const {
  return !(this==&rhs);
}

SElem::DataSource SElem::operator*() {
  return source();
}

std::string SElem::toString() const {
  std::stringstream stemp;
  std::string
  outstr = "V";
  stemp  << isValid();
  outstr += stemp.str();
  stemp.clear();
  stemp.str("");
  outstr += ", ";
  outstr += "R";
  stemp  << red();
  outstr += stemp.str();
  stemp.clear();
  stemp.str("");
  outstr += ", ";
  outstr += "G";
  stemp  << green();
  outstr += stemp.str();
  stemp.clear();
  stemp.str("");
  outstr += ", ";
  outstr += "B";
  stemp  << blue();
  outstr += stemp.str();
  stemp.clear();
  stemp.str("");
  outstr += ", ";
  outstr += "I";
  stemp   << intensity();
  outstr += stemp.str();
  stemp.clear();
  stemp.str("");
  return outstr;
}

void SElem::rgb_mix(SElem &bg, SElem &fg, float alpha, SElem&targ) {
  targ.red  ( alpha * fg.red  () + (1 - alpha) * bg.red  () );
  targ.green( alpha * fg.green() + (1 - alpha) * bg.green() );
  targ.blue ( alpha * fg.blue () + (1 - alpha) * bg.blue () );
}

void SElem::rgb_mix_zerotrans(SElem &bg, SElem &fg, float alpha, SElem&targ) {
  if ( (fg.red() > 0) || (fg.green() > 0) || (fg.blue() > 0) ) {
    targ.red  ( alpha * fg.red  () + (1 - alpha) * bg.red  () );
    targ.green( alpha * fg.green() + (1 - alpha) * bg.green() );
    targ.blue ( alpha * fg.blue () + (1 - alpha) * bg.blue () );
  }
  else {
    targ.red  ( bg.red  () );
    targ.green( bg.green() );
    targ.blue ( bg.blue () );
  }
}

// ************************** B/W SElem Classes ********************************

BW8SElem::BW8SElem(SElem::DataSource newdata) {
  source(newdata);
}

BW8SElem::BW8SElem(const BW8SElem& target) {
  source((SElem::DataSource)target.SElemData);
}

const std::string& BW8SElem::name() const {
  static const std::string name = "8-bit BW Unsigned";
  return name;
}

void BW8SElem::source(SElem::DataSource newdata) {
  SElemData = ((uint8_t*)newdata);
}

SElem::DataSource BW8SElem::source() {
  return (SElem::DataSource)SElemData;
}

SElem& BW8SElem::rgb(SElem::Precision, SElem::Precision,SElem::Precision) {
    return *this;
}

uint32_t BW8SElem::rgba() const {
  return 0xFF000000 | GET_BLUE(*SElemData) | (GET_BLUE(*SElemData) << 8) 
                                           | (GET_BLUE(*SElemData) << 16);;
}

SElem& BW8SElem::rgba(uint32_t newdata) {
  *SElemData = 0 | GET_BLUE(newdata) | (GET_BLUE(newdata) << 8) 
                                 | (GET_BLUE(newdata) << 16);
  return *this;
}

SElem::Precision BW8SElem::red() const {
  return GET_BLUE(*SElemData);
}

SElem& BW8SElem::red(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}


SElem::Precision BW8SElem::green() const {
  return GET_BLUE(*SElemData);
}


SElem& BW8SElem::green(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}


SElem::Precision BW8SElem::blue() const {
  return GET_BLUE(*SElemData);
}


SElem& BW8SElem::blue(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}

SElem& BW8SElem::intensity(SElem::Precision newdata) {
  *SElemData = (uint8_t)newdata;
  return *this;
}

SElem::Precision BW8SElem::segment() const {
  return 0;
}

SElem& BW8SElem::segment(SElem::Precision) {
  return *this;
}

bool BW8SElem::isValid() const {
  return true;
}

SElem& BW8SElem::isValid(bool) {
  return *this;
}

SElem::Precision BW8SElem::size() const {
  return sizeof(uint8_t);
}

SElem::Precision BW8SElem::zero() const {
  return 0;
}

bool BW8SElem::isSigned() const {
  return false;
}

SElem& BW8SElem::isSigned(bool) {
  return *this;
}

SElem* BW8SElem::New(SElem::DataSource data) {
  return new BW8SElem(data);
}

SElem& BW8SElem::operator=(const SElem& target) {
  intensity(target.intensity());
  return *this;
}

SElem::Precision BW8SElem::intensity() const {
  return *SElemData;
}

SElem::Precision_Signed BW8SElem::intensitySigned() {
  return signextend<SElem::Precision_Signed,8>(intensity());
}

SElem::Precision BW8SElem::intensityUnsigned() const {
  return *SElemData;
}

//-------------------

BW16SElem::BW16SElem(SElem::DataSource newdata) {
  source(newdata);
}

BW16SElem::BW16SElem(const BW16SElem& target) {
  source((SElem::DataSource)target.SElemData);
}

const std::string& BW16SElem::name() const {
  static const std::string name = "16-bit BW Unsigned";
  return name;
}

void BW16SElem::source(SElem::DataSource newdata) {
  SElemData = ((uint16_t*)newdata);
}

SElem::DataSource BW16SElem::source() {
  return (SElem::DataSource)SElemData;
}

SElem& BW16SElem::rgb(SElem::Precision, SElem::Precision,SElem::Precision) {
    return *this;
}

uint32_t BW16SElem::rgba() const {
  return 0xFF000000 | GET_BLUE(*SElemData) | (GET_BLUE(*SElemData) << 8) 
                                           | (GET_BLUE(*SElemData) << 16);;
}

SElem& BW16SElem::rgba(uint32_t newdata) {
  *SElemData = 0 | GET_BLUE(newdata) | (GET_BLUE(newdata) << 8) 
                                 | (GET_BLUE(newdata) << 16);
  return *this;
}

SElem::Precision BW16SElem::red() const {
  return GET_BLUE(*SElemData);
}

SElem& BW16SElem::red(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}


SElem::Precision BW16SElem::green() const {
  return GET_BLUE(*SElemData);
}


SElem& BW16SElem::green(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}


SElem::Precision BW16SElem::blue() const {
  return GET_BLUE(*SElemData);
}


SElem& BW16SElem::blue(SElem::Precision newdata) {
  *SElemData = GET_BLUE(newdata);
  return *this;
}

SElem& BW16SElem::intensity(SElem::Precision newdata) {
  *SElemData = (uint16_t)newdata;
  return *this;
}

SElem::Precision BW16SElem::segment() const {
  return 0;
}

SElem& BW16SElem::segment(SElem::Precision) {
  return *this;
}

bool BW16SElem::isValid() const {
  return true;
}

SElem& BW16SElem::isValid(bool) {
  return *this;
}

SElem::Precision BW16SElem::size() const {
  return sizeof(uint16_t);
}

SElem::Precision BW16SElem::zero() const {
  return 0;
}

bool BW16SElem::isSigned() const {
  return false;
}

SElem& BW16SElem::isSigned(bool) {
  return *this;
}

SElem* BW16SElem::New(SElem::DataSource data) {
  return new BW16SElem(data);
}

SElem& BW16SElem::operator=(const SElem& target) {
  intensity(target.intensity());
  return *this;
}

SElem::Precision BW16SElem::intensity() const {
  return *SElemData;
}

SElem::Precision_Signed BW16SElem::intensitySigned() {
  return signextend<SElem::Precision_Signed,16>(intensity());
}

SElem::Precision BW16SElem::intensityUnsigned() const {
  return *SElemData;
}

// special methods for a signed version

BW16SignedSElem::BW16SignedSElem(SElem::DataSource data): BW16SElem(data) {

}

BW16SignedSElem::BW16SignedSElem(const BW16SignedSElem& target):
                                  BW16SElem((SElem::DataSource)target.SElemData) {

}

const std::string& BW16SignedSElem::name() const {
  static const std::string name = "16-bit BW Signed";
  return name;
}

bool BW16SignedSElem::isSigned() const {
  return true;
}

SElem::Precision BW16SignedSElem::zero() const {
  return 32768;
}

SElem* BW16SignedSElem::New(SElem::DataSource data) {
  return new BW16SignedSElem(data);
}

SElem::Precision BW16SignedSElem::intensityUnsigned() const {
  SElem::Precision_Signed intense = (int16_t)intensity();
  return (SElem::Precision)(intense + (32768));
}

SElem& BW16SignedSElem::operator=(const SElem& target) {
  intensity(target.intensity());
  return *this;
}

// *************************** RGBAI32SElem ************************************

RGBAI32SElem::RGBAI32SElem(SElem::DataSource newdata) {
  source(newdata);
}

RGBAI32SElem::RGBAI32SElem(const RGBAI32SElem& target) {
  source((SElem::DataSource)target.SElemData);
}

const std::string& RGBAI32SElem::name() const {
  static const std::string name = "32-bit RGBA";
  return name;
}

void RGBAI32SElem::source(SElem::DataSource newdata) {
  SElemData = ((uint32_t*)newdata);
}

SElem::DataSource RGBAI32SElem::source() {
  return (SElem::DataSource) SElemData;
}

SElem& RGBAI32SElem::rgb(SElem::Precision rr, 
                         SElem::Precision gg, 
                         SElem::Precision bb) {
  SET_RED  (*SElemData,rr);
  SET_GREEN(*SElemData,gg);
  SET_BLUE (*SElemData,bb);
  return *this;
}

uint32_t RGBAI32SElem::rgba() const {
  return *SElemData;
}

SElem& RGBAI32SElem::rgba(uint32_t newdata) {
  *SElemData = newdata;
  return *this;
}

SElem::Precision RGBAI32SElem::red() const {
  return GET_RED(*SElemData);
}

SElem& RGBAI32SElem::red(SElem::Precision newr) {
  SET_RED(*SElemData,newr);
  return *this;
}

SElem::Precision RGBAI32SElem::green() const {
  return GET_GREEN(*SElemData);
}

SElem& RGBAI32SElem::green(SElem::Precision newg) {
  SET_GREEN(*SElemData,newg);
  return *this;
}

SElem::Precision RGBAI32SElem::blue() const {
 return GET_BLUE(*SElemData);
}

SElem& RGBAI32SElem::blue(SElem::Precision newb) {
  SET_BLUE(*SElemData,newb);
  return *this;
}

SElem::Precision RGBAI32SElem::alpha() const {
  return GET_ALPHA(*SElemData);
}

SElem& RGBAI32SElem::alpha(SElem::Precision newa) {
  SET_ALPHA(*SElemData,newa);
  return *this;
}

SElem::Precision RGBAI32SElem::intensity() const {
  return (red()+green()+blue()) / 3;
}

SElem& RGBAI32SElem::intensity(SElem::Precision newi) {
  red(newi);
  green(newi);
  blue(newi);
  return *this;
}

SElem::Precision_Signed RGBAI32SElem::intensitySigned() {
  return (SElem::Precision_Signed) (red()+green()+blue()) / 3;
}

SElem::Precision RGBAI32SElem::intensityUnsigned() const {
  return (red()+green()+blue()) / 3;
}

SElem::Precision RGBAI32SElem::segment() const {
 return 0;
}

SElem& RGBAI32SElem::segment(SElem::Precision ) {
  return *this;
}

bool RGBAI32SElem::isValid() const {
  return true;
}

SElem& RGBAI32SElem::isValid(bool) {
  return *this;
}

bool RGBAI32SElem::isSigned() const {
  return false;
}

SElem& RGBAI32SElem::isSigned(bool) {
  return *this;
}

SElem::Precision RGBAI32SElem::zero() const {
  return 0;
}

SElem::Precision RGBAI32SElem::size() const {
  return sizeof(uint32_t);
}

void RGBAI32SElem::clear() {
  rgba(0xFF000000);
}

SElem* RGBAI32SElem::New(SElem::DataSource data) {
  return new RGBAI32SElem(data);
}

SElem& RGBAI32SElem::operator=(const SElem& target) {
  rgba(target.rgba());
  return *this;
}

/*------------------------------------------------------------------------------
 *                          Coordinates Classes
 * ---------------------------------------------------------------------------*/

SVector::SVector() {

}

SVector::SVector(unsigned dims) {
  setDim(dims);
}

SVector::SVector(const SVector& that) {
  (*this) = that;
}

SVector::SVector(SCoordinate& scoord) {
  setDim(scoord.getDim());
  unsigned bound = this->Scalars.size();
  for (unsigned i = 0; i < bound; i++)
    (*this)[i] = (SVector::Precision) scoord.getCoord(i);
}

SVector::SVector(const SCoordinate& scoord) {
  setDim(scoord.getDim());
  unsigned bound = this->Scalars.size();
  for (unsigned i = 0; i < bound; i++)
    (*this)[i] = (SVector::Precision) scoord.getCoord(i);
}

SVector::SVector(std::initializer_list< SVector::Precision > initlist) {
  setDim(initlist.size());
  for (unsigned i=0; i<initlist.size(); i++)
    (*this)[i] = *(initlist.begin()+i);
}

SVector::~SVector() {

}

void SVector::setDim(unsigned int newdims) {
  Scalars.resize(newdims, 0.0);
}

void SVector::reset() {
  unsigned bound = this->Scalars.size();
  for (unsigned i=0; i<bound; i++)
    Scalars[i] = 0.0;
}

unsigned int SVector::getDim() const {
  return Scalars.size();
}

SVector::Precision& SVector::operator[](unsigned int index) {
  return Scalars[index];
}

const SVector::Precision& SVector::operator[](unsigned int index) const {
  return Scalars[index];
}

SVector& SVector::operator= (const SVector &rhs){
  if (this != &rhs){
    this->Scalars = rhs.Scalars;
  }
  return *this;
}

SVector& SVector::operator+=(const SVector &rhs){
  if (Scalars.size() < rhs.Scalars.size())
    Scalars.resize(rhs.Scalars.size(),0.0);
  unsigned bound = rhs.Scalars.size();
  for (unsigned dimension=0;dimension<bound;dimension++)
    this->Scalars[dimension] += rhs.Scalars[dimension];
  return *this;
}

SVector& SVector::operator-=(const SVector &rhs){
  if (Scalars.size() < rhs.Scalars.size())
    Scalars.resize(rhs.Scalars.size(),0.0);
  unsigned bound = rhs.Scalars.size();
  if (bound == rhs.Scalars.size()){
    for (unsigned dimension=0;dimension<bound;dimension++)
      this->Scalars[dimension] -= rhs.Scalars[dimension];
  }
  return *this;
}

SVector SVector::operator+ (const SVector &rhs) const {
  SVector temp;
  temp = *this;
  temp += rhs;
  return temp;
}

SVector SVector::operator- (const SVector &rhs) const {
  SVector temp;
  temp = *this;
  temp -= rhs;
  return temp;
}

bool SVector::operator==(const SVector &rhs) const {
  unsigned bound = this->Scalars.size();
  if (bound != rhs.Scalars.size()) return false;
  for (unsigned dimension=0; dimension<bound; dimension++)
    if (! SGeom::isEqual(this->Scalars[dimension], rhs.Scalars[dimension]))
      return false;
  return true;
}

bool SVector::operator!=(const SVector &rhs) const{
  return !(*this == rhs);
}

bool SVector::operator<(const SVector& targ) const {
  if (mag() < targ.mag()) {
    return true;
  }
  else {
    return false;
  }
}

SVector& SVector::operator*=(const SVector::Precision scale) {
  unsigned bound = this->Scalars.size();
  for (unsigned dimension=0;dimension<bound;dimension++)
    this->Scalars[dimension] *= scale;
  return *this;
}

SVector SVector::operator*(const SVector::Precision scale) const {
  SVector temp;
  temp = *this;
  temp *= scale;
  return temp;
}

SVector& SVector::operator*=(const SVector& source) {
  unsigned bound = getDim() > source.getDim() ? source.getDim() : getDim();
  for (unsigned dimension=0;dimension<bound;dimension++)
    (*this)[dimension] = source[dimension] * Scalars[dimension];
  return *this;
}

SVector SVector::operator*(const SVector& source) const {
  SVector retval;
  retval = *this;
  retval *= source;
  return retval;
}

SVector& SVector::operator/=(const SVector::Precision scale) {
  unsigned bound = this->Scalars.size();
  for (unsigned dimension=0;dimension<bound;dimension++)
    this->Scalars[dimension] /= scale;
  return *this;
}

SVector SVector::operator/(const SVector::Precision scale) const {
  SVector temp;
  temp = *this;
  temp /= scale;
  return temp;
}

SVector& SVector::operator/=(const SVector& source) {
  unsigned bound = getDim() > source.getDim() ? source.getDim() : getDim();
  for (unsigned dimension=0;dimension<bound;dimension++)
    (*this)[dimension] = Scalars[dimension] / source[dimension];
  return *this;
}

SVector SVector::operator/(const SVector& source) const {
  SVector retval;
  retval = *this;
  retval /= source;
  return retval;
}

SVector& SVector::operator+=(const SVector::Precision addt) {
  unsigned bound = this->Scalars.size();
  for (unsigned dimension=0;dimension<bound;dimension++)
    this->Scalars[dimension] += addt;
  return *this;
}

SVector SVector::operator+(const SVector::Precision addt) const {
  SVector temp;
  temp = *this;
  temp += addt;
  return temp;
}

SVector SVector::operator!() const {
  SVector retval(getDim());
  unsigned bound = this->Scalars.size();
  for (unsigned dimension=0;dimension<bound;dimension++) {
    retval[dimension] = 0-(this->Scalars[dimension]);
  }
  return retval;
}

SVector::Precision SVector::dot(const SVector& targ) const {
  SVector::Precision accum = 0.0;
  // operation does not make sense on != dimensions
  unsigned bound =  (this->Scalars.size() < targ.Scalars.size()) ?
                                     this->Scalars.size() : targ.Scalars.size();
  for (unsigned i = 0; i < bound; i++)
    accum += ( Scalars[i] * targ.Scalars[i] );
  return accum;
}

SVector SVector::cross(const SVector& s2) const {
  SVector result;
  if ((getDim() == 3) && (s2.getDim() == 3)) {
    result.setDim(3);
    const SVector& s1 = *this;
    result[0] = ( s1.Scalars[1] * s2.Scalars[2] ) -
                ( s1.Scalars[2] * s2.Scalars[1] );
    result[1] = ( s1.Scalars[2] * s2.Scalars[0] ) -
                ( s1.Scalars[0] * s2.Scalars[2] );
    result[2] = ( s1.Scalars[0] * s2.Scalars[1] ) -
                ( s1.Scalars[1] * s2.Scalars[0] );
  }
  return result;
}

SVector::Precision SVector::mag() const {
  return sqrt(dot(*this));
}

SVector SVector::unit() const {
  unsigned bound = this->Scalars.size();
  SVector           unitvec(bound);
  SVector::Precision magnitude = mag();
  if ( ! SGeom::isEqual(magnitude,0.0) ) {
    for (unsigned i = 0; i < bound; i++)
      unitvec.Scalars[i] = (Scalars[i] / magnitude);
  }
  return unitvec;
}

SVector::Precision SVector::sum() const {
  double sum = 0.0;
  unsigned bound = this->Scalars.size();
  for (unsigned i=0; i < bound; i++) {
      sum += Scalars[i];
  }
  return sum;
}

double SVector::volume() const {
  double sum = 0.0;
  unsigned bound = this->Scalars.size();
  if (bound > 0) {
    sum = Scalars[0];
    for (unsigned i=1; i < bound; i++) {
      sum *= Scalars[i];
    }
  }
  return sum;
}

SVector SVector::fabs() const {
  SVector result;
  unsigned bound = this->Scalars.size();
  result.setDim(bound);
  for (unsigned i=0; i < bound; i++) {
    result.Scalars[i] = SGeom::fabs(Scalars[i]);
  }
  return result;
}

SVector::Precision SVector::x() const {
  return Scalars[0];
}

SVector::Precision SVector::y() const {
  return Scalars[1];
}

SVector::Precision SVector::z() const {
  return Scalars[2];
}

SVector& SVector::x(const SVector::Precision xx) {
  (*this)[0] = xx;
  return *this;
}

SVector& SVector::y(const SVector::Precision yy) {
  (*this)[1] = yy;
  return *this;
}

SVector& SVector::z(const SVector::Precision zz) {
  (*this)[2] = zz;
  return *this;
}

SVector& SVector::xy(const SVector::Precision xx, const SVector::Precision yy) {
  (*this)[0] = xx;
  (*this)[1] = yy;
  return *this;
}

SVector& SVector::xyz(const SVector::Precision xx, const SVector::Precision yy,
                      const SVector::Precision zz) {
  (*this)[0] = xx;
  (*this)[1] = yy;
  (*this)[2] = zz;
  return *this;
}

void SVector::promote() {
  setDim(getDim()+1);
}

void SVector::append(const SVector& newvec) {
  Scalars.insert(Scalars.end(),newvec.Scalars.begin(),newvec.Scalars.end());
}

std::string SVector::toString() const {
  std::stringstream outputstr;
  outputstr.precision(2);
  unsigned bound = this->Scalars.size();
  for (unsigned i=0; i < bound; i++) {
    outputstr << std::fixed << Scalars[i];
    if ( i != (bound - 1) )
      outputstr << "x";
  }
  return outputstr.str();
}

bool SVector::withinSpace(const SVector& coordref) const {
  if ( coordref.getDim() != getDim() )
    return false;
  unsigned dimcheck;
  unsigned rdim = coordref.getDim();
  for (dimcheck=0;dimcheck<rdim;dimcheck++)
    if ( ((coordref[dimcheck] >= (*this)[dimcheck])
       || (coordref[dimcheck] < -0.0)) 
      && (! SGeom::isEqual(0.0,coordref[dimcheck])) ) {
      return false;
    }
  return true;
}

bool SVector::isOrtho() const {
  bool retval = false;
  const unsigned bound = Scalars.size();
  for (unsigned long d=0; d < bound; d++) {
    if ( (SGeom::fabs(Scalars[d]) > 0.0) && !retval)
      retval = true;
    else if (! SGeom::isEqual(0.0,Scalars[d]) ) {
      retval = false;
      break;
    }
  }
  return retval;
}

SMatrix::SMatrix(unsigned int rows, unsigned int columns) {
  for (unsigned i=0; i<rows; i++)
    Rows.push_back(SVector(columns));
}

SMatrix::SMatrix(const SMatrix& ref) {
  *this = ref;
}

SMatrix::~SMatrix() {

}

unsigned int SMatrix::rows() const {
  return Rows.size();
}

unsigned int SMatrix::columns() const {
  unsigned res = 0;
  if (rows() > 0)
    res = Rows[0].getDim();
  return res;
}

const SVector& SMatrix::row(unsigned int rownum) const {
  return Rows[rownum];
}

SVector SMatrix::column(unsigned int colnum) const {
  unsigned numrows = rows();
  SVector result(numrows);
  for (unsigned i=0; i<numrows; i++) {
    auto pos = {i,colnum};
    result[i] = (*this)[pos];
  }
  return result;
}

void SMatrix::row(unsigned int rownum, const SVector& newrow) {
  Rows[rownum] = newrow;
}

void SMatrix::column(unsigned int column, const SVector& newcol) {
  unsigned lrows = newcol.getDim() < rows() ? newcol.getDim() : rows();
  for (unsigned i=0; i < lrows; i++) {
    auto pos = {i,column};
    (*this)[pos] = newcol[i];
  }
}

SMatrix& SMatrix::operator=(const SMatrix& targ) {
 Rows = targ.Rows;
 return *this;
}

bool SMatrix::operator==(const SMatrix& targ) const {
  bool retval = true;
  if ( rows() == targ.rows() && columns() == targ.columns() ) {
    for (unsigned i=0; i<rows(); i++)
      if (row(i) != targ.row(i))
        retval = false;
  }
  else
    retval = false;
  return retval;
}

SMatrix SMatrix::operator+(const SMatrix& targ) {
  SMatrix temp(*this);
  temp += targ;
  return temp;
}

SMatrix& SMatrix::operator+=(const SMatrix& targ) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  if (rowsl == targ.rows() && colsl == targ.columns()) {
    for (unsigned r=0; r<rowsl; r++)
      for (unsigned c=0; c<colsl; c++) {
        auto pos = {r,c};
        (*this)[pos] += targ[pos];
      }
  }
  return *this;
}

SMatrix SMatrix::operator-(const SMatrix& targ) {
  SMatrix temp(*this);
  temp -= targ;
  return temp;
}

SMatrix& SMatrix::operator-=(const SMatrix& targ) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  if (rowsl == targ.rows() && colsl == targ.columns()) {
    for (unsigned r=0; r<rowsl; r++)
      for (unsigned c=0; c<colsl; c++) {
        auto pos = {r,c};
        (*this)[pos] -= targ[pos];
      }
  }
  return *this;
}

SMatrix SMatrix::operator*(const SMatrix& targ) {
  unsigned rowsl    = rows();
  unsigned columnsl = targ.columns();
  SMatrix tmp(rowsl,columnsl);
  for (unsigned r=0; r < rowsl; r++)
    for (unsigned c=0; c < columnsl; c++) {
      auto pos = {r,c};
      tmp[pos] = targ.column(c).dot(row(r));
    }
  return tmp;
}

SMatrix& SMatrix::operator*=(const SMatrix& targ) {
  (*this) = (*this) * targ;
  return *this;
}

SMatrix SMatrix::operator*(const SVector::Precision scale) {
  SMatrix temp(*this);
  temp *= scale;
  return temp;
}

SMatrix& SMatrix::operator*=(const SVector::Precision scale) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  for (unsigned r=0; r<rowsl; r++)
    for (unsigned c=0; c<colsl; c++) {
      auto pos = {r,c};
      (*this)[pos] *= scale;
    }
  return *this;
}

SMatrix SMatrix::operator+(const SVector::Precision shift) {
  SMatrix temp(*this);
  temp += shift;
  return temp;
}

SMatrix& SMatrix::operator+=(const SVector::Precision shift) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  for (unsigned r=0; r<rowsl; r++)
    for (unsigned c=0; c<colsl; c++) {
      auto pos = {r,c};
      (*this)[pos] += shift;
    }
  return *this;
}

SMatrix SMatrix::operator-(const SVector::Precision shift) {
  SMatrix temp(*this);
  temp -= shift;
  return temp;
}

SMatrix& SMatrix::operator-=(const SVector::Precision shift) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  for (unsigned r=0; r<rowsl; r++)
    for (unsigned c=0; c<colsl; c++) {
      auto pos = {r,c};
      (*this)[pos] -= shift;
    }
  return *this;
}

SMatrix SMatrix::operator/(const SVector::Precision div) {
  SMatrix temp(*this);
  temp /= div;
  return temp;
}

SMatrix& SMatrix::operator/=(const SVector::Precision div) {
  unsigned rowsl = rows();
  unsigned colsl = columns();
  for (unsigned r=0; r<rowsl; r++)
    for (unsigned c=0; c<colsl; c++) {
      auto pos = {r,c};
      (*this)[pos] /= div;
    }
  return *this;
}

SVector::Precision& SMatrix::operator[]
                                (std::initializer_list< unsigned int > coords) {
  if (coords.size() == 2)
    return Rows[*coords.begin()][*(coords.begin()+1)];
  else 
    return Rows[0][0];
}

const SVector::Precision& SMatrix::operator[]
                          (std::initializer_list< unsigned int > coords) const {
  
  if (coords.size() == 2)
    return Rows[*coords.begin()][*(coords.begin()+1)];
  else
    return Rows[0][0];
}

std::string SMatrix::toString() {
  std::stringstream outstr;
  outstr << "\t";
  for (unsigned i=0; i<columns(); i++)
    outstr << "c" << i << "\t";
  for (unsigned r=0; r<rows(); r++) {
    outstr << std::endl << "r" << r << "\t";
    for (unsigned c=0; c<columns(); c++) {
      auto pos = {r,c};
      outstr << (*this)[pos] << "\t";
    }
  }
  return outstr.str();
}

SComplex::SComplex (SVector::Precision reali, SVector::Precision imaginaryi) {
  r(reali);
  i(imaginaryi);
}

SComplex::SComplex(const SComplex& inp) {
  *this = inp;
}

SComplex::~SComplex() {

}

SVector::Precision SComplex::mag() const {
  return sqrt((real*real) + (imaginary*imaginary));
}

SVector::Precision SComplex::r() const {
  return real;
}

SVector::Precision SComplex::i() const {
  return imaginary;
}

void  SComplex::r(SVector::Precision reali) {
  real = reali;
}

void SComplex::i(SVector::Precision imagi) {
  imaginary = imagi;
}

SComplex SComplex::conj() const {
  return SComplex(this->r(),-this->i());
}

SComplex& SComplex::operator= (const SComplex& rhs) {
  this->r(rhs.r());
  this->i(rhs.i());
  return *this;
}

bool SComplex::operator==(const SComplex& rhs) const {
  return (SGeom::isEqual(this->r(),rhs.r()) &&
          SGeom::isEqual(this->i(), rhs.i()));
}

SComplex SComplex::operator+ (const SComplex& rhs) const{
  return SComplex(*this) += rhs;
}

SComplex& SComplex::operator+=(const SComplex& rhs) {
  this->r(this->r() + rhs.r());
  this->i(this->i() + rhs.i());
  return *this;
}

SComplex SComplex::operator- (const SComplex& rhs) const {
  return SComplex(*this) -= rhs;
}

SComplex& SComplex::operator-=(const SComplex& rhs) {
  this->r(this->r() - rhs.r());
  this->i(this->i() - rhs.i());
  return *this;
}

SComplex SComplex::operator* (const SComplex& rhs) const {
  return SComplex(*this) *= rhs;
}

SComplex& SComplex::operator*=(const SComplex& rhs) {
  // (a+bi)(c+di)
  // (ac-bd)+(bc+ad)i
  SComplex lhs(*this);
  this->r( (lhs.r() * rhs.r()) - (lhs.i() * rhs.i()) );
  this->i( (lhs.i() * rhs.r()) + (lhs.r() * rhs.i()) );
  return *this;
}

SComplex SComplex::operator/ (const SComplex& rhs) const {
  return SComplex(*this) /= rhs;
}

SComplex& SComplex::operator/=(const SComplex& rhs) {
  // (a+bi)(c+di)
  // ((ac+bd)/(c^2+d^2)) +  ((bc-ad)/(c^2+d^2))i
  SComplex lhs(*this);
  SVector::Precision divterm = (rhs.r() * rhs.r()) + (rhs.i() * rhs.i());
  this->r( ( (lhs.r() * rhs.r()) + (lhs.i() * rhs.i()) ) / divterm );
  this->i( ( (lhs.i() * rhs.r()) - (lhs.r() * rhs.i()) ) / divterm );
  return *this;
}

SComplex SComplex::operator+(SVector::Precision addi) const {
  return SComplex(*this) += addi;
}

SComplex & SComplex::operator+=(SVector::Precision addi) {
  this->r(this->r()+addi);
  this->i(this->i()+addi);
  return *this;
}

SComplex SComplex::operator-(SVector::Precision mini) const {
  return SComplex(*this) -= mini;
}

SComplex & SComplex::operator-=(SVector::Precision mini) {
  this->r(this->r()-mini);
  this->i(this->i()-mini);
  return *this;
}

SComplex SComplex::operator*(SVector::Precision muli) const {
  return SComplex(*this) *= muli;
}

SComplex & SComplex::operator*=(SVector::Precision muli) {
  this->r(this->r()*muli);
  this->i(this->i()*muli);
  return *this;
}

SComplex SComplex::operator/(SVector::Precision divi) const {
  return SComplex(*this) /= divi;
}


SComplex & SComplex::operator/=(SVector::Precision divi) {
  this->r(this->r()/divi);
  this->i(this->i()/divi);
  return *this;
}

std::string SComplex::toString() const {
  std::stringstream output;
  output << this->r();
  if (this->i() > 0.0)
    output << "+"; // minus is added automatically for negatives
  output << this->i() << "i";
  return output.str();
}

SLine::SLine() {
  Definite = false;
}

SLine::SLine(SVector nstart, SVector nend) {
  Definite = false;
  start(nstart);
  end(nend);
}

SLine::SLine(const SLine& that) {
  (*this) = that;
}

SLine::~SLine() {

}

SLine& SLine::operator=(const SLine& target){
  start(target.start());
  end(target.end());
  definite(target.definite());
  return *this;
}

bool SLine::definite() const {
  return Definite;
}

void SLine::definite(bool ndef) {
  Definite = ndef;
}

const SVector& SLine::start() const {
  return Start;
}

void SLine::start(const SVector& news) {
  Start = news;
}

const SVector& SLine::end()  const {
  return End;
}

void SLine::end(const SVector& nend) {
  End = nend;
}

SVector SLine::unit() const {
  return (end() - start()).unit();
}

bool SLine::check() const {
  return Start.getDim() == End.getDim();
}

void SLine::reset() {
  Start.reset();
  End.reset();
}

SPlane::SPlane() {

}

SPlane::SPlane(SVector newpoint,SVector newnormal) {
  point(newpoint);
  normal(newnormal);
}

SPlane::SPlane(const SPlane& source) {
  *this = source;
}

SPlane::~SPlane() {

}

const SVector& SPlane::point() const {
  return Point;
}

void SPlane::point(const SVector& newp) {
  Point = newp;
}

const SVector& SPlane::normal() const {
  return Normal;
}

void SPlane::normal(const SVector& nnorm) {
  Normal = nnorm;
}

bool SPlane::check() const {
  return Normal.getDim() == Point.getDim();
}

void SPlane::reset() {
  Normal.reset();
  Point.reset();
}

SPlane& SPlane::operator=(const SPlane& newplane) {
  Point  = newplane.Point;
  Normal = newplane.Normal;
  return *this;
}

bool SPlane::operator==(const SPlane& otherplane) const {
  return ( (Point == otherplane.Point) && (Normal == otherplane.Normal) );
}

const SVector::Precision SGeom::Epsilon=0.0001;

SVector::Precision SGeom::fabs(SVector::Precision source) {
  if (source < 0)
    return -source;
  else
    return source;
}

long unsigned int SGeom::abs(long int tabsval) {
  return std::abs(tabsval);
}


SVector::Precision SGeom::sround(SVector::Precision toround) {
  return toround < 0.0 ? ceil(toround - 0.5) : floor(toround + 0.5);
}

SVector::Precision SGeom::sceil(SVector::Precision toceil) {
  return ceil(toceil);
}

SVector::Precision SGeom::sfloor(SVector::Precision tofloor) {
  return floor(tofloor);
}

bool SGeom::isEqual(SVector::Precision val1, SVector::Precision val2,
                    SVector::Precision localep) {
  bool result = false;
  if (val1 == val2)
    result = true;
  else {
    SVector::Precision val1a = SGeom::fabs(val1);
    SVector::Precision val2a = SGeom::fabs(val2);
    SVector::Precision diff  = SGeom::fabs(val1 - val2);
    if ( val1a < localep || val2a < localep)
      result = diff < localep;
    else {
      SVector::Precision largest = (val2a > val1a) ? val1a : val1a;
      if (diff <= largest * localep)
        result = true;
    }
  }
  return result;
}

SGeom::intersection_res  SGeom::lineplaneintersect
                           (const SLine& sourceline,const SPlane& targetplane) {
  /*
   *     (p0 - l0) . n
   * d = --------------
   *        l . n
   */
  SVector::Precision distance         = 0.0;
  SVector::Precision planelinedotnorm = 0.0;
  SVector::Precision linedotnorm      = 0.0;
  SGeom::intersection_res result;

  planelinedotnorm =
           (targetplane.point() - sourceline.start()).dot(targetplane.normal());
  linedotnorm =
              sourceline.unit().dot(targetplane.normal());
  // check nature of result
  if ( SGeom::isEqual(0.0,linedotnorm) ) {
    if ( SGeom::isEqual(0.0,planelinedotnorm) ) {
      // parallel and in-plane
      result.intersects = true;
      result.parallel   = true;
      result.intersection = sourceline.start();
    }
    else {
      // parallel but out-of-plane
      result.intersects = false;
      result.parallel   = true;
    }
  } else {
  // calculate 'd' and determinine if it is within the line (on a definite line)
    result.parallel = false;
    distance        = planelinedotnorm / linedotnorm;
    // check if the line is definite, in which case check if it's within length
    if ( sourceline.definite() &&
         ((distance < 0) ||
          (distance >  (sourceline.end() - sourceline.start()).mag() ) )
       ) {
      // outside of line range
      result.intersects = false;
    }
    else {
      // inside line range
      result.intersects = true;
      result.intersection = (sourceline.unit() * distance) + sourceline.start();
    }
  }
  return result;
}

SLine SGeom::planeplaneintersection(const SPlane& plane1,const SPlane& plane2) {
  SLine result;
  // compute intersection line -- the ortho line to the two plane normals
  SVector intdir = plane1.normal().cross(plane2.normal());
  // check that the result is valid and that there is an intersection
  if (intdir.getDim() == 3 && (intdir != SVector({0,0,0}) )) {
    // scale direction to |1|
    intdir = intdir.unit();
    // Next steps: find a point on the plane-intersection direction vector
    // 1 project normal from plane2 onto plane1
    SLine projnorm(plane2.point(),plane2.point()+plane2.normal());
    projnorm.definite(false);
    intersection_res projint = SGeom::lineplaneintersect(projnorm,plane1);
    if ( (!projint.intersects) && (projint.parallel)) {
      // correct for ortho planes
      projint.intersects = true;
      projint.intersection = plane1.point() + plane2.normal();
    }
    if (projint.intersects) {
      // 2 construct a line on plane1, with respect to the plane-point
      SLine planeline(plane1.point(),projint.intersection);
      planeline.definite(false);
      // 3 intersect this line with plane2, to reveal point on line
      intersection_res projlinint = SGeom::lineplaneintersect(planeline,plane2);
      if (projlinint.intersects) {
        // result a line representing the plane-plane intersection
        result = SLine(projlinint.intersection,projlinint.intersection+intdir);
      }
    }
  }
  return result;
}

/*******************************************************************************
 *                               Coordinate
 ******************************************************************************/

SCoordinate::SCoordinate() {

}

SCoordinate::SCoordinate(SCoordinate::Precision dims) {
  setDim(dims);
}

SCoordinate::SCoordinate(const SCoordinate& rhs) {
  *this = rhs;
}

SCoordinate::SCoordinate(std::initializer_list< SCoordinate::Precision > coordlist) {
  setDim(coordlist.size());
  for (unsigned i=0; i < coordlist.size(); i++)
    (*this)[i] = *(coordlist.begin()+i);
}

SCoordinate::~SCoordinate() {

}

void SCoordinate::reset()
{
  SCoordinate::Precision coordsmax = getDim();
  for (SCoordinate::Precision ldim=0;ldim<coordsmax;ldim++)
    Coords[ldim] = 0;
}

SCoordinate::Precision SCoordinate::getDim() const {
  return Coords.size();
}

void SCoordinate::setDim(SCoordinate::Precision newdims) {
  Coords.resize(newdims,0);
}

SCoordinate::Precision SCoordinate::popDim() {
  SCoordinate::Precision retval = 0;
  if (getDim() > 0) {
    retval = Coords.back();
    Coords.pop_back();
  }
  return retval;
}

void SCoordinate::promote() {
  setDim(getDim()+1);
  (*this)[getDim()-1] = 1;
}

SCoordinate::Precision SCoordinate::getCoord(SCoordinate::Precision dim) const {
  return Coords[dim];
}

void SCoordinate::setCoord(SCoordinate::Precision dim, SCoordinate::Precision loc) {
  Coords[dim] = loc;
}

void SCoordinate::append(const SCoordinate& newcoord) {
  Coords.insert(Coords.end(),newcoord.Coords.begin(),newcoord.Coords.end());
}

SCoordinate::Precision SCoordinate::volume() const {
  SCoordinate::Precision    dimension;
  unsigned long long accum;
  if (getDim() < 1) return 0;
  accum=1;
  SCoordinate::Precision tdim = getDim();
  for (dimension=0; dimension<tdim;dimension++)
    accum*=Coords[dimension];
  return accum;
}

std::vector< SCoordinate::Precision > SCoordinate::precalcLinearIndices() const {
  std::vector< SCoordinate::Precision > IndicCache;
  SCoordinate::Precision sub_accum;
  long long int    dimpos,subdimpos;
  long long int    dimnum = getDim();
  IndicCache.resize(dimnum,0);
  for (dimpos=dimnum-1;dimpos>0;dimpos--) {
    sub_accum=1;
    for (subdimpos=dimpos-1;subdimpos>=0;subdimpos--)
      sub_accum*=getCoord(subdimpos);
    IndicCache[dimpos] = sub_accum;
  }
  return IndicCache;
}

SCoordinate::Precision SCoordinate::toLinear(const SCoordinate &constraint) const {
  SCoordinate::Precision linear_accummulator=0, sub_accum=0;
  long long int    dimpos,subdimpos;
  long long int    dimnum = getDim();
  if (dimnum > 0) {
    for (dimpos=dimnum-1;dimpos>0;dimpos--) {
      sub_accum=1;
      for (subdimpos=dimpos-1;subdimpos>=0;subdimpos--)
        sub_accum*=constraint.getCoord(subdimpos);
      linear_accummulator+=(getCoord(dimpos)*sub_accum);
    }
    linear_accummulator+=getCoord(0);
  }
  return linear_accummulator;
}

SCoordinate::Precision SCoordinate::toLinear(
                      const std::vector< SCoordinate::Precision >& indiccache) const {
  SCoordinate::Precision linear_accummulator=0;
  long long int    dimpos;
  long long int    dimnum = getDim();
  if (dimnum > 0) {
    for (dimpos=dimnum-1;dimpos>0;dimpos--)
      linear_accummulator+=(getCoord(dimpos)*indiccache[dimpos]);
    linear_accummulator+=getCoord(0);
  }
  return linear_accummulator;
}

void SCoordinate::fromLinear(SCoordinate::Precision lin,
                                                const SCoordinate& constraint) {
  std::vector<SCoordinate::Precision> indiccache = constraint.precalcLinearIndices();
  setDim(constraint.getDim());
  unsigned dims = getDim();
  for (unsigned dimpos=dims-1;dimpos>0;dimpos--) {
    (*this)[dimpos] = lin / indiccache[dimpos];
    lin = lin % indiccache[dimpos];
  }
  if (dims > 0)
    (*this)[0] = lin;
}

std::string SCoordinate::toString() const {
  std::stringstream stemp;
  std::string outstr;
  SCoordinate::Precision counter;

  if(!getDim()) return outstr.c_str();

  //output each dimension
  for (counter=0;counter<getDim();counter++) {
    stemp  << getCoord(counter);
    outstr += stemp.str();
    stemp.clear();
    stemp.str("");
    if (counter != (getDim()-1)) outstr += "x";
  }
  return outstr;
}

SCoordinate::Precision SCoordinate::x() const {
  return Coords[0];
}

SCoordinate& SCoordinate::x(SCoordinate::Precision newx) {
  Coords[0] = newx;
  return *this;
}

SCoordinate::Precision SCoordinate::y() const {
  return Coords[1];
}

SCoordinate& SCoordinate::y(SCoordinate::Precision newy) {
  Coords[1] = newy;
  return *this;
}

SCoordinate::Precision SCoordinate::z() const {
  return Coords[2];
}

SCoordinate& SCoordinate::z(SCoordinate::Precision newz) {
  Coords[2] = newz;
  return *this;
}

SCoordinate::Precision SCoordinate::t() const {
  return Coords[3];
}

SCoordinate& SCoordinate::t(SCoordinate::Precision newt) {
  Coords[3] = newt;
  return *this;
}

SCoordinate& SCoordinate::xy(SCoordinate::Precision x1, SCoordinate::Precision y1) {
  x(x1);
  y(y1);
  return *this;
}

SCoordinate& SCoordinate::xyz(SCoordinate::Precision x1, SCoordinate::Precision y1,
                                                          SCoordinate::Precision z1) {
  x(x1);
  y(y1);
  z(z1);
  return *this;
}

/* Increment a coordinate space according the row/column/dimension constaint
 * Return false when the end of the Coordinate space constrain is reached
 * (when false is returned, the subsequent value of the Coordinate is undefined)
 */
bool SCoordinate::tabincrement(const SCoordinate& constraint) {
  if (this->getDim() != constraint.getDim()) return false;
  SCoordinate::Precision curpos;
  SCoordinate::Precision ldim = getDim();
  /* cycle through each dimension in the coordinate space */
  for (curpos=0;curpos<ldim; curpos++){
    if ( constraint.getCoord(curpos) != 0 && this->getCoord(curpos) < 
       (constraint.getCoord(curpos)-1) ){
      /* incrememnt this dimension if there is room */
      (*this)[curpos] ++;
      return true;
    }
    else{
      /* limit of this dimension reached */
      if ( curpos == (constraint.getDim()-1)){
        /* final dimension, final element */
        return false;
      }
      else{
        /* reset and start next order dimension */
        (*this)[curpos] = 0;
        continue;
      }
    }
  }
  return false;
}

bool SCoordinate::tabdecrement(const SCoordinate& constraint) {
  if (this->getDim() != constraint.getDim()) return false;
  SCoordinate::Precision curpos;
  SCoordinate::Precision ldim = getDim();
  /* cycle through each dimension in the coordinate space */
  for (curpos=0;curpos<ldim; curpos++){
    if ( constraint.getCoord(curpos) != 0 && this->getCoord(curpos) > 0) {
      /* decrememnt this dimension if there is room */
      (*this)[curpos] --;
      return true;
    }
    else{
      /* limit of this dimension reached */
      if ( curpos == (constraint.getDim()-1)){
        /* final dimension, final element */
        return false;
      }
      else{
        /* reset and start next order dimension */
        (*this)[curpos] = constraint[curpos]-1;
        continue;
      }
    }
  }
  return false;
}

SCoordinate::Precision& SCoordinate::operator[](const SCoordinate::Precision index) {
  return Coords[index];
}

const SCoordinate::Precision& SCoordinate::operator[]
                                          (const SCoordinate::Precision index) const {
  return Coords[index];
}

SCoordinate& SCoordinate::operator= (const SCoordinate &rhs) {
  if (this != &rhs){
    setDim(rhs.getDim());
    SCoordinate::Precision ldim = getDim();
    for (SCoordinate::Precision dimension=0;dimension<ldim;dimension++) {
      this->Coords[dimension] = rhs.Coords[dimension];
    }
  }
  return *this;
}

SCoordinate& SCoordinate::operator+=(const SCoordinate &rhs) {
  if (this->getDim() == rhs.getDim()) {
    SCoordinate::Precision dimension;
    SCoordinate::Precision ldim = getDim();
    for (dimension=0;dimension<ldim;dimension++)
      this->Coords[dimension] += rhs.Coords[dimension];
  }
  return *this;
}

SCoordinate& SCoordinate::operator-=(const SCoordinate &rhs) {
  if (this->getDim() == rhs.getDim()) {
    SCoordinate::Precision dimension;
    SCoordinate::Precision ldim = getDim();
    for (dimension=0;dimension<ldim;dimension++)
      this->Coords[dimension] -= rhs.Coords[dimension];
  } 
  return *this;
}

SCoordinate& SCoordinate::operator*=(const SCoordinate &rhs) {
  if (this->getDim() == rhs.getDim()) {
    SCoordinate::Precision dimension;
    SCoordinate::Precision ldim = getDim();
    for (dimension=0;dimension<ldim;dimension++)
      this->Coords[dimension] *= rhs.Coords[dimension];
  }
  return *this;
}

SCoordinate SCoordinate::operator+ (const SCoordinate &rhs) const {
  SCoordinate temp;
  temp = *this;
  if (this->getDim() == rhs.getDim()) {
    temp += rhs;
  }
  return temp;
}

SCoordinate SCoordinate::operator- (const SCoordinate &rhs) const {
  SCoordinate temp;
  temp = *this;
  if (this->getDim() == rhs.getDim()) {
    temp -= rhs;
  }
  return temp;
}

SCoordinate SCoordinate::operator* (const SCoordinate &rhs) const {
  SCoordinate temp;
  temp = *this;
  if (this->getDim() == rhs.getDim()) {
    temp *= rhs;
  }
  return temp;
}

bool SCoordinate::operator==(const SCoordinate &rhs) const {
  if (this->getDim() != rhs.getDim()) return false;
  SCoordinate::Precision dimension;
  SCoordinate::Precision ldim = getDim();
  for (dimension=0; dimension<ldim; dimension++)
    if (this->Coords[dimension] != rhs.Coords[dimension])
      return false;
  return true;
}

bool SCoordinate::operator!=(const SCoordinate &rhs) const {
  return !(*this == rhs);
}

SCoordinate& SCoordinate::clamp_floor(const SVector& source) {
  setDim(source.getDim());
  SCoordinate::Precision ldim = getDim();
  for (SCoordinate::Precision dimension=0;dimension<ldim;dimension++)
    this->Coords[dimension]=(SCoordinate::Precision)SGeom::sfloor(source[dimension]);
  return (*this);
}

SCoordinate& SCoordinate::clamp_ceiling(const SVector& source) {
  setDim(source.getDim());
  SCoordinate::Precision ldim = getDim();
  for (SCoordinate::Precision dimension=0;dimension<ldim;dimension++)
    this->Coords[dimension]=(SCoordinate::Precision) SGeom::sceil(source[dimension]);
  return (*this);
}

SCoordinate& SCoordinate::clamp_round(const SVector& source) {
  setDim(source.getDim());
  SCoordinate::Precision ldim = getDim();
  for (SCoordinate::Precision dimension=0;dimension<ldim;dimension++) {
    this->Coords[dimension]=(SCoordinate::Precision) SGeom::sround(source[dimension]);
  }
  return (*this);
}

bool SCoordinate::withinSpace(const SCoordinate& coordref) const {
  if ( coordref.getDim() > getDim() )
    return false;
  SCoordinate::Precision dimcheck;
  SCoordinate::Precision rdim = coordref.getDim();
  for (dimcheck=0;dimcheck<rdim;dimcheck++)
    if ( (coordref.getCoord(dimcheck) >= getCoord(dimcheck))
      || (coordref.getCoord(dimcheck) < 0))
      return false;
  return true;
}

std::vector< std::pair < SCoordinate, SCoordinate > > SCoordinate::decompose
                                              (const SCoordinate& dsize) const {
  std::vector< std::pair < SCoordinate, SCoordinate > > result;
  if (dsize.getDim() == getDim()) {
    // cycle through dimensions
    for (SCoordinate::Precision d=0; d < getDim(); d++) {
      if (d == 0) {
        // bootstrap the first dimension
        for (SCoordinate::Precision b=0; b <= (getCoord(0)-dsize[0]); b+=dsize[0]) {
          SCoordinate newcoord(getDim());
          newcoord[0] = b;
          result.push_back(std::make_pair(newcoord,dsize));
        }
        // handle the remainder
        SCoordinate::Precision rem = getCoord(0) % dsize[0];
        if (rem > 0) {
          SCoordinate remcoord(getDim());
          SCoordinate remainext(dsize);
          remcoord[0]  = getCoord(0) - rem;
          remainext[0] = rem;
          result.push_back(std::make_pair(remcoord,remainext));
        }
      } else {
        // expand & extend previous dims
        SCoordinate::Precision prevcoordstart = 0;
        SCoordinate::Precision prevcoordend  = result.size();
        for (SCoordinate::Precision b=0; b <= (getCoord(d)-dsize[d]); b+=dsize[d]) {
          if (b > 0) {
            for (SCoordinate::Precision i = prevcoordstart; i <prevcoordend; i++)
              result.push_back(result[i]);
            SCoordinate::Precision added = prevcoordend - prevcoordstart;
            prevcoordstart = prevcoordend;
            prevcoordend  += added;
          }
          // set missing dim
          for (SCoordinate::Precision p=prevcoordstart; p < prevcoordend; p ++)
            result[p].first[d] = b;
        }
        // handle the remainder
        SCoordinate::Precision rem = getCoord(d) % dsize[d];
        if (rem > 0) {
          for (SCoordinate::Precision rcp = prevcoordstart;rcp < prevcoordend;rcp++) {
            SCoordinate remcoord(result[rcp].first);
            SCoordinate remext  (result[rcp].second);
            remcoord[d] = getCoord(d) - rem;
            remext  [d] = rem;
            result.push_back(std::make_pair(remcoord,remext));
          }
        }
      }
    }
  }
  return result;
}

/*------------------------------------------------------------------------------
 *                         ---------END---------
 * ---------------------------------------------------------------------------*/
