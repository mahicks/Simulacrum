/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "definitions.h"
#include "slockable.h"
#include "error.h"
#ifndef NOCURL
#include <curl/curl.h>
#endif

using namespace Simulacrum;

static bool      SIMU_INIT_DONE = false;
static SLockable SIMU_INIT_LOCK;

bool SimulacrumLibrary::init() {
  bool result;
  SIMU_INIT_LOCK.lock();
  if (!SIMU_INIT_DONE) {
    // perform the one-time library init process
#ifndef NOCURL
  // curl init
  curl_global_init(CURL_GLOBAL_DEFAULT);
#endif
    SIMU_INIT_DONE = true;
  }
  result = SIMU_INIT_DONE;
  result = SIMU_INIT_DONE;
  SIMU_INIT_LOCK.unlock();
  return result;
}

const std::string SimulacrumInformation::getInfoString() {
  return "Simulacrum: A library suite for volume and \
image space interaction";
}

const std::string SimulacrumInformation::getVersionString() {
  std::stringstream versionout;
  versionout << SIMULACRUM_VERSION_MAJOR << "."
             << SIMULACRUM_VERSION_MINOR << "."
             << SIMULACRUM_VERSION_REV;
#ifdef SREVISION
  versionout << "-" << TOSTRING(SREVISION);
#endif
  return versionout.str();
}

// error handling wrapper for int return functions
bool Simulacrum::success(serror_t retval) {
  return retval == 0;
}

// convert an error code into an exception when needed, providing a message
void Simulacrum::excepter(serror_t retval, const std::string& description) {
  if (!success(retval)) {
    std::stringstream message;
    message << description << " (error code: " << retval << ")";
    throw SimulacrumExceptionGeneric(message.str());
  }
}

const std::string SimulacrumInformation::getAuthors() {
  return "Michael A. Hicks";
}

const std::string SimulacrumInformation::getURI() {
  return "http://www.mahicks.org/Simulacrum";
}

const std::string SimulacrumInformation::getLic() {
  return "Copyright (C) 2022 Michael A. Hicks. Distributed under the terms of \
the GNU Lesser General Public License.";
}

const std::string SimulacrumInformation::getBuildInformation() {
  std::string result = __DATE__;
  result += " ";
  result += __TIME__;
  result += " ";
#ifdef __GNUC__
  result += "GCC-";
  std::stringstream conv;
  conv << __GNUC__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__;
  result += conv.str() + " ";
#endif
#ifdef __DEBUG
  result += "DEBUG ";
#else
  result += "RELEASE ";
#endif
#ifdef BOUND_CHECK
  result += "BOUND_CHECK ";
#endif
#ifdef NOCURL
  result += "NOCURL ";
#endif
#ifdef NOLUA
  result += "NOLUA ";
#endif
#ifdef NOJ2k
  result += "NOJ2K ";
#endif
#ifdef NOLJPEG
  result += "NOLJPEG ";
#endif
#ifdef NODCMDIC
  result += "NODCMDIC ";
#endif
#ifdef NOZLIB
  result += "NOZLIB ";
#endif
#ifdef BOOST_MUTEX
  result += "BOOST_MUTEX ";
#endif
#ifdef ANDROID
  result += "ANDROID ";
#endif
#ifdef MOBILE
  result += "MOBILE ";
#else
  result += "DESKTOP ";
#endif
  return result;
}
