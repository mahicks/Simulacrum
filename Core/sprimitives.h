/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Primitives:
 * The basic building blocks of the Simulacrum library, including pixel/voxel
 * containers, collections of pixel/voxels and their interfaces.
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_PRIMITIVES
#define SIMULACRUM_PRIMITIVES

#include <stdint.h>
#include <cstdarg>
#include <string>
#include <vector>
#include <initializer_list>
#include "definitions.h"

namespace Simulacrum {

/*------------------------------------------------------------------------------
 *                             SElem Classes
 * ---------------------------------------------------------------------------*/

#ifdef MOBILE
typedef uint32_t BaseInt;
typedef int32_t  BaseIntSign;
typedef float    BaseFP;
#else
typedef uint64_t BaseInt;
typedef int64_t  BaseIntSign;
typedef double   BaseFP;
#endif

/* General purpose sign extension, useful for signed intensities */

template <typename T, unsigned B>
inline T signextend(const T x) {
  struct {T x:B;} s;
  return s.x = x;
}

#define RGBA_ONECHAN(X)    ( 0xFF000000 | (X << 16) | (X << 8) | (X) )

/* The SElem interface specifier */
class SIMU_API SElem {
public:
  typedef unsigned char*    DataSource;
  typedef unsigned char     Data;
  typedef BaseIntSign       Precision_Signed;
  typedef BaseInt           Precision;
  typedef SPtr<SElem>       Ptr;
  virtual                  ~SElem();
  virtual
  const std::string&        name() const =0;
  virtual void              source(SElem::DataSource)=0;
  virtual SElem::DataSource source()=0;
  virtual
  SElem::DataSource         newDataSource() const;
  virtual SElem&            rgb
                           (SElem::Precision,SElem::Precision,SElem::Precision);
  virtual SElem&            rgba
                           (SElem::Precision,SElem::Precision,SElem::Precision,
                            SElem::Precision);
  virtual uint32_t          rgba() const;
  virtual SElem&            rgba(uint32_t);
  virtual SElem::Precision  red() const =0;
  virtual SElem&            red(SElem::Precision) =0;
  virtual SElem::Precision  green() const =0;
  virtual SElem&            green(SElem::Precision)=0;
  virtual SElem::Precision  blue() const =0;
  virtual SElem&            blue(SElem::Precision)=0;
  virtual SElem::Precision  alpha() const;
  virtual SElem&            alpha(SElem::Precision);
  virtual SElem::Precision  intensity() const =0;
  virtual SElem&            intensity(SElem::Precision)=0;
  virtual SElem::Precision  intensityUnsigned() const =0;
  virtual SElem::Precision_Signed
                            intensitySigned()=0;
  virtual SElem::Precision  segment() const =0;
  virtual SElem&            segment(SElem::Precision)=0;
  virtual bool              isSigned() const =0;
  virtual SElem&            isSigned(bool)=0;
  virtual SElem::Precision  zero() const =0;
  virtual SElem::Precision  size() const =0;
  virtual bool              isValid() const =0;
  virtual SElem&            isValid(bool)=0;
  virtual void              clear();
  virtual SElem &           operator= (const SElem &);
  virtual SElem &           operator+=(const SElem &);
  virtual SElem &           operator-=(const SElem &);
  virtual SElem &           operator*=(const SElem &);
  virtual bool              operator==(const SElem &) const;
  virtual bool              operator!=(const SElem &) const;
  virtual SElem::DataSource operator* ();
  virtual std::string       toString() const;
  virtual SElem*            New
                              (SElem::DataSource data = nullptr)=0;
  static  void              rgb_mix
                              (SElem &bg,SElem &fg,float alpha,SElem &targ);
  static  void              rgb_mix_zerotrans
                              (SElem &bg,SElem &fg,float alpha,SElem &targ);
  static DataSource         nullSource();
};

typedef uint64_t TightSElemInternType;

class SIMU_API TightSElem : public SElem {
private:
  TightSElemInternType* SElemData;
public:
                    TightSElem(SElem::DataSource);
                    TightSElem(const TightSElem&);
  virtual const std::string&
                    name() const override;
  void              source(SElem::DataSource) override;
  SElem::DataSource source() override;
  SElem&            rgb
                  (SElem::Precision,SElem::Precision,SElem::Precision) override;
  uint32_t          rgba() const override;
  SElem&            rgba(uint32_t) override;
  SElem::Precision  red() const override;
  SElem&            red(SElem::Precision) override;
  SElem::Precision  green() const override;
  SElem&            green(SElem::Precision) override;
  SElem::Precision  blue() const override;
  SElem&            blue(SElem::Precision) override;
  SElem::Precision  intensity() const override;
  SElem&            intensity(SElem::Precision) override;
  SElem::Precision  intensityUnsigned() const override;
  SElem::Precision_Signed
                    intensitySigned() override;
  SElem::Precision  segment() const override;
  SElem&            segment(SElem::Precision) override;
  bool              isValid() const override;
  SElem&            isValid(bool) override;
  bool              isSigned() const override;
  SElem&            isSigned(bool) override;
  SElem::Precision  zero() const override;
  void              clear() override;
  SElem::Precision  size() const override;
  TightSElem &      operator= (const TightSElem&);
  TightSElem        operator+ (const TightSElem &);
  TightSElem        operator- (const TightSElem &);
  TightSElem        operator* (const TightSElem &);
  SElem*            New(SElem::DataSource data = nullptr) override;
};

class SIMU_API BW8SElem : public SElem {
protected:
  uint8_t*                  SElemData;
public:
                            BW8SElem(SElem::DataSource);
                            BW8SElem(const BW8SElem&);
  virtual
  const std::string&        name() const override;
  virtual void              source(SElem::DataSource) override;
  virtual SElem::DataSource source() override;
  virtual SElem&            rgb
                            (SElem::Precision,
                             SElem::Precision,SElem::Precision) override;
  virtual uint32_t          rgba() const override;
  virtual SElem&            rgba(uint32_t) override;
  virtual SElem::Precision  red() const override;
  virtual SElem&            red(SElem::Precision) override;
  virtual SElem::Precision  green() const override;
  virtual SElem&            green(SElem::Precision) override;
  virtual SElem::Precision  blue() const override;
  virtual SElem&            blue(SElem::Precision) override;
  virtual SElem::Precision  intensity() const override;
  virtual SElem&            intensity(SElem::Precision) override;
  virtual SElem::Precision  intensityUnsigned() const override;
  virtual SElem::Precision_Signed
                            intensitySigned() override;
  virtual SElem::Precision  segment() const;
  virtual SElem&            segment(SElem::Precision) override;
  virtual bool              isValid() const override;
  virtual SElem&            isValid(bool) override;
  virtual bool              isSigned() const override;
  virtual SElem&            isSigned(bool) override;
  virtual SElem::Precision  zero() const override;
  virtual SElem::Precision  size() const override;
  SElem*                    New(SElem::DataSource data = nullptr) override;
  virtual SElem &           operator= (const SElem &) override;
};

class SIMU_API BW16SElem : public SElem {
protected:
  uint16_t*               SElemData;
public:
                            BW16SElem(SElem::DataSource);
                            BW16SElem(const BW16SElem&);
  virtual
  const std::string&        name() const override;
  virtual void              source(SElem::DataSource) override;
  virtual SElem::DataSource source() override;
  virtual SElem&            rgb
                            (SElem::Precision,
                             SElem::Precision,SElem::Precision) override;
  virtual uint32_t          rgba() const override;
  virtual SElem&            rgba(uint32_t) override;
  virtual SElem::Precision  red() const override;
  virtual SElem&            red(SElem::Precision) override;
  virtual SElem::Precision  green() const override;
  virtual SElem&            green(SElem::Precision) override;
  virtual SElem::Precision  blue() const override;
  virtual SElem&            blue(SElem::Precision) override;
  virtual SElem::Precision  intensity() const override;
  virtual SElem&            intensity(SElem::Precision) override;
  virtual SElem::Precision  intensityUnsigned() const override;
  virtual SElem::Precision_Signed
                            intensitySigned() override;
  virtual SElem::Precision  segment() const override;
  virtual SElem&            segment(SElem::Precision) override;
  virtual bool              isValid() const override;
  virtual SElem&            isValid(bool) override;
  virtual bool              isSigned() const override;
  virtual SElem&            isSigned(bool) override;
  virtual SElem::Precision  zero() const override;
  virtual SElem::Precision  size() const override;
  SElem*                    New(SElem::DataSource data = nullptr) override;
  virtual SElem &           operator= (const SElem &) override;
};

class SIMU_API BW16SignedSElem : public BW16SElem {
public:
                           BW16SignedSElem(SElem::DataSource);
                           BW16SignedSElem(const BW16SignedSElem&);
  virtual
  const std::string&       name() const override;
  virtual bool             isSigned() const override;
  virtual SElem::Precision zero() const override;
  SElem*                   New(SElem::DataSource data = nullptr) override;
  virtual SElem::Precision intensityUnsigned() const override;
  virtual SElem &          operator= (const SElem &) override;
};

class SIMU_API RGBAI32SElem : public SElem {
private:
  uint32_t                 *SElemData;
public:
                            RGBAI32SElem(SElem::DataSource);
                            RGBAI32SElem(const RGBAI32SElem&);
  virtual
  const std::string&        name() const override;
  virtual void              source(SElem::DataSource) override;
  virtual SElem::DataSource source() override;
  virtual SElem&            rgb
                            (SElem::Precision,
                             SElem::Precision,SElem::Precision) override;
  virtual uint32_t          rgba() const override;
  virtual SElem&            rgba(uint32_t) override;
  virtual SElem::Precision  red() const override;
  virtual SElem&            red(SElem::Precision) override;
  virtual SElem::Precision  green() const override;
  virtual SElem&            green(SElem::Precision) override;
  virtual SElem::Precision  blue() const override;
  virtual SElem&            blue(SElem::Precision) override;
  virtual SElem::Precision  alpha() const override;
  virtual SElem&            alpha(SElem::Precision) override;
  virtual SElem::Precision  intensity() const override;
  virtual SElem&            intensity(SElem::Precision) override;
  virtual SElem::Precision  intensityUnsigned() const override;
  virtual SElem::Precision_Signed
                            intensitySigned() override;
  virtual SElem::Precision  segment() const override;
  virtual SElem&            segment(SElem::Precision) override;
  virtual bool              isValid() const override;
  virtual SElem&            isValid(bool) override;
  virtual bool              isSigned() const override;
  virtual SElem&            isSigned(bool) override;
  virtual SElem::Precision  zero() const override;
  virtual SElem::Precision  size() const override;
  virtual void              clear() override;
  SElem*                    New(SElem::DataSource data = nullptr) override;
  virtual SElem &           operator= (const SElem &) override;
};

/*------------------------------------------------------------------------------
 *                          Coordinates Classes
 * ---------------------------------------------------------------------------*/

class SCoordinate;

class SIMU_API SVector {
public:
  typedef BaseFP  Precision;
                  SVector();
                  SVector(unsigned);
                  SVector(const SVector&);
                  SVector(SCoordinate&);
                  SVector(const SCoordinate&);
                  SVector(std::initializer_list<SVector::Precision>);
  virtual        ~SVector();
  void            reset();
  unsigned        getDim() const;
  void            setDim(unsigned);
  SVector::Precision&
                  operator[] (unsigned);
  const SVector::Precision&
                  operator[] (unsigned) const;
  SVector &       operator= (const SVector &);
  SVector &       operator+=(const SVector &);
  SVector &       operator-=(const SVector &);
  SVector         operator+ (const SVector &) const;
  SVector         operator- (const SVector &) const;
  bool            operator==(const SVector &) const;
  bool            operator!=(const SVector &) const;
  bool            operator< (const SVector &) const;
  SVector &       operator*=(const SVector::Precision);
  SVector         operator* (const SVector::Precision) const;
  SVector &       operator*=(const SVector &);
  SVector         operator* (const SVector &) const;
  SVector &       operator/=(const SVector::Precision);
  SVector         operator/ (const SVector::Precision) const;
  SVector &       operator/=(const SVector &);
  SVector         operator/ (const SVector &) const;
  SVector &       operator+=(const SVector::Precision);
  SVector         operator+ (const SVector::Precision) const;
  SVector         operator! () const;
  SVector::Precision
                  dot(const SVector&) const;
  SVector         cross(const SVector&) const;
  SVector         unit() const;
  SVector::Precision
                  mag() const;
  SVector::Precision
                  sum() const;
  double          volume() const;
  SVector         fabs() const;
  SVector::Precision
                  x() const;
  SVector::Precision
                  y() const;
  SVector::Precision
                  z() const;
  SVector&        x(const SVector::Precision);
  SVector&        y(const SVector::Precision);
  SVector&        z(const SVector::Precision);
  SVector&        xy(const SVector::Precision,const SVector::Precision);
  SVector&        xyz(const SVector::Precision,const SVector::Precision,
                      const SVector::Precision);
  std::string     toString() const;
  bool            withinSpace(const SVector&) const;
  bool            isOrtho() const;
  void            promote();
  void            append(const SVector&);
private:
  std::vector<SVector::Precision> Scalars;
};

// Convenience types:
typedef SVector SV;
typedef SVector SPoint;

class SIMU_API SMatrix {
protected:
  std::vector<SVector> Rows;
public:
           SMatrix   (unsigned rows,unsigned columns);
           SMatrix   (const SMatrix&);
  virtual ~SMatrix   ();
  unsigned rows      () const;
  unsigned columns   () const;
  const SVector&
           row       (unsigned) const;
  SVector  column    (unsigned) const;
  void     row       (unsigned, const SVector&);
  void     column    (unsigned, const SVector&);
  SMatrix& operator= (const SMatrix&);
  bool     operator==(const SMatrix&) const;
  SMatrix  operator+ (const SMatrix&);
  SMatrix& operator+=(const SMatrix&);
  SMatrix  operator- (const SMatrix&);
  SMatrix& operator-=(const SMatrix&);
  SMatrix  operator* (const SMatrix&);
  SMatrix& operator*=(const SMatrix&);
  SMatrix  operator+ (const SVector::Precision);
  SMatrix& operator+=(const SVector::Precision);
  SMatrix  operator- (const SVector::Precision);
  SMatrix& operator-=(const SVector::Precision);
  SMatrix  operator* (const SVector::Precision);
  SMatrix& operator*=(const SVector::Precision);
  SMatrix  operator/ (const SVector::Precision);
  SMatrix& operator/=(const SVector::Precision);
  SVector::Precision&
           operator[](std::initializer_list<unsigned>);
  const SVector::Precision&
           operator[](std::initializer_list<unsigned>) const;
  std::string
            toString  ();
};

// Convenience types:
typedef SMatrix SM;

class SIMU_API SComplex {
private:
  SVector::Precision real, imaginary;
public:
                    SComplex(SVector::Precision reali,
                             SVector::Precision imaginaryi);
                    SComplex(const SComplex&);
  virtual          ~SComplex();
  SVector::Precision mag() const;
  SVector::Precision r() const;
  SVector::Precision i() const;
  void              r(SVector::Precision);
  void              i(SVector::Precision);
  SComplex          conj() const;
  SComplex&         operator= (const SComplex&);
  bool              operator==(const SComplex&) const;
  SComplex          operator+ (const SComplex&) const;
  SComplex&         operator+=(const SComplex&);
  SComplex          operator- (const SComplex&) const;
  SComplex&         operator-=(const SComplex&);
  SComplex          operator* (const SComplex&) const;
  SComplex&         operator*=(const SComplex&);
  SComplex          operator/ (const SComplex&) const;
  SComplex&         operator/=(const SComplex&);
  SComplex          operator+ (SVector::Precision) const;
  SComplex&         operator+=(SVector::Precision);
  SComplex          operator- (SVector::Precision) const;
  SComplex&         operator-=(SVector::Precision);
  SComplex          operator* (SVector::Precision) const;
  SComplex&         operator*=(SVector::Precision);
  SComplex          operator/ (SVector::Precision) const;
  SComplex&         operator/=(SVector::Precision);
  std::string       toString  () const;
};

class SIMU_API SPlane {
private:
  SVector Point,Normal;
public:
                 SPlane();
                 SPlane(SVector point,SVector normal);
                 SPlane(const SPlane&);
  virtual       ~SPlane();
  const SVector& point() const;
  void           point(const SVector&);
  const SVector& normal() const;
  void           normal(const SVector&);
  bool           check() const;
  void           reset();
  bool           operator==(const SPlane &) const;
  SPlane &       operator= (const SPlane &);
};

class SIMU_API SLine {
private:
  SVector Start,End;
  bool    Definite;
public:
                 SLine();
                 SLine(SVector start,SVector end);
                 SLine(const SLine&);
  virtual       ~SLine();
  SLine &        operator= (const SLine &);
  bool           definite() const;
  void           definite(bool);
  const SVector& start() const;
  void           start(const SVector&);
  const SVector& end() const ;
  void           end(const SVector&);
  SVector        unit() const;
  bool           check() const;
  void           reset();
};

class SIMU_API SGeom {
public:
  struct intersection_res {
    bool intersects, parallel;
    SPoint intersection;
  };
  static const SVector::Precision Epsilon;
  static bool isEqual(SVector::Precision, SVector::Precision,
                      SVector::Precision epsilon = Epsilon);
  static SVector::Precision fabs(SVector::Precision);
  static long unsigned     abs(long int);
  static SVector::Precision sround(SVector::Precision);
  static SVector::Precision sfloor(SVector::Precision);
  static SVector::Precision sceil(SVector::Precision);
  static intersection_res  lineplaneintersect
                            (const SLine& sourceline,const SPlane& targetplane);
  static SLine             planeplaneintersection(const SPlane&, const SPlane&);
};

class SIMU_API SCoordinate {
public:
  typedef BaseIntSign Precision;
                   SCoordinate();
                   SCoordinate(SCoordinate::Precision);
                   SCoordinate(const SCoordinate&);
                   SCoordinate(std::initializer_list<SCoordinate::Precision>);
                  ~SCoordinate();
  void             reset();
  SCoordinate::Precision getDim() const;
  void             setDim(SCoordinate::Precision);
  SCoordinate::Precision popDim();
  void             promote();
  SCoordinate::Precision getCoord(SCoordinate::Precision) const;
  void             setCoord(SCoordinate::Precision,SCoordinate::Precision);
  void             append(const SCoordinate&);
  SCoordinate::Precision volume() const;
  SCoordinate::Precision toLinear(const SCoordinate &constraint) const;
  void             fromLinear(SCoordinate::Precision, const SCoordinate& constraint);
  std::string      toString() const;
  bool             tabincrement(const SCoordinate&);
  bool             tabdecrement(const SCoordinate&);
  /* Simple shortcut methods to access common dimensions */
  SCoordinate::Precision x() const;
  SCoordinate &    x(SCoordinate::Precision);
  SCoordinate::Precision y() const;
  SCoordinate &    y(SCoordinate::Precision);
  SCoordinate::Precision z() const;
  SCoordinate &    z(SCoordinate::Precision);
  SCoordinate::Precision t() const;
  SCoordinate &    t(SCoordinate::Precision);
  SCoordinate &    xy(SCoordinate::Precision,SCoordinate::Precision);
  SCoordinate &    xyz(SCoordinate::Precision,SCoordinate::Precision,SCoordinate::Precision);
  SCoordinate::Precision&
                   operator[](const SCoordinate::Precision);
  const SCoordinate::Precision&
                   operator[](const SCoordinate::Precision) const;
  SCoordinate &    operator= (const SCoordinate &);
  SCoordinate &    operator+=(const SCoordinate &);
  SCoordinate &    operator-=(const SCoordinate &);
  SCoordinate &    operator*=(const SCoordinate &);
  SCoordinate      operator+ (const SCoordinate &) const;
  SCoordinate      operator- (const SCoordinate &) const;
  SCoordinate      operator* (const SCoordinate &) const;
  bool             operator==(const SCoordinate &) const;
  bool             operator!=(const SCoordinate &) const;
  SCoordinate &    clamp_floor  (const SVector&);
  SCoordinate &    clamp_ceiling(const SVector&);
  SCoordinate &    clamp_round  (const SVector&);
  bool             withinSpace(const SCoordinate&) const;
  std::vector<SCoordinate::Precision>
                   precalcLinearIndices() const;
  SCoordinate::Precision toLinear(const std::vector<SCoordinate::Precision>&) const;
  std::vector< std::pair < SCoordinate, SCoordinate > >
                   decompose(const SCoordinate&) const;
private:
  std::vector<SCoordinate::Precision>
                   Coords;

};

// Convenience types:
typedef SCoordinate SC;

/*------------------------------------------------------------------------------
 *                         ---------END---------
 * ---------------------------------------------------------------------------*/

} //end namespace
#endif //SIMULACRUM_PRIMITIVES
