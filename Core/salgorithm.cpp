/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SAlgorithm:
 * Generic algorithm implementation
 * Author: M.A.Hicks
 */

#include "salgorithm.h"
#include <Core/error.h>
#include <Toolbox/SLua/slua.h>

using namespace Simulacrum;

class SAlgorithm::SAlgorithmPIMPL {
public:
  SCoordinate          BlockSize;
  bool                 DriverIn;
  int                  Driver;
  std::vector<SSpace*> Inputs;
  std::vector<SSpace*> Outputs;
  SAlgorithmPIMPL(): DriverIn(false), Driver(0) {}
};

SAlgorithm::SAlgorithm(): PrivateData(new SAlgorithmPIMPL()) {

}

SAlgorithm::~SAlgorithm() {
  delete PrivateData;
}

std::string SAlgorithm::typeString() {
  return typeid(SAlgorithm).name();
}

int SAlgorithm::inputs() const {
  return PrivateData->Inputs.size();
}

int SAlgorithm::outputs() const {
  return PrivateData->Outputs.size();
}

void SAlgorithm::setInput(int pos , SSpace& sref, bool doconnect) {
  if (pos < inputs()) {
    if (doconnect && (PrivateData->Inputs[pos] != nullptr))
      sdisconnect(*(PrivateData->Inputs[pos]));
    PrivateData->Inputs[pos] = &sref;
    if (doconnect)
      sconnect(sref);
  }
  else
    throw SAlgorithmException();
}

void SAlgorithm::setOutput(int pos, SSpace& sref, bool doconnect) {
  if (pos < outputs()) {
    if (doconnect && (PrivateData->Outputs[pos] != nullptr))
      PrivateData->Outputs[pos]->sdisconnect(*this);
    PrivateData->Outputs[pos] = &sref;
    if (doconnect)
      sref.sconnect(*this);
  }
  else
    throw SAlgorithmException();
}

void SAlgorithm::reset() {
  int tmpsize = PrivateData->Inputs.size();
  for (unsigned i=0; i<PrivateData->Inputs.size();i++)
    if (PrivateData->Inputs[i] != nullptr)
      sdisconnect(*(PrivateData->Inputs[i]));
  PrivateData->Inputs.clear();
  PrivateData->Inputs.resize(tmpsize,nullptr);
  tmpsize = PrivateData->Outputs.size();
  for (unsigned i=0; i<PrivateData->Outputs.size();i++)
    if (PrivateData->Outputs[i] != nullptr)
      PrivateData->Outputs[i]->sdisconnect(*this);
  PrivateData->Outputs.clear();
  PrivateData->Outputs.resize(tmpsize,nullptr);
  PrivateData->BlockSize.reset();
  setDriver(driver_t::Input,0);
  init();
}

bool SAlgorithm::isReady() const {
  bool ready = true;
  for (unsigned i=0; i<PrivateData->Inputs.size();i++)
    if (PrivateData->Inputs[i] == nullptr)
      ready = false;
  for (unsigned i=0; i<PrivateData->Outputs.size();i++)
    if (PrivateData->Outputs[i] == nullptr)
      ready = false;
  return ready;
}

SSpace& SAlgorithm::operator()(std::initializer_list
                                < std::reference_wrapper<SSpace> > spaces,
                               bool doconnect) {
  if ((long int)spaces.size() != (inputs() + outputs()))
    throw SAlgorithmException();
  else {
    for (int i=0; (i<inputs())&&(i<(int)spaces.size());i++)
      setInput(i,*(spaces.begin()+i),doconnect);
    for (int i=0; (i<outputs())&&(i<(int)spaces.size());i++)
      setOutput(i,*(spaces.begin()+(inputs()+i)),doconnect);
  }
  return execute();
}

SSpace& SAlgorithm::execute() {
  if (isReady()) {
    preExecute();
    doExecute();
    postExecute();
    if (outputs() > 0)
      return *(PrivateData->Outputs[0]);
    else {
      if (inputs() > 0)
        return *(PrivateData->Inputs[0]);
      else
        throw SAlgorithmException();
    }
  } else
      throw SAlgorithmException();
}

void SAlgorithm::refresh(bool deep) {
  if (deep)
    execute();
  SConnectable::refresh(deep);
}

void SAlgorithm::setInputs(int newin) {
  PrivateData->Inputs.resize(newin,nullptr);
}

void SAlgorithm::setOutputs(int newout) {
  PrivateData->Outputs.resize(newout,nullptr);
}

SSpace& SAlgorithm::input(int inputval) const {
  if (inputval >= inputs())
    throw SAlgorithmException();
  else
    return *(PrivateData->Inputs[inputval]);
}

SSpace& SAlgorithm::output(int outputval) const {
  if (outputval >= outputs())
    throw SAlgorithmException();
  else
    return *(PrivateData->Outputs[outputval]);
}

SSpace& SAlgorithm::driver() {
  return *(PrivateData->DriverIn ? PrivateData->Inputs[PrivateData->Driver] :
                                   PrivateData->Outputs[PrivateData->Driver]);
}

void SAlgorithm::setDriver(driver_t drvin, int drvnum) {
  PrivateData->DriverIn = drvin;
  PrivateData->Driver   = drvnum;
}

void SAlgorithm::setBlockSize(const SCoordinate& newbs) {
  PrivateData->BlockSize = newbs;
}

const SCoordinate& SAlgorithm::blocksize() const {
  return PrivateData->BlockSize;
}

void SAlgorithm::preExecute() {

}

void SAlgorithm::postExecute() {

}

//------------------------------------------------------------------------------

class SAlgorithmCPU::SAlgorithmCPUPIMPL {
public:
    SPool        GCD;
    std::vector< std::pair<SSpaceIterator*,SSpaceIterator*> >
                 IteratorStore;
};

SAlgorithmCPU::SAlgorithmCPU(): PrivateData(new SAlgorithmCPUPIMPL()) {

}

SAlgorithmCPU::~SAlgorithmCPU() {
  for (unsigned i=0; i< PrivateData->IteratorStore.size(); i++) {
    delete PrivateData->IteratorStore[i].first;
    delete PrivateData->IteratorStore[i].second;
  }
  PrivateData->IteratorStore.clear();
  delete PrivateData;
}

void SAlgorithmCPU::bg(SPool::SFunction newjob) {
  PrivateData->GCD.addJob(newjob);
}

void SAlgorithmCPU::bgWait() {
  PrivateData->GCD.wait();
}

void SAlgorithmCPU::doExecute() {
  // first generate the possible sub-regions for execution
  if (blocksize().volume() == 0) {
    // no blocking, no threading, just a simple algorithm
    SSpaceIterator* begin = new SSpaceIteratorCompat(driver());
    SSpaceIterator* end   = new SSpaceIteratorCompat(driver());
    end->toEnd();
    PrivateData->IteratorStore.push_back(std::make_pair(begin,end));
  }
  else if (blocksize().volume() == 1) {
    // this is a selem-parallel algorithm; use fast iterators
    // -> chop up along highest-rank dimension
    SCoordinate::Precision stride   = 1;
    int totaldims             = driver().extent().getDim();
    SCoordinate::Precision dimtouse = totaldims-1;
    if (dimtouse > 0) {
      // shift back the dimension if the last dimension is meaningless
      if ((driver().extent()[dimtouse] == 1) && (dimtouse > 0))
        dimtouse--;
      SCoordinate subber(totaldims);
      for (SCoordinate::Precision d=0; d<totaldims; d++)
        subber[d] = 1;
      SCoordinate::Precision dimsize = driver().extent()[dimtouse]-1;
      if (dimsize > PrivateData->GCD.poolSize())
        stride = dimsize / PrivateData->GCD.poolSize();
      // create the range spanning fast interators
      for (SCoordinate::Precision pos=0; pos<=dimsize; pos+=stride) {
        SCoordinate starter(totaldims);
        SCoordinate ender  (driver().extent());
        starter[dimtouse] = pos;
        ender -= subber;
        if ( (pos + stride) <= dimsize )
          ender[dimtouse] = pos + (stride-1);
        else {
          ender[dimtouse] = dimsize;
          pos = dimsize + 1; // end loop
        }
        SSpaceIterator* begin = driver().getFastNativeIteratorP();
        SSpaceIterator* end   = driver().getFastNativeIteratorP();
        begin->toCoord(starter);
        end->toCoord(ender);
        PrivateData->IteratorStore.push_back(std::make_pair(begin,end));
        if ( (pos < dimsize) && ((pos+stride) > dimsize))
          stride = dimsize - pos;
      }
    }
  }
  else {
    // this is a block-restricted algorithm
    std::vector< std::pair < SCoordinate, SCoordinate > > decomposedspaces
                                  = driver().extent().decompose(blocksize());
    for (unsigned i =0; i < decomposedspaces.size(); i++) {
      SSpaceIterator *newiter = new SSpaceIteratorCompatConstrained
                (driver(),decomposedspaces[i].first,decomposedspaces[i].second);
      SSpaceIterator *newiterend = new SSpaceIteratorCompat(driver());
      newiterend->toCoord(decomposedspaces[i].first +
                          decomposedspaces[i].second);
      PrivateData->IteratorStore.push_back(std::make_pair(newiter,newiterend));
    }
  }
  // push all instances of the kernel onto the pool
  for (unsigned i=0; i< PrivateData->IteratorStore.size(); i++){
    bg(std::bind(&SAlgorithmCPU::kernel,this,
                 PrivateData->IteratorStore[i].first,
                 PrivateData->IteratorStore[i].second));
  }
  // wait for them all to finish
  bgWait();
  // cleanup iterators immediately
  for (unsigned i=0; i< PrivateData->IteratorStore.size(); i++){
    delete PrivateData->IteratorStore[i].first;
    delete PrivateData->IteratorStore[i].second;
  }
  PrivateData->IteratorStore.clear();
}

//------------------------------------------------------------------------------

class SAlgorithmSLua::SAlgorithmSLuaPIMPL {
public:
  std::string SLuaAlgorithimSource;
};

std::string SAlgorithmSLua::userAlgorithmsPath() {
  return "Algorithms";
}

SAlgorithm* SAlgorithmSLua::New() {
  return new SAlgorithmSLua(PrivateData->SLuaAlgorithimSource);
}

SAlgorithmSLua::SAlgorithmSLua(const std::string& src):
                                        PrivateData(new SAlgorithmSLuaPIMPL()) {
  PrivateData->SLuaAlgorithimSource = src;
}

SAlgorithmSLua::~SAlgorithmSLua() {
  delete PrivateData;
}

void SAlgorithmSLua::lsetInputs(int num) {
  setInputs(num);
}

void SAlgorithmSLua::lsetOutputs(int num) {
  setOutputs(num);
}

SSpace& SAlgorithmSLua::linput(int num) {
  return input(num);
}

SSpace& SAlgorithmSLua::loutput(int num) {
  return output(num);
}

SSpace& SAlgorithmSLua::ldriver() {
  return driver();
}

void SAlgorithmSLua::lsetDriver(driver_t inout, int num) {
  setDriver(inout,num);
}

void SAlgorithmSLua::lsetBlockSize(const SCoordinate& blocks) {
  setBlockSize(blocks);
}

void SAlgorithmSLua::init() {
  SLua SLuaHandler(PrivateData->SLuaAlgorithimSource);
  // seek method
  SLuaHandler.pos("init");
  // push parameters
  SLuaHandler.push
  (SLVariant(SLuaHandler.queryType("Simulacrum::SAlgorithmSLua *"),this,false));
  // make call
  SLuaHandler.exec();
}

void SAlgorithmSLua::kernel(SSpaceIterator *beginc,SSpaceIterator *endc) {
  SLua SLuaHandler(PrivateData->SLuaAlgorithimSource);
  // seek method
  SLuaHandler.pos("init");
  // push parameters
  SLuaHandler.push(SLVariant(SLuaHandler.queryType
                                  ("Simulacrum::SAlgorithmSLua *"),this,false));
  SLuaHandler.push(SLVariant(SLuaHandler.queryType
                                ("Simulacrum::SSpaceIterator *"),beginc,false));
  SLuaHandler.push(SLVariant(SLuaHandler.queryType
                                ("Simulacrum::SSpaceIterator *"),endc,false));
  // make call
  SLuaHandler.exec();
}
