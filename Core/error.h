/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Error:
 * Various methods and exceptions for errors, and debugging.
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_ERROR
#define SIMULACRUM_ERROR

#include <exception>
#include <iostream>
#include "definitions.h"

namespace Simulacrum {

/* ------------------------Exceptions------------------------------------------
 * General exceptions which should be thrown where possible
 */
class SimulacrumException: public std::exception {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: General Exception";
  }
  virtual bool isFatal(){
    return true;
  }
};

class SimulacrumExceptionGeneric : public SimulacrumException
{
   std::string s;
   const char* what() const throw() { return s.c_str(); }
public:
    SimulacrumExceptionGeneric(std::string ss) : s(ss) {}
   ~SimulacrumExceptionGeneric() throw () {}
};

class SimulacrumIOException: public SimulacrumException {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: IO Exception";
  }
};

class SimulacrumMemException: public SimulacrumException {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: Memory Exception";
  }
};

class SimulacrumBoundsException: public SimulacrumException {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: SSpace Bounds Exception";
  }
};

class SimulacrumSSpaceException: public SimulacrumException {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: General SSpace Exception";
  }
};

class SAlgorithmException: public SimulacrumException {
public:
  virtual const char* what() const throw(){
    return "Simulacrum: SAlgorithm error";
  }
  virtual bool isFatal(){
    return true;
  }
};


/* -------------------------Error Handling-----------------------------------*/
typedef int serror_t;
/* convert error code into boolean for success */
SIMU_API bool success(serror_t err_code);
/* convert error code into exception when needed, using message in exception */
SIMU_API void excepter(int err,const std::string& message);

/* -------------------------DEBuggery------------------------------------------
 * General exceptions which should be thrown where possible
 */

#ifdef DEBUG
//write debug output
  #define DEBUG_OUT(X) std::cerr << __FILE__ << ":" << __LINE__ << ":" \
  << std::endl << "  " << X << std::endl;
#else
//no debugging
  #define DEBUG_OUT(X)
#endif

/* ---------------------000000000000------------------------------------------*/

} //end namespace
#endif //SIMULACRUM_ERROR
