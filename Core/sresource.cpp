/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for Resources (abstract)
 * Author: M.A.Hicks
 */

#include "sresource.h"
#include <Toolbox/SLua/slua.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/error.h>
#include "NNode/nnode.h"
#include "sspace.h"

#include <string.h>

using namespace Simulacrum;

#ifndef NOZLIB
/*-----------------------------------------------------------------------------
 *  General zlib gubbins, mostly taken from example zpipe.c
 *----------------------------------------------------------------------------*/
#include <stdio.h>
#include <assert.h>
#include <External/zlib/zlib.h>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

#define CHUNK 262144

/* Compress from file source to file dest until EOF on source.
   def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files. */
static int zlib_deflate(FILE *source, FILE *dest, int level) {
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
static int zlib_inflate(FILE *source, FILE *dest) {
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit2(&strm, 32+MAX_WBITS);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
                __FALLTHROUGH__
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/*------------------------------------------------------------------------------
 * End of zlib gubbins
 *----------------------------------------------------------------------------*/

void zlib_throw(int retstat) {
  switch(retstat) {
    case Z_OK:
      break;
    case Z_MEM_ERROR:
      throw SimulacrumMemException();
      break;
    case Z_STREAM_ERROR:
      throw SimulacrumExceptionGeneric("ZLib: invalid compression level");
    case Z_VERSION_ERROR:
      throw SimulacrumExceptionGeneric("ZLib: zlib.h does not match library");
      break;
    case Z_ERRNO:
      throw SimulacrumExceptionGeneric("ZLib: I/O error");
      break;
    case Z_DATA_ERROR:
      throw SimulacrumExceptionGeneric("ZLib: invalid deflate data");
      break;
    default:
      break;
  }
}

void SResource::decompress(const std::string& target) const {
  FILE *in  = fopen(getLocation().c_str(),"rb");
  FILE *out = fopen(target.c_str(),"wb");
  SET_BINARY_MODE(in);
  SET_BINARY_MODE(out);
  int ret = zlib_inflate(in,out);
  fclose(in);
  fclose(out);
  zlib_throw(ret);
}

void SResource::compress(const std::string& target) const {
  FILE *in  = fopen(getLocation().c_str(),"rb");
  FILE *out = fopen(target.c_str(),"wb");
  SET_BINARY_MODE(in);
  SET_BINARY_MODE(out);
  int ret = zlib_deflate(in,out,Z_DEFAULT_COMPRESSION);
  fclose(in);
  fclose(out);
  zlib_throw(ret);
}

#else //no zlib
void SResource::decompress(const std::string&) const {
  throw SimulacrumExceptionGeneric("Not built with zlib support!");
}

void SResource::compress(const std::string&) const {
  throw SimulacrumExceptionGeneric("Not built with zlib support!");
}
#endif

// compress file identification info
static const unsigned int  Z_PREAMBLE_LENGTH = 2;
static const unsigned char Z_PREAMBLE[Z_PREAMBLE_LENGTH] = {0x1f,0x8b};

bool SResource::isCompressed() const {
  bool         res = false;
  std::fstream ZFile;
  char         readdata[Z_PREAMBLE_LENGTH];
  ZFile.open(getLocation(), std::ios::in | std::ios::binary);
  SFile pathtest(getLocation());
  if (ZFile.is_open() && (!pathtest.isDIR())) {
    /* Now check the magic post-preamble string */
    ZFile.read(readdata,Z_PREAMBLE_LENGTH);
    ZFile.close();
    if (strncmp(readdata,(const char*)Z_PREAMBLE,Z_PREAMBLE_LENGTH) == 0)
      res = true;
    else
      res = false;
  }
  return res;
}

struct SResource::SResourcePrivates {
  SResourceLoader *Loader;
};

SimulacrumLibrary::str_enc SResource::stringEncoding() {
  return SimulacrumLibrary::str_enc::UTF8;
}

void SResource::setStringEncoding(SimulacrumLibrary::str_enc) {
  throw SimulacrumExceptionGeneric("Resource does not implemented string \
encoding specification!");
}

std::string SResource::typeString() {
  return typeid(SResource).name();
}

void SResource::changeLocation(const std::string& newloc) {
  setLocation(newloc);
}

void SResource::load() {
  refresh();
}

void SResource::loadMissingData() {

}

const std::string SResource::getLocationPathID(const std::string&) {
  // if unimplemented, assume above methods are invariant in the resource
  return getLocation();
}

void SResource::doStop(bool) {

}

SURI SResource::URI() {
  return SURI(getLocation());
}

const std::string& SResource::resourceType() {
  static std::string resgen = "Generic Resource";
  return resgen;
}

SResourceLoader& SResource::loader() {
  return *(PrivateData->Loader);
}

void SResource::setLoader(SResourceLoader* newloader) {
  if (PrivateData->Loader != nullptr)
    delete PrivateData->Loader;
  if (newloader != nullptr)
    newloader->setResource(*this);
  PrivateData->Loader = newloader;
}

SResource::SResource() : PrivateData(new SResourcePrivates) {
  PrivateData->Loader = nullptr;
  setLoader(new SResourceLoader());
}

SResource::~SResource() {
  setLoader(nullptr);
  delete PrivateData;
}

void SResource::labelSSpace(SSpace& target, const std::string& path) {
  NNode resource;
  NNode resourcepath;
  resource.setName("SimulacrumResourceURI");
  resource.setData(getLocation());
  resourcepath.setName("SimulacrumResourcePath");
  resourcepath.setData(path);
  if (target.informationNode().hasChildNode(resource.getName())) {
    target.informationNode().getChildNode
                               (resource.getName()).setData(resource.getData());
  }
  else {
    target.informationNode().addChildNode(resource);
  }
  if (target.informationNode().hasChildNode(resourcepath.getName())) {
    target.informationNode().getChildNode
                       (resourcepath.getName()).setData(resourcepath.getData());
  }
  else {
    target.informationNode().addChildNode(resourcepath);
  }
}

void SResource::getSSpaceInto(SSpace&, const std::string&) {
  throw SimulacrumExceptionGeneric("SSpace Loading Unimplemented");
}

void SResource::putSSpaceInto(SSpace&, const std::string&) {
  throw SimulacrumExceptionGeneric("SSpace Storing Unimplemented");
}

void SResource::loadInto(SSpace& targ, SResource&& src, const std::string& uri,
                                                  const std::string& respath) {
  src.setLocation(uri);
  excepter(!src.isValid(),"Location not valid for type of resource");
  src.refresh(true); // for DICOM -- load full data
  excepter(!src.hasSSpace(respath),"No SSpace found in resource at \"" +
                                   respath + "\"");
  src.getSSpaceInto(targ,respath);
}

void SResource::storeInto(SSpace& src, SResource&& targ,const std::string& uri,
                                                  const std::string& respath) {
  targ.setLocation(uri);
  targ.putSSpaceInto(src,respath);
}

//------------------------------------------------------------------------------

struct SResourceSLua::SResourceSLuaPrivates {
  std::string LocationStore;
  NNode       RootNode;
  SLua        SLuaHandler;
};

SResourceSLua::SResourceSLua(const std::string& srcode):
                                    SLuaPrivateData(new SResourceSLuaPrivates) {
  SLuaPrivateData->SLuaHandler.loadMem(srcode);
  clear();
}

SResourceSLua::~SResourceSLua() {
  delete SLuaPrivateData;
}

int SResourceSLua::setLocation(const std::string& newpath) {
  getRoot().NodeValue(newpath);
  return 0;
}

const std::string& SResourceSLua::getLocation() const {
  SLuaPrivateData->LocationStore = SLuaPrivateData->RootNode.getData();
  return SLuaPrivateData->LocationStore;
}

void SResourceSLua::clear() {
  SLuaPrivateData->RootNode.clear();
  setLocation("");
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("clear");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

std::string SResourceSLua::getInfo(const std::string& path) {
  std::string result;
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("getInfo");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  SLuaPrivateData->SLuaHandler.push(path);
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return result;
}

SAbsTreeNode& SResourceSLua::getRoot() {
  return (SAbsTreeNode&) SLuaPrivateData->RootNode;
}

void SResourceSLua::getSSpaceInto(SSpace& target, const std::string& path) {
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("getSSpaceInto");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SSpace *"),&target,false));
  SLuaPrivateData->SLuaHandler.push(path);
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

bool SResourceSLua::hasArchive() {
  bool result = false;
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("hasArchive");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return result;
}

bool SResourceSLua::hasSSpace(const std::string& path) {
  bool result = false;
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("hasSSpace");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  SLuaPrivateData->SLuaHandler.push(path);
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return result;
}

bool SResourceSLua::isValid() const {
  bool result = false;
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("isValid");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
                                           queryType("Simulacrum::SResource *"),
                                       const_cast<SResourceSLua*>(this),false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return result;
}

void SResourceSLua::load() {
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("load");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

void SResourceSLua::store() {
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("store");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

void SResourceSLua::refresh(SConnectable::sdepth_t deep) {
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("refresh");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  SLuaPrivateData->SLuaHandler.push(deep);
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

std::string SResourceSLua::userResourcesPath() {
  return "Resources";
}

SimulacrumLibrary::str_enc SResourceSLua::stringEncoding() {
  int result = 0;
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("stringEncoding");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return (SimulacrumLibrary::str_enc) result;
}

void SResourceSLua::setStringEncoding(SimulacrumLibrary::str_enc encoding) {
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("setStringEncoding");
  // push parameters
  SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
             queryType("Simulacrum::SResource *"),this,false));
  SLuaPrivateData->SLuaHandler.push(encoding);
  // make call
  SLuaPrivateData->SLuaHandler.exec();
  SLuaPrivateData->SLuaHandler.unlock();
}

//------------------------------------------------------------------------------

struct SResourceLoader::SResourceLoaderPrivates {
  SResource *Source;
};

SResourceLoader::SResourceLoader(): PrivateData(new SResourceLoaderPrivates) {
  PrivateData->Source = nullptr;
}

SResourceLoader::~SResourceLoader() {
  delete PrivateData;
}

std::string SResourceLoader::userLoadersPath() {
  return "ResourceLoaders";
}

std::string SResourceLoader::name() {
  return "DefaultLoader";
}

void SResourceLoader::setResource(SResource& targ) {
  PrivateData->Source = &targ;
}

std::vector< std::string > SResourceLoader::sspaceList(const std::string& path){
  std::vector< std::string > result;
  if (PrivateData->Source != nullptr) {
    if (PrivateData->Source->hasSSpace(path) &&
        PrivateData->Source->getRoot().NodePathExists(path)) {
      result.push_back
                  (PrivateData->Source->getRoot().NodeByPath(path).NodeValue());
    }
    else {
      if (PrivateData->Source->getRoot().NodePathExists(path)) {
        SAbsTreeNode &srcnode = PrivateData->Source->getRoot().NodeByPath(path);
        SAbsTreeNodeList_t children = srcnode.NodeChildren(true,true);
        for (unsigned c=0; c<children.size(); c++) {
          SURI cpath(path);
          cpath.addComponentBack(children[c]->NodeValue());
          if (PrivateData->Source->hasSSpace(cpath.getURI(true)))
            result.push_back(children[c]->NodeValue());
        }
      }
    }
  }
  return result;
}

void SResourceLoader::getSSpaceInto(SSpace& target, const std::string& path,
                                    const std::string& name) {
  if ((PrivateData->Source != nullptr)) {
    if (PrivateData->Source->getRoot().NodePathExists(path)) {
      SAbsTreeNode &srcnode = PrivateData->Source->getRoot().NodeByPath(path);
      if ((name == srcnode.NodeValue()) &&
           PrivateData->Source->hasSSpace(path)) {
        PrivateData->Source->getSSpaceInto(target,path);
      }
      else {
        SURI npath(path);
        npath.addComponentBack(name);
        if (srcnode.NodePathExists(name) &&
            PrivateData->Source->hasSSpace(npath.getURI(true))) {
          PrivateData->Source->getSSpaceInto(target,npath.getURI(true));
        }
        else {
          target.setName(name);
          SLogger::global().addMessage("SResourceLoader::getSSpaceInto: \
Request for path without SSpace - " + name);
        }
      }
    }
  }
}

std::string SResourceLoader::sspaceListTitle(const std::string& path) {
  return path;
}

std::string SResourceLoader::layoutRecommendation(const std::string&) {
  return std::string();
}

//------------------------------------------------------------------------------

struct SResourceSLuaLoader::SResourceSLuaLoaderPrivates {
  SLua SLuaHandler;
};

SResourceSLuaLoader::SResourceSLuaLoader(const std::string& srcode): 
                              SLuaPrivateData(new SResourceSLuaLoaderPrivates) {
  SLuaPrivateData->SLuaHandler.lock();
  SLuaPrivateData->SLuaHandler.exceptError(
                                 SLuaPrivateData->SLuaHandler.loadMem(srcode));
  SLuaPrivateData->SLuaHandler.unlock();
}

SResourceSLuaLoader::~SResourceSLuaLoader() {
  delete SLuaPrivateData;
}

std::string SResourceSLuaLoader::name() {
  std::string result = "UntitledLuaLoader";
  SLuaPrivateData->SLuaHandler.lock();
  // seek method
  SLuaPrivateData->SLuaHandler.pos("loaderName");
  // make call
  SLuaPrivateData->SLuaHandler.exceptError(
                                          SLuaPrivateData->SLuaHandler.exec());
  // get result
  SLuaPrivateData->SLuaHandler.pop(result);
  SLuaPrivateData->SLuaHandler.unlock();
  return result;
}

std::vector< std::string > SResourceSLuaLoader::sspaceList
                                                     (const std::string& path) {
  std::vector< std::string > result;
  if (PrivateData->Source != nullptr) {
    SLuaPrivateData->SLuaHandler.lock();
    // seek method
    SLuaPrivateData->SLuaHandler.pos("sspaceList");
    // push parameters
    SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
               queryType("Simulacrum::SResource *"),PrivateData->Source,false));
    SLuaPrivateData->SLuaHandler.push(path);
    SLuaPrivateData->SLuaHandler.push
    (SLVariant(SLuaPrivateData->SLuaHandler.
              queryType("std::vector<std::string> *"),&result,false));
    // make call
    SLuaPrivateData->SLuaHandler.exceptError(
                                          SLuaPrivateData->SLuaHandler.exec());
    SLuaPrivateData->SLuaHandler.unlock();
  }
  return result;
}

std::string SResourceSLuaLoader::sspaceListTitle(const std::string& path) {
  std::string result;
  if (PrivateData->Source != nullptr) {
    SLuaPrivateData->SLuaHandler.lock();
    // seek method
    SLuaPrivateData->SLuaHandler.pos("sspaceListTitle");
    // push parameters
    SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
               queryType("Simulacrum::SResource *"),PrivateData->Source,false));
    SLuaPrivateData->SLuaHandler.push(path);
    // make call
    SLuaPrivateData->SLuaHandler.exceptError(
                                          SLuaPrivateData->SLuaHandler.exec());
    // get result
    SLuaPrivateData->SLuaHandler.pop(result);
    SLuaPrivateData->SLuaHandler.unlock();
  }
  return result;
}

void SResourceSLuaLoader::getSSpaceInto(SSpace& target, const std::string& path,
                                        const std::string& sspacename) {
  if (PrivateData->Source != nullptr) {
    SLuaPrivateData->SLuaHandler.lock();
    // seek method
    SLuaPrivateData->SLuaHandler.pos("getSSpaceInto");
    // push parameters
    SLuaPrivateData->SLuaHandler.push(SLVariant(SLuaPrivateData->SLuaHandler.
               queryType("Simulacrum::SResource *"),PrivateData->Source,false));
    SLuaPrivateData->SLuaHandler.push(path);
    SLuaPrivateData->SLuaHandler.push(sspacename);
    SLuaPrivateData->SLuaHandler.push
                  (SLVariant(SLuaPrivateData->SLuaHandler.queryType
                             ("Simulacrum::SSpace *"),&target,false));
    // make call
    SLuaPrivateData->SLuaHandler.exceptError(
                                          SLuaPrivateData->SLuaHandler.exec());
    SLuaPrivateData->SLuaHandler.unlock();
  }
}

std::string SResourceSLuaLoader::layoutRecommendation(const std::string& path) {
  std::string result;
  if (PrivateData->Source != nullptr) {
    SLuaPrivateData->SLuaHandler.lock();
    // seek method
    SLuaPrivateData->SLuaHandler.pos("layoutRecommendation");
    // push parameters
    SLuaPrivateData->SLuaHandler.push
    (SLVariant(SLuaPrivateData->SLuaHandler.queryType("Simulacrum::SResource *"),
                PrivateData->Source,false));
    SLuaPrivateData->SLuaHandler.push(path);
    // make call
    SLuaPrivateData->SLuaHandler.exceptError(
                                          SLuaPrivateData->SLuaHandler.exec());
    // get result
    SLuaPrivateData->SLuaHandler.pop(result);
    SLuaPrivateData->SLuaHandler.unlock();
  }
  return result;
}
