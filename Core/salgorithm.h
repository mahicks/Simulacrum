/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SAlgorithm:
 * Generic algorithm specification
 * Author: M.A.Hicks
 */

#ifndef SALGORITHM
#define SALGORITHM

#include <vector>
#include <map>
#include <string>
#include <initializer_list>
#include <functional>

#include <Core/sspace.h>
#include <Core/sconnectable.h>
#include <Core/SPool/SPool.h>

namespace Simulacrum {

  class SIMU_API SAlgorithm: public SConnectable {
  public:
    enum         driver_t { Input = true, Output = false };
                 SAlgorithm     ();
    virtual     ~SAlgorithm     ();
    static
    std::string  typeString     ();
    virtual
    SAlgorithm*  New            ()=0;
    int          inputs         () const;
    int          outputs        () const;
    void         setInput       (int, SSpace&,bool doconnect = true);
    void         setOutput      (int, SSpace&,bool doconnect = true);
    void         reset          ();
    virtual
    bool         isReady        () const;
    virtual
    SSpace&      execute        ();
    SSpace&      operator()     (std::initializer_list
                                  < std::reference_wrapper<SSpace> >,
                                 bool doconnect = false);
    void         refresh        (bool) override;
  protected:
    void         setInputs      (int);
    void         setOutputs     (int);
    SSpace&      input          (int) const;
    SSpace&      output         (int) const;
    SSpace&      driver         ();
    void         setDriver      (driver_t,int);
    void         setBlockSize   (const SCoordinate&);
    const
    SCoordinate& blocksize      () const;
    virtual
    void         init           ()=0;
    virtual
    void         preExecute     ();
    virtual
    void         doExecute      ()=0;
    virtual
    void         postExecute    ();
  private:
    class SAlgorithmPIMPL;
    SAlgorithmPIMPL *PrivateData;
  };

  class SIMU_API SAlgorithmCPU: public SAlgorithm {
  public:
    virtual
    SAlgorithm*  New            () override =0;
                 SAlgorithmCPU  ();
    virtual     ~SAlgorithmCPU  ();
  protected:
    virtual
    void         init           () override =0;
    virtual
    void         kernel         (SSpaceIterator *beginc,
                                 SSpaceIterator *endc) =0;
    virtual
    void         doExecute      () override;
    void         bg             (SPool::SFunction);
    void         bgWait         ();
  private:
    class SAlgorithmCPUPIMPL;
    SAlgorithmCPUPIMPL *PrivateData;
  };

  class SIMU_API SAlgorithmSLua: public SAlgorithmCPU {
  public:
    static
    std::string  userAlgorithmsPath();
    virtual
    SAlgorithm*  New            () override;
                 SAlgorithmSLua (const std::string&);
    virtual     ~SAlgorithmSLua ();
    /* The following made public for Lua-wrapped access */
    void         lsetInputs     (int);
    void         lsetOutputs    (int);
    SSpace&      linput         (int);
    SSpace&      loutput        (int);
    SSpace&      ldriver        ();
    void         lsetDriver     (driver_t,int);
    void         lsetBlockSize  (const SCoordinate&);
  protected:
    virtual
    void         init           () override;
    virtual
    void         kernel         (SSpaceIterator *beginc,
                                 SSpaceIterator *endc) override;
  private:
    class SAlgorithmSLuaPIMPL;
    SAlgorithmSLuaPIMPL *PrivateData;
  };

}

#endif //SALGORITHM
