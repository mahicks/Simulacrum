/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for Resources (abstract)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_RESOURCE
#define SIMULACRUM_RESOURCE

#include "sabtree.h"
#include "definitions.h"
#include "slockable.h"
#include <Toolbox/SURI/suri.h>

#include <string>

namespace Simulacrum {

  class SSpace;
  class SResourceLoader;
  class SIMU_API SResource: public SConnectable {
  private:
    struct SResourcePrivates;
    SResourcePrivates *PrivateData;
  protected:
    void                       labelSSpace(SSpace&,const std::string& path);
  public:
    virtual SimulacrumLibrary::str_enc
                               stringEncoding();
    virtual void               setStringEncoding(SimulacrumLibrary::str_enc);
    static
    std::string                typeString();
    virtual const std::string& getLocation() const =0;
    virtual int                setLocation(const std::string&) =0;
    virtual void               changeLocation(const std::string&);
    virtual bool               isValid() const =0;
    virtual SAbsTreeNode&      getRoot()=0;
    virtual void               refresh(SConnectable::sdepth_t deep = false) override =0;
    virtual void               load();
    virtual void               loadMissingData();
    virtual void               store() =0;
    virtual std::string        getInfo(const std::string &path = "") =0;
    virtual bool               hasArchive() =0;
    virtual bool               hasSSpace(const std::string &path = "") =0;
    virtual void               getSSpaceInto(SSpace&,
                                             const std::string &path = "");
    virtual void               putSSpaceInto(SSpace&,
                                             const std::string &path = "");
    virtual const std::string  getLocationPathID(const std::string &path = "");
    virtual void               clear()=0;
    virtual void               doStop(bool);
    virtual SURI               URI();
    virtual const std::string& resourceType();
    virtual SResourceLoader&   loader();
    virtual void               setLoader(SResourceLoader*);
    bool                       isCompressed() const;
    void                       decompress(const std::string &target) const;
    void                       compress(const std::string &target) const;
                               SResource();
    virtual                   ~SResource();
    static  void               loadInto (SSpace& targ, SResource&& src,
                                         const std::string& uri,
                                         const std::string& respath = "");
    static  void               storeInto(SSpace& src, SResource&& targ,
                                         const std::string& uri,
                                         const std::string& respath = "");
  };

  class SIMU_API SResourceSLua : public SResource {
  protected:
    struct SResourceSLuaPrivates;
    SResourceSLuaPrivates *SLuaPrivateData;
  public:
                               SResourceSLua(const std::string& srcode);
    virtual                   ~SResourceSLua();
    virtual const std::string& getLocation() const override;
    virtual int                setLocation(const std::string&) override;
    virtual bool               isValid() const override;
    virtual SAbsTreeNode&      getRoot() override;
    virtual void               refresh(SConnectable::sdepth_t deep = false) override;
            void               load() override;
    virtual void               store() override;
    virtual std::string        getInfo(const std::string &path = "") override;
    virtual bool               hasArchive() override;
    virtual bool               hasSSpace(const std::string &path = "") override;
    virtual void               getSSpaceInto(SSpace&,
                                             const std::string &path = "") override;
    virtual void               clear() override;
    static  std::string        userResourcesPath();
    virtual SimulacrumLibrary::str_enc
                               stringEncoding() override;
    virtual void               setStringEncoding(SimulacrumLibrary::str_enc) override;
  };

  class SIMU_API SResourceLoader {
  protected:
    struct SResourceLoaderPrivates;
    SResourceLoaderPrivates *PrivateData;
  public:
                               SResourceLoader();
    virtual                   ~SResourceLoader();
    static  std::string        userLoadersPath();
    virtual std::string        name();
    virtual void               setResource(SResource&);
    virtual std::vector<std::string>
                               sspaceList(const std::string &path);
    virtual std::string        sspaceListTitle(const std::string &path);
    virtual void               getSSpaceInto(SSpace&,
                                             const std::string &path,
                                             const std::string& sspacename);
    virtual std::string        layoutRecommendation
                                                 (const std::string &path);
  };

  class SIMU_API SResourceSLuaLoader: public SResourceLoader {
  protected:
    struct SResourceSLuaLoaderPrivates;
    SResourceSLuaLoaderPrivates *SLuaPrivateData;
  public:
                               SResourceSLuaLoader(const std::string& srcode);
    virtual                   ~SResourceSLuaLoader();
    virtual std::string        name() override;
    virtual std::vector<std::string>
                               sspaceList(const std::string &path ) override;
    virtual std::string        sspaceListTitle(const std::string &path ) override;
    virtual void               getSSpaceInto(SSpace&,
                                             const std::string &path,
                                             const std::string& sspacename) override;
    virtual std::string        layoutRecommendation
                                                 (const std::string &path) override;
  };

}

#endif
