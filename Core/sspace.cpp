/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Images:
 * The core classes of the Simulacrum library. Deisgned to store
 * n-dimensional ' images', these classes include SElem sets, 
 * images, dimensions and coordinate transformations.
 * Author: M.A.Hicks
 */

#include "sspace.h"
#include "SPool/SPool.h"
#include "salgorithms.h"
#include <string.h>
#include "error.h"
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/sysInfo/sysInfo.h>

using namespace Simulacrum;

/*------------------------------------------------------------------------------
 *                             Image Classes
 * ---------------------------------------------------------------------------*/

namespace Simulacrum {

class SSpaceIteratorNativeFast: public SSpaceIterator {
  friend class SSpace;
public:
                                  SSpaceIteratorNativeFast
                                              (const SSpaceIteratorNativeFast&);
  virtual                        ~SSpaceIteratorNativeFast ();
  void                            toBegin        () override;
  bool                            isBegin        () override;
  void                            toEnd          () override;
  bool                            isEnd          () override;
  void                            toCoord        (const SCoordinate&) override;
  const SSpaceIteratorNativeFast  operator+      (SElemSet::Size) const;
  const SSpaceIteratorNativeFast& operator+=     (SElemSet::Size);
  const SSpaceIteratorNativeFast  operator-      (SElemSet::Size) const;
  const SSpaceIteratorNativeFast& operator-=     (SElemSet::Size);
  const SSpaceIteratorNativeFast& operator++     () override;
  const SSpaceIteratorNativeFast& operator--     () override;
  const SSpaceIteratorNativeFast& operator=      (const SSpaceIterator&) override;
  const SSpaceIteratorNativeFast& operator=      (const SSpaceIteratorNativeFast&);
  SElem&                          operator*      () const override;
  const
  SCoordinate&                    pos            () const override;
                                  SSpaceIteratorNativeFast (SSpace&);
private:
  SSpace                         *SourceSSpace;
  const SCoordinate              *Extent;
  SCoordinate                     CoordPosition;
  SElem                          *NativeSElem;
  SElemSet::Size              Position;
  void                            initNativeSElem();
  virtual
  void                            step(bool forwards, SElemSet::Size);
};

} // namespace Simulacrum

class SSpace::SSpacePIMPL {
public:
  SCoordinate       Extent;
  std::vector<SCoordinate::Precision>
                    ExtentIndicesCache;
  NNode             Information;
  bool              LockNativeSElem;
  SVector           Spacing;
  std::vector<std::string>
                    SpacingUnits;
  std::vector<DataContext*>
                    DataContexts;
  CONTEXTS_t        ActiveContext;
  std::function<SVector(SSpace*,const SVector&)>
                    ToGlobalSpaceFunc;
  std::function<SVector(SSpace*,const SVector&)>
                    FromGlobalSpaceFunc;
  std::string       GlobalSpaceID;
  std::vector< std::pair<SVector,std::string> >
                    VectorMeanings;
                    SSpacePIMPL(): LockNativeSElem(false) {}
};

SElemSet::Size SSpace::calcIndex(const SCoordinate& coordref) const {
  return coordref.toLinear(PrivateData->ExtentIndicesCache);
}

int SSpace::concatenate(std::vector<SSpace*>& spacelist) {
  int                error      = 0;
  SElemSet::Size datasize   = 0;
  bool               userealloc = sysInfo::preferRealloc();
  reset();
  if (spacelist.size() > 0) {
    progress(-1);
    SElem      *natpox  = spacelist[0]->selemDataStore().getNativeSElem();
    SCoordinate addext  = spacelist[0]->extent();
    // check each space for combine-ability
    for (unsigned i = 0; i < spacelist.size(); i++) {
      SElem::Ptr      newpox  = spacelist[i]->getNativeSElem();
      SCoordinate newext  = spacelist[i]->extent();
      if (newext != addext || (typeid(*natpox) != typeid(*newpox)) ) {
        error = 2; // unmatching SElem types
        break;
      }
      if (!userealloc) {
        datasize += spacelist[i]->selemDataStore().size();
      }
    }
    if (!error) {
      SElem::DataSource olddat = nullptr, ndata = nullptr;
      // concatenate the image data
      if (!userealloc) {
        ndata = (SElem::DataSource)malloc(datasize);
      }
      else {
        ndata = nullptr;
      }
      SElemSet::Size ndatapos = 0;
      spacelist[0]->copyInto(*this,false);
      for (unsigned i = 0; i < spacelist.size(); i++) {
        if (userealloc) {
        datasize += spacelist[i]->selemDataStore().size();
        olddat = ndata;
        ndata = (SElem::DataSource)realloc(ndata,datasize);
        }
        if (ndata == nullptr) {
          if (userealloc) {
            free(olddat);
          }
          progress(100);
          delete natpox;
          throw SimulacrumMemException();
        }
        memcpy(&(ndata[ndatapos]),spacelist[i]->selemDataStore().SElems(0),
                                      spacelist[i]->selemDataStore().size());
        ndatapos+=spacelist[i]->selemDataStore().size();
        spacelist[i]->reset();
      }
      // configure new extent
      SCoordinate finext;
      if (spacelist.size() > 1) {
        finext.setDim(spacelist[0]->extent().getDim() + 1);
        finext[spacelist[0]->extent().getDim()] = spacelist.size();
        for (SCoordinate::Precision i=0; i < finext.getDim()-1; i++)
          finext[i] = addext[i];
      }
      else
        finext = addext;
      setNativeSElemType(natpox);
      selemDataStore().useData(ndata,datasize,true);
      resize(finext);
    }
  }
  else
    error = 1; // no SSpaces provided
  progress(100);
  return error;
}

SSpace::SSpace() : PrivateData(new SSpacePIMPL()) {
  SCoordinate tmpExtent;
  reset();
  resize(tmpExtent);
}

SSpace::SSpace(const SCoordinate& extent): PrivateData(new SSpacePIMPL()) {
  reset();
  resize(extent);
}

SSpace::~SSpace(){
  reset();
  for (unsigned i=0; i<PrivateData->DataContexts.size();i++)
    removeContext(i);
  delete PrivateData;
}

SSpace::SSpace(SSpace& targ): PrivateData(new SSpacePIMPL()) {
  reset();
  *this = targ;
}

SSpace::SSpace(const SSpace&): PrivateData(new SSpacePIMPL()) {
  SCoordinate tmpExtent;
  reset();
  resize(tmpExtent);
}

SSpace* SSpace::New() {
  return new SSpace();
}

SSpace& SSpace::operator=(SSpace& source) {
  source.copyInto(*this);
  return *this;
}

SSpace& SSpace::operator=(const SSpace&) {
 // just a stub!
 return *this;
}

void SSpace::getRGBAScanlinesInto(SSpace* target, SCoordinate::Precision startrow,
                                  SCoordinate::Precision endrow,
                                  SCoordinate::Precision width,
                                  short unsigned int step,
                                  bool mergecontexts,
                                  float opacity,
                                  SCoordinate::Precision startcol,
                                  SCoordinate::Precision endcol,
                                  SCoordinate::Precision pixeloffset) {
  // balance the crop sizes
  if ( startcol % step != 0 )
    startcol -= startcol % step;
  if ( (width - endcol) % step != 0 )
    endcol += (width - endcol) % step;
  SCoordinate::Precision rowc              = width*startrow;
  SCoordinate::Precision rowc_out          = ((width/step)*(startrow/step));
  SCoordinate::Precision rowc_incr         = width*step;
  SCoordinate::Precision rowc_out_prestep  = startcol / step;
  SCoordinate::Precision rowc_out_poststep = (width - endcol) / step;
  RGBAI32SElem targetp(nullptr);
  SElem::Ptr sourcep = getNativeSElem();
  GPLUT& locallut = LUT();
  std::vector<SElem*> additcontexpox;
  std::vector<GPLUT*> additcontexlut;
  for (CONTEXTS_t c=1; (c < contexts()) && mergecontexts; c++) {
    additcontexpox.push_back(
                    PrivateData->DataContexts[c]->ContextData.getNativeSElem());
    additcontexlut.push_back(&(PrivateData->DataContexts[c]->ContextLUT));
  }
  for (SCoordinate::Precision y = startrow; y <= endrow-step; y+=step) {
    rowc_out += rowc_out_prestep;
    for (SCoordinate::Precision x = startcol; x <= endcol-step; x+=step) {
      targetp.source(target->selemDataStore().SElems(rowc_out++));
      sourcep->source(selemDataStore().SElems_Passive(rowc+x+pixeloffset));
      targetp.rgba(locallut.lookupRGBA(*sourcep));
      // now merge each additional context, if requested
      for (CONTEXTS_t c=1; (c < contexts()) && mergecontexts; c++) {
        additcontexpox[c-1]->source(PrivateData->DataContexts[c]->
                                ContextData.SElems_Passive(rowc+x+pixeloffset));
        SElem::rgb_mix_zerotrans(targetp,*additcontexpox[c-1],opacity,targetp);
      }
    }
    rowc_out += rowc_out_poststep;
    rowc += rowc_incr;
  }
  for (CONTEXTS_t c=0; c < additcontexpox.size(); c++)
    delete additcontexpox[c];
}

void SSpace::get2DRGBAInto(SSpace& target, short unsigned int downsample,
                           bool mergecontexts, float opacity,
                           SCoordinate* cropb, SCoordinate* crope,
                           SCoordinate::Precision zoffset) {
  SPool scanlinePool;
  SCoordinate::Precision pixeloffset = 0;
  target.reset();
  if (extent().getDim() < 2)
    return;
  SCoordinate::Precision xdim,ydim,crowstart,crowend ,cstartcol, cendcol;
  cendcol = xdim = extent().x();
  crowend = ydim = extent().y();
  crowstart = cstartcol = 0;
  // setup crop geometry
  if ( (cropb != nullptr) && (cropb->getDim() > 1)) {
    // columns
    if (cropb->x() < xdim && (cropb->x() > 0))
      cstartcol = cropb->x();
    if ((crope != nullptr) && (crope->getDim() > 1) &&
        (crope->x() < xdim) && (crope->x() > 0))
      cendcol = crope->x();
    // rows
    if (cropb->y() < ydim && (cropb->y() > 0))
      crowstart = cropb->y();
    if ((crope != nullptr) && (crope->getDim() > 1) &&
        (crope->y() < ydim) && (crope->y() > 0))
      crowend = crope->y();
  }
  if (!(xdim == 0 || ydim == 0)) {
    // determine an appropriate stepsize for the scanlinepool
    int poolSize                 = scanlinePool.poolSize();
    SCoordinate::Precision stepsize    = (crowend-crowstart) / poolSize;
    SCoordinate::Precision stepsizerem = 0;
    if (stepsize == 0) {
      stepsize    = 1;
      stepsizerem = 0;
    } else
      stepsizerem = (crowend-crowstart) % stepsize;
    // choose a sensible downsample
    if (downsample < 2)
      // clamp to 1
      downsample = 1;
    else {
      // ensure that stepsize divides evenly by downsample
      stepsize = downsample*(SGeom::sfloor((float)stepsize/(float)downsample));
      if (stepsize == 0)
        stepsize    = 1;
      stepsizerem = (crowend-crowstart) % stepsize;
    }
    SCoordinate newcoord(2);
    newcoord.xy(xdim/downsample,ydim/downsample);
    target.setNativeSElemType(new RGBAI32SElem(nullptr));
    target.resize(newcoord);
    if (cropb != nullptr)
      target.selemDataStore().zeroData();
    // apply the z-axis index offset (safely) if requested and possible
    if ((zoffset > -1) && (extent().getDim() > 2) && (extent().z() > zoffset)) {
      pixeloffset = extent().y() * extent().x() * zoffset;
      // add the scanline jobs to the thread pool
      SCoordinate::Precision scanline;
      for (scanline = crowstart;
          scanline <= (crowend - stepsize); scanline+=stepsize) {
        scanlinePool.addJob(std::bind(&SSpace::getRGBAScanlinesInto,this,
                                      &target,scanline,scanline+stepsize,xdim,
                                      downsample, mergecontexts, opacity,
                                      cstartcol,cendcol,pixeloffset
                                    ), {},
                                      false
                            );
      }
      if (stepsizerem > 0)
        scanlinePool.addJob(std::bind(&SSpace::getRGBAScanlinesInto,this,
                                      &target,scanline,scanline+stepsizerem,
                                      xdim,downsample,mergecontexts,opacity,
                                      cstartcol,cendcol,pixeloffset
                                    ), {},
                                      false
                          );
    }
    else { // otherwise fall back, on virtual method
      // add the scanline jobs to the thread pool
      SCoordinate::Precision scanline;
      for (scanline = crowstart;
          scanline <= (crowend - stepsize); scanline+=stepsize) {
        scanlinePool.addJob(std::bind(&SSpace::getRGBAScanlinesInto,this,
                                      &target,scanline,scanline+stepsize,xdim,
                                      downsample, mergecontexts, opacity,
                                      cstartcol,cendcol,0
                                    ), {},
                                      false
                            );
      }
      if (stepsizerem > 0)
        scanlinePool.addJob(std::bind(&SSpace::getRGBAScanlinesInto,this,
                                      &target,scanline,scanline+stepsizerem,xdim,
                                      downsample,mergecontexts,opacity,
                                      cstartcol,cendcol,0
                                    ), {},
                                      false
                          );
    }
    scanlinePool.dispatcher();
    target.setSpacing(spacing());
    target.PrivateData->SpacingUnits = PrivateData->SpacingUnits;
    scanlinePool.wait();
  }
}

void SSpace::copyInto(SSpace& target, bool withimagedata) {
  if (withimagedata) {
    target.resize(extent());
    SCoordinate iter(extent().getDim());
    SElem::Ptr sourcep = getNativeSElem();
    SElem::Ptr targetp = target.getNativeSElem();
    if (extent().volume() > 0) {
      if (typeid(*sourcep).hash_code() != 
          typeid(*targetp).hash_code()) {
        // perform an implicit conversion
        do {
          sourcep->source(SElemData(iter));
          targetp->source(target.SElemData(iter));
          *targetp = *sourcep;
        } while (iter.tabincrement(extent()));
      }
      else {
        // simplified memory copy (after sanity check)
        if (selemDataStore().size() == target.selemDataStore().size()) {
          memcpy(target.selemDataStore()[0],selemDataStore()[0],
                 selemDataStore().size());
        }
      }
    }
  }
  target.setSpacing     (spacing());
  target.PrivateData->SpacingUnits = PrivateData->SpacingUnits;
  target.LUT            ().copyWL(LUT());
  target.PrivateData->Information         = PrivateData->Information;
  target.PrivateData->ToGlobalSpaceFunc   = PrivateData->ToGlobalSpaceFunc;
  target.PrivateData->FromGlobalSpaceFunc = PrivateData->FromGlobalSpaceFunc;
  target.PrivateData->GlobalSpaceID       = PrivateData->GlobalSpaceID;
  target.PrivateData->VectorMeanings      = PrivateData->VectorMeanings;
}

bool SSpace::resize(const SCoordinate& nncoord){
  bool res = false;
  if (PrivateData->Extent != nncoord) {
    PrivateData->Extent             = nncoord;
    PrivateData->ExtentIndicesCache =
                                     PrivateData->Extent.precalcLinearIndices();
    for (CONTEXTS_t cont=0; cont < PrivateData->DataContexts.size(); cont++)
      PrivateData->DataContexts[cont]->ContextData.resize(extent().volume());
    PrivateData->Spacing.setDim(PrivateData->Extent.getDim());
    PrivateData->SpacingUnits.resize(PrivateData->Extent.getDim());
    res = true;
  }
  return res;
}

SElem::DataSource SSpace::SElemData(const SCoordinate& selemcoord) {
  return (selemDataStore().SElems(calcIndex(selemcoord)));
}

SElem::DataSource SSpace::SElemData_Passive(const SCoordinate& selemcoord) {
  return (selemDataStore().SElems_Passive(calcIndex(selemcoord)));
}

SElem::DataSource SSpace::operator[](const SCoordinate& selemcoord) {
  return (SElemData(selemcoord));
}

void SSpace::SElemInto(const SCoordinate& selemcoord, SElem& target) {
  SElem *tmppox = getNativeSElemP();
  SElem::DataSource tmpsrc = tmppox->newDataSource();
  tmppox->source(tmpsrc);
  tmppox->source(SElemData_Passive(selemcoord));
  target = *tmppox;
  delete   tmppox;
  delete[] tmpsrc;
}

SElem::Ptr SSpace::getNativeSElem() {
  return SPtr<SElem>(selemDataStore().getNativeSElem());
}

SElem* SSpace::getNativeSElemP() {
  return selemDataStore().getNativeSElem();
}

void SSpace::setNativeSElemType(SElem* newpox) {
  if (!PrivateData->LockNativeSElem)
    selemDataStore().setNativeSElemType(newpox);
}

void SSpace::lockNativeSElemType(bool lockpox) {
  PrivateData->LockNativeSElem = lockpox;
}

bool SSpace::nativeSElemTypeLocked() {
  return PrivateData->LockNativeSElem;
}

const SCoordinate& SSpace::extent() const {
  return PrivateData->Extent;
}

const SCoordinate* SSpace::extentP() const {
  return &(extent());
}

bool SSpace::reinterpretExtent(const SCoordinate& newextent) {
  bool res = false;
  if (PrivateData->Extent.volume() == newextent.volume()) {
    PrivateData->Extent = newextent;
    PrivateData->ExtentIndicesCache =
                                     PrivateData->Extent.precalcLinearIndices();
    PrivateData->Spacing.setDim(PrivateData->Extent.getDim());
    PrivateData->SpacingUnits.resize(PrivateData->Extent.getDim());
    res = true;
  }
  return res;
}

void SSpace::promoteExtent() {
  SCoordinate tmpext = PrivateData->Extent;
  tmpext.promote();
  reinterpretExtent(tmpext);
}

void SSpace::reset() {
  PrivateData->Extent.reset();
  PrivateData->Information.clear();
  PrivateData->Spacing.reset();
  PrivateData->SpacingUnits.clear();
  for (unsigned i=0; i<PrivateData->DataContexts.size();i++)
    removeContext(i);
  PrivateData->DataContexts.clear();
  newContext();
  setActiveContext(0);
  PrivateData->ToGlobalSpaceFunc   = nullptr;
  PrivateData->FromGlobalSpaceFunc = nullptr;
  PrivateData->GlobalSpaceID.clear();
  PrivateData->VectorMeanings.clear();
}

SSpace& SSpace::getSourceSSpace() {
  return *this;
}

void SSpace::setActiveContext(CONTEXTS_t newcon) {
  PrivateData->ActiveContext = newcon;
}

CONTEXTS_t SSpace::activeContext() const {
  return PrivateData->ActiveContext;
}

CONTEXTS_t SSpace::contexts() const {
  return PrivateData->DataContexts.size();
}

CONTEXTS_t SSpace::newContext() {
  selemDataContexts().resize(PrivateData->DataContexts.size()+1);
  selemDataContexts()[selemDataContexts().size()-1] = new DataContext;
  selemDataContexts()[selemDataContexts().size()-1]->
                               ContextData.resize(PrivateData->Extent.volume());
  return PrivateData->DataContexts.size()-1;
}

void SSpace::removeContext(CONTEXTS_t contextnum) {
  if (contextnum < selemDataContexts().size()) {
    delete PrivateData->DataContexts[contextnum];
    selemDataContexts().erase(selemDataContexts().begin()+contextnum);
  }
}

GPLUT& SSpace::LUT() {
  return selemDataContexts()[PrivateData->ActiveContext]->ContextLUT;
}

GPLUT* SSpace::LUTP() {
  return &LUT();
}

SElemSet& SSpace::selemDataStore() {
  return selemDataContexts()[PrivateData->ActiveContext]->ContextData;
}

SElemSet* SSpace::selemDataStoreP() {
  return &selemDataStore();
}

std::vector< DataContext* >& SSpace::selemDataContexts() {
  return PrivateData->DataContexts;
}

std::vector< DataContext* >* SSpace::selemDataContextsP() {
  return &selemDataContexts();
}

void SSpace::applyLUT() {
  SElem::Ptr selemp = getNativeSElem();
  if ( LUT().sizeWL() < 1 ) {
    DEBUG_OUT("applyWLLUT: Could not apply window level - empty!");
    return;
  }
  for (SElemSet::Size pos = 0; pos < selemDataStore().length(); pos++) {
    selemp->source(selemDataStore().SElems(pos));
    uint8_t lutval = LUT().lookup(selemp->intensityUnsigned());
    selemp->rgb(lutval,lutval,lutval);
  }
}

void SSpace::autoLUT(int percentile, long long int width, long long int center,
                     const std::vector< std::pair< float, float > >*histogram) {
  long long int centerl, widthl;
  std::vector< std::pair< float, float > > histograml;
  float percentilel;
  float interval = 1;
  percentilel = percentile;
  // clamp percentile
  if ((percentilel > 100.0) || (percentilel < 0.0))
    percentilel = 100.0;
  percentilel = percentilel / 100.0;
  // determine a histogram
  if (histogram == nullptr) {
    // generate histogram
    SBucketHistogram histoalg;
    histoalg.setParameter("maximum",pow(2.0,(double)LUT().bppIn()));
    histoalg.setParameter("buckets", 64.0);
    histoalg.setParameter("threshold", 1.0);
    histoalg.setInput(0,*this,false);
    histoalg.execute();
    histograml = histoalg.histogram();
  }
  else {
    histograml = *histogram;
  }
  if (histograml.size() > 1)
    interval = histograml[1].first - histograml[0].first;
  // determine window centre
  if (center == -1) {
    float maxval = 0.0;
    float maxpos = 0.0;
    for (unsigned i=0; i<histograml.size(); i++) {
      if (histograml[i].second > maxval) {
        maxval = histograml[i].second;
        maxpos = histograml[i].first;
      }
    }
    centerl = (long long int)maxpos;
  }
  else {
    centerl = center;
  }
  // determine window width
  if (width == -1) {
    // count based on percentile
    int    index = 0;
    float  accum = 0.0;
    double total = 0.0;
    widthl = 0.0;
    // locate the centre bucket and count the total
    for (unsigned i=0; i< histograml.size(); i++) {
      total += histograml[i].second;
      if (histograml[i].first <= centerl) {
        index = i;
      }
    }
    // scale the total
    total = total * percentilel;
    accum = histograml[index].second; //reset accum for resuse here
    // expand outwards to find width
    unsigned count;
    for (count=1; count < histograml.size(); count ++) {
      if (accum >= total) {
        break;
      }
      // pos
      if ((index + count) < histograml.size())
        accum += histograml[index + count].second;
      // neg
      if ((index - count) < histograml.size())
        accum += histograml[index - count].second;
    }
    // compute final width
    widthl = (long long int)(interval * (float)count);
  }
  else {
    widthl = width;
  }
  // assign to LUT
  LUT().genLUT(LUT().bppIn(),centerl,widthl);
}

bool SSpace::postLoadConfig() {
  return true;
}

NNode& SSpace::informationNode() {
  return PrivateData->Information;
}

NNode* SSpace::informationNodeP() {
  return &informationNode();
}

const SVector& SSpace::spacing() const {
  return PrivateData->Spacing;
}

const SVector* SSpace::spacingP() const {
  return &spacing();
}

void SSpace::setSpacing(const SVector& nspace) {
  PrivateData->Spacing = nspace;
}

std::string SSpace::spacingUnits(long long int index) const {
  std::string result;
  if (index < (long long int)PrivateData->SpacingUnits.size())
    result = PrivateData->SpacingUnits[index];
  return result;
}

void SSpace::setSpacingUnits(const std::string& nunits, long long int index) {
  if (index < (long long int)PrivateData->SpacingUnits.size())
    PrivateData->SpacingUnits[index] = nunits;
}

SPtr<SSpaceIterator> SSpace::getFastNativeIterator() {
  return SPtr<SSpaceIterator>(new SSpaceIteratorNativeFast(*this));
}

SSpaceIterator* SSpace::getFastNativeIteratorP() {
  return new SSpaceIteratorNativeFast(*this);
}

SVector SSpace::toGlobalSpace(const SVector& source) {
  if (PrivateData->ToGlobalSpaceFunc != nullptr)
    return PrivateData->ToGlobalSpaceFunc(this,source);
  else
    return source;
}

SVector SSpace::fromGlobalSpace(const SVector& source) {
  if (PrivateData->FromGlobalSpaceFunc != nullptr)
    return PrivateData->FromGlobalSpaceFunc(this,source);
  else
    return source;
}

void SSpace::setToGlobalSpaceFunction
                 (std::function<SVector(SSpace*,const SVector&)> newf) {
  PrivateData->ToGlobalSpaceFunc = newf;
}

void SSpace::setFromGlobalSpaceFunction
                 (std::function<SVector(SSpace*,const SVector&)> newf) {
  PrivateData->FromGlobalSpaceFunc = newf;
}

std::string SSpace::globalSpaceID() const {
  return PrivateData->GlobalSpaceID;
}

void SSpace::setGlobalSpaceID(const std::string& newspaceid) {
  PrivateData->GlobalSpaceID = newspaceid;
}

static bool coscomp(std::pair<std::string,float> v1,
                    std::pair<std::string,float> v2) {
  return v1.second > v2.second;
}

std::string SSpace::vectorMeaning(const SVector& avect, bool strict) const {
  std::string result;
  if (strict) {
    // only precise vector matching
    for (unsigned i=0; i<PrivateData->VectorMeanings.size(); i++)
      if (PrivateData->VectorMeanings[i].first == avect) {
        result = PrivateData->VectorMeanings[i].second;
        break;
      }
  }
  else {
    // all matching, weighted, components (comma separated)
    std::vector< std::pair<std::string,float> > matches;
    const float match_threshold = 0.001;
    // find matching
    for (unsigned i=0; i<PrivateData->VectorMeanings.size(); i++) {
      float cwvect = avect.dot(PrivateData->VectorMeanings[i].first);
      if (cwvect > match_threshold)
        matches.push_back(
                  std::make_pair(PrivateData->VectorMeanings[i].second,cwvect));
    }
    // sort them by cosine, greatest first
    std::sort(matches.begin(),matches.end(),coscomp);
    // produce output string
    for(unsigned i=0; i<matches.size();i++) {
      std::stringstream resstr;
      resstr.precision(2);
      resstr << matches[i].first;
      if (matches.size() > 1)
        resstr << "(" << matches[i].second << ")";
      result += resstr.str();;
      if (i < matches.size()-1)
        result += ", ";
    }
  }
  return result;
}

SVector SSpace::meaningVector(const std::string& meaning) const {
  SVector retval;
  for (unsigned i=0; i<PrivateData->VectorMeanings.size(); i++)
    if (PrivateData->VectorMeanings[i].second == meaning) {
      retval = PrivateData->VectorMeanings[i].first;
      break;
    }
  return retval;
}

void SSpace::assignVectorMeaning(const SVector& nvect,
                                 const std::string& nmean) {
  bool existed = false;
  for (unsigned i=0; i<PrivateData->VectorMeanings.size(); i++)
    if (PrivateData->VectorMeanings[i].second == nmean) {
      PrivateData->VectorMeanings[i].first = nvect;
      existed = true;
    }
  if (!existed)
    PrivateData->VectorMeanings.push_back(std::make_pair(nvect,nmean));
}

void SSpace::resetMeaningVectors() {
  PrivateData->VectorMeanings.clear();
}

SCoordinate SSpace::toSourceCoords(const SCoordinate& localc) const {
  return localc;
}

bool SSpace::fromSourceCoords(const SCoordinate& sourcecoord,
                              SCoordinate& localcoord) const {
  bool retval = false;
  if (sourcecoord.getDim() == extent().getDim()) {
    retval = true;
    localcoord = sourcecoord;
  }
  return retval;
}

/*------------------------------------------------------------------------------
 *                          SElem Collection Classes
 * ---------------------------------------------------------------------------*/

class SElemSet::SElemSetPIMPL {
public:
  SElem::DataSource     SElemsStore;
  SElem*              SElemExemplar;
  int                 SElemBaseSize;
  SElemSet::Size  Size;
  bool                CStyle;
  virtual void        clearArray();
  SElemSetPIMPL(): SElemsStore(nullptr),SElemExemplar(nullptr),Size(0),CStyle(false) {

  }
  virtual ~SElemSetPIMPL() { }
};

SElemSet::SElemSet(): PrivateData(new SElemSetPIMPL()) {
  setNativeSElemType(new TightSElem(nullptr));
}

SElemSet::SElemSet(SElemSet::Size setsize): PrivateData(new SElemSetPIMPL())
{
  setNativeSElemType(new TightSElem(nullptr));
  resize(setsize);
}

SElemSet::SElemSet(const SElemSet& source): PrivateData(new SElemSetPIMPL()) {
  setNativeSElemType(source.getNativeSElem());
  resize(source.length());  
}

void SElemSet::SElemSetPIMPL::clearArray(){
  if (SElemsStore != nullptr) {
    if (CStyle)
      free(SElemsStore);
    else
      delete [] SElemsStore;
  }
  Size = 0;
  SElemsStore = nullptr;
}

SElemSet::~SElemSet(){
  PrivateData->clearArray();
  delete PrivateData->SElemExemplar;
  delete PrivateData;
}

void SElemSet::useData(SElem::DataSource newdata, SElemSet::Size nbytes,
                       bool carr) {
  // check for allocation safety
  if ( nbytes >= size() ) {
    PrivateData->clearArray();
    PrivateData->SElemsStore = newdata;
    PrivateData->Size = nbytes / PrivateData->SElemBaseSize;
    PrivateData->CStyle = carr;
  }
  else {
    SLogger::global().addMessage
                ("SElemSet::useData: request to use data of insufficient size. \
This is a serious error in a sofware module which will likely result in large \
memory leaks.",
                  SLogLevels::HIGH_SYNC);
    throw SimulacrumMemException();
  }
}

bool SElemSet::carrData() {
  return PrivateData->CStyle;
}

SElemSet::Size SElemSet::size() const {
  return (PrivateData->SElemBaseSize * PrivateData->Size);
}

SElemSet::Size SElemSet::length() const {
  return PrivateData->Size;
}

void SElemSet::setNativeSElemType(SElem* newpox) {
  if (newpox == nullptr) {
    throw SimulacrumMemException();
  }
  SElemSet::Size currentsize = PrivateData->Size;
  if (PrivateData->SElemExemplar != nullptr)
    delete PrivateData->SElemExemplar;
  PrivateData->SElemExemplar = newpox;
  PrivateData->SElemBaseSize = PrivateData->SElemExemplar->size();
  resize(currentsize);
}

SElem* SElemSet::getNativeSElem() const {
  return PrivateData->SElemExemplar->New();
}

SElem& SElemSet::exemplar() {
  return *(PrivateData->SElemExemplar);
}


SElem::DataSource SElemSet::SElems(SElemSet::Size reqselem) {
  SElemSet::Size pos = reqselem * PrivateData->SElemBaseSize;
#ifdef BOUND_CHECK
  if (pos>(PrivateData->Size*PrivateData->SElemBaseSize))
    throw SimulacrumMemException();
#endif
  return &(PrivateData->SElemsStore[pos]);
}

SElem::DataSource SElemSet::SElems_Passive(SElemSet::Size reqselem) {
  return SElems(reqselem);
}

SElem::DataSource SElemSet::SElem_Null() {
  return SElems_Passive(PrivateData->Size);
}

SElem::DataSource SElemSet::operator[](SElemSet::Size reqselem) {
  return SElems(reqselem);
}

void SElemSet::resize(SElemSet::Size newsize){
  if (newsize != PrivateData->Size) {
    PrivateData->clearArray();
    PrivateData->Size = newsize;
    // +1 for SElem_Null -> used for out-of-bounds data and sparse elements
    PrivateData->CStyle = false;
    try {
      PrivateData->SElemsStore=new unsigned char[(newsize+1)*PrivateData->SElemBaseSize];
    }
    catch (std::exception &e) {
      PrivateData->CStyle = false;
    }
    if (PrivateData->SElemsStore == nullptr)
      throw SimulacrumMemException();
  }
}

void SElemSet::reset(){
  resize(0);
  setNativeSElemType(new TightSElem(nullptr));
}

void SElemSet::zeroData() {
  memset(PrivateData->SElemsStore,0,
         PrivateData->Size*PrivateData->SElemBaseSize);
}

//------------------------------------------------------------------------------

SSpaceIterator::~SSpaceIterator() {

}

bool SSpaceIterator::operator<(const SSpaceIterator& targ) const {
  return pos().withinSpace(targ.pos());
}

bool SSpaceIterator::operator==(const SSpaceIterator& targ) const {
  return pos() == targ.pos();
}

bool SSpaceIterator::operator!=(const SSpaceIterator& targ) const {
  return ! ( (*this) == targ );
}

const SSpaceIterator& SSpaceIterator::operator=(const SSpaceIterator& rhs){
  toCoord(rhs.pos());
  return *this;
}

SElem& SSpaceIterator::selem() const {
  return operator*();
}

const SSpaceIterator& SSpaceIterator::operator++(int) {
  ++(*this);
  return *this;
}

const SSpaceIterator& SSpaceIterator::increment() {
  ++(*this);
  return *this;
}

const SSpaceIterator& SSpaceIterator::operator--(int) {
  --(*this);
  return *this;
}

const SSpaceIterator& SSpaceIterator::decrement() {
  --(*this);
  return *this;
}

const SCoordinate SSpaceIterator::sourcePos() const {
  return pos();
}

const SCoordinate* SSpaceIterator::posP() const {
  return &pos();
}

SElem* SSpaceIterator::selemp() const {
  return &(*(*this));
}

//------------------------------------------------------------------------------

SSpaceIteratorNativeFast::SSpaceIteratorNativeFast(SSpace& source): 
                                      SourceSSpace(&source), NativeSElem(nullptr) {
  Extent = &SourceSSpace->extent();
  initNativeSElem();
  toBegin();
}

SSpaceIteratorNativeFast::SSpaceIteratorNativeFast
                                        (const SSpaceIteratorNativeFast& targ) {
  *this = targ;
}

SSpaceIteratorNativeFast::~SSpaceIteratorNativeFast() {
  if (NativeSElem != nullptr)
    delete NativeSElem;
}

void SSpaceIteratorNativeFast::initNativeSElem() {
  if (NativeSElem != nullptr)
    delete NativeSElem;
  NativeSElem = SourceSSpace->selemDataStore().getNativeSElem();
}

void SSpaceIteratorNativeFast::toBegin() {
  toCoord(SCoordinate(SourceSSpace->extent().getDim()));
}

bool SSpaceIteratorNativeFast::isBegin() {
  return Position == 0;
}

void SSpaceIteratorNativeFast::toEnd() {
  SCoordinate endcoord = SourceSSpace->extent();
  for (int i=0; i<endcoord.getDim(); i++)
    endcoord[i] -= 1;
  toCoord(endcoord);
}

bool SSpaceIteratorNativeFast::isEnd() {
  return ! (Position < SourceSSpace->selemDataStore().length());
}

void SSpaceIteratorNativeFast::toCoord(const SCoordinate& newpos) {
  Position = SourceSSpace->calcIndex(newpos);
  CoordPosition = newpos;
  step(true,0);
}

const SSpaceIteratorNativeFast SSpaceIteratorNativeFast::operator+
                                          (SElemSet::Size stepsize) const {
  SSpaceIteratorNativeFast retval(*SourceSSpace);
  retval = *this;
  retval += stepsize;
  return retval;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator+=
                                                 (SElemSet::Size stepsize) {
  step(true,stepsize);
  return *this;
}

const SSpaceIteratorNativeFast SSpaceIteratorNativeFast::operator-
                                           (SElemSet::Size stepsize) const {
  SSpaceIteratorNativeFast retval(*SourceSSpace);
  retval = *this;
  retval -= stepsize;
  return retval;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator-=
                                                 (SElemSet::Size stepsize) {
  step(false,stepsize);
  return *this;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator++() {
  step(true,1);
  return *this;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator--() {
  step(false,1);
  return *this;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator=
                                                 (const SSpaceIterator& targp) {
  SSpaceIteratorNativeFast const *targ =
                          dynamic_cast<const SSpaceIteratorNativeFast*>(&targp);
  if (targ != nullptr) {
    *this = *targ;
  }
  return *this;
}

const SSpaceIteratorNativeFast& SSpaceIteratorNativeFast::operator=
                                        (const SSpaceIteratorNativeFast& targ) {
  SourceSSpace = targ.SourceSSpace;
  Extent = &SourceSSpace->extent();
  initNativeSElem();
  return *this;
}

SElem& SSpaceIteratorNativeFast::operator*() const {
  return *NativeSElem;
}

const SCoordinate& SSpaceIteratorNativeFast::pos() const {
  return CoordPosition;
}

void SSpaceIteratorNativeFast::step(bool forwards, SElemSet::Size stepsize){
  if (forwards) {
    Position += stepsize;
    for (unsigned i=0; i< stepsize; i++)
      CoordPosition.tabincrement(*Extent);
  }
  else {
    Position -= stepsize;
    for (unsigned i=0; i< stepsize; i++)
      CoordPosition.tabdecrement(*Extent);
  }
  NativeSElem->source(SourceSSpace->selemDataStore()[Position]);
}

//------------------------------------------------------------------------------

class SSpaceIteratorCompat::SSpaceIteratorCompatPIMPL {
public:
  SSpace               *SourceSpace;
  const SCoordinate    *SourceExtent;
  SCoordinate           Position;
  SElem                *NativeSElem;
  void                  initNativeSElem();
  SSpaceIteratorCompatPIMPL(SSpace &targ): SourceSpace(&targ),
                                           SourceExtent(&targ.extent()),
                                           NativeSElem(nullptr) {
    initNativeSElem();
  }
};

SSpaceIteratorCompat::SSpaceIteratorCompat(SSpace& targ):
                              PrivateData(new SSpaceIteratorCompatPIMPL(targ)) {
  toBegin();
}

SSpaceIteratorCompat::SSpaceIteratorCompat(const SSpaceIteratorCompat& targ):
  PrivateData(new SSpaceIteratorCompatPIMPL(*(targ.PrivateData->SourceSpace))) {
  *this = targ;
}

SSpaceIteratorCompat::~SSpaceIteratorCompat() {
  if (PrivateData->NativeSElem != nullptr)
    delete PrivateData->NativeSElem;
  delete PrivateData;
}

void SSpaceIteratorCompat::toBegin() {
  toCoord(SCoordinate(PrivateData->SourceExtent->getDim()));
}

bool SSpaceIteratorCompat::isBegin() {
  return PrivateData->Position == SCoordinate(PrivateData->Position.getDim());
}

void SSpaceIteratorCompat::toEnd() {
  SCoordinate endcoord = *(PrivateData->SourceExtent);
  for (int i=0; i<endcoord.getDim(); i++)
    endcoord[i] -= 1;
  toCoord(endcoord);
}

bool SSpaceIteratorCompat::isEnd() {
  return ! PrivateData->SourceExtent->withinSpace(PrivateData->Position);
}

void SSpaceIteratorCompat::toCoord(const SCoordinate& ncoord) {
  PrivateData->Position = ncoord;
}

const SSpaceIterator& SSpaceIteratorCompat::operator++() {
  if (!PrivateData->Position.tabincrement(*PrivateData->SourceExtent))
    PrivateData->Position = *(PrivateData->SourceExtent);
  return *this;
}

const SSpaceIterator& SSpaceIteratorCompat::operator--() {
  PrivateData->Position.tabdecrement(*(PrivateData->SourceExtent));
  return *this;
}

const SSpaceIterator& SSpaceIteratorCompat::operator=
                                                  (const SSpaceIterator& targ) {
  const SSpaceIteratorCompat *ntarg = 
                               dynamic_cast<const SSpaceIteratorCompat*>(&targ);
  if (ntarg != nullptr) {
    PrivateData->SourceSpace = ntarg->PrivateData->SourceSpace;
    PrivateData->SourceExtent = &PrivateData->SourceSpace->extent();
    PrivateData->Position = ntarg->PrivateData->Position;
    PrivateData->initNativeSElem();
  }
  return *this;
}

const SSpaceIterator& SSpaceIteratorCompat::operator=
                                            (const SSpaceIteratorCompat& targ) {

  PrivateData->SourceSpace = targ.PrivateData->SourceSpace;
  PrivateData->SourceExtent = &PrivateData->SourceSpace->extent();
  PrivateData->Position = targ.PrivateData->Position;
  PrivateData->initNativeSElem();
  return *this;
}


SElem& SSpaceIteratorCompat::operator*() const {
  PrivateData->NativeSElem->source(
                      PrivateData->SourceSpace->SElemData_Passive(sourcePos()));
  return *(PrivateData->NativeSElem);
}

const SCoordinate& SSpaceIteratorCompat::pos() const {
  return PrivateData->Position;
}

void SSpaceIteratorCompat::SSpaceIteratorCompatPIMPL::initNativeSElem() {
  if (NativeSElem != nullptr)
    delete NativeSElem;
  NativeSElem = SourceSpace->getNativeSElemP();
}

//------------------------------------------------------------------------------

class SSpaceIteratorCompatConstrained::SSpaceIteratorCompatConstrainedPIMPL {
public:
  SCoordinate BasePos, Constraint;
};

SSpaceIteratorCompatConstrained::SSpaceIteratorCompatConstrained(SSpace& src, 
                        const SCoordinate& base, const SCoordinate& constraint):
                                                       SSpaceIteratorCompat(src),
                      PrivateDataL(new SSpaceIteratorCompatConstrainedPIMPL()) {
  PrivateDataL->BasePos     = base;
  PrivateDataL->Constraint  = constraint;
  PrivateData->SourceExtent = &(PrivateDataL->Constraint);
}

SSpaceIteratorCompatConstrained::~SSpaceIteratorCompatConstrained() {
  delete PrivateDataL;
}

const SCoordinate SSpaceIteratorCompatConstrained::sourcePos() const {
  return (PrivateDataL->BasePos + PrivateDataL->Constraint);
}

/*------------------------------------------------------------------------------
 *                         ---------END---------
 * ---------------------------------------------------------------------------*/
