/*
 * decomp.c --
 *
 * This is the routine that is called to decompress a frame 
 * image data. It is based on the program originally named ljpgtopnm.c.
 * Major portions taken from the Independent JPEG Group' software, and
 * from the Cornell lossless JPEG code
 */
/*
 * $Id: decomp.c,v 1.6 2013/06/25 21:32:20 enlf Exp $
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>  
#include "io.h"
#include "jpeg.h"
#include "mcu.h"
#include "proto.h"

/*
 *--------------------------------------------------------------
 *
 * ReadJpegData --
 *
 *        This is an interface routine to the JPEG library.  The
 *        JPEG library calls this routine to "get more data"
 *
 * Results:
 *        Number of bytes actually returned.
 *
 * Side effects:
 *        None.
 *
 *--------------------------------------------------------------
 */
static void efree(void **ptr)
{
        if((*ptr) != 0)
                free((*ptr));
        *ptr = 0;
}

unsigned short * JPEGLosslessDecodeImage (const char* data, int depth, int length, DecompressInfo* dcInfo)
{ 
    /* Initialization */
    MEMSET (dcInfo, 0, sizeof (*dcInfo));
    inputBufferOffset = 0;
    unsigned short *resimage = NULL;
    
    /* Allocate input buffer */
    inputBuffer = (unsigned char*)malloc((size_t)length+5);
    inputBufferSize = length+5;
    if (inputBuffer == NULL)
                return resimage;

        /* Read input buffer */
    memcpy(inputBuffer,data,length);
    inputBuffer [length] = (unsigned char)EOF;
    
        /* Read JPEG File header */
    ReadFileHeader (dcInfo);
    if ((*dcInfo).error) { efree ((void **)&inputBuffer); return resimage; }

    /* Read the scan header */
    if (!ReadScanHeader (dcInfo)) { efree ((void **)&inputBuffer); return resimage; }
    
    /* 
     * Decode the image bits stream. Clean up everything when
     * finished decoding.
     */
    DecoderStructInit (dcInfo);
    if ((*dcInfo).error) { efree ((void **)&inputBuffer); return resimage; }

    HuffDecoderInit (dcInfo);
    if ((*dcInfo).error) { efree ((void **)&inputBuffer); return resimage; }

    /* Resize the target array to match the resultant image size */
    resimage = malloc((*dcInfo).imageWidth * (*dcInfo).imageHeight * (*dcInfo).compsInScan * sizeof(unsigned short));
    DecodeImage (dcInfo, (unsigned short **) &resimage, depth);

    /* Free input buffer */
    efree ((void **)&inputBuffer);
    
    return resimage;
}
