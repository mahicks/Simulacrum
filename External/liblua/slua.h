#ifndef SLUA_WRAP
#define SLUA_WRAP

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

extern int luaopen_SLua(lua_State* L);

#include "slua_extern.h"

#endif
