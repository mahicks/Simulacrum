%module SLua
// Standard library wrappers
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"
%include "stdint.i"

// exclude some dubious methods, and map the SWIG-safe versions in their place
%ignore Simulacrum::SSpace::extent();
%rename (extent) Simulacrum::SSpace::extentP();
%ignore Simulacrum::SSpace::getNativeSElem();
%rename (getNativeSElem) Simulacrum::SSpace::getNativeSElemP();
%ignore Simulacrum::SSpace::LUT();
%rename (LUT) Simulacrum::SSpace::LUTP();
%ignore Simulacrum::SSpace::selemDataStore();
%rename (selemDataStore) Simulacrum::SSpace::selemDataStoreP();
%ignore Simulacrum::SSpace::selemDataContexts();
%rename (selemDataContexts) Simulacrum::SSpace::selemDataContextsP();
%ignore Simulacrum::SSpace::informationNode();
%rename (informationNode) Simulacrum::SSpace::informationNodeP();
%ignore Simulacrum::SSpace::spacing();
%rename (spacing) Simulacrum::SSpace::spacingP();
%ignore Simulacrum::SSpace::getFastNativeIterator();
%rename (getFastNativeIterator) Simulacrum::SSpace::getFastNativeIteratorP();
%ignore Simulacrum::SSpaceIterator::operator*();
%rename (selem) Simulacrum::SSpaceIterator::selemp();
%ignore Simulacrum::SSpaceIterator::pos();
%rename (pos) Simulacrum::SSpaceIterator::posP();
%ignore Simulacrum::SAbsTreeNode::query(const std::string&);
%rename (query) Simulacrum::SAbsTreeNode::queryP(const std::string&);
%ignore Simulacrum::SCoordinate::SCoordinate(std::initializer_list<SCoordinate::Precision>);
%ignore Simulacrum::SVector::SVector(std::initializer_list<SVector::Precision>);
%ignore Simulacrum::SSlicer::setReductionFunction(linereduction_t);

%{
#define SWIG
// Simulacrum CORE
#include "../../Core/definitions.h"
#include "../../Core/error.h"
#include "../../Core/sabtree.h"
#include "../../Core/sconnectable.h"
#include "../../Core/slockable.h"
#include "../../Core/sresource.h"
#include "../../Core/sfileio.h"
#include "../../Core/sprimitives.h"
#include "../../Core/LUT/LUT.h"
#include "../../Core/sspace.h"
#include "../../Core/slicer/slicer.h"
#include "../../Core/SPool/SPool.h"
#include "../../Core/NNode/nnode.h"
#include "../../Core/salgorithm.h"
// Simulacrum Networking
#include "../../Network/snet.h"
// Simulacrum DICOM
#include "../../Resources/DICOM/types.h"
#include "../../Resources/DICOM/dcmtag.h"
#include "../../Resources/DICOM/datadic.h"
#include "../../Resources/DICOM/sarch.h"
#include "../../Resources/DICOM/sdicom.h"
#include "../../Resources/DICOM/sdicom-net.h"
// Resources
#include "../../Resources/sj2k.h"
#include "../../Resources/spbm.h"
#include "../../Resources/sjpegls.h"
#include "../../Resources/sjpeg.h"
#include "../../Resources/sljpeg.h"
#include "../../Resources/spng.h"
// Toolbox
#include "../../Toolbox/SFile/sfile.h"
#include "../../Toolbox/SLogger/slogger.h"
#include "../../Toolbox/SResourceAllocator/sresourceallocator.h"
#include "../../Toolbox/SRing/sring.h"
#include "../../Toolbox/SURI/suri.h"
#include "../../Toolbox/cmdProgress/cmdProgress.h"
#include "../../Toolbox/parseArgs/parseArgs.h"
#include "../../Toolbox/sysInfo/sysInfo.h"
#include "../../Toolbox/SPlugins/splugins.h"

// Set the namespace
using namespace Simulacrum;
%}

// Simulacrum CORE
%include "../../Core/definitions.h"
%include "../../Core/error.h"
%include "../../Core/sabtree.h"
%include "../../Core/sconnectable.h"
%include "../../Core/slockable.h"
%include "../../Core/sresource.h"
%include "../../Core/sfileio.h"
%include "../../Core/sprimitives.h"
%include "../../Core/LUT/LUT.h"
%include "../../Core/sspace.h"
%include "../../Core/slicer/slicer.h"
%include "../../Core/SPool/SPool.h"
%include "../../Core/NNode/nnode.h"
%include "../../Core/salgorithm.h"
// Simulacrum Networking
%include "../../Network/snet.h"
// Simulacrum DICOM
%include "../../Resources/DICOM/types.h"
%include "../../Resources/DICOM/dcmtag.h"
%include "../../Resources/DICOM/datadic.h"
%include "../../Resources/DICOM/sarch.h"
%include "../../Resources/DICOM/sdicom.h"
%include "../../Resources/DICOM/sdicom-net.h"
// Resource
%include "../../Resources/sj2k.h"
%include "../../Resources/spbm.h"
%include "../../Resources/sjpegls.h"
%include "../../Resources/sjpeg.h"
%include "../../Resources/sljpeg.h"
%include "../../Resources/spng.h"
// Toolbox
%include "../../Toolbox/SFile/sfile.h"
%include "../../Toolbox/SLogger/slogger.h"
%include "../../Toolbox/SResourceAllocator/sresourceallocator.h"
%include "../../Toolbox/SRing/sring.h"
%include "../../Toolbox/SURI/suri.h"
%include "../../Toolbox/cmdProgress/cmdProgress.h"
%include "../../Toolbox/parseArgs/parseArgs.h"
%include "../../Toolbox/sysInfo/sysInfo.h"
%include "../../Toolbox/SPlugins/splugins.h"

// wrap some common template types
%template(StringVector) std::vector< std::string >;
%template(SSpaceVector) std::vector< Simulacrum::SSpace* >;
%template(DCMTagVector) std::vector< Simulacrum::DCMTag* >;
%template(NodeVector) std::vector< Simulacrum::SAbsTreeNode* >;
%template(XMLDataVector) std::vector< Simulacrum::NNode::XMLCharType >;
%template(FloatVector) std::vector< float >;
%template(IntVector) std::vector< int >;
%template(DoubletVector) std::vector< double >;
%template(UnsignedVector) std::vector< unsigned >;
%template(SCoordinateVector) std::vector<Simulacrum::SCoordinate::Precision>;
