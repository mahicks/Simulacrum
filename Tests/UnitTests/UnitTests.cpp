/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#define CATCH_CONFIG_MAIN
#include <External/catch2/catch.hpp>
#include <simulacrum.h>

using namespace Simulacrum;

TEST_CASE("Complex numbers are handled correctly", "[SComplex]") {
  SComplex complex_test(2,3);
  SECTION("Basic behaviour") {
    REQUIRE(complex_test.r() == 2);
    REQUIRE(complex_test.i() == 3);
  }
  SECTION("Mutations") {
    complex_test.r(4);
    complex_test.i(7);
    REQUIRE(complex_test.r() == 4);
    REQUIRE(complex_test.i() == 7);
  }
  SECTION("Operators") {
    complex_test = {1,4};
    REQUIRE(complex_test.mag() > 4.12);
    REQUIRE(complex_test.mag() < 4.13);
    REQUIRE(complex_test.conj() == SComplex(1,-4));
    REQUIRE((complex_test * SComplex(5,1)) == SComplex(1,21));
    REQUIRE((SComplex(1,-3) / SComplex(1,2)) == SComplex(-1,-1));
    REQUIRE((complex_test + SComplex(3,-3)) == SComplex(4,1));
    REQUIRE((complex_test - SComplex(10,4)) == SComplex(-9,0));
    REQUIRE((complex_test   * 3) == SComplex(3,12));
    REQUIRE((SComplex(6,-8) / 2) == SComplex(3,-4));
    REQUIRE((complex_test + 3.5) == SComplex(4.5,7.5));
    REQUIRE((complex_test - 1.5) == SComplex(-0.5,2.5));
    REQUIRE(complex_test.conj().toString() == "1-4i");
  }
}
