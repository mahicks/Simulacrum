-- check wrapping container

-- print table contents
function table_print (tbl, prefix)
  if not prefix then prefix = "" end
  for k, v in pairs(tbl) do
    new_prefix = prefix .. "." .. k
    if type(v) == "table" then
      print(new_prefix .. " -> class/table")
      table_print(v, "  " .. new_prefix)
    else
      print(new_prefix .. " -> " .. tostring(v))
    end
  end
end

table_print(SLua,"SLua")
