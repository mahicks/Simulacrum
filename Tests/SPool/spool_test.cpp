#include <iostream>
#include <string>
#include <Core/SPool/SPool.h>
#include <Toolbox/sysInfo/sysInfo.h>

using namespace Simulacrum;

void function1() {
  sysInfo::sleep(100);
  std::cout << "Function 1!" << std::endl;
}

void function2() {
  sysInfo::sleep(200);
  std::cout << "Function 2!" << std::endl;
}

void function3() {
  sysInfo::sleep(300);
  std::cout << "Function 3!" << std::endl;
}

void function4() {
  sysInfo::sleep(400);
  std::cout << "Function 4!" << std::endl;
}

void function5() {
  sysInfo::sleep(500);
  std::cout << "Function 5!" << std::endl;
}

void function6() {
  sysInfo::sleep(600);
  std::cout << "Function 6!" << std::endl;
}

void function7() {
  sysInfo::sleep(700);
  std::cout << "Function 7!" << std::endl;
}

void function8() {
  sysInfo::sleep(800);
  std::cout << "Function 8!" << std::endl;
}

int main(int,char**) {
  SPool TestPool(4);
  std::cout << "Pool Size: " << TestPool.poolSize() << std::endl;
  TestPool.addJob(std::bind(function1));
  TestPool.addJob(std::bind(function2));
  TestPool.addJob(std::bind(function3));
  TestPool.addJob(std::bind(function4));
  TestPool.addJob(std::bind(function5));
  TestPool.addJob(std::bind(function6));
  TestPool.addJob(std::bind(function7));
  TestPool.addJob(std::bind(function8));
  std::cout << "Threads added." << std::endl;
  std::cout << "Active threads: " << TestPool.activeThreads() << std::endl;
  std::cout << "Pending Jobs: " << TestPool.jobs() << std::endl;
}
