/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>

#include <Core/sprimitives.h>
#include <Core/sspace.h>
#include <Core/sfileio.h>
#include <Core/NNode/nnode.h>
#include <Resources/spbm.h>
#include <Core/error.h>

using namespace Simulacrum;

int main(int, char **) {
    std::cout << "Testing Simulacrum:" << std::endl;
    DEBUG_OUT("Testing DEBUG OUTPUT")
    std::cout << "SElem (using TightSElem)..." << std::endl;
    TightSElemInternType tmpdata1;
    SElem* TestSElem = new TightSElem((SElem::DataSource)&tmpdata1);
    TestSElem->red(233);
    std::cout << " Set R233,  Got: R" << TestSElem->red() << std::endl;
    TestSElem->green(223);
    std::cout << " Set G223,  Got: G" << TestSElem->green() << std::endl;
    TestSElem->blue(213);
    std::cout << " Set R213,  Got: R" << TestSElem->blue() << std::endl;
    TestSElem->intensity(0xFFAC);
    std::cout << " Set IFFAC, Got: I" << std::hex << TestSElem->intensity() << std::dec << std::endl;
    TestSElem->segment(2048);
    std::cout << " Set S2048, Got: S" << TestSElem->segment() << std::endl;
    TestSElem->isValid(true);
    std::cout << " Set Valid, Got: V" << TestSElem->isValid() << std::endl;
    std::cout << " Get Size (bytes): " << TestSElem->size() << std::endl;
    std::cout << " " << TestSElem->toString() << std::endl;
    delete TestSElem;

    std::cout << "SElemSet (using TightSElem)..." << std::endl;
    SElemSet* TestSet = new SElemSet(128);
    std::cout << " Getting getting length (SElems): " << TestSet->length() << std::endl;
    std::cout << " Getting size (bytes): " << TestSet->size() << std::endl;
    delete TestSet;

    std::cout << "Testing SElem copy and operator override..." << std::endl;
    TightSElemInternType tmpdata2;
    TightSElemInternType tmpdata3;
    TestSElem = new TightSElem((SElem::DataSource)&tmpdata2);
    TestSElem->rgb(123,123,123);
    SElem* TestSElem2 = new TightSElem((SElem::DataSource)&tmpdata3);
    *TestSElem2 = *TestSElem;
    TestSElem2->red(62);
    TestSElem2->blue(66);
    std::cout << " TestSElem:  " << TestSElem->red() << "," << TestSElem->green() << "," << TestSElem->blue() << std::endl;
    std::cout << " TestSElem2: " << TestSElem2->red() << "," << TestSElem2->green() << "," << TestSElem2->blue() << std::endl;
    delete TestSElem; delete TestSElem2;

    std::cout << "Testing SSpace (3-D)..." << std::endl;
    SCoordinate ImageCoords(3); ImageCoords.x(3); ImageCoords.y(3); ImageCoords.z(3);
    SSpace* TestImage = new SSpace(ImageCoords);
    std::cout << "Created test image OK." << std::endl;
    ImageCoords.x(1); ImageCoords.y(1); ImageCoords.z(1);
    std::cout << "Testing Coords: " << ImageCoords.getDim() << ": " << ImageCoords.x() << ", " << ImageCoords.y() << ", " << ImageCoords.z() << std::endl;
    std::cout << "Fetched Extent: " << TestImage->extent().toString() << std::endl;
    std::cout << "Getting SElem in SSpace..." << std::endl;
    SCoordinate ImageCoords2(3); ImageCoords2.x(1); ImageCoords2.y(1); ImageCoords2.z(1);
    SElem::Ptr FetchedSElem = TestImage->getNativeSElem();
    FetchedSElem->source(TestImage->SElemData(ImageCoords2));
    std::cout << "Get SElem done." << std::endl;
    std::cout << " RGBI: " << FetchedSElem->red() << ", " << FetchedSElem->green() << ", " << FetchedSElem->blue() << ", " << FetchedSElem->intensity() << std::endl;
    std::cout << "Setting some values in the SElem (and refetching)..." << std::endl;

    TightSElemInternType tmpdata4;
    TightSElem tmpselem((SElem::DataSource)&tmpdata4);
    SElem::Ptr     targetp = TestImage->getNativeSElem();
    tmpselem.red(123); tmpselem.green(123); tmpselem.blue(22); tmpselem.intensity(321);
    targetp->source(TestImage->SElemData(ImageCoords2));
    *targetp = tmpselem;
    FetchedSElem->source(TestImage->SElemData(ImageCoords2));
    std::cout << "Get SElem done." << std::endl;
    std::cout << " RGBI: " << FetchedSElem->red() << ", " << FetchedSElem->green() << ", " << FetchedSElem->blue() << ", " << FetchedSElem->intensity() << std::endl;
    TestImage->resize(ImageCoords2);
    delete TestImage;

    std::cout << "File IO (SBPM) test..." << std::endl;
    SIO* FileTest = new SPBM();
    FileTest->setLocation("TestData/test_image.ppm");
    if (FileTest->isValid()) std::cout << "File opened successfully!" << std::endl;
    else throw SimulacrumExceptionGeneric("Failed to open file!");
    delete FileTest;

    std::cout << "Dimension iterator: " << std::endl;
    SCoordinate citerator = SCoordinate(3);
    SCoordinate constraint = SCoordinate(3);
    constraint.x(2); constraint.y(2); constraint.z(0);
    do {
      std::cout << "Iterator: " << citerator.toString() << std::endl;
    }while (citerator.tabincrement(constraint));

    std::cout << "Loading Image Data..." << std::endl;
    TestImage = new SSpace();
    FileTest  = new SPBM();
    FileTest->setLocation("TestData/test_image.ppm");
    FileTest->loadSSpace(*TestImage);
    std::cout << TestImage->extent().toString() << std::endl;
    delete TestImage;
    delete FileTest;

    std::cout << "Testing XML Reading (small test)" << std::endl;
    NNode xmltest = NNode();
    NNode::XMLParseRes parseresult;
    parseresult = xmltest.loadFromXMLFile("TestData/XMLTest.xml");
    xmltest.printTree();
    if(xmltest.good())
      std::cout << "XML tree is good." << std::endl;
    else
      SimulacrumExceptionGeneric("XML tree is bad.");
    //now output result info
    std::cout << "   Line: " << parseresult.Line + 1 << std::endl
              << " Column: " << parseresult.Column + 1 << std::endl
              << "  Token: " << parseresult.Token_str << std::endl
              << "  State: " << parseresult.State_str  << std::endl
              << " Messages:" << std::endl
              << "  " << parseresult.Messages << std::endl;
    std::cout << "Storing XML tree...";
    if(xmltest.storeToXMLFile("TestData/store_test.xml"))
      std::cout << "Success!" << std::endl;
    else
      SimulacrumExceptionGeneric("XML Store Fail!");
    xmltest.clear();

    std::cout << std::endl << "**TEST COMPLETE**" << std::endl;
    return 0;
}
