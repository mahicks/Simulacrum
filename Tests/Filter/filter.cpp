#include <iostream>
#include <simulacrum.h>

using namespace Simulacrum;

int main(int argc, char **argv) {
  int status = 1;
  if (argc < 3 ) {
    std::cerr << "ERROR: requires input file and output file!" << std::endl;
    status = 1;
  }
  // paramters seem to be good, now do the business
  else {
    // load the data
    SSpace input_space, output_space;
    SC input_ext, output_ext;
    SResourceAllocator::loadInto(input_space,argv[1]);
    input_ext = input_space.extent();
    std::cout << "Input  Extent: " << input_ext.toString()
              << std::endl;
    // apply a simple filter
    SFilter fmap( [] (SSpaceIterator&, SElem& in, SElem& out) { out = in; } );
    fmap({input_space,output_space});
    // store output data
    output_ext = output_space.extent();
    SResource::storeInto(output_space, SPNG(), argv[2]);
    std::cout << "Output Extent: " << output_ext.toString()
              << std::endl;
    status = !(input_ext == output_ext);
  }
  return status;
}
