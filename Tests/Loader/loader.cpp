#include <iostream>

#include <Core/sresource.h>
#include <Core/sspace.h>
#include <Toolbox/SResourceAllocator/sresourceallocator.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SLogger/slogger.h>

using namespace Simulacrum;

int main (int argc, char *argv[]) {
  if (argc != 4) {
    std::cerr << argv[0] << " <SResource> <SLuaLoader> <Path>" << std::endl;
    return 1;
  }
  SLogger::global().setStream(std::cerr);
  SResource *resource = SResourceAllocator::SResourceFromPath(argv[1]);
  resource->load();
  if (resource != NULL) {
    resource->setLoader(new SResourceSLuaLoader(SFile(argv[2]).toString()));
    std::cout << "===============================================" << std::endl;
    std::cout << "               sspaceListTitle" << std::endl;
    std::cout << "===============================================" << std::endl;
    std::cout << ">> " << resource->loader().sspaceListTitle(argv[3])
              << std::endl;
    std::cout << "===============================================" << std::endl;
    std::cout << "                  sspaceList" << std::endl;
    std::cout << "===============================================" << std::endl;
    std::vector<std::string> spacelist = resource->loader().sspaceList(argv[3]);
    for (unsigned i=0; i < spacelist.size(); i++)
      std::cout << ">> " << i << ": " << spacelist[i] << std::endl;
    std::cout << "===============================================" << std::endl;
    std::cout << "                 getSSpacesInto" << std::endl;
    std::cout << "===============================================" << std::endl;
    for (unsigned i=0; i < spacelist.size(); i++) {
      SSpace tmpspace;
      resource->loader().getSSpaceInto(tmpspace,argv[3],spacelist[i]);
      std::cout << ">> " << i << ": " << tmpspace.getName() << "\t : " 
                << tmpspace.extent().toString() << std::endl;
    }
    std::cout << "===============================================" << std::endl;
    std::cout << "---------------------END-----------------------" << std::endl;
    std::cout << "===============================================" << std::endl;
    delete resource;
  }
  else {
    return 2;
  }
}
