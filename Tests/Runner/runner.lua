-- Program to execute various tests and check the results
-- !! RUN FROM TOP-LEVEL TESTS DIRECTORY !!

-- TestSchedule: just the overall list of tests to actually run
function TestSchedule()
  local status = PASSVAL
  local function tally(res)
    if (not (res == PASSVAL)) then
      status = FAILVAL
    end
  end
  -- begin schedule ----------------------------------------------------------
  tally(ExecuteTest("UTs",     Extern("UnitTests/UnitTests")))
  tally(ExecuteTest("Core",    Extern("Core/Core")))
  tally(ExecuteTest("Filter",  Extern("Filter/Filter TestData/test_image.ppm TestData/test_image_out.png")))
  tally(ExecuteTest("Loader",  Extern("Loader/Loader TestData/BRAINSGS.gz TestData/TestLoader.lua /")))
  tally(ExecuteTest("SPool",   Extern("SPool/SPool")))
  tally(ExecuteTest("DCMDump", Extern("../Applications/dcmtool/dcmtool TestData/test_dicom.dcm -V")))
  tally(ExecuteTest("DCMRW",   Extern("../Applications/dcmtool/dcmtool -a rewrite TestData/test_dicom.dcm -o TestData/test_rewrite.dcm -V")))
  tally(ExecuteTest("DCMRWD",  Extern("../Applications/dcmtool/dcmtool TestData/test_rewrite.dcm -V")))
  tally(ExecuteTest("SLUA",    Extern("../Applications/SLua/SLua TestData/list_wrapped_api.lua")))
  -- end schedule   ----------------------------------------------------------
  return status
end

-- ----- Globals -------------------------------------------------------------
require "os"
-- PASS and FAIL values can be different by platform
PASSVAL=true
FAILVAL=nil
ABORTIMMED=false
TESTLOG=""
-- ---------------------------------------------------------------------------

-- ExecuteTest method will execute a named test
-- and write some interesting information to the stdout (name, duration, pass)
function ExecuteTest(name,test)
  local runtime   = os.time()
  local result    = test()
  runtime         = os.time() - runtime
  local resultstr = "FAIL"
  if (result == PASSVAL) then
    resultstr = "PASS"
  end
  logstr = name .. "\t\t" .. runtime .. "s\t\t" .. resultstr
  print(logstr)
  TESTLOG = TESTLOG .. logstr .. "\n"
  if (ABORTIMMED and not (result==PASSVAL)) then
    os.exit(FAILVAL)
  end
  return result
end

-- Extern: wrap an external program as a nameless function
function Extern(path)
  local wrapped = function()
    return os.execute(path)
  end
  return wrapped
end

-- Start the overall test framework
result = ExecuteTest("Overall",TestSchedule)
print()
print("----------------------------")
print("|         Results          |")
print("----------------------------")
print("Name\t\tTime\t\tResult")
print("----\t\t----\t\t------")
print(TESTLOG)
if (result == FAILVAL) then
  os.exit(1)
else
  os.exit(0)
end

