/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Implementation for DICOM DataDictionaries
 * Author: M.A.Hicks
 */

#include "datadic.h"
#include "dcmtag.h"
#include <Toolbox/sysInfo/sysInfo.h>

#include "types.h"
#include <sstream>
#include <iomanip>
#include <string.h>
#include <cstdlib>
#include <locale>

using namespace Simulacrum;

/*----------------------- Data Dictionary Storage --------------------------- */

class DCMDataDic::DCMDataDicPIMPL {
public:
  dcm_data_dic_t        Dictionary;
  static
  dcm_data_dic_t       &BuiltInDictionary;
  static const
  std::string           DictionaryTitle;
  static const
  std::string           DictionaryEntryName;
  static const unsigned PseudoIDLength = 8;
  NNode                 dicNodetoNNode(const dcm_info_store&);
  dcm_info_store        nnodeToDicNode(NNode&);
};

#ifdef NODCMDIC
void init_DCM_DATA_DIC(dcm_data_dic_t&) {
  return;
}
#else
#include "dcm_data_dic.h"
#endif
dcm_data_dic_t DCM_DATA_DIC;
dcm_data_dic_t &DCMDataDic::DCMDataDicPIMPL::BuiltInDictionary = DCM_DATA_DIC;

DCMDataDic::DCMDataDic(): PrivateData(new DCMDataDicPIMPL()) {
  if (PrivateData->BuiltInDictionary.size() == 0)
    init_DCM_DATA_DIC(PrivateData->BuiltInDictionary);
  srand(sysInfo::time());
}

DCMDataDic::~DCMDataDic() {
  clear();
  delete PrivateData;
}

void DCMDataDic::clear(bool builtin) {
  for (dcm_data_dic_t::iterator it = PrivateData->Dictionary.begin();
        it != PrivateData->Dictionary.end(); it ++)
    delete it->second.Name;
  PrivateData->Dictionary.clear();
  if (builtin) {
    PrivateData->BuiltInDictionary.clear();
  }
}

bool DCMDataDic::contains(DICOM_ID_LENGTH tagid) {
  if (PrivateData->Dictionary.count(tagid) ||
      PrivateData->BuiltInDictionary.count(tagid))
    return true;
  else
    return false;
}

bool DCMDataDic::contains(DICOM_ID_PART_LENGTH lid1,DICOM_ID_PART_LENGTH lid2) {
  DCMTag tmp;
  tmp.setID(lid1,lid2);
  return contains(tmp.getID());
}

static bool strcompare(const std::string str1, const std::string str2,
                       bool casesen = true) {
  if (casesen) {
    return str1 == str2;
  }
  else {
    std::locale loc;
    std::string str1l = str1;
    std::string str2l = str2;
    for (size_t p=0; p<str1l.size(); p++)
      str1l[p] = std::toupper(str1l[p],loc);
    for (size_t p=0; p<str2l.size(); p++)
      str2l[p] = std::toupper(str2l[p],loc);
    return str1l == str2l;
  }
}

bool DCMDataDic::contains(const std::string& lookupname, bool casesen) {
  // f*cking slow linear lookup - sort this out later (another sorted map?)
  dcm_data_dic_t::iterator dcmdicit;
  for (dcmdicit=PrivateData->Dictionary.begin();
       dcmdicit != PrivateData->Dictionary.end();dcmdicit++)
    if (strcompare((*dcmdicit).second.Name,lookupname,casesen))
      return true;
  for (dcmdicit=PrivateData->BuiltInDictionary.begin();
       dcmdicit != PrivateData->BuiltInDictionary.end();dcmdicit++)
    if (strcompare((*dcmdicit).second.Name,lookupname,casesen))
      return true;
  return false;
}

const dcm_info_store& DCMDataDic::getEntry(DICOM_ID_LENGTH tagid) {
  if (PrivateData->Dictionary.count(tagid))
    return PrivateData->Dictionary[tagid];
  else if (PrivateData->BuiltInDictionary.count(tagid))
    return PrivateData->BuiltInDictionary[tagid];
  throw SimulacrumDCMDictionaryException();
}

const dcm_info_store& DCMDataDic::getEntry(DICOM_ID_PART_LENGTH lid1,
                                           DICOM_ID_PART_LENGTH lid2) {
  DCMTag tmp;
  tmp.setID(lid1,lid2);
  return getEntry(tmp.getID());
}

const dcm_info_store& DCMDataDic::getEntry(const std::string& lookupname,
                                           bool casesen) {
  // f*cking slow linear lookup - sort this out later (another sorted map?)
  dcm_data_dic_t::iterator dcmdicit;
  for (dcmdicit=PrivateData->Dictionary.begin();dcmdicit !=
       PrivateData->Dictionary.end();dcmdicit++)
    if (strcompare((*dcmdicit).second.Name,lookupname,casesen))
      return (*dcmdicit).second;
  for (dcmdicit=PrivateData->BuiltInDictionary.begin();
       dcmdicit != PrivateData->BuiltInDictionary.end();dcmdicit++)
    if (strcompare((*dcmdicit).second.Name,lookupname,casesen))
      return (*dcmdicit).second;
  throw SimulacrumDCMDictionaryException();
}

void DCMDataDic::addEntry(dcm_info_store& newentry, bool toglobal) {
  if (toglobal)
    PrivateData->BuiltInDictionary.insert
                      (std::pair<DICOM_ID_LENGTH,dcm_info_store>
                      (newentry.ID,newentry));
  else
    PrivateData->Dictionary.insert
                      (std::pair<DICOM_ID_LENGTH,dcm_info_store>
                      (newentry.ID,newentry));
}

void DCMDataDic::addEntry(DICOM_ID_LENGTH lid, const std::string& vr,
                          const std::string& name, char sens) {
  dcm_info_store tmp;
  tmp.ID    = lid;
  tmp.VR[0] = vr[0];
  tmp.VR[1] = vr[1];
  char *tmpname = new char[name.size()+1];
  if (name.length() > 0)
    strcpy(tmpname,&name[0]);
  tmpname[name.size()] = '\0';
  tmp.Name = tmpname;
  tmp.Sensitivity = sens;
  addEntry(tmp);
}

void DCMDataDic::addEntry(DICOM_ID_PART_LENGTH lid1, DICOM_ID_PART_LENGTH lid2,
                          const std::string& vr, const std::string& name,
                          char sens) {
  DCMTag tmp;
  tmp.setID(lid1,lid2);
  addEntry(tmp.getID(),vr,name,sens);
}

const std::string DCMDataDic::DCMDataDicPIMPL::DictionaryTitle = 
                                                "SimulacrumDICOMDataDictionary";
const std::string DCMDataDic::DCMDataDicPIMPL::DictionaryEntryName = "Entry";

NNode DCMDataDic::DCMDataDicPIMPL::dicNodetoNNode
                                            (const dcm_info_store& sourcenode) {
  NNode  targetnode;
  DCMTag convert;
  std::stringstream conv;
  conv << sourcenode.Sensitivity;
  convert.setID(sourcenode.ID);
  convert.setVR(sourcenode.VR[0],sourcenode.VR[1]);
  convert.setName(sourcenode.Name);
  targetnode.setName(DictionaryEntryName);
  targetnode.setAttribute("ID",convert.getIDstr());
  targetnode.setAttribute("VR",convert.getVR());
  targetnode.setAttribute("Name",convert.getName());
  targetnode.setAttribute("Sensitivity",conv.str());
  return targetnode;
}

dcm_info_store DCMDataDic::DCMDataDicPIMPL::nnodeToDicNode(NNode& sourcenode) {
  dcm_info_store targetstore;
  targetstore.VR[0] = ' ';
  targetstore.VR[1] = ' ';
  DCMTag convert;
  std::string vrtmp;
  convert.setID(sourcenode.getAttribute("ID"));
  targetstore.ID = convert.getID();
  vrtmp = sourcenode.getAttribute("VR");
  if (vrtmp.length() >= 2) {
    targetstore.VR[0] = vrtmp[0];
    targetstore.VR[1] = vrtmp[1];
  }
  targetstore.Name = new char[sourcenode.getAttribute("Name").length()+1];
  if (sourcenode.getAttribute("Name").length() > 0)
    strcpy(targetstore.Name,&(sourcenode.getAttribute("Name")[0]));
  targetstore.Name[sourcenode.getAttribute("Name").length()] = '\0';
  if (sourcenode.getAttribute("Sensitivity").length() > 0)
    targetstore.Sensitivity = sourcenode.getAttribute("Sensitivity")[0];
  return targetstore;
}

void DCMDataDic::loadFromFile(const std::string& filename, bool global) {
  NNodeResource indic;
  indic.setLocation(filename);
  indic.load();
  if (indic.getRootNode().hasChildNode(PrivateData->DictionaryTitle)) {
    NNode &NodeList = indic.getRootNode().getChildNode
                                                 (PrivateData->DictionaryTitle);
    for ( NNode::ChildMap::iterator it = NodeList.getChildNodes().begin();
         it != NodeList.getChildNodes().end(); it++) {
      dcm_info_store newitem = PrivateData->nnodeToDicNode(*(it->second));
      addEntry(newitem, global);
    }
  }
}

void DCMDataDic::storeToFile(const std::string& filename, bool internal) {
  NNodeResource outdic;
  outdic.setLocation(filename);
  NNode *rootdic = new NNode;
  rootdic->setName(PrivateData->DictionaryTitle);
  outdic.getRootNode().addChildNode(rootdic);
  if (internal)
    for (dcm_data_dic_t::iterator it = PrivateData->BuiltInDictionary.begin();
        it != PrivateData->BuiltInDictionary.end(); it ++) {
      NNode newnode = PrivateData->dicNodetoNNode(it->second);
      rootdic->addChildNode(newnode);
    }
  for (dcm_data_dic_t::iterator it = PrivateData->Dictionary.begin();
        it != PrivateData->Dictionary.end(); it ++) {
      NNode newnode = PrivateData->dicNodetoNNode(it->second);
      rootdic->addChildNode(newnode);
    }
  outdic.store();
}

void DCMDataDic::pseudonTag(DCMTag& target, NNode& pseudodb) {
  if (!pseudodb.hasChildNode(target.getIDstr())) {
    // add type node
    NNode newnode;
    newnode.setName(target.getIDstr());
    pseudodb.addChildNode(newnode);
  }
  NNode &mapnode = pseudodb.getChildNode(target.getIDstr());
  if (!mapnode.hasChildNode(target.toString())) {
    // add instance node
    NNode newmap;
    newmap.setName(target.toString());
    std::stringstream numberstr;
    numberstr << rand();
    std::string finalstr = numberstr.str();
    if (finalstr.length() > DCMDataDicPIMPL::PseudoIDLength)
      finalstr = finalstr.substr(0,DCMDataDicPIMPL::PseudoIDLength-1);
    else
      if (finalstr.length() < DCMDataDicPIMPL::PseudoIDLength) {
        std::string padstr;
        padstr.resize(DCMDataDicPIMPL::PseudoIDLength-finalstr.length(),' ');
        finalstr = padstr + finalstr;
      }
    newmap.setData(finalstr);
    mapnode.addChildNode(newmap);
  }
  target.fromString(mapnode.getChildNode(target.toString()).getData());
}

bool DCMDataDic::deidentifyTagTree(DCMTag& target,
                                   NNode&  psedonmap,
                                   bool    removeprivate,
                                   bool    recurse ) {
  bool didremove = false;
  if ( removeprivate && target.isPrivate() ) {
    target.removeNode();
    didremove = true;
  }
  else {
    DCMSens::DCMSensType deidentform = DCMSens::UNKNOWN;
    if (contains(target.getID()))
      deidentform = getEntry(target.getID()).Sensitivity;
    switch (deidentform) {
      case DCMSens::REMOVE:
        target.removeNode();
        didremove = true;
        break;
      case DCMSens::BLANK:
        target.NodeValue("");
        break;
      case DCMSens::PSEUDON:
        pseudonTag(target,psedonmap);
        break;
      case DCMSens::LEAVE:
        break;
      case DCMSens::UNKNOWN:
      default:
        break;
    }
    if (recurse & !didremove)
      for (int i=0; (unsigned)i<target.getTags().size();i++) {
        if (
          deidentifyTagTree(*target.getTags()[i],psedonmap, removeprivate,
                            recurse)
           )
          i--;
      }
  }
  return didremove;
}

bool DCMDataDic::isTagTreeDeidentified(DCMTag& isdeident) {
  return isdeident.hasTag(0x00120062);
}

void DCMDataDic::setTagTreeDeidentified(DCMTag& target, bool isdeident) {
  if (isdeident) {
    DCMTag deident, deidentmethod;
    deident.setID(0x00120062);
    deident.setVR('C','S');
    deident.fromString("YES ");
    deidentmethod.setID(0x00120063);
    deidentmethod.setVR('L','O');
    deidentmethod.fromString("Simulacrum");
    target.addTag(deident);
    target.addTag(deidentmethod);
  }
  else {
    if (target.hasTag(0x00120062))
      target.removeTag(0x00120062);
    if (target.hasTag(0x00120063))
      target.removeTag(0x00120063);
  }
}

const char* DCMDataDic::pseudonDBName() {
  return "Simulacrum-PseudoDB";
}

DICOM_ID_LENGTH DCMDataDic::toID(const std::string& tagname) {
  DCMDataDic lookup;
  DICOM_ID_LENGTH retval = 0xFFFFFFFF;
  if (lookup.contains(tagname))
    retval = lookup.getEntry(tagname).ID;
  return retval;
}

std::string DCMDataDic::toIDStr(const std::string& tagname) {
  DCMDataDic lookup;
  std::string retval = "FFFF,FFFF";
  if (lookup.contains(tagname)) {
    std::stringstream conv;
    DCMTag tmptag;
    tmptag.setID(lookup.getEntry(tagname).ID);
    conv << std::hex << tmptag.getID1() << "," << tmptag.getID2();
    retval = conv.str();
  }
  return retval;
}

std::string DCMDataDic::toName(const std::string& tagid) {
  DCMDataDic lookup;
  std::string retval = "UnknownTag";
  if (lookup.contains(tagid))
    retval = lookup.getEntry(tagid).Name;
  return retval;
}

std::string DCMDataDic::toName(DICOM_ID_LENGTH tagid) {
  DCMDataDic lookup;
  std::string retval = "UnknownTag";
  if (lookup.contains(tagid))
    retval = lookup.getEntry(tagid).Name;
  return retval;
}
