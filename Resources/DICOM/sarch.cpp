/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:SArch
 * Implementation for DICOM Archive
 * Author: M.A.Hicks
 */

#include "sarch.h"
#include "sdicom-net.h"
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>

#include <fstream>
#include <sstream>

using namespace Simulacrum;

/*----------------------------- SDCMArchNode ---------------------------------*/

class SDCMArchNode::SDCMArchNodePIMPL {
public:
    DCMTag         *DCMRoot;
    SDCMArchNodeMap_t
                    ChildCacheMap;
    SDCMArchKeyMap_t
                    KVPCache;
    SDCMArchKeyMap_t
                    FileCache;
    SDCMArchNode*   ParentNode;
    void            initCaches();
    void            init();
    void            clearChildMap();
    SDCMArchNode*   NodeRef;
    SDCMArchNodePIMPL(SDCMArchNode* noderef): NodeRef(noderef) {}
};

SDCMArchNode::SDCMArchNode(DCMTag& roottag, SDCMArchNode* parent) :
                                      PrivateData(new SDCMArchNodePIMPL(this)) {
 PrivateData->DCMRoot = &roottag;
 PrivateData->ParentNode = parent;
 if (! isValid())
   PrivateData->init();
 else
   PrivateData->initCaches();
}

SDCMArchNode::~SDCMArchNode() {
  PrivateData->clearChildMap();
  delete PrivateData;
}

void SDCMArchNode::SDCMArchNodePIMPL::initCaches() {
  clearChildMap();
  KVPCache.clear();
  FileCache.clear();
  // update children
  for ( tagset_t::iterator childit =
          DCMRoot->getTag(ChildrenTag).getTags().begin();
        childit !=
          DCMRoot->getTag(ChildrenTag).getTags().end();
        childit++ ) {
    if ( !((**childit).getID() == SequenceDelim) ) {
      SDCMArchNode *newchild = new SDCMArchNode((**childit).getTag(NodeTag),
                                                NodeRef);
      ChildCacheMap.insert(std::pair<std::string,SDCMArchNode*>
                            (newchild->getKeyStr(),newchild) );
    }
  }
  // update KVP
  for ( tagset_t::iterator childit =
          DCMRoot->getTag(KVPTag).getTags().begin();
        childit !=
          DCMRoot->getTag(KVPTag).getTags().end();
        childit++ ) {
    if ( !((**childit).getID() == SequenceDelim) )
      if ( (*childit)->isArray() )
        KVPCache[(*childit)->getTag(KVPElemTag).getElement(0)] = (*childit);
  }
  // update Files
  for ( tagset_t::iterator childit =
          DCMRoot->getTag(FilesTag).getTags().begin();
        childit !=
          DCMRoot->getTag(FilesTag).getTags().end();
        childit++ ) {
    if ( !((**childit).getID() == SequenceDelim) )
      FileCache[(*childit)->getTag(FileObjTag).toString()] = (*childit);
  }
}

bool SDCMArchNode::isValid() {
  bool isvalid;
  if ( (PrivateData->DCMRoot->hasTag(KeyTag)) ) isvalid = true;
  else isvalid = false;
  return isvalid;
}

bool SDCMArchNode::operator==(const SDCMArchNode& rhs) const {
  return PrivateData->DCMRoot == rhs.PrivateData->DCMRoot;
}

void SDCMArchNode::SDCMArchNodePIMPL::init() {
  // build clean structure
  DCMTag *RootClean,*key,*type,*fspath,*kvp,*general,*files,*children;
  DCMRoot->clear();
  RootClean = DCMTag::genSequenceTag(NodeTag);
  *DCMRoot = *RootClean;
  delete RootClean;
  DCMTag *SArchMembers = DCMTag::genItemTag();
  DCMRoot->addTag(SArchMembers);
  key = DCMTag::genIntTag(KeyTag);
  SArchMembers->addTag(key);
  type = new DCMTag();
  type->setID(TypeTag);
  type->setVR('U','L');
  SArchMembers->addTag(type);
  fspath = new DCMTag();
  fspath->setID(FSTag);
  fspath->setVR('L','O');
  SArchMembers->addTag(fspath);
  kvp = DCMTag::genSequenceTag(KVPTag);
  SArchMembers->addTag(kvp);
  general = DCMTag::genSequenceTag(GeneralTag);
  general->addTag(DCMTag::genItemTag());
  SArchMembers->addTag(general);
  files = DCMTag::genSequenceTag(FilesTag);
  SArchMembers->addTag(files);
  children = DCMTag::genSequenceTag(ChildrenTag);
  SArchMembers->addTag(children);
  initCaches();
}

DICOM_ID_LENGTH SDCMArchNode::getKeyID() {
  return (DICOM_ID_LENGTH) PrivateData->DCMRoot->getTag(KeyTag).toInt();
}

DCMTag& SDCMArchNode::getKeyTag() {
  return getTag(getKeyID());
}

const char* SDCMArchNode::getKeyName() {
  return getKeyTag().getName();
}

std::string SDCMArchNode::getKeyStr() {
  return getKeyTag().toString();
}

bool SDCMArchNode::hasKey() {
  return hasTag(getKeyID());
}

void SDCMArchNode::setKey(DICOM_ID_LENGTH newkey) {
  bool hasparent = !isTop();
  SDCMArchNode *parentn;
  if (hasparent) {
    parentn = &getParentNode();
    if (parentn->PrivateData->ChildCacheMap.count(getKeyStr()) > 0)
      parentn->PrivateData->ChildCacheMap.erase(getKeyStr());
    else { 
      // check iteratively, in case of inconsistency
      bool found = false;
      for (SDCMArchNodeMap_t::iterator it = 
                                    parentn->PrivateData->ChildCacheMap.begin();
           it != parentn->PrivateData->ChildCacheMap.end(); it++) {
        if (it->second == this) {
          parentn->PrivateData->ChildCacheMap.erase(it);
          found = true;
          break;
        }
      }
      hasparent = found;
    }
  }
  PrivateData->DCMRoot->getTag(KeyTag).fromInt(newkey);
  getKeyID();
  if (hasparent) {
    parentn->PrivateData->ChildCacheMap.insert
                                             (std::make_pair(getKeyStr(),this));
  }
}

SDCMArchNode_t SDCMArchNode::getType() {
  return (SDCMArchNode_t) PrivateData->DCMRoot->getTag(TypeTag).toInt();
}

void SDCMArchNode::setType(SDCMArchNode_t newtype) {
  PrivateData->DCMRoot->getTag(TypeTag).fromInt(newtype);
}

SDCMArchNode& SDCMArchNode::getParentNode() {
  return *(PrivateData->ParentNode);
}

bool SDCMArchNode::isTop() const {
  return PrivateData->ParentNode == nullptr;
}

std::string SDCMArchNode::getFSPath() {
  std::string fspath;
  PrivateData->DCMRoot->getTag(FSTag).toString(fspath);
  return fspath;
}

std::string SDCMArchNode::getFullFSPath() {
  std::string curpath;
  if (!isTop())
    curpath = getParentNode().getFullFSPath();
  std::string thispath = getFSPath();
  if (thispath.length() > 0) {
    SURI tmpuri(curpath);
    tmpuri.addComponentBack(thispath);
    curpath = tmpuri.getURI();
  }
  return curpath;
}

void SDCMArchNode::setFSPath(const std::string& newfspath) {
  PrivateData->DCMRoot->getTag(FSTag).fromString(newfspath);
}

std::string SDCMArchNode::getPath() {
  SURI path;
  SDCMArchNode *node = this;
  while ( ! node->isTop() ) {
    path.addComponentFront(node->getKeyStr());
    node = &(node->getParentNode());
  }
  return path.getURI();
}

bool SDCMArchNode::pathExists(const std::string& pathstr) {
  SURI luri(pathstr);
  if (luri.depth() > 0) {
    if (hasChildNode(luri.getComponent(0))) {
      if ( luri.depth() == 1 )
        return true;
      else {
        std::string childnode = luri.getComponent(0);
        luri.deleteComponent(0);
        return getChildNode(childnode).pathExists(luri.getURI(true));
      }
    }
    else
      return false;
  }
  else
    return true;
}

SDCMArchNode& SDCMArchNode::getByPath(const std::string& pathstr) {
  SURI luri(pathstr);
  if (luri.depth() > 0) {
    std::string tmplookup = luri.getComponent(0);
    if (luri.depth() == 1)
      return getChildNode(tmplookup);
    else {
      luri.deleteComponent(0);
      return getChildNode(tmplookup).getByPath(luri.getURI(true));
    }
  }
  else
    return *this;
}

SDCMArchNode& SDCMArchNode::makePath(std::list< DCMTag* > &pathkeys) {
  std::string idtmp = pathkeys.front()->toString();
  SDCMArchNode *thechild = getChildNodeP(idtmp);
  if ( thechild == nullptr ) {
    thechild = &genChildNode(*(pathkeys.front()),false);
  }
  pathkeys.pop_front();
  thechild = getChildNodeP(idtmp);
  if (!pathkeys.empty())
    return thechild->makePath(pathkeys);
  else
    return *thechild;
}

DCMTag& SDCMArchNode::getTag(DICOM_ID_LENGTH lookupid, bool safe) {
  if (safe  && (!hasTag(lookupid))) {
    throw SimulacrumDCMPresenceException();
  }
  return PrivateData->DCMRoot->getTag(GeneralTag).getTag(lookupid);
}

DCMTag* SDCMArchNode::findTag(DICOM_ID_LENGTH lid, bool useexemplars,
                              bool usefiles) {
  // check tags here and recursively, until reaching childless node
  // then, consider: exemplars and/or attached files
  DCMTag *result = nullptr;
  if (hasTag(lid)) {
    result = &getTag(lid);
  }
  else {
    if (!PrivateData->ChildCacheMap.empty()) {
      // recurse on children
      for ( SDCMArchNodeMap_t::iterator it = PrivateData->ChildCacheMap.begin();
            it != PrivateData->ChildCacheMap.end(); it ++) {
        result = it->second->findTag(lid, useexemplars,usefiles);
        if (result != nullptr) break;
      }
    }
    else {
      if (!hasTag(ExemplarTagSet)) {
        // decode the exemplar
        if (hasTag(ExemplarTag)) {
          DCMTag *decexmpl = SDICOMArch::decExemplar(getTag(ExemplarTag));
          // we don't want to save this expanded tagset
          decexmpl->setShouldSave(false);
          addTag(decexmpl);
        }
      }
      // check again, in case something went wrong
      if(hasTag(ExemplarTagSet)) {
        DCMTag &ExmplTag = getTag(ExemplarTagSet);
        if (ExmplTag.hasTagDFS(lid))
          result = &(ExmplTag.getTagDFS(lid));
      }
    }
  }
  return result;
}

tagset_t& SDCMArchNode::getTags() {
  return PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker).getTags();
}

bool SDCMArchNode::hasTag(DICOM_ID_LENGTH lookupid) {
  return PrivateData->DCMRoot->getTag(GeneralTag).hasTag(lookupid);
}

std::string SDCMArchNode::toString() {
  return getKeyTag().toString();
}

bool SDCMArchNode::hasChildNode(const std::string& keyvalue) {
  return PrivateData->ChildCacheMap.count(keyvalue);
}

SDCMArchNode& SDCMArchNode::getChildNode(const std::string& keyvalue) {
  return *(PrivateData->ChildCacheMap.find(keyvalue)->second);
}

SDCMArchNode* SDCMArchNode::getChildNodeP(const std::string& keyvalue) {
  SDCMArchNode *retval = nullptr;
  SDCMArchNodeMap_t::iterator lookup = 
                                      PrivateData->ChildCacheMap.find(keyvalue);
  if (lookup != PrivateData->ChildCacheMap.end())
    retval = lookup->second;
  return retval;
}

SDCMArchNodeMap_t& SDCMArchNode::getChildNodesMap() {
  return PrivateData->ChildCacheMap;
}

SDCMArchNode& SDCMArchNode::genChildNode(DCMTag& keytag, bool docheck) {
  DCMTag *newres = new DCMTag();
  SDCMArchNode newnode(*newres);
  newnode.addKeyTag(keytag);
  addChildNode(newnode, docheck);
  return getChildNode(keytag.toString());
}

void SDCMArchNode::addChildNode(SDCMArchNode& newnode, bool docheck) {
  if (docheck && hasChildNode(newnode.getKeyStr()))
    throw SDCMArchNodeException();
  PrivateData->DCMRoot->getTag(ChildrenTag).addItem
                                                 (newnode.PrivateData->DCMRoot);
  PrivateData->ChildCacheMap.insert(std::pair<std::string,SDCMArchNode*>
                       (newnode.getKeyStr(),
                        new SDCMArchNode(*newnode.PrivateData->DCMRoot, this)));
}

void SDCMArchNode::removeChildNode(SDCMArchNode& remnode) {
  if (PrivateData->ChildCacheMap.count(remnode.getKeyStr())) {
    delete getChildNodeP(remnode.getKeyStr());
    PrivateData->ChildCacheMap.erase(remnode.getKeyStr());
    remnode.PrivateData->DCMRoot->getParent().removeNode();
  }
}

void SDCMArchNode::addTag(DCMTag& newtag) {
  DCMTag *newtagtmp = new DCMTag();
  *newtagtmp = newtag;
  addTag(newtagtmp);
}

void SDCMArchNode::addTag(DCMTag* newtag) {
  PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker).addTag(newtag);
}


void SDCMArchNode::addKeyTag(DCMTag& newtag) {
  addTag(newtag);
  setKey(newtag.getID());
}

void SDCMArchNode::addKeyTag(DCMTag* newtag) {
  addTag(newtag);
  setKey(newtag->getID());
}

void SDCMArchNode::removeTag(DCMTag& newtag) {
  PrivateData->DCMRoot->getTag
                             (GeneralTag).getTag(ItemMarker).removeTag(&newtag);
}

void SDCMArchNode::removeTag(DICOM_ID_LENGTH lid) {
  if (PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker).hasTag(lid))
    PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker).removeTag(lid);
}

bool SDCMArchNode::hasKVP(const std::string& key) {
  return PrivateData->KVPCache.count(key);
}

std::string SDCMArchNode::getKeyValue(const std::string& key) {
  return PrivateData->KVPCache[key]->getElement(1,true);
}

SDCMArchKeyMap_t& SDCMArchNode::getKeyValues() {
  return PrivateData->KVPCache;
}

void SDCMArchNode::setKeyValue(const std::string& key, const std::string& val) {
  if (hasKVP(key)) {
    PrivateData->KVPCache[key]->fromString(key + DCMArraySep + val);
  }
  else {
    DCMTag *newkvp = DCMTag::genStringTag(KVPElemTag);
    newkvp->fromString(key + DCMArraySep + val);
    PrivateData->DCMRoot->getTag(KVPTag).addItem(newkvp);
    PrivateData->KVPCache[key] = newkvp;
  }
}

void SDCMArchNode::removeKeyValue(const std::string& key) {
  if (hasKVP(key)) {
    PrivateData->KVPCache[key]->getParent().removeNode();
    PrivateData->KVPCache.erase(key);
  }
}

void SDCMArchNode::addFile(const std::string& newfile, bool strippath) {
  if (PrivateData->FileCache.count(newfile) == 0) {
    SURI filepath(newfile);
    if (strippath) {
      SURI stripper(getFullFSPath());
      if (stripper.depth() < filepath.depth()) {
        while (stripper.depth() > 0) {
          if ( stripper.getComponent(0) == filepath.getComponent(0) ) {
            stripper.deleteComponent(0);
            filepath.deleteComponent(0);
          }
          else
            break;
        }
      }
    }
    DCMTag *newfiletag = DCMTag::genStringTag(FileObjTag);
    std::string finalpath = filepath.getURI();
    newfiletag->fromString(finalpath);
    PrivateData->FileCache[finalpath] =
                     PrivateData->DCMRoot->getTag(FilesTag).addItem(newfiletag);
  }
}

void SDCMArchNode::removeFile(const std::string& remfile) {
  if (PrivateData->FileCache.count(remfile) > 0) {
    PrivateData->FileCache[remfile]->getParent().removeNode();
    PrivateData->FileCache.erase(remfile);
  }
}

std::vector< std::string > SDCMArchNode::getFileList() {
  std::vector< std::string > retlist;
  for ( tagset_t::iterator childit =
          PrivateData->DCMRoot->getTag(FilesTag).getTags().begin();
        childit !=
          PrivateData->DCMRoot->getTag(FilesTag).getTags().end();
        childit++ ) {
    if ( (*childit)->hasTag(FileObjTag) )
      retlist.push_back((*childit)->getTag(FileObjTag).toString());
  }
  return retlist;
}

std::vector< std::string > SDCMArchNode::getFileListFullPath() {
  std::vector< std::string > filepathlist = getFileList();
  // fold in the path of each file
  SURI fullpath(getFullFSPath());
  for (unsigned i=0; i<filepathlist.size();i++) {
    SURI tmpfull = fullpath;
    tmpfull.addComponentBack(filepathlist[i]);
    filepathlist[i] = tmpfull.getURI();
  }
  return filepathlist;
}

void SDCMArchNode::clearKVP() {
  PrivateData->DCMRoot->getTag(KVPTag).clearTags();
  PrivateData->KVPCache.clear();
}

void SDCMArchNode::clearTags() {
  PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker).clearTags();
}

void SDCMArchNode::clearFiles() {
  PrivateData->DCMRoot->getTag(FilesTag).clearTags();
  PrivateData->FileCache.clear();
}

void SDCMArchNode::clearChildren() {
  PrivateData->DCMRoot->getTag(ChildrenTag).clearTags();
  PrivateData->clearChildMap();
}

void SDCMArchNode::SDCMArchNodePIMPL::clearChildMap() {
  for ( SDCMArchNodeMap_t::iterator it = ChildCacheMap.begin();
        it != ChildCacheMap.end();
        it++) {
       delete it->second;
  }
  ChildCacheMap.clear();
}

void SDCMArchNode::clear() {
  clearChildren();
  PrivateData->DCMRoot->clear();
  PrivateData->init();
}

/* Implementation of SAbsTreeNode methods */
std::string SDCMArchNode::NodeID() {
  return getKeyStr();
}

std::string SDCMArchNode::NodeType() {
  // change this to identify type name
  std::stringstream typestr;
  typestr << getType();
  return typestr.str();
}

unsigned long SDCMArchNode::NodeSize() {
  return 0;
}

std::string SDCMArchNode::NodeName() {
  return getKeyTag().NodeName();
}

std::string SDCMArchNode::NodeValue() {
  return getKeyStr();
}

void SDCMArchNode::NodeValue(const std::string& newval) {
  getKeyTag().NodeValue(newval);
}

bool SDCMArchNode::NodeEmph() {
  return true;
}

unsigned long SDCMArchNode::NodeChildrenNum(bool strict) {
  unsigned long children = PrivateData->ChildCacheMap.size();
  if (!strict) {
    children = children + PrivateData->KVPCache.size() +
                            PrivateData->FileCache.size();
    if ( ! (std::string(getKeyTag().getName()) == "SDCMArchive Resource" ||
        std::string(getKeyTag().getName()) == "ArchiveTitle" ) )
      children++;
  }
  return children;
}

bool SDCMArchNode::NodePathExists(const std::string& path) {
  return pathExists(path);
}

SAbsTreeNode& SDCMArchNode::NodeByPath(const std::string& path) {
  return getByPath(path);
}

SAbsTreeNode& SDCMArchNode::NodeParent() {
  return getParentNode();
}

bool SDCMArchNode::hasNodeParent() {
  return !isTop();
}

SAbsTreeNodeList_t SDCMArchNode::NodeChildren(bool strict, bool simpleonly) {
  std::vector<SAbsTreeNode*> retvec;
  if (!strict && !simpleonly) {
    if ( ! (std::string(getKeyTag().getName()) == "SDCMArchive Resource" ||
        std::string(getKeyTag().getName()) == "ArchiveTitle" ) ) {
      DCMTag *gentags = 
                 &(PrivateData->DCMRoot->getTag(GeneralTag).getTag(ItemMarker));
      gentags->setName("General Tags");
      retvec.push_back(gentags);
    }
    for (SDCMArchKeyMap_t::iterator fit = PrivateData->KVPCache.begin();
        fit != PrivateData->KVPCache.end(); fit++)
      retvec.push_back(&(fit->second->getTag(KVPElemTag)));
    for (SDCMArchKeyMap_t::iterator fit = PrivateData->FileCache.begin();
        fit != PrivateData->FileCache.end(); fit++)
      retvec.push_back(&(fit->second->getTag(FileObjTag)));
  }
  for (SDCMArchNodeMap_t::iterator tit = PrivateData->ChildCacheMap.begin();
       tit != PrivateData->ChildCacheMap.end(); tit++)
    retvec.push_back(tit->second);
  return retvec;
}

bool SDCMArchNode::NodeError() {
  return PrivateData->DCMRoot->NodeError();
}

void SDCMArchNode::detachNode() {
  if (!PrivateData->DCMRoot->isTop()) {
    DCMTag& parenttag = PrivateData->DCMRoot->getParent();
    PrivateData->DCMRoot->detachNode();
    // consider dangling items
    if (parenttag.getID() ==  ItemMarker)
      if (!parenttag.hasTags(true))
        parenttag.removeNode();
  }
  if (PrivateData->ParentNode->hasChildNode(NodeID()))
    PrivateData->ParentNode->PrivateData->ChildCacheMap.erase(NodeID());
  PrivateData->ParentNode = nullptr;
}

bool SDCMArchNode::removeNode() {
  detachNode();
  delete this;
  return true;
}

SAbsTreeNode& SDCMArchNode::NewChild() {
  DCMTag *newkey = DCMTag::genStringTag(DefaultString,"New Archive Node");
  SDCMArchNode &thenewone = genChildNode(*newkey);
  delete newkey;
  return thenewone;
}

std::map< std::string,std::string > SDCMArchNode::getAttributes() {
  std::map< std::string,std::string > tmpmap;
  tagset_t &tmpset = getTags();
  for (unsigned i=0; i < tmpset.size(); i++) {
    std::stringstream tmpconv;
    tmpconv << std::hex << tmpset[i]->getID();
    tmpmap[tmpconv.str()] = tmpset[i]->toString();
  }
  return tmpmap;
}

bool SDCMArchNode::hasAttribute(const std::string& attrname) {
  DCMTag tmptag;
  tmptag.setID(attrname);
  DCMTag *lookup = findTag(tmptag.getID());
  if (lookup != nullptr)
    return true;
  else
    return false;
}

std::string SDCMArchNode::getAttribute(const std::string& attrname) {
  DCMTag tmptag;
  tmptag.setID(attrname);
  DCMTag *lookup = findTag(tmptag.getID());
  if (lookup != nullptr)
    return lookup->toString();
  else
    return "";
}

SimulacrumLibrary::str_enc SDCMArchNode::stringEncoding() {
  SimulacrumLibrary::str_enc result = SimulacrumLibrary::str_enc::Unknown;
  DCMTag *lookup = findTag(DCMTag(0x0008,0x0005).getID(),true,false);
  if (lookup != nullptr) {
    result = lookup->stringEncoding();
  }
  return result;
}

/*-------------------------------- SDCMArch ----------------------------------*/

class SDCMArch::SDCMArchPIMPL {
public:
    SDICOM        SDCMArcResource;
    DCMTag        DCMStore;
    SDCMArchNode  RootCache;
    bool          isArch;
    static const  char*
                  MagicString;
    static const unsigned
                  MagicStringLength = 5;
    bool          DoStop;
    bool          EmbeddedLoaders;
    // used by derived classes:
    static
    const char*   SARCHTRANSX;
    std::string   RootKey;
    void          initRootCache(SDCMArch*);
    void          checkSetEmbeddedLoader(SDCMArch*);
    SDCMArchPIMPL(): RootCache(DCMStore), isArch(false) {}
};

const char *SDCMArch::SDCMArchPIMPL::SARCHTRANSX = "1.2.840.10008.1.2.1";
const char* SDCMArch::SDCMArchPIMPL::MagicString = "SARCH";

SDCMArch::SDCMArch() : PrivateData(new SDCMArchPIMPL()) {
  PrivateData->initRootCache(this);
  doStop(false);
  setEmbeddedLoadersEnabled(true);
}

SDCMArch::~SDCMArch() {
  PrivateData->RootCache.clear();
  delete PrivateData;
}

int SDCMArch::setLocation(const std::string& archloc) {
  PrivateData->SDCMArcResource.setLocation(archloc);
  /* Sniff the first 5-bytes to see if they contain the MagicString in the
   * DICOM preamble
   */
  std::fstream SArchFile;
  char mgcstr[PrivateData->MagicStringLength+1];
  for (unsigned i=0; i<PrivateData->MagicStringLength; i++)
    mgcstr[i] = ' ';
  mgcstr[PrivateData->MagicStringLength] = '\0';
  SArchFile.open(getLocation().c_str(), std::ios::in | std::ios::binary);
  SArchFile.read(mgcstr,PrivateData->MagicStringLength);
  if ( std::string(PrivateData->MagicString).compare
                      (std::string(mgcstr,PrivateData->MagicStringLength)) == 0)
    PrivateData->isArch = true;
  else
    PrivateData->isArch = false;
  PrivateData->initRootCache(this);
  return PrivateData->isArch;
}

void SDCMArch::changeLocation(const std::string& archloc) {
  PrivateData->SDCMArcResource.changeLocation(archloc);
  PrivateData->initRootCache(this);
}

bool SDCMArch::isValid() const {
  return PrivateData->SDCMArcResource.isValid() && PrivateData->isArch;
}

const std::string& SDCMArch::getLocation() const {
  return PrivateData->SDCMArcResource.getLocation();
}

void SDCMArch::loadArchive() {
  PrivateData->SDCMArcResource.loadAllTags(true);
  PrivateData->initRootCache(this);
  if (PrivateData->EmbeddedLoaders)
    PrivateData->checkSetEmbeddedLoader(this);
}

void SDCMArch::SDCMArchPIMPL::checkSetEmbeddedLoader(SDCMArch* targ) {
  if(SDCMArcResource.getRootTag().hasTag(EmbeddedLdrTag)) {
    targ->setLoader(new SResourceSLuaLoader(SDCMArcResource.getRootTag().
                                            getTag(EmbeddedLdrTag).toString()
                                           )
                   );
  }
}

void SDCMArch::storeArchive() {
  PrivateData->SDCMArcResource.setTransferSyntax(PrivateData->SARCHTRANSX);
  /* First 128 Chars of a DICOM file are unused - sneak in a cheeky magic number
   * at the beginning of the file (0x0) of 5 chars (SARCH)
   */
  PrivateData->SDCMArcResource.setPreambleString(PrivateData->MagicString);
  PrivateData->SDCMArcResource.writeAllTags();
  PrivateData->isArch = true;
}

void SDCMArch::store() {
  storeArchive();
}

void SDCMArch::SDCMArchPIMPL::initRootCache(SDCMArch* targ) {
  char *resname        = (char*)"SDCMArchive Resource";
  DCMTag *roottitle    = DCMTag::genStringTag(GeneralTag,targ->getLocation());
  roottitle->setName(resname);
  // now clear the whole thing
  RootCache.clear();
  // now set the name as the path
  RootCache.addKeyTag(roottitle);
  //generate a root tag if one does not exist
  if (! SDCMArcResource.getRootTag().hasTag(RootNodeTag)) {
    DCMTag *newroot = new DCMTag();
    SDCMArchNode tmp(*newroot);
    newroot->setID(RootNodeTag);
    SDCMArcResource.getRootTag().addTag(newroot);
  }
  // now add the root node as a child of
  SDCMArchNode tmproot(SDCMArcResource.getRootTag().getTag(RootNodeTag));
  if (! tmproot.hasKey()) {
    // add a dummy key
    tmproot.addKeyTag(DCMTag::genStringTag(DefaultString,"EmptyKey"));
  }
  RootCache.addChildNode(tmproot);
  // stop the erroneous double-free of DCMTag tree owned by SDICOM object
  if (DCMStore.hasTag(ChildrenTag))
    if (DCMStore.getTag(ChildrenTag).hasTag(ItemMarker))
      DCMStore.getTag(ChildrenTag).getTag(ItemMarker).setIsParent(false);
}

SDCMArchNode& SDCMArch::getRootNodeChild() {
  if (PrivateData->RootCache.getChildNodesMap().size() != 1)
    throw SDCMArchNodeException();
  return *(PrivateData->RootCache.getChildNodesMap().begin()->second);
}

SDCMArchNode& SDCMArch::getRootNode() {
  return PrivateData->RootCache;
}

SAbsTreeNode& SDCMArch::getRoot() {
  return PrivateData->RootCache;
}

void SDCMArch::clear() {
  PrivateData->SDCMArcResource.reset();
  PrivateData->initRootCache(this);
}

bool SDCMArch::hasArchive() {
  return true;
}

std::string SDCMArch::getInfo(const std::string& reqpath) {
  SURI path(reqpath);
  std::stringstream res;
  if (path.depth() > 1)
    path.deleteComponent(0);
  else
    path.clear();
  if (getRootNodeChild().pathExists(path.getURI(true))) {
    SDCMArchNode *tmpnode = &(getRootNodeChild().getByPath(path.getURI(true)));
    std::vector<std::string> sspacelist = loader().sspaceList(reqpath);
    res << "<u>" << getRootNodeChild().NodeID() << "</u><br>"
        << "SDCM Archive Resource" << "<br/>"
        << "<i>" << tmpnode->NodeID() << "</i><br/>"
        << tmpnode->getChildNodesMap().size() << " Child(ren)<br/>"
        << tmpnode->getTags().size() << " Tag(s)<br/>"
        << tmpnode->getKeyValues().size() << " Key-Value Pair(s)<br/>"
        << tmpnode->getFileList().size() << " File(s)";
    if (sspacelist.size() > 0) {
      res << "<br/>Loadable objects:<br/>";
      for (unsigned i=0; i<sspacelist.size(); i++) {
        res << sspacelist[i];
        if (i != (sspacelist.size()-1))
          res << ", ";
      }
    }
  }
  return res.str();
}

void SDCMArch::refresh(bool) {
  std::string curloc = getLocation();
  clear();
  setLocation(curloc);
  loadArchive();
}

const std::string SDCMArch::getLocationPathID(const std::string& newpath) {
  return getLocation() + "::" + newpath;
}

bool SDCMArch::hasSSpace(const std::string& reqpath) {
  SURI path(reqpath);
  bool res = false;
  if (path.depth() > 1) {
    SDCMArchNode &root = getRootNodeChild();
    path.deleteComponent(0);
    if (root.pathExists(path.getURI(true))) {
      SDCMArchNode *tmpnode = &(root.getByPath(path.getURI(true)));
      if ( tmpnode->getFileList().size() > 0 )
        res = true;
      // check for WADO fulfillment
      if ( (!res) && root.hasTag(WADOURITag) && root.hasTag(DCMQRYTag) &&
           ((tmpnode->NodeName() == "SeriesInstanceUID") ||
            (tmpnode->NodeName() == "SOPInstanceUID")) ) {
        res = true; // assume positive result, since WADO fetch is specified
      }
    }
  }
  return res;
}

void SDCMArch::doStop (bool nstop) {
  PrivateData->DoStop = nstop;
  PrivateData->SDCMArcResource.doStop(nstop);
}

void SDCMArch::getSSpaceInto(SSpace& target, const std::string& reqpath) {
  if (hasSSpace(reqpath)) {
    SURI path(reqpath);
    target.reset();
    if (path.depth() > 1) {
      path.deleteComponent(0);
      try {
        SDCMArchNode &root       = getRootNodeChild();
        SDCMArchNode *sspacenode = &(root.getByPath(path.getURI(true)));
        std::vector<std::string> filepathlist = 
                                              sspacenode->getFileListFullPath();
        // check for WADO fulfillment
        if (filepathlist.size() == 0 && root.hasTag(WADOURITag) ) {
          filepathlist = SDICOMArch::wadoURIs(root,*sspacenode);
        }
        target.progress(-1); // ensure progress completes
        if (filepathlist.size() > 1) {
          std::vector<SDICOM>       filelist;
          SDICOMLoaders::loadDICOMS(filelist,filepathlist,PrivateData->DoStop);
          if (PrivateData->DoStop) {
            doStop(false);
            return;
          }
          SDICOMLoaders::combineDICOMS(target,
                                      SDICOMLoaders::genSortedInstanceList
                                                  (filelist),PrivateData->DoStop);
        }
        else {
          if (filepathlist.size() == 1) {
            SURII fetcher = filepathlist[0];
            SFile fileref = fetcher.file();
            SDICOM singleload;
            singleload.setLocation(fileref.getLocation());
            singleload.refresh(false);
            if (singleload.isValid())
              singleload.loadSSpace(target);
          }
        }
        target.setName(sspacenode->toString());
      }
      catch (std::exception &e) {
        target.setName("ERROR: " + std::string(e.what()));
      }
    }
    labelSSpace(target,reqpath);
    target.progress(100); // ensure progress completes
    target.refresh(true);
  }
}

void SDCMArch::setEmbeddedLoadersEnabled(bool nauto) {
  PrivateData->EmbeddedLoaders = nauto;
}

bool SDCMArch::embeddedLoadersEnabled() {
  return PrivateData->EmbeddedLoaders;
}

/*-------------------------------- SDICOMArch ----------------------------------
 *
 * Class to extend SDCMArch such that DICOM files can be imported.
 *
 *------------------------------------------------------------------------------
 */

SDICOMArch::SDICOMArch() {
  doStop(false);
  setTitle(sysInfo::genGUIDString());
}

SDICOMArch::~SDICOMArch() {

}

const std::string SDICOMArch::defaultPath() {
  SURI respath;
  respath.addComponentBack("0010,0020");
  respath.addComponentBack("0020,000d");
  respath.addComponentBack("0020,000e");
  return respath.getURI();
}

const std::string SDICOMArch::defaultTitle() {
  return sysInfo::genGUIDString();
}

SDCMArchNode& SDICOMArch::importGeneric(SDCMArchNode& target, DCMTag& source) {
  // try to get the path construction directive
  std::string pathdirective;
  std::list<DCMTag*> pathstack;
  if ( ! target.hasTag(PathDirectiveTag) ) {
    DEBUG_OUT("SDICOMArch: ERROR - no path directive in target tree");
    throw SDCMArchNodeException();
  }
  pathdirective = getGenericImportPath(target);
  SURI pathuri(pathdirective);
  for ( unsigned i = 0; i < pathuri.depth(); i++) {
    pathstack.push_back(&(source.getByPath(pathuri.getComponent(i))));
  }
  return target.makePath(pathstack);
}

void SDICOMArch::setGenericImportPath(SDCMArchNode &target,
                                      const std::string &path) {
  // convert to global path from native
  SURI  native(path);
  SURII global;
  global.add(native);
  if ( ! target.hasTag(PathDirectiveTag) ) {
    DCMTag *tmppath = DCMTag::genStringTag(PathDirectiveTag,global.getURI());
    target.addTag(*tmppath);
    delete tmppath;
  }
  else {
    target.getTag(PathDirectiveTag).fromString(global.getURI());
  }
}

std::string SDICOMArch::getGenericImportPath(SDCMArchNode &source) {
  std::string respath;
  if (source.hasTag(PathDirectiveTag) ) {
    // convert to global path from native
    SURI  native;
    SURII global(source.getTag(PathDirectiveTag).toString());
    native.add(global);
    respath = native.getURI();
  }
  return respath;
}

void SDICOMArch::setTitle(const std::string& newtitle) {
  if ( ! getRootNodeChild().hasTag(ArchiveTitle) ) {
    DCMTag *tmppath = DCMTag::genStringTag(ArchiveTitle,newtitle);
    tmppath->setName("ArchiveTitle");
    getRootNodeChild().addKeyTag(*tmppath);
    delete tmppath;
  }
  else {
    getRootNodeChild().getTag(ArchiveTitle).fromString(newtitle);
    getRootNodeChild().setKey(ArchiveTitle);
  }
}

std::string SDICOMArch::getTitle() {
  if ( getRootNodeChild().hasTag(ArchiveTitle) ) {
    return getRootNodeChild().getTag(ArchiveTitle).toString();
  }
  else
    return "";
}

void SDICOMArch::setFSBase(const std::string& path) {
  getRootNodeChild().setFSPath(path);
}

std::string SDICOMArch::getFSBase() {
  return getRootNodeChild().getFSPath();
}

void SDICOMArch::setDICOMNetQuery(const std::string& newval) {
  SDCMArchNode &root = getRootNodeChild();
  if (root.hasTag(DCMQRYTag))
    root.getTag(DCMQRYTag).fromString(newval);
  else {
    DCMTag DCMQryObj;
    DCMQryObj.setID(DCMQRYTag);
    DCMQryObj.setVR('L','O');
    DCMQryObj.fromString(newval);
    root.addTag(DCMQryObj);
  }
}

std::string SDICOMArch::getDICOMNetQuery() {
  SDCMArchNode &root = getRootNodeChild();
  std::string result;
  if (root.hasTag(DCMQRYTag))
    result = root.getTag(DCMQRYTag).toString();
  return result;
}

void SDICOMArch::setWADOURI(const std::string& newval) {
  SDCMArchNode &root = getRootNodeChild();
  if (root.hasTag(WADOURITag))
    root.getTag(WADOURITag).fromString(newval);
  else {
    DCMTag DCMQryObj;
    DCMQryObj.setID(WADOURITag);
    DCMQryObj.setVR('L','O');
    DCMQryObj.fromString(newval);
    root.addTag(DCMQryObj);
  }
}

std::string SDICOMArch::getWADOURI() {
  SDCMArchNode &root = getRootNodeChild();
  std::string result;
  if (root.hasTag(WADOURITag))
    result = root.getTag(WADOURITag).toString();
  return result;
}

void SDICOMArch::setImportDirective(const std::string& path) {
  setGenericImportPath(getRootNodeChild(),path);
}

std::string SDICOMArch::getImportDirective() {
  return getGenericImportPath(getRootNodeChild());
}

void SDICOMArch::importFile(const std::string& path, ImportModes importmode,
                            bool addexemplar, bool forceexemplar, bool stripexm,
                            long int maxreadsize) {
  SDICOM impfile;
  SURI pathtofile;
  SDCMArchNode &tmpnode = getRootNodeChild();
  impfile.setLocation(path);
  if (!impfile.isValid())
    return;
  impfile.setMaxTagReadSize(maxreadsize);
  impfile.loadAllTags(false);
  SDCMArchNode &leafnode = importGeneric(tmpnode,impfile.getRootTag());
  pathtofile.setSeparator(SFile::getSystemSeparator());
  pathtofile.setURI(path);
  switch (importmode) {
    case CopyToBase:
    case MoveToBase:
      //need to be implemented
      break;
    default:
      leafnode.addFile(pathtofile.getURI());
      break;
  }
  if ( (addexemplar && (!(leafnode.hasTag(ExemplarTag)))) || forceexemplar ) {
    if (forceexemplar && leafnode.hasTag(ExemplarTag))
      leafnode.getTag(ExemplarTag).removeNode();
    leafnode.addTag(genExemplar(impfile,stripexm));
  }
}

void SDICOMArch::changeLocation(const std::string& newloc) {
  SDCMArch::changeLocation(newloc);
  SURII tmpuri(newloc);
  if (!tmpuri.isLocal()) {
    if (newloc[newloc.length()-1] != '/')
      tmpuri.deleteComponent(tmpuri.depth()-1);
    setFSBase(tmpuri.getURI());
  }
}

bool stripTags(DCMTag& target, const char VR[2], unsigned maxlen) {
  bool didrem = false;
  if ( (target.getVR()[0] == VR[0]       &&
        target.getVR()[1] == VR[1])      ||
      ( target.getDataLength() > maxlen) ) {
    target.removeNode();
    didrem = true;
  }
  else
    if (target.getTags().size() > 0)
      for (long int c=0; (unsigned)c< target.getTags().size(); c++) {
        if (stripTags(*target.getTags()[c],VR, maxlen))
          c--;
      }
  return didrem;
}

DCMTag* SDICOMArch::genExemplar(SDICOM& sourcedicom, bool minimal) {
  std::stringstream targetstream;
  DCMTag *exemplartag = new DCMTag();
  char *datastore;
  // configure the exemplar DCMTag
  exemplartag->setID(ExemplarTag);
  exemplartag->setVR('O','B');
  // strip unnecesary tags
  if (minimal) {
    for (long int i = 0;
         (unsigned)i < sourcedicom.getRootTag().getTags().size();
         i++)
      if(stripTags(*sourcedicom.getRootTag().getTags()[i],"SQ",64))
        i--;

  }
  // write the tags to the exemplar (with meta-headers -- it can be different)
  sourcedicom.setTransferSyntax(SDCMArch::SDCMArchPIMPL::SARCHTRANSX);
  sourcedicom.writeDICOMTags(targetstream,
                             sourcedicom.getRootTag(),
                             true);
  //last character written will also be size of array
  unsigned streamsize = targetstream.tellp();
  streamsize++; //account for length being p+1
  datastore = new char[streamsize]();
  // copy data into pure char buffer
  targetstream.read(datastore,streamsize);
  exemplartag->setData(streamsize,datastore);
  return exemplartag;
}

DCMTag* SDICOMArch::decExemplar(DCMTag& sourceexemplar) {
  DCMTag *targettag  = new DCMTag();
  DCMTag *targetitem = DCMTag::genItemTag();
  SDICOM  dcmwriter;
  std::stringstream exemplarstream
         (std::string(sourceexemplar.getData(),sourceexemplar.getDataLength()));
  // configure the target DCMTag
  targettag->setID(ExemplarTagSet);
  targettag->setVR('S','Q');
  targettag->addTag(targetitem);
  // read the DICOM tree into the exemplar item
  dcmwriter.readDICOMTags(exemplarstream,targetitem,-1,true);
  return targettag;
}

std::vector< std::string > SDICOMArch::wadoURIs(SDCMArchNode& root,
                                                SDCMArchNode& sspacenode) {
  std::vector< std::string > filepathlist;
  // check for WADO fulfillment
  if (root.hasTag(WADOURITag) && root.hasTag(DCMQRYTag) && 
      ((sspacenode.NodeName() == "SeriesInstanceUID") ||
        (sspacenode.NodeName() == "SOPInstanceUID")) ) {
    DCMTag QueryAttrs, QueryRes, InstAttr;
    bool foundattrs = true;
    DCMTag *lookupstudy,*lookupseries;
    std::string wadobase = root.getTag(WADOURITag).toString();
    // add SOPInstanceUID
    InstAttr.setID(SOPInstUID_);
    InstAttr.setVR('U','I');
    QueryAttrs.addTag(InstAttr);
    // get StudyInstanceUID
    lookupstudy = sspacenode.findTag(StudyUID_);
    if (lookupstudy  != nullptr)
      QueryAttrs.addTag( *lookupstudy );
    else
      foundattrs = false;
    // get SeriesInstanceUID
    lookupseries = sspacenode.findTag(SeriesUID_);
    if (lookupseries  != nullptr)
      QueryAttrs.addTag( *lookupseries );
    else
      foundattrs = false;
    if (foundattrs && (sspacenode.NodeName() == "SeriesInstanceUID")) {
      // query for instances in the series
      SURI dcmqrydir;
      dcmqrydir.setSeparator(":");
      dcmqrydir.setURI(root.getTag(DCMQRYTag).toString());
      if (dcmqrydir.depth() >= 4) {
        short int port;
        std::stringstream conv;
        conv << dcmqrydir.getComponent(1);
        conv >> port;
        if ( SDICOM_Client::findOneShot(QueryAttrs,QueryRes,
                                              SDICOM_Server::DCM_INSTANCE,
                                                dcmqrydir.getComponent(0),
                                                                      port,
                                                dcmqrydir.getComponent(2),
                                                  dcmqrydir.getComponent(3)
                                        ) 
            ) {
          // build complete URIs & add to the (empty) path list
          for (unsigned u=0; u < QueryRes.getTags().size(); u++) {
            if (QueryRes.getTags()[u]->hasTag(SOPInstUID_)) {
              filepathlist.push_back( 
                    SDICOM_Client::wadoURI(wadobase,
                                          lookupstudy->toString(),
                                          lookupseries->toString(),
                                          QueryRes.getTags()[u]->
                                            getTag(SOPInstUID_).toString()
                                          )
                                    );
            }
          }
        }
      }
    } // end series addition
    else if (foundattrs&&(sspacenode.NodeName() == "SOPInstanceUID")){
      // just try taking the SOP instances from the archive cache
      if (sspacenode.hasTag(SOPInstUID_)) {
        filepathlist.push_back(
                    SDICOM_Client::wadoURI(wadobase,
                                          lookupstudy->toString(),
                                          lookupseries->toString(),
                                          sspacenode.
                                            getTag(SOPInstUID_).toString()
                                          )
                              );
      }
    }
  } // end of WADO fulfillment
  return filepathlist;
}

unsigned int SDICOMArch::exportFiles(SDCMArchNode& root,
                                     SDCMArchNode& sspacenode,
                                     const std::string& target,
                                     SConnectable* progress) {
  std::vector< std::string > filepathlist;
  unsigned count = 0;
  unsigned progressc = 0;
  bool wado = false;
  SFile target_(target);
  // set progress to prep.
  if (progress!=nullptr) progress->progress(-1);
  // fetch regular paths first
  filepathlist = sspacenode.getFileListFullPath();
  // check for WADO fulfillment
  if (root.hasTag(WADOURITag) && (filepathlist.size() == 0)) {
    filepathlist = wadoURIs(root,sspacenode);
    wado = true;
  }
  // store each path to the target
  if (target_.isDIR()) {
    unsigned listsize = filepathlist.size();
    for (unsigned p=0; p<listsize; p++) {
      SURI srcpath(filepathlist[p]);
      if (srcpath.depth()>0) {
        SURI targpath(target);
        if (wado) {
          std::stringstream resname;
          resname << sspacenode.NodeValue() << "-" << count << ".dcm";
          targpath.addComponentBack(resname.str());
        }
        else {
          std::string name = srcpath.getComponent(srcpath.depth()-1);
#ifdef _WIN32
          for (unsigned i=0; i<name.size(); i++)
              if (name[i] == '\\' || name[i] == '/')
                  name[i] = '-';
#endif
          targpath.addComponentBack(name);
        }
        try {
          SLogger::global().addMessage("URI: " + targpath.getURI());
          srcpath.toFile(targpath.getURI());
        }
        catch (std::exception &e) {
          SLogger::global().addMessage("SDICOMArch::exportFiles:" + 
                                       std::string(e.what()) +
                                       ":" + srcpath.getURI());
        }
        count++;
        unsigned tprogress = (unsigned)(((float)count / (float)listsize)*100.0);
        if (tprogress != progressc) {
          progressc = tprogress;
          if (progress!=nullptr) progress->progress(progressc);
        }
      }
    }
  }
  else {
    SLogger::global().addMessage
                 ("SDICOMArch::exportFiles:: Target not a directory:" + target);
  }
  if (progress!=nullptr) progress->progress(100);
  return count;
}
