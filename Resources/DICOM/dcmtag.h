/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for DICOM Tags
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM_TAG
#define SIMULACRUM_DICOM_TAG

#include "types.h"
#include <Core/sabtree.h>
#include <Core/sprimitives.h>
#include <vector>
#include <map>
#include <sstream>
#include <iomanip>

namespace Simulacrum {
struct dcm_info_store;
/*-------- A class to store DICOM tags, including conversion methods ---------*/
class SIMU_API DCMTag : public SAbsTreeNode {
friend class SDICOM;
private:
  class DCMTagPIMPL;
  DCMTagPIMPL *PrivateData;
protected:
  char*                      data();
  tagmap_t&                  getTagsMap();
public:
                             DCMTag ();
                             DCMTag (DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH,
                                     char, char, const char*, char*,
                                     DICOM_LONG_LENGTH);
                             DCMTag (DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
                             DCMTag (const DCMTag&);
                            ~DCMTag ();
  DCMTag&                    operator= (const DCMTag&);
  DCMTag&                    operator= (const dcm_info_store&);
  bool                       operator==(const DCMTag &) const;
  bool                       operator< (const DCMTag &) const;
  DICOM_ID_PART_LENGTH       getID1    ()  const;
  DICOM_ID_PART_LENGTH       getID2    ()  const;
  DICOM_ID_LENGTH            getID     ()  const;
  const std::string          getIDstr  ()  const;
  const char*                getVR     ()  const;
  const char*                getName   ()  const;
  const char*                getData   ()  const;
  DICOM_LONG_LENGTH          getDataLength() const;
  DICOM_LONG_LENGTH          getDataLengthUnpadded() const;
  DCMTag&                    getTag(DICOM_ID_PART_LENGTH,
                                    DICOM_ID_PART_LENGTH);
  DCMTag&                    getTag(DICOM_ID_LENGTH);
  DCMTag&                    getTagDFS(DICOM_ID_LENGTH);
  DCMTag&                    getTagDFS(DICOM_ID_PART_LENGTH,
                                       DICOM_ID_PART_LENGTH);
  tagset_t&                  getTags();
  bool                       hasTag(DICOM_ID_PART_LENGTH,
                                    DICOM_ID_PART_LENGTH);
  bool                       hasTag(DICOM_ID_LENGTH);
  bool                       hasTagDFS(DICOM_ID_LENGTH);
  bool                       hasTagDFS(DICOM_ID_PART_LENGTH,
                                       DICOM_ID_PART_LENGTH);
  unsigned                   hasTags(bool ignoredelimeters = false) const;
  bool                       hasError() const;
  DCMTag&                    getParent();
  std::string                getPath();
  bool                       pathExists(const std::string&);
  DCMTag&                    getByPath(const std::string&);
  bool                       dataPresent() const;
  bool                       isMeta() const;
  bool                       isNULL(bool = false) const;
  bool                       isTop() const;
  bool                       isInt() const;
  bool                       isFloat() const;
  bool                       isString() const;
  bool                       isArray() const;
  bool                       isPrivate() const;
  bool                       shouldSave() const;
  std::string                getElement(unsigned short, bool toend=false) const;
  SVector                    toVector() const;
  void                       fromVector(const SVector&);
  int64_t                    toInt() const;
  int64_t                    toInt(unsigned) const;
  double                     toFloat() const;
  void                       toString(std::string&) const;
  std::string                toString() const;
  void                       toStringSafe(std::string&) const;
  void                       fromInt(int64_t);
  void                       fromFloat(double);
  void                       fromString(const std::string&);
  void                       clear();
  void                       clearData();
  void                       clearTags();
  void                       setID  (DICOM_ID_LENGTH);
  void                       setID  (DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
  void                       setID  (const std::string&);
  void                       setVR  (char, char);
  void                       setName(const char*, bool ownsname=false);
  void                       setDataLength(DICOM_LONG_LENGTH);
  void                       setData(DICOM_LONG_LENGTH, char*);
  void                       setMeta(bool);
  void                       setParent(DCMTag*);
  void                       setError(bool);
  void                       setIsParent(bool isTheParent);
  void                       setShouldSave(bool);
  bool                       matches(const std::string&, bool recurse = false);
  DCMTag&                    addTag(DCMTag&, bool check = true);
  void                       addTag(DCMTag*, bool check = true);
  DCMTag&                    addItem(DCMTag&);
  DCMTag*                    addItem(DCMTag*);
  bool                       removeNode();
  void                       detachNode();
  bool                       removeTag(const DCMTag*, bool dodelete=true);
  bool                       removeTag(DICOM_ID_LENGTH, bool doelete=true);
  bool                       removeTag(DICOM_ID_PART_LENGTH,
                                       DICOM_ID_PART_LENGTH, bool doelete=true);
  /* Methods to generate several default kinds of tags */
  static DCMTag*             genItemTag    ();
  static DCMTag*             genSequenceTag(DICOM_ID_LENGTH);
  static DCMTag*             genIntTag     (DICOM_ID_LENGTH,int64_t = 0);
  static DCMTag*             genFloatTag   (DICOM_ID_LENGTH,double  = 0.0);
  static DCMTag*             genStringTag  (DICOM_ID_LENGTH,const std::string& = "");

  /* For AbsTreenode compliance */
  std::string                NodeID();
  void                       NodeID(const std::string&);
  std::string                NodeType();
  void                       NodeType(const std::string&);
  unsigned long              NodeSize();
  std::string                NodeName();
  std::string                NodeValue();
  void                       NodeValue(const std::string&);
  std::string                NodeData();
  void                       NodeData(const std::string&);
  unsigned long              NodeChildrenNum(bool strict = false);
  SAbsTreeNodeList_t         NodeChildren(bool strict = false, bool = false);
  bool                       NodePathExists(const std::string&);
  SAbsTreeNode&              NodeByPath(const std::string&);
  SAbsTreeNode&              NodeParent();
  bool                       hasNodeParent();
  bool                       NodeError();
  SAbsTreeNode&              NewChild();
  SimulacrumLibrary::str_enc stringEncoding() override;
};

}

#endif
