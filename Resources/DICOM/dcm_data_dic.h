/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_BUILTIN_DIC
#define SIMULACRUM_BUILTIN_DIC
/* HEADER STUB: Necessary for inclusion of prebuilt
 * DCMDIC in Windows Visiual Studio compilation
 */

#include "types.h"
#include "datadic.h"

namespace Simulacrum {

#ifndef NODCMDIC
  extern void init_DCM_DATA_DIC(dcm_data_dic_t&);
#endif

}

#endif // SIMULACRUM_BUILTIN_DIC
