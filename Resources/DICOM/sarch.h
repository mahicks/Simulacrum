/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:SArch
 * Interface specification for DICOM Archive
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM_ARCH
#define SIMULACRUM_DICOM_ARCH

#include "dcmtag.h"
#include "types.h"
#include "sdicom.h"
#include <Core/sresource.h>
#include <Core/sabtree.h>
#include <Toolbox/SURI/suri.h>

#include <string>
#include <list>

namespace Simulacrum {

/*-------------------------------- SDCMArchNode --------------------------------
 * A special kind of DICOM node, with methods to interact with a prescribed
 * Tag structure and members.
 *
 * <SDCMArchNodeKey> SQ (DCMTag representing a SDCMArchNode)
 *   - Key           UL (key to use as ID)
 *   - Type          UL (type of this node)
 *   - FSPath        LO (URI path to this Node)
 *   - KVP           SQ (key value pairs)
 *   - General Tags  SQ (general DICOM tags for this node)
 *   - Files         SQ (associated files)
 *   - Children      SQ (Proper Children of this node)
 *------------------------------------------------------------------------------
 */

  class SIMU_API SDCMArchNode: public SAbsTreeNode {
  private:
    class SDCMArchNodePIMPL;
    SDCMArchNodePIMPL *PrivateData;
  public:
                    SDCMArchNode(DCMTag&,SDCMArchNode* parent = nullptr);
                   ~SDCMArchNode();
    bool            isValid();
    bool            isTop() const;
    SDCMArchNode&
                    getParentNode();
    bool            operator==(const SDCMArchNode&) const;
    DICOM_ID_LENGTH getKeyID();
    DCMTag&         getKeyTag();
    const char*     getKeyName();
    std::string     getKeyStr();
    bool            hasKey();
    void            setKey(DICOM_ID_LENGTH);
    SDCMArchNode_t  getType();
    void            setType(SDCMArchNode_t);
    std::string     getFSPath();
    std::string     getFullFSPath();
    void            setFSPath(const std::string&);
    std::string     getPath();
    bool            pathExists(const std::string&);
    SDCMArchNode&   getByPath(const std::string&);
    SDCMArchNode&   makePath(std::list<DCMTag*> &pathkeys);
    DCMTag&         getTag(DICOM_ID_LENGTH, bool safe = true);
    DCMTag*         findTag(DICOM_ID_LENGTH,bool exemplars = true,
                            bool usefiles = false);
    tagset_t&       getTags();
    bool            hasKVP(const std::string&);
    std::string     getKeyValue(const std::string&);
    SDCMArchKeyMap_t&
                    getKeyValues();
    void            setKeyValue(const std::string&,const std::string&);
    void            removeKeyValue(const std::string&);
    bool            hasTag(DICOM_ID_LENGTH);
    std::string     toString();
    bool            hasChildNode(const std::string& keyvalue);
    SDCMArchNode&
                    getChildNode(const std::string& keyvalue);
    SDCMArchNode*   getChildNodeP(const std::string& keyvalue);
    SDCMArchNodeMap_t&
                    getChildNodesMap();
    SDCMArchNode&   genChildNode(DCMTag& keytag, bool check = true);
    void            addChildNode(SDCMArchNode&, bool check = true);
    void            removeChildNode(SDCMArchNode&);
    void            addTag(DCMTag&);
    void            addTag(DCMTag*);
    void            addKeyTag(DCMTag&);
    void            addKeyTag(DCMTag*);
    void            removeTag(DICOM_ID_LENGTH);
    void            removeTag(DCMTag&);
    void            addFile(const std::string&, bool strippath = true);
    void            removeFile(const std::string&);
    void            clearFiles();
    void            clearKVP();
    void            clearTags();
    void            clearChildren();
    void            clear();
    std::vector<std::string>
                    getFileList();
    std::vector<std::string>
                    getFileListFullPath();
    /* Implementation of SAbsTreeNode methods */
    std::string     NodeID();
    std::string     NodeType();
    unsigned long   NodeSize();
    std::string     NodeName();
    std::string     NodeValue();
    void            NodeValue(const std::string&);
    bool            NodeEmph();
    unsigned long   NodeChildrenNum(bool strict = false);
    bool            NodePathExists(const std::string&);
    SAbsTreeNode&   NodeByPath(const std::string&);
    SAbsTreeNode&   NodeParent();
    bool            hasNodeParent();
    void            detachNode();
    bool            removeNode();
    SAbsTreeNodeList_t
                    NodeChildren(bool strict = false, bool simpleonly = false);
    bool            NodeError();
    SAbsTreeNode&   NewChild();
    std::map<std::string,std::string>
                    getAttributes();
    bool            hasAttribute(const std::string&);
    std::string     getAttribute(const std::string&);
    SimulacrumLibrary::str_enc
                    stringEncoding() override;
  };

/*-------------------------------- SDCMArch ------------------------------------
 *
 * Class to manage the loading and storing of an SDCMArchNode tree.
 *
 *------------------------------------------------------------------------------
 */

  class SIMU_API SDCMArch : public SResource {
  protected:
    class SDCMArchPIMPL;
    SDCMArchPIMPL *PrivateData;
  public:
                       SDCMArch();
    virtual           ~SDCMArch();
    int                setLocation(const std::string&);
    void               changeLocation(const std::string&);
    bool               isValid() const;
    const std::string& getLocation() const;
    void               loadArchive();
    void               storeArchive();
    void               store();
    SDCMArchNode&      getRootNodeChild();
    SDCMArchNode&      getRootNode();
    SAbsTreeNode&      getRoot();
    void               clear();
    void               refresh(bool);
    std::string        getInfo(const std::string &path = "");
    bool               hasArchive();
    bool               hasSSpace(const std::string &path = "");
    void               doStop(bool);
    void               getSSpaceInto(SSpace&, const std::string &path = "");
    const std::string  getLocationPathID(const std::string &path = "");
    bool               embeddedLoadersEnabled();
    void               setEmbeddedLoadersEnabled(bool);
  };

/*-------------------------------- SDICOMArch ----------------------------------
 *
 * Class to extend SDCMArch such that DICOM files can be imported.
 *
 *------------------------------------------------------------------------------
 */

  class SIMU_API SDICOMArch : public SDCMArch {
  public:
                       SDICOMArch();
    virtual           ~SDICOMArch();
    static const
    std::string        defaultPath();
    static const
    std::string        defaultTitle();
    enum               ImportModes {LeaveAlone,CopyToBase,MoveToBase};

    static SDCMArchNode&
                       importGeneric(SDCMArchNode &target, DCMTag &source);
    static void        setGenericImportPath(SDCMArchNode &target,
                                            const std::string &path);
    static
    std::string        getGenericImportPath(SDCMArchNode &target);

    void               setTitle(const std::string &path);
    std::string        getTitle();
    void               setFSBase(const std::string&path);
    std::string        getFSBase();
    void               setDICOMNetQuery(const std::string&);
    std::string        getDICOMNetQuery();
    void               setWADOURI(const std::string&);
    std::string        getWADOURI();
    void               setImportDirective(const std::string &path);
    std::string        getImportDirective();
    void               importFile(const std::string& path,
                                  ImportModes importmode=LeaveAlone,
                                  bool addexemplar = true,
                                  bool forceexmplar = false,
                                  bool stripexemplar = true,
                                  long int maxreadsize = -1);
    void               changeLocation(const std::string&);
    static DCMTag*     genExemplar(SDICOM&, bool minimal = true);
    static DCMTag*     decExemplar(DCMTag&);
    static std::vector<std::string>
                       wadoURIs(SDCMArchNode& root, SDCMArchNode& sspacenode);
    static unsigned    exportFiles(SDCMArchNode& root,
                                   SDCMArchNode& sspacenode,
                                   const std::string& target,
                                   SConnectable* progress = nullptr);
  };

}

#endif
