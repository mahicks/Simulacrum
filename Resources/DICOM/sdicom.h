/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for DICOM Files
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM
#define SIMULACRUM_DICOM

#include <Core/sfileio.h>
#include <Core/error.h>
#include <Core/NNode/nnode.h>
#include <Core/sresource.h>
#include "datadic.h"
#include "dcmtag.h"
#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>
#include <vector>

namespace Simulacrum {

/* -------------------- Simulacrum DICOM  file storage  ----------------------*/
class SIMU_API SDICOM : public SIO {
private:
  class SDICOMPIMPL;
  SDICOMPIMPL *PrivateData;
public:
                     SDICOM();
                     SDICOM(const SDICOM&);
  static
  const std::string& implementationName();
  static
  const std::string& implementationUID();
  static
  const std::string& implementationVersion();
  SDICOM&            operator=(const SDICOM&);
  SDICOM&            operator=(SDICOM&);
                    ~SDICOM();
  void               reset();
  void               clear();
  int                setLocation(const std::string&);
  void               changeLocation(const std::string&);
  const std::string& getLocation() const;
  const SCoordinate& getExtent();
  DCMTag&            getRootTag();
  SAbsTreeNode&      getRoot();
  void               refresh(bool pixeldata = false);
  std::string        getInfo(const std::string &path = "");
  const tagset_t&    getTags();
  const DCMTag&      getTag(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
  bool               hasTag(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
  const std::string& getTransferSyntax();
  const std::string& getTransferSyntaxName();
  SimulacrumLibrary::str_enc
                     stringEncoding() override;
  void               setTransferSyntax(const std::string&);
  void               setAETitle(const std::string&);
  void               setMaxTagReadSize(long int);
  void               setDataDictionary(DCMDataDic*);
  void               addTag(DCMTag*, bool = true);
  bool               removeTag(const DCMTag*);
  bool               removeTag(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
  bool               isValid() const;
  bool               hasArchive();
  bool               hasSSpace(const std::string &path = "");
  bool               PixelDataPresent();
  void               loadMissingData();
  void               getSSpaceInto(SSpace&,const std::string &path = "");
  static void        printTag(const DCMTag&, bool=true, bool=true,
                              bool=true, unsigned depth=0,
                              bool structure = true);
  int                loadAllTags(bool readpixeldata=false);
  int                writeAllTags(bool standardize = true);
  void               store();
  int                loadSSpace(SSpace&);
  int                storeSSpace(SSpace&);
  void               setNativePixelData(SSpace&);
  void               makeSecondaryCapture(SSpace&);
  bool               compressJPEGLS();
  void               setPreambleString(const std::string&);
  const std::string& getPreambleString() const;
  static void        standardizeTag(DCMTag&, bool recursive);
  static
  bool               postLoadSSpaceConfiguration(SSpace&,SDICOM&,
                                                 bool forceZAxisDiff = false,
                                                 SVector *zaxisdiff = nullptr);
  static bool        globalDeident();
  static void        setGlobalDeident(bool);
  static NNode&      globalDeidentMap();
  /* Raw methods: not for the faint of heart */
  int                readDICOMTags
                       (std::istream& dcmsource, DCMTag *target,
                        unsigned numtoread=(0-1),
                        bool readpixeldata = false,
                        int MetaInfoEnd = -1);
  int                writeOneTag(std::ostream& dcmtarget, DCMTag &sourcetags,
                                 int& MetaInfoEnd);
  int                writeDICOMTags
                       (std::ostream& dcmtarget, DCMTag &roottag,
                        bool dometa = true, bool standardize = true);
  // some classes wish to do clever things, for example, with DICOM streams
  friend class       SDICOMArch;
  friend class       SDICOM_Server;
  friend class       SDICOM_Client;
  friend class       UserInfoItem;
};

/* ----------------- Simulacrum DICOM geometry methods  ----------------------*/

class SDICOMGeom {
public:
  static SVector imageToPatient(SVector &x, SVector &y, SVector &z,
                                SVector &offset, SVector &spacing,
                                SSpace*,const SVector& coord);
  static SVector patientToImage(SVector &x, SVector &y, SVector &z,
                                SVector &offset, SVector &spacing,
                                SSpace*,const SVector& coord);
};

/* ------------------ Simulacrum DICOM loading mathods  ----------------------*/

class SDICOMLoaders {
public:
  static void loadDICOMS   (std::vector<SDICOM>& target,
                            const std::vector<std::string>& pathlist,
                            bool &stopevent);
  static std::vector<SDICOM*>
              genSortedInstanceList (std::vector<SDICOM>&);
  static void combineDICOMS(SSpace& target,std::vector<SDICOM*>,bool&stopevent);
};

} //end namespace
#endif //SIMULACRUM_DICOM
