/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for DICOM DataDictionaries
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM_TYPES
#define SIMULACRUM_DICOM_TYPES

#include <Core/error.h>
#include <stdint.h>
#include <vector>
#include <map>
#include <unordered_map>

#include <functional>

namespace Simulacrum {

  /* Universal types used during DICOM reading/writing */
  typedef uint16_t        DICOM_VR_LENGTH;
  typedef uint16_t        DICOM_ID_PART_LENGTH;
  typedef uint32_t        DICOM_ID_LENGTH;
  typedef uint16_t        DICOM_LENGTH_LENGTH;
  typedef uint32_t        DICOM_LONG_LENGTH;
  typedef uint32_t        DICOM_PDU_LENGTH;
  typedef uint16_t        DICOM_SHORT_PDU_LENGTH;
  typedef uint8_t         DICOM_PDU_TYPE;
  typedef uint8_t         DIMSE_CMD_TYPE;
  class SDICOM;
  typedef std::function<void(SDICOM*)>
                          DICOM_SERVER_CALLBACK;

  /* Some important tags, which need to be statically defined */
  const DICOM_ID_LENGTH   TransferSyntax = 0x00020010;
  const DICOM_ID_LENGTH   SOPClass       = 0x00020002;
  const DICOM_ID_LENGTH   SOPClassUID    = 0x00080016;
  const DICOM_ID_LENGTH   PixelData      = 0x7fe00010;
  const DICOM_ID_LENGTH   MetaInfoVer    = 0x00020001;
  const DICOM_ID_LENGTH   MetaInfoLen    = 0x00020000;
  const DICOM_ID_LENGTH   ItemMarker     = 0xfffee000;
  const DICOM_ID_LENGTH   ItemDelim      = 0xfffee00d;
  const DICOM_ID_LENGTH   SequenceDelim  = 0xfffee0dd;
  const DICOM_ID_LENGTH   SequenceUnspec = 0xFFFFFFFF;
  const DICOM_ID_LENGTH   PatientID_     = 0x00100020;
  const DICOM_ID_LENGTH   PatientName_   = 0x00100010;
  const DICOM_ID_LENGTH   PatientSex_    = 0x00100040;
  const DICOM_ID_LENGTH   PatientDOB_    = 0x00100030;
  const DICOM_ID_LENGTH   StudyUID_      = 0x0020000D;
  const DICOM_ID_LENGTH   SeriesUID_     = 0x0020000E;
  const DICOM_ID_LENGTH   InstanceNum_   = 0x00200013;
  const DICOM_ID_LENGTH   SOPInstUID_    = 0x00080018;
  const char              DCMArraySep    = '\\';

  /* Tag Storage Types */
  class DCMTag;
  typedef std::vector<DCMTag*>                      tagset_t;
  typedef std::multimap<DICOM_LONG_LENGTH,DCMTag*>  tagmap_t;

  class SDCMArchNode;
  typedef uint16_t SDCMArchNode_t;
  typedef std::unordered_map<std::string,SDCMArchNode*>  SDCMArchNodeMap_t;
  typedef std::unordered_map<std::string,DCMTag*>        SDCMArchKeyMap_t;

  /* archive tag definitions */
  enum            SDCMArchNode_Types {Level0, Level1, Level2, Level3, Level4};

  static const
  DICOM_ID_LENGTH PrivateTagBlock  = 0xFEFF00EE;
  static const
  DICOM_ID_LENGTH NodeTag          = 0xFEFFEE01;
  static const
  DICOM_ID_LENGTH RootNodeTag      = 0xFEFFEE02;
  static const
  DICOM_ID_LENGTH KeyTag           = 0xFEFFEE03;
  static const
  DICOM_ID_LENGTH TypeTag          = 0xFEFFEE04;
  static const
  DICOM_ID_LENGTH FSTag            = 0xFEFFEE05;
  static const
  DICOM_ID_LENGTH KVPTag           = 0xFEFFEE06;
  static const
  DICOM_ID_LENGTH GeneralTag       = 0xFEFFEE07;
  static const
  DICOM_ID_LENGTH FilesTag         = 0xFEFFEE08;
  static const
  DICOM_ID_LENGTH ChildrenTag      = 0xFEFFEE09;
  static const
  DICOM_ID_LENGTH KVPElemTag       = 0xFEFFEE0A;
  static const
  DICOM_ID_LENGTH FileObjTag       = 0xFEFFEE0B;
  static const
  DICOM_ID_LENGTH PathDirectiveTag = 0xFEFFEE0C;
  static const
  DICOM_ID_LENGTH ArchiveTitle     = 0xFEFFEE0D;
  static const
  DICOM_ID_LENGTH DefaultString    = 0xFEFFEE0E;
  static const
  DICOM_ID_LENGTH ExemplarTag      = 0xFEFFEE0F;
  static const
  DICOM_ID_LENGTH ExemplarTagSet   = 0xFEFFEE10;
  static const
  DICOM_ID_LENGTH QueryResultSeq   = 0xFEFFEE11;
  static const
  DICOM_ID_LENGTH EmbeddedLdrTag   = 0xFEFFEE12;
  static const
  DICOM_ID_LENGTH DCMQRYTag        = 0xFEFFEE13;
  static const
  DICOM_ID_LENGTH WADOURITag       = 0xFEFFEE14;

  /* SDICOM_Net definitions */
  static const DICOM_PDU_TYPE ASSOCR_PDU = 0x01;
  static const DICOM_PDU_TYPE ASSOCA_PDU = 0x02;
  static const DICOM_PDU_TYPE ASSOCJ_PDU = 0x03;
  static const DICOM_PDU_TYPE ASSOCS_PDU = 0x05;
  static const DICOM_PDU_TYPE ASSOCP_PDU = 0x06;
  static const DICOM_PDU_TYPE ASSOCB_PDU = 0x07;
  static const DICOM_PDU_TYPE APPCTX_PDU = 0x10;
  static const DICOM_PDU_TYPE PRSCTX_PDU = 0x20;
  static const DICOM_PDU_TYPE PRSITM_PDU = 0x21;
  static const DICOM_PDU_TYPE ABSCTX_PDU = 0x30;
  static const DICOM_PDU_TYPE TRXCTX_PDU = 0x40;
  static const DICOM_PDU_TYPE USRNFO_PDU = 0x50;
  static const DICOM_PDU_TYPE DCMDAT_PDU = 0x04;
  static const DICOM_PDU_TYPE MAXLEN_PDU = 0x51;
  static const DICOM_PDU_TYPE IMPUID_PDU = 0x52;
  static const DICOM_PDU_TYPE IMPNAM_PDU = 0x55;
  static const short          AETLENGTH  = 16;

  /* common exception types */

  class SimulacrumDCMException: public SimulacrumIOException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: General DICOM Exception";
    }
  };

  class SimulacrumDCMParsException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: DICOM Parsing Exception";
    }
  };

  class SimulacrumDCMTagsException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: DICOM Tags exception";
    }
  };

  class SimulacrumDCMSyntaxException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: Unsupported DICOM TransferSyntax";
    }
  };

  class SimulacrumDCMPresenceException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: Sufficient DICOM Tags Not Present";
    }
  };

  class SimulacrumDCMImageException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: Invalid DICOM Image Data";
    }
  };

  class SimulacrumDCMDictionaryException: public SimulacrumDCMException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: DICOM Dictionary error";
    }
  };

  class SDCMArchNodeException : public SimulacrumDCMTagsException {
  public:
    virtual const char* what() const throw(){
      return "Simulacrum: General SDCMArch Exception";
    }
  };

}

#endif
