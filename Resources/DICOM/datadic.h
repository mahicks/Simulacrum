/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Interface specification for DICOM DataDictionaries
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM_DATADIC
#define SIMULACRUM_DICOM_DATADIC

#include "types.h"
#include <Core/definitions.h>
#include <Core/NNode/nnode.h>
#include <vector>
#include <map>

namespace Simulacrum {
/*----------------------- Data Dictionary Storage --------------------------- */

  class SIMU_API DCMSens {
  public:
    typedef char DCMSensType;
    static const DCMSensType UNKNOWN = '0';
    static const DCMSensType PSEUDON = 'P';
    static const DCMSensType REMOVE  = 'R';
    static const DCMSensType BLANK   = 'B';
    static const DCMSensType LEAVE   = 'L';
  };

  struct dcm_info_store {
    DICOM_ID_LENGTH ID;
    char            VR[2];
    char*           Name;
    DCMSens::DCMSensType
                    Sensitivity;
  };

  typedef std::map<DICOM_ID_LENGTH,dcm_info_store> dcm_data_dic_t;
  class DCMTag;

  /* Encapsulate data dictionary access, loading and saving */
  class SIMU_API DCMDataDic {
  private:
    class DCMDataDicPIMPL;
    DCMDataDicPIMPL *PrivateData;
  public:
                          DCMDataDic();
                         ~DCMDataDic();
    void                  clear(bool builtin=false);
    bool                  contains(DICOM_ID_LENGTH);
    bool                  contains(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
    bool                  contains(const std::string&, bool casesen = true);
    const dcm_info_store& getEntry(DICOM_ID_LENGTH);
    const dcm_info_store& getEntry(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH);
    const dcm_info_store& getEntry(const std::string&, bool casesen = true);
    void                  addEntry(dcm_info_store&, bool toglobal = false);
    void                  addEntry(DICOM_ID_LENGTH,const std::string&,
                                  const std::string&, char sens = 0);
    void                  addEntry(DICOM_ID_PART_LENGTH,DICOM_ID_PART_LENGTH,
                                  const std::string&, const std::string&,
                                  char sens = 0);
    void                  loadFromFile(const std::string&, bool global = false);
    void                  storeToFile(const std::string&, bool builtin = true);
    bool                  deidentifyTagTree(DCMTag& target,
                                            NNode&  psedonmap,
                                            bool    removeprivate  = true ,
                                            bool    recurse        = false );
    static bool           isTagTreeDeidentified(DCMTag&);
    static void           setTagTreeDeidentified(DCMTag&,bool=true);
    static void           pseudonTag(DCMTag&,NNode&);
    static const char*    pseudonDBName();
    static DICOM_ID_LENGTH
                          toID(const std::string& tagname);
    static std::string    toIDStr(const std::string& tagname);
    static std::string    toName(const std::string& tagname);
    static std::string    toName(DICOM_ID_LENGTH);
  };
}

#endif
