/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * A simple DICOM file reader/writer
 * M. A. Hicks
 * Sensitive on the following #defines:
 * NODCMDIC (attempts to parse without a DICOM dictionary)
 * DEBUG
 */
#include "sdicom.h"
#include "datadic.h"
#include <string.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SURI/suri.h>
#include <Resources/sj2k.h>
#include <Resources/sljpeg.h>
#include <Resources/sjpegls.h>
#include <Core/SPool/SPool.h>
#include <Core/slicer/slicer.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>
#include <functional>

using namespace Simulacrum;

extern dcm_data_dic_t DCM_DATA_DIC;

static inline void  reverseBytes(char* dat,std::size_t datsize);
static inline void  reverseBytesBlock(char* dat,std::size_t datsize,
                                      short blocksize);
static inline bool  isEndianSensitive(DCMTag& ttag);
static inline short reverseBlockSizePixelData(DCMTag& rootnode);

static int loadJP2KtoSSpace(SSpace& target, DCMTag &JPEGItemTag);
static int loadLosslessJPEGtoSSpace(SSpace& target,DCMTag &JPEGItemTag,
                                    unsigned short bitsalloc);
static int foldOverPixelItems(SSpace &target,
                              std::function<int(SSpace&,DCMTag&)>,
                              DCMTag &PixelDataParent, bool issigned);

static bool zAxisEnh(SDICOM&,SVector &sliceposdiff);
static bool zAxisMulti(SDICOM &d1,SDICOM &d2,SVector &sliceposdiff);

/* -------------------- Simulacrum DICOM  file storage  ----------------------*/

class SDICOM::SDICOMPIMPL {
public:
  /* DICOM Meta-Information */
  static
  const std::string            SDICOMVersion;
  static
  const std::string            ImplementationName;
  static
  const std::string            ImplementationUID;
  static const uint16_t        MetaVersion;
  static const unsigned        DICOM_PREAMBLE_LENGTH;
  static const uint32_t        DICOM_PREFIX_STRING;
  static const DICOM_ID_LENGTH ROOT_TAG_ID;
  /* Aligned arrays of transfer syntax information */
  static const unsigned        TransferSyntaxNum = 13;
  static const std::string     TransferSyntaxs[TransferSyntaxNum];
  static const std::string     TransferSyntaxNames[TransferSyntaxNum];
  static bool                  GlobalDeident;
  static NNode                 GlobalDeidentMap;
  std::string                  PREAMBLESTR;
  typedef uint32_t             findword_t;
  std::string                  AETitle;
  int                          TransferSyntaxType;
  /* Data Dictionary */
  DCMDataDic                   DataDicIntern;
  DCMDataDic                  *DataDictionary;
  /* General File I/O Structure */
  std::string                  DICOMPath;
  SCoordinate                  Extent;
  bool                         Valid;
  DCMTag                       RootTag;
  long int                     MaxTagReadSize;

  int64_t            findFirstWord(std::fstream&, findword_t);
  int64_t            findFirstTag
                       (std::fstream& fsource, DICOM_ID_LENGTH findtag);
  int                readDICOMKeys(std::istream& dcmsource, DCMTag &ttag,
                                   int MetaInfoEnd = -1);
  int                readDICOMVRLength(std::istream& dcmsource, DCMTag &ttag,
                                       int MetaInfoEnd = -1);
  int                readDICOMData(std::istream& dcmsource, DCMTag &ttag,
                                   bool readpixeldata = false,
                                   int MetaInfoEnd = -1);
  int                handleSequence(std::istream& dcmsource, DCMTag&,
                                    bool readpix = false,int MetaInfoEnd = -1);
  unsigned int       postDecodeActions(std::istream& dcmsource, DCMTag&,
                                       int& MetaInfoEnd,bool readpixeldata);
  int                writeMetaHeaders
                       (std::ostream& dcmtarget, int& MetaInfoEnd);
  inline short
                     readData(std::istream& instream, char* data,
                              unsigned int osize, bool fixorder = false,
                              int MetaInfoEnd = -1, short reorderblock = 1);
  inline short       writeData(std::ostream& outstream, const char* data,
                               unsigned int osize, bool fixorder = false,
                               int MetaInfoEnd = -1, short reorderblock = 1);
  DCMTag*            genMetaHeader();
  bool               isDICOM(const char*);
  void               initFriendlyRootTag();
  void               readPreamble(std::fstream&);
  void               writePreamble(std::fstream&);
  void               dumpDICOMtagout(unsigned, DICOM_ID_PART_LENGTH,
                                      DICOM_ID_PART_LENGTH, const char*,
                                      DICOM_LONG_LENGTH, const char*);
  int                loadNativetoSSpace(SSpace&, bool allowstealing = false);
  SDICOM            *Parent;
  SDICOMPIMPL(SDICOM* parent): Parent(parent) {}
};

/* Constant SDICOM Implementation Information */
const std::string     SDICOM::SDICOMPIMPL::SDICOMVersion         = "1.0";
const std::string     SDICOM::SDICOMPIMPL::ImplementationName    = 
                                                              "SimulacrumDICOM";
const std::string     SDICOM::SDICOMPIMPL::ImplementationUID     =
                                 "2.25.329575135628586529295659785119061185918";
const uint16_t        SDICOM::SDICOMPIMPL::MetaVersion           = 0x0000;
const unsigned        SDICOM::SDICOMPIMPL::DICOM_PREAMBLE_LENGTH = 128;
const uint32_t        SDICOM::SDICOMPIMPL::DICOM_PREFIX_STRING   = 0x4d434944;
const DICOM_ID_LENGTH SDICOM::SDICOMPIMPL::ROOT_TAG_ID           = 0x00000000;
      bool            SDICOM::SDICOMPIMPL::GlobalDeident         = false;
      NNode           SDICOM::SDICOMPIMPL::GlobalDeidentMap;
/* Abbreviation of Syntax Name -- TUK *always* last */
enum TransferSyntaxs_        {LIM,LEX,BEX,JLL,JLF,RLL,J2L,JPB,JPE,J2S,JLS,JL2,
                              TUK};
enum PixelRepresentations_   {PX_MONO,PX_MONO_SIGNED,PX_UNKNOWN};
// Initialise static transfersyntax arrays (remember TransferSyntaxNum)
const std::string  SDICOM::SDICOMPIMPL::TransferSyntaxs[] = {
  "1.2.840.10008.1.2",
  "1.2.840.10008.1.2.1",
  "1.2.840.10008.1.2.2",
  "1.2.840.10008.1.2.4.57",
  "1.2.840.10008.1.2.4.70",
  "1.2.840.10008.1.2.5",
  "1.2.840.10008.1.2.4.90",
  "1.2.840.10008.1.2.4.50",
  "1.2.840.10008.1.2.4.51",
  "1.2.840.10008.1.2.4.91",
  "1.2.840.10008.1.2.4.80",
  "1.2.840.10008.1.2.4.81",
  "Unknown"
};
const std::string SDICOM::SDICOMPIMPL::TransferSyntaxNames[] = {
  "LittleEndianImplicit",
  "LittleEndianExplicit",
  "BigEndianExplicit",
  "JPEG (lossless)",
  "JPEG First Order (lossless)",
  "RLE (lossless)",
  "JPEG 2000 (lossless)",
  "JPEG Baseline (lossy)",
  "JPEG Extended (lossy)",
  "JPEG 2000 (lossy)",
  "JPEG-LS Lossless",
  "JPEG-LS Lossy (Near-Lossless)",
  "UnknownSyntax"
};

void SDICOM::SDICOMPIMPL::dumpDICOMtagout(unsigned pos,
                                          DICOM_ID_PART_LENGTH element1,
                                          DICOM_ID_PART_LENGTH element2,
                                          const char* vrtype,
                                          DICOM_LONG_LENGTH elength,
                                          const char*){
  char* tagdescription;
  DCMTag lookupkey;
  lookupkey.setID(element1,element2);
  if (!DCM_DATA_DIC.count(lookupkey.getID()))
    tagdescription = (char*)"Unknown";
  else tagdescription = DCM_DATA_DIC[lookupkey.getID()].Name;
  std::cout << std::hex  <<"@0x" << std::setfill('0') << std::setw(8)
  << pos << " (" << std::setfill('0')
  << std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
  << element1 << "," << std::setfill('0')
  << std::setw(sizeof(DICOM_ID_PART_LENGTH) * 2)
  << element2 << ") VR:"  << (char*)vrtype << " VL:0x"
  << std::setfill('0') << std::setw(sizeof(DICOM_LONG_LENGTH) * 2)
  << elength <<" " << std::setfill(' ') << std::setw(34);
  if (tagdescription != 0)
    std::cout << tagdescription;
  std::cout << std::endl;
}

/* Read through a file, in byte offsets, to locate a memory word
 * (can change prototype signature to any size *scalar* parameter */
int64_t SDICOM::SDICOMPIMPL::findFirstWord(std::fstream& fsource,
                                      findword_t findbytes){
  findword_t word_f ;
  char       bytestore;
  int64_t    position = fsource.tellg();
  fsource.read((char*)&word_f,sizeof(word_f)); //read the first word, of size n
  while ( word_f != findbytes && fsource.good() && position < 256 ) {
    //shift, read one more byte and continue
    fsource.read((char*)&bytestore,1);
    word_f = word_f >> 8; //assuming MSBy at MSAd, in file
    ((char*)&word_f)[sizeof(word_f)-1] = bytestore;//into MSBy
    position++;
  }
  if (word_f == findbytes) return position;
  else return -1;
}

/* Take a tag value pair (as 32-bit unsigned) and reverse them for use in the
 * above method */
int64_t SDICOM::SDICOMPIMPL::findFirstTag(std::fstream& fsource,
                                     DICOM_ID_LENGTH findtag){
  findword_t reversedhalfwords;
  //reversedhalfwords = *((DICOM_ID_PART_LENGTH*)&findtag);
  DICOM_ID_LENGTH bitmask  = 0xFFFFFFFF;
  bitmask           = bitmask <<
    ((sizeof(DICOM_ID_LENGTH)*8)-(sizeof(DICOM_ID_PART_LENGTH)*8));
  bitmask           = bitmask >>
    ((sizeof(DICOM_ID_LENGTH)*8)-(sizeof(DICOM_ID_PART_LENGTH)*8));
  reversedhalfwords = findtag & bitmask;
  reversedhalfwords = reversedhalfwords << sizeof(DICOM_ID_PART_LENGTH)*8;
  bitmask           = bitmask << (sizeof(DICOM_ID_PART_LENGTH)*8);
  reversedhalfwords = reversedhalfwords | bitmask;
  return findFirstWord(fsource,reversedhalfwords);
}

/* check the length values for crazy sizes, to avoid memory faults */
inline int crazySizeCheck(DICOM_LONG_LENGTH asize){
  const DICOM_LONG_LENGTH threshold = 0xEFFFFFFF;
  if (asize > threshold)
    return 1;
  else
    return 0;
}

/* Read Tag Keys
 * Return: != 0 -> error
 */
int SDICOM::SDICOMPIMPL::readDICOMKeys(std::istream& dcmsource, DCMTag &ttag,
                                       int MetaInfoEnd) {
  DICOM_ID_PART_LENGTH element1    = 0;
  DICOM_ID_PART_LENGTH element2    = 0;
  int                  result      = 0;
  result += readData(dcmsource,(char*)&element1,sizeof(DICOM_ID_PART_LENGTH),
                     true, MetaInfoEnd);
  result += readData(dcmsource,(char*)&element2,sizeof(DICOM_ID_PART_LENGTH),
                     true, MetaInfoEnd);
  /* Place results into tag */
  ttag.setID(element1,element2);
  //make the lookup in to the DCM Data Dict
  if (DataDictionary->contains(ttag.getID()))
    ttag.setName(DataDictionary->getEntry(ttag.getID()).Name);
  DEBUG_OUT("DICOM IO: Done - Tag Keys")
  return result;
}

/* Read VR & Length
 * Return: != 0 -> error
 */
int SDICOM::SDICOMPIMPL::readDICOMVRLength(std::istream& dcmsource,
                                           DCMTag& ttag,
                                           int MetaInfoEnd) {
  /* --READ TAG, perform per-tag ops, read/set VR, read/set Length--
   * Prescribed behaviour varies from here on in, depending on the tag (and VR)
   * Switch based on uint64_t lookup key. This ensures is unique for
   * element key.
   */
  DICOM_LONG_LENGTH    elength     = 0;
  /* for shorter lengths normal lenghts */
  DICOM_LENGTH_LENGTH  slength     = 0;
  char                 vrtype[sizeof(DICOM_VR_LENGTH) +1];
                       vrtype[0] = ' '; vrtype[1] = ' ';
                       vrtype[sizeof(DICOM_VR_LENGTH)] = '\0';
  int                  result      = 0;
  switch (ttag.getID()) {
    case MetaInfoVer: // exception for fruity 'FileMetaInformationVersion' tag
      //read the VR
      result += readData(dcmsource,(char*)vrtype,sizeof(DICOM_VR_LENGTH));
      vrtype[0] = 'O'; vrtype[1] = 'B';
      //read element length
      result += readData(dcmsource,(char*)&slength,sizeof(DICOM_LENGTH_LENGTH),
                         true, MetaInfoEnd);
      elength = slength;
      elength = 6;
      break;
    case ItemMarker: // exception for VR-less items
    case ItemDelim:
    case SequenceDelim:
      vrtype[0] = ' '; vrtype[1] = ' ';
      result += readData(dcmsource,(char*)&elength,sizeof(DICOM_LONG_LENGTH),
                         true, MetaInfoEnd);
      //elength   = 4;
      break;
    default: // should be good for all other tags
      // VR in the case of Implicit syntax (after meta info only)
      if (TransferSyntaxType == LIM && (dcmsource.tellg() >= MetaInfoEnd)){
        if ( DataDictionary->contains(ttag.getID()) ) {
          const char *tvr = DataDictionary->getEntry(ttag.getID()).VR;
          vrtype[0] = tvr[0]; vrtype[1] = tvr[1];
        }
        else{
          vrtype[0] = 'N'; vrtype[1] = 'A';
        }
        //read element length
        result += readData(dcmsource,(char*)&elength,sizeof(DICOM_LONG_LENGTH),
                           true, MetaInfoEnd);
      }
      else {  //Explicit syntax
        //read the VR
        result += readData(dcmsource,(char*)vrtype,sizeof(DICOM_VR_LENGTH));
        //read element length
        result += readData(dcmsource,(char*)&slength,
                           sizeof(DICOM_LENGTH_LENGTH),true, MetaInfoEnd);
        elength = slength;
      }
      break;
  }
  /* Some VR Types have extended lengths. These are handled here. */
  switch ( (vrtype[0]<<(sizeof(char)*8)) | vrtype[1] ){
    case ('O'<<(sizeof(char)*8)) | 'B':
    case ('O'<<(sizeof(char)*8)) | 'F':
    case ('O'<<(sizeof(char)*8)) | 'W':
    case ('U'<<(sizeof(char)*8)) | 'N':
    case ('S'<<(sizeof(char)*8)) | 'Q':
    case ('U'<<(sizeof(char)*8)) | 'T':
    case ('O'<<(sizeof(char)*8)) | 'D':
      /*can ignore previously read value for elength:
        *-> it is undefined for longer VL types */
      if (!(TransferSyntaxType == LIM && (dcmsource.tellg() >= MetaInfoEnd))) {
        DEBUG_OUT("DICOM IO: Reading extended tag size");
        result += readData(dcmsource,(char*)&elength,sizeof(DICOM_LONG_LENGTH),
                           true, MetaInfoEnd);
      }
      __FALLTHROUGH__
    case ('N'<<(sizeof(char)*8)) | 'A':
      /* Apparently sequences can appear in Pixel Data -- uglier than sin */
      if ( (elength == SequenceUnspec) && ((ttag.getID() == PixelData) ||
           // Or, an unknown tag VR type with Sequence unspecified size
                                          ((vrtype[0]='N')&&(vrtype[0]='A'))
                                          )
         ){
        // Appears to be Pixel Data Sequence -- treat as sequence
        vrtype[0] = 'S'; vrtype[1] = 'Q';
      }
      break;
    default:
      break;
  }
  /* Place results into tag */
  ttag.setVR(vrtype[0],vrtype[1]);
  ttag.setDataLength(elength);
  DEBUG_OUT("DICOM IO: Done - VR and length")
  return result;
}

static inline bool isEndianSensitive(DCMTag& ttag) {
  if ( (ttag.getVR()[0] == 'F' && ttag.getVR()[1] == 'L') ||
       (ttag.getVR()[0] == 'F' && ttag.getVR()[1] == 'D') ||
       (ttag.getVR()[0] == 'S' && ttag.getVR()[1] == 'S') ||
       (ttag.getVR()[0] == 'S' && ttag.getVR()[1] == 'L') ||
       (ttag.getVR()[0] == 'U' && ttag.getVR()[1] == 'S') ||
       (ttag.getVR()[0] == 'U' && ttag.getVR()[1] == 'L')
       )
    return true;
  else
    return false;
}

/* Read Data
 * Return: != 0 -> error
 */
int SDICOM::SDICOMPIMPL::readDICOMData(std::istream& dcmsource, DCMTag& ttag,
                                       bool readpixeldata, int MetaInfoEnd) {
  char*             tagdata = nullptr;
  int               result  = 0;
  /* --READ TAG DATA-- */
  if (ttag.getDataLength() == SequenceUnspec){
    /* Undefined Length -- read nothing */
    DEBUG_OUT("DICOM IO: Undefined length on standard tag")
  }
  else {
    //allocate space for the tag value
    if (crazySizeCheck(ttag.getDataLength()))
      return ++result;
    if ((((long int)ttag.getDataLength() <= MaxTagReadSize) || (MaxTagReadSize < 0)) &&
        // skip PixelData if it should not be read
        (!(ttag.getID() == PixelData && (!readpixeldata))) &&
        // including inside PixelData encapsulated sequences
        (! ((!ttag.isTop()) && (ttag.getParent().getID() == PixelData)
        && (!readpixeldata)) ) ) {
      tagdata = new char[ttag.getDataLength()];
      if (tagdata != nullptr) {
        result += readData(dcmsource,tagdata,ttag.getDataLength(),
                          isEndianSensitive(ttag), MetaInfoEnd);
      }
    }
    else {
      // skip the data by seeking past it
      DEBUG_OUT("DICOM IO: Skipping long tag");
      dcmsource.seekg(ttag.getDataLength(),std::ios_base::cur);
    }
  }
  if (tagdata != nullptr)
    ttag.setData(ttag.getDataLength(),tagdata);
  DEBUG_OUT("DICOM IO: Done - Tag Data")
  return result;
}

/* Handle a potential sequence
 * Return: != 0 -> error
 */
int SDICOM::SDICOMPIMPL::handleSequence(std::istream& dcmsource, DCMTag& ttag,
                                        bool readpix, int MetaInfoEnd) {
  /* SQ: Recurse on sequences and Items */
  unsigned retval = 0;
  if ( ((ttag.getVR()[0] == 'S' && ttag.getVR()[1] == 'Q') ||
         (ttag.getID() == ItemMarker))
    && (ttag.getDataLength() > 0)) {

    if ((!ttag.isTop()) &&
          ttag.getParent().getID() == PixelData &&
          ttag.getID() == ItemMarker ) {
      DEBUG_OUT("DICOM IO: Skipping Sequence/Item descent in PixelData");
    }
    else {
      DEBUG_OUT("DICOM IO: Recursing on sequence")
      if (ttag.getDataLength() == SequenceUnspec)
        //read sequence into children - end marker tag should return to parent
        retval = 
               Parent->readDICOMTags(dcmsource, &ttag,0-1,readpix, MetaInfoEnd);
      else {
        std::string       sequencedata(ttag.getData(),
                                      ttag.getDataLength());
        std::stringstream sequence(sequencedata);
        // set MetaInfoEnd to -1, since sequence stream starts at zero again
        retval = Parent->readDICOMTags(sequence, &ttag,0-1,readpix, -1);
      }
      // remove data from memory AND reset the size
      ttag.clearData();
      ttag.setData(0,nullptr);
    }

  }
  return retval;
}

/* Post-Decode actions
 * Return: true if parsing instance should end
 */
unsigned SDICOM::SDICOMPIMPL::postDecodeActions(std::istream& dcmsource,
                                                DCMTag& ttag,
                                                int& MetaInfoEnd, bool) {
  /* --POST-DECODE BEHAVIOUR-- */
  unsigned result = 0;
  std::string tmpstr;
  switch (ttag.getID()){
    case MetaInfoLen:
      MetaInfoEnd = dcmsource.tellg() + ttag.toInt();
      ttag.setMeta(true);
      break;
    case SequenceDelim:
      /* This is the end of a sequence */
      if ( (!ttag.isTop()) && (!ttag.getParent().isTop()) )
        result = 1;
      break;
    case ItemDelim:
      /* This is the end of an item */
      /* N.B. the following check is linked to the method above, for ignoring
       * item descent in PixelData Items */
      if ( (!ttag.isTop()) && (ttag.getParent().getID() == PixelData) )
        result = 0;
      else
        result = 1;
      break;
    case TransferSyntax:
      /* Initialise Transfer Syntax variables */
      ttag.toString(tmpstr);
      Parent->setTransferSyntax(tmpstr);
      break;
    case PixelData:
      if (ttag.getVR()[0] == 'S' && ttag.getVR()[1] == 'Q')
        // correct work-around for sequence in PixelData
        ttag.setVR('O','B');
      // endiannes correction of PixelData
      if (ttag.dataPresent() && (ttag.getTags().size() == 0))
        if ( (TransferSyntaxType == BEX) != sysInfo::isBigEndian)
          reverseBytesBlock(ttag.data(),ttag.getDataLength(),
                            reverseBlockSizePixelData(Parent->getRootTag()));
      break;
  }
  return result;
}

/* read a tree of tags by cycling through the ID,LENGTH,VALUE
   until the end of DICOM headers are reached. Returns the number
   of errors encountered */
int SDICOM::readDICOMTags(std::istream& dcmsource,
                             DCMTag *target,
                             unsigned numtoread,
                             bool readpixeldata,
                             int MetaInfoEnd){
  DEBUG_OUT("DICOM IO: BEGIN HEADER READING");
  unsigned tagcount    = 0;
  DCMTag   *CTag;
  int      result      = 0;

  while ( dcmsource.good() && tagcount < numtoread && (!stop())) {
    int tmpres = 0;
    CTag       = new DCMTag();
    // set the parent tag immediately, so that it can be used
    CTag->setParent(target);
    try {
      // this is a meta tag if it falls within the meta-header size range
      CTag->setMeta((dcmsource.tellg() < MetaInfoEnd) && target->isMeta());
      // read the key pair
      tmpres += PrivateData->readDICOMKeys    (dcmsource, *CTag, MetaInfoEnd);
      // read the VR and the Length (depends on previous key pair)
      tmpres += PrivateData->readDICOMVRLength(dcmsource, *CTag, MetaInfoEnd);
      // read the actual tag data
      tmpres += PrivateData->readDICOMData    (dcmsource, *CTag, 
                                               readpixeldata, MetaInfoEnd);
      if (tmpres > 0) {
          DEBUG_OUT("DICOM IO: Error present, so clearing component");
          CTag->clearData();
      }
    }
    // safety to avoid dangling CTag (but no exceptions should be thrown)
    catch (std::exception &e) {
      DEBUG_OUT("Fatal Exception while reading a single tag.");
      tmpres++;
    }
    /* Add the tag to the class storage vector
     * (if it's a meta-header, try adding to the meta-object first)
     */
    if (CTag->isMeta() &&
       (CTag->getID() != MetaInfoLen) &&
        target->hasTag(MetaInfoLen))
      target->getTag(MetaInfoLen).addTag(CTag);
    else
      target->addTag(CTag, false);
    tagcount++;
    DEBUG_OUT("DICOM IO: Added tag to tree");
    // handle a potential sequence
    tmpres += PrivateData->handleSequence(dcmsource, *CTag,
                                          readpixeldata, MetaInfoEnd);
    // perform any necessary post-decode actions
    // (some post decode actions stipulate that this parse context must end)
    if (PrivateData->postDecodeActions(dcmsource, *CTag,
                                       MetaInfoEnd, readpixeldata)) {
      DEBUG_OUT("DICOM IO: Explicit end of stream");
      break;
    }
    // the following peek enables correct eof reporting after tag reading
    dcmsource.peek();
    // check if an error is found and flag + log it
    if (tmpres) {
      CTag->setError(true);
      std::stringstream posval;
      posval << dcmsource.tellg();
      SLogger::global().addMessage
                            ("SDICOM::readDICOMTags: error @ " + posval.str());
    }
    result = tmpres;
  }
  if (stop())
    doStop(false);
  DEBUG_OUT("DICOM IO: END READING");
  // perform global deidentification
  if (PrivateData->GlobalDeident && target->isTop()) {
    DEBUG_OUT("DICOM IO: GLOBAL DEIDENTIFICATION");
    if (!DCMDataDic::isTagTreeDeidentified(*target)) {
      PrivateData->DataDicIntern.deidentifyTagTree(*target,
                                                  PrivateData->GlobalDeidentMap,
                                                   true,true);
    }
  }
  return result;
}

static inline void reverseBytes(char* dat,std::size_t datsize) {
  std::size_t resteps = datsize / 2; // integer division takes account for odd
  std::size_t cup     = 0;
  while (  cup < resteps ) {
    char tmpval = dat[ (datsize-1) - cup];
    dat[ (datsize-1) - cup] = dat[cup];
    dat[cup] = tmpval;
    cup++;
  }
}

static inline void reverseBytesBlock(char* dat,std::size_t datsize,
                                     short blocksize) {
  if (blocksize == 1)
    reverseBytes(dat,datsize);
  else if (blocksize == 0)
    return;
  else {
    for (size_t i = 0; i < (datsize - blocksize); i+= blocksize )
      reverseBytes(&dat[i],blocksize);
  }
}

static inline short reverseBlockSizePixelData(DCMTag& rootnode) {
  short res = 0;
  if (rootnode.hasTag(PixelData)) {
    if(rootnode.getTag(PixelData).dataPresent()) {
      if (rootnode.hasTag(0x0028,0x0100)) {
        DCMTag &BitsAllocated = rootnode.getTag(0x0028,0x0100);
        long int bits = BitsAllocated.toInt();
        // scale according to samples per pixel
        if (rootnode.hasTag(0x0028,0x0002)) {
          long int samples = rootnode.getTag(0x0028,0x0002).toInt();
          if (rootnode.hasTag(0x0028,0x0006)) {
            long int planarconfig = rootnode.getTag(0x0028,0x0006).toInt();
            // only if this is a composite RGB planar configuration
            if (planarconfig == 0)
              bits *= samples;
          }
          else
            bits *= samples;
        }
        res= bits / 8;
        // now reordering of a single byte stream
        if (res == 1)
          res = 0;
      }
    }
  }
  return res;
};

static const std::size_t ioblocksize = 16384;

inline short SDICOM::SDICOMPIMPL::readData(std::istream& instream, char* data,
                                           unsigned int osize,bool fixorder,
                                           int MetaInfoEnd, short reorderblock){
  // is this file in big endian?
  bool sourceisbig = (TransferSyntaxType == BEX);
  // are were in the meta header? (which is always LEX) 
  if (instream.tellg() < MetaInfoEnd)
    sourceisbig = false;
  // perform the read, blocking according to blocksize
  std::size_t readremain = osize;
  std::size_t read       = 0;
  while (!Parent->stop()) {
    if (readremain < ioblocksize) {
      instream.read(data+read,readremain);
      break;
    }
    else {
      instream.read(data+read,ioblocksize);
      readremain -= ioblocksize;
      read       += ioblocksize;
    }
  }
  // handle endian reordering
  if (instream.fail()){ // && (!noenderror)){
    DEBUG_OUT("DICOM IO: Read specified data length failed");
    //throw simdcmparexcep;
    return 1;
  }
  if ( (sourceisbig != sysInfo::isBigEndian) && fixorder)
    reverseBytesBlock(data,osize,reorderblock);
  return 0;
}

inline short SDICOM::SDICOMPIMPL::writeData(std::ostream& outstream,
                                            const char* data,
                                            unsigned int osize, bool fixorder,
                                            int MetaInfoEnd,
                                            short reorderblock) {
  // is this file in big endian?
  bool targisbig = (TransferSyntaxType == BEX);
  // are were in the meta header? (which is always LEX) 
  if (outstream.tellp() < MetaInfoEnd)
    targisbig = false;
  if ( (targisbig != sysInfo::isBigEndian) && fixorder) {
    char *tmpdat = new char[osize];
    memcpy(tmpdat,data,osize);
    reverseBytesBlock(tmpdat,osize,reorderblock);
    outstream.write(tmpdat,osize);
    delete[] tmpdat;
  }
  else
    outstream.write(data,osize);
  if (outstream.fail()){ // && (!noenderror)){
    DEBUG_OUT("DICOM IO: Write specified data length failed");
    //throw simdcmparexcep;
    return 1;
  }
  return 0;
}

int SDICOM::writeOneTag(std::ostream& dcmtarget, DCMTag& sourcetag,
                        int& MetaInfoEnd) {
  // write out tag values
  bool                 writedata     = true;
  int                  result        = 0;
  DICOM_LENGTH_LENGTH  normal_length = 0;
  DICOM_LONG_LENGTH    long_length   = 0;
  DICOM_ID_PART_LENGTH tag1          = sourcetag.getID1();
  DICOM_ID_PART_LENGTH tag2          = sourcetag.getID2();
  DICOM_ID_LENGTH      tagc          = (tag1 <<
                                       (sizeof(DICOM_ID_PART_LENGTH)*8)) |
                                       (tag2);
  DICOM_LONG_LENGTH    dummylength   = 0;
  DICOM_ID_PART_LENGTH SequenceD1    = (SequenceDelim & 0xFFFF0000) >> 16;
  DICOM_ID_PART_LENGTH ItemD1        = (ItemDelim & 0xFFFF0000) >> 16;
  DICOM_ID_PART_LENGTH SequenceD2    = SequenceDelim & 0x0000FFFF;
  DICOM_ID_PART_LENGTH ItemD2        = ItemDelim & 0x0000FFFF;
  bool                 usechildren; // initialised later

  // Firstly, skip all delimination tags -- they will be written out later
  if ( (tagc == SequenceDelim) || (tagc == ItemDelim) || sourcetag.hasError()
       || (!sourcetag.shouldSave()) )
    return 0;
  // Secondly, skip all tags for which there is a length > 0, but not data
  if ( (!sourcetag.dataPresent()) && (sourcetag.getDataLength() > 0) &&
    // with an exception for unspecified-length sequences with legnth present
       (! ((sourcetag.getDataLength() == SequenceUnspec) 
            && ((sourcetag.getVR()[0] == 'S' && sourcetag.getVR()[1] == 'Q')
                 || (sourcetag.getID() == ItemMarker)) )) )
    return 0;
  // 1: write out IDs
  result += PrivateData->writeData(dcmtarget,(char*)&tag1,
                                   sizeof(DICOM_ID_PART_LENGTH),
                                   true,MetaInfoEnd);
  result += PrivateData->writeData(dcmtarget,(char*)&tag2,
                                   sizeof(DICOM_ID_PART_LENGTH),
                                   true,MetaInfoEnd);
  // 2: write out VR
  if ((!( PrivateData->TransferSyntaxType==LIM &&
          (dcmtarget.tellp() >= MetaInfoEnd))) &&
          (tagc != ItemMarker)) {
    result += PrivateData->writeData(dcmtarget,sourcetag.getVR(),2);
  }
  // 3: write out length
  switch ( (sourcetag.getVR()[0]<<(sizeof(char)*8)) | sourcetag.getVR()[1] ){
    case ('O'<<(sizeof(char)*8)) | 'W':
    case ('O'<<(sizeof(char)*8)) | 'B':
      // PixelData may have a VR of OB, but should be handled as a sequence
      if (sourcetag.getID() == PixelData && 
                        (sourcetag.hasTags() > 0 ||
                         PrivateData->TransferSyntaxType==LIM )) {
        goto pixeldataskip;
      }
      //otherwise, fall-through
      __FALLTHROUGH__
    case ('O'<<(sizeof(char)*8)) | 'F':
    case ('U'<<(sizeof(char)*8)) | 'T':
    case ('U'<<(sizeof(char)*8)) | 'N':
      if ( PrivateData->TransferSyntaxType==LIM &&
          (dcmtarget.tellp() >= MetaInfoEnd))
        result +=
          PrivateData->writeData(dcmtarget,(const char*)&long_length,
                                 sizeof(long_length),true,MetaInfoEnd);
      else
        result +=
          PrivateData->writeData(dcmtarget,(const char*)&normal_length,
                                 sizeof(normal_length));
      long_length = sourcetag.getDataLength();
      result +=
        PrivateData->writeData(dcmtarget,(const char*)&long_length,
                               sizeof(long_length),true,MetaInfoEnd);
      break;
    default:
    pixeldataskip:
    /* Sequences (and later items, see below)
     * Always write implicit length sequences, with an end marker.
     * This is always valid, and avoids a length restriction on sequences
     */
      if ( (sourcetag.getVR()[0] == 'S' && sourcetag.getVR()[1] == 'Q')
          || (sourcetag.getID() == ItemMarker)
          || (sourcetag.getID() == PixelData && sourcetag.hasTags() > 0 ) ) {
        /* If there are child nodes, then output a sequence/item of undefined
         * length (use delimeters). If there are no child nodes, output an
         * encapsultated sequence/item block */
        usechildren = (sourcetag.getTags().size() > 0);
        // FIXME: could change if sequence delimiters are children!!!
        if (usechildren)
          long_length = SequenceUnspec;
        else
          long_length = sourcetag.getDataLength();
        // write the implicit length indicator
        if  (PrivateData->TransferSyntaxType==LIM &&
            (dcmtarget.tellp() >= MetaInfoEnd)) {
          result +=
            PrivateData->writeData(dcmtarget,(const char*)&long_length,
                                   sizeof(long_length),true,MetaInfoEnd);
        }
        else {
          // need to squirt out a filler for sequences
          if (tagc != ItemMarker)
            result +=  PrivateData->writeData(dcmtarget,(const char*)&dummylength,
                                              sizeof(DICOM_LENGTH_LENGTH));
          result +=
            PrivateData->writeData(dcmtarget,(const char*)&long_length,
                                   sizeof(long_length),true,MetaInfoEnd);
        }
        //output children
        if (usechildren) {
          for (unsigned child=0;child < sourcetag.getTags().size();child++)
            result += writeOneTag(dcmtarget,*(sourcetag.getTags())[child],
                                  MetaInfoEnd);
          // output delimeters
          if (tagc == ItemMarker) { // an item delim
              result += PrivateData->writeData(dcmtarget,(const char*)&ItemD1,
                                               sizeof(ItemD1),true,MetaInfoEnd);
              result += PrivateData->writeData(dcmtarget,(const char*)&ItemD2,
                                               sizeof(ItemD2),true,MetaInfoEnd);
          }
          else { // a sequence delim
            result += PrivateData->writeData(dcmtarget,(const char*)&SequenceD1,
                                           sizeof(SequenceD1),true,MetaInfoEnd);
            result += PrivateData->writeData(dcmtarget,(const char*)&SequenceD2,
                                           sizeof(SequenceD2),true,MetaInfoEnd);
          }
          result += PrivateData->writeData(dcmtarget,(const char*)&dummylength,
                                           sizeof(dummylength));
          // do not try to write any tag data
          writedata = false;
        }
        break;
      }

      if ( PrivateData->TransferSyntaxType==LIM &&
          (dcmtarget.tellp() >= MetaInfoEnd)) {
        long_length = sourcetag.getDataLength();
        result +=
          PrivateData->writeData(dcmtarget,(const char*)&long_length,
                                 sizeof(long_length),true,MetaInfoEnd);
      }
      else {
        normal_length = sourcetag.getDataLength();
        result +=
          PrivateData->writeData(dcmtarget,(const char*)&normal_length,
                                 sizeof(normal_length),true,MetaInfoEnd);
      }
      break;
  }
  // 4: write out data
  if (writedata) {
    if (sourcetag.dataPresent())
    result +=
      PrivateData->writeData(dcmtarget,sourcetag.getData(),
                             sourcetag.getDataLength(),
                             isEndianSensitive(sourcetag),MetaInfoEnd);
    else {
      if (sourcetag.getDataLength() > 0) {
        DEBUG_OUT("ERROR! Attempting to write data that is not present!");
        throw SimulacrumDCMException();
      }
    }
  }
  return result;
}

int SDICOM::SDICOMPIMPL::writeMetaHeaders(std::ostream& dcmtarget,
                                          int& MetaInfoEnd) {
  if (TransferSyntaxType == TUK) {
    DEBUG_OUT("Unknown TransferSyntax specified");
    SLogger::global().addMessage("SDICOM::writeMetaHeaders: Unknow Tran-X");
  }
  // First, remove existing Meta-Headers; this method will generate new ones
  for ( unsigned tag=0; tag < Parent->getTags().size(); tag++) {
    if (Parent->getTags()[tag]->isMeta())
      Parent->removeTag(Parent->getTags()[tag]);
  }
  // Now generate the new meta-headers
  DCMTag *metainfotag = genMetaHeader();
  // Set the end end-of-meta-headers marker
  MetaInfoEnd = DICOM_PREAMBLE_LENGTH + sizeof(DICOM_PREFIX_STRING) + 4 + 2 + 2
                + 4 + metainfotag->toInt();
  // Write them out, as normal tags
  Parent->writeOneTag(dcmtarget,*metainfotag,MetaInfoEnd);
  for (unsigned t=0; t<metainfotag->getTags().size(); t++)
    Parent->writeOneTag(dcmtarget,*(metainfotag->getTags())[t],MetaInfoEnd);
  // Finally, add them back to the tree, as the single meta header
  Parent->addTag(metainfotag);
  return 0;
}

int SDICOM::writeDICOMTags(std::ostream& dcmtarget, DCMTag& sourcetag,
                           bool dometa, bool standardize) {
  int result = 0;
  int MetaInfoEnd = -1;
  if (standardize)
    standardizeTag(sourcetag,true);
  tagmap_t &sourcetags = sourcetag.getTagsMap();
  // first write out the obligatory meta headers
  if (dometa)
    PrivateData->writeMetaHeaders(dcmtarget, MetaInfoEnd);
  // cycle through each header in the list, serialising output to stream
  tagmap_t::iterator tagit;
  for ( tagit = sourcetags.begin(); tagit != sourcetags.end(); tagit++) {
    if (!(tagit->second->isMeta())) {
      if ( (tagit->second->getID() == PixelData) &&
           ((PrivateData->TransferSyntaxType == BEX) != sysInfo::isBigEndian)
         ) {
        // need to reorder pixeldata format from native rep. to target
        reverseBytesBlock(tagit->second->data(),
                          tagit->second->getDataLength(),
                          reverseBlockSizePixelData(sourcetag));
        result += writeOneTag(dcmtarget,*tagit->second, MetaInfoEnd);
        // and back again, since this was in-place
        reverseBytesBlock(tagit->second->data(),
                          tagit->second->getDataLength(),
                          reverseBlockSizePixelData(sourcetag));
      }
      else {
        result += writeOneTag(dcmtarget,*tagit->second, MetaInfoEnd);
      }
    }
    if (stop()) {
      doStop(false);
      result = 1;
      break;
    }
  }
  return result;
}

DCMTag* SDICOM::SDICOMPIMPL::genMetaHeader() {
  DCMTag *metainfotag = new DCMTag(0x0002,0x0000); metainfotag->setVR('U','L');
  DCMTag *metavers    = new DCMTag(0x0002,0x0001); metavers->setVR   ('O','B');
  DCMTag *medsopcl    = new DCMTag(0x0002,0x0002); medsopcl->setVR   ('U','I');
  DCMTag *medsopinst  = new DCMTag(0x0002,0x0003); medsopinst->setVR ('U','I');
  DCMTag *transyn     = new DCMTag(0x0002,0x0010); transyn->setVR    ('U','I');
  DCMTag *implcl      = new DCMTag(0x0002,0x0012); implcl->setVR     ('U','I');
  DCMTag *implnm      = new DCMTag(0x0002,0x0013); implnm->setVR     ('S','H');
  DCMTag *srcae       = new DCMTag(0x0002,0x0016); srcae->setVR      ('A','E');
  uint16_t LMetaVersion = MetaVersion;
  // Set their values
  // meta version
  char *newver = new char[2];
  strncpy(newver,(const char*)&LMetaVersion,2);
  metavers->setData(2,newver);
  // SOP Class
  medsopcl->fromString("99999.99999.99999.99999");
  // -> should match the SOPClassUID
  if (Parent->hasTag(0x0008,0x0016))
    medsopcl->fromString(Parent->getTag(0x0008,0x0016).toString());
  // SOP Instance
  medsopinst->fromString("99999.99999.99999.99999");
  // -> should match the SOPInstanceUID
  if (Parent->hasTag(0x0008,0x0018))
    medsopinst->fromString(Parent->getTag(0x0008,0x0018).toString());
  // TransferSyntax
  transyn->fromString(Parent->getTransferSyntax());
  // ImplementationClass
  implcl->fromString(ImplementationUID);
  // ImplementationName
  implnm->fromString(ImplementationName);
  // AETitle
  srcae->fromString(AETitle);
  // Meta Size
  unsigned short tagsoverhead = (7*(4+2+2)) + 4; // size excluding data
  metainfotag->addTag(metavers);
  metainfotag->addTag(medsopcl);
  metainfotag->addTag(medsopinst);
  metainfotag->addTag(transyn);
  metainfotag->addTag(implcl);
  metainfotag->addTag(implnm);
  metainfotag->addTag(srcae);
  metainfotag->setMeta(true);
  standardizeTag(*metainfotag,true);
  metainfotag->fromInt(metavers->getDataLength()    +
                       medsopcl->getDataLength()    +
                       medsopinst->getDataLength()  +
                       transyn->getDataLength()     +
                       implcl->getDataLength()      +
                       implnm->getDataLength()      +
                       srcae->getDataLength()       +
                       tagsoverhead );
  return metainfotag;
}

bool SDICOM::SDICOMPIMPL::isDICOM(const char *dcmfile) {
  std::fstream  DCMFile;
  uint32_t      readword = 0;
  DCMFile.open(dcmfile, std::ios::in | std::ios::binary);
  if (!DCMFile.is_open())
    return false;
  /* Now check the magic post-preamble string */
  DCMFile.seekg(DICOM_PREAMBLE_LENGTH);
  DCMFile.read((char*)&readword,sizeof(readword));
  DCMFile.close();
  if (readword == DICOM_PREFIX_STRING)
    return true;
  else
    return false;
}

void SDICOM::SDICOMPIMPL::initFriendlyRootTag() {
  // configure convenience attributes of root tag
  RootTag.setName("DICOM Resource");
  RootTag.setVR('L','O');
  RootTag.fromString(DICOMPath);
  RootTag.setID(ROOT_TAG_ID);
}

int SDICOM::setLocation(const std::string& location){
  changeLocation(location);
  PrivateData->Valid = PrivateData->isDICOM(getLocation().c_str());
  return PrivateData->Valid;
}

void SDICOM::changeLocation(const std::string& newlocation) {
  PrivateData->DICOMPath = newlocation;
  PrivateData->initFriendlyRootTag();
}

const std::string& SDICOM::getLocation() const {
  return PrivateData->DICOMPath;
}

const SCoordinate& SDICOM::getExtent() {
  /* INITIALISE IT -HERE- FIRST!
   * - First, assume a 2D image
   * - Then, check for 3rd dimension
   * - (rows, columns & depth are well defined DICOM tags)
   */
  if (!( (hasTag(0x0028,0x0011)) && (hasTag(0x0028,0x0010)) ))
    throw SimulacrumDCMPresenceException();
  //columns
  const DCMTag& width  = getTag(0x0028,0x0011);
  //rows
  const DCMTag& height = getTag(0x0028,0x0010);

  //number of frames (enhanced)
  if (!(hasTag(0x0028,0x0008))) {
    PrivateData->Extent.setDim(2);
    PrivateData->Extent.setCoord(0,(SCoordinate::Precision)width.toInt());
    PrivateData->Extent.setCoord(1,(SCoordinate::Precision)height.toInt());
  }
  else {
    const DCMTag& depth = getTag(0x0028,0x0008);
    PrivateData->Extent.setDim(3);
    PrivateData->Extent.setCoord(0,(SCoordinate::Precision)width.toInt());
    PrivateData->Extent.setCoord(1,(SCoordinate::Precision)height.toInt());
    PrivateData->Extent.setCoord(2,(SCoordinate::Precision)depth.toInt());
  }
  return PrivateData->Extent;
}

void SDICOM::printTag(const DCMTag& atag, bool writelen,
                      bool writevr, bool writename,
                      unsigned depth, bool showstruct) {
  if (showstruct) {
    for (unsigned i=0; i<depth; i++)
      std::cout << " |";
    if (atag.isMeta())
      std::cout << "-";
  }
  std::cout << std::hex
    << "(" << std::setfill('0') << std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
    << atag.getID1() << "," << std::setfill('0')
    << std::setw(sizeof(DICOM_ID_PART_LENGTH) * 2)
    << atag.getID2() << ")" << '\t';
  if(writevr)
    std::cout << "VR:"  << (char*)atag.getVR() << '\t';
  if(writelen)
    std::cout << "VL:0x" << std::setfill('0')
              << std::setw(sizeof(DICOM_LONG_LENGTH) * 2)
              << atag.getDataLength() << '\t';
  if(writename){
    std::cout  << std::setfill(' ') << std::setw(40);
    std::cout << atag.getName() << '\t';
  }
  std::cout << "[";
  std::string ObjectValue;
  atag.toStringSafe(ObjectValue);
  std::cout << ObjectValue;
  std::cout << "]";
  if (atag.hasError())
    std::cout << "\t*CONTAINS ERROR*";
  if ((!atag.dataPresent())&&(atag.getDataLength()>0)&&(atag.hasTags() == 0))
    std::cout << "\t(data not read)";
  std::cout<< std::endl;
}

bool SDICOM::isValid() const {
  return PrivateData->Valid;
}

void SDICOM::SDICOMPIMPL::readPreamble(std::fstream &DCMFile) {
  char preamble[DICOM_PREAMBLE_LENGTH+1];
  DCMFile.read(preamble,DICOM_PREAMBLE_LENGTH); // skip preamble and DICM string
  preamble[DICOM_PREAMBLE_LENGTH] = '\0';
  Parent->setPreambleString(std::string(preamble));
  uint32_t dcmstr;
  DCMFile.read((char*)&dcmstr,4);
  if ( dcmstr != DICOM_PREFIX_STRING ) {
    DEBUG_OUT("DICM read error: endian problem?");
    throw SimulacrumDCMException();
  }
}

void SDICOM::SDICOMPIMPL::writePreamble(std::fstream& dcmout) {
  for ( unsigned i = 0; i < DICOM_PREAMBLE_LENGTH; i++)
    dcmout.put(PREAMBLESTR[i % PREAMBLESTR.length()]);
  uint32_t dcmtmp = DICOM_PREFIX_STRING;
  dcmout.write((char*)&dcmtmp,sizeof(dcmtmp));
}

const std::string& SDICOM::getPreambleString() const {
  return PrivateData->PREAMBLESTR;
}

void SDICOM::setPreambleString(const std::string& newstr) {
  if (newstr.length() > 0)
    PrivateData->PREAMBLESTR = newstr;
}

int SDICOM::loadAllTags(bool readpixeldata) {
  int readstatus;
  PrivateData->RootTag.clearTags();
  if (isValid()) {
    /* IMPORTANT: construction of local string of getLocation is necessary,
     * because getLocation passes reference to value changed by set method */
    setLocation(std::string(getLocation()));
    std::fstream DCMFile;
    DCMFile.open(getLocation().c_str(), std::ios::in | std::ios::binary);
    PrivateData->readPreamble(DCMFile);
    readstatus=readDICOMTags(DCMFile,&(PrivateData->RootTag),-1,readpixeldata);
    DCMFile.close();
    return readstatus;
  }
  else {
    throw SimulacrumDCMException();
  }
}

int SDICOM::writeAllTags(bool standardize) {
  std::fstream DCMFile;
  DCMFile.open(PrivateData->DICOMPath.c_str(), std::ios::out |
                                               std::ios::binary |
                                               std::ios::trunc);
  if ( !DCMFile.is_open() )
    throw SimulacrumDCMException();
  PrivateData->writePreamble(DCMFile);
  return writeDICOMTags(DCMFile, PrivateData->RootTag, true, standardize);
}

void SDICOM::store() {
  writeAllTags();
}

const std::string& SDICOM::getTransferSyntax() {
  return PrivateData->TransferSyntaxs[PrivateData->TransferSyntaxType];
}

const std::string& SDICOM::getTransferSyntaxName() {
  return PrivateData->TransferSyntaxNames[PrivateData->TransferSyntaxType];
}

SimulacrumLibrary::str_enc SDICOM::stringEncoding() {
  // default to raw encoding
  SimulacrumLibrary::str_enc result = SimulacrumLibrary::str_enc::Raw;
  auto tagenc = DCMTag(0x0008,0x0005);
  if(hasTag(tagenc.getID1(),tagenc.getID2())) {
    return getRootTag().getTag(tagenc.getID()).stringEncoding();
  }
  return result;
}

void SDICOM::setTransferSyntax(const std::string& transyn) {
  unsigned synnum;
  for (synnum = 0; synnum < PrivateData->TransferSyntaxNum - 1; synnum++)
    if (!transyn.compare(PrivateData->TransferSyntaxs[synnum]))
      break;
  /* should have fallen through to the matching UID or the default */
  PrivateData->TransferSyntaxType = synnum;
  DEBUG_OUT("TransferSyntax:");
  DEBUG_OUT(getTransferSyntax());
  if (PrivateData->TransferSyntaxType == TUK) {
    SLogger::global().addMessage
             ("SDICOM::setTransferSyntax: Unknown Transfer Syntax: " + transyn);
  }
}

void SDICOM::setAETitle(const std::string& newae){
  PrivateData->AETitle = newae;
}

void SDICOM::setMaxTagReadSize(long int newsize) {
  PrivateData->MaxTagReadSize = newsize;
}

void SDICOM::setDataDictionary(DCMDataDic* newdict) {
  PrivateData->DataDictionary = newdict;
}

const tagset_t& SDICOM::getTags() {
  return PrivateData->RootTag.getTags();
}

DCMTag& SDICOM::getRootTag() {
  return PrivateData->RootTag;
}

SAbsTreeNode& SDICOM::getRoot() {
  return PrivateData->RootTag;
}


void SDICOM::refresh(bool pixeldata) {
  try {
    loadAllTags(pixeldata);
  } catch (std::exception &e) {
    PrivateData->RootTag.clearTags();
  }
}

std::string SDICOM::getInfo(const std::string &) {
  std::string infostring;
  if (isValid()) {
    SFile fileinfo(getLocation());
    SURI  uriinfo;
    uriinfo.setSeparator(SFile::getSystemSeparator());
    uriinfo.setURI(getLocation());
    std::stringstream fsize;
    // roudn the size to 1 DP
    if (fileinfo.exists())
      fsize << (static_cast<float>
                    (static_cast<int>((fileinfo.size()/1024.0) * 10.0)) / 10.0);
    else
      fsize << 0;
    infostring += "<table spacing=\"5\">";
    //type
    infostring += "<tr><td align=\"right\"><b>Type</b></td><td>DICOM File";
    infostring += "</td></tr>";
    //size
    infostring += "<tr><td align=\"right\"><b>Size</b></td><td>"
                  + fsize.str() + " KiB</td></tr>";
    //transfersyntax
    infostring += "<tr><td align=\"right\"><b>Transfer Syntax</b></td><td>"
                  + getTransferSyntaxName()+"</td></tr>";
    if (hasTag(0x0008,0x0060)) {
      infostring += "<tr><td align=\"right\"><b>Modality</b></td><td>";
      infostring += getTag(0x0008,0x0060).toString();
      infostring += "</td></tr>";
    }
    //contains image
    infostring += "<tr><td align=\"right\"><b>Contains Image</b></td><td>";
    if (hasSSpace())
      infostring += "Yes";
    else
      infostring += "No";
    infostring += "</td></tr>";
    try {
    // image extent
      if (hasSSpace())
        infostring += "<tr><td align=\"right\"><b>Image Extent</b></td><td>"
                  + getExtent().toString() + "</td></tr>";
    }
    catch (std::exception &) {}
    //filename
    infostring += "<tr><td align=\"right\"><b>FileName</b></td></tr>";
    infostring += "<tr><td colspan=\"2\">";
    if (uriinfo.depth() > 0)
      infostring += uriinfo.getComponent(uriinfo.depth()-1);
    else
      infostring += getLocation();
    infostring += "</td></tr></table>";
  }
  return infostring;
}

bool SDICOM::hasArchive() {
  return false;
}

bool SDICOM::hasSSpace(const std::string &) {
  return PrivateData->RootTag.hasTag(PixelData);
}

bool SDICOM::PixelDataPresent() {
  if (PrivateData->RootTag.hasTag(PixelData) && 
      (PrivateData->RootTag.getTag(PixelData).dataPresent()))
    return true;
  else
    return false;
}

void SDICOM::loadMissingData() {
  if (hasSSpace() && (!(PixelDataPresent()))) {
    SDICOM tmpdicom;
    tmpdicom.setLocation(getLocation());
    if (tmpdicom.isValid()) {
      tmpdicom.loadAllTags(true);
      if (tmpdicom.getRootTag().hasTag(PixelData)) {
        DCMTag &pixdatt  = getRootTag().getTag(PixelData);
        DCMTag &pixdattn = tmpdicom.getRootTag().getTag(PixelData);
        tagset_t pxdattags = pixdatt.getTags();
        // assign contents but do not remove any existing tags
        if (pxdattags.size() > 0) { // has child items for compress tranx
          tagset_t pxdattagsn = pixdattn.getTags();
          if (pxdattags.size() == pxdattagsn.size()) {
            for (unsigned t=0; t<pxdattags.size(); t++) {
              if (pxdattags[t]->getTags().size() == 0) {
                *(pxdattags[t]) = *(pxdattagsn[t]);
              }
            }
          }
        }
        else { // single top-level tag
          pixdatt = pixdattn;
        }
      }
    }
  }
}

void SDICOM::getSSpaceInto(SSpace& target, const std::string&) {
  loadSSpace(target);
}

const DCMTag& SDICOM::getTag(DICOM_ID_PART_LENGTH lid1,
                                     DICOM_ID_PART_LENGTH lid2) {
  return PrivateData->RootTag.getTag(lid1,lid2);
}

bool SDICOM::hasTag(DICOM_ID_PART_LENGTH lid1, DICOM_ID_PART_LENGTH lid2) {
  return PrivateData->RootTag.hasTag(lid1,lid2);
}


bool SDICOM::removeTag(const DCMTag* tagtogo){
  return PrivateData->RootTag.removeTag(tagtogo);
}

bool SDICOM::removeTag(DICOM_ID_PART_LENGTH lid1,
                               DICOM_ID_PART_LENGTH lid2){
  return PrivateData->RootTag.removeTag(lid1,lid2);
}

void SDICOM::addTag(DCMTag* newtag, bool check){
  PrivateData->RootTag.addTag(newtag,check);
}

static bool zAxisEnh(SDICOM& dcmsrc,SVector &sliceposdiff) {
  bool res = false;
  DCMTag &dcmroot = dcmsrc.getRootTag();
  sliceposdiff.reset();
  if (dcmroot.hasTag(0x52009230)) { //PerFrameFunctionalGroupsSequence
    DCMTag &perframe = dcmroot.getTag(0x52009230);
    if (perframe.NodeChildrenNum() > 3) { // at least two slices available
      std::vector<SVector> posvects;
      for (unsigned i=0; i<2; i++) {
        DCMTag &frameitem = *perframe.getTags()[i];
        if (frameitem.hasTag(0x00209113)) { // PlanePositionSequence
          DCMTag &planeposseq = frameitem.getTag(0x00209113);
          if (planeposseq.hasTag(0xfffee000)) { // planepositem
            DCMTag planeposit = planeposseq.getTag(0xfffee000);
            if (planeposit.hasTag(0x00200032)) { // ImagePositionPatient
              DCMTag &imgpospat = planeposit.getTag(0x00200032);
              posvects.push_back(imgpospat.toVector());
            }
          }
        }
      }
      if (posvects.size() > 1) {
        sliceposdiff = posvects[1] - posvects[0];
        res = true;;
      }
    }
  }
  return res;
}

static bool zAxisMulti(SDICOM &d1,SDICOM &d2,SVector &sliceposdiff) {
  bool res = false;
  sliceposdiff.reset();
  if (   d1.getRootTag().hasTag(0x00200032)
      && d2.getRootTag().hasTag(0x00200032) ) {
    sliceposdiff = d2.getRootTag().getTag(0x00200032).toVector() -
                   d1.getRootTag().getTag(0x00200032).toVector();
    res = true;
  }
  return res;
}

bool SDICOM::postLoadSSpaceConfiguration(SSpace& targetspace,SDICOM& refdcm,
                                         bool forcez, SVector *fzaxisdiff) {
  bool result = false;
  SElem::Ptr nativepox = targetspace.getNativeSElem();
  unsigned BPP = 0;
  SElem::Precision zerolevel = 0;
  long int wlcentre, wlwidth;
  float rescaleslope, rescaleintercept;
  //----------------------------------------------------------------------------
  // if this is signed value rep, use the zero offet for the specified WL
  //----------------------------------------------------------------------------
  if (targetspace.selemDataStore().length() > 0) {
    nativepox->source(targetspace.selemDataStore().SElems(0));
    zerolevel = nativepox->zero();
  }
  if (refdcm.getRootTag().hasTag(0x0028,0x0101))
    BPP = refdcm.getRootTag().getTag(0x0028,0x0101).toInt();
  if ( refdcm.getRootTag().hasTagDFS(0x0028,0x0100) ) {
    // check for rescale tags
    if (refdcm.hasTag(0x0028,0x1053)) {
      rescaleslope = refdcm.getTag(0x0028,0x1053).toFloat();
    }
    else {
      rescaleslope = 1.0;
    }
    if (refdcm.hasTag(0x0028,0x1052)) {
      rescaleintercept = refdcm.getTag(0x0028,0x1052).toFloat();
    }
    else {
      rescaleintercept = 0.0;
    }
    if ( refdcm.getRootTag().hasTagDFS(0x0028,0x1051) ) {
      wlwidth = (refdcm.getRootTag().getTagDFS(0x0028,0x1051).toInt(0))
                 * rescaleslope;
    }
    else { // assume full width
      wlwidth = pow(2.0,(int)BPP);
    }
    if ( refdcm.getRootTag().hasTagDFS(0x0028,0x1050) ) {
      wlcentre = refdcm.getRootTag().getTagDFS(0x0028,0x1050).toInt(0)
                  + zerolevel - rescaleintercept;
    }
    else { // assume midpoint
      wlcentre = pow(2.0,(int)BPP)/2;
    }
    // Only set a window level for grey-scale images
    long int samples = 0;
    if (refdcm.hasTag(0x0028,0x0002))
      samples = refdcm.getTag(0x0028,0x0002).toInt();
    if (samples <= 1) {
      targetspace.LUT().genLUT(BPP, wlcentre, wlwidth);
      targetspace.LUT().useWL(true);
      // check photometric interpretation -- invert WL if necessary
      if ( refdcm.getRootTag().hasTag(0x0028,0x0004) ) {
        std::string photoint =
                           refdcm.getRootTag().getTag(0x0028,0x0004).toString();
        if (photoint == "MONOCHROME1") {
          targetspace.LUT().invertWL(true);
        }
      }
      result = true;
    }
  } else {
    result = false;
    DEBUG_OUT("Sufficient tags not present to configure window level");
  }
  //----------------------------------------------------------------------------
  // try to configure spatial information
  //----------------------------------------------------------------------------
  try {
    DICOM_ID_LENGTH pixelspacingid = 0x00280030;
    DICOM_ID_LENGTH directcosineid = 0x00200037;
    DICOM_ID_LENGTH imagepositid   = 0x00200032;
    DICOM_ID_LENGTH frameofref     = 0x00200052;
    SVector sliceposdiff;
    bool corrzaxis = forcez;
    // use the forced zaxis difference here if specified
    if (corrzaxis)
      sliceposdiff = *fzaxisdiff;
    else
      corrzaxis = zAxisEnh(refdcm,sliceposdiff);

    if (refdcm.getRootTag().hasTag(0x0018,0x1164))
      pixelspacingid = 0x00181164;
    // voxel spacing
    if (refdcm.getRootTag().hasTagDFS(pixelspacingid)) {
      SVector spacinginfo;
      DCMTag &pixelspacing = refdcm.getRootTag().getTagDFS(pixelspacingid);
      if (pixelspacing.isArray()) {
        SVector spacing = pixelspacing.toVector();
        spacing.setDim(refdcm.getExtent().getDim());
        if (refdcm.getRootTag().hasTagDFS(0x0018,0x0050)) { //SliceThickness
          // 3D spacing
          if (spacinginfo.getDim() < 3)
            spacinginfo.setDim(3);
          DCMTag &slicethickness = refdcm.getRootTag().getTagDFS(0x0018,0x0050);
          double slicespacing = slicethickness.toFloat();
          if (refdcm.getRootTag().hasTagDFS(0x0018,0x0088)) {
            // spacing between slices takes precedence if extant
            DCMTag &spcbetweenslices =
                                   refdcm.getRootTag().getTagDFS(0x0018,0x0088);
            slicespacing = SGeom::fabs(spcbetweenslices.toFloat());
          }
          spacinginfo.xyz(spacing[0],spacing[1],slicespacing);
        }
        else {
          // 2D spacing
          if (spacinginfo.getDim() < 2)
            spacinginfo.setDim(2);
          spacinginfo.xy(spacing[0],spacing[1]);
        }
        // if this was an enhanced DICOM, override any z-axis information
        if (corrzaxis) {
          if (spacinginfo.getDim() < 3)
            spacinginfo.setDim(3);
          spacinginfo.z(sliceposdiff.mag());
        }
        targetspace.setSpacing(spacinginfo);
        std::string units("mm");
        targetspace.setSpacingUnits(units);
      }
      // orientation information (as meaning vectors) and 
      if (refdcm.getRootTag().hasTagDFS(directcosineid)) {
        // first, create meaning vectors inside image sace
        DCMTag &dircos = refdcm.getRootTag().getTagDFS(directcosineid);
        SVector dircosvect = dircos.toVector();
        // sanity check of dimensions
        if (dircosvect.getDim() == 6) {
          SVector cosx(3), cosy(3), cosz(3), patx(3), paty(3), patz(3);
          cosx.xyz(dircosvect[0],dircosvect[1],dircosvect[2]);
          cosy.xyz(dircosvect[3],dircosvect[4],dircosvect[5]);
          // infer the direction cosine for z (but we don't know pos-dir!)
          cosz = cosx.cross(cosy);
          // check enhanced DICOM information, to see if we can figure out the
          // direction of positive along the z-axis
          if (corrzaxis) {
            SVector zaxisdir = sliceposdiff.unit();
            if ( zaxisdir == (SVector(cosz.getDim()) - cosz) ) {
              // we should flip our understanding of the zaxisdir
              cosz = (SVector(cosz.getDim()) - cosz);
            }
          }
          // pull out the patient axis basis
          patx.xyz(cosx.x(),cosy.x(),cosz.x());
          paty.xyz(cosx.y(),cosy.y(),cosz.y());
          patz.xyz(cosx.z(),cosy.z(),cosz.z());
          // assign meaning vectors to SSpace
          SVector patnull(3);
          targetspace.assignVectorMeaning(patx, "L");
          targetspace.assignVectorMeaning(patnull-patx, "R");
          targetspace.assignVectorMeaning(paty, "P");
          targetspace.assignVectorMeaning(patnull-paty, "A");
          targetspace.assignVectorMeaning(patz, "H");
          targetspace.assignVectorMeaning(patnull-patz, "F");
          // now attemt to create the mapping functions to/from patient space
          if (refdcm.getRootTag().hasTagDFS(imagepositid)) {
            //extract image position and convert to svector
            DCMTag &imgpos = refdcm.getRootTag().getTagDFS(imagepositid);
            SVector imgposv = imgpos.toVector();
            // check that 3 dims are specified
            if (imgposv.getDim() == 3) {
              // bind mapping functions to oritentation and spacing
              std::function<SVector(SSpace*,const SVector&)>
                                                          topatient,toimage;
              topatient = std::bind(&SDICOMGeom::imageToPatient,patx,paty,patz,
                                    imgposv,spacinginfo,
                                    std::placeholders::_1,
                                    std::placeholders::_2);
              toimage   = std::bind(&SDICOMGeom::patientToImage,patx,paty,patz,
                                    imgposv,spacinginfo,
                                    std::placeholders::_1,
                                    std::placeholders::_2);
              // assign to the sspace
              targetspace.setToGlobalSpaceFunction(topatient);
              targetspace.setFromGlobalSpaceFunction(toimage);
              // specify a global space ID
              if (refdcm.getRootTag().hasTagDFS(frameofref)) {
                targetspace.setGlobalSpaceID(
                          refdcm.getRootTag().getTagDFS(frameofref).toString());
              }
            }
          }
        }
      }
    }
  }
  catch (std::exception &spaceexcep) {
    DEBUG_OUT("Exception during configuration of DICOM spatial information");
  }
  //----------------------------------------------------------------------------
  // assign a name to the space
  //----------------------------------------------------------------------------
  if (refdcm.getRootTag().hasTag(SOPInstUID_)) {
    targetspace.setName(refdcm.getRootTag().getTag(SOPInstUID_).toString());
  }
  return result;
}

bool SDICOM::globalDeident() {
  return SDICOMPIMPL::GlobalDeident;
}

void SDICOM::setGlobalDeident(bool newident) {
  SDICOMPIMPL::GlobalDeident = newident;
}

NNode& SDICOM::globalDeidentMap() {
  return SDICOMPIMPL::GlobalDeidentMap;
}

static int loadJP2KtoSSpace(SSpace& target, DCMTag &JPEGItemTag) {
  int result = -1;
  if (JPEGItemTag.dataPresent()) {
    if (SJ2K::decodeBuffer(JPEGItemTag.getData(),
                           JPEGItemTag.getDataLength(),
                           target))
      result = 0;
  }
  return result;
}

static int loadLosslessJPEGtoSSpace(SSpace& target, DCMTag &JPEGItemTag,
                                    unsigned short bitsalloc) {
  int result = -1;
  if (JPEGItemTag.dataPresent()) {
    if (SLJPEG::decodeBuffer_16bittarg(JPEGItemTag.getData(),
                                       JPEGItemTag.getDataLength(),
                                       bitsalloc,
                                       target))
      result = 0;
  }
  return result;
}

static int loadJPEGtoSSpace(SSpace& target, DCMTag &JPEGItemTag) {
  int result = -1;
  if (JPEGItemTag.dataPresent()) {
    if (SLJPEG::decodeBuffer_RegularJPEG(JPEGItemTag.getData(),
                                         JPEGItemTag.getDataLength(),
                                         target))
      result = 0;
  }
  return result;
}

static int loadJPLStoSSpace(SSpace& target, DCMTag &JPEGItemTag) {
  int result = -1;
  if (JPEGItemTag.dataPresent()) {
    if (SJPEGLS::decodeBuffer(JPEGItemTag.getData(),
                              JPEGItemTag.getDataLength(),
                              target))
      result = 0;
  }
  return result;
}

static int foldOverPixelItems(SSpace &target,
                              std::function<int(SSpace&,DCMTag&)> pdloader,
                              DCMTag &PixelDataParent, bool issigned) {
  unsigned bound = PixelDataParent.getTags().size();
  int retval = 0;
  if (bound == 2 || 
      ( 
        (bound == 3) && 
          (PixelDataParent.getTags()[2]->getID() == SequenceDelim)
      )
     ) {
    retval = pdloader(target,*(PixelDataParent.getTags()[1]));
  }
  else  if (bound > 2) {
    std::vector<SSpace>  stack_sspaces(bound-1);
    std::vector<SSpace*> intermediates_sspace;
    for (unsigned item = 1; item < bound; item++) {
      DCMTag &itemtag = *(PixelDataParent.getTags()[item]);
      if (itemtag.getID() != ItemMarker)
        continue;
      retval += pdloader(stack_sspaces[item-1],itemtag);
      if (!(stack_sspaces[item-1].extent().volume() > 0))
        continue;
      intermediates_sspace.push_back(&stack_sspaces[item-1]);
    }
    target.concatenate(intermediates_sspace);
  }
  // fixup signed representation where necessary (only for simple types)
  if (issigned) {
    SElem::Ptr itype = target.getNativeSElem();
    BW16SElem bw16type(nullptr);
    // is this data signed, but decoded as unsigned?
    if (typeid(*itype).name() == typeid(bw16type).name()) {
      // force reinterpretation
      target.setNativeSElemType(new BW16SignedSElem(nullptr));
    }
  }
  return retval;
}

int SDICOM::SDICOMPIMPL::loadNativetoSSpace
                                     (SSpace& targetimage, bool allowstealing) {
  // prepare various parameters
  const DCMTag& pixelbits  = Parent->getTag(0x0028,0x0100);
        DCMTag& pixeldata  = Parent->getRootTag().getTag(0x7FE0,0x0010);
  const DCMTag& pixelrepr  = Parent->getTag(0x0028,0x0103);
  bool          isRGB      = false;
  bool          isSigned   = false;
  bool          isInterLvd = true;
  int           success    = 0;
  // check if this is rgb or not
  if ( Parent->hasTag(0x0028,0x0002) && 
      (Parent->getTag(0x0028,0x0002).toInt() == 3) ) {
    isRGB = true;
    targetimage.setNativeSElemType(new RGBAI32SElem(nullptr));
  }
  else {
    if (pixelbits.toInt() == 16) {
      if (pixelrepr.toInt() == PX_MONO)
        targetimage.setNativeSElemType(new BW16SElem(nullptr));
      else if (pixelrepr.toInt() == PX_MONO_SIGNED)
        targetimage.setNativeSElemType(new BW16SignedSElem(nullptr));
    }
    else {
      if(pixelbits.toInt() == 8) {
        targetimage.setNativeSElemType(new BW8SElem(nullptr));
      }
      else {
        targetimage.setNativeSElemType(new TightSElem(nullptr));
      }
    }
  }
  // check if the format is interleaved
  if ( Parent->hasTag(0x0028,0x0006) &&
      (Parent->getTag(0x0028,0x0006).toInt() == 1) ) {
    isInterLvd = false;
  }
  // check the smaple format
  switch (pixelrepr.toInt()) {
    case PX_MONO:
      isSigned = false;
      break;
    case PX_MONO_SIGNED:
      isSigned = true;
      break;
    default:
      isSigned = false;
      break;
  }
  if (pixeldata.dataPresent()) {
    // --- PixelData has already been read into memory---
    // --------------------------------------------------
    // Certain native formats can make use of a (dangerous) low-level transfer,
    // as they use a format organised and readily understood by SSpace and 
    // the SElemExemplar class
    if ( 
         ( (pixelbits.toInt() == 16) || (pixelbits.toInt() == 8)) && 
         (!isRGB) // format matches BW16|BW16Signed
         && (!targetimage.nativeSElemTypeLocked()) // format could be anything
       ) {
      if (allowstealing) {
        // trigger the SElemStore to use this new memory allocation
        targetimage.selemDataStore().useData((SElem::DataSource)pixeldata.data(),
                                             pixeldata.getDataLength());
        // detach it from the DICOM
        pixeldata.setData(pixeldata.getDataLength(),nullptr);
      }
      else {
        char *newpxdat = new char[pixeldata.getDataLength()];
        memcpy(newpxdat,pixeldata.data(),pixeldata.getDataLength());
        targetimage.selemDataStore().useData((SElem::DataSource)newpxdat,
                                             pixeldata.getDataLength());
      }
      // size up the container, which will keep the data
      targetimage.resize(Parent->getExtent());
    }
    else {
      // Otherwise, use a represetnation-safe tabular read, into an SI image
      targetimage.resize(Parent->getExtent());
      std::iostream *pixelstream;
      std::string pixeldatal(pixeldata.getData(),pixeldata.getDataLength());
      pixelstream = new std::stringstream(pixeldatal);
      // perform the actual read
      success = readTabulatedData(targetimage,*pixelstream,
               pixelbits.toInt()/8,isRGB,isSigned,isInterLvd,&(Parent->stop()));
      delete pixelstream;
    }
  } else {
    // PixelData is not present
    throw SimulacrumDCMPresenceException();
  }
  return success;
}


int SDICOM::loadSSpace(SSpace& targetimage) {
  if (isValid()){
    int res = 1;
    // load in the missing pixel data if necessary
    bool pixdatpres = PixelDataPresent();
    bool issigned   = false;
    if (!pixdatpres)
      loadMissingData();
    // pixelbits, pixeldata and pixelrepr are well defined tags
    // try to read pixel information
    if (!((hasTag(0x0028,0x0100))&&
          (hasTag(0x7FE0,0x0010))&&
          (hasTag(0x0028,0x0103)))){
      throw SimulacrumDCMPresenceException();
    }
    // check sign of data here (since it should be passed to decoders)
    if (hasTag(0x0028,0x0103)) {
      const DCMTag& pixelrepr  = getTag(0x0028,0x0103);
      if (pixelrepr.toInt() == PX_MONO_SIGNED)
        issigned = true;
    }
    SIO::loadSSpace(targetimage);
    // Check the transfer syntaxs
    switch (PrivateData->TransferSyntaxType) {
      case LIM:
      case LEX:
      case BEX:
        // allow PixelData stealing, when it was not present initially
        res = PrivateData->loadNativetoSSpace(targetimage,!pixdatpres);
        break;
      case JLF: /* main JPEG Losses */
      case JLL:
        res = foldOverPixelItems(targetimage,
                                 std::bind(loadLosslessJPEGtoSSpace,
                                           std::placeholders::_1,
                                           std::placeholders::_2,
                                           getTag(0x0028,0x0100).toInt()),
                                 getRootTag().getTag(0x7FE0,0x0010),issigned);
        break;
      case JPB: /* Baseline, regular JPEG */
        res = foldOverPixelItems(targetimage,
                                 loadJPEGtoSSpace,
                                 getRootTag().getTag(0x7FE0,0x0010),issigned);
        break;
      case J2L:
      case J2S:
        res = foldOverPixelItems(targetimage,
                                 loadJP2KtoSSpace,
                                 getRootTag().getTag(0x7FE0,0x0010),issigned);
        break;
      case JLS:
      case JL2:
        res = foldOverPixelItems(targetimage,
                                 loadJPLStoSSpace,
                                 getRootTag().getTag(0x7FE0,0x0010),issigned);
        break;
      case JPE: /* Extended 12-bit */
      default:
        targetimage.setName("Load Error");
        SLogger::global().addMessage(
        std::string("SDICOM: Cannot decode transfer syntax: ") + 
             PrivateData->TransferSyntaxNames[PrivateData->TransferSyntaxType]);
        targetimage.refresh(true);
        // clear the PixelData for objects where it was not present at loadtime
        if ((!pixdatpres) && getRootTag().hasTag(PixelData)) {
          tagset_t pxchildren = getRootTag().getTag(PixelData).getTags();
          for (unsigned t=0; t<pxchildren.size(); t++) {
            pxchildren[t]->clearData();
          }
          getRootTag().getTag(PixelData).clearData();
        }
        throw SimulacrumDCMSyntaxException();
        break;
    }
    /* copy the DICOM tree to the target image information tree */
    SAbsTreeNodeConversions::DCMTagToNNode
                                   (getRootTag(),targetimage.informationNode());
    postLoadSSpaceConfiguration(targetimage,*this);
    targetimage.postLoadConfig();
    targetimage.refresh(true);
    // clear the PixelData for objects where it was not present at loadtime
    if ((!pixdatpres) && getRootTag().hasTag(PixelData)) {
      tagset_t pxchildren = getRootTag().getTag(PixelData).getTags();
      for (unsigned t=0; t<pxchildren.size(); t++) {
        pxchildren[t]->clearData();
      }
      getRootTag().getTag(PixelData).clearData();
    }
    return res;
  }
  else {
    throw SimulacrumDCMImageException();
    return 1;
  }
}

int SDICOM::storeSSpace(SSpace& targ) {
  SIO::storeSSpace(targ);
  makeSecondaryCapture(targ);
  setNativePixelData(targ);
  store();
  return 0;
}

void SDICOM::makeSecondaryCapture(SSpace& source) {
  DICOM_ID_LENGTH tagid;
    //ImageType--------------------------
    tagid = 0x00080008;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString("DERIVED\\SECONDARY\\OTHER");
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('C','S');
      newtag->fromString("DERIVED\\SECONDARY\\OTHER");
      getRootTag().addTag(newtag);
    }
    //SOPClassUID--------------------------
    tagid = 0x00080016;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString("1.2.840.10008.5.1.4.1.1.7");
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','I');
      newtag->fromString("1.2.840.10008.5.1.4.1.1.7");
      getRootTag().addTag(newtag);
    }
    //SOPInstanceUID--------------------------
    tagid = 0x00080018;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString(sysInfo::genGUIDString());
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','I');
      newtag->fromString(sysInfo::genGUIDString());
      getRootTag().addTag(newtag);
    }
    //StudyDate--------------------------
    tagid = 0x00080020;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('D','A');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("StudyDate")) {
      getRootTag().getTag(tagid).fromString(
                source.informationNode().getChildNode("StudyDate").NodeValue());
    }
    //ContentDate--------------------------
    tagid = 0x00080023;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','I');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("ContentDate")) {
      getRootTag().getTag(tagid).fromString(
              source.informationNode().getChildNode("ContentDate").NodeValue());
    }
    //StudyTime--------------------------
    tagid = 0x00080030;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('T','M');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("StudyTime")) {
      getRootTag().getTag(tagid).fromString(
                source.informationNode().getChildNode("StudyTime").NodeValue());
    }
    //ContentTime--------------------------
    tagid = 0x00080033;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('T','M');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("ContentTime")) {
      getRootTag().getTag(tagid).fromString(
              source.informationNode().getChildNode("ContentTime").NodeValue());
    }
    //AccessionNumber--------------------------
    tagid = 0x00080050;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('S','H');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("AccessionNumber")) {
      getRootTag().getTag(tagid).fromString(
          source.informationNode().getChildNode("AccessionNumber").NodeValue());
    }
    //Modality--------------------------
    tagid = 0x00080060;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('C','S');
      newtag->fromString("OT");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("Modality")) {
      getRootTag().getTag(tagid).fromString(
                 source.informationNode().getChildNode("Modality").NodeValue());
    }
    //ConversionType--------------------------
    tagid = 0x00080064;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString("WSD");
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('C','S');
      newtag->fromString("WSD");
      getRootTag().addTag(newtag);
    }
    //ReferringPhysicianName-------------------
    tagid = 0x00080090;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('P','N');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("ReferringPhysicianName")) {
      getRootTag().getTag(tagid).fromString(
                source.informationNode().
                            getChildNode("ReferringPhysicianName").NodeValue());
    }
    //PatientName-------------------
    tagid = 0x00100010;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('P','N');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("PatientName")) {
      getRootTag().getTag(tagid).fromString(
              source.informationNode().getChildNode("PatientName").NodeValue());
    }
    //PatientID-------------------
    tagid = 0x00100020;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('L','O');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("PatientID")) {
      getRootTag().getTag(tagid).fromString(
                source.informationNode().getChildNode("PatientID").NodeValue());
    }
    //PatientBirthDate-------------------
    tagid = 0x00100030;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('D','A');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("PatientBirthDate")) {
      getRootTag().getTag(tagid).fromString(
         source.informationNode().getChildNode("PatientBirthDate").NodeValue());
    }
    //PatientSex-------------------
    tagid = 0x00100040;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('C','S');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("PatientSex")) {
      getRootTag().getTag(tagid).fromString(
                source.informationNode().getChildNode("PatientSex").NodeValue());
    }
    //StudyInstanceUID-------------------
    tagid = 0x0020000d;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','I');
      newtag->fromString(sysInfo::genGUIDString());
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("StudyInstanceUID")) {
      getRootTag().getTag(tagid).fromString(
         source.informationNode().getChildNode("StudyInstanceUID").NodeValue());
    }
    //SeriesInstanceUID-------------------
    tagid = 0x0020000e;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','I');
      newtag->fromString(sysInfo::genGUIDString());
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("SeriesInstanceUID")) {
      getRootTag().getTag(tagid).fromString(
        source.informationNode().getChildNode("SeriesInstanceUID").NodeValue());
    }
    //StudyID-------------------
    tagid = 0x00200010;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('S','H');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("StudyID")) {
      getRootTag().getTag(tagid).fromString(
                  source.informationNode().getChildNode("StudyID").NodeValue());
    }
    //SeriesNumber-------------------
    tagid = 0x00200011;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('I','S');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("SeriesNumber")) {
      getRootTag().getTag(tagid).fromString(
             source.informationNode().getChildNode("SeriesNumber").NodeValue());
    }
    //InstanceNumber-------------------
    tagid = 0x00200013;
    if (!getRootTag().hasTag(tagid)) {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('I','S');
      newtag->fromString("");
      getRootTag().addTag(newtag);
    }
    if (source.informationNode().hasChildNode("InstanceNumber")) {
      getRootTag().getTag(tagid).fromString(
           source.informationNode().getChildNode("InstanceNumber").NodeValue());
    }
}

bool SDICOM::compressJPEGLS() {
  bool retval = true;
  if (hasSSpace()) {
    if (PrivateData->TransferSyntaxType != JLS) {
      SSpace decoded;
      int ret = loadSSpace(decoded);
      if (ret == 0) {
        SSlicer decodedslice(decoded);
        std::vector<SIO::imgStreamData> pxdat;
        //encode it
        for (unsigned s=0; s<decodedslice.depth(); s++) {
          SSpace slicerout;
          decodedslice.setSlice(s);
          decodedslice.refresh(true);
          SIO::imgStreamData encdat;
          slicerout.setNativeSElemType(decoded.getNativeSElemP());
          slicerout = decodedslice;
          if (SJPEGLS::encodeBuffer(slicerout, encdat,0)) {
              pxdat.push_back(encdat);    
          }
          else {
            retval = false; // failed to encode
          }
        }
        // now modify the PixelData
        if (getRootTag().hasTag(PixelData)) {
          DCMTag &pd = getRootTag().getTag(PixelData);
          tagset_t pdtags = pd.getTags();
          for (unsigned p=0; p<pdtags.size(); p++) { // remove delimiters
            if ((pdtags[p]->getID() == ItemDelim) ||
                (pdtags[p]->getID() == SequenceDelim) ) {
              pdtags.erase(pdtags.begin()+p);
              p=0;
            }
          }
          pd.clearData();
          pd.setData(0,nullptr);
          pd.setVR('S','Q');
          if (pdtags.size() == (pxdat.size() + 1)) {
            // preserve existing items in memory
            for (unsigned p=0; p<pxdat.size(); p++) {
              pdtags[p+1]->setData(pxdat[p].size,pxdat[p].data);
              pxdat[p].data = nullptr;
            }
          pd.setVR('O','B');
          }
          else {
            if (pdtags.size() == 0) {
              pd.clearData();
              pd.setData(0,nullptr);
              pd.setVR('S','Q');
              // add new tags
              pd.addTag(DCMTag::genItemTag()); // prefix item
              for (unsigned p=0; p<pxdat.size(); p++) {
                DCMTag *newitem = DCMTag::genItemTag();
                newitem->setData(pxdat[p].size,pxdat[p].data);
                pxdat[p].data = nullptr;
                pd.addTag(newitem,false);
              }
              pd.setVR('O','B');
            }
            else {
              // something weird is happening
              retval = false;
              SLogger::global().addMessage("SDICOM::compressJPEGLS: error 1");
            }
          }
        }
        else {
          retval = false;
          SLogger::global().addMessage("SDICOM::compressJPEGLS: error 2");
        }
        // cleanup any lingering pxdat
        for (unsigned p=0; p<pxdat.size(); p++) {
          if (pxdat[p].data != nullptr) {
            delete [] pxdat[p].data;
          }
        }
      }
    }
    else {
      SLogger::global().addMessage("SDICOM::compressJPEGLS: already JPEGLS");
    }
  }
  else {
    SLogger::global().addMessage("SDICOM::compressJPEGLS: no image data present");
    retval = false;
  }
  if (retval) { // was successful, set the transx
    setTransferSyntax("1.2.840.10008.1.2.4.80");
  }
  return retval;
}

void SDICOM::setNativePixelData(SSpace& source) {
  unsigned short  samples  = 0;
  unsigned short  planar   = 0;
  unsigned short  frames   = 0;
  unsigned        rows     = 0;
  unsigned        cols     = 0;
  std::string     photint  = "";
  std::string     spacing  = "1\\1";
  unsigned short  bitsaloc = 0;
  unsigned short  bitsstor = 0;
  unsigned short  bithigh  = 0;
  unsigned short  pixelrep = 0;
  unsigned        smallest = 0;
  unsigned        largeest = 0;
  unsigned        dims     = source.extent().getDim();
  DICOM_ID_LENGTH tagid    = 0;
  SElem::Precision bytes    = 0;
  // check if the source is of a valid extent
  if ( dims == 3 || dims == 2 ) {
    // set the transfer syntax
    if (sysInfo::isBigEndian) {
      //big endian explicit
    }
    else {
      // little endian explicit
    }
    // configure variables
    BW16SElem       bw16bit(nullptr);
    BW16SignedSElem bw16bitsign(nullptr);
    BW8SElem        bw8bit(nullptr);
    SElem::Ptr          nativep = source.getNativeSElem();
    if ( typeid(*nativep) == typeid(bw16bit) ||
         typeid(*nativep) == typeid(bw16bitsign) ) {
      // 16 bit
      samples  = 1;
      planar   = 0;
      photint  = "MONOCHROME2";
      bitsaloc = 16;
      bitsstor = 16;
      bithigh  = 15;
      pixelrep = 0;
      smallest = 0;
      largeest = 65535;
      bytes    = 2;
    }
    else {
      if (typeid(*nativep) == typeid(bw8bit)) {
        // 8 bit
        samples  = 1;
        planar   = 0;
        photint  = "MONOCHROME2";
        bitsaloc = 8;
        bitsstor = 8;
        bithigh  = 7;
        pixelrep = 0;
        smallest = 0;
        largeest = 255;
        bytes    = 1;
      }
      else {
        // just use RGB output
        samples  = 3;
        planar   = 0;
        photint  = "RGB";
        bitsaloc = 8;
        bitsstor = 8;
        bithigh  = 7;
        pixelrep = 0;
        smallest = 0;
        largeest = 255;
        bytes    = 1;
      }
    }
    // set remaining tags
    cols = source.extent().x();
    rows = source.extent().y();
    if (dims == 3)
      frames = source.extent().z();
    else
      frames = 1;
    if ( (source.spacing().getDim() > 1) && (source.spacingUnits() == "mm") ) {
      std::stringstream conv;
      conv << source.spacing().x() << "\\" << source.spacing().y();
      spacing = conv.str();
    }
    // add/set the tags
    //--------------------------
    tagid = 0x00280002;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(samples);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(samples);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280004;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString(photint);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('C','S');
      newtag->fromString(photint);;
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280006;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(planar);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(planar);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280008;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(frames);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('I','S');
      newtag->fromInt(frames);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280010;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(rows);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(rows);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280011;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(cols);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(cols);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280030;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromString(spacing);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('D','S');
      newtag->fromString(spacing);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280100;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(bitsaloc);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(bitsaloc);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280101;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(bitsstor);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(bitsstor);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280102;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(bithigh);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(bithigh);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280103;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(pixelrep);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(pixelrep);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280106;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(smallest);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(smallest);
      getRootTag().addTag(newtag);
    }
    //--------------------------
    tagid = 0x00280107;
    if (getRootTag().hasTag(tagid))
      getRootTag().getTag(tagid).fromInt(largeest);
    else {
      DCMTag *newtag = new DCMTag();
      newtag->setID(tagid);
      newtag->setVR('U','S');
      newtag->fromInt(largeest);
      getRootTag().addTag(newtag);
    }
    // generate and add the pixeldata
    DCMTag *pixeldat;
    if (!getRootTag().hasTag(PixelData)) {
      pixeldat = new DCMTag();
      pixeldat->setID(PixelData);
      pixeldat->setVR('O','B');
      getRootTag().addTag(pixeldat);
    }
    else {
      pixeldat = &(getRootTag().getTag(PixelData));
    }
    std::stringstream pixdatstream;
    SIO::writeTabulatedData(source,pixdatstream,bytes,(samples == 3));
    char *finaldat = new char[pixdatstream.tellp()];
    memcpy(finaldat,pixdatstream.str().c_str(),pixdatstream.tellp());
    pixeldat->setData(pixdatstream.tellp(),finaldat);
  }
  else {
   // THROW!
  }
}

SDICOM::SDICOM(): PrivateData(new SDICOMPIMPL(this)) {
  reset();
}

SDICOM::SDICOM(const SDICOM& targ): PrivateData(new SDICOMPIMPL(this)) {
  reset();
  setLocation(targ.getLocation());
}

const std::string& SDICOM::implementationName() {
  return SDICOMPIMPL::ImplementationName;
}

const std::string& SDICOM::implementationUID() {
  return SDICOMPIMPL::ImplementationUID;
}

const std::string& SDICOM::implementationVersion() {
return SDICOMPIMPL::SDICOMVersion;
}

SDICOM& SDICOM::operator=(const SDICOM& targ) {
  setLocation(targ.getLocation());
  return *this;
}

SDICOM& SDICOM::operator=(SDICOM& targ) {
  setLocation(targ.getLocation());
  getRootTag() = targ.getRootTag();
  return *this;
}

void SDICOM::reset(){
  PrivateData->Valid                    = false;
  PrivateData->DICOMPath                = "";
  PrivateData->TransferSyntaxType       = TUK;
  PrivateData->MaxTagReadSize           = -1;
  PrivateData->DataDictionary           = &(PrivateData->DataDicIntern);
  setPreambleString("!tU yKtYk SgQkY yKtYk");
  PrivateData->RootTag.clear();
  PrivateData->RootTag.setMeta(true);
  PrivateData->Extent.reset();
  setAETitle(sysInfo::systemName());
  // Use a default transfer syntax of LittleEndianExplicit
  setTransferSyntax("1.2.840.10008.1.2.1");
  return;
}

void SDICOM::clear() {
  reset();
}

SDICOM::~SDICOM(){
  reset();
  delete PrivateData;
}

void SDICOM::standardizeTag(DCMTag& ttag, bool recursive) {
  // assign an even length to non-even string tags, by padding
  if ( ((ttag.getDataLength() % 2) != 0) && ttag.dataPresent() ) {
    char *newdata = new char [ttag.getDataLength()+1];
    memcpy(newdata,ttag.getData(),ttag.getDataLength());
    if (ttag.isString() &&
                          (!(ttag.getVR()[0] == 'U' && ttag.getVR()[1] == 'I'))
       )
      newdata[ttag.getDataLength()] = ' ';
    else
      newdata[ttag.getDataLength()] = '\0';
    ttag.clearData();
    ttag.setData(ttag.getDataLength()+1,newdata);
  }
  // TODO: order child array by IDs
  // now recurse on children if necessary
  if (recursive)
    for (unsigned i = 0; i < ttag.getTags().size(); i++)
      standardizeTag(*ttag.getTags()[i], recursive);
}

/* ----------------- Simulacrum DICOM geometry mathods  ----------------------*/

SVector SDICOMGeom::imageToPatient(SVector &x, SVector &y, SVector &z,
                                   SVector &offset, SVector &spacing,
                                   SSpace*, const SVector& coord) {
  SVector lcoord,result(3);
  // apply voxel spacing deform
  lcoord = coord * spacing;
  SVector::Precision mag = lcoord.mag();
  SVector lcoordunit    = lcoord.unit();
  // project onto patient axis
  result.xyz((mag * lcoordunit.dot(x)),
             (mag * lcoordunit.dot(y)),
             (mag * lcoordunit.dot(z)));
  // add to corner location
  result += offset;
  return result;
}

SVector SDICOMGeom::patientToImage(SVector &x, SVector &y, SVector &z,
                                   SVector &offset, SVector &spacing,
                                   SSpace*, const SVector& coord) {
  SVector result(3), lcoord;
  // make relative to image offset
  lcoord = coord - offset;
  lcoord.setDim(3); // ensure there are enough dimensions to avoid problems
  // project onto image axis
  result = (x*lcoord.x()) + (y*lcoord.y()) + (z*lcoord.z());
  // reverse spacing deform
  result = result / spacing;
  return result;
}

/* ------------------ Simulacrum DICOM loading mathods  ----------------------*/

void handleResourceLoad(SResource* target, bool *stopevent) {
  if (!(*stopevent)) {
    SURI  fetcher = target->URI();
    fetcher.externalStop(*stopevent);
    SFile fileref = fetcher.file();
    target->setLocation(fileref.getLocation());
    if (!(*stopevent))
      target->refresh(true);
  }
}

void SDICOMLoaders::loadDICOMS(std::vector< SDICOM >& target,
                               const std::vector< std::string >& pathlist,
                               bool &stopevent) {
  SPool threadedloader;
  target.resize(pathlist.size());
  for (unsigned i = 0; i < pathlist.size(); i++) {
    target[i].setLocation(pathlist[i]);
#ifdef _WIN32
    SURI remcheck(pathlist[i]);
    if (remcheck.isLocal()) { // only thread on on local FS
#endif
      threadedloader.addJob
                   (std::bind(&handleResourceLoad,&target[i],&stopevent),false);
#ifdef _WIN32
    }
    else {
      handleResourceLoad(&target[i],&stopevent);
    }
#endif
  }
  threadedloader.dispatcher();
  threadedloader.wait();
}

std::vector< SDICOM* > SDICOMLoaders::genSortedInstanceList
                                           (std::vector< SDICOM >& sourcelist) {
  std::vector <SDICOM*> res;
  res.resize(sourcelist.size()+1,nullptr); // instance numbers start from 0 or 1?
  // set the pointer positions based on (valid) instance numbers
  for (unsigned i = 0 ; i < sourcelist.size(); i++ ) {
    if (sourcelist[i].getRootTag().hasTag(InstanceNum_)) {
      DCMTag &InstNumTag = sourcelist[i].getRootTag().getTag(InstanceNum_);
      long int InstNum   = InstNumTag.toInt();
      if (InstNum > 0) {
        if ((unsigned)InstNum > (res.size()-1))
          res.resize(InstNumTag.toInt()+1,nullptr);
        res[(unsigned)InstNum] = &(sourcelist[i]);
      }
    }
  }
  unsigned bound = res.size();
  for (long int i = 0 ; (unsigned)i < bound; i++ )
    if (res[i] == nullptr) {
      std::stringstream msg;
      if (i>0) {
        msg << "SDICOMLoaders::genSortedInstanceList: removing potentially " 
            << "missing instance @ " << i;
        SLogger::global().addMessage(msg.str(),SLogLevels::MEDIUM);
      }
      res.erase(res.begin()+i);
      i--;
      bound = res.size();
    }
  return res;
}

void SDICOMLoaders::combineDICOMS(SSpace& target,
                              std::vector< SDICOM* > sources, bool &stopevent) {
  std::vector<SSpace>  intermediates;
  std::vector<SSpace*> intermediates_sspace;
  SVector zaxisdiff;
  intermediates.resize(sources.size());
  SPool progupdates;
  float step, accum = 0.0;
  int lastupdate = 0;
  step = 100.0 / (float)sources.size();
  for (unsigned i = 0; i < sources.size(); i++) {
    if (stopevent) break;
    if (sources[i] == nullptr) continue;
    sources[i]->loadSSpace(intermediates[i]);
    intermediates_sspace.push_back(&intermediates[i]);
    accum += step;
    if ( ((int)accum) > lastupdate) {
      lastupdate = (int) accum;
      // background progress updates
      progupdates.addJob(std::bind(&SSpace::progress, &target, lastupdate));
    }
  }
  target.concatenate(intermediates_sspace);
  // fixup z-axis issues after combination
  if ( sources.size() > 1 &&
       zAxisMulti(*(sources[0]),*(sources[1]),zaxisdiff) ) {
    SDICOM::postLoadSSpaceConfiguration(target,*(sources[0]),true,&zaxisdiff);
  }
  progupdates.wait();
}
