/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Implementation for DICOM Tags
 * Author: M.A.Hicks
 */

#include "dcmtag.h"
#include <string.h>
#include <Core/definitions.h>
#include "datadic.h"
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SLogger/slogger.h>

using namespace Simulacrum;

/*-------- A class to store DICOM tags, including conversion methods ---------*/

class DCMTag::DCMTagPIMPL {
public:
  DCMTag                    *Parent;
  DICOM_ID_PART_LENGTH       ID1;
  DICOM_ID_PART_LENGTH       ID2;
  char                       VR[3];
  const char*                Name;
  char*                      Data;
  bool                       isParent;
  bool                       ownsName;
  bool                       isMetaTag;
  DICOM_LONG_LENGTH          Length;
  tagset_t                   Children;
  tagmap_t                   ChildrenMap;
  bool                       Error;
  bool                       SaveMe;
  DCMTagPIMPL() {
    Parent = nullptr;
    Data = nullptr;
    Name = nullptr;
  }
};

DCMTag::DCMTag (): PrivateData(new DCMTagPIMPL) {
  PrivateData->ownsName = false;
  setIsParent(true);
  clear();
}

DCMTag::DCMTag (DICOM_ID_PART_LENGTH lID1,DICOM_ID_PART_LENGTH lID2,
                          char VR1, char VR2, const char* lName, char* lData,
                DICOM_LONG_LENGTH lLength): PrivateData(new DCMTagPIMPL) {
  PrivateData->ownsName  = false;
  setIsParent(true);
  clear();
  PrivateData->ID1       = lID1;
  PrivateData->ID2       = lID2;
  PrivateData->VR[0]     = VR1;
  PrivateData->VR[1]     = VR2;
  PrivateData->VR[2]     = '\0';
  PrivateData->Name      = lName;
  PrivateData->Data      = lData;
  PrivateData->Length    = lLength;
}

DCMTag::DCMTag(DICOM_ID_PART_LENGTH lID1, DICOM_ID_PART_LENGTH lID2): 
                                                  PrivateData(new DCMTagPIMPL) {
  PrivateData->ownsName = false;
  setIsParent(true);
  clear();
  setID(lID1,lID2);
}

DCMTag::DCMTag(const DCMTag& that): PrivateData(new DCMTagPIMPL) {
  PrivateData->ownsName = false;
  setIsParent(true);
  (*this) = that;
}

DCMTag::~DCMTag (){
  clear();
  delete PrivateData;
}

DCMTag& DCMTag::operator=(const DCMTag& rhs) {
  clear();
  PrivateData->ID1 = rhs.PrivateData->ID1;
  PrivateData->ID2 = rhs.PrivateData->ID2;
  PrivateData->VR[0] = rhs.PrivateData->VR[0];
  PrivateData->VR[1] = rhs.PrivateData->VR[1];
  if (rhs.PrivateData->ownsName) {
    std::size_t namelength = strlen(rhs.PrivateData->Name);
    char * newname = new char[namelength+1];
    strncpy(newname,rhs.PrivateData->Name,namelength);
    setName(newname, true);
  } else
    setName(rhs.getName());
  if (rhs.dataPresent()) {
    char * newdata = new char[rhs.PrivateData->Length];
    memcpy(newdata,rhs.PrivateData->Data,rhs.PrivateData->Length);
    setData(rhs.PrivateData->Length,newdata);
  }
  // now copy the children too
  for (unsigned child=0; child < rhs.PrivateData->Children.size(); child++) {
    DCMTag *newchild = new DCMTag();
    *newchild = *(rhs.PrivateData->Children[child]);
    this->addTag(newchild,false);
  }
  return *this;
}

DCMTag& DCMTag::operator=(const dcm_info_store& dicinfo) {
  setID(dicinfo.ID);
  setVR(dicinfo.VR[0],dicinfo.VR[1]);
  setName(dicinfo.Name);
  return *this;
}

bool DCMTag::operator==(const DCMTag& rhs) const {
  if (PrivateData->isMetaTag != rhs.PrivateData->isMetaTag)  return false;
  if (PrivateData->ID1 != rhs.PrivateData->ID1)              return false;
  if (PrivateData->ID2 != rhs.PrivateData->ID2)              return false;
  if (strncmp(PrivateData->VR,rhs.PrivateData->VR,2)!=0)     return false;
  if (PrivateData->Length != rhs.PrivateData->Length)        return false;
  if (strncmp(PrivateData->Name,rhs.PrivateData->Name,2)!=0) return false;
  if (memcmp(PrivateData->Data,rhs.PrivateData->Data,2)!=0)  return false;
  return true;
}

bool DCMTag::operator<(const DCMTag& rhs) const {
  return getID() < rhs.getID();
}

tagset_t& DCMTag::getTags(){
  return PrivateData->Children;
}

tagmap_t& DCMTag::getTagsMap() {
  return PrivateData->ChildrenMap;
}

DICOM_ID_PART_LENGTH DCMTag::getID1 () const{
  return PrivateData->ID1;
}

DICOM_ID_PART_LENGTH DCMTag::getID2 () const{
  return PrivateData->ID2;
}

DICOM_ID_LENGTH DCMTag::getID() const {
  DICOM_ID_LENGTH result;
  result = PrivateData->ID1;
  result = result << (sizeof(PrivateData->ID1)*8);
  result = result | PrivateData->ID2;
  return result;
}

const std::string DCMTag::getIDstr() const {
  std::stringstream nums;
  nums << std::hex
       << std::setfill('0') << std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
       << getID1()
       <<','
       << std::setfill('0') << std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
       << getID2();
  return nums.str();
}


const char* DCMTag::getVR  () const {
  return PrivateData->VR;
}

const char* DCMTag::getName() const {
  if (PrivateData->Name) return PrivateData->Name;
  else return "";
}

const char* DCMTag::getData() const{
  return PrivateData->Data;
}

char* DCMTag::data() {
  return PrivateData->Data;
}

DICOM_LONG_LENGTH DCMTag::getDataLength() const{
  return PrivateData->Length;
}

DICOM_LONG_LENGTH DCMTag::getDataLengthUnpadded() const {
  int offset = 0;
  if (PrivateData->Data){
    if (PrivateData->Length % 2 == 0)
      if (PrivateData->Data[PrivateData->Length-1] == 0x00 ||
          PrivateData->Data[PrivateData->Length-1] == ' ')
        offset = -1;
  }
  return PrivateData->Length + offset;
}

DCMTag& DCMTag::getTag(DICOM_ID_PART_LENGTH lid1,
                             DICOM_ID_PART_LENGTH lid2) {
  DCMTag findtag;
  findtag.setID(lid1,lid2);
  return getTag(findtag.getID());
}

DCMTag& DCMTag::getTag(DICOM_ID_LENGTH lid) {
  // return immediate child first
  if (getTagsMap().count(lid))
    return *(getTagsMap().find(lid)->second);
  // handle lookups into single item children
  if (PrivateData->Children.size() > 0 && (lid != ItemMarker)
      && (PrivateData->Children[0]->getID() == ItemMarker)) {
    return PrivateData->Children[0]->getTag(lid);
  }
  else {
    throw SimulacrumDCMTagsException();
  }
}

DCMTag& DCMTag::getTagDFS(DICOM_ID_LENGTH lid) {
  if (hasTag(lid)) return getTag(lid);
  else {
    for ( unsigned pos=0; pos < PrivateData->Children.size(); pos++ )
      if ( PrivateData->Children[pos]->hasTagDFS(lid) )
        return PrivateData->Children[pos]->getTagDFS(lid);
    throw SimulacrumDCMTagsException();
  }
}

DCMTag& DCMTag::getTagDFS(DICOM_ID_PART_LENGTH lid1,
                          DICOM_ID_PART_LENGTH lid2) {
  DCMTag findtag;
  findtag.setID(lid1,lid2);
  return getTagDFS(findtag.getID());
}

bool DCMTag::hasTag(DICOM_ID_PART_LENGTH lid1,
                    DICOM_ID_PART_LENGTH lid2) {
  DCMTag findtag;
  findtag.setID(lid1,lid2);
  return hasTag(findtag.getID());
}

bool DCMTag::hasTag(DICOM_ID_LENGTH lid) {
  // first look in immediate children
  unsigned count;
  count = getTagsMap().count(lid);
  // handle lookups into single item children
  if (count == 0 && PrivateData->Children.size() > 0 && (lid != ItemMarker)
      && (PrivateData->Children[0]->getID() == ItemMarker))
    count = PrivateData->Children[0]->getTagsMap().count(lid);
  return count;
}

bool DCMTag::hasTagDFS(DICOM_ID_LENGTH lid) {
  if (hasTag(lid)) return true;
  else {
    for ( unsigned pos=0; pos < PrivateData->Children.size(); pos++ )
      if ( PrivateData->Children[pos]->hasTagDFS(lid) )
        return true;
    return false;
  }
}

unsigned int DCMTag::hasTags(bool ignoredelimeters) const {
  unsigned retval = PrivateData->Children.size();;
  if (ignoredelimeters)
    if (PrivateData->Children.size() == 1)
      if ( (PrivateData->Children[0]->getID() == ItemDelim) ||
           (PrivateData->Children[0]->getID() == SequenceDelim) )
        retval = 0;
  return retval;
}

bool DCMTag::hasTagDFS(DICOM_ID_PART_LENGTH lid1, DICOM_ID_PART_LENGTH lid2) {
  DCMTag findtag;
  findtag.setID(lid1,lid2);
  return hasTagDFS(findtag.getID());
}

bool DCMTag::hasError() const {
  return PrivateData->Error;
}

/* VR Types to return as 64int
 * US (unsigned 16-bit)
 * UL (unsigned 32-bit)
 * SS (signed   16-bit)
 * SL (signed   32-bit)
 */
bool DCMTag::isInt() const {
  if ( (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='S') ||
       (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='L') ||
       (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='S') ||
       (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='L') )
    return true;
  else
    return false;
}

/* VR Types to return as 64-bit float
 * FL (single)
 * FD (double)
 * OF (32bt string)
 * DS (decimal string)
 */
bool DCMTag::isFloat() const {
  if ( (PrivateData->VR[0]=='F' && PrivateData->VR[1]=='L') ||
       (PrivateData->VR[0]=='F' && PrivateData->VR[1]=='D') )
    return true;
  else
    return false;
}


/* VR types that are strings:
 * AE
 * AS
 * CS
 * DA
 * DS
 * DT
 * IS
 * LO
 * LT
 * PN
 * SH
 * ST
 * TM
 * UI
 * UT
 * OF
 */
bool DCMTag::isString() const {
  if( (PrivateData->VR[0]=='A' && PrivateData->VR[1]=='E') ||
      (PrivateData->VR[0]=='A' && PrivateData->VR[1]=='S') ||
      (PrivateData->VR[0]=='C' && PrivateData->VR[1]=='S') ||
      (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='A') ||
      (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='S') ||
      (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='T') ||
      (PrivateData->VR[0]=='I' && PrivateData->VR[1]=='S') ||
      (PrivateData->VR[0]=='L' && PrivateData->VR[1]=='O') ||
      (PrivateData->VR[0]=='L' && PrivateData->VR[1]=='T') ||
      (PrivateData->VR[0]=='P' && PrivateData->VR[1]=='N') ||
      (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='H') ||
      (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='T') ||
      (PrivateData->VR[0]=='T' && PrivateData->VR[1]=='M') ||
      (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='I') ||
      (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='T') ||
      (PrivateData->VR[0]=='O' && PrivateData->VR[1]=='F') )
    return true;
  else
    return false;
}

// return true if the DICOM '\' separator is present
bool DCMTag::isArray() const {
  if ( isString() && (toString().find(DCMArraySep) != std::string::npos) )
    return true;
  else
    return false;
}

bool DCMTag::isPrivate() const {
  return (getID1() % 2);
}

bool DCMTag::shouldSave() const {
  return PrivateData->SaveMe;
}

void DCMTag::setShouldSave(bool dosave) {
  PrivateData->SaveMe = dosave;
}

std::string DCMTag::getElement(short unsigned elem, bool) const {
  std::string retval;
  SURI dcmarray;
  std::string separator;
  separator.push_back(DCMArraySep);
  dcmarray.setSeparator(separator);
  dcmarray.setURI(toString());
  if (elem < dcmarray.depth())
    retval = dcmarray.getComponent(elem);
  return retval;
}

SVector DCMTag::toVector() const {
  SURI    arrvals;
  SVector result;
  arrvals.setSeparator("\\");
  arrvals.setURI(toString());
  result.setDim(arrvals.depth());
  for (int i=0; i<(int)arrvals.depth(); i++) {
    std::stringstream conv;
    SVector::Precision targ;
    conv << arrvals.getComponent(i);
    conv >> targ;
    result[i] = targ;
  }
  return result;
}

void DCMTag::fromVector(const SVector& srcvect) {
  std::stringstream conv;
  for (unsigned i=0; i<srcvect.getDim();i++) {
    conv << srcvect[i];
    if (i < (srcvect.getDim()-1))
      conv << "\\";
  }
  fromString(conv.str());
}

DCMTag& DCMTag::getParent() {
  if (isTop())
    throw SimulacrumDCMTagsException();
  return *(PrivateData->Parent);
}

std::string DCMTag::getPath() {
  SURI path;
  DCMTag *tag = this;
  while ( ! tag->isTop() ) {
    path.addComponentFront(tag->getIDstr());
    tag = &tag->getParent();
  }
  return path.getURI();
}

bool DCMTag::pathExists(const std::string& lpath) {
  SURI luri(lpath);
  if (luri.depth() > 0) {
    DCMTag tmplookup;
    tmplookup.setID(luri.getComponent(0));
    if (hasTag(tmplookup.getID())) {
      luri.deleteComponent(0);
      return getTag(tmplookup.getID()).pathExists(luri.getURI());
    }
    else
      return false;
  }
  else
    return true;
}

DCMTag& DCMTag::getByPath(const std::string& lpath) {
  SURI luri(lpath);
  if (luri.depth() > 0) {
    DCMTag tmplookup;
    tmplookup.setID(luri.getComponent(0));
    luri.deleteComponent(0);
    return getTag(tmplookup.getID()).getByPath(luri.getURI());
  }
  else
    return *this;
}

bool DCMTag::isTop() const {
  return (PrivateData->Parent == nullptr);
}

bool DCMTag::dataPresent() const {
  return getData() != nullptr;
}

bool DCMTag::isNULL(bool onlyfirst) const {
  unsigned pos;
  if (PrivateData->Length > 0) {
  if ((PrivateData->Data != nullptr) && onlyfirst && PrivateData->Data[0] == 0x00)
    return true;
  for (pos=0;pos<PrivateData->Length;pos++)
    if (PrivateData->Data[pos] != 0x00)
      return false;
  }
  return true;
}

bool DCMTag::isMeta() const {
  return PrivateData->isMetaTag;
}

int64_t DCMTag::toInt() const {
  if (!dataPresent()) return 0;
  if(PrivateData->VR[0]=='U' && PrivateData->VR[1]=='S'){
    if (getDataLength() < sizeof(uint16_t)) return 0.0;
    uint16_t *dcm_short = (uint16_t*)PrivateData->Data;
    return (int64_t)*dcm_short;
  }
  else
    if(PrivateData->VR[0]=='U' && PrivateData->VR[1]=='L'){
      if (getDataLength() < sizeof(uint32_t)) return 0.0;
      uint32_t *dcm_long = (uint32_t*)PrivateData->Data;
      return (int64_t)*dcm_long;
    }
    else
      if(PrivateData->VR[0]=='S' && PrivateData->VR[1]=='S'){
        if (getDataLength() < sizeof(int16_t)) return 0.0;
        int16_t *dcm_short = (int16_t*)PrivateData->Data;
        return (int64_t)*dcm_short;
      }
      else
        if(PrivateData->VR[0]=='S' && PrivateData->VR[1]=='L'){
          if (getDataLength() < sizeof(int32_t)) return 0.0;
          int32_t *dcm_long = (int32_t*)PrivateData->Data;
          return (int64_t)*dcm_long;
        }
        else
          /* convert strings that represent numbers */
          if( (PrivateData->VR[0]=='I' && PrivateData->VR[1]=='S') ||
              (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='S') ){
            int dcm_long = 0;
            std::stringstream(std::string(getData(),getDataLength())) >> dcm_long;
            return (int64_t)dcm_long;
          }
          else return 0;
}

int64_t DCMTag::toInt(unsigned elem) const {
  if (!isArray()) return toInt();
  else {
    std::stringstream outstream;
    int64_t           retval;
    outstream << getElement(elem);
    outstream >> retval;
    return retval;
  }
}

/* VR Types to return as 64-bit float
 * FL (single)
 * FD (double)
 * OF (32bt string)
 * DS (decimal string)
 */
double DCMTag::toFloat() const {
  if (!dataPresent()) return 0.0;
  if(PrivateData->VR[0]=='F' && PrivateData->VR[1]=='L'){
    if (getDataLength() < sizeof(float)) return 0.0;
    float *shortfloat = (float*)PrivateData->Data;
    return (double)*shortfloat;
  }
  else if (PrivateData->VR[0]=='F' && PrivateData->VR[1]=='D') {
    if (getDataLength() < sizeof(double)) return 0.0;
    return (double)*PrivateData->Data;
  }
  else if ( (PrivateData->VR[0]=='O' && PrivateData->VR[1]=='F') ||
            (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='S') ) {
    double res;
    std::stringstream(std::string(getData(),getDataLength())) >> res;
    return res;
  }
  else
    return 0.0;
}

void DCMTag::toString(std::string &target) const {
  if (!dataPresent()) return;
  std::stringstream output_convert;
  if((PrivateData->VR[0]=='A' && PrivateData->VR[1]=='E') ||
     (PrivateData->VR[0]=='A' && PrivateData->VR[1]=='S') ||
     (PrivateData->VR[0]=='C' && PrivateData->VR[1]=='S') ||
     (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='A') ||
     (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='S') ||
     (PrivateData->VR[0]=='D' && PrivateData->VR[1]=='T') ||
     (PrivateData->VR[0]=='I' && PrivateData->VR[1]=='S') ||
     (PrivateData->VR[0]=='L' && PrivateData->VR[1]=='O') ||
     (PrivateData->VR[0]=='L' && PrivateData->VR[1]=='T') ||
     (PrivateData->VR[0]=='P' && PrivateData->VR[1]=='N') ||
     (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='H') ||
     (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='T') ||
     (PrivateData->VR[0]=='T' && PrivateData->VR[1]=='M') ||
     (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='I') ||
     (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='T') ||
     (PrivateData->VR[0]=='O' && PrivateData->VR[1]=='F')
  ) {
      if (isNULL(true))
        target.assign("");
      else
        target.assign(PrivateData->Data,getDataLengthUnpadded());
  }
  else
     if((PrivateData->VR[0]=='U' && PrivateData->VR[1]=='S') ||
        (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='L') ||
        (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='S') ||
        (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='L')
     ) {
         output_convert << toInt();
         target.assign(output_convert.str());
     }
   else
     if((PrivateData->VR[0]=='F' && PrivateData->VR[1]=='L') ||
        (PrivateData->VR[0]=='F' && PrivateData->VR[1]=='D') ) {
       output_convert << toFloat();
       target.assign(output_convert.str());
     }
     else
       target.assign("|...|");
}

std::string DCMTag::toString() const {
  std::string newstring;
  toString(newstring);
  return newstring;
}


void DCMTag::toStringSafe(std::string &target) const {
  toString(target);
  /* Now filter out DICOM padded crazy non-printing characters:
   * - Only allow ASCI characters between:
   *   0x20 (space) and 0x7e (~)
   */
  for (unsigned pos = 0; pos < target.length(); pos++)
    if ((target[pos] < 0x20)||(target[pos] > 0x7e))
      target[pos] = ' ';
}

template <class DTYPE>
char* char_alloc_integral_dtype(DTYPE newval) {
  DTYPE *newdata = (DTYPE*) new char[sizeof(DTYPE)];
  *newdata = newval;;
  return (char*) newdata;
}

/* VR Types to return as 64int
 * US (unsigned 16-bit)
 * UL (unsigned 32-bit)
 * SS (signed   16-bit)
 * SL (signed   32-bit)
 */
void DCMTag::fromInt(int64_t newint) {
  clearData();
  if(PrivateData->VR[0]=='U' && PrivateData->VR[1]=='S') {
    setData(sizeof(uint16_t),char_alloc_integral_dtype<uint16_t>(newint));
  }
  else if (PrivateData->VR[0]=='U' && PrivateData->VR[1]=='L') {
    setData(sizeof(uint32_t),char_alloc_integral_dtype<uint32_t>(newint));
  }
  else if (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='S') {
    setData(sizeof(int16_t),char_alloc_integral_dtype<int16_t>(newint));
  }
  else if (PrivateData->VR[0]=='S' && PrivateData->VR[1]=='L') {
    setData(sizeof(int32_t),char_alloc_integral_dtype<int32_t>(newint));
  }
  else if (isString()) {
    std::stringstream conv;
    conv << newint;
    fromString(conv.str());
  }
  else throw SimulacrumDCMTagsException();
}

void DCMTag::fromFloat(double newfloat) {
  clearData();
  if(PrivateData->VR[0]=='F' && PrivateData->VR[1]=='L') {
    setData(sizeof(float),char_alloc_integral_dtype<float>(newfloat));
  }
  else if (PrivateData->VR[0]=='F' && PrivateData->VR[1]=='D') {
    setData(sizeof(double),char_alloc_integral_dtype<double>(newfloat));
  }
  else if (isString()) {
    std::stringstream tostr;
    tostr << newfloat;
    fromString(tostr.str());
  }
  else throw SimulacrumDCMTagsException();
}

void DCMTag::fromString(const std::string& newstring) {
  if (isInt()) {
    std::stringstream convert(newstring);
    int64_t intval;
    convert >> intval;
    fromInt(intval);
  }
  else if (isFloat()) {
    std::stringstream convert(newstring);
    double floatval;
    convert >> floatval;
    fromFloat(floatval);
  }
  else {
    clearData();
    char *newdata = new char[newstring.length()];
    strncpy(newdata,newstring.c_str(),newstring.length());
    setData(newstring.length(),newdata);
  }
}

void DCMTag::clear (){
  PrivateData->ID1       = 0;
  PrivateData->ID2       = 0;
  PrivateData->VR[0]     = 0;
  PrivateData->VR[1]     = 0;
  PrivateData->VR[2]     = '\0';
  if ( PrivateData->ownsName && (PrivateData->Name != nullptr) )
    delete PrivateData->Name;
  PrivateData->Name      = nullptr;
  PrivateData->ownsName  = false;
  clearData();
  PrivateData->Length    = 0;
  clearTags();
  PrivateData->isMetaTag = false;
  PrivateData->Error     = false;
  setShouldSave(true);
}

void DCMTag::clearData() {
  if (PrivateData->Data) delete[] PrivateData->Data;
    PrivateData->Data   = nullptr;
}

void DCMTag::clearTags() {
  if (PrivateData->isParent) {
    tagset_t::iterator it;
    for ( it=PrivateData->Children.begin() ; it < 
          PrivateData->Children.end(); it++ )
      delete *it;
  }
  PrivateData->Children.clear();
  PrivateData->ChildrenMap.clear();
  setIsParent(true);
}


void DCMTag::setID(DICOM_ID_LENGTH lID) {
  DICOM_ID_PART_LENGTH lid1 = (lID >> (sizeof(lid1)*8));
  DICOM_ID_PART_LENGTH lid2 = ((lID << (sizeof(lid1)*8)) >> (sizeof(lid1)*8));
  setID(lid1,lid2);
}

void DCMTag::setID(const std::string& newid) {
  SURI tags;
  tags.setSeparator(",");
  tags.setURI(newid);
  setID(0xFFFF,0xFFFF);
  if (tags.depth() == 2) {
    std::stringstream lid1s;
    std::stringstream lid2s;
    lid1s << tags.getComponent(0);
    lid2s << tags.getComponent(1);
    DICOM_ID_PART_LENGTH lid1;
    DICOM_ID_PART_LENGTH lid2;
    lid1s >> std::hex >> lid1;
    lid2s >> std::hex >> lid2;
    setID(lid1,lid2);
  }
}

void DCMTag::setID (DICOM_ID_PART_LENGTH lID1, DICOM_ID_PART_LENGTH lID2){
  // remove from the parent
  bool wasinparent = false;
  if ( (! isTop()) && getParent().PrivateData->ChildrenMap.count(getID()) ) {
    getParent().PrivateData->ChildrenMap.erase
                           (getParent().PrivateData->ChildrenMap.find(getID()));
    wasinparent = true;
  }
  PrivateData->ID1 = lID1;
  PrivateData->ID2 = lID2;
  // reset the pointer in the parent map
  if (wasinparent) {
    getParent().PrivateData->ChildrenMap.insert(
                              std::pair<DICOM_ID_LENGTH,DCMTag*>(getID(),this));
  }
}

void DCMTag::setVR  (char VR1, char VR2){
  PrivateData->VR[0] = VR1;
  PrivateData->VR[1] = VR2;
}

void DCMTag::setName(const char* lName, bool ownsname){
  PrivateData->ownsName = ownsname;
  PrivateData->Name = lName;
}

void DCMTag::setDataLength(DICOM_LONG_LENGTH LDataLength) {
  PrivateData->Length = LDataLength;
}


void DCMTag::setData(DICOM_LONG_LENGTH LDataLength, char* lData){
  PrivateData->Data = lData;
  PrivateData->Length = LDataLength;
}

void DCMTag::setParent(DCMTag* newp) {
  PrivateData->Parent = newp;
}

void DCMTag::setError(bool errstat) {
  PrivateData->Error = errstat;
}

void DCMTag::setIsParent(bool isTheParent){
  PrivateData->isParent = isTheParent;
}

void DCMTag::setMeta(bool metatag) {
  PrivateData->isMetaTag = metatag;
}

bool DCMTag::matches(const std::string &searchstr, bool recurse) {
  std::string searcher(getName());
  if ( searcher.find(searchstr)   != std::string::npos)
    return true;
  toStringSafe(searcher);
  if ( searcher.find(searchstr)   != std::string::npos)
    return true;
  if ( getIDstr().find(searchstr) != std::string::npos)
    return true;
  if (recurse) {
    for (unsigned i = 0; i < PrivateData->Children.size(); i++)
      if (PrivateData->Children[i]->matches(searchstr, true))
        return true;
  }
  return false;
}

void DCMTag::addTag(DCMTag* newtag, bool check) {
  if(check && hasTag(newtag->getID1(),newtag->getID2()))
    throw SimulacrumDCMTagsException();
  newtag->setParent(this);
  PrivateData->Children.push_back(newtag);
  PrivateData->ChildrenMap.insert
              (std::pair<DICOM_LONG_LENGTH,DCMTag*>(newtag->getID(),newtag));
}

DCMTag& DCMTag::addTag(DCMTag& copytag, bool check) {
  DCMTag *newtag = new DCMTag();
  *newtag = copytag;
  addTag(newtag,check);
  return *newtag;
}

DCMTag* DCMTag::addItem(DCMTag* newtag) {
  if ( std::string(getVR()) != "SQ" )
    throw SimulacrumDCMTagsException();
  DCMTag * newitem = genItemTag();
  newitem->addTag(newtag);
  addTag(newitem,false);
  return newitem;
}

DCMTag& DCMTag::addItem(DCMTag& copytag) {
  DCMTag *newtag = new DCMTag();
  *newtag = copytag;
  return *addItem(newtag);
}

bool DCMTag::removeNode() {
  if (!isTop())
    return getParent().removeTag(this, true);
  else {
    delete this;
    return false;
  }
}

void DCMTag::detachNode() {
  if (!isTop())
    getParent().removeTag(this, false);
}

bool DCMTag::removeTag(const DCMTag* tagtogo, bool dodelete) {
  bool removed = false;
  for (tagset_t::iterator it=getTags().begin() ;
        it < getTags().end(); it++ ) {
    if ( *it == tagtogo ) {
      getTags().erase(it);
      tagmap_t::iterator maprem =
                         getTagsMap().find(tagtogo->getID());
      if ( maprem != getTagsMap().end() &&
           maprem->second == tagtogo )
        getTagsMap().erase(tagtogo->getID());
      if (dodelete)
        delete tagtogo;
      removed = true;
      break;
    }
  }
  return removed;
}

bool DCMTag::removeTag(DICOM_ID_LENGTH dcmid, bool dodelete) {
  bool removed = false;
  for ( tagset_t::iterator it=getTags().begin() ;
        it < getTags().end(); it++ )
    if ( (*it)->getID() == dcmid ){
      if (dodelete)
        delete *it;
      getTags().erase(it);
      removed = true;
      break;
    }
    else
      return false;
  getTagsMap().erase(dcmid);
  return removed;
}

bool DCMTag::removeTag(DICOM_ID_PART_LENGTH lid1, DICOM_ID_PART_LENGTH lid2,
                       bool dodelete) {
  DCMTag tmptag;
  tmptag.setID(lid1,lid2);
  return removeTag(tmptag.getID(), dodelete);
}

/*-------- A class to store DICOM tags, including conversion methods ---------*/

DCMTag* DCMTag::genItemTag() {
  DCMTag *newItem = new DCMTag();
  newItem->setID(0xfffee000);
  newItem->setVR('<','>');
  return newItem;
}

DCMTag* DCMTag::genSequenceTag(DICOM_ID_LENGTH newid)
{
  DCMTag *newSequence = new DCMTag();
  newSequence->setID(newid);
  newSequence->setVR('S','Q');
  return newSequence;
}

DCMTag* DCMTag::genIntTag(DICOM_ID_LENGTH newid, int64_t newint)
{
  DCMTag *newInt = new DCMTag();
  newInt->setID(newid);
  newInt->setVR('S','L');
  newInt->fromInt(newint);
  return newInt;
}

DCMTag* DCMTag::genFloatTag(DICOM_ID_LENGTH newid, double newfloat)
{
  DCMTag *newFloat = new DCMTag();
  newFloat->setID(newid);
  newFloat->setVR('F','D');
  newFloat->fromFloat(newfloat);
  return newFloat;
}

DCMTag* DCMTag::genStringTag(DICOM_ID_LENGTH newid, const std::string &newstr)
{
  DCMTag *newString = new DCMTag();
  newString->setID(newid);
  newString->setVR('L','O');
  newString->fromString(newstr);
  return newString;
}

/*-------------------- For AbsTreenode compliance ----------------------------*/

std::string DCMTag::NodeID() {
  return getIDstr();
}

void DCMTag::NodeID(const std::string& newid) {
  setID(newid);
}


std::string DCMTag::NodeType() {
  return getVR();
}

void DCMTag::NodeType(const std::string& newvr) {
  if (newvr.length() == 2) {
    setVR(newvr[0],newvr[1]);
  }
}

unsigned long DCMTag::NodeSize() {
  return getDataLength();
}

std::string DCMTag::NodeName() {
  std::string nodename(getName());
  if (nodename.length() == 0)
    nodename = NodeID();
  return nodename;
}

std::string DCMTag::NodeValue() {
  return toString();
}

void DCMTag::NodeValue(const std::string& newvalue) {
  try {
    fromString(newvalue);
  }
  catch(std::exception &e) {
    // do nothing
    DEBUG_OUT("Exception setting node value from string:");
    DEBUG_OUT(e.what());
  }
}

std::string DCMTag::NodeData() {
  if (dataPresent()) {
   return std::string(getData(),getDataLength());
  }
  else {
    SLogger::global().addMessage
                       ("DCMTag::NodeData: requesting non-present data from " +
                        NodeName(), SLogLevels::HIGH);
    return "";
  }
}

void DCMTag::NodeData(const std::string& src) {
  if (src.size() > 0) {
    char *newdata = new char[src.size()];
    memcpy(newdata,&(src[0]),src.size());
    setData(src.size(),newdata);
  }
  else {
    clearData();
  }
}

unsigned long DCMTag::NodeChildrenNum(bool) {
  return PrivateData->Children.size();
}

SAbsTreeNodeList_t DCMTag::NodeChildren(bool,bool) {
  std::vector<SAbsTreeNode*> retvec;
  for (tagset_t::iterator tit = PrivateData->Children.begin();
       tit != PrivateData->Children.end(); tit++)
    retvec.push_back(*tit);
  return retvec;
}

bool DCMTag::NodePathExists(const std::string& path) {
  return pathExists(path);
}

SAbsTreeNode& DCMTag::NodeByPath(const std::string& path) {
  return getByPath(path);
}

SAbsTreeNode& DCMTag::NodeParent() {
  return getParent();
}

bool DCMTag::hasNodeParent() {
  return !isTop();
}

bool DCMTag::NodeError() {
  return hasError();
}

SAbsTreeNode& DCMTag::NewChild() {
  DCMTag *newtag = genStringTag(DefaultString,"Empty String");
  addTag(newtag,false);
  return *newtag;
}

SimulacrumLibrary::str_enc DCMTag::stringEncoding() {
  SimulacrumLibrary::str_enc result = SAbsTreeNode::stringEncoding();
  auto tagenc = DCMTag(0x0008,0x0005).getID();
  if (getID() == tagenc) {
    // this tag contains encoding information
    auto dcmenc = toString();
    if (dcmenc == "ISO_IR 100") {
      result = SimulacrumLibrary::str_enc::Latin1;
    }
    else {
      result = SimulacrumLibrary::str_enc::Raw;
    }
  }
  else {
    // attempt to traverse structure of tags to find the associated encoding
    auto root = dynamic_cast<DCMTag*>(&(NodeRoot()));
    if (root && root->hasTag(tagenc)) {
      return root->getTag(tagenc).stringEncoding();
    }
  }
  return result;
}
