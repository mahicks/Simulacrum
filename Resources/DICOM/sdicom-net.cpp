/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Implementation for DICOM Networking
 * Author: M.A.Hicks
 */

#include "sdicom-net.h"
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Core/slockable.h>
#include <Core/SPool/SPool.h>
#include "sarch.h"

#include <string.h>
#include <sstream>
#include <stdio.h>

#include <boost/asio.hpp>
using boost::asio::ip::tcp;

using namespace Simulacrum;

ssocket_t_impl // implementation for ssocket

/*------------------------------------------------------------------------------
 *                        SDICOM-Net Data Structures
 *----------------------------------------------------------------------------*/

static const uint16_t    PROTOCOL_VERSION = 0x1;
static const std::string AppContextStr    = "1.2.840.10008.3.1.1.1";
static const uint8_t     PDVPREFIXSIZE    = 12;
static const uint32_t    MAXBLOCKSIZE     = (1024*1024);
static const uint16_t    DEFAULTPRIORITY  = 0x0001;
static const uint32_t    DICOM_MAX_LEN    = 0x0;
typedef uint32_t         DICOM_NET_SIZE;

static void constructAssociationRequest(const std::string& localaetitle,
                                        const std::string& remoteaetitle,
                                        SDICOM& exemplar,
                                        Associate_PDU& targetpdu);
static void constructAssociationAccept(Associate_PDU &incoming,
                                       Associate_PDU &target );

static std::string charVectorToString (const std::vector<char>& source) {
  std::string retval;
  for (unsigned i=0; i<source.size(); i++)
    retval.push_back(source[i]);
  return retval;
}

static std::vector<char> stringToCharVector (const std::string& source) {
  std::vector<char> retval;
  for (unsigned i=0; i<source.size(); i++)
    retval.push_back(source[i]);
  return retval;
}

template < class srctype >
static void SDICOM_PDU_ENDIAN_CORR(std::vector< srctype > &fixme) {
  if ( ! sysInfo::isBigEndian )
    std::reverse(fixme.begin(),fixme.end());
}

SDICOM_PDU_Serialisable::~SDICOM_PDU_Serialisable() {

}

SDICOM_Generic_PDU::SDICOM_Generic_PDU() {
  Type = 0;
}

std::string SDICOM_Generic_PDU::toString() const {
  std::stringstream  retval;
  retval << "PDU Type: " << (unsigned)Type << " PDU Size: " << Data.size();
  return retval.str();
}

long int SDICOM_Generic_PDU::deSerialise(const std::vector< char >& source) {
  long int res = -1;
  const unsigned short prefixsize = 6;
  if (source.size() >= prefixsize) {
    std::vector<char> sizeval(4,0);
    Type = source[0];
    memcpy(&sizeval[0],&source[2],4);
    SDICOM_PDU_ENDIAN_CORR<char>(sizeval);
    long unsigned sizetoread = *((DICOM_PDU_LENGTH*)&sizeval[0]);
    if ((source.size() - prefixsize) >= sizetoread) {
      Data.resize(sizetoread);
      memcpy(&Data[0],&source[prefixsize],sizetoread);
      res = prefixsize+sizetoread;
    }
  }
  return res;
}

long int SDICOM_Generic_PDU::serialise(std::vector< char >& target,
                                       ACTION_ENUM ) {
  long int res = -1;
  DICOM_PDU_LENGTH sizetowrite = Data.size();
  std::vector<char> sizeval(sizeof(sizetowrite),0);
  target.resize(2);
  // write type
  target[0] = Type;
  // write size
  memcpy(&sizeval[0],&sizetowrite,sizeof(sizetowrite));
  SDICOM_PDU_ENDIAN_CORR<char>(sizeval);
  target.insert(target.end(),sizeval.begin(),sizeval.end());
  // write data
  target.insert(target.end(),Data.begin(),Data.end());
  res = target.size();
  return res;
}

//------------------------------------------------------------------------------

long int SDICOM_SHORT_PDU::deSerialise(const std::vector< char >& source) {
  return deSerialise(source,0);
}

long int SDICOM_SHORT_PDU::deSerialise(const std::vector< char >& source,
                                       unsigned start) {
  long int result = -1;
  const static unsigned short basesize = 4;
  if ( (source.size()-start) >= basesize) {
    std::vector<char> pdulen(2,0);
    Type = source[start];
    // skip one for reserved byte
    pdulen[0] = source[start+2];
    pdulen[1] = source[start+3];
    SDICOM_PDU_ENDIAN_CORR<char>(pdulen);
    uint16_t *sizeval = (uint16_t*)&pdulen[0];
    if ((unsigned)(*sizeval - basesize) <= (source.size() - start)) {
      Data.resize(*sizeval);
      memcpy(&Data[0],&source[start+basesize],*sizeval);
      result = *sizeval + basesize;
    }
  }
  return result;
}

long int SDICOM_SHORT_PDU::serialise(std::vector< char >& target, ACTION_ENUM) {
  long int result = -1;
  const static unsigned short basesize = 4;
  uint16_t sizeval = (uint16_t)Data.size();
  // extract size chars
  std::vector<char> sizedat(2,0);
  memcpy(&sizedat[0],(char*)&sizeval,2);
  SDICOM_PDU_ENDIAN_CORR<char>(sizedat);
  // start configuring PDU output
  long int beginpos = target.size();
  // extend the target by the necessary size
  target.resize(beginpos+basesize+Data.size());
  // work from end
  char *targp = &target[beginpos];
  targp[0] = Type;
  targp[1] = '\0';
  targp[2] = sizedat[0];
  targp[3] = sizedat[1];
  // copy in the data
  memcpy(&targp[basesize],&Data[0],sizeval);
  result = basesize + sizeval;
  return result;
}

//------------------------------------------------------------------------------

long int Associate_PDU::deSerialise(const std::vector< char >& source) {
  /**** PROTOCOL Version: dictates the entire structure of things are
   **** parsed here. If the protocol does not match, everything could
   ***  need checking here in the code. */
  const uint16_t PROTOCOL_VERSION = 0x01;
  long int result = 0;
  const unsigned basesize = 74;
  if (source.size() >= basesize) {
    const char *startp = &source[0];
    std::vector<char> pdulen(4,0);
    std::vector<char> version(2,0);
    uint32_t *sizeval = (uint32_t*)&pdulen[0];
    uint16_t *versval = (uint16_t*)&version[0];
    // read type
    if (startp[0] != ASSOCR_PDU && startp[0] != ASSOCA_PDU)
      // wrong PDU type!
      return -1;
    // skip reserved bytes
    startp += 2;
    // read size
    for (unsigned i=0;i<pdulen.size();i++)
      pdulen[i] = startp[i];
    SDICOM_PDU_ENDIAN_CORR<char>(pdulen);
    startp += 4;
    // read protocol version
    version[0] = startp[0];
    version[1] = startp[1];
    SDICOM_PDU_ENDIAN_CORR<char>(version);
    ProtocolVersion = *versval;
    if (ProtocolVersion != PROTOCOL_VERSION) {
      SLogger::global().addMessage
     ("Associate_PDU: SERIOUS WARNING: Association protocol version mismatch!");
    }
    startp += 2;
    // skip reserved
    startp += 2;
    // read called AE
    CalledAETitle.resize(AETLENGTH);
    memcpy(&CalledAETitle[0],startp,AETLENGTH);
    startp += AETLENGTH;
    // read calling AE
    CallingAETitle.resize(AETLENGTH);
    memcpy(&CallingAETitle[0],startp,AETLENGTH);
    startp += AETLENGTH;
    // skip final reserved block
    startp += 32;
    // clear maxlength
    PresContexts.clear();
    // check that there is enough data left to read the PDU size
    if ( source.size() >= (*sizeval - 6)) {
      // the contents must just be set of SHORT_PDUs (for further deSerialising)
      while (startp < &source[source.size()-1]) {
        SDICOM_SHORT_PDU  newpdu;
        PresContextItem   newpresitem;
        UserInfoItem      newusernfoitem;
        std::vector<char> subdecode;
        long int readsize = newpdu.deSerialise(source,startp - &source[0]);
        if (readsize < 0) {
          result = -2;
          break;
        }
        // perform the appropriate action with the child pdu
        switch (newpdu.Type) {
          case USRNFO_PDU:
            newpdu.serialise(subdecode);
            newusernfoitem.deSerialise(subdecode);
            UserInfo = newusernfoitem;
            break;
          case PRSCTX_PDU:
          case PRSITM_PDU:
            newpdu.serialise(subdecode);
            newpresitem.deSerialise(subdecode);
            PresContexts.insert(std::pair<DICOM_PDU_TYPE,PresContextItem>
                                (newpresitem.PresContexID,newpresitem));
            break;
          case APPCTX_PDU:
            AppContext = newpdu;
            break;
          default:
            SLogger::global().addMessage("Associate_PDU: unrecognized sub_PDU");
            break;
        }
        // increment the read pointer by the size read
        startp += readsize;
      }
    }
    if (result == 0)
      result = 6 + *sizeval;
  }
  else
    result = -1;
  return result;
}

long int Associate_PDU::serialise(std::vector< char >& target,
                                  ACTION_ENUM act) {
  long int res = -1;
  const unsigned basesize   = 68;
  SDICOM_Generic_PDU thisout;
  thisout.Data.resize(basesize);
  std::vector<char> dataout;
  // construct full pdu from data elements
  switch (act) {
    case DCM_ACCEPT:
      thisout.Type = ASSOCA_PDU;
      break;
    case DCM_REQUEST:
      thisout.Type = ASSOCR_PDU;
      break;
    default:
      return -2;
      break;
  }
  // internal attributes
  // protocol version
  std::vector<char> protver(sizeof(ProtocolVersion),0);
  memcpy(&protver[0],&ProtocolVersion,sizeof(ProtocolVersion));
  SDICOM_PDU_ENDIAN_CORR<char>(protver);
  thisout.Data[0] = protver[0];
  thisout.Data[1] = protver[1];
  // called entity
  CalledAETitle.resize(AETLENGTH,' ');
  memcpy(&thisout.Data[4],&CalledAETitle[0],AETLENGTH);
  // calling entity
  CallingAETitle.resize(AETLENGTH,' ');
  memcpy(&thisout.Data[20],&CallingAETitle[0],AETLENGTH);
  // AppContext
  AppContext.Type = APPCTX_PDU;
  AppContext.serialise(dataout);
  thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  // PresContex
  for (std::map<DICOM_PDU_TYPE,PresContextItem>::iterator it = 
       PresContexts.begin(); it != PresContexts.end(); it ++ ) {
    dataout.clear();
    (*it).second.serialise(dataout,act);
    thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  }
  // UserInfo
  dataout.clear();
  UserInfo.serialise(dataout);
  thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  // write out the final data
  res = thisout.serialise(target);
  return res;
}

std::string Associate_PDU::toString() const {
  std::stringstream res;
  res << "ProtocolVersion: " << ProtocolVersion     << std::endl
      << "CalledAETitle  : " << CalledAETitle       << std::endl
      << "CallingAETitle : " << CallingAETitle      << std::endl
      << "AppContext     : ";
  for (unsigned i=0; i<AppContext.Data.size(); i++)
    res << AppContext.Data[i];
  res << std::endl
      << "PresContexts   : " << PresContexts.size() << std::endl
      << "UserItems      : " << UserInfo.UserInfoItems.size() << std::endl
      << "UserMaxLength  : " << UserInfo.MaxLength;
  return res.str();
}

//------------------------------------------------------------------------------

long int PresContextItem::deSerialise(const std::vector< char >& source) {
  long int result   = 0;
  unsigned basesize = 8;
  TransferSyntaxs.clear();
  AbstractSyntax.Data.clear();
  if (source.size() >= basesize) {
    const char *startp = &source[0];
    std::vector<char> pdulen(2,0);
    uint16_t *sizeval = (uint16_t*)&pdulen[0];
    if (startp[0] != PRSCTX_PDU && startp[0] != PRSITM_PDU)
      return -1;
    // skip type and reserved bytes
    startp += 2;
    // read size
    pdulen[0] = startp[0];
    pdulen[1] = startp[1];
    SDICOM_PDU_ENDIAN_CORR<char>(pdulen);
    startp += 2;
    // presid
    PresContexID = startp[0];
    // skip reserved and pres
    startp +=2 ;
    // read result/reason
    ResultReason = startp[0];
    // skip reserved and result
    startp += 2;
    // check that there is enough data left to read the PDU size
    if ( source.size() >= (unsigned)(*sizeval - 4)) {
      // the contents must just be set of SHORT_PDUs (for further deSerialising)
      while (startp < &source[source.size()-1]) {
        SDICOM_SHORT_PDU newpdu;
        long int readsize = newpdu.deSerialise(source,startp - &source[0]);
        if (readsize < 0) {
          result = -2;
          break;
        }
        else
          startp += readsize;
        // perform the appropriate action with the child pdu
        switch (newpdu.Type) {
          case ABSCTX_PDU:
            AbstractSyntax = newpdu;
            break;
          case TRXCTX_PDU:
            TransferSyntaxs.push_back(newpdu);
            break;
          default:
            SLogger::global().addMessage
                                      ("PresContextItem: unrecognized sub_PDU");
            break;
        }
      }
    }
    if (result == 0)
      result = 4 + *sizeval;
  }
  else
    result = -1;
  return result;
}

long int PresContextItem::serialise(std::vector< char >& target,
                                    ACTION_ENUM act) {
  SDICOM_SHORT_PDU thisout;
  std::vector<char> dataout;
  // set type based on desired output
  switch (act) {
    case DCM_ACCEPT:
      thisout.Type = PRSITM_PDU;
      break;
    case DCM_REQUEST:
      thisout.Type = PRSCTX_PDU;
      break;
    default:
      return -2;
      break;
  }
  // set this PDU's fields
  thisout.Data.resize(4);
  thisout.Data[0] = PresContexID;
  thisout.Data[3] = ResultReason;
  // only output abssyntax for request
  if (act == DCM_REQUEST) {
    AbstractSyntax.Type = ABSCTX_PDU;
    AbstractSyntax.serialise(dataout);
    thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  }
  // add each transfer syntax item
  for (unsigned i=0; i<TransferSyntaxs.size();i++) {
    dataout.clear();
    TransferSyntaxs[i].Type = TRXCTX_PDU;
    TransferSyntaxs[i].serialise(dataout);
    thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  }
  return thisout.serialise(target);
}

//------------------------------------------------------------------------------

long int UserInfoItem::deSerialise(const std::vector< char >& source) {
  long int res = 0;
  MaxLength = 0;
  if (source[0] != USRNFO_PDU) {
    res = -2;
  }
  else {
    long int res = 4;
    while ((unsigned) res < source.size()) {
      SDICOM_SHORT_PDU subpdu;
      std::vector<char> maxlen(subpdu.Data.size(),0);
      long int subres = subpdu.deSerialise(source,res);
      if (subres > 0) {
        switch (subpdu.Type) {
          case MAXLEN_PDU:
            SDICOM_PDU_ENDIAN_CORR<char>(subpdu.Data);
            MaxLength = *((uint32_t*)&subpdu.Data[0]);
            break;
          default:
            UserInfoItems.push_back(subpdu);
            break;
        }
        res += subres;
      }
      else
        break;
    }
  }
  return res;
}

long int UserInfoItem::serialise(std::vector< char >& target, ACTION_ENUM) {
  SDICOM_SHORT_PDU thisout;
  SDICOM_SHORT_PDU maxlen, impuid, impvname;
  std::vector<char> dataout;
  thisout.Type = USRNFO_PDU;
  // generate the special maxlen element
  maxlen.Type = MAXLEN_PDU;
  maxlen.Data.resize(sizeof(MaxLength),0);
  memcpy(&maxlen.Data[0],(char*)&MaxLength,sizeof(MaxLength));
  SDICOM_PDU_ENDIAN_CORR<char>(maxlen.Data);
  maxlen.serialise(dataout);
  // generate implementation UID item
  impuid.Type = IMPUID_PDU;
  impuid.Data.resize(strlen(SDICOM::implementationUID().c_str()),0);
  memcpy(&impuid.Data[0],SDICOM::implementationUID().c_str(),
         strlen(SDICOM::implementationUID().c_str()));
  impuid.serialise(dataout);
  // generate implementation version name item
  impvname.Type = IMPNAM_PDU;
  impvname.Data.resize(strlen(SDICOM::implementationName().c_str()),0);
  memcpy(&impvname.Data[0],SDICOM::implementationName().c_str(),
         strlen(SDICOM::implementationName().c_str()));
  impvname.serialise(dataout);
  // insert output data
  thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  // write out each generic userinfo item
  for (unsigned i=0;i<UserInfoItems.size();i++) {
    UserInfoItems[i].serialise(dataout);
    thisout.Data.insert(thisout.Data.end(),dataout.begin(),dataout.end());
  }
  return thisout.serialise(target);
}

//------------------------------------------------------------------------------

long int AssociateRJ_PDU::deSerialise(const std::vector< char >& source) {
  long int res = -1;
  if (source.size() >= 10) {
    Type   = source[0];
    Result = source[7];
    Source = source[8];
    Reason = source[9];
    res = 10;
  }
  return res;
}

long int AssociateRJ_PDU::serialise(std::vector< char >&target, ACTION_ENUM) {
  SDICOM_Generic_PDU res;
  res.Data.resize(4);
  res.Type = Type;
  res.Data[1] = Result;
  res.Data[2] = Source;
  res.Data[3] = Reason;
  res.serialise(target);
  return target.size();
}

//------------------------------------------------------------------------------

Data_PDU::Data_PDU() {
  Type = DCMDAT_PDU;
}

void Data_PDU::addPDV(DICOM_PDU_TYPE presid, std::vector< char >& sdata,
                      bool iscmd, bool islast) {
  DICOM_PDU_LENGTH  dlength = sdata.size() + 2; // + 2 for presid & msginfo
  DICOM_PDU_TYPE    msginfo = 0;
  std::vector<char> dlengthen(sizeof(dlength),0);
  memcpy(&dlengthen[0],(char*)&dlength,dlengthen.size());
  SDICOM_PDU_ENDIAN_CORR<char>(dlengthen);
  if (iscmd)
    msginfo = msginfo | 0x01;
  if (islast)
    msginfo = msginfo | 0x02;
  Data.insert(Data.end(),dlengthen.begin(),dlengthen.end());
  Data.push_back(presid);
  Data.push_back(msginfo);
  Data.insert(Data.end(),sdata.begin(),sdata.end());
}

Data_PDU::messagelist_t
    Data_PDU::getDICOMMessagesFromDataField(const std::vector< char >& source) {
  const char *pos = &source[0];
  messagelist_t result;
  while (pos <= &source[source.size()-5]) {
    DICOM_PDU_TYPE prescontext;
    std::vector<char> sizeval(sizeof(DICOM_PDU_LENGTH),0);
    DICOM_PDU_LENGTH *msgsize = (DICOM_PDU_LENGTH*)&sizeval[0];
    memcpy(&sizeval[0],pos,sizeof(DICOM_PDU_LENGTH));
    SDICOM_PDU_ENDIAN_CORR<char>(sizeval);
    *msgsize -= 1;
    pos += sizeof(DICOM_PDU_LENGTH);
    prescontext = pos[0];
    pos++;
    result.insert(std::pair<uint8_t,std::pair<const char*,std::size_t> >
    (prescontext,std::pair<const char*,std::size_t>(pos,*msgsize)));
    pos += *msgsize;
  }
  return result;
}

int16_t Data_PDU::accumulateDICOMMessages(Data_PDU::accumset_t&    targetset,
                                          Data_PDU::accumset_t&    commandset,
                                          Data_PDU::messagelist_t& sourcemsgs) {
  int16_t retval = -1;
  messagelist_t::iterator msgiterator;
  for (msgiterator=sourcemsgs.begin();
       msgiterator!=sourcemsgs.end();
       msgiterator++) {
    const char *msg     = msgiterator->second.first;
    std::size_t msgsize = msgiterator->second.second;
    std::string toadd;
    toadd.resize(msgsize-1,0);
    if (msg[0] & 0x02 && !(msg[0] & 0x01))
      retval = msgiterator->first;
    memcpy(&toadd[0],&msg[1],msgsize-1);
    // do not accumulate  command messages
    if (! (msg[0] & 0x01) ) {
      if (targetset.count(msgiterator->first))
        targetset[msgiterator->first].insert(targetset
                                                     [msgiterator->first].end(),
                                            toadd.begin(),
                                            toadd.end());
      else {
        targetset[msgiterator->first] = toadd;
      }
    }
    // handle a command message
    else {
      if (commandset.count(msgiterator->first))
        commandset[msgiterator->first].insert(commandset
                                                     [msgiterator->first].end(),
                                            toadd.begin(),
                                            toadd.end());
      else {
        commandset[msgiterator->first] = toadd;
      }
    }
  }
  return retval;
}

/*------------------------------------------------------------------------------
 *                               SDICOM Client
 *----------------------------------------------------------------------------*/

class SDICOM_Client::SDICOM_ClientPIMPL {
public:
  SClient       NetClient;
  std::string   TargetAddress, AETitle, TargetAETitle;
  short         Port;
  uint32_t      MaxPDataSize;
  Associate_PDU AcceptedPDU;
  SDICOM_ClientPIMPL(): MaxPDataSize(0) {}
};

SDICOM_Client::SDICOM_Client() : PrivateData(new SDICOM_ClientPIMPL()) {
  setAETitle("SimulacrumDICOM");
  setTargetAETitle("AnyOldDICOMSCP");
}

SDICOM_Client::~SDICOM_Client() {
  delete PrivateData;
}

void SDICOM_Client::setTargetAddress(const std::string& targa) {
  PrivateData->TargetAddress = targa;
}

void SDICOM_Client::setTargetPort(short int nport) {
  PrivateData->Port = nport;
}

void SDICOM_Client::setAETitle(const std::string& naet) {
  PrivateData->AETitle = naet;
  PrivateData->AETitle.resize(AETLENGTH,' ');
}

void SDICOM_Client::setTargetAETitle(const std::string& taet) {
  PrivateData->TargetAETitle = taet;
}

static std::string getRejectReason(SDICOM_Generic_PDU& src) {
  std::string res = "Unknown reason";
  if (src.Type == ASSOCJ_PDU) {
    if (src.Data.size() > 8) {
      uint8_t reason = src.Data[8];
      switch (reason) {
        case 1:
          res = "no-reason-given";
          break;
        case 2:
          res = "application-context-name-not-supported";
          break;
        case 3:
          res = "calling-AE-title-not-recognized";
          break;
        case 7:
          res = "called-AE-title-not-recognized";
          break;
        default:
          break;
      }
    }
  }
  return res;
}

bool SDICOM_Client::associate(SDICOM& newdicom) {
  bool res = false;
  SDICOM_Generic_PDU readwritepdu;
  std::vector<char>  serialdat;
  std::stringstream  msg;
  msg << "SDICOM_Client::associate: Opening connection to remote host: "
           << PrivateData->TargetAddress << ":" << PrivateData->Port;
  SLogger::global().addMessage(msg.str());
  PrivateData->NetClient.open(PrivateData->TargetAddress,PrivateData->Port);
  if (PrivateData->NetClient.isOpen()) {
    SLogger::global().addMessage(
                               "SDICOM_Client::associate: Association-RQ sent");
    // Associate: Prop
    Associate_PDU assocprop;
    constructAssociationRequest(PrivateData->AETitle,
                                PrivateData->TargetAETitle,
                                newdicom,assocprop);
    serialdat.clear();
    assocprop.serialise(serialdat,SDICOM_PDU_Serialisable::DCM_REQUEST);
    readwritepdu.deSerialise(serialdat);
    SDICOM_Server::sendPDU(readwritepdu,PrivateData->NetClient.socket());
    // Associate: Acceptance
    SDICOM_Server::readPDU(PrivateData->NetClient.socket(),readwritepdu);
    if (readwritepdu.Type == ASSOCA_PDU) {
      serialdat.clear();
      readwritepdu.serialise(serialdat);
      if (PrivateData->AcceptedPDU.deSerialise(serialdat) < 0) {
        SLogger::global().addMessage(
                    "SDICOM_Client::associate: Association Response Malformed");
      }
      else {
        SLogger::global().addMessage(
                             "SDICOM_Client::associate: Association Succeeded");
#ifdef DEBUG
        std::map<DICOM_PDU_TYPE,PresContextItem>::iterator presconit;
        for (presconit=assocprop.PresContexts.begin();
             presconit != assocprop.PresContexts.end(); presconit++) {
          std::cerr << "Presentation Context: "
                    << (int)presconit->second.PresContexID
                    << " -> Result: "
                    << (int)presconit->second.ResultReason << std::endl;
        }
#endif
        PrivateData->MaxPDataSize = PrivateData->AcceptedPDU.UserInfo.MaxLength;
        if (PrivateData->MaxPDataSize > 0)
          PrivateData->MaxPDataSize-= PDVPREFIXSIZE;
        res = true;
      }
    }
    else {
      std::stringstream failstr;
      failstr << "SDICOM_Client::associate: Association Failed. Reason given: "
              << getRejectReason(readwritepdu);
      SLogger::global().addMessage(failstr.str());
    }
  }
  else {
    std::stringstream failmsg;
    failmsg << "SDICOM_Client::associate: Cannot connect to remote host: " 
            << PrivateData->TargetAddress << ":" << PrivateData->Port;
    SLogger::global().addMessage(failmsg.str());
  }
  return res;
}

bool SDICOM_Client::disassociate() {
  bool smooth = false;
  if (PrivateData->NetClient.isOpen()) {
    AssociateRJ_PDU    release_response;
    SDICOM_Generic_PDU final_response;
    release_response.Type = ASSOCS_PDU;
    SLogger::global().addMessage(
                             "SDICOM_Client::disassociate: ending association");
    smooth=(SDICOM_Server::sendPDU(release_response,
                                   PrivateData->NetClient.socket()) >= 0);
    if (smooth)
      smooth=(SDICOM_Server::readPDU
                         (PrivateData->NetClient.socket(),final_response) >= 0);
    PrivateData->NetClient.close();
  }
  return smooth;
}

bool SDICOM_Client::sendDICOM(SDICOM& sendobject) {
  bool res = true;
  Data_PDU dataparcel;
  SDICOM_Generic_PDU genpdustore;
  std::vector<char>  datvect;
  std::stringstream  transstream;
  int                msgid;
  DICOM_PDU_TYPE     presid = 0x1;
  // DIMSE C-STORE
  // send the command pdv
  if (res) { // if association was good
    SLogger::global().addMessage(
                "SDICOM_Client::sendDICOM: Starting CStore DICOM transmission");
    msgid =
    SDICOM_Server::encodeDIMSECMD(SDICOM_Server::DCM_CSTORE,sendobject,datvect);
    dataparcel.addPDV(presid,datvect,true,true);
    res = (SDICOM_Server::sendPDU(dataparcel,
                                  PrivateData->NetClient.socket())==0);
    if (res) { // if command transmitted
      // serialise the DICOM object, without a meta-header
      sendobject.writeDICOMTags(transstream,sendobject.getRootTag(),false,true);
      // now send the DICOM object in chunks of no greater than MaxPDataSize
      uint32_t blocksize = PrivateData->MaxPDataSize;
      if (blocksize == 0 || blocksize > MAXBLOCKSIZE)
        blocksize = MAXBLOCKSIZE; // just send in 1M blocks
      while (res) { // while there are no transmission ballfudges
        std::streamsize readsize;
        dataparcel.Data.clear();
        datvect.resize(blocksize);
        transstream.read(&datvect[0],blocksize);
        readsize = transstream.gcount();
        if (readsize != (std::streamsize)blocksize) { // final block
          datvect.resize(readsize);
          dataparcel.addPDV(presid,datvect,false,true);
          res = (SDICOM_Server::sendPDU(dataparcel,
                                        PrivateData->NetClient.socket())>=0);
          break;
        }
        else { // more blocks to come
          dataparcel.addPDV(presid,datvect,false,false);
          res = (SDICOM_Server::sendPDU(dataparcel,
                                        PrivateData->NetClient.socket())>=0);
        }
      }
      SLogger::global().addMessage(
                   "SDICOM_Client::sendDICOM: Data transmitted");
      // get DIMSE response
      res = (SDICOM_Server::readPDU
                              (PrivateData->NetClient.socket(),genpdustore)>=0);
      if (res && (genpdustore.Type == DCMDAT_PDU)) {
        Data_PDU::accumset_t data, commands;
        Data_PDU:: messagelist_t msgs;
        msgs = Data_PDU::getDICOMMessagesFromDataField(genpdustore.Data);
        Data_PDU::accumulateDICOMMessages(data,commands,msgs);
        if (commands.size() > 0) {
          DCMTag resmmsg = 
                   SDICOM_Server::extractDIMSEMessage(commands.begin()->second);
          if (resmmsg.hasTag(0x0000,0x0120) && resmmsg.hasTag(0x0000,0x0120)) {
            DCMTag &status  = resmmsg.getTag(0x0000,0x0900);
            DCMTag &resid = resmmsg.getTag(0x0000,0x0120);
            if ((resid.toInt()  == msgid) && 
                (status.toInt() == SDICOM_Server::DCM_SUCCESS)) {
              res = true;
              SLogger::global().addMessage(
                   "SDICOM_Client::sendDICOM: Success - confirmation received");
            }
            else { // tags not indicative of success
              res = false;
              SLogger::global().addMessage(
                   "SDICOM_Client::sendDICOM: Error - failure indicated");
            }
          } 
          else {  // tags not present
            res = false;
            SLogger::global().addMessage(
         "SDICOM_Client::sendDICOM: Error - malformed response (missing tags)");
          }
        }
        else {    // no command response
          SLogger::global().addMessage(
           "SDICOM_Client::sendDICOM: Error - malformed response (no command)");
          res = false;
        }
      }
      else {     // strange response
        SLogger::global().addMessage(
                   "SDICOM_Client::sendDICOM: Error - unexpected response");
        res = false;
      }
    }
    else {      // premature end
      SLogger::global().addMessage(
                   "SDICOM_Client::sendDICOM: Error transmitting DIMSE CStore");
    }
  }
  return res;
}

bool SDICOM_Client::echo(SDICOM *exemplarp) {
  bool res = false;
  DICOM_PDU_TYPE presid = 0x1;
  Data_PDU echoreq;
  std::vector<char> dimsecmd;
  SDICOM *exemplar = exemplarp;
  SDICOM  statexem;
  if (exemplar == nullptr) {
    //create a new exemplar dicom that uses the secondary capture SOPClassUID
    DCMTag sopclass;
    sopclass.setID(SOPClassUID);
    sopclass.setVR('U','I');
    sopclass.fromString("1.2.840.10008.1.1");
    statexem.getRootTag().addTag(sopclass);
    statexem.setTransferSyntax("1.2.840.10008.1.2");
    exemplar = &statexem;
  }
  SDICOM_Server::encodeDIMSECMD(SDICOM_Server::DCM_ECHO,*exemplar,dimsecmd,
                                false);
  echoreq.addPDV(presid,dimsecmd,true,true);
  SLogger::global().addMessage("SDICOM_Client::echo: Sending CEcho");
  if (SDICOM_Server::sendPDU(echoreq,PrivateData->NetClient.socket())==0) {
    Data_PDU response;
    if (SDICOM_Server::readPDU(PrivateData->NetClient.socket(),response)==0 &&
        response.Type == DCMDAT_PDU) {
      Data_PDU::accumset_t data, commands;
      Data_PDU:: messagelist_t msgs;
      msgs = Data_PDU::getDICOMMessagesFromDataField(response.Data);
      Data_PDU::accumulateDICOMMessages(data,commands,msgs);
      if (commands.size() > 0) {
        DCMTag dimseres = SDICOM_Server::extractDIMSEMessage
                                                     (commands.begin()->second);
        if (dimseres.hasTag(0x0000,0x0900)) {
          if (dimseres.getTag(0x0000,0x0900).toInt() ==
                                                   SDICOM_Server::DCM_SUCCESS) {
            res = true;
            SLogger::global().addMessage
                                ("SDICOM_Client::echo: Confirmation received");
          }
          else { // status not successfull
            std::stringstream badstat;
            badstat << std::hex << dimseres.getTag(0x0000,0x0900).toInt();
            if (dimseres.hasTag(0x0000,0x0902))
              badstat << ": " << dimseres.getTag(0x0000,0x0902).toString();
            SLogger::global().addMessage
                     ("SDICOM_Client::echo: Error - bad status:"+badstat.str());
          }
        }
        else { // no status in res
          SLogger::global().addMessage
                                ("SDICOM_Client::echo: Error - no status");
        }
      }
      else { //no command in response
        SLogger::global().addMessage
                           ("SDICOM_Client::echo: Error - no response command");
      }
    }
    else {
      SLogger::global().addMessage
                                ("SDICOM_Client::echo: Error reading response");
    }
  }
  else {
    SLogger::global().addMessage("SDICOM_Client::echo: Error sending CEcho");
  }
  return res;
}

bool SDICOM_Client::find(DCMTag& attrs, DCMTag& results,
                         int dolevel) {
  SDICOM               tranconf;
  DCMTag               infmodel, qlevel;
  Data_PDU             findreq,findreqdat, resultpdu;
  std::vector<char>    findreqmsg, keylist;
  std::stringstream    keystream;
  std::vector<Data_PDU::accumset_t>
                       result_objects;
  Data_PDU::accumset_t dataobjects;
  const DICOM_PDU_TYPE presid = 0x1;
  bool                 res = true;
  bool                 waitforresponse = true;
  infmodel.setID(SOPClassUID);
  infmodel.setVR('U','I');
  infmodel.fromString("1.2.840.10008.5.1.4.1.2.1.1"); // Patient Model
  qlevel.setID(0x0008,0x0052);
  qlevel.setVR('C','S');
  switch(dolevel) {
    default:
    case SDICOM_Server::DCM_STUDY:
      qlevel.fromString("STUDY");
      SLogger::global().addMessage("SDICOM_Client::find: Study-level query");
      break;
    case SDICOM_Server::DCM_PATIENT:
      qlevel.fromString("PATIENT");
      SLogger::global().addMessage("SDICOM_Client::find: Patient-level query");
      break;
    case SDICOM_Server::DCM_SERIES:
      qlevel.fromString("SERIES");
      SLogger::global().addMessage("SDICOM_Client::find: Series-level query");
    break;
    case SDICOM_Server::DCM_INSTANCE:
      qlevel.fromString("IMAGE");
      SLogger::global().addMessage("SDICOM_Client::find: Image-level query");
    break;
  }
  // build find command
  tranconf.getRootTag().addTag(infmodel);
  SDICOM_Server::encodeDIMSECMD(SDICOM_Server::DCM_FIND,tranconf,findreqmsg,
                                true);
  findreq.addPDV(presid,findreqmsg,true,true);
  // send find command
  SLogger::global().addMessage("SDICOM_Client::find: Transmitting query");
  if(SDICOM_Server::sendPDU(findreq,PrivateData->NetClient.socket()) < 0) {
    SLogger::global().addMessage("SDICOM_Client::find: Error during query");
    res = false;
  }
  else {
    // build find-key data
    tranconf.clear();
    tranconf.setTransferSyntax("1.2.840.10008.1.2"); //LIM
    tranconf.getRootTag().addTag(qlevel);
    for (unsigned t=0; t < attrs.getTags().size(); t++) {
      tranconf.getRootTag().addTag(*(attrs.getTags()[t]));
    }
    tranconf.writeDICOMTags(keystream,tranconf.getRootTag(),false,true);
    keylist = stringToCharVector(keystream.str());
    findreqdat.addPDV(presid,keylist,false,true);
    // send key data
    if (SDICOM_Server::sendPDU(findreqdat,PrivateData->NetClient.socket()) < 0){
      SLogger::global().addMessage
                         ("SDICOM_Client::find: Error during key transmission");
      res = false;
    }
    else { 
      // read response messages (multiple pending are possible)
      SLogger::global().addMessage
                               ("SDICOM_Client::find: Awaiting query response");
      while ( waitforresponse &&
              (SDICOM_Server::readPDU
                           (PrivateData->NetClient.socket(),resultpdu) >= 0) ) {
        Data_PDU::accumset_t    cmds;
        Data_PDU::messagelist_t msgs;
        DCMTag                  cmdtags;
        msgs = Data_PDU::getDICOMMessagesFromDataField(resultpdu.Data);
        if (Data_PDU::accumulateDICOMMessages(dataobjects,cmds,msgs)) {
          result_objects.push_back(dataobjects);
          dataobjects.clear();
        }
        // check the command message, if there is one
        for (Data_PDU::accumset_t::iterator c=cmds.begin();c!=cmds.end();c++) {
          cmdtags.clear();
          SDICOM_Server::decodeDIMSECMD(cmds,c->first,&cmdtags);
          if ( ( ! cmdtags.hasTag(0x0000,0x0900) ) || // status tags
                (cmdtags.getTag(0x0000,0x0900).toInt() != 0xFF00 &&
                cmdtags.getTag(0x0000,0x0900).toInt() != 0xFF00)
            ) {
            SLogger::global().addMessage
                            ("SDICOM_Client::find: Query completion indicated");
            waitforresponse = false;
          }
        }
      }
    }
    // configure the result tree
    tranconf.clear();
    tranconf.setTransferSyntax("1.2.840.10008.1.2"); //LIM
    results.clear();
    results.setID(QueryResultSeq);
    results.setName("QueryResultSequence (active)");
    results.setVR('S','Q');
    // decode the result objects
    for (unsigned r=0; r<result_objects.size(); r++) {
      for (Data_PDU::accumset_t::iterator it=result_objects[r].begin();
          it != result_objects[r].end(); it++) {
        DCMTag newres;
        newres.setID(ItemMarker);
        newres.setVR(' ',' ');
        newres.setName("ResultItem");
        std::stringstream resstr(it->second);
        tranconf.readDICOMTags(resstr,&newres,0-1,true);
        results.addTag(newres,false);
      }
    }
  }
  return res;
}

bool SDICOM_Client::move(DCMTag& attrs, const std::string target, int dolevel) {
  SDICOM               tranconf;
  DCMTag               infmodel, qlevel, targetae;
  Data_PDU             findreq,findreqdat, resultpdu;
  std::vector<char>    findreqmsg, keylist;
  std::stringstream    keystream;
  std::vector<Data_PDU::accumset_t>
                       result_objects;
  Data_PDU::accumset_t dataobjects;
  const DICOM_PDU_TYPE presid = 0x1;
  bool                 res = true;
  bool                 waitforresponse = true;
  infmodel.setID(SOPClassUID);
  infmodel.setVR('U','I');
  infmodel.fromString("1.2.840.10008.5.1.4.1.2.1.2"); // Patient Model
  targetae.setID(0x0000,0x0600);
  targetae.setVR('A','E');
  targetae.fromString(target);
  qlevel.setID(0x0008,0x0052);
  qlevel.setVR('C','S');
  switch(dolevel) {
    default:
    case SDICOM_Server::DCM_STUDY:
      qlevel.fromString("STUDY");
      SLogger::global().addMessage("SDICOM_Client::move: Study-level query");
      break;
    case SDICOM_Server::DCM_PATIENT:
      qlevel.fromString("PATIENT");
      SLogger::global().addMessage("SDICOM_Client::move: Patient-level query");
      break;
    case SDICOM_Server::DCM_SERIES:
      qlevel.fromString("SERIES");
      SLogger::global().addMessage("SDICOM_Client::move: Series-level query");
    break;
    case SDICOM_Server::DCM_INSTANCE:
      qlevel.fromString("IMAGE");
      SLogger::global().addMessage("SDICOM_Client::move: Image-level query");
    break;
  }
  // build move command
  tranconf.getRootTag().addTag(infmodel);
  tranconf.getRootTag().addTag(targetae);
  SDICOM_Server::encodeDIMSECMD(SDICOM_Server::DCM_MOVE,tranconf,findreqmsg,
                                true);
  findreq.addPDV(presid,findreqmsg,true,true);
  // send find command
  SLogger::global().addMessage("SDICOM_Client::move: Transmitting move RQ");
  if(SDICOM_Server::sendPDU(findreq,PrivateData->NetClient.socket()) < 0) {
    SLogger::global().addMessage("SDICOM_Client::move: Error during RQ");
    res = false;
  }
  else {
    // build find-key data
    tranconf.clear();
    tranconf.setTransferSyntax("1.2.840.10008.1.2"); //LIM
    tranconf.getRootTag().addTag(qlevel);
    for (unsigned t=0; t < attrs.getTags().size(); t++) {
      tranconf.getRootTag().addTag(*(attrs.getTags()[t]));
    }
    tranconf.writeDICOMTags(keystream,tranconf.getRootTag(),false,true);
    keylist = stringToCharVector(keystream.str());
    findreqdat.addPDV(presid,keylist,false,true);
    // send key data
    if (SDICOM_Server::sendPDU
                             (findreqdat,PrivateData->NetClient.socket()) < 0) {
      SLogger::global().addMessage
                         ("SDICOM_Client::move: Error during key transmission");
      res = false;
    }
    else {
      // read response messages (multiple pending are possible)
      SLogger::global().addMessage
                               ("SDICOM_Client::move: Awaiting RQ response");
      while ( waitforresponse &&
              (SDICOM_Server::readPDU
                           (PrivateData->NetClient.socket(),resultpdu) >= 0) ) {
        Data_PDU::accumset_t    cmds;
        Data_PDU::messagelist_t msgs;
        DCMTag                  cmdtags;
        msgs = Data_PDU::getDICOMMessagesFromDataField(resultpdu.Data);
        if (Data_PDU::accumulateDICOMMessages(dataobjects,cmds,msgs)) {
          result_objects.push_back(dataobjects);
          dataobjects.clear();
        }
        // check the command message, if there is one
        for (Data_PDU::accumset_t::iterator c=cmds.begin();c!=cmds.end();c++) {
          cmdtags.clear();
          SDICOM_Server::decodeDIMSECMD(cmds,c->first,&cmdtags);
          if ( ( ! cmdtags.hasTag(0x0000,0x0900) ) || // status tags
                (cmdtags.getTag(0x0000,0x0900).toInt() != 0xFF00 &&
                cmdtags.getTag(0x0000,0x0900).toInt() != 0xFF00)
            ) {
            if (cmdtags.getTag(0x0000,0x0900).toInt() ==
                                                   SDICOM_Server::DCM_SUCCESS) {
              SLogger::global().addMessage
                             ("SDICOM_Client::move: Move completion indicated");
            }
            else {
              std::stringstream conv;
              conv << std::hex << cmdtags.getTag(0x0000,0x0900).toInt();
              SLogger::global().addMessage
               ("SDICOM_Client::move: Move error indicated, code: "+conv.str());
               res = false;
            }
            waitforresponse = false;
          }
        }
      }
    }
  }
  return res;
}

bool SDICOM_Client::sendDICOMOneShot(SDICOM& dcmobj, const std::string& addr,
                                     short int port,
                                     const std::string& myaetitle,
                                     const std::string& theiraetitle) {
  bool res = false;
  SDICOM_Client localsender;
  localsender.setTargetAddress(addr);
  localsender.setTargetPort(port);
  localsender.setTargetAETitle(theiraetitle);
  localsender.setAETitle(myaetitle);
  if (localsender.associate(dcmobj))
    if(localsender.sendDICOM(dcmobj))
      if (localsender.disassociate())
        res = true;
  return res;
}

bool SDICOM_Client::echoOneShot(const std::string& targ, short int port,
                                const std::string& myaetitle,
                                const std::string& theiraetitle) {
  SDICOM_Client DCMClient;
  std::stringstream conv;
  SDICOM exemplardcm;
  DCMTag sopclass;
  int    errors = 0;
  // configure a LittleEndianImplit/VerificationSOPClass DICOM object
  // for association and echo request;
  exemplardcm.setTransferSyntax("1.2.840.10008.1.2");
  sopclass.setID(SOPClassUID);
  sopclass.setVR('U','I');
  sopclass.fromString("1.2.840.10008.1.1");
  exemplardcm.getRootTag().addTag(sopclass);
  DCMClient.setTargetPort(port);
  DCMClient.setTargetAddress(targ);
  DCMClient.setAETitle(myaetitle);
  DCMClient.setTargetAETitle(theiraetitle);
  if (DCMClient.associate(exemplardcm)) {
    if (!DCMClient.echo(&exemplardcm))
      errors++;
  }
  else {
    errors++;
  }
  if ((errors > 0) || !DCMClient.disassociate())
    errors++;
  return errors == 0;
}

bool SDICOM_Client::findOneShot(DCMTag& attrs, DCMTag& results, int level,
                                const std::string& targ, short int port,
                                const std::string& myaetitle,
                                const std::string& theiraetitle) {
  SDICOM_Client     DCMClient;
  DCMTag            SOPClass;
  SDICOM            AssocDCM;
  int               errors = 0;
  // configure the association object
  AssocDCM.setTransferSyntax("1.2.840.10008.1.2"); //LIM
  SOPClass.setID(SOPClassUID);
  SOPClass.setVR('U','I');
  SOPClass.fromString("1.2.840.10008.5.1.4.1.2.1.1"); // PatientRootFind
  AssocDCM.getRootTag().addTag(SOPClass);
  // now configure the query
  DCMClient.setTargetPort(port);
  DCMClient.setTargetAddress(targ);
  DCMClient.setAETitle(myaetitle);
  DCMClient.setTargetAETitle(theiraetitle);
  if (DCMClient.associate(AssocDCM)) {
    if (!DCMClient.find(attrs, results, level)) {
      errors++;
    }
  }
  else {
    errors++;
  }
  if ((errors > 0) || !DCMClient.disassociate())
    errors++;
  return errors == 0;
}

bool SDICOM_Client::moveOneShot(DCMTag& attrs, const std::string targetae,
                                int level, const std::string& targ,
                                short int port, const std::string& myaetitle,
                                const std::string& theiraetitle) {
  SDICOM_Client     DCMClient;
  DCMTag            SOPClass;
  SDICOM            AssocDCM;
  int               errors = 0;
  // configure the association object
  AssocDCM.setTransferSyntax("1.2.840.10008.1.2"); //LIM
  SOPClass.setID(SOPClassUID);
  SOPClass.setVR('U','I');
  SOPClass.fromString("1.2.840.10008.5.1.4.1.2.1.2"); // PatientRootFind
  AssocDCM.getRootTag().addTag(SOPClass);
  // now configure the query
  DCMClient.setTargetPort(port);
  DCMClient.setTargetAddress(targ);
  DCMClient.setAETitle(myaetitle);
  DCMClient.setTargetAETitle(theiraetitle);
  if (DCMClient.associate(AssocDCM)) {
    if (!DCMClient.move(attrs, targetae, level)) {
      errors++;
    }
  }
  else {
    errors++;
  }
  if ((errors > 0) || !DCMClient.disassociate())
    errors++;
  return errors == 0;
}

bool SDICOM_Client::findToSArch(DCMTag& findresults, SDICOMArch& target) {
  bool   res = true;
  SDICOM impres;
  if (findresults.getID() == QueryResultSeq) {
    for (unsigned i=0; i < findresults.getTags().size(); i++) {
      if (findresults.getTags()[i]->getID() == ItemMarker) {
        impres.clear();
        impres.getRootTag().addTag(*(findresults.getTags()[i]));
        SDCMArchNode &leafnode = 
                 target.importGeneric(target.getRootNodeChild(),impres.getRootTag());
        if (!leafnode.hasTag(ExemplarTag))
          leafnode.addTag(target.genExemplar(impres,true));
      }
      else {
        res = false;
      }
    }
  }
  else {
    res = false;
  }
  return res;
}

std::string SDICOM_Client::wadoURI(const std::string& baseuri,
                                   const std::string& studyuid,
                                   const std::string& seriesuid,
                                   const std::string& instanceuid) {
  // <base>?requestType=WADO&studyUID=<study>&seriesUID=<series>&objectUID=
  // <instance>&contentType=application/dicom
  return baseuri + "?requestType=WADO&studyUID=" + studyuid + "&seriesUID=" + 
         seriesuid + "&objectUID=" + instanceuid + 
         "&contentType=application/dicom";
}

/*------------------------------------------------------------------------------
 *                               SDICOM Server
 *----------------------------------------------------------------------------*/

// TODO: Association restrictions (AETitles etc)

class SDICOM_Server::SDICOM_ServerPIMPL {
public:
  std::function<void(SDICOM*)> Callback;
  std::string                  DiskDir;
  bool                         DiskStoreMode, RestrictedMode;
  std::string                  AETitle;
};

SDICOM_Server::SDICOM_Server(): PrivateData(new SDICOM_ServerPIMPL()) {
  setDiskStoreMode(false);
  setRestrictedMode(true);
  setAETitle(sysInfo::systemName());
  setReceptionCallback(nullptr);
  setTargetStoreDirectory(SFile::localTempDIR());
}

SDICOM_Server::~SDICOM_Server() {
  delete PrivateData;
}

void SDICOM_Server::setDiskStoreMode(bool nds) {
  PrivateData->DiskStoreMode = nds;
}

bool SDICOM_Server::diskStoreMode() const {
  return PrivateData->DiskStoreMode;
}

void SDICOM_Server::setTargetStoreDirectory(const std::string& ndir) {
  PrivateData->DiskDir = ndir;
}

std::string SDICOM_Server::targetStoreDirectory() const {
  return PrivateData->DiskDir;
}

void SDICOM_Server::setAETitle(const std::string& nae) {
  PrivateData->AETitle = nae;
  PrivateData->AETitle.resize(AETLENGTH,' ');
}

std::string SDICOM_Server::aetitle() const {
  return PrivateData->AETitle;
}

bool SDICOM_Server::restrictedMode() const {
  return PrivateData->RestrictedMode;
}

void SDICOM_Server::setRestrictedMode(bool nres) {
  PrivateData->RestrictedMode = nres;
}

void SDICOM_Server::setReceptionCallback(std::function< void(SDICOM*) > ncb) {
  PrivateData->Callback = ncb;
}

int SDICOM_Server::readPDU(ssocket_t &source, SDICOM_Generic_PDU &read_pdu) {
  size_t readsize = 0;
  const unsigned TYPE_SIZE = sizeof(DICOM_PDU_TYPE);
  const unsigned SKIP_SIZE = 1;
  const unsigned SIZE_SIZE = sizeof(DICOM_PDU_LENGTH);
  uint8_t        SKIP      = 0;
  uint32_t       SIZE      = 0;
  std::vector<char>
                 SIZEBUFF;
  SIZEBUFF.resize(SIZE_SIZE,0);
  std::stringstream
                 saddr;
  try {
    saddr << source.remote_endpoint().address();
  }
  catch (std::exception &e) {
    saddr << "disconnected_ip";
  }
  if (source.is_open()) {
    try {
      // read the type
      readsize = read(source,
                      boost::asio::buffer((char*)&read_pdu.Type,TYPE_SIZE));
      if (readsize != TYPE_SIZE) {
        SLogger::global().addMessage
        ("SDICOM_Server::readPDU Error: could not read type! ("
          + saddr.str() + ")");
        return -1;
      }
      // read skip token
      readsize = read(source,boost::asio::buffer((char*)&SKIP,SKIP_SIZE));
      if (readsize != SKIP_SIZE) {
        SLogger::global().addMessage
        ("SDICOM_Server::readPDU Error: could not read reserved token! ("
          + saddr.str() + ")");
        return -2;
      }
      // read the size
      readsize = read(source,boost::asio::buffer(&SIZEBUFF[0],SIZE_SIZE));
      if (readsize != SIZE_SIZE) {
        SLogger::global().addMessage
        ("SDICOM_Server::readPDU Error: could not read length! ("
        + saddr.str() + ")");
        return -3;
      }
      // first, store the full data prefix in the PDU class
      read_pdu.Data.clear();
      // read the data
      SDICOM_PDU_ENDIAN_CORR<char>(SIZEBUFF);
      SIZE = *((uint32_t*)(&SIZEBUFF[0]));
      read_pdu.Data.resize(SIZE,0);
      readsize = read(source,boost::asio::buffer(&read_pdu.Data[0],SIZE));
      if (readsize != SIZE) {
        SLogger::global().addMessage
        ("SDICOM_Server::readPDU Error: could not read data! ("
          + saddr.str() + ")");
        return -4;
      }
    }
    catch (std::exception &e) {
      SLogger::global().addMessage
        ("SDICOM_Server::readPDU Error: " + std::string(e.what()) + " ("
          + saddr.str() + ")");
      return -5;
    }
  }
  return 0;
}

int SDICOM_Server::sendPDU(SDICOM_PDU_Serialisable& source,
                           ssocket_t & target) {
  std::size_t sendres;
  int         result = -1;
  std::vector<char> datastream;
  source.serialise(datastream);
  if (target.is_open()) {
    try {
      // write serialised data
      sendres = target.send
                 (boost::asio::buffer((char*)&datastream[0],datastream.size()));
      if (sendres == datastream.size())
        result = 0;
    }
    catch (std::exception &e) {
      result = 0;
      std::stringstream saddr;
      saddr << target.remote_endpoint().address() << " -- " << e.what();
      SLogger::global().addMessage
      ("SDICOM_Server::sendPDU Error: could not write data ("
        + saddr.str() + ")");
    }
  }
  return result;
}

SDICOM_Server::DIMSE_CMD SDICOM_Server::decodeDIMSECMD
         (Data_PDU::accumset_t& cmds, DIMSE_CMD_TYPE msgid, DCMTag* tagtarget) {
  DIMSE_CMD retval = DCM_UNKNOWN_CMD;
  try {
    std::stringstream dcmstream(cmds[msgid]);
    SDICOM cmddecoder;
    // force little-endian implicit
    cmddecoder.setTransferSyntax("1.2.840.10008.1.2");
    cmddecoder.readDICOMTags(dcmstream,&cmddecoder.getRootTag(),-1,true,0);
    if(cmddecoder.hasTag(0x0000,0x0100))
      retval = (DIMSE_CMD)cmddecoder.getTag(0x0000,0x0100).toInt();
    if (tagtarget != nullptr) {
      for (unsigned t=0; t < cmddecoder.getRootTag().getTags().size(); t++)
        tagtarget->addTag(*(cmddecoder.getRootTag().getTags()[t]));
    }
  }
  catch (std::exception& excep) {
    std::stringstream errmsg;
    errmsg << "SDICOM_Server: could not handle command due to:"
           << excep.what();
    SLogger::global().addMessage(errmsg.str());
  }
  return retval;
}

DCMTag SDICOM_Server::extractDIMSEMessage(std::string& msgstr) {
  SDICOM decoder;
  std::stringstream msgstream(msgstr);
  decoder.setTransferSyntax("1.2.840.10008.1.2");
  decoder.readDICOMTags(msgstream,&decoder.getRootTag(),-1,true,0);
  return decoder.getRootTag();
}

int SDICOM_Server::encodeDIMSECMD(SDICOM_Server::DIMSE_CMD command,
                                  SDICOM& exemplar, std::vector<char>&target,
                                  bool hasdata) {
  int result = 0;
  static SLockable incrlock;
  static uint16_t messageid = 0;
  uint16_t lmessageid;
  int minfoend = 0;
  unsigned sizetotal = 0;
  DCMTag cmdgrouplen, cmdfield, priority, msgid, // general
         affsopinstuid, affsopcluid, datatype, // for CSTORE
         targetae; //for CMove
  SDICOM writer;
  std::stringstream dcmout;
  incrlock.lock();
  lmessageid = ++messageid;
  incrlock.unlock();
  writer.clear();
  writer.setTransferSyntax("1.2.840.10008.1.2");
  cmdgrouplen.setID(0x0000,0x0000);
  cmdgrouplen.setVR('U','L');
  cmdfield.setID(0x0000,0x0100);
  cmdfield.setVR('U','S');
  cmdfield.fromInt((uint16_t) command);
  msgid.setID(0x0000,0x0110);
  msgid.setVR('U','S');
  msgid.fromInt(lmessageid);
  priority.setID(0x0000,0x0700);
  priority.setVR('U','S');
  priority.fromInt(DEFAULTPRIORITY);
  affsopcluid.setID(0x0000,0x0002);
  affsopcluid.setVR('U','I');
  datatype.setID(0x0000,0x0800);
  datatype.setVR('U','S');
  affsopinstuid.setID(0x0000,0x1000);
  affsopinstuid.setVR('U','I');
  targetae.setID(0x0000,0x0600);
  targetae.setVR('A','E');
  // now perform the final message struction on a per-command basis
  switch (command) {
    default:
      SLogger::global().addMessage("SDICOM_Server::encodeDIMSECMD: request " +
                       std::string("to construct DIMSE from unknown command!"));
      result--;
      __FALLTHROUGH__
    case DCM_FIND:
    case DCM_CSTORE:
    case DCM_ECHO:
    case DCM_MOVE:
      if (exemplar.getRootTag().hasTag(0x0008,0x0016)) {
        affsopcluid.fromString
                       (exemplar.getRootTag().getTag(0x0008,0x0016).toString());
        sizetotal+= (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE));
        SDICOM::standardizeTag(affsopcluid,false);
        sizetotal+= affsopcluid.getDataLength();
      }
      else
        result--;
      if (exemplar.getRootTag().hasTag(0x0008,0x0018)) {
        affsopinstuid.fromString
                       (exemplar.getRootTag().getTag(0x0008,0x0018).toString());
        sizetotal+= (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE));
        SDICOM::standardizeTag(affsopinstuid,false);
        sizetotal+= affsopinstuid.getDataLength();
      }
      else
        result--;
      if (exemplar.getRootTag().hasTag(0x0000,0x0600)) {
        targetae.fromString
                       (exemplar.getRootTag().getTag(0x0000,0x0600).toString());
        sizetotal+= (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE));
        SDICOM::standardizeTag(targetae,false);
        sizetotal+= targetae.getDataLength();
      }
      if (hasdata)
        datatype.fromInt(1);
      else
        datatype.fromInt(0x0101);
      sizetotal += ((sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE))*4);
      SDICOM::standardizeTag(cmdfield,false);
      SDICOM::standardizeTag(msgid,false);
      SDICOM::standardizeTag(priority,false);
      SDICOM::standardizeTag(datatype,false);
      sizetotal += + cmdfield.getDataLength()
                   + priority.getDataLength()
                   + msgid.getDataLength()
                   + datatype.getDataLength();
      cmdgrouplen.fromInt(sizetotal);
      SDICOM::standardizeTag(cmdgrouplen,false);
      writer.writeOneTag(dcmout,cmdgrouplen,minfoend);
      if (affsopcluid.getDataLength() > 0) {
        writer.writeOneTag(dcmout,affsopcluid,minfoend);
      }
      writer.writeOneTag(dcmout,cmdfield,minfoend);
      writer.writeOneTag(dcmout,msgid,minfoend);
      if (targetae.getDataLength() > 0) {
        writer.writeOneTag(dcmout,targetae,minfoend);
      }
      writer.writeOneTag(dcmout,priority,minfoend);
      writer.writeOneTag(dcmout,datatype,minfoend);
      if (affsopinstuid.getDataLength() > 0) {
        writer.writeOneTag(dcmout,affsopinstuid,minfoend);
      }
      target = stringToCharVector(dcmout.str());
      result = lmessageid;
      break;
  }
  return result;
}

int SDICOM_Server::respondDIMSE(ssocket_t& target,
                                 Data_PDU::accumset_t& cmdset,
                                 DICOM_PDU_TYPE presid,
                                 SDICOM_Server::DIMSE_RSP response) {
  DCMTag cmdgrplen, affsopuid, cmdfl, dstyp, affsopinuid, respondingto,
         status;
  SDICOM dcmrw;
  int result = 0;
  unsigned sizetotal = 0;
  int minfoend = 0;
  Data_PDU dimseresponse;
  std::vector<char> dimsedat;
  // force little-endian implicit
  dcmrw.setTransferSyntax("1.2.840.10008.1.2");
  unsigned short rmessageid = 0;
  std::stringstream cmdstrm(cmdset[presid]);
  std::stringstream resstream;
  // first, get the message id to which this will reply
  dcmrw.readDICOMTags(cmdstrm,&dcmrw.getRootTag(),-1,true,0);
  if(dcmrw.hasTag(0x0000,0x0110))
    rmessageid = (unsigned short) dcmrw.getTag(0x0000,0x0110).toInt();
  else
    result = -1;
  // then AffectedSOPClassUID
  if(dcmrw.hasTag(0x0000,0x0002)) {
    affsopuid = dcmrw.getTag(0x0000,0x0002);
    sizetotal += (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE))
              + affsopuid.getDataLength();
  }
  else
    result = -2;
  // then CommandField
  if(dcmrw.hasTag(0x0000,0x0100)) {
    uint16_t cmdtmp = (uint16_t)dcmrw.getTag(0x0000,0x0100).toInt();
    cmdfl.setID(0x0000,0x0100);
    cmdfl.setVR('U','S');
    cmdfl.fromInt(cmdtmp | 0x8000);
    sizetotal += (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE))
              + cmdfl.getDataLength();
  }
  else
    result = -3;
  // then AffectedSOPInstanceUID
  if(dcmrw.hasTag(0x0000,0x1000)) {
    affsopinuid = dcmrw.getTag(0x0000,0x1000);
    sizetotal += (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE))
              + affsopinuid.getDataLength();
  }
  else
    result = -5;
  // rest the dcmrw
  dcmrw.clear();
  dcmrw.setTransferSyntax("1.2.840.10008.1.2");
  // now configure response-specific tags, starting with responding-to
  respondingto.setID(0x0000,0x0120);
  respondingto.setVR('U','S');
  respondingto.fromInt(rmessageid);
  // set the response status to the whatever the argument says
  status.setID(0x0000,0x0900);
  status.setVR('U','S');
  status.fromInt(response);
  // no type for this data set
  dstyp.setID(0x0000,0x0800);
  dstyp.setVR('U','S');
  dstyp.fromInt(0x0101);
  // calculate the remaining size of the message (the definite tags)
  sizetotal = (sizeof(DICOM_ID_LENGTH)+sizeof(DICOM_NET_SIZE)) * 3;
  sizetotal += dstyp.getDataLength() + respondingto.getDataLength()
            + status.getDataLength();
  // set the cml group tag
  cmdgrplen.setID(0x0000,0x0000);
  cmdgrplen.setVR('U','L');
  cmdgrplen.fromInt(sizetotal);
    // write each tag to the stream
  SDICOM::standardizeTag(cmdgrplen,false);
  dcmrw.writeOneTag(resstream,cmdgrplen,minfoend);
  if (affsopuid.getDataLength() > 0) {
    SDICOM::standardizeTag(affsopuid,false);
    dcmrw.writeOneTag(resstream,affsopuid,minfoend);
  }
  if (cmdfl.getDataLength() > 0) {
    SDICOM::standardizeTag(cmdfl,false);
    dcmrw.writeOneTag(resstream,cmdfl,minfoend);
  }
  if (respondingto.getDataLength() > 0) {
    SDICOM::standardizeTag(respondingto,false);
    dcmrw.writeOneTag(resstream,respondingto,minfoend);
  }
  if (dstyp.getDataLength() > 0) {
    SDICOM::standardizeTag(dstyp,false);
    dcmrw.writeOneTag(resstream,dstyp,minfoend);
  }
  if (status.getDataLength() > 0) {
    SDICOM::standardizeTag(status,false);
    dcmrw.writeOneTag(resstream,status,minfoend);
  }
  if (affsopinuid.getDataLength() > 0) {
    SDICOM::standardizeTag(affsopinuid,false);
    dcmrw.writeOneTag(resstream,affsopinuid,minfoend);
  }
  dcmrw.clear();
  dimsedat = stringToCharVector(resstream.str());
  dimseresponse.addPDV(presid,dimsedat,true,true);
  sendPDU(dimseresponse,target);
  return result;
}

static void constructAssociationRequest(const std::string& localaetitle,
                                        const std::string& remoteaetitle,
                                        SDICOM& exemplar,
                                        Associate_PDU& targetpdu) {
  targetpdu.ProtocolVersion = PROTOCOL_VERSION;
  targetpdu.CalledAETitle   = remoteaetitle;
  targetpdu.CallingAETitle  = localaetitle;
  targetpdu.AppContext.Data = stringToCharVector(AppContextStr) ;
  // Presentation Contexts: currently just propose the native object's SOP
  // and the secondary capture SOP
  PresContextItem proposal;
  proposal.PresContexID = 1; //incremented later
  proposal.ResultReason = 0;
  // just one transfer syntax; that of the exemplar
  SDICOM_SHORT_PDU transyn;
  transyn.Type = TRXCTX_PDU;
  transyn.Data = stringToCharVector(exemplar.getTransferSyntax());
  proposal.TransferSyntaxs.push_back(transyn);
  if (exemplar.getRootTag().hasTagDFS(SOPClassUID)) {
    proposal.AbstractSyntax.Data =
    stringToCharVector(exemplar.getRootTag().getTagDFS(SOPClassUID).toString());
    targetpdu.PresContexts[proposal.PresContexID] = proposal;
    proposal.PresContexID += 2; //incremented later
  }
  else {
    proposal.AbstractSyntax.Data =
      stringToCharVector("1.2.840.10008.5.1.4.1.1.7");
    targetpdu.PresContexts[proposal.PresContexID] = proposal;
  }
  // userinfo
  targetpdu.UserInfo.MaxLength = DICOM_MAX_LEN;
}

static void constructAssociationAccept(Associate_PDU &incoming,
                                       Associate_PDU &target ) {
  target.ProtocolVersion = incoming.ProtocolVersion;
  target.CalledAETitle   = incoming.CalledAETitle;
  target.CallingAETitle  = incoming.CallingAETitle;
  target.AppContext = incoming.AppContext;
  target.PresContexts.clear();
  for (std::map<DICOM_PDU_TYPE,PresContextItem>::iterator it =
       incoming.PresContexts.begin();
       it != incoming.PresContexts.end(); it ++ ) {
    if ( (*it).second.TransferSyntaxs.size() > 0) {
      target.PresContexts.insert(std::pair<DICOM_PDU_TYPE,PresContextItem>
                                 ((*it).second.PresContexID,(*it).second));
      target.PresContexts[(*it).second.PresContexID].ResultReason = 0;
      // accept first!
      target.PresContexts[(*it).second.PresContexID].TransferSyntaxs.resize(1);
    }
  }
  target.UserInfo.MaxLength = DICOM_MAX_LEN;
}

static std::string nameDICOMFile(SDICOM& dcmsrc) {
  static SLockable  locker;
  static uint16_t   counter = 0;
  std::stringstream resname;
  if (dcmsrc.hasTag(0x0008, 0x0018)) {
    resname << dcmsrc.getTag(0x0008, 0x0018).toString();
  }
  else {
    sysInfo::localTime();
  }
  locker.lock();
  resname << "-" << counter++ << ".dcm";
  locker.unlock();
  return resname.str();
}

void SDICOM_Server::handleCStore(const Associate_PDU& association,
                                       Data_PDU::accumset_t& msgsource,
                                       DIMSE_CMD_TYPE msgid) {
  if (msgsource.count((uint8_t)msgid) > 0) {
    SDICOM *dicomobject = new SDICOM();
    std::stringstream dcmstream(msgsource[msgid]);
    try {
      dicomobject->setTransferSyntax(charVectorToString
               (association.PresContexts.at(msgid).TransferSyntaxs.at(0).Data));
      dicomobject->readDICOMTags(dcmstream,&dicomobject->getRootTag(),-1,true);
      if (diskStoreMode()) {
        SURI disloc(targetStoreDirectory());
        disloc.addComponentBack(nameDICOMFile(*dicomobject));
        dicomobject->changeLocation(disloc.getURI());
        dicomobject->writeAllTags();
        SLogger::global().addMessage("SDICOM_Server: Stored DICOM file: "
                                     + disloc.getURI());
      }
      else
        dicomobject->changeLocation("MemoryResident");
      if (PrivateData->Callback != nullptr) {
        SPool::fireAndForget(std::bind(PrivateData->Callback,dicomobject));
      }
      else
        delete dicomobject;
    }
    catch (std::exception& excep) {
      std::stringstream errmsg;
      errmsg << "SDICOM_Server: could not handle message due to:" 
             << excep.what();
      SLogger::global().addMessage(errmsg.str());
      delete dicomobject;
    }
  }
}

void SDICOM_Server::termAssociation(ssocket_t& targ, DICOM_PDU_TYPE succ) {
  AssociateRJ_PDU closepdu;
  closepdu.Type = succ;
  sendPDU(closepdu,targ);
}

bool SDICOM_Server::handleAcceptedPDU(const Associate_PDU& association,
                                      const SDICOM_Generic_PDU& newpdu,
                                      ssocket_t& sourcesocket,
                                      Associate_PDU&,
                                      Data_PDU::accumset_t  &dicomobjs,
                                      Data_PDU::accumset_t  &dicomcmds) {
  bool                    res = true;
  Data_PDU::messagelist_t newobjects;
  int16_t                 objstat = -1;
  std::stringstream       msg;
  std::stringstream       msg_addr;
  Data_PDU::accumset_t    newcmds;
  // pre build a simple release response, for potential later use
  AssociateRJ_PDU release_response;
  release_response.Type = ASSOCP_PDU;
  // try to get the remote address, but may through if disconnected already
  try {
    msg_addr << sourcesocket.remote_endpoint().address();
  }
  catch (std::exception &e) {
    msg_addr << "disconnected_ip";
  }
  switch (newpdu.Type) {
    case DCMDAT_PDU:
      newobjects = Data_PDU::getDICOMMessagesFromDataField(newpdu.Data);
      objstat = Data_PDU::accumulateDICOMMessages(dicomobjs,
                                                  newcmds,
                                                  newobjects);
      // check commands immediately
      if (newcmds.size() > 0) {
        // reset the data on a new command
        dicomobjs.clear();
        for (Data_PDU::accumset_t::iterator newcmd = newcmds.begin();
             newcmd != newcmds.end();
             newcmd++) {
          DIMSE_CMD cmd = decodeDIMSECMD(newcmds,newcmd->first);
          std::stringstream cmdval;
          switch (cmd) {
            case DCM_CSTORE:
              SLogger::global().addMessage
                                  ("SDICOM_Server: CStore command received");
              // merge commands
              dicomcmds.insert(*newcmd);
              break;
            case DCM_ECHO:
              SLogger::global().addMessage
                    ("SDICOM_Server: CEcho command received and response sent");
              respondDIMSE(sourcesocket,newcmds,newcmd->first,DCM_SUCCESS);
              break;
            default:
              cmdval << std::hex << cmd;
              SLogger::global().addMessage
                                  ("SDICOM_Server: Unrecognised DIMSE Command: "
                                  + cmdval.str());
              respondDIMSE(sourcesocket,newcmds,newcmd->first,DCM_ERROR);
              dicomcmds.insert(*newcmd);
              break;
          }
        }
      }
      // end-of-transmission consolidation
      // returns the prescontext ID if this is the last message in the set
      if (objstat >= 0) {
        msg << "SDICOM_Server: Object complete "
            << "(" << msg_addr.str() << ")";
        SLogger::global().addMessage(msg.str());
        // select the appropriate action based on the command requested
        SDICOM decodecmd,decodeobj;
        std::stringstream tmpcmd(dicomcmds.begin()->second);
        std::stringstream tmpobj(dicomobjs.begin()->second);
        switch (decodeDIMSECMD(dicomcmds,objstat)) {
          case DCM_CSTORE:
            handleCStore(association,dicomobjs,objstat);
            respondDIMSE(sourcesocket,dicomcmds,objstat,DCM_SUCCESS);
            dicomobjs.clear();
            dicomcmds.clear();
            newobjects.clear();
            break;
          default:
            SLogger::global().addMessage
             ("SDICOM_Server: Unrecognised DIMSE Command (post-data)");
             respondDIMSE(sourcesocket,newcmds,objstat,DCM_ERROR);
             break;
        }
        // remove the data from memory
        dicomobjs.erase(objstat);
        res = true; // should wait for assoc conf
      }
      break;
    case ASSOCJ_PDU:
      SLogger::global().addMessage("SDICOM_Server: Association Rejected ("
                                   +msg_addr.str()+")");
      res = false;
      break;
    case ASSOCS_PDU:
    case ASSOCP_PDU:
      sendPDU(release_response,sourcesocket);
      SLogger::global().addMessage("SDICOM_Server: Association Released ("
                                   +msg_addr.str()+")");
      res = false;
      break;
    case ASSOCB_PDU:
      SLogger::global().addMessage("SDICOM_Server: Association Aborted ("
                                   +msg_addr.str()+")");
      res = false;
      break;
    default:
      SLogger::global().addMessage("SDICOM_Server: Unrecognised PDU (" 
                                    + msg_addr.str() + ")");
      break;
  }
  return res;
}

bool SDICOM_Server::validateAssociation(const Associate_PDU& reqassoc) {
  bool result = true;
  if (PrivateData->RestrictedMode) {
    if (reqassoc.CalledAETitle != PrivateData->AETitle) {
       SLogger::global().addMessage
        ("SDICOM_Server::validateAssociation: Unrecognised called AETitle: " +
         reqassoc.CalledAETitle);
      result = false;
    }
  }
  return result;
}

void SDICOM_Server::handleConnection(ssocket_t& source) {
  // Handle an incoming connection by making an association
  SDICOM_Generic_PDU   readwritepdu;
  Data_PDU::accumset_t dicomobjs;
  Data_PDU::accumset_t dicomcmds;
  std::stringstream saddr;
  try {
    saddr << source.remote_endpoint().address();
  }
  catch (std::exception &e) {
    saddr << "disconnected_ip";
  }
  SLogger::global().addMessage("SDICOM_Server: Association Requested ("
                               +saddr.str()+")");
  // 1) read PDU (expect associate request)
  if ( readPDU(source,readwritepdu) ) {
    SLogger::global().addMessage("SDICOM_Server: Association error - Aborting ("
                                 + saddr.str() + ")");
  } else {
    if (readwritepdu.Type == ASSOCR_PDU) {
      Associate_PDU newassocrequest;
      Associate_PDU newassocaccept;
      std::vector<char> newassocdat;
      readwritepdu.serialise(newassocdat);
      if (newassocrequest.deSerialise(newassocdat) > 0) {
        // Association Request received
        if (validateAssociation(newassocrequest)) {
          // construct Association Accept
          constructAssociationAccept(newassocrequest,newassocaccept);
          newassocdat.clear();
          newassocaccept.serialise(newassocdat,
                                  SDICOM_PDU_Serialisable::DCM_ACCEPT);
          readwritepdu.deSerialise(newassocdat);
          SLogger::global().addMessage
                  ("SDICOM_Server: Association Accepted (" + saddr.str() + ")");
          // send acceptance
          sendPDU(readwritepdu,source);
          // read handle subsequent PDUs, post association acceptance
          while (readPDU(source,readwritepdu) >= 0)
            if (!handleAcceptedPDU(newassocaccept,readwritepdu,source,
                                  newassocrequest,dicomobjs,dicomcmds)
              )
              break;
        }
        else {
          SLogger::global().addMessage
                               ("SDICOM_Server: Association failed validation (" 
                                 + saddr.str() + ")");
          AssociateRJ_PDU    release_response;
          SDICOM_Generic_PDU final_response;
          release_response.Type = ASSOCJ_PDU;
          SLogger::global().addMessage(
                             "SDICOM_Server: Ending association");
          sendPDU(release_response,source);
        }
      }
      else
        SLogger::global().addMessage
                           ("SDICOM_Server: Error - malformed association PDU");
    }
    else
      SLogger::global().addMessage
                   ("SDICOM_Server: Error - expected association request ("
                                                           + saddr.str() + ")");
  }
  SLogger::global().addMessage("SDICOM_Server: Association Ended ("
                               +saddr.str()+")");
}

short unsigned int SDICOM_Server::genMessageID() {
  static SLockable        locker;
  static unsigned short   msgidcount = 0;
  locker.lock();
  msgidcount++;
  locker.unlock();
  return msgidcount;
}
