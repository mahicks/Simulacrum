/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::DICOM::Networking
 * Interface specification for DICOM Networking
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_DICOM_NET
#define SIMULACRUM_DICOM_NET

#include "types.h"
#include "sdicom.h"
#include <Core/slockable.h>
#include <Core/SPool/SPool.h>
#include <Network/snet.h>

#include <vector>
#include <map>
#include <functional>

namespace Simulacrum {

  class SIMU_API SDICOM_PDU_Serialisable {
  public:
    enum ACTION_ENUM {NIL,DCM_REQUEST,DCM_ACCEPT,DCM_REJECT,DCM_ABORT,
                      DCM_CLOSE};
    virtual          ~SDICOM_PDU_Serialisable();
    virtual
    long int          deSerialise(const std::vector<char>&) =0;
    virtual
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL) =0;
  };

  class SIMU_API SDICOM_Generic_PDU: public SDICOM_PDU_Serialisable {
  public:
    DICOM_PDU_TYPE    Type;
    std::vector<char> Data;
    std::string       toString() const;
                      SDICOM_Generic_PDU();
    long int          deSerialise(const std::vector<char>&);
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL);
  };

  class SIMU_API SDICOM_SHORT_PDU: public SDICOM_PDU_Serialisable {
  public:
    DICOM_PDU_TYPE    Type;
    std::vector<char> Data;
    long int          deSerialise(const std::vector<char>&);
    long int          deSerialise(const std::vector<char>&, unsigned start);
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL);
  };

  class SIMU_API PresContextItem: public SDICOM_PDU_Serialisable {
  public:
    DICOM_PDU_TYPE    PresContexID;
    DICOM_PDU_TYPE    ResultReason;
    SDICOM_SHORT_PDU  AbstractSyntax;
    std::vector<SDICOM_SHORT_PDU>
                      TransferSyntaxs;
    long int          deSerialise(const std::vector<char>&);
    long int          serialise(std::vector<char>&,ACTION_ENUM);
  };

  class SIMU_API UserInfoItem: public SDICOM_PDU_Serialisable {
  public:
    uint32_t          MaxLength;
    std::vector<SDICOM_SHORT_PDU>
                      UserInfoItems;
    long int          deSerialise(const std::vector<char>&);
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL);
  };

  class SIMU_API Associate_PDU: public SDICOM_PDU_Serialisable {
  public:
    uint16_t          ProtocolVersion;
    std::string       CalledAETitle;
    std::string       CallingAETitle;
    SDICOM_SHORT_PDU  AppContext;
    std::map<DICOM_PDU_TYPE,PresContextItem>
                      PresContexts;
    UserInfoItem      UserInfo;
    long int          deSerialise(const std::vector<char>&);
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL);
    std::string       toString() const;
  };

  class SIMU_API AssociateRJ_PDU: public SDICOM_PDU_Serialisable {
  public:
    DICOM_PDU_TYPE    Type;
    uint8_t           Result;
    uint8_t           Source;
    uint8_t           Reason;
    long int          deSerialise(const std::vector<char>&);
    long int          serialise(std::vector<char>&,ACTION_ENUM=NIL);
  };

  class SIMU_API Data_PDU: public SDICOM_Generic_PDU {
  public:
                      Data_PDU();
    void              addPDV(DICOM_PDU_TYPE presid, std::vector<char>&,
                             bool iscmd, bool islast);
    typedef           std::multimap<uint8_t,std::pair<const char*,std::size_t> >
                      messagelist_t;
    typedef           std::map<uint8_t,std::string> accumset_t;
    static
    messagelist_t     getDICOMMessagesFromDataField(const std::vector<char>&);
    static
    int16_t           accumulateDICOMMessages(accumset_t&,     //msgs
                                              accumset_t&,     //cmds
                                              messagelist_t&); //source
  };

//------------------------------------------------------------------------------

  class SDICOMArch;
  class SIMU_API SDICOM_Client {
  public:
             SDICOM_Client    ();
    virtual ~SDICOM_Client    ();
    void     setTargetAddress (const std::string&);
    void     setTargetPort    (short port);
    void     setAETitle       (const std::string&);
    void     setTargetAETitle (const std::string&);
    bool     associate        (SDICOM&);
    bool     disassociate     ();
    bool     sendDICOM        (SDICOM&);
    bool     echo             (SDICOM* = nullptr);
    bool     find             (DCMTag& attrs, DCMTag& results,
                               int level);
    bool     move             (DCMTag& attrs, const std::string target,
                               int level);
    static
    bool     sendDICOMOneShot (SDICOM& dcmobj, const std::string& add,
                               short port = 104, 
                               const std::string& myaetitle = "LOCALAE",
                               const std::string& theiraetitle = "ANYAE");
    static
    bool     echoOneShot      (const std::string& targ, short port=104,
                               const std::string& myaetitle    = "LOCALAE",
                               const std::string& theiraetitle = "ANYAE");
    static
    bool     findOneShot      (DCMTag& attrs, DCMTag& results, int level,
                               const std::string& targ, short port=104,
                               const std::string& myaetitle    = "LOCALAE",
                               const std::string& theiraetitle = "ANYAE");
    static
    bool     moveOneShot      (DCMTag& attrs, const std::string targetae,
                               int level,
                               const std::string& targ, short port=104,
                               const std::string& myaetitle    = "LOCALAE",
                               const std::string& theiraetitle = "ANYAE");
    static
    bool     findToSArch      (DCMTag& findresult, SDICOMArch&);
    static std::string
             wadoURI          (const std::string& baseuri,
                               const std::string& studyuid,
                               const std::string& seriesuid,
                               const std::string& instanceuid);
  private:
    class SDICOM_ClientPIMPL;
    SDICOM_ClientPIMPL *PrivateData;
  };

  class SIMU_API SDICOM_Server: public SServer {
  public:
    enum     DIMSE_CMD {DCM_UNKNOWN_CMD=-1,DCM_CSTORE=0x1,DCM_FIND=0x20,
                        DCM_MOVE=0x21, DCM_ECHO=0x30};
    enum     DIMSE_RSP {DCM_SUCCESS=0x0,DCM_CANCEL=0xFE00,DCM_ERROR=0x0211};
    enum     DIMSE_LEV {DCM_PATIENT,DCM_STUDY,DCM_SERIES,DCM_INSTANCE};
             SDICOM_Server           ();
    virtual ~SDICOM_Server           ();
    void     setDiskStoreMode        (bool);
    bool     diskStoreMode           () const;
    void     setTargetStoreDirectory (const std::string&);
    std::string
             targetStoreDirectory    () const;
    std::string
             aetitle                 () const;
    void     setAETitle              (const std::string&);
    void     setRestrictedMode       (bool);
    bool     restrictedMode          () const;
    void     setReceptionCallback    (std::function<void(SDICOM*)>);
    static
    int
             readPDU                 (ssocket_t&,SDICOM_Generic_PDU&);
    static
    int      sendPDU                 (SDICOM_PDU_Serialisable&,
                                      ssocket_t&);
    static DIMSE_CMD
             decodeDIMSECMD          (Data_PDU::accumset_t&,
                                      DIMSE_CMD_TYPE msgid, DCMTag* tags=nullptr);
    static int
             encodeDIMSECMD          (DIMSE_CMD,SDICOM&,std::vector<char>&,
                                      bool hasdata = true);
    static DCMTag 
             extractDIMSEMessage(std::string&);
  protected:
    int      respondDIMSE            (ssocket_t&,Data_PDU::accumset_t&,
                                      DICOM_PDU_TYPE presid, DIMSE_RSP);
    void     handleCStore            (const Associate_PDU& association,
                                      Data_PDU::accumset_t&,
                                      DIMSE_CMD_TYPE msgid);
    virtual
    bool     handleAcceptedPDU       (const Associate_PDU&,
                                      const SDICOM_Generic_PDU&, ssocket_t&,
                                      Associate_PDU&, Data_PDU::accumset_t&,
                                      Data_PDU::accumset_t&);
    void     handleConnection        (ssocket_t&);
    void     termAssociation         (ssocket_t&,
                                      DICOM_PDU_TYPE val = ASSOCS_PDU);
    bool     validateAssociation     (const Associate_PDU&);
    static unsigned short
             genMessageID            ();
  private:
    class SDICOM_ServerPIMPL;
    SDICOM_ServerPIMPL *PrivateData;
  };

}

#endif //SIMULACRUM_DICOM_NET
