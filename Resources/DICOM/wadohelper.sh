#!/bin/bash
# Simple pass-through CGI wrapper to work around j_security_check

PREPAUTHURI='http://somehostname:8080/wado/'
AUTHURI='http://somehostname:8080/wado/j_security_check?j_username=userhere&j_password=somepasshere'
WGET="wget"
TMPDIR="/tmp"
JSESSION="$TMPDIR/wadocookie.$$"
LOGFILE="/dev/null"

# write out the content type (DICOM)
echo "Content-type: application/dicom"
echo ""
#echo $WGET --no-check-certificate --keep-session-cookies --save-cookies=$JSESSION -o /dev/null -O /dev/null "$PREPAUTHURI?$QUERY_STRING"
#echo "<br/>"
# setup authentication
$WGET --no-check-certificate --keep-session-cookies --save-cookies=$JSESSION -o /dev/null -O /dev/null "$PREPAUTHURI?$QUERY_STRING"
# fetch the file to stdout
$WGET --no-check-certificate --load-cookies=$JSESSION --keep-session-cookies -o /dev/null "$AUTHURI&$QUERY_STRING" -O-
# remove the session cookie
rm -f $JSESSION

