/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_SJ2K
#define SIMULACRUM_SJ2K

/*------------------------------------------------------------------------------
 *                          SJ2K File I/O
 * ---------------------------------------------------------------------------*/

#include <Core/sfileio.h>

namespace Simulacrum {

  class SIMU_API SJ2K : public SIO {
  private:
    class SJ2KPIMPL;
    SJ2KPIMPL *PrivateData;
  public:
    enum {J2KStream,J2KEncap} J2KType;
                          SJ2K();
    virtual              ~SJ2K();
    int                   setLocation(const std::string&) override;
    void                  changeLocation(const std::string&) override;
    const SCoordinate&    getExtent() override;
    bool                  isValid() const override;
    const std::string&    getLocation() const override;
    bool                  hasSSpace(const std::string &path = "") override;
    int                   loadSSpace(SSpace&) override;
    int                   storeSSpace(SSpace&) override;
    void                  clear() override;
    std::string           getInfo(const std::string &path = "") override;
    static
    bool                  decodeBuffer(const char*,long unsigned, SSpace&, 
                                       int type = J2KStream);
    static
    bool                  encodeBuffer(const SSpace&, SIO::imgStreamData&,
                                       int type = J2KStream);
    const std::string&    resourceType() override;
  };

}

#endif //SIMULACRUM_SJ2K
