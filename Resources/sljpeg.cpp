/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sljpeg.h"

/*------------------------------------------------------------------------------
 *                          SLJPEG File I/O
 * ---------------------------------------------------------------------------*/

using namespace Simulacrum;

#include <string.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/slockable.h>

#ifndef NOLJPEG
#include <External/libljpeg/jpegless.h>

static unsigned short * promoteRGBtoRGBA(unsigned short * inputr, size_t length) {
  unsigned char * input  = (unsigned char *) inputr;
  unsigned char * output = (unsigned char *) malloc(length * 4);
  if (output) {
    size_t ip = 0;
    size_t op = 0;
    for(size_t p=0; p<length; p++) {
      output[op++] = input [ip+2];
      output[op++] = input [ip+1];
      output[op++] = input [ip  ];
      output[op++] = 0xFF;
      ip+=3;
    }
  }
  return (unsigned short *) output;
}

bool SLJPEG::decodeBuffer_16bittarg(const char* data, long unsigned int length,
                                    short unsigned int depth, SSpace& target) {
  bool retval = false;
  if (depth == 8 || depth == 16) {
    DecompressInfo dcinfo;
    unsigned short *imgdata =
                         JPEGLosslessDecodeImage (data, depth, length, &dcinfo);
    if (imgdata) {
      SCoordinate ressize(2);
      ressize.xy(dcinfo.imageWidth,dcinfo.imageHeight);
      if (dcinfo.numComponents == 1) {
        if (depth == 16)
          target.setNativeSElemType(new BW16SElem(nullptr));
        else
          target.setNativeSElemType(new BW8SElem(nullptr));
      }
      else {
        // RGB data needs to be RGBA data
        if (dcinfo.numComponents == 3) {
          unsigned short * promoted = promoteRGBtoRGBA
                                (imgdata,dcinfo.imageWidth*dcinfo.imageHeight);
          if(promoted) {
            dcinfo.numComponents = 4;
            free(imgdata);
            imgdata = promoted;
          }
          target.setNativeSElemType(new RGBAI32SElem(nullptr));
        }
        else {
          // two channels (or something else) ==> cannot be handled
          std::stringstream outp;
          outp << dcinfo.numComponents;
          SLogger::global().addMessage
                    ("SLJPEG: cannot handle number of columns: " + outp.str());
        }
      }
      target.resize(ressize);
      try {
        target.selemDataStore().useData((SElem::DataSource)imgdata,
                                  ressize.volume()*(depth/8)*dcinfo.numComponents,
                                        true);
        retval = true;
      }
      catch(std::exception& e) {
        SLogger::global().addMessage
               ("SLJPEG: failed on SSpace::useData: " + std::string(e.what()));
        free(imgdata);
        retval = false;
      }
    }
    else
      SLogger::global().addMessage("SLJPEG: general decode error");
  }
  else
    SLogger::global().addMessage
                        ("SLJPEG: unable to decode pixel depth (not 8 or 16).");
  return retval;
}

#else // NOLJPEG built

bool SLJPEG::decodeBuffer_16bittarg(const char*, long unsigned int,
                                    short unsigned int, SSpace&) {
  bool retval = false;
  return retval;
}

#endif

#ifndef NOJPEG // JPEG Decoding
extern "C"{
#define _NJ_INCLUDE_HEADER_ONLY
#include <External/libnanojpeg/nanojpeg.c>
}

bool SLJPEG::decodeBuffer_RegularJPEG(const char* data,long unsigned int length,
                                      SSpace& target) {
  static SLockable threadsafe;
  bool retval = false;
  threadsafe.lock();
  njInit();
  nj_result_t decode_res = njDecode(data,length);
  if (decode_res == NJ_OK) {
    target.resize(SCoordinate(2).xy(njGetWidth(),njGetHeight()));
    int   dcsize = njGetImageSize();
    char *dcdat  = (char*)njGetImage();
    if (njIsColor()) {
      target.setNativeSElemType(new RGBAI32SElem(nullptr));
      SElemSet::Size targdatsz = target.selemDataStore().size();
      memset(target.selemDataStore()[0],0xFFFFFFFF,targdatsz);
      if (targdatsz==(SElemSet::Size)((dcsize/3)*4)) {
        int                dcpos    = 0;
        SElem::DataSource    targpos  = target.selemDataStore()[0];
        SElemSet::Size position = 0;
        RGBAI32SElem       setter(targpos);
        // copy the packed RGB into the RGBA data
        while (position < targdatsz) {
          setter.source(targpos);
          setter.rgb(dcdat[dcpos],dcdat[dcpos+1],dcdat[dcpos+2]);
          position+=4;
          dcpos+=3;
          targpos+=4;
        }
      }
    }
    else {
      target.setNativeSElemType(new BW8SElem(nullptr));
      if ((target.selemDataStore().size() == (SElemSet::Size)dcsize)
          && dcsize > 0) {
        memcpy(target.selemDataStore()[0],dcdat,dcsize);
      }
    }
    retval = true;
  }
  else {
    std::stringstream decerror;
    switch (decode_res) {
      case NJ_NO_JPEG:
        decerror << "Not a JPEG";
        break;
      case NJ_UNSUPPORTED:
        decerror << "Format unsupported by decoder";
        break;
      case NJ_OUT_OF_MEM:
        decerror << "Decoder out of memory";
        break;
      case NJ_INTERNAL_ERR:
        decerror << "Internal decoder error";
        break;
      case NJ_SYNTAX_ERROR:
        decerror << "Syntax error";
        break;
      default:
        decerror << "Unknown decoder error (code: " << decode_res
                 << ")";
        break;
    }
    SLogger::global().addMessage("decodeBuffer_RegularJPEG: error decoding: " +
                                 decerror.str() + ".");
  }
  njDone();
  threadsafe.unlock();
  return retval;
}

#else

bool SLJPEG::decodeBuffer_RegularJPEG(const char*, long unsigned int, SSpace&) {
  return false;
}

#endif
