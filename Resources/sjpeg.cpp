/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*------------------------------------------------------------------------------
 *                          SJPEG File I/O
 * ---------------------------------------------------------------------------*/

#include "sjpeg.h"
#include "sljpeg.h"
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/sprimitives.h>
#include <Core/error.h>
#include <string.h>

using namespace Simulacrum;

class SJPEG::SJPEGPIMPL {
public:
  std::string Path;
  SCoordinate Ext;
};

SJPEG::SJPEG(): PrivateData(new SJPEGPIMPL()) {

}

SJPEG::~SJPEG() {
  delete PrivateData;
}

void SJPEG::refresh(bool depth) {
  SIO::refresh(depth);
}

int SJPEG::setLocation(const std::string& npath) {
  changeLocation(npath);
  return true;
}

void SJPEG::changeLocation(const std::string& npath) {
  PrivateData->Path = npath;
}

const std::string& SJPEG::getLocation() const {
  return PrivateData->Path;
}

const SCoordinate& SJPEG::getExtent() {
  return PrivateData->Ext;
}

static const unsigned      JPEG_PREAMBLE_LENGTH = 2;
static const unsigned char JPEGPreamble[JPEG_PREAMBLE_LENGTH] =
                                                            {0xFF,0xD8};

bool SJPEG::isValid() const {
  bool         res = false;
  std::fstream JPEGFile;
  char         readdata[JPEG_PREAMBLE_LENGTH];
  JPEGFile.open(getLocation(), std::ios::in | std::ios::binary);
  SFile pathtest(getLocation());
  if (JPEGFile.is_open() && (!pathtest.isDIR())) {
    /* Now check the magic post-preamble string */
    JPEGFile.read(readdata,JPEG_PREAMBLE_LENGTH);
    JPEGFile.close();
    if (strncmp(readdata,(const char*)JPEGPreamble,
        JPEG_PREAMBLE_LENGTH) == 0)
      res = true;
    else
      res = false;
  }
  return res;
}

void SJPEG::clear() {
  setLocation("");
}

bool SJPEG::hasSSpace(const std::string&) {
  return isValid();
}

int SJPEG::loadSSpace(SSpace& target) {
  int retval = -1;
  if (isValid()) {
    SFile src(PrivateData->Path);
    std::string filedat = src.toString();
    if ((filedat.size() > 0) &&
        SLJPEG::decodeBuffer_RegularJPEG
                              (&(filedat[0]),filedat.size(), target) ) {
      retval = 0;
      std::string namestr = getLocation();
      SURI nameval(namestr);
      if (nameval.depth() > 0) {
        namestr = nameval.getComponent(nameval.depth()-1);
      }
      target.setName(namestr);
      target.refresh(true);
    }
  }
  return retval;
}

int SJPEG::storeSSpace(SSpace&) {
  int retval = -1;
  throw SimulacrumExceptionGeneric("SJPEG::store not implemented");
  return retval;
}

std::string SJPEG::getInfo(const std::string&) {
  std::string tmpinfo = "JPEG Image";
  tmpinfo += "<br/>";
  tmpinfo += SIO::getInfo();
  return tmpinfo;
}

const std::string& SJPEG::resourceType() {
  static const std::string jjpegresname = "SJPEG Resource";
  return jjpegresname;
}
