/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sjpegls.h"

/*------------------------------------------------------------------------------
 *                          SJPEGLS File I/O
 * ---------------------------------------------------------------------------*/

using namespace Simulacrum;

#include <string.h>
#include <Toolbox/SLogger/slogger.h>

#ifndef NOJPEGLS
#include <External/libcharls/interface.h>

bool SJPEGLS::decodeBuffer(const char* data, long unsigned int length,
                           SSpace& target) {
  bool retval = false;
    JlsParameters info;
    JLS_ERROR error = JpegLsReadHeader(&data[0],length, &info);
  if (error == OK) {
    SCoordinate ressize(2);
    ressize.xy(info.width,info.height);
    if (info.components == 1) {
      if (info.bitspersample == 16)
        target.setNativeSElemType(new BW16SElem(nullptr));
      else
        target.setNativeSElemType(new BW8SElem(nullptr));
    }
    else {
      target.setNativeSElemType(new RGBAI32SElem(nullptr));
    }
    target.resize(ressize);
    size_t datasize = info.bytesperline * info.height;
    SElem::DataSource dataunc = new unsigned char[datasize];
    error = JpegLsDecode(dataunc,datasize,data,length, &info);
    if ( target.selemDataStore().size() == datasize) {
      target.selemDataStore().useData(dataunc,datasize,true);
    }
    else {
      delete[] dataunc;
      dataunc = nullptr;
    }
    if ((error != OK) || (dataunc == nullptr)) {
      SLogger::global().addMessage("SJPEGLS: general decode error");
    }
    else {
      retval = true;
    }
  }
  else {
    SLogger::global().addMessage("SJPEGLS: header decode error");
  }
  return retval;
}

bool SJPEGLS::encodeBuffer(const SSpace& source, SIO::imgStreamData &target,
                           int ratio) {
  bool retval = false;
  SCoordinate const &ext = source.extent();
  memset(&target, 0, sizeof (target));
  if ( ( (ext.getDim() ==2) || ((ext.getDim() ==3)&&(ext[2]==1)) ) &&
      (const_cast<SSpace&>(source).selemDataStore().size()>0)
     ) {
    SElem::Ptr dattype = const_cast<SSpace&>(source).getNativeSElem();
    JlsParameters info = JlsParameters();
    // type exemplars
    BW16SElem       bw16bit(nullptr);
    BW16SignedSElem bw16bitsign(nullptr);
    BW8SElem        bw8bit(nullptr);
    RGBAI32SElem    rgba32bit(nullptr);
    if (typeid(*dattype) == typeid(bw16bit)) {
      info.components    = 1;
      info.bitspersample = 16;
    }
    else if (typeid(*dattype) == typeid(bw16bitsign)) {
      info.components    = 1;
      info.bitspersample = 16;
    }
    else if (typeid(*dattype) == typeid(bw8bit)) {
      info.components    = 1;
      info.bitspersample = 8;
    }
    else if (typeid(*dattype) == typeid(rgba32bit)) {
      info.components    = 4;
      info.bitspersample = 8;
      info.ilv           = ILV_LINE;
    }
    else {
      SLogger::global().addMessage("SJPEGLS: unsupported element type: " + 
                                   std::string(typeid(*dattype).name()));
      return false;
    }
    // regular info
    info.height = ext.y();
    info.width  = ext.x();
    // compression target info
    size_t compsize = (info.height * info.width * 
                       info.components * info.bitspersample);
    if (ratio > 0) {
      compsize =  compsize / ratio;
    }
    else {
      info.allowedlossyerror = 0;
    }
    // create array and result
    target.data     = new char[compsize];
    target.size     = compsize;
    target.bpp      = info.bitspersample;
    target.channels = info.components;
    target.x        = info.width;
    target.y        = info.height;
    // begin compression
    size_t compressedLength;
    JLS_ERROR err = JpegLsEncode(target.data,target.size, &compressedLength,
                                const_cast<SSpace&>(source).selemDataStore()[0],
                            const_cast<SSpace&>(source).selemDataStore().size(),
                                 &info);
    if (err == OK) {
      target.valid = true;
      target.size  = compressedLength; // consider realloc of data?
      retval = true;
    }
    else {
      delete[] target.data;
      target.data = nullptr;
      target.size = 0;
      target.valid = false;
      SLogger::global().addMessage("SJPEGLS: generall encoding error");
    }
  }
  else {
    SLogger::global().addMessage("SJPEGLS: trying encode non-2D SSpace: " +
                                 ext.toString());
  }
  return retval;
}

#else // NOJPEGLS built

bool SJPEGLS::decodeBuffer(const char*, long unsigned int,
                           SSpace&) {
  bool retval = false;
  return retval;
}

bool SJPEGLS::encodeBuffer(const SSpace&, SIO::imgStreamData&, int) {
  bool retval = false;
  return retval;
}

#endif
