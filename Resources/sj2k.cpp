/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sj2k.h"

using namespace Simulacrum;

/*------------------------------------------------------------------------------
 *                          SJ2K File I/O
 *                     (Uses OpenJPEG Library)
 * ---------------------------------------------------------------------------*/

#ifndef NOJ2K
#ifdef _WIN32
// set here to make OpenJPEG behave nicely in Visual Studio
#define OPJ_STATIC
#endif
#include <External/libopenjpeg/openjpeg.h>
#endif
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/error.h>
#include <string.h>

class SJ2K::SJ2KPIMPL {
public:
  std::string           ImagePath;
  SCoordinate           FileExtent;
  bool                  Valid;
  bool                  isPathJ2K(const std::string&);
};

SJ2K::SJ2K(): PrivateData(new SJ2KPIMPL()) {
  clear();
}

SJ2K::~SJ2K(){
  delete PrivateData;
}

static const unsigned      J2K_PREAMBLE_LENGTH = 23;
static const unsigned char J2KPreamble[J2K_PREAMBLE_LENGTH] =
                   {0x00,0x00,0x00,0x0C,0x6A,0x50,0x20,0x20,0x0D,0x0A,0x87,0x0A,
                        0x00,0x00,0x00,0x14,0x66,0x74,0x79,0x70,0x6A,0x70,0x32};

bool SJ2K::SJ2KPIMPL::isPathJ2K(const std::string& j2kpath) {
  bool         res = false;
  std::fstream J2KFile;
  char         readdata[J2K_PREAMBLE_LENGTH];
  J2KFile.open(j2kpath, std::ios::in | std::ios::binary);
  SFile pathtest(j2kpath);
  if (J2KFile.is_open() && (!pathtest.isDIR())) {
    /* Now check the magic post-preamble string */
    J2KFile.read(readdata,J2K_PREAMBLE_LENGTH);
    J2KFile.close();
    if (strncmp(readdata,(const char*)J2KPreamble,J2K_PREAMBLE_LENGTH) == 0)
      res = true;
    else
      res = false;
  }
  return res;
}

int SJ2K::setLocation(const std::string& newfile){
  PrivateData->ImagePath = newfile;
  PrivateData->Valid = PrivateData->isPathJ2K(newfile);
  return PrivateData->Valid;
}

void SJ2K::changeLocation(const std::string& newlocation) {
  PrivateData->ImagePath = newlocation;
}

const SCoordinate& SJ2K::getExtent() {
  return PrivateData->FileExtent;
}

bool SJ2K::isValid() const {
  return PrivateData->Valid;
}

const std::string& SJ2K::getLocation() const {
  return PrivateData->ImagePath;
}

std::string SJ2K::getInfo(const std::string&) {
  std::string tmpinfo = "JPEG2000 Image";
  tmpinfo += "<br/>";
  tmpinfo += SIO::getInfo();
  return tmpinfo;
}

bool SJ2K::hasSSpace(const std::string&) {
  return PrivateData->Valid;
}

int SJ2K::loadSSpace(SSpace& target) {
  int retval = -1;
  if (PrivateData->Valid) {
    SIO::loadSSpace(target);
    SFile j2kfile(getLocation());
    std::stringstream j2kdata;
    j2kfile.toStream(j2kdata);
    if (decodeBuffer
                 (j2kdata.str().c_str(),j2kdata.str().size(),target,J2KEncap)) {
      retval = 0;
      target.refresh(true);
    }
    else
      retval = 2;
  }
  return retval;
}

int SJ2K::storeSSpace(SSpace &target) {
  SIO::loadSSpace(target);
  return -1;
}

void SJ2K::clear() {
  PrivateData->Valid = false;
  PrivateData->ImagePath.clear();
}

#ifndef NOJ2K
bool SJ2K::decodeBuffer(const char* srcdata, long unsigned srcsize,
                                                    SSpace &target, int rtype) {
  bool             success = false;
  opj_dinfo_t      *dinfo       = nullptr;      /* handle to a decompressor */
  opj_event_mgr_t   event_mgr;               /* event manager */
  opj_dparameters_t parameters;              /* decompression parameters */
  opj_cio_t        *cio         = nullptr;
  opj_image_t      *image       = nullptr;
  int              *component   = nullptr;
  SCoordinate       extent(2);
  SCoordinate       counter(2);
  int               pixelmask   = 0;
  OPJ_CODEC_FORMAT  type        = CODEC_J2K;
  if (rtype == J2KEncap)
    type = CODEC_JP2;
  DEBUG_OUT("JP2K: Starting decoding...");
  // initialise event_mgr to null
  for (unsigned i = 0; i < sizeof(opj_event_mgr_t); i++) {
    *((char*)(&event_mgr)+i) = 0;
  }
  /* set decoding parameters to default values */
  opj_set_default_decoder_parameters(&parameters);
  /* get a decoder handle */
  dinfo = opj_create_decompress(type); // CODEC_J2K CODEC_JP2 CODEC_JPT
  /* catch events using our callbacks and give a local context */
  opj_set_event_mgr((opj_common_ptr)dinfo, &event_mgr, stderr);
  /* setup the decoder decoding parameters using user parameters */
  opj_setup_decoder(dinfo, &parameters);
  /* open a byte stream */
  cio = opj_cio_open((opj_common_ptr)dinfo,
                                    (unsigned char*)srcdata, srcsize);
  /* decode the stream and fill the image structure */
  image = opj_decode(dinfo, cio);
  // read the pixels into the Simulacrum selems
  if (image != nullptr) {
    DEBUG_OUT("JP2K: Decoding successful - reading into SSpace...");
    if ((image->numcomps >= 1) && (image->numcomps <= 3)) {
      // determine the image extent (assume all components are the same)
      int components = image->numcomps;
      if (components == 1) {
        int bpp = image->comps[0].prec;
        if (image->comps[0].sgnd) {
          target.setNativeSElemType(new BW16SignedSElem(nullptr));
          if (bpp > 16)
            SLogger::global().addMessage(
                       "WARNING! Cannot handle Signed BPP != 16 in JP2K image.",
                                                            SLogLevels::MEDIUM);
        }
        else {
          target.setNativeSElemType(new BW16SElem(nullptr));
          if (bpp > 16)
            SLogger::global().addMessage("WARNING! Unusual BPP in JP2K image.",
                                         SLogLevels::MEDIUM);
        }
        // configure a default Window-Level
        if (bpp>8) {
          long int wlheur = pow(2.0,(int)bpp);
          target.LUT().genLUT(bpp,wlheur/2,wlheur);
          target.LUT().useWL(true);
        }
      }
      else {
        target.setNativeSElemType(new RGBAI32SElem(nullptr));
      }
      extent.x(image->comps[0].w); extent.y(image->comps[0].h);
      // resize the target image space
      target.resize(extent);
      SElem::Ptr targetselem = target.getNativeSElem(); 
      for (int pos = 0;
           pos < (image->comps[0].w * image->comps[0].h);
           pos++ ) {
        targetselem->source(target.SElemData(counter));
        targetselem->clear();
        targetselem->isSigned(image->comps[0].sgnd);
        for (int comppos = 0; comppos < components; comppos++) {
          // check for safety
          if (!(pos < (image->comps[comppos].w * image->comps[comppos].h))) {
            comppos++;
            continue;
          }
          pixelmask                = ( 1 << image->comps[comppos].prec ) - 1;
          component                = image->comps[comppos].data;
          SElem::Precision newpixel = 0;
          //consider sign-extension
          if (image->comps[comppos].sgnd) {
            newpixel = -1;
            newpixel = newpixel & (~pixelmask);
          }
          newpixel = newpixel | (component[pos] & pixelmask);
          if (components == 1) {
            targetselem->intensity(newpixel);
          }
          else {
            switch (comppos) {
              case 0: // Red
                targetselem->red(newpixel);
                break;
              case 1: // Green
                targetselem->green(newpixel);
                break;
              case 2: // Blue
                targetselem->blue(newpixel);
                break;
              default:
                DEBUG_OUT("JP2K: Ignoring strange channel/component");
                break;
            }
          }
        }
        counter.tabincrement(extent);
      }
      success = true;
      //------------------------------------------------------------------------
    } else {
      // cannot be handled in Simulacrum
      DEBUG_OUT("JP2K: Cannot understand number of components; invalid image.");
    }
    // image resource de-allocation
    opj_image_destroy (image);
  } else {
    DEBUG_OUT("JP2K: Decoding failed.");
  }
  // general resource de-allocation
  opj_destroy_decompress(dinfo);
  opj_cio_close(cio);
  DEBUG_OUT("JP2k: Decoding done.");
  return success;
}
#else
bool SJ2K::decodeBuffer(const char*, long unsigned, SSpace &, int) {
  return false;
}
#endif

bool SJ2K::encodeBuffer(const SSpace&, SIO::imgStreamData&, int) {
  bool result = false;
  return result;
}

const std::string& SJ2K::resourceType() {
  static const std::string j2kresname = "SJ2K Resource";
  return j2kresname;
}

