/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_SPBM
#define SIMULACRUM_SPBM
/*------------------------------------------------------------------------------
 *                          SPBM File I/O
 * ---------------------------------------------------------------------------*/

#include <Core/sfileio.h>

namespace Simulacrum {

    /* The Simulacrum PBM-based file storage  */
  class SIMU_API SPBM : public SIO {
  private:
    class SPBMPIMPL;
    SPBMPIMPL *PrivateData;
  public:
                          SPBM();
    virtual              ~SPBM();
    int                   setLocation(const std::string&) override;
    void                  changeLocation(const std::string&) override;
    const SCoordinate&    getExtent() override;
    bool                  checkType();
    bool                  isValid() const override;
    const std::string&    getLocation() const override;
    int                   loadSSpace(SSpace&) override;
    int                   storeSSpace(SSpace&) override;
    void                  clear() override;
    std::string           getInfo(const std::string &path = "") override;
    const std::string&    resourceType() override;
    void                  refresh(bool) override;
  };

}

#endif //SIMULACRUM_SPBM
