/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_SLJPEG
#define SIMULACRUM_SLJPEG

/*------------------------------------------------------------------------------
 *                          SLJPEG File I/O
 * ---------------------------------------------------------------------------*/

#include <Core/sfileio.h>

namespace Simulacrum {

  class SIMU_API SLJPEG {
  public:
    static
    bool decodeBuffer_16bittarg(const char*,long unsigned length,
                                unsigned short depth, SSpace& target);
    static
    bool decodeBuffer_RegularJPEG(const char*,long unsigned length,
                                  SSpace& target);
  };

}

#endif //SIMULACRUM_SLJPEG
