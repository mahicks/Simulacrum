--==============================================================================
------------------------- Generic Resource Template ----------------------------
--==============================================================================

-- Clear and Initialise the resource (called at creation)
-- No return value
function clear(resource)
  resource:getRoot():NodeName("ResourceTemplateTest")
end

-- RETURNS: true if the path is a valid resource of this type, false otherwise
function isValid(resource)
  return true;
end

-- Load any data that is required for correct behaviour (e.g. meta-data)
-- No return value
function load(resource)

end

-- store any data that is required for correct behaviour (e.g. meta-data)
-- No return value
function store(resource)

end

-- refresh the state of the object, deep->bool (deep or shallow)
-- No return value
function refresh(resource, deep)

end

-- RETURNS: a string containing information about the path within this resource
function getInfo(resource, path)
  return "This is a test piece of resource information."
end

-- RETURNS: a boolean expressing whether or not path contains an SSpace
function hasSSpace(resource, path)
  return true
end

-- RETURNS: a boolean expressing whether the meta-data tree represents and arch
function hasArchive(resource)
  return false
end

-- Loads the SSpace at Path into the target SSpace
-- No return value
function getSSpaceInto(resource, target, path)

end