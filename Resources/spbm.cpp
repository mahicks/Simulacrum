/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "spbm.h"

#include <stdio.h>
#include <stdlib.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Core/error.h>

using namespace Simulacrum;

/*------------------------------------------------------------------------------
 *                          SPBM File I/O
 * ---------------------------------------------------------------------------*/

static const std::string ResourceType = "NetPBM";

class SPBM::SPBMPIMPL {
public:
  std::string           ImagePath;
  SCoordinate           FileExtent;
  static const unsigned VerSpec = 1;
  bool                  Valid;
  unsigned              Type;
  unsigned              PixelMaxVal;
  unsigned              HeaderEndPos;
  std::fstream          SPBMFile;
  bool                  parseheaderline(const std::string &, unsigned);
  bool                  isRGB;
  std::string           ImpName;
  std::ios::pos_type    readHeaders(SAbsTreeNode* comment_target = nullptr);
};

SPBM::SPBM(): PrivateData(new SPBMPIMPL()) {
  PrivateData->ImagePath = "Empty";
  PrivateData->Valid     = false;
}

SPBM::~SPBM(){
  delete PrivateData;
}

std::ios::pos_type SPBM::SPBMPIMPL::readHeaders(SAbsTreeNode* comment_target){
  if (Valid == false) return false;
  else Valid = false; //this image is invalid unless the header is successful
  SPBMFile.open(ImagePath.c_str(), std::ios::in | std::ios::binary);
  std::string headerline;
  size_t firstchar, //to skip over white space
  reallinepos=0; //to store the enumeration of each non-comment line
  //file processing loop (format is simple, so just do it here)
  while ( SPBMFile.good() && reallinepos < 3){ //loop over all lines, until EOH or EOF
    std::getline(SPBMFile,headerline);
    firstchar = headerline.find_first_not_of(" \t\r\n");
    if (firstchar!=headerline.npos){ //line has contents other than wspace
      if (headerline[firstchar] != '#'){
#ifdef DEBUG
        std::cerr << "  Header line: " << &headerline[firstchar] << std::endl;
#endif
        //this line is not a comment
        if (!parseheaderline(headerline, reallinepos)){
          throw SimulacrumIOException();
          return -1;
        }
        reallinepos++; //now parser has at least seen a non-comment line
      }
      else {
        // handle comments
        if (comment_target) {
          if (headerline.size() > firstchar+1) {
            std::string commentstr(headerline,firstchar+1,
                                   headerline.size()-(firstchar+1));
            // catch a name reference
            std::size_t namepos = commentstr.find("Name:");
            if ( namepos != std::string::npos)
              ImpName = std::string(commentstr,namepos+5,commentstr.size()-5);
            SAbsTreeNode &newcmntnode = comment_target->NewChild();
            newcmntnode.NodeName("Comment");
            newcmntnode.NodeValue(commentstr);
          }
        }
      }
    }
  }
  DEBUG_OUT("Finished fetching loop");
  Valid = true;
  std::ios::pos_type tempgp = SPBMFile.tellg();
  SPBMFile.close();
  switch (Type) {
  /* the following are all ASCII variants -- not supported */
    case 1:
    case 2:
    case 3:
    case 4:
      Valid = false;
      break;
    case 5:
      isRGB = false;
      break;
    case 6:
      isRGB = true;
      break;
    default:
      isRGB = false;
      break;
  }
#ifdef DEBUG
  std::cerr << "  Headers read (from " << ImagePath << "):" << std::endl;
  std::cerr << "  Type level: " << Type << std::endl;
  std::cerr << "  File Extent: " << FileExtent.toString() << std::endl;
  std::cerr << "  Pixel MaxVal: " << PixelMaxVal << std::endl;
  std::cerr << "  End Headers." << std::endl;
#endif
  return tempgp;
}

/* Example Net PBM Header:
 * -----------
 * P3
 * # The P3 means colors are in ASCII, then 3 columns and 2 rows,
 * # then 255 for max color, then RGB triplets
 * 3 2
 * 255
 * <data>
 * -----------
 */

bool SPBM::SPBMPIMPL::parseheaderline(const std::string & hline, unsigned hpos){
  if(hline.length() < 1) return false; //should never get an empty string
  switch (hpos){
    default:
      return false;
      break;
    case 2: //third line, should specify the maximum value on each pixel
      DEBUG_OUT("Getting max pixexval");
      PixelMaxVal = atoi(hline.c_str());
      DEBUG_OUT("Got max pixelval");
      break;
    case 0: //first line, should specify image type
      DEBUG_OUT("Getting P Value");
      if (hline.length() < 2 ||
          hline[hline.find_first_not_of(" \t\r\n")] != 'P') //check 'P' char
        return false;
      else
        Type = atoi(&hline.c_str()[1]); //skip the 'P' and extract the number
      DEBUG_OUT("Got P Value");
      break;
    case 1: //second line, should specify image extent (space separate dims)
      DEBUG_OUT("Getting Extent");
      //use spaces to calculate the proposed dimensionality
      unsigned strpos=0;
      SCoordinate::Precision dimcount=1; //start dimcount at one, since that is mindim
      std::string dimstr = ""; //a string to store dimension values
      for(strpos=0; strpos<hline.length(); strpos++)
        if(hline[strpos] == ' ') dimcount++;
      FileExtent.setDim(dimcount);
      //now extract each dimension value (separate from above for clarity)
      strpos=0;
      for (dimcount=0; dimcount<FileExtent.getDim(); dimcount++){
        while(strpos < hline.length())
          if (hline[strpos] != ' ' && strpos != (hline.length()-1) ){
            dimstr+=hline[strpos]; //add cars to string
            strpos++;
          }
          else{ //it's a space (or end of line), so set the extent dimension
            //include final char if it's eol
            if(strpos == (hline.length()-1)) dimstr+=hline[strpos];
            FileExtent.setCoord(dimcount,atoi(dimstr.c_str()));
            dimstr = ""; //reset string
            strpos++;
            break;
          }
      }
      DEBUG_OUT("Got Extent");
      break;
  }
  return true;
}

int SPBM::setLocation(const std::string& newfile){
  PrivateData->ImagePath = newfile;
  PrivateData->Valid = checkType();
  if (PrivateData->Valid)
    PrivateData->HeaderEndPos = PrivateData->readHeaders();
  return PrivateData->Valid;
}

void SPBM::changeLocation(const std::string& newlocation) {
  PrivateData->ImagePath = newlocation;
}

const SCoordinate& SPBM::getExtent() {
  return PrivateData->FileExtent;
}

bool SPBM::checkType() {
  char magicstr[2] = {'!','!'};
  bool validity = false;
  PrivateData->SPBMFile.open
              (PrivateData->ImagePath.c_str(), std::ios::in | std::ios::binary);
  if (PrivateData->SPBMFile.is_open()) {
    PrivateData->SPBMFile.read(magicstr,2);
    // NOTE: accept P5 and P6 netpbm formats only
    if (magicstr[0] == 'P' && ((magicstr[1] == '5')||(magicstr[1] == '6')))
      validity = true;
  }
  PrivateData->SPBMFile.close();
  return validity;
}


bool SPBM::isValid() const {
  return PrivateData->Valid;
}

const std::string& SPBM::getLocation() const {
  return PrivateData->ImagePath;
}

int SPBM::loadSSpace(SSpace& targetimage){
  if ( isValid() ){
    targetimage.reset();
    PrivateData->HeaderEndPos =
                       PrivateData->readHeaders(&targetimage.informationNode());
    targetimage.resize(this->PrivateData->FileExtent);
    PrivateData->SPBMFile.open
              (PrivateData->ImagePath.c_str(), std::ios::in | std::ios::binary);
    PrivateData->SPBMFile.seekg(PrivateData->HeaderEndPos);
    this->readTabulatedData(targetimage,PrivateData->SPBMFile,
                            this->maxvaltobytewidth(PrivateData->PixelMaxVal),
                            PrivateData->isRGB, false,&stop());
    PrivateData->SPBMFile.close();
    SIO::loadSSpace(targetimage);
    targetimage.setName(PrivateData->ImpName);
    targetimage.refresh(true);
    return 0;
  } else{
    throw SimulacrumIOException();
    return -1;
  }
}

int SPBM::storeSSpace(SSpace& sourceimage){
  // For now, just write rgb data, from a 2D sspace; otherwise explode
  int retval = 0;
  std::stringstream tmpout;
  SFile outfile(getLocation());
  SSpace RGBAIMG;
  sourceimage.get2DRGBAInto(RGBAIMG);
  const SCoordinate &srcextent = RGBAIMG.extent();
  if (srcextent.getDim() == 2) {
    // write the simple fixed header here
    tmpout << "P6" << "\n" 
           << "# Simulacrum PPM" << "\n"
           << "# Name: " << sourceimage.getName() << "\n"
           << "# WL-Centre: " 
                          << sourceimage.getSourceSSpace().LUT().getWLCentre()
                          << "\n"
           << "# WL-Width: "  
                          << sourceimage.getSourceSSpace().LUT().getWLWidth()
                          << "\n"
           << "# System: " << sysInfo::systemName() << "\n"
           << "# Generated: " << sysInfo::timeString(sysInfo::time()) << "\n"
           << "# ID: " << sysInfo::genGUIDString() << "\n"
           << srcextent.x() << " " << srcextent.y() << "\n"
           << "255" << "\n";
    SIO::writeTabulatedData(RGBAIMG,tmpout,1,true);
    outfile.fromStream(tmpout);
    SIO::storeSSpace(RGBAIMG);
  }
  else
    retval = -1;
  return retval;
}

void SPBM::clear() {
  PrivateData->ImpName.clear();
}

std::string SPBM::getInfo(const std::string&) {
  std::string tmpinfo = resourceType() + " Image (";
  if (PrivateData->isRGB)
    tmpinfo += "RGB";
  else
    tmpinfo += "Greyscale";
  tmpinfo += ")<br/>";
  tmpinfo += SIO::getInfo();
  return tmpinfo;
}

const std::string& SPBM::resourceType() {
  return ResourceType;
}

void SPBM::refresh(bool ) {
  if (PrivateData->Valid) {
    getRoot().clearChildren();
    PrivateData->HeaderEndPos = PrivateData->readHeaders(&getRoot());
  }
}

/*------------------------------------------------------------------------------
 *                         ---------END---------
 * ---------------------------------------------------------------------------*/
