--==============================================================================
-------------------------- Generic Volume Loader -------------------------------
--==============================================================================
SLua.SLogger_global():addMessage("SLua ProstateMRI Study Loader")

--==============================================================================
--------------------------- Modifiable Structures ------------------------------
--==============================================================================

-- Records of standard Lua patterns for recognised volumes to be loaded.
-- Add a new record and pattern(s) to have it loaded (optionally, add a special
-- loading function too, to the VolLoaders table)
VolPatterns          = {} -- empty declaration
VolPatterns["T2Cor"] = { "t2_tse_cor", "COR T2", "T2W_TSE_cor" }
VolPatterns["T2Sag"] = { "t2_tse_sag", "SAG T2", "Sag T2", "T2W_TSE_sag" }
VolPatterns["T2Tra"] = { "3D PD SPGR", "t2_tse_tra", "AX T2", "T2W_TSE_ax" }
VolPatterns["ADC"]   = { "ADC", "Apparent Diffusion Coefficient" }
VolPatterns["DWI"]   = { "BVAL" , "_DWI", "_dwi", "Exponential Apparent", "DWI" }
VolPatterns["Dyn"]   = { "tfl_dyn", "tfl_3d dyn", "t1_twist_tra", "_dynamic_", "itfl_tra", ":TRICKS DYNA", ":DYN", "THRIVE_DYN" }
-- VolPatterns["KTran"] = { "KTrans" }

-- The node level on which we should operate (one level below contains the vols)
NodeLevelName = "StudyInstanceUID"

-- Record for volumes requiring special loader functions
-- Special methods should enter a function in the table with the same name as
-- as the volume that they load, with the same parameters as getVol.
VolLoaders           = {} -- empty declaration (add functions at declaration)

-- Some useful constants
PatNameAttr = SLua.DCMDataDic_toIDStr("PatientName")
AccsNumAttr = SLua.DCMDataDic_toIDStr("AccessionNumber")
SerDescAttr = SLua.DCMDataDic_toIDStr("SeriesDescription")
DynOrder    = { "TemporalPositionIdentifier", "AcquisitionTime" }
DynPoints   = "NumberOfTemporalPositions"

-- Return a name to represent the functions of this loader
function loaderName()
  return "Prostate MRI"
end

--==============================================================================
----------------------------- Layout Method ------------------------------------
--==============================================================================

-- Recommend a layout for the above set of SSaces
function layoutRecommendation(resource,path)
  local result = ""
  if (resource:getRoot():NodePathExists(path)) then
    local node = resource:getRoot():NodeByPath(path)
    if (node:NodeName() == NodeLevelName) then
      local namesuffix = ": " .. sspaceListTitle(resource,path)
      namesuffix = string.gsub( namesuffix , "%s+", "")
      result = "<hsplit border=\"0\" size1=\"892\" size2=\"501\"><vsplit border=\"0\" precedent=\"1\" size1=\"550\" size2=\"549\"><hsplit border=\"0\" precedent=\"1\" size1=\"445\" size2=\"444\"><viewportslicer vpconf=\"2x\" precedent=\"1\" name=\"Dyn" .. namesuffix .. "\" /><viewportslicer vpconf=\"2x\" name=\"ADC" .. namesuffix .. "\" /></hsplit><hsplit border=\"0\" size1=\"445\" size2=\"444\"><viewportslicer precedent=\"1\" vpconf=\"2x\" name=\"T2Sag" .. namesuffix .. "\" /><viewportslicer vpconf=\"2x\" name=\"DWI" .. namesuffix .. "\" /></hsplit></vsplit><viewportslicer vpconf=\"3x\" name=\"T2Tra" .. namesuffix .. "\" /></hsplit>"
    end
  end
  return result
end

--==============================================================================
---------------------------- Special Loaders -----------------------------------
--==============================================================================

-- Special loading function for Dynamic Series
VolLoaders.Dyn = 
function(resource, volname, nodevector, volpatternsl, targetspace)
  local objnum  = nodevector:size()
  local dynordf = false
  local dynord  = ""
  if ( not ((volpatternsl == nil) or (objnum == nil)) ) then
    -- first, pull out the dyn series
    local dynnodes = {}
    targetspace:progress(-1) -- set an uncertain progress
    for obj=0,objnum-1,1 do
      for pat=1,#volpatternsl,1 do
        if nodevector[obj]:hasAttribute(SerDescAttr) then
          local SeriesDesc = nodevector[obj]:getAttribute(SerDescAttr)
          if not (string.find( SeriesDesc, volpatternsl[pat]) == nil) then
            if resource:hasSSpace(nodevector[obj]:NodePath(true)) then
              local newsspace = SLua.SSpace()
              resource:getSSpaceInto(newsspace,nodevector[obj]:NodePath(true))
              -- Find an ordinal value only on the first sort
              if not dynordf then
                for ord=1,#DynOrder,1 do
                  if newsspace:informationNode():hasChildNode(DynOrder[ord]) then
                    dynordf = true
                    dynord  = DynOrder[ord]
                    break
                  end
                end
              end
              -- Add to map with ordinal value
              if newsspace:informationNode():hasChildNode(dynord) then
                local ordinal =
                 newsspace:informationNode():getChildNode(dynord):NodeValue()
                dynnodes[ordinal] = newsspace
              else
                SLua.SLogger_global():addMessage("MRIResLoader: Cannot find Dyna ordinal value: " .. dynord)
              end
            else
              targetspace:setName("DataNotPresent")
            end
          end
        end
      end
      targetspace:progress(-1) -- ensure it understand sspace is still waiting
      targetspace:progress(((obj+1)/objnum)*100) -- actual progress
    end
    targetspace:progress(100) -- ensure completion is sent
    -- sort, by ordinal
    local sorteddynnodes = {}
    for n in pairs(dynnodes) do table.insert(sorteddynnodes,n) end
    table.sort(sorteddynnodes)
    -- consolidate, into 4D SSpace
    local accumvector = SLua.SSpaceVector()
    local refcoord    = nil
    for i,n in ipairs(sorteddynnodes) do
      if refcoord == nil then
        refcoord = dynnodes[n]:extent()
      end
      if refcoord == dynnodes[n]:extent() then
        accumvector:push_back(dynnodes[n])
      else
        SLua.SLogger_global():addMessage("MRIResLoader: " ..
                                         "Skipping DynSeries " ..
                                         "(Extent Mismatch): " ..
                                         dynnodes[n]:getName())
      end
    end
    local conres = targetspace:concatenate(accumvector)
    -- handle single-series dyna objects (by reinterpreting the extent)
    if accumvector:size() == 1 then -- check for single-series dyna
      if targetspace:extent():getDim() == 3 then
        if targetspace:informationNode():hasChildNode(DynPoints) then
          local numofpoints = targetspace:informationNode():getChildNode(DynPoints):NodeValue()
          local newext      = targetspace:extent()
          local newz        = newext:z() / numofpoints
          newext:setDim(4)
          newext:z(newz)
          newext:t(numofpoints)
          targetspace:reinterpretExtent(newext)
        else
          SLua.SLogger_global():addMessage("MRIResLoader: Cannot find number of time positions: " .. DynPoints)
        end
      end
    end
    -- assign expected volume name
    targetspace:setName(volname)
  end
end

--==============================================================================
--*********************** Less Sensible to Modify... ***************************
--==============================================================================

-- Returns true in the presence of a named volume
function hasVol(nodevector, volpatternsl)
  local objnum = nodevector:size()
  if ( not ((volpatternsl == nil) or (objnum == nil)) ) then
    for obj=0,objnum-1,1 do
      for pat=1,#volpatternsl,1 do
        if nodevector[obj]:hasAttribute(SerDescAttr) then
          local SeriesDesc = nodevector[obj]:getAttribute(SerDescAttr)
        if not (string.find( SeriesDesc, volpatternsl[pat]) == nil) then
          return true
        end
        end
      end
    end
  end
  return false
end

-- Loads a volume into a named SSpace, if it is available
function getVol(resource, volname, nodevector, volpatternsl, targetspace)
  if not (VolLoaders[volname] == nil) then -- use a special loader
    VolLoaders[volname](resource,volname, nodevector, volpatternsl, targetspace)
  else -- the regular method
    local objnum = nodevector:size()
    if ( not ((volpatternsl == nil) or (objnum == nil)) ) then
      for obj=0,objnum-1,1 do
        for pat=1,#volpatternsl,1 do
          if nodevector[obj]:hasAttribute(SerDescAttr) then
            local SeriesDesc = nodevector[obj]:getAttribute(SerDescAttr)
            if not (string.find( SeriesDesc, volpatternsl[pat]) == nil) then
              if resource:hasSSpace(nodevector[obj]:NodePath(true)) then
                resource:getSSpaceInto(targetspace,
                                       nodevector[obj]:NodePath(true))
                targetspace:setName(volname)
              else
                targetspace:setName("DataNotPresent")
              end
            end
          end
        end
      end
    end
  end
end

-- Return a proposed title for the path requested inside the resource
-- (a string containing the PatientName and AccessionNumber)
function sspaceListTitle(resource,path)
  local retval  = "Untitled"
  if (resource:getRoot():NodePathExists(path)) then
    local reqnode = resource:getRoot():NodeByPath(path)
    if (reqnode:NodeName() == NodeLevelName) and
        reqnode:hasAttribute(PatNameAttr) and
        reqnode:hasAttribute(AccsNumAttr) then
      retval = reqnode:getAttribute(PatNameAttr) ..
               " - " ..
               reqnode:getAttribute(AccsNumAttr)
    else
      retval = reqnode:NodeValue()
    end
  end
  return retval
end

-- Add strings to a result list which names the available volumes
function sspaceList(resource,path,resultlist)
  if (resource:getRoot():NodePathExists(path)) then
    local node     = resource:getRoot():NodeByPath(path)
    if (node:NodeName() == NodeLevelName) then
      local nodelist = node:NodeChildren(true)
      for volname,volmatch in pairs(VolPatterns) do
        if ( hasVol(nodelist, volmatch) ) then
          resultlist:push_back(volname)
        end
      end
    else
      if resource:hasSSpace(node:NodePath(true)) then
        resultlist:push_back(node:NodeValue())
      end
    end
  end
end

-- Add the loaded SSpace volumes into a list of the SAME SIZE as the sspaceList
function getSSpaceInto(resource,path,sspacename,targetspace)
  if (resource:getRoot():NodePathExists(path)) then
    local node     = resource:getRoot():NodeByPath(path)
    if (node:NodeName() == NodeLevelName) then
      local nodevector = node:NodeChildren(true)
      if ( (not(VolPatterns[sspacename] == nil)) and
            hasVol(nodevector,VolPatterns[sspacename]) ) then
        getVol(resource, sspacename, nodevector, VolPatterns[sspacename],
               targetspace)
        local tmpstr = targetspace:getName() .. ": " ..
                       sspaceListTitle(resource,path)
        tmpstr = string.gsub( tmpstr , "%s+", "")
        targetspace:setName( tmpstr )
      end
    else
      if resource:hasSSpace(node:NodePath(true)) then
        resource:getSSpaceInto(targetspace,node:NodePath(true))
      end
    end
  end
end
