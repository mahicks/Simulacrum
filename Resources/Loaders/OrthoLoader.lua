--==============================================================================
--------------------------- Generic Ortho Loader -------------------------------
--==============================================================================

-- Return a name to represent the functions of this loader
function loaderName()
  return "Ortho Layout"
end

-- Recommend a layout for the above set of SSaces
function layoutRecommendation(resource,path)
  local result = ""
  if (resource:getRoot():NodePathExists(path) and
      resource:hasSSpace(path)) then
    local node   = resource:getRoot():NodeByPath(path)
    local vpname = node:NodeValue()
    result = "<hsplit border=\"1\" size1=\"600\" size2=\"240\"><viewportslicer precedent=\"1\" name=\"" .. vpname .. "\" /><vsplit border=\"1\" size1=\"418\" size2=\"204\"><vsplit border=\"1\" precedent=\"1\" size1=\"207\" size2=\"207\"><viewportslicer precedent=\"1\" vpconf=\"nohud;\" name=\"" .. vpname .. "\" /><viewportslicer spconf=\"1;0;1;1;0;0;0;0;1;\" vpconf=\"nohud;\" name=\"" .. vpname .. "\" /></vsplit><viewportslicer spconf=\"0;1;1;0;0;1;0;1;0;\" vpconf=\"nohud;\" name=\"" .. vpname .. "\" /></vsplit></hsplit>"
  end
  return result
end

-- Return a proposed title for the path requested inside the resource
function sspaceListTitle(resource,path)
  local retval  = "Untitled"
  if (resource:getRoot():NodePathExists(path)) then
    local reqnode = resource:getRoot():NodeByPath(path)
    retval = reqnode:NodeValue()
  end
  return retval
end

-- Add strings to a result list which names the available volumes
function sspaceList(resource,path,resultlist)
  if (resource:getRoot():NodePathExists(path)) then
    local node     = resource:getRoot():NodeByPath(path)
    if resource:hasSSpace(node:NodePath(true)) then
      resultlist:push_back(node:NodeValue())
    end
  end
end

-- Add the loaded SSpace volumes into a list of the SAME SIZE as the sspaceList
function getSSpaceInto(resource,path,sspacename,targetspace)
  resource:getSSpaceInto(targetspace,path)
  if (resource:getRoot():NodePathExists(path)) then
    local reqnode = resource:getRoot():NodeByPath(path)
    targetspace:setName(reqnode:NodeValue())
  end
end
