/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_JPEGLS
#define SIMULACRUM_JPEGLS

/*------------------------------------------------------------------------------
 *                          JPEGLS File I/O
 * ---------------------------------------------------------------------------*/

#include <Core/sfileio.h>

namespace Simulacrum {

  class SIMU_API SJPEGLS {
  public:
    static
    bool decodeBuffer(const char*,long unsigned length,SSpace& target);
    static
    bool encodeBuffer(const SSpace&, SIO::imgStreamData&, int ratio = 4);
  };

}

#endif //SIMULACRUM_JPEGLS
