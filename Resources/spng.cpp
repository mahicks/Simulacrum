/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*------------------------------------------------------------------------------
 *                          SPNG File I/O
 * ---------------------------------------------------------------------------*/

#include "spng.h"
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/sprimitives.h>
#include "spbm.h"
#ifndef NOPNG
#include <External/liblodepng/lodepng.h>
#endif
#include <string.h>

using namespace Simulacrum;

class SPNG::SPNGPIMPL {
public:
  std::string Path;
  SCoordinate Ext;
};

SPNG::SPNG(): PrivateData(new SPNGPIMPL()) {

}

SPNG::~SPNG() {
  delete PrivateData;
}

void SPNG::refresh(bool depth) {
  SIO::refresh(depth);
}

int SPNG::setLocation(const std::string& npath) {
  changeLocation(npath);
  return true;
}

void SPNG::changeLocation(const std::string& npath) {
  PrivateData->Path = npath;
}

const std::string& SPNG::getLocation() const {
  return PrivateData->Path;
}

const SCoordinate& SPNG::getExtent() {
  return PrivateData->Ext;
}

static const unsigned      PNG_PREAMBLE_LENGTH = 8;
static const unsigned char PNGPreamble[PNG_PREAMBLE_LENGTH] =
                                      {0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A};

bool SPNG::isValid() const {
  bool         res = false;
  std::fstream PNGFile;
  char         readdata[PNG_PREAMBLE_LENGTH];
  PNGFile.open(getLocation(), std::ios::in | std::ios::binary);
  SFile pathtest(getLocation());
  if (PNGFile.is_open() && (!pathtest.isDIR())) {
    /* Now check the magic post-preamble string */
    PNGFile.read(readdata,PNG_PREAMBLE_LENGTH);
    PNGFile.close();
    if (strncmp(readdata,(const char*)PNGPreamble,PNG_PREAMBLE_LENGTH) == 0)
      res = true;
    else
      res = false;
  }
  return res;
}

void SPNG::clear() {
  setLocation("");
}

bool SPNG::hasSSpace(const std::string&) {
  return isValid();
}

int SPNG::loadSSpace(SSpace& target) {
  int retval = -1;
  if (isValid()) {
    SFile src(PrivateData->Path);
    std::string filedat = src.toString();
    if ((filedat.size() > 0) &&
        decodeBuffer(&(filedat[0]),filedat.size(), target) ) {
      retval = 0;
      std::string namestr = getLocation();
      SURI nameval(namestr);
      if (nameval.depth() > 0) {
        namestr = nameval.getComponent(nameval.depth()-1);
      }
      target.setName(namestr);
      target.refresh(true);
    }
  }
  return retval;
}

int SPNG::storeSSpace(SSpace& source) {
  int retval = -1;
  SIO::imgStreamData encdat;
  memset((void*)&encdat,0,sizeof(encdat));
  // check it is RGBA, extract if not
  SElem::Ptr        dattype = source.getNativeSElem();
  RGBAI32SElem  rgba32bit(nullptr);
  SSpace        converted;
  source.get2DRGBAInto(converted);
  if (encodeBuffer(converted,encdat)) {
    SFile outf(getLocation());
    if(outf.fromString(std::string(encdat.data,encdat.size))) {
      retval = 0;
    }
  }
  if (encdat.data != nullptr) {
    free(encdat.data);
  }
  return retval;
}

std::string SPNG::getInfo(const std::string&) {
  std::string tmpinfo = "PNG Image";
  tmpinfo += "<br/>";
  tmpinfo += SIO::getInfo();
  return tmpinfo;
}

const std::string& SPNG::resourceType() {
  static const std::string jpngresname = "SPNG Resource";
  return jpngresname;
}

#ifndef NOPNG

bool SPNG::decodeBuffer(const char* dat, long unsigned int sz,
                        SSpace& targ, int) {
  bool retval = false;
  unsigned char* pixels = nullptr;
  unsigned       width  = 0;
  unsigned       height = 0;
  if (!lodepng_decode32(&pixels,&width,&height,
                             (unsigned char*)dat,sz)) {
    // fixup RGBA <-> ABGR
    SElemSet::Size datsize = width*height*4;
    RGBAI32SElem       px(nullptr);
    unsigned char      cr,cg,cb;
    for (size_t p=0; p<(datsize);p+=4) {
      px.source((SElem::DataSource)pixels+p);
      cr = *(pixels+p);
      cg = *(pixels+p+1);
      cb = *(pixels+p+2);
      px.rgb(cr,cg,cb);
    }
    targ.setNativeSElemType(new RGBAI32SElem(nullptr));
    targ.resize(SCoordinate(2).xy(width,height));
    targ.selemDataStore().useData((SElem::DataSource)pixels,datsize);
    retval = true;
  }
  else {
    SLogger::global().addMessage("SPNG::decodeBuffer: general decode error");
  }
  return retval;
}

bool SPNG::encodeBuffer(SSpace& source, SIO::imgStreamData& target, int) {
  bool retval = false;
  SCoordinate const &ext = source.extent();
  memset(&target, 0, sizeof (target));
  SElemSet &datast  = const_cast<SSpace&>(source).selemDataStore();
  if ( ( (ext.getDim() ==2) || ((ext.getDim() ==3)&&(ext[2]==1)) ) &&
      (datast.size()>0) ) {
    SElem::Ptr          dattype = const_cast<SSpace&>(source).getNativeSElem();
    RGBAI32SElem    rgba32bit(nullptr);
    SElem::DataSource datasrc = datast[0];
    if (typeid(*dattype) == typeid(rgba32bit)) {
      std::vector<unsigned char> outpix(datast.size(),0xFF);
      // fixup RGBA <-> ABGR
      for (SElemSet::Size p=0; p<outpix.size(); p+=4) {
        outpix[p]   = *( datasrc + p + 2 );
        outpix[p+1] = *( datasrc + p + 1 );
        outpix[p+2] = *( datasrc + p + 0 );
        outpix[p+3] = *( datasrc + p + 3 );
      }
      // encode the new data
      if (! lodepng_encode32((unsigned char**)(&target.data),&target.size,
                             &outpix[0],ext.x(),ext.y()) ) {
        target.bpp = 8;
        target.channels = 3;
        target.x = ext.x();
        target.y = ext.y();
        target.valid = true;
        retval = true;
      }
      else {
        SLogger::global().addMessage
                                   ("SPNG::decodeBuffer: general encode error");
      }
    }
    else {
      SLogger::global().addMessage("SPNG: unsupported element type");
      return false;
    }
  }
  return retval;
}

#else
bool SPNG::decodeBuffer(const char*, long unsigned int, SSpace&, int) {
  return false;
}

bool SPNG::encodeBuffer(SSpace&, SIO::imgStreamData&, int) {
  return false;
}
#endif
