/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SLogger
 * Implementation for SLogger (simple threadsafe logger)
 * Author: M.A.Hicks
 */
#include "slogger.h"
#include <Toolbox/sysInfo/sysInfo.h>
#include <Core/error.h>

#include <iostream>
#include <sstream>

using namespace Simulacrum;

class SLogger::SLoggerPIMPL: public SLockable {
public:
  std::string       Title;
  SRingBuffer       CoreRing;
  SPool             MessageQueue;
  std::ostream     *OutStream;
  void             _addText(SLogger*,const std::string, SLogLevel);
  unsigned long     SequenceNumber;
  SLoggerPIMPL(): CoreRing(32 * 1024), MessageQueue(1), OutStream(nullptr),
                                                             SequenceNumber(0){}
  virtual ~SLoggerPIMPL() {}
};

SLogger::SLogger(): PrivateData(new SLoggerPIMPL()) {
  setTitle("Simulacrum Log");
}

SLogger::~SLogger() {
  delete PrivateData;
}

void SLogger::setTitle(const std::string& newtitle) {
  PrivateData->Title = newtitle;
}

std::string SLogger::getTitle() {
  return PrivateData->Title;
}

void SLogger::SLoggerPIMPL::_addText(SLogger* targ, const std::string msg,
                                     SLogLevel msglevel) {
  lock();
  for (unsigned i=0; i < msg.size(); i++)
    CoreRing.add(msg[i]);
  if (OutStream != nullptr && (msglevel > SLogLevels::DEBUGL))
    (*OutStream) << msg;
  unlock();
  if (msglevel > SLogLevels::MEDIUM)
    targ->refresh(true);
  else
    targ->refresh(false);
}

void SLogger::addText(const std::string& newmsg, SLogLevel ll) {
  if (ll == SLogLevels::HIGH_SYNC)
    PrivateData->_addText(this,newmsg,ll);
  else
    PrivateData->MessageQueue.addJob
      (std::bind(&SLogger::SLoggerPIMPL::_addText,PrivateData,this,newmsg,ll));
}

void SLogger::addLine(const std::string& newmsg, SLogLevel ll) {
  std::stringstream newlsteam;
  newlsteam << std::endl;
  addText(newmsg + newlsteam.str(), ll);
}

void SLogger::addMessage(const std::string& newmsg, SLogLevel mlev) {
  std::string finalmsg = "|" + time() + "| "
                              + newmsg;
  addLine(finalmsg,mlev);
}

SLogger& SLogger::operator<<(const std::string& newmsg) {
  addText(newmsg);
  return *this;
}

SLogger& SLogger::operator<<(const long int newval) {
  std::stringstream newmsg;
  newmsg << newval;
  addText(newmsg.str());
  return *this;
}

SLogger& SLogger::operator<<(const double newval) {
  std::stringstream newmsg;
  newmsg << newval;
  addText(newmsg.str());
  return *this;
}

void SLogger::clear() {
  PrivateData->lock();
  PrivateData->CoreRing.reset();
  PrivateData->unlock();
}

void SLogger::setStream(std::ostream& newstream) {
  PrivateData->lock();
  PrivateData->OutStream = &newstream;
  PrivateData->unlock();
}

void SLogger::clearStream() {
  PrivateData->OutStream = nullptr;
}

std::string SLogger::getLog() {
  PrivateData->lock();
  PrivateData->CoreRing.resetGet();
  PrivateData->unlock();
  return getNew();
}

std::string SLogger::getNew() {
  std::string retval;
  PrivateData->lock();
  retval.reserve(PrivateData->CoreRing.usedSize());
  while (PrivateData->CoreRing.hasMore())
    retval.push_back(PrivateData->CoreRing.get());
  PrivateData->unlock();
  return retval;
}

void SLogger::waitPending() {
  PrivateData->MessageQueue.wait();
}

SLogger& SLogger::global() {
  static SLogger globallogger;
  return globallogger;
}

std::string SLogger::time() {
  return sysInfo::timeString(sysInfo::localTime());
}

long unsigned int SLogger::genSequenceNumber() {
  PrivateData->lock();
  return PrivateData->SequenceNumber++;
  PrivateData->unlock();
}

std::ostream& operator <<(std::ostream & os, SLogger & log) {
  os << log.getNew();
  return os;
}
