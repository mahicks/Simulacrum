/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SLogger
 * Interface specification for SLogger (simple threadsafe logger)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SLOGGER
#define SIMULACRUM_SLOGGER

#include <Core/slockable.h>
#include <Core/sconnectable.h>
#include <Toolbox/SRing/sring.h>
#include <Core/SPool/SPool.h>

namespace Simulacrum {

  typedef int SLogLevel;

  class SLogLevels {
  public:
    static const SLogLevel DEBUGL    = 0;
    static const SLogLevel LOW       = 1;
    static const SLogLevel MEDIUM    = 2;
    static const SLogLevel HIGH      = 3;
    static const SLogLevel HIGH_SYNC = 4;
  };

  class SIMU_API SLogger: public SConnectable {
  private:
    class SLoggerPIMPL;
    SLoggerPIMPL *PrivateData;
  public:
                SLogger();
    virtual    ~SLogger();
    void        setTitle(const std::string&);
    std::string getTitle();
    void        addText(const std::string&, SLogLevel = SLogLevels::LOW);
    void        addLine(const std::string&, SLogLevel = SLogLevels::LOW);
    void        addMessage(const std::string&, SLogLevel = SLogLevels::LOW);
    SLogger&    operator<<(const std::string&);
    SLogger&    operator<<(const long int);
    SLogger&    operator<<(const double);
    void        clear();
    void        setStream(std::ostream&);
    void        clearStream();
    std::string getLog();
    std::string getNew();
    void        waitPending();
    static
    SLogger&    global();
    static
    std::string time();
    unsigned long
                genSequenceNumber();
  };

 std::ostream& operator <<(std::ostream & os, SLogger & log);

}

#endif // SLOGGER
