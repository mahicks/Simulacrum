/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SLua
 * Implementation for SLua (Simulacrum Lua Runtime Handler)
 * Author: M.A.Hicks
 */

#include "slua.h"
#include <Core/error.h>
#include <Toolbox/SLogger/slogger.h>
#include <limits>

#ifndef NOLUA
// This should be the only point of Lua inclusion in Simulacrum
extern "C" {
 #include "../../External/liblua/slua.h"
}
#endif

using namespace Simulacrum;

namespace Simulacrum {
  class SLuaException: public SimulacrumException {
  protected:
    virtual const char* what() const throw(){
      return "SLua: run-time exception!";
    }
    bool isFatal(){
      return true;
    }
  };
}

// SVariant implementation

enum SVariant_Type_Enum {inttype,doubletype,booltype,stringtype,ptype};

class SLVariant::SLVariantPIMPL {
public:
  union svariant_types {
    int          anint;
    double       adouble;
    bool         abool;
    SLUserDataP  adatap;
  }              pod_data;
  std::string    str_data;
  int            cnt_type;
};

SLVariant::SLVariant(const int newdat): PrivateData(new SLVariantPIMPL()) {
  PrivateData->pod_data.anint = newdat;
  PrivateData->cnt_type       = inttype;
}

SLVariant::SLVariant(const double newdat): PrivateData(new SLVariantPIMPL()) {
  PrivateData->pod_data.adouble = newdat;
  PrivateData->cnt_type         = doubletype;
}

SLVariant::SLVariant(const bool newdat): PrivateData(new SLVariantPIMPL()) {
  PrivateData->pod_data.abool = newdat;
  PrivateData->cnt_type       = booltype;
}

SLVariant::SLVariant(const std::string &newdat): 
                                             PrivateData(new SLVariantPIMPL()) {
  PrivateData->str_data = newdat;
  PrivateData->cnt_type = stringtype;
}

SLVariant::SLVariant(swig_type_info* wraptype, void* wrapdat, bool mng):
                                             PrivateData(new SLVariantPIMPL()) {
  PrivateData->pod_data.adatap.sltype = wraptype;
  PrivateData->pod_data.adatap.data   = wrapdat;
  PrivateData->pod_data.adatap.manage = mng;
  PrivateData->cnt_type               = ptype;
}

SLVariant::~SLVariant() {
  delete PrivateData;
}

// Main SLua implementation

class SLua::SLuaPIMPL {
public:
  lua_State          *SLuaInterpreter;
  unsigned            Pushed;
  std::string         CodePrefix;
  bool                Primed;
  virtual int         prime      ();
           SLuaPIMPL(): SLuaInterpreter(nullptr) {}
  virtual ~SLuaPIMPL() {}
};

#ifndef NOLUA
// SLua IS built into Simulacrum

SLua::SLua(): PrivateData(new SLuaPIMPL()) {
  reset();
}

SLua::SLua(const std::string& src): PrivateData(new SLuaPIMPL()) {
  reset();
  loadMem(src);
}

SLua::SLua(SURI targ): PrivateData(new SLuaPIMPL()) {
  reset();
  loadURI(targ);
}

SLua::~SLua() {
  lua_close(PrivateData->SLuaInterpreter);
  delete PrivateData;
}

void SLua::reset() {
  if (PrivateData->SLuaInterpreter != nullptr)
    lua_close(PrivateData->SLuaInterpreter);
  PrivateData->SLuaInterpreter = luaL_newstate();
  luaL_checkversion(PrivateData->SLuaInterpreter);
  lua_gc(PrivateData->SLuaInterpreter, LUA_GCSTOP, 0);
  luaL_openlibs(PrivateData->SLuaInterpreter);
  luaopen_SLua(PrivateData->SLuaInterpreter);
  lua_gc(PrivateData->SLuaInterpreter, LUA_GCRESTART, 0);
  PrivateData->Pushed = 0;
  PrivateData->Primed = false;
}

void SLua::setPrefix(const std::string& newpref) {
  PrivateData->CodePrefix = newpref;
}

std::string SLua::checkError(int status) {
  std::string errorstr;
  if (status != LUA_OK && !lua_isnil(PrivateData->SLuaInterpreter, -1))
    errorstr = lua_tostring(PrivateData->SLuaInterpreter, -1);
  return errorstr;
}

void SLua::exceptError(int errorcode) {
  if (errorcode != LUA_OK) {
    SLogger::global().addMessage("SLua::exceptError: " +
                                 checkError(errorcode));
    throw SLuaException();
  }
}

swig_type_info* SLua::queryType(const std::string& classname) {
  return SWIG_TypeQuery(PrivateData->SLuaInterpreter, classname.c_str());
}

int SLua::loadMem(const std::string& newprog) {
  const std::string toexec = PrivateData->CodePrefix + newprog;
  return luaL_loadbuffer
             (PrivateData->SLuaInterpreter,toexec.c_str(),toexec.size(),"SLua");
}

int SLua::loadFile(SFile newf) {
  return loadMem(newf.toString());
}

int SLua::loadURI(SURI nuri) {
  return loadFile(nuri.file());
}

void SLua::push(int newi) {
  lua_pushinteger(PrivateData->SLuaInterpreter,newi);
  PrivateData->Pushed++;
}

void SLua::push(double newd) {
  lua_pushnumber(PrivateData->SLuaInterpreter,newd);
  PrivateData->Pushed++;
}

void SLua::push(bool newb) {
  lua_pushboolean(PrivateData->SLuaInterpreter,newb);
  PrivateData->Pushed++;
}

void SLua::push(const std::string& news) {
  lua_pushstring(PrivateData->SLuaInterpreter,news.c_str());
  PrivateData->Pushed++;
}

void SLua::push(SLUserDataP userdata) {
  SWIG_Lua_NewPointerObj
   (PrivateData->SLuaInterpreter,userdata.data,userdata.sltype,userdata.manage);
  PrivateData->Pushed++;
}

void SLua::push(const SLVariant& newvariant) {
  switch (newvariant.PrivateData->cnt_type) {
    case inttype:
      push(newvariant.PrivateData->pod_data.anint);
      break;
    case doubletype:
      push(newvariant.PrivateData->pod_data.adouble);
      break;
    case booltype:
      push(newvariant.PrivateData->pod_data.abool);
      break;
    case stringtype:
      push(newvariant.PrivateData->str_data);
      break;
    case ptype:
      push(newvariant.PrivateData->pod_data.adatap);
      break;
    default:
      throw SLuaException();
      break;
  }
}

int SLua::pop(int& targ) {
  int retval = 0;
  if (lua_isnumber(PrivateData->SLuaInterpreter, -1)) {
    targ = lua_tointeger(PrivateData->SLuaInterpreter,-1);
    lua_pop(PrivateData->SLuaInterpreter,1);
  }
  else
    retval = 1;
  return retval;
}

int SLua::pop(double& targ) {
  int retval = 0;
  if (lua_isnumber(PrivateData->SLuaInterpreter, -1)) {
    targ = lua_tonumber(PrivateData->SLuaInterpreter,-1);
    lua_pop(PrivateData->SLuaInterpreter,1);
  }
  else
    retval = 1;
  return retval;
}

int SLua::pop(bool& targ) {
  int retval = 0;
  if (lua_isboolean(PrivateData->SLuaInterpreter, -1)) {
    targ = lua_toboolean(PrivateData->SLuaInterpreter,-1);
    lua_pop(PrivateData->SLuaInterpreter,1);
  }
  else
    retval = 1;
  return retval;
}

int SLua::pop(std::string& targ) {
  int retval = 0;
  const char *stdval = lua_tostring(PrivateData->SLuaInterpreter,-1);
  if (stdval != nullptr)
    targ = stdval;
  lua_pop(PrivateData->SLuaInterpreter,1);
  return retval;
}

int SLua::pop(SLUserDataP& targ) {
  int retval = SWIG_Lua_ConvertPtr
         (PrivateData->SLuaInterpreter,-1,&(targ.data),targ.sltype,targ.manage);
  return retval;
}

int SLua::pos(const std::string& fname) {
  if (!PrivateData->Primed)
    PrivateData->prime();
  lua_getglobal(PrivateData->SLuaInterpreter, fname.c_str());
  return 0;
}

int SLua::exec() {
  int res = lua_pcall(PrivateData->SLuaInterpreter, PrivateData->Pushed,
                      LUA_MULTRET, 0);
  PrivateData->Pushed = 0;
  return res;
}

int SLua::SLuaPIMPL::prime() {
  Primed = true;
  return lua_pcall(SLuaInterpreter, 0, 0, 0);
}

#else
//------------------------------------------------------------------------------
// SLua is NOT built into Simulacrum
//------------------------------------------------------------------------------
SLua::SLua() { }

SLua::SLua(const std::string&) { }

SLua::SLua(SURI) { }

SLua::~SLua() { }

void SLua::reset() { }

std::string SLua::checkError(int) {
  throw SLuaException();
}

void SLua::exceptError(int) {
  throw SLuaException();
}

swig_type_info* SLua::queryType(const std::string&) {
  throw SLuaException();
}

void SLua::setPrefix(const std::string&) {

}

int SLua::loadMem(const std::string&) {
  throw SLuaException();
  return 1;
}

int SLua::loadFile(SFile newf) {
  return loadMem(newf.toString());
}

int SLua::loadURI(SURI) {
  throw SLuaException();
  return 1;
}

void SLua::push(int) {
  throw SLuaException();
}

void SLua::push(double) {
  throw SLuaException();
}

void SLua::push(bool) {
  throw SLuaException();
}

void SLua::push(const std::string&) {
  throw SLuaException();
}

void SLua::push(SLUserDataP) {
  throw SLuaException();
}

void SLua::push(const SLVariant&) {
  throw SLuaException();
}

int SLua::pop(int&) {
  int retval = 0;
  throw SLuaException();
  return retval;
}

int SLua::pop(double&) {
  int retval = 0;
  throw SLuaException();
  return retval;
}

int SLua::pop(bool&) {
  int retval = 0;
  throw SLuaException();
  return retval;
}

int SLua::pop(std::string&) {
  int retval = 0;
  throw SLuaException();
  return retval;
}

int SLua::pop(SLUserDataP&) {
  int retval = 0;
  throw SLuaException();
  return retval;
}

int SLua::exec() {
  throw SLuaException();
}

int SLua::SLuaPIMPL::prime() {
  throw SLuaException();
}

int SLua::pos(const std::string& ) {
  throw SLuaException();
}

#endif
