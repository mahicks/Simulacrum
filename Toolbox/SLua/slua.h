/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SLua
 * Interface specification for SLua (Simulacrum Lua Runtime Handler)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SLUA
#define SIMULACRUM_SLUA

#include <string>
#include <tuple>
#include <Core/slockable.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SURI/suri.h>

// forward decls of some internally used Lua state structs
struct lua_State;
struct swig_type_info;

namespace Simulacrum {

  class SLua;

  class SIMU_API SLUserDataP {
  public:
    swig_type_info *sltype;
    void           *data;
    bool            manage;
  };

  class SIMU_API SLVariant {
  friend class SLua;
  private:
    class SLVariantPIMPL;
    SLVariantPIMPL *PrivateData;
  public:
                   SLVariant(const int);
                   SLVariant(const double);
                   SLVariant(const bool);
                   SLVariant(const std::string&);
                   SLVariant(swig_type_info*, void *, bool manage = false);
    virtual       ~SLVariant();
  };

  class SIMU_API SLua: public SLockable {
  private:
    class SLuaPIMPL;
    SLuaPIMPL *PrivateData;
  public:
                        SLua       ();
                        SLua       (const std::string&);
                        SLua       (SURI);
    virtual            ~SLua       ();
    virtual void        reset      ();
    virtual void        setPrefix  (const std::string&);
    virtual std::string checkError (int);
    virtual void        exceptError(int);
    /* query Swig type wrapping */
    virtual swig_type_info*
                        queryType  (const std::string& classname);
    /* load programs into interpreter */
    virtual int         loadMem    (const std::string&);
    virtual int         loadFile   (SFile);
    virtual int         loadURI    (SURI);
    /* add data to the Lua stack */
    virtual void        push       (int);
    virtual void        push       (double);
    virtual void        push       (bool);
    virtual void        push       (const std::string&);
    virtual void        push       (SLUserDataP);
    virtual void        push       (const SLVariant&);
    /* get data from the Lua stack */
    virtual int         pop        (int&);
    virtual int         pop        (double&);
    virtual int         pop        (bool&);
    virtual int         pop        (std::string&);
    virtual int         pop        (SLUserDataP&);
    /* call the execution of Lua */
    virtual int         pos        (const std::string&);
    virtual int         exec       ();
    /* syntactic sugar for calling Lua methods */
    template<class rettype = int>
            rettype     operator() (const std::string& f,
                                    std::initializer_list<SLVariant> params) {
      rettype retval;
      lock();
      pos(f);
      for (unsigned i = 0; i< params.size(); i++)
        push(*(params.begin()+i));
      exec();
      pop(retval);
      unlock();
      return retval;
    }
  };

}

#endif //SIMULACRUM_SLUA
