/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SRING:
 * Implementation specification for SRING
 * Author: M.A.Hicks
 */

#include "sring.h"

using namespace Simulacrum;

class SRingBuffer::SRingBufferPIMPL {
public:
  std::vector< bufftype > buffer;
  unsigned                beginp,endp,getp;
  bool                    Empty, HasMore;
};

SRingBuffer::SRingBuffer(unsigned size): PrivateData(new SRingBufferPIMPL()) {
  resize(size);
}

SRingBuffer::~SRingBuffer() {
  delete PrivateData;
}

unsigned SRingBuffer::size() {
  return PrivateData->buffer.size();
}

unsigned SRingBuffer::usedSize() {
  unsigned res = 0;
  if (!PrivateData->Empty) {
  if (PrivateData->endp <= PrivateData->beginp)
    res = (PrivateData->buffer.size()-PrivateData->beginp)+PrivateData->endp;
  else
    res = PrivateData->endp - PrivateData->beginp;
  }
  return res;
}

void SRingBuffer::reset() {
  PrivateData->Empty   = true;
  PrivateData->HasMore = false;
  PrivateData->beginp  = PrivateData->endp = PrivateData->getp = 0;
}

void SRingBuffer::resize(unsigned newsize) {
  PrivateData->buffer.resize(newsize);
  reset();
}

void SRingBuffer::add(bufftype newdatum) {
  if (!PrivateData->Empty) {
    PrivateData->endp = (PrivateData->endp + 1) % PrivateData->buffer.size();
    if (PrivateData->beginp == PrivateData->endp)
      PrivateData->beginp = (PrivateData->beginp + 1) % 
                             PrivateData->buffer.size();
    if ((PrivateData->getp == PrivateData->endp) || (!PrivateData->HasMore))
      PrivateData->getp = (PrivateData->getp + 1) % PrivateData->buffer.size();
  }
  else {
    PrivateData->Empty   = false;
  }
  PrivateData->buffer[PrivateData->endp] = newdatum;
  PrivateData->HasMore = true;
}

bool SRingBuffer::isEnd() {
  bool res = false;
  if ( PrivateData->Empty || PrivateData->getp==PrivateData->endp )
    res = true;
  return res;
}

bool SRingBuffer::hasMore() {
  return PrivateData->HasMore;
}

SRingBuffer::bufftype SRingBuffer::get() {
  bufftype retval;
  if (PrivateData->HasMore) {
    retval = PrivateData->buffer[PrivateData->getp];
    if (PrivateData->getp == PrivateData->endp)
      PrivateData->HasMore = false;
    else {
      PrivateData->getp = (PrivateData->getp+1) % PrivateData->buffer.size();
    }
  }
  else {
    retval = PrivateData->buffer[PrivateData->endp];
  }
  return retval;
}

void SRingBuffer::resetGet() {
  PrivateData->getp = PrivateData->beginp;
  if (!PrivateData->Empty)
    PrivateData->HasMore = true;
}
