/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::SRING:
 * Interface specification for SRING
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SRING
#define SIMULACRUM_SRING

#include <vector>
#include <iostream>
#include <Core/definitions.h>

namespace Simulacrum {

class SIMU_API SRingBuffer {
public:
  typedef  char bufftype;
           SRingBuffer(unsigned size);
  virtual ~SRingBuffer();
  unsigned size();
  unsigned usedSize();
  void     reset();
  void     resize(unsigned newsize);
  void     add(bufftype newdatum);
  bool     isEnd();
  bool     hasMore();
  bufftype get();
  void     resetGet();
private:
  class SRingBufferPIMPL;
  SRingBufferPIMPL *PrivateData;
};

} //end namespace
#endif //SIMULACRUM_SRING
