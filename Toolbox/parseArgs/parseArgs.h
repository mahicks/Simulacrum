/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::parseArgs:
 * Simple Command-Line Argument Parser and Storage Class
 * Author: M.A.Hicks
 */
#ifndef PARSARGS
#define PARSARGS

#include <iostream>
#include <map>
#include <vector>
#include "../../Core/definitions.h"

namespace Simulacrum {

  struct parsedArg {
    std::string Name;
    std::string Value;
    std::string Switch;
    std::string Help;
    bool        isSet;
    bool        isSwitch;
    bool        isPositional;
    bool        isParsed;
    bool        isMandatory;
    bool        isGobbler;
    std::vector<std::string>
                Gobbled;
  };

  class SIMU_API parseArgs {
  private:
    class parseArgsPIMPL;
    parseArgsPIMPL *PrivateData;
  public:
                parseArgs();
    virtual    ~parseArgs();
    void        doParseArgs(int argc, char* argv[]);
    void        addArg(const std::string& switchstr,
                       const std::string& name,
                       const std::string& defvalue,
                       bool isswitch,
                       bool defset,
                       bool madatory,
                       bool  gobbler,
                       const std::string& helpstr);
    void       addPositional(const std::string& name,
                             const std::string& defvalue,
                             bool  madatory,
                             bool  gobbler,
                             const std::string& helpstr);
    parsedArg& getArg(const std::string&, bool check = true);
    parsedArg& getPositional(unsigned, bool check = true);
    unsigned   positonals();
    unsigned   switches();
    void       setHelp(const std::string&);
    void       setName(const std::string&);
    void       setPreamble(const std::string&);
    void       writeHelp();
  };

}

#endif
