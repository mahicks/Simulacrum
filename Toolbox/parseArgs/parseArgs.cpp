/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::parseArgs:
 * Simple Command-Line Argument Parser and Storage Class
 * Author: M.A.Hicks
 */

#include "parseArgs.h"
#include <Toolbox/SURI/suri.h>
#include <Core/error.h>

#include <string>

using namespace Simulacrum;

namespace Simulacrum {
  class parseException : public SimulacrumIOException {
  public:
    virtual const char* what() const throw(){
      return "Argument Parsing Exception";
    }
  };

  class parseHelpException : public parseException {
  public:
    virtual const char* what() const throw(){
      return "Help message requested";
    }
  };
}

class parseArgs::parseArgsPIMPL {
public:
  std::string                       AppName;
  std::string                       AppHelp;
  std::string                       AppPreamble;
  std::map<std::string, parsedArg>  Args;
  std::map<std::string, parsedArg*> switchArgs;
  std::vector<parsedArg*>           positionArgs;
  unsigned                          parsedSwitches;
  unsigned                          parsedPositionals;
  void                              parseError(const std::string&);
  parseArgsPIMPL(): parsedSwitches(0), parsedPositionals(0) {}
};

void parseArgs::parseArgsPIMPL::parseError(const std::string &error) {
  std::cerr << AppName <<": Error parsing arguments: " << error << std::endl
            << std::endl;
  throw parseException();
}

parseArgs::parseArgs(): PrivateData(new parseArgsPIMPL()) {
  // add a standard help switch
  addArg("-h","Help", "",true, false, false, false,
         "Output the standard help message.");
}

parseArgs::~parseArgs() {
  delete PrivateData;
}

void parseArgs::doParseArgs(int argc, char* argv[]) {
  std::string  argument;
  parsedArg    *gobbler = nullptr;
  if (argc < 1)
    PrivateData->parseError("No arguments!");
  if (PrivateData->AppName.size() == 0) {
    SURI tmp (argv[0]);
    if (tmp.depth() > 0)
      PrivateData->AppName = tmp.getComponent(tmp.depth()-1);
  }
  for (int arg=1; arg<argc; arg++) {
    argument = argv[arg];
    if ((argument.at(0) == '-') || (argument.at(0) == '+')) {
      // this is a switch
      if (PrivateData->switchArgs.count(argument) == 1) {
        if (PrivateData->switchArgs[argument]->isParsed &&
            !(PrivateData->switchArgs[argument]->isGobbler))
          PrivateData->parseError("Switch already set! (" + argument + ")");
        else {
          if (PrivateData->switchArgs[argument]->isSwitch) {
            PrivateData->switchArgs[argument]->isSet  = 
                                    !(PrivateData->switchArgs[argument]->isSet);
            PrivateData->switchArgs[argument]->isParsed = true;
          }
          else {
            if ((arg+1) < argc) {
              PrivateData->switchArgs[argument]->Value =
                                                       std::string(argv[arg+1]);
              PrivateData->switchArgs[argument]->isParsed = true;
              PrivateData->parsedSwitches++;
              if (PrivateData->switchArgs[argument]->isGobbler) {
                PrivateData->switchArgs[argument]->
                                    Gobbled.push_back(std::string(argv[arg+1]));
              }
              arg++;
            }
            else {
              PrivateData->parseError
                                  ("Switch requires value! (" + argument + ")");
            }
          }
        }
      }
      else {
        PrivateData->parseError("Switch not recognized! (" + argument + ")");
      }
    }
    else {
      // this is a positional parameter
      if (gobbler != nullptr) {
        gobbler->Gobbled.push_back(argument);
      }
      else {
        if (PrivateData->parsedPositionals < PrivateData->positionArgs.size()) {
          PrivateData->positionArgs[PrivateData->parsedPositionals]->
                                                               Value = argument;
          PrivateData->positionArgs[PrivateData->parsedPositionals]->isParsed =
                                                                           true;
          if (PrivateData->positionArgs
                                  [PrivateData->parsedPositionals]->isGobbler) {
            gobbler = PrivateData->positionArgs[PrivateData->parsedPositionals];
            gobbler->Gobbled.push_back(argument);
          }
          PrivateData->parsedPositionals++;
        }
        else
          PrivateData->parseError("Too many positional arguments!");
      }
    }
  }
  // short-cut for help message
  if (getArg("Help").isSet) {
    writeHelp();
    throw parseHelpException();
  }
}

void parseArgs::addArg(const std::string& switchstr,
                       const std::string& name,
                       const std::string& defvalue,
                       bool isswitch,
                       bool defset,
                       bool mandatory,
                       bool  gobbler,
                       const std::string& helpstr) {
  parsedArg newarg;
  newarg.Switch       = switchstr;
  newarg.Name         = name;
  newarg.Value        = defvalue;
  newarg.isSwitch     = isswitch;
  newarg.isSet        = defset;
  newarg.Help         = helpstr;
  newarg.isParsed     = false;
  newarg.isPositional = false;
  newarg.isMandatory  = mandatory;
  newarg.isGobbler    = gobbler;
  PrivateData->Args.insert
                         (std::pair<std::string,parsedArg>(newarg.Name,newarg));
  PrivateData->switchArgs.insert(std::pair<std::string,parsedArg*>
                    (newarg.Switch,&getArg(newarg.Name, false)));
}

void parseArgs::addPositional(const std::string& name,
                              const std::string& defvalue,
                              bool  mandatory,
                              bool  gobbler,
                              const std::string& helpstr) {
  parsedArg newarg;
  newarg.Name         = name;
  newarg.Value        = defvalue;
  newarg.isSwitch     = false;
  newarg.isSet        = false;
  newarg.Help         = helpstr;
  newarg.isParsed     = false;
  newarg.isPositional = true;
  newarg.isMandatory  = mandatory;
  newarg.isGobbler    = gobbler;
  PrivateData->Args.insert
                         (std::pair<std::string,parsedArg>(newarg.Name,newarg));
  PrivateData->positionArgs.push_back(&getArg(name, false));
}


parsedArg& parseArgs::getArg(const std::string& name, bool check) {
  if (!PrivateData->Args.count(name))
    PrivateData->parseError("Argument not found!");
  parsedArg &getarg = PrivateData->Args[name];
  if (check && getarg.isMandatory && (!getarg.isParsed))
    PrivateData->parseError("missing argument (" + getarg.Name + ")");
  return getarg;
}

parsedArg& parseArgs::getPositional(unsigned pos, bool check) {
  if (pos > positonals())
    PrivateData->parseError("Positional argument now found!");
  parsedArg &getarg = *(PrivateData->positionArgs[pos]);
  if (check && getarg.isMandatory && (!getarg.isParsed))
    PrivateData->parseError("missing argument (" + getarg.Name + ")");
  return getarg;
}

unsigned int parseArgs::positonals() {
  return PrivateData->parsedPositionals;
}

unsigned int parseArgs::switches() {
  return PrivateData->parsedSwitches;
}


void parseArgs::setHelp(const std::string& newhelp) {
  PrivateData->AppHelp = newhelp;
}

void parseArgs::setName(const std::string& newname) {
  PrivateData->AppName = newname;
}

void parseArgs::setPreamble(const std::string& newpre) {
  PrivateData->AppPreamble = newpre;
}


void parseArgs::writeHelp() {
  std::cout << "Help: " << PrivateData->AppName << std::endl;
  std::cout << "======";
  for (unsigned i=0; i<PrivateData->AppName.length();i++)
    std::cout.put('=');
  std::cout << std::endl;
  if (PrivateData->AppPreamble.length() > 0) {
    std::cout << PrivateData->AppPreamble << std::endl  << std::endl;
  }
  std::cout << "Usage: " << PrivateData->AppName << " ";
  std::map<std::string,parsedArg>::iterator argsit;
  // First, write out the overall format of the command
  for (argsit=PrivateData->Args.begin();
       argsit != PrivateData->Args.end(); argsit++) {
    if (!((*argsit).second.isPositional)) {
      std::cout << "[" << (*argsit).second.Switch;
      if ((*argsit).second.isSwitch)
        std::cout << "]" << " ";
      else {
        std::cout << " ";
        std::cout << (*argsit).second.Name << "]" << " ";
      }
    }
  }
  for (unsigned posarg=0; posarg < PrivateData->positionArgs.size(); posarg++) {
    std::cout << "<" << PrivateData->positionArgs[posarg]->Name;
    if (PrivateData->positionArgs[posarg]->isGobbler)
      std::cout << " ... ";
    std::cout << "> ";
  }
  std::cout << std::endl;
  // now write out the fuller description of each argument
  if (PrivateData->Args.size() > 0) {
    unsigned longest = 0;
    for (argsit=PrivateData->Args.begin();
         argsit != PrivateData->Args.end(); argsit++) {
      if ((*argsit).second.isPositional) {
        if ((*argsit).second.Name.length() > longest)
            longest = (*argsit).second.Name.length();
      }
      else {
         if ((*argsit).second.Switch.length() > longest)
          longest = (*argsit).second.Switch.length();
      }
    }
    std::cout << std::endl;
    std::cout << "Description:" << std::endl
              << "------------" << std::endl;
    for (argsit=PrivateData->Args.begin();
         argsit != PrivateData->Args.end(); argsit++) {
      std::cout << " ";
      if ((*argsit).second.isPositional) {
        for (unsigned pad=0;
             pad<(longest - (int)(*argsit).second.Name.length()); pad++)
          std::cout.put(' ');
        std::cout << (*argsit).second.Name;
      }
      else {
        for (unsigned pad=0;
             pad<(longest - (int)(*argsit).second.Switch.length()); pad++)
          std::cout.put(' ');
        std::cout << (*argsit).second.Switch;
      }
      std::cout << " : ";
      std::cout << (*argsit).second.Help;
      if ((*argsit).second.isSwitch) {
        std::cout << " (status: ";
        if ((*argsit).second.isSet)
          std::cout << "set";
        else
          std::cout << "not-set";
        std::cout << ")";
      }
      else
        if ((*argsit).second.Value.length() > 0)
          std::cout << " (status: \"" << (*argsit).second.Value << "\")";
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
  if (PrivateData->AppHelp.length() > 0)
    std::cout << PrivateData->AppHelp << std::endl;
}
