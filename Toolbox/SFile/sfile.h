/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SFile
 * Interface specification for SFile (simple local file tool)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SFILE
#define SIMULACRUM_SFILE

#include "../../Core/definitions.h"
#include <string>
#include <vector>
#include <sstream>

namespace Simulacrum {

  class SIMU_API SFile {
  private:
    class SFilePIMPL;
    SFilePIMPL *PrivateData;
  public:
                        SFile             ();
                        SFile             (const std::string&);
                        SFile             (const SFile&);
#ifndef SWIG
                        SFile             (SFile&&);
    SFile&              operator=         (SFile&);
    SFile&              operator=         (SFile&&);
#endif
              virtual  ~SFile             ();
    const std::string&  getLocation       ();
    void                setLocation       (const std::string&);
    bool                exists            ();
    bool                isDIR             ();
    std::vector<std::string>
                        getDIRContents    (bool recurse = false);
    void                deleteFromDisk    ();
    void                move              (const std::string&);
    void                copy              (const std::string&);
    void                mkdir             ();
    unsigned long       size              ();
    static std::string  getSystemSeparator();
    SFile&              operator=         (const SFile&);
    bool                toStream          (std::ostream&);
    bool                fromStream        (std::istream&);
    std::string         toString          ();
    bool                fromString        (const std::string&);
    void                removeOnDestroy   (bool=true);
    void                clear             ();
    int                 exec              (const std::string& args);
    // useful methods
    static std::string  localTempDIR      ();
    static std::string  genTempFileName   ();
  };

}

#endif
