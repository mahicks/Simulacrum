/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SFile
 * Implementation for SFile (simple local file tool)
 * Author: M.A.Hicks
 */

#include "sfile.h"
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/error.h>
#include <fstream>
#include <cstdlib>
#ifdef _WIN32
#include <Windows.h>
#endif

/* following define avoid c++0x linking issues with
 * certain compilations of boost 
 */
#define BOOST_NO_SCOPED_ENUMS
#include <boost/filesystem.hpp>

using namespace Simulacrum;
namespace fs = boost::filesystem;

namespace Simulacrum {
  class SFileException : public SimulacrumIOException {
    virtual const char* what() const throw(){
      return "Simulacrum: File Exception";
    }
  };
}

class SFile::SFilePIMPL {
public:
  std::string         Location;
  bool                RemoveSelf;
  SFilePIMPL(): RemoveSelf(false) { }
};

SFile::SFile(): PrivateData(new SFilePIMPL()) {

}

SFile::SFile(const std::string& newfile): PrivateData(new SFilePIMPL()) {
  setLocation(newfile);
}

SFile::SFile(const SFile& that): PrivateData(new SFilePIMPL()) {
 (*this) = that;
}

SFile::SFile(SFile&& that): PrivateData(new SFilePIMPL()) {
 (*this) = that;
}

SFile::~SFile() {
  clear();
  delete PrivateData;
}

const std::string& SFile::getLocation() {
  return PrivateData->Location;
}

void SFile::setLocation(const std::string& newlocation) {
  clear();
  (PrivateData->Location) = newlocation;
}

bool SFile::exists() {
  bool res = false;
  try {
    fs::path location = fs::system_complete(fs::path(PrivateData->Location));
    res = fs::exists( location );
  }
  catch (std::exception &e) {
    res = false;
  }
  return res;
}

bool SFile::isDIR() {
  bool res = false;
  try {
    fs::path location = fs::system_complete(fs::path(PrivateData->Location));
    res = fs::is_directory( location );
  }
  catch (std::exception &e) {
    res = false;
  }
  return res;
}

std::vector< std::string > SFile::getDIRContents(bool recurse) {
  std::vector< std::string > DIRStringList;
  fs::path location = fs::path(PrivateData->Location);
  try {
    if (isDIR()) {
      fs::directory_iterator end_iter;
      for ( fs::directory_iterator dir_list( location );
            dir_list != end_iter;
            ++dir_list ) {
        DIRStringList.push_back(dir_list->path().string());
        if (recurse && (SFile(dir_list->path().string()).isDIR())) {
          fs::path tmp = fs::path(PrivateData->Location)/
                                            fs::path(dir_list->path().filename());
          std::vector< std::string > SubDIRStringList
            = SFile(tmp.string()).getDIRContents(recurse);
          for (unsigned i=0;i<SubDIRStringList.size();i++) {
            DIRStringList.push_back(SubDIRStringList[i]);
          }
        }
      }
    }
  }
  catch(std::exception &e) {
    SLogger::global().addMessage(std::string
            ("SFile::getDIRContents: Error Listing: ") + PrivateData->Location);
  }
  return DIRStringList;
}

void SFile::deleteFromDisk() {
  fs::path location = fs::system_complete( fs::path( PrivateData->Location ) );
  fs::remove(location);
}

void SFile::move(const std::string& target) {
  fs::path location  = fs::system_complete( fs::path( PrivateData->Location ) );
  fs::path tlocation = fs::system_complete( fs::path( target ) );
  fs::rename(location,tlocation);
}

void SFile::copy(const std::string& target) {
  fs::path location  = fs::system_complete( fs::path( PrivateData->Location ) );
  fs::path tlocation = fs::system_complete( fs::path( target ) );
  fs::copy(location,tlocation);
}

void SFile::mkdir() {
  fs::path location  = fs::system_complete( fs::path( PrivateData->Location ) );
  fs::create_directories(location);
}

long unsigned int SFile::size() {
  if (isDIR()) return 0;
  fs::path location = fs::system_complete( fs::path( PrivateData->Location ) );
  return fs::file_size( location );
}

std::string SFile::getSystemSeparator() {
#ifdef _WIN32
  return "\\";
#else
  return "/";
#endif
}

SFile& SFile::operator= (SFile& target) {
  clear();
  PrivateData->Location          = target.PrivateData->Location;
  PrivateData->RemoveSelf        = target.PrivateData->RemoveSelf;
  target.PrivateData->RemoveSelf = false;
  return *this;
}

SFile& SFile::operator= (const SFile& target) {
  clear();
  PrivateData->Location          = target.PrivateData->Location;
  PrivateData->RemoveSelf        = target.PrivateData->RemoveSelf;
  return *this;
}

SFile& SFile::operator=(SFile&& target) {
  return ((*this)=target);
}

bool SFile::toStream(std::ostream& targ) {
  std::ifstream InFile;
  bool res = true;
  InFile.open(getLocation().c_str(), std::ios::in | std::ios::binary);
  if (!InFile.is_open())
    res = false;
  char writechar;
  while (InFile.read(&writechar,1)) {
    targ.write(&writechar,1);
  }
  return res;
}

bool SFile::fromStream(std::istream& source){
  std::ofstream OutFile;
  bool res = true;
  OutFile.open(getLocation().c_str(), std::ios::out | std::ios::binary);
  if (!OutFile.is_open())
    res = false;
  char writechar;
  while (source.read(&writechar,1)) {
    OutFile.write(&writechar,1);
  }
  return res;
}

std::string SFile::toString() {
  std::stringstream tmp;
  toStream(tmp);
  return tmp.str();
}

bool SFile::fromString(const std::string& source) {
  std::stringstream wrapstream(source);
  return fromStream(wrapstream);
}

void SFile::removeOnDestroy (bool dorem) {
  PrivateData->RemoveSelf = dorem;
}

void SFile::clear() {
  if (PrivateData->RemoveSelf)
    deleteFromDisk();
  PrivateData->RemoveSelf = false;
  PrivateData->Location.clear();
}

int SFile::exec(const std::string& args) {
  std::string cmd = "\"" + getLocation() + "\" " + args;
#ifdef _WIN32
  return (int) WinExec(cmd.c_str(), SW_HIDE);
#else
  return system(cmd.c_str());
#endif
}

std::string SFile::localTempDIR() {
  return fs::temp_directory_path().string();
}

std::string SFile::genTempFileName() {
  std::stringstream tmpfilename;
  tmpfilename << sysInfo::time() << "-" << rand() << ".stmp";
  return tmpfilename.str();
}
