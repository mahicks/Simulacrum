/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::URI:
 * Interface specification for URI Handler
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_URI
#define SIMULACRUM_URI

#include <string>
#include <vector>
#include <Core/definitions.h>
#include <Core/sconnectable.h>
#include <Toolbox/SFile/sfile.h>

namespace Simulacrum {

  class SIMU_API SURI: public SConnectable {
  private:
    class SURIPIMPL;
    SURIPIMPL *PrivateData;
  public:
                SURI();
                SURI(const std::string&);
                SURI(const SURI&);
    virtual    ~SURI();
    virtual
    void        clear(bool fullclear = true);
    void        doStop(bool);
    void        externalStop(bool&);
    bool        shouldStop();
    void        setSeparator(const std::string);
    std::string separator() const;
    void        setURI(const std::string&);
    void        setProtocol(const std::string &);
    std::string getURI(bool escaped=false) const;
    std::string getProtocol() const;
    unsigned    depth() const;
    std::string getComponent(unsigned) const;
    SURI&       deleteComponent(unsigned);
    SURI&       addComponentFront(const std::string&);
    SURI&       addComponentBack(const std::string&);
    const SURI& operator=(const SURI&);
    const SURI& add(const SURI&);
    bool        isLocal();
    SFile       file();
    SFile       toFile(const std::string&);
    SFile       toTempFile();
    bool        fromMem(const std::string&);
    std::string user();
    std::string password();
    void        progress(int);
    static
    std::string gsanitize(const std::string);
  };

  class SIMU_API SURII: public SURI {
  public:
                SURII();
                SURII(const std::string&);
    void        standardForm();
    virtual
    void        clear(bool fullclear = true);
  };

}

#endif
