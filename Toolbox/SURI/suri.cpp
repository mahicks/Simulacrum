/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::URI:
 * Implementation for URI Handler
 * Author: M.A.Hicks
 */

#include "suri.h"
#include <Core/error.h>
#include <Core/slockable.h>

#include <iostream>
#include <functional>
#include <stdio.h>
#include <Toolbox/SLogger/slogger.h>
#ifndef NOCURL
#include <curl/curl.h>
#endif

using namespace Simulacrum;

#ifdef _WIN32
static const std::string PATH_SEPARATOR        = "\\";
#else
static const std::string PATH_SEPARATOR        = "/";
#endif

namespace Simulacrum {
  class SURIRemoteException: public SimulacrumException {
  protected:
    virtual const char* what() const throw(){
      return "SURI: Remote fetching exception (no remote fetching support)";
    }
    bool isFatal(){
      return true;
    }
  };
}

static const std::string SEPREP = "%%";

static std::string escapeString(const std::string& src,
                                const std::string& sep,
                                const std::string& rep = SEPREP) {
  std::string result = src;
  while (true) {
    size_t pos = result.find(sep);
    if (pos == std::string::npos)
      break;
    result.replace(pos, sep.length(), rep);
  }
  return result;
}

static std::string unescapeString(const std::string& src,
                                  const std::string& sep) {
  return escapeString(src,SEPREP,sep);
}

std::string SURI::gsanitize(const std::string src) {
  std::string result = escapeString(src,"/");
  result = escapeString(result,"\\");
  return result;
}

class SURI::SURIPIMPL {
public:
  std::string              Protocol;
  std::vector<std::string> URI;
  std::string              Separator;
  std::string              ProtocolSeparator;
  std::string              User,Pass;
  bool                     isAbsolute;
  bool                     DoStop;
  bool                    *ExternalStop;
  std::string              popFront(std::string&);
  std::string              popProtocol(std::string&);
  bool                     tryUserPass(std::string&);
  SFile                    fetchRemoteObject(SURI* src,
                                             const std::string& remoteuri,
                                             const std::string& localuri,
                                             const std::string& user ="",
                                             const std::string& password ="");
  bool                     postRemoteObject (SURI* src,
                                             const std::string& remoteuri,
                                             const std::string& data,
                                             const std::string& user ="",
                                             const std::string& password ="");
  void                     setUserPass(const std::string& user,
                                        const std::string& pass);
};

SURI::SURI(): PrivateData(new SURIPIMPL()) {
  clear();
}

SURI::SURI(const std::string& newuri): PrivateData(new SURIPIMPL()) {
  clear();
  setURI(newuri);
}

SURI::SURI(const SURI& that): PrivateData(new SURIPIMPL()) {
  (*this) = that;
}

SURI::~SURI() {
  delete PrivateData;
}

void SURI::clear(bool fullclear) {
  if (fullclear) {
    PrivateData->ProtocolSeparator = "://";
    setSeparator(PATH_SEPARATOR);
  }
  PrivateData->URI.clear();
  PrivateData->Protocol.clear();
  PrivateData->User.clear();
  PrivateData->Pass.clear();
  PrivateData->isAbsolute=false;
  doStop(false);
  PrivateData->ExternalStop = nullptr;
}

void SURI::doStop (bool nstop) {
  PrivateData->DoStop = nstop;
}

void SURI::externalStop(bool &nexstop) {
  PrivateData->ExternalStop = &nexstop;
}

bool SURI::shouldStop() {
  if (PrivateData->ExternalStop !=nullptr)
    return *(PrivateData->ExternalStop);
  else
    return PrivateData->DoStop;
}

void SURII::clear(bool fullclear) {
  SURI::clear(fullclear);
  setSeparator("/");
}

SURII::SURII() {
  clear();
}

SURII::SURII (const std::string& nuri) : SURI() {
  clear();
  setURI(nuri);
}

void SURII::standardForm() {
  SURII newuri;
  SURI  exist(getURI());
  newuri.add(exist);
  setURI(newuri.getURI());
}

void SURI::setSeparator(const std::string newsep) {
  PrivateData->Separator = newsep;
}

std::string SURI::separator() const {
  return PrivateData->Separator;
}

std::string SURI::SURIPIMPL::popProtocol(std::string& baseuri) {
  std::string protocol;
  size_t protsep = baseuri.find(ProtocolSeparator);
  if (protsep != std::string::npos) {
    protocol = baseuri.substr(0,protsep);
    baseuri  = baseuri.substr(protsep+ProtocolSeparator.length(),
                              baseuri.length());
  }
  return protocol;
}

bool SURI::SURIPIMPL::tryUserPass (std::string& firstparturi) {
  bool retval = false;
  size_t userpasssep = firstparturi.rfind("@");
  if (userpasssep != std::string::npos) {
   size_t usersep = firstparturi.rfind(":");
   if (usersep != std::string::npos && usersep < userpasssep
       && usersep > 0) {
     User = firstparturi.substr(0,usersep);
     Pass = firstparturi.substr(usersep+1,userpasssep-usersep-1);
     retval = true;
   }
  }
  return retval;
}

std::string SURI::SURIPIMPL::popFront(std::string& baseuri) {
  std::string front;
  size_t sepf = baseuri.find(Separator);
  if (sepf == std::string::npos) {
    front = baseuri;
    baseuri.clear();
  }
  else {
    front = baseuri.substr(0,sepf);
    baseuri = baseuri.substr(sepf+Separator.length(),baseuri.length());
  }
  return front;
}

void SURI::setURI(const std::string& newuri) {
  clear(false);
  std::string nuri = newuri;
  setProtocol(PrivateData->popProtocol(nuri));
  while(nuri.size() > 0) {
    std::string newelement = PrivateData->popFront(nuri);
    if (newelement.length() > 0)
      addComponentBack(newelement);
  }
  if (newuri.size() > 0)
    if (newuri[0] == '/')
      PrivateData->isAbsolute = true;
  if (depth() > 0) {
    std::string firstpart = getComponent(0);
    PrivateData->tryUserPass(firstpart);
  }
}

void SURI::setProtocol(const std::string& newprot) {
  PrivateData->Protocol = newprot;
}

std::string SURI::getURI(bool escaped) const {
  std::string uristring;
  if (PrivateData->Protocol.length() > 0)
    uristring += PrivateData->Protocol + PrivateData->ProtocolSeparator;
  for (unsigned i=0; i< PrivateData->URI.size(); i++) {
    uristring += PrivateData->URI[i];
    if (i < (PrivateData->URI.size()-1))
      uristring += PrivateData->Separator;
  }
  if (PrivateData->isAbsolute)
    uristring = PrivateData->Separator + uristring;
  if (!escaped)
    uristring = unescapeString(uristring,PrivateData->Separator);
  return uristring;
}

std::string SURI::getProtocol() const {
  return PrivateData->Protocol;
}

unsigned int SURI::depth() const {
  return PrivateData->URI.size();
}

std::string SURI::getComponent(unsigned int element) const {
  return unescapeString(PrivateData->URI.at(element),PrivateData->Separator);
}

SURI& SURI::deleteComponent(unsigned element) {
  PrivateData->URI.erase(PrivateData->URI.begin()+element);
  return *this;
}

SURI& SURI::addComponentFront(const std::string& frontcl) {
  std::string frontc = frontcl;
  std::string prot = PrivateData->popProtocol(frontc);
  if (prot.size() > 0) {
    setProtocol(prot);
  }
  PrivateData->URI.insert(PrivateData->URI.begin(),
                          escapeString(frontc,PrivateData->Separator));
  return *this;
}

SURI& SURI::addComponentBack(const std::string& backcl) {
  std::string backc = backcl;
  if (PrivateData->URI.size() == 0) {
    std::string prot = PrivateData->popProtocol(backc);
    if (prot.size() > 0) {
      setProtocol(prot);
    }
  }
  PrivateData->URI.push_back(escapeString(backc,PrivateData->Separator));
  return *this;
}

const SURI& SURI::operator=(const Simulacrum::SURI& rhs) {
  PrivateData->Separator         = rhs.PrivateData->Separator;
  PrivateData->ProtocolSeparator = rhs.PrivateData->ProtocolSeparator;
  PrivateData->Protocol          = rhs.PrivateData->Protocol;
  PrivateData->User              = rhs.PrivateData->User;
  PrivateData->Pass              = rhs.PrivateData->Pass;
  PrivateData->URI               = rhs.PrivateData->URI;
  PrivateData->isAbsolute        = rhs.PrivateData->isAbsolute;
  return *this;
}

const SURI& SURI::add(const SURI& src) {
  for (unsigned p=0; p < src.depth(); p++)
    addComponentBack(src.getComponent(p));
  return *this;
}

bool SURI::isLocal() {
  return getProtocol().length() < 1;
}

SFile SURI::file() {
  SFile retval;
  if (isLocal())
    retval.setLocation(getURI());
  else
    retval = toTempFile();
  return retval;
}

SFile SURI::toFile(const std::string& dest) {
  SFile result;
  if (isLocal()) {
    result.setLocation(getURI());
    result.copy(dest);
    result.setLocation(dest);
  }
  else {
    result = PrivateData->fetchRemoteObject
                  (this,getURI(),dest,PrivateData->User,PrivateData->Pass);
  }
  return result;
}

SFile SURI::toTempFile() {
  SFile tmpfile;
  SURI  tmpfilepathname(SFile::localTempDIR());
  tmpfilepathname.addComponentBack(SFile::genTempFileName());
  tmpfile.setLocation( toFile(tmpfilepathname.getURI()).getLocation());
  tmpfile.removeOnDestroy(true);
  return tmpfile;
}

bool SURI::fromMem(const std::string& data) {
  return PrivateData->postRemoteObject
                       (this,getURI(),data,PrivateData->User,PrivateData->Pass);
}

void SURI::SURIPIMPL::setUserPass
                            (const std::string& user, const std::string& pass) {
  User = user;
  Pass = pass;
}

std::string SURI::user() {
  return PrivateData->User;
}

std::string SURI::password() {
  return PrivateData->Pass;
}

size_t curl_write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  return fwrite(ptr, size, nmemb, stream);
}

size_t curl_nowrite_data(void *, size_t, size_t nmemb, FILE *) {
  return nmemb;
}

#ifndef NOCURL
static int curlProgress (void* clientp, double dltotal, double dlnow,
                         double ultotal, double ulnow ) {
  int retval = 0;
  double progress = 0.0;
  SURI *caller = (SURI*) clientp;
  if (caller->shouldStop())
    retval = 500;
  if (dltotal > 0)  // this is a download
    progress = (dlnow / dltotal) * 100;
  else
    if (ultotal>0)// this is an upload
      progress = (ulnow / ultotal) * 100;
  caller->progress((int)progress);
  return retval;
}
#endif

void SURI::progress(int nprog) {
  SConnectable::progress(nprog);
}

#ifndef NOCURL
SFile SURI::SURIPIMPL::fetchRemoteObject(SURI* src,const std::string& remoteuri,
                                         const std::string& localuri,
                                         const std::string& ,
                                         const std::string& ) {
  SURII fetchuri(remoteuri);
  fetchuri.standardForm();
  src->refresh(true); // busy begin
  CURL    *curlhandler;
  FILE    *outfp;
  CURLcode res;
  SFile    target(localuri);
  if (!target.isDIR()) {
    curlhandler = curl_easy_init();
    if (curlhandler) {
      outfp = fopen(localuri.c_str(),"wb");
      if (outfp) {
        // configure curl environment
        curl_easy_setopt(curlhandler, CURLOPT_URL, fetchuri.getURI().c_str());
        curl_easy_setopt(curlhandler, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curlhandler, CURLOPT_WRITEFUNCTION, curl_write_data);
        curl_easy_setopt(curlhandler, CURLOPT_WRITEDATA, outfp);
        // skip peer verification (careful!)
        curl_easy_setopt(curlhandler, CURLOPT_SSL_VERIFYPEER, 0L);
        // skip hostname verification (careful!)
        curl_easy_setopt(curlhandler, CURLOPT_SSL_VERIFYHOST, 0L);
        // set the progress function
        curl_easy_setopt(curlhandler, CURLOPT_XFERINFOFUNCTION, curlProgress);
        // pass a pointer to this object
        curl_easy_setopt(curlhandler, CURLOPT_PROGRESSDATA, (void*)src);
        // ensure that progress function is called
        curl_easy_setopt(curlhandler, CURLOPT_NOPROGRESS, 0L);
        // set a connection timeout
        curl_easy_setopt(curlhandler, CURLOPT_CONNECTTIMEOUT, 30L);
        // correct signal handling related bug
        // http://curl.haxx.se/mail/lib-2008-09/0197.html
        curl_easy_setopt(curlhandler, CURLOPT_NOSIGNAL, 1);
        // perform the fetch
        res = curl_easy_perform(curlhandler);
        if (res != CURLE_OK) {
          std::stringstream errorstr;
          errorstr << "SURI Fetch Failure -- " << fetchuri.getURI() << " -- "
                   << res << ": " << curl_easy_strerror(res);
          SLogger::global().addMessage(errorstr.str(),
                                       SLogLevels::MEDIUM);
        }
        fclose(outfp);
      }
      /* always cleanup */
      curl_easy_cleanup(curlhandler);
    }
  }
  src->refresh(false); // busy end
  return SFile(localuri);
}
#else
SFile SURI::SURIPIMPL::fetchRemoteObject(SURI*,const std::string&,
                                         const std::string& ,
                                         const std::string& ,
                                         const std::string& ) {
  throw SURIRemoteException();
}
#endif

#ifndef NOCURL
bool SURI::SURIPIMPL::postRemoteObject(SURI* src,const std::string& remoteuri,
                                       const std::string& data,
                                       const std::string& ,
                                       const std::string& ) {
  SURII posturi(remoteuri);
  posturi.standardForm();
  src->refresh(true); // busy begin
  CURL    *curlhandler;
  CURLcode res = CURLE_OK;
  curlhandler = curl_easy_init();
  if (curlhandler) {
    // configure curl environment
    curl_easy_setopt(curlhandler, CURLOPT_URL, posturi.getURI().c_str());
    curl_easy_setopt(curlhandler, CURLOPT_FOLLOWLOCATION, 1L);
    // skip peer verification (careful!)
    curl_easy_setopt(curlhandler, CURLOPT_SSL_VERIFYPEER, 0L);
    // skip hostname verification (careful!)
    curl_easy_setopt(curlhandler, CURLOPT_SSL_VERIFYHOST, 0L);
    // set the progress function
    curl_easy_setopt(curlhandler, CURLOPT_XFERINFOFUNCTION, curlProgress);
    // pass a pointer to this object
    curl_easy_setopt(curlhandler, CURLOPT_PROGRESSDATA, (void*)src);
    // ensure that progress function is called
    curl_easy_setopt(curlhandler, CURLOPT_NOPROGRESS, 0L);
    // set a connection timeout
    curl_easy_setopt(curlhandler, CURLOPT_CONNECTTIMEOUT, 30L);
    // correct signal handling related bug
    // http://curl.haxx.se/mail/lib-2008-09/0197.html
    curl_easy_setopt(curlhandler, CURLOPT_NOSIGNAL, 1);
    // ignore any returned data
    curl_easy_setopt(curlhandler, CURLOPT_WRITEFUNCTION, curl_nowrite_data);
    // set the post data
    curl_easy_setopt(curlhandler, CURLOPT_POSTFIELDS, data.c_str());
    // set custom headers for binary data
    struct curl_slist *headers=nullptr;
    headers = curl_slist_append(headers,
                                "Content-Type: application/octet-stream");
    // set the size of the postfields data
    curl_easy_setopt(curlhandler, CURLOPT_POSTFIELDSIZE, data.size());
    // pass list of custom made headers
    curl_easy_setopt(curlhandler, CURLOPT_HTTPHEADER, headers); 
    // perform the post
    res = curl_easy_perform(curlhandler);
    if (res != CURLE_OK) {
      std::stringstream errorstr;
      errorstr << "SURI Post Failure -- " << posturi.getURI() << " -- "
                << res << ": " << curl_easy_strerror(res);
      SLogger::global().addMessage(errorstr.str(),
                                    SLogLevels::MEDIUM);
    }
    /* always cleanup */
    curl_easy_cleanup(curlhandler);
    curl_slist_free_all(headers);
  }
  src->refresh(false); // busy end
  return res == 0;
}
#else
bool SURI::SURIPIMPL::postRemoteObject(SURI*,const std::string&,
                                       const std::string& ,
                                       const std::string& ,
                                       const std::string& ) {
  throw SURIRemoteException();
}
#endif
