/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::sysInfo:
 * The Simulacrum System Information Methods
 * Author: M.A.Hicks
 */

#ifndef SIMULACRUM_SYSINFO
#define SIMULACRUM_SYSINFO

#include <Core/definitions.h>
#include <string>
#include <vector>
#include <ctime>

namespace Simulacrum {

  class NNode;

  class SIMU_API sysInfo {
  public:
    // General system properties
    static const bool        isBigEndian;
    static std::string       systemType();
    static void              overrideSystemType(const std::string&);
    static std::string       systemName();
    static
    std::vector<std::string> systemAddresses(bool ipv6);
    // Resource related
    static int               systemConcurrency();
    static void              setSystemConcurrency(uint16_t);
    static uint64_t          systemMemory();
    static uint64_t          systemMemoryUsed();
    static uint64_t          systemDiskCacheSize();
    static uint64_t          systemMemoryAvailable();
    static std::string       sysInfoString();
    static bool              preferRealloc();
    static void              setPreferRealloc(bool);
    // FS related
    static std::string       homeDIR();
    static void              overrideHomeDIR(const std::string&);
    static std::string       simulacrumDIR();
    static void              initDiskStore();
    static bool              loadConfig(NNode&);
    static bool              storeConfig(NNode&);
    // time related
    typedef std::time_t      stime_t;
    static stime_t           time();
    static stime_t           localTime();
    static std::string       timeString(stime_t);
    static void              sleep(long unsigned ms);
    // UUIDs
    static std::string       genGUIDString();
  };

}

#endif // SIMULACRUM_SYSINFO
