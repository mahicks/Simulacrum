/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::sysInfo:
 * The Simulacrum System Information Methods
 * Author: M.A.Hicks
 */

#include <sstream>
#include <ctime>
#include <stdlib.h>
#include "sysInfo.h"
#include <Toolbox/SFile/sfile.h>
#include <Core/NNode/nnode.h>
#include <Core/slockable.h>
#include <Core/sresource.h>
#include <Core/salgorithm.h>
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/SPlugins/splugins.h>
#include <thread>
#include <chrono>

extern "C"
{
// mem info
#ifdef __unix__
#include <unistd.h>
#endif
#ifdef _WIN32
#include <windows.h>
#endif
}

/* UUIDS */
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

/* Network related */
#include <boost/asio/ip/host_name.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/io_service.hpp>

using namespace Simulacrum;

#ifdef MOBILE
static std::string systemtype = "mobile";
#else
static std::string systemtype = "desktop";
#endif

std::string sysInfo::systemType() {
  return systemtype;
}

void sysInfo::overrideSystemType(const std::string& newtype) {
  systemtype = newtype;
}

std::string sysInfo::systemName() {
  return boost::asio::ip::host_name();
}

std::vector< std::string > sysInfo::systemAddresses(bool ipv6) {
  using boost::asio::ip::tcp;
  boost::asio::io_service io_service;
  tcp::resolver resolver(io_service);
  tcp::resolver::query query(boost::asio::ip::host_name(),"");
  tcp::resolver::iterator it=resolver.resolve(query);
  std::vector< std::string > result;
  while(it!=tcp::resolver::iterator()) {
      boost::asio::ip::address addr=(it++)->endpoint().address();
      if(addr.is_v6()) {
        if (ipv6)
          result.push_back(addr.to_string());
      }
      else {
        result.push_back(addr.to_string());
      }
  }
  return result;
}

static bool doIsBigEndian() {
  union {
    uint32_t i;
    char c[4];
  } bint = {0x01020304};
  return bint.c[0] == 1; 
}

const bool sysInfo::isBigEndian = doIsBigEndian();
static uint16_t SystemConcurrency = std::thread::hardware_concurrency();

int sysInfo::systemConcurrency() {
  int result = SystemConcurrency;
  // always admit one core
  if (result == 0)
    result = 1;
  return result;
}

void sysInfo::setSystemConcurrency(uint16_t nsystcon) {
  SystemConcurrency = nsystcon;
  std::stringstream logmsg;
  logmsg << "sysInfo::setSystemConcurrency: System Concurrency "
         << "manually adjusted to " << SystemConcurrency;
  SLogger::global().addMessage(logmsg.str());
}

static std::string procValLookup(const std::string &procmeminfo,
                                 const std::string &valname) {
  std::string cacheval;
  std::size_t findres = procmeminfo.find(valname + ":");
  if (findres != std::string::npos) {
    findres = procmeminfo.find(":",findres);
    if (findres != std::string::npos) {
      // skip to the value
      findres++;
      char charval = procmeminfo[findres];
      while (findres < procmeminfo.size() && charval == ' ') {
        findres++;
        charval = procmeminfo[findres];
      }
      // read the value
      while (findres < procmeminfo.size() && charval != ' ') {
        cacheval.push_back(charval);
        findres++;
        charval = procmeminfo[findres];
      }
    }
  }
  return cacheval;
}

uint64_t sysInfo::systemMemory() {
  uint64_t result = 0;
#ifdef __unix__
  // use /proc/meminfo -- seems more reliable on different unixes
  SFile procmem("/proc/meminfo");
  if (procmem.exists()) {
    std::string cacheval = procValLookup(procmem.toString(),"MemTotal");
    if (cacheval.size() > 0) {
      std::stringstream cachesizeconv(cacheval);
      cachesizeconv >> result;
      result = result << 10;
    }
  }
#elif _WIN32
  MEMORYSTATUSEX status;
  status.dwLength = sizeof(status);
  GlobalMemoryStatusEx(&status);
  result = status.ullTotalPhys;
#endif
  return result;
}

uint64_t sysInfo::systemMemoryUsed() {
  uint64_t result = 0;
#ifdef __unix__
  SFile procmem("/proc/meminfo");
  if (procmem.exists()) {
    std::string cacheval = procValLookup(procmem.toString(),"MemFree");
    if (cacheval.size() > 0) {
      std::stringstream cachesizeconv(cacheval);
      cachesizeconv >> result;
      result = result << 10;
      result = systemMemory() - result;
    }
  }
#elif _WIN32
  MEMORYSTATUSEX memInfo;
  memInfo.dwLength = sizeof(MEMORYSTATUSEX);
  GlobalMemoryStatusEx(&memInfo);
  result = memInfo.ullTotalPhys - memInfo.ullAvailPhys;
#endif
  return result;
}

uint64_t sysInfo::systemMemoryAvailable() {
  uint64_t result = 0;
#ifdef __unix__
  SFile procmem("/proc/meminfo");
  if (procmem.exists()) {
    std::string cacheval = procValLookup(procmem.toString(),"MemAvailable");
    if (cacheval.size() > 0) {
      std::stringstream cachesizeconv(cacheval);
      cachesizeconv >> result;
      result = result << 10;
    }
  }
#endif
  // perform this check and possible action in all cases since even on unix
  // the 'MemAvailable' entry in proc may not be present on all platforms
  if (result == 0) {
    result = (systemMemory() - (systemMemoryUsed() - systemDiskCacheSize()));
  }
  return result;
}

uint64_t sysInfo::systemDiskCacheSize() {
  uint64_t result = 0;
#ifdef __unix__
  SFile procmem("/proc/meminfo");
  if (procmem.exists()) {
    std::string cacheval = procValLookup(procmem.toString(),"Cached");
    if (cacheval.size() > 0) {
      std::stringstream cachesizeconv(cacheval);
      cachesizeconv >> result;
      result = result << 10;
    }
  }
#endif
  return result;
}

std::string sysInfo::sysInfoString() {
  std::stringstream outputstream;
  // do not include a trailing new-line
  outputstream << "System Concurrency: " << systemConcurrency() << std::endl;
  outputstream << "System Memory: " << (systemMemory()/1024/1024) << " MiB";
  return outputstream.str();
}

static bool prefer_realloc = false;

bool sysInfo::preferRealloc() {
  return prefer_realloc;
}

void sysInfo::setPreferRealloc(bool nval) {
  prefer_realloc = nval;
  std::stringstream logmsg;
  logmsg << "PreferRealloc set to: " << prefer_realloc;
  SLogger::global().addMessage(logmsg.str());
}

#ifdef _WIN32
static std::string homedir = getenv("APPDATA");
static const std::string SDIRName = "Simulacrum";
#else
#ifdef ANDROID
static std::string homedir = "/sdcard";
static const std::string SDIRName = "Simulacrum";
#else
static std::string homedir = getenv("HOME");
static const std::string SDIRName = ".Simulacrum";
#endif 
#endif
static const std::string SConfigFileName = "simulacrum.xml";
static SLockable SConfigLoadStorelock;

std::string sysInfo::homeDIR() {
  return homedir;
}

void sysInfo::overrideHomeDIR(const std::string& newhome) {
  homedir = newhome;
}

std::string sysInfo::simulacrumDIR() {
  SURI simuhome;
  simuhome = homeDIR();
  simuhome.addComponentBack(SDIRName);
  return simuhome.getURI();
}

void sysInfo::initDiskStore() {
  SURI  dirpath = simulacrumDIR();
  SFile tester(dirpath.getURI());
  if (!tester.exists()) {
    SLogger::global().addMessage
    ("sysInfo::initDiskStore: Creating disk structure:" + tester.getLocation());
    tester.mkdir();
    dirpath.addComponentBack(SPluginManager::pluginsPath());
    tester = dirpath.getURI();
    tester.mkdir();
    dirpath = simulacrumDIR();
    dirpath.addComponentBack(SResourceLoader::userLoadersPath());
    tester = dirpath.getURI();
    tester.mkdir();
    dirpath = simulacrumDIR();
    dirpath.addComponentBack("Layouts");
    tester = dirpath.getURI();
    tester.mkdir();
    dirpath = simulacrumDIR();
    dirpath.addComponentBack(SResourceSLua::userResourcesPath());
    tester = dirpath.getURI();
    tester.mkdir();
    dirpath = simulacrumDIR();
    dirpath.addComponentBack(SAlgorithmSLua::userAlgorithmsPath());
    tester = dirpath.getURI();
    tester.mkdir();
    dirpath = simulacrumDIR();
    dirpath.addComponentBack(SPluginManager::includesPath());
    tester = dirpath.getURI();
    tester.mkdir();
  }
}

bool sysInfo::loadConfig(NNode& target) {
  SURI fileuri;
  fileuri = simulacrumDIR();
  fileuri.addComponentBack(SConfigFileName);
  target.clear();
  SConfigLoadStorelock.lock();
  target.loadFromXMLFile(fileuri.getURI());
  SConfigLoadStorelock.unlock();
  return target.good();
}

bool sysInfo::storeConfig(NNode& source) {
  SURI fileuri;
  fileuri = simulacrumDIR();
  fileuri.addComponentBack(SConfigFileName);
  SConfigLoadStorelock.lock();
  source.storeToXMLFile(fileuri.getURI());
  SConfigLoadStorelock.unlock();
  return source.good();
}

sysInfo::stime_t sysInfo::time() {
  return std::time(nullptr);
}

sysInfo::stime_t sysInfo::localTime() {
  stime_t timetmp = time();
  std::tm *timeinfo = std::localtime(&timetmp);
  return std::mktime(timeinfo);
}

std::string sysInfo::timeString(stime_t ltime) {
  std::string timebuff;
  timebuff.resize(81);
  std::tm* timeinfo = std::localtime(&ltime);
  if (timeinfo != nullptr) {
#ifdef _WIN32
    // Currently Windows version of strftime prints very long zone name, skip it
    const char* tformat = "%Y-%m-%dT%H:%M:%S";
#else
    const char* tformat = "%Y-%m-%dT%H:%M:%S%z";
#endif
    timebuff.resize(std::strftime(&timebuff[0],80,tformat,timeinfo));
  }
  return std::string(timebuff);
}

void sysInfo::sleep(long unsigned ms) {
  std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

std::string sysInfo::genGUIDString() {
  std::stringstream outstr;
  boost::uuids::uuid uuid = boost::uuids::random_generator()();
  outstr << uuid;
  return outstr.str();
}
