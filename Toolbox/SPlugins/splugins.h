/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SPlugins
 * Interface specification for SPlugins (Simulacrum Plugin Manager)
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_SPLUGINS
#define SIMULACRUM_SPLUGINS

#include <vector>
#include <typeinfo>

#include <Core/definitions.h>

namespace Simulacrum {

  typedef void* splugininst_t;

  class SIMU_API SPluginFactory {
  public:
    virtual              ~SPluginFactory();
    virtual std::string   name          ()=0;
    virtual std::string   type          ()=0;
    virtual splugininst_t factory       ()=0;
  };

  template <class plugin_s, class plugin_t>
  class SPluginConvenience : SPluginFactory {
  private:
    std::string Name;
  public:
                          SPluginConvenience(const std::string& nname) :
                                                      Name(nname) {   }
    virtual              ~SPluginConvenience() { }
    virtual std::string   name   () { return Name; }
    virtual std::string   type   () 
                                { return std::string(typeid(plugin_s).name()); }
    virtual splugininst_t factory() { return (void*)new plugin_t; }
  };

  class SIMU_API SPluginManager {
  private:
    struct                     PluginPrivates;
    PluginPrivates            *PrivateData;
  public:
                               SPluginManager();
                              ~SPluginManager();
    static SPluginManager&     global();
    static std::string         pluginsPath();
    static std::string         includesPath();
    bool                       isLoaded() const;
    int                        loadAll (const std::string& source = "");
    void                       clearAll();
    bool                       addPlugin(const std::string& path);
    void                       addPlugin(SPluginFactory*);
    bool                       removePlugin(const std::string& ptypename,
                                            const std::string& pluginname);
    std::vector< std::string > pluginTypes() const;
    std::vector< std::string > plugins(const std::string& ptypename) const;
    bool                       hasPlugin(const std::string& ptypename,
                                         const std::string& pluginname);
    splugininst_t              getPluginInstance(const std::string& ptypename,
                                                 const std::string& pluginname);
    template <class plugin_t>
    std::vector< std::string > plugins() {
      return plugins(std::string(typeid(plugin_t).name()));
    }
    template <class plugin_t>
    bool                       hasPlugin(const std::string &pname) {
      return static_cast<plugin_t*>(hasPlugin(
                                   std::string(typeid(plugin_t).name()),pname));
    }
    template <class plugin_t>
    plugin_t*                  getPluginInstance(const std::string &pname) {
      return static_cast<plugin_t*>(getPluginInstance(
                                   std::string(typeid(plugin_t).name()),pname));
    }
  };
  
  /* Macros for the methods that must be present in plugin object files */

  // int SIMU_API abiVersion(); SPluginFactory* SIMU_API getPluginFactory();
#define SPLUGIN_DECL(x) extern "C" int SIMU_API abiVersion() { return \
(SIMULACRUM_VERSION_MAJOR * 10) + SIMULACRUM_VERSION_MINOR; } \
extern "C" void SIMU_API registerPlugin(SPluginManager* pmngr) { x(pmngr); }

}

#endif // SPLUGINS
