/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::Tools:SPlugins
 * Implementation for SPlugins (Simulacrum Plugin Manager)
 * Author: M.A.Hicks
 */

#include <map>

#include "splugins.h"
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SLogger/slogger.h>
#include <Core/sresource.h>
#include <Core/salgorithm.h>

using namespace Simulacrum;

#ifdef _WIN32
#include <windows.h>
static std::wstring toWString(std::string orig) {
  std::wstring result;
  result.assign(orig.begin(),orig.end());
  if (result.size() < 1)
    result.resize(1);
  return result;
}
#define LOADEDOBJ HMODULE
#define LOADOBJ(x) LoadLibrary(&(toWString( x )[0]))
#define LOADERROR ""
#define UNLOADOBJ(x) FreeLibrary(x)
#define SYMREF FARPROC
#define SYMLOOKUP(x,y) GetProcAddress(x,y)
#else
#include <dlfcn.h>
#define LOADEDOBJ void*
#define LOADOBJ(x) dlopen(x,RTLD_NOW|RTLD_GLOBAL)
#define LOADERROR dlerror()
#define UNLOADOBJ(x) dlclose(x)
#define SYMREF void*
#define SYMLOOKUP(x,y) dlsym(x,y)
#endif
typedef int  (*abifunc_t)(void);
typedef void (*regfunc_t)(SPluginManager*);

typedef std::map<std::string,std::map<std::string,SPluginFactory*>> pstore_t;
typedef std::map<std::string,SPluginFactory*>                   pstorelist_t;
typedef std::map<std::string,LOADEDOBJ>                           pobjlist_t;

static SPluginManager GlobalPluginManager;

SPluginFactory::~SPluginFactory() {

}

struct SPluginManager::PluginPrivates {
  bool                    IsLoaded;
  pstore_t                PluginStore;
  pobjlist_t              LoadedObjs;
};

SPluginManager::SPluginManager(): PrivateData(new PluginPrivates) {
  PrivateData->IsLoaded = false;
}

SPluginManager::~SPluginManager() {
  clearAll();
  delete PrivateData;
}

SPluginManager& SPluginManager::global() {
  return GlobalPluginManager;
}

std::string SPluginManager::pluginsPath() {
  return "Plugins";
}

std::string SPluginManager::includesPath() {
  return "Include";
}

bool SPluginManager::isLoaded() const {
  return PrivateData->IsLoaded;
}

namespace Simulacrum {
  class SPluginFactoryLuaResource: public SPluginFactory {
  private:
    std::string Name, LuaSRC;
  public:
    SPluginFactoryLuaResource(const std::string& name, const std::string& src) {
      Name  = name;
      LuaSRC = src;
    }
    virtual              ~SPluginFactoryLuaResource() { }
    virtual std::string   name          () {
      return Name;
    }
    virtual std::string   type          () {
      return std::string(typeid(SResource).name());
    };
    virtual splugininst_t factory       () {
      return (void*) new SResourceSLua(LuaSRC);
    }
  };

  class SPluginFactoryLuaAlgorithm: public SPluginFactory {
  private:
    std::string Name, LuaSRC;
  public:
    SPluginFactoryLuaAlgorithm(const std::string& name, const std::string& src) {
      Name  = name;
      LuaSRC = src;
    }
    virtual              ~SPluginFactoryLuaAlgorithm() { }
    virtual std::string   name          () {
      return Name;
    }
    virtual std::string   type          () {
      return std::string(typeid(SAlgorithm).name());
    };
    virtual splugininst_t factory       () {
      return (void*) new SAlgorithmSLua(LuaSRC);
    }
  };
}

#ifndef NOLUA
int loadLuaResources(SPluginManager& plugman, const std::string &source = "") {
  int plugincount = 0;
  SURI pluginsdir;
  std::string pluginext = ".lua";
  if (source.size() > 0) {
    pluginsdir = source;
  }
  else {
    pluginsdir = sysInfo::simulacrumDIR();
    pluginsdir.addComponentBack(SResourceSLua::userResourcesPath());
  }
  SFile pluginsdirf(pluginsdir.getURI());
  if (pluginsdirf.isDIR()) {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: Loading plugins from: "
                            + pluginsdir.getURI());
    std::vector<std::string> pluginslist = pluginsdirf.getDIRContents();
    SFile filetest;
    for (unsigned i=0; i<pluginslist.size(); i++) {
      filetest = pluginslist[i];
      if (filetest.exists() && (!filetest.isDIR()) &&
          (filetest.getLocation().find(pluginext) != std::string::npos) ) {
        SURI  fileuri = filetest.getLocation();
        std::string pluginname;
        if (fileuri.depth() > 0)
          pluginname = fileuri.getComponent(fileuri.depth() -1);
        else
          pluginname = fileuri.getComponent(0);
        if (pluginname.size() > pluginext.size()) //strip of the ext
          pluginname.resize(pluginname.size() - pluginext.size());
        plugman.addPlugin(new SPluginFactoryLuaResource(pluginname,
                                                        filetest.toString()));
        plugincount++;
      }
    }
  }
  else {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: No plugins directory at: "
                            + pluginsdir.getURI());
  }
  return plugincount;
}

int loadLuaAlgorithms(SPluginManager& plugman, const std::string &source = "") {
  int plugincount = 0;
  SURI pluginsdir;
  std::string pluginext = ".lua";
  if (source.size() > 0) {
    pluginsdir = source;
  }
  else {
    pluginsdir = sysInfo::simulacrumDIR();
    pluginsdir.addComponentBack(SAlgorithmSLua::userAlgorithmsPath());
  }
  SFile pluginsdirf(pluginsdir.getURI());
  if (pluginsdirf.isDIR()) {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: Loading plugins from: "
                            + pluginsdir.getURI());
    std::vector<std::string> pluginslist = pluginsdirf.getDIRContents();
    SFile filetest;
    for (unsigned i=0; i<pluginslist.size(); i++) {
      filetest = pluginslist[i];
      if (filetest.exists() && (!filetest.isDIR()) &&
          (filetest.getLocation().find(pluginext) != std::string::npos) ) {
        SURI  fileuri = filetest.getLocation();
        std::string pluginname;
        if (fileuri.depth() > 0)
          pluginname = fileuri.getComponent(fileuri.depth() -1);
        else
          pluginname = fileuri.getComponent(0);
        if (pluginname.size() > pluginext.size()) //strip of the ext
          pluginname.resize(pluginname.size() - pluginext.size());
        plugman.addPlugin(new SPluginFactoryLuaAlgorithm(pluginname,
                                                        filetest.toString()));
        plugincount++;
      }
    }
  }
  else {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: No plugins directory at: "
                            + pluginsdir.getURI());
  }
  return plugincount;
}
#else
int loadLuaResources(SPluginManager&, const std::string & = "") {
  return 0;
}
int loadLuaAlgorithms(SPluginManager&, const std::string & = "") {
  return 0;
}
#endif

int SPluginManager::loadAll(const std::string& source) {
  SURI pluginsdir;
  int plugincount = 0;
  if (source.size() > 0) {
    pluginsdir = source;
  }
  else {
    pluginsdir = sysInfo::simulacrumDIR();
    pluginsdir.addComponentBack(pluginsPath());
  }
  SFile pluginsdirf(pluginsdir.getURI());
  if (pluginsdirf.isDIR()) {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: Loading plugins from: "
                            + pluginsdir.getURI());
    std::vector<std::string> pluginslist = pluginsdirf.getDIRContents();
    SFile filetest;
    SURI  fileuri;
    for (unsigned i=0; i<pluginslist.size(); i++) {
      filetest = pluginslist[i];
      if (filetest.exists() && (!filetest.isDIR())) {
        if(addPlugin(filetest.getLocation()))
          plugincount++;
      }
    }
    PrivateData->IsLoaded = true;
  }
  else {
    SLogger::global().addMessage
                           ("SPluginManager::loadAll: No plugins directory at: "
                            + pluginsdir.getURI());
  }
  plugincount += loadLuaResources(*this,source);
  plugincount += loadLuaAlgorithms(*this,source);
  return plugincount;
}

void SPluginManager::clearAll() {
  pstore_t tmpstore = PrivateData->PluginStore;
  PrivateData->PluginStore.clear();
  PrivateData->IsLoaded = false;
  for (pstore_t::iterator tit = tmpstore.begin();tit != tmpstore.end();tit++) {
    for (pstorelist_t::iterator pit = tit->second.begin();
         pit != tit->second.end();
         pit++) {
      delete pit->second;
    }
  }
  for (pobjlist_t::iterator obj=PrivateData->LoadedObjs.begin();
       obj != PrivateData->LoadedObjs.end(); obj++) {
    UNLOADOBJ(obj->second);
  }
  PrivateData->LoadedObjs.clear();
}

bool SPluginManager::addPlugin(const std::string& path) {
  bool result = false;
  SFile newplugin = path;
  if (PrivateData->LoadedObjs.count(path) == 0) {
    if (newplugin.exists()) {
      LOADEDOBJ newpluginobj = LOADOBJ( newplugin.getLocation().c_str() );
      if (newpluginobj != nullptr) {
        PrivateData->LoadedObjs.insert(std::make_pair(path,newpluginobj));
        abifunc_t abicheck   = (abifunc_t) SYMLOOKUP(newpluginobj,"abiVersion");
        regfunc_t pluginreg  = (regfunc_t) SYMLOOKUP(newpluginobj,
                                                    "registerPlugin");
        if ((abicheck != nullptr) && (pluginreg != nullptr)) {
          int pluginabiversion = abicheck();
          if (pluginabiversion == 
              ((SIMULACRUM_VERSION_MAJOR * 10) + SIMULACRUM_VERSION_MINOR) ) {
            SLogger::global().addMessage
                            ("SPluginManager::addPlugin: Registering: "
                              + newplugin.getLocation());
            pluginreg(this);
          }
          else {
            SLogger::global().addMessage
                            ("SPluginManager::addPlugin: Error, ABI mismatch: "
                              + newplugin.getLocation());
          }
        }
        else {
          SLogger::global().addMessage
                    ("SPluginManager::addPlugin: Error, missing entry symbols: "
                              + newplugin.getLocation());
        }
      }
      else {
        SLogger::global().addMessage
                            ("SPluginManager::addPlugin: Error loading: "
                              + newplugin.getLocation() + ", with error: "
                              + LOADERROR);
      }
    }
    else {
      SLogger::global().addMessage
                           ("SPluginManager::addPlugin: Plugin does not exist: "
                              + newplugin.getLocation());
    }
  }
  else {
    SLogger::global().addMessage
                           ("SPluginManager::addPlugin: Plugin already loaded: "
                              + newplugin.getLocation());
  }
  return result;
}

void SPluginManager::addPlugin(SPluginFactory* newplugin) {
  SLogger::global().addMessage
                           ("SPluginManager::addPlugin: Plugin added: "
                            + newplugin->name());
  if (PrivateData->PluginStore.count(newplugin->type()) == 0) {
    PrivateData->PluginStore.insert
                             (std::make_pair(newplugin->name(),pstorelist_t()));
  }
  if (PrivateData->PluginStore[newplugin->type()].count(newplugin->name()) > 0){
    PrivateData->PluginStore[newplugin->type()][newplugin->name()] = newplugin;
  }
  else {
    PrivateData->PluginStore[newplugin->type()].insert
                                  (std::make_pair(newplugin->name(),newplugin));
  }
}

bool SPluginManager::removePlugin(const std::string& ptypename,
                                  const std::string& pluginname) {
  bool result = false;
  if (PrivateData->PluginStore.count(ptypename) > 0) {
    if (PrivateData->PluginStore[ptypename].count(pluginname)  > 0) {
      delete PrivateData->PluginStore[ptypename][pluginname];
      PrivateData->PluginStore[ptypename].erase(pluginname);
      if (PrivateData->PluginStore[ptypename].size() == 0) {
        PrivateData->PluginStore.erase(ptypename);
      }
      result = true;
    }
  }
  return result;
}

std::vector< std::string > SPluginManager::pluginTypes() const {
  std::vector< std::string > result;
  pstore_t::iterator         pit;
  for (pit=PrivateData->PluginStore.begin();
       pit != PrivateData->PluginStore.end();
       pit++) {
    result.push_back(pit->first);
  }
  return result;
}

std::vector< std::string > SPluginManager::plugins
                                          (const std::string& ptypename) const {
  std::vector< std::string > result;
  if (PrivateData->PluginStore.count(ptypename)) {
    pstorelist_t &pluginlist = PrivateData->PluginStore[ptypename];
    for (pstorelist_t::iterator pit = pluginlist.begin();
         pit != pluginlist.end();
         pit++) {
      result.push_back(pit->first);
    }
  }
  return result;
}

bool SPluginManager::hasPlugin(const std::string& ptypename,
                               const std::string& pluginname) {
  bool result = false;
  if (PrivateData->PluginStore.count(ptypename) > 0) {
    if (PrivateData->PluginStore[ptypename].count(pluginname)  > 0) {
      result = true;
    }
  }
  return result;
}

splugininst_t SPluginManager::getPluginInstance(const std::string& ptypename,
                                                const std::string& pluginname) {
  return PrivateData->PluginStore[ptypename][pluginname]->factory();
}
