/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::ResourceAllocator
 * Interface specification for SResource allocation
 * Author: M.A.Hicks
 */
#ifndef SIMULACRUM_RESOURCE_ALLOC
#define SIMULACRUM_RESOURCE_ALLOC

#include <Core/sresource.h>
#include <string>

namespace Simulacrum {

  class SIMU_API SResourceAllocator {
  public:
    static SResource* SResourceFromPath(const std::string &);
                      // convenience wrapper for the above -- throws
    static void       loadInto (SSpace& targ, const std::string& uri,
                                const std::string& respath = "");
  };

}

#endif
