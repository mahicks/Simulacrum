/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::ResourceAllocator
 * Implementation for SResource allocation
 * Author: M.A.Hicks
 */

#include "sresourceallocator.h"
#include <Core/sfileio.h>
#include <Resources/DICOM/sdicom.h>
#include <Resources/DICOM/sarch.h>
#include <Resources/spbm.h>
#include <Resources/sj2k.h>
#include <Resources/spng.h>
#include <Resources/sjpeg.h>
#include <Core/NNode/nnode.h>
#include <Toolbox/SPlugins/splugins.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>

#include <vector>

using namespace Simulacrum;

static bool handleDecompress(SResource &res) {
  bool did_decom = false;
  if (res.isCompressed()) {
    SURI src;
    std::string ext;
    const unsigned int max_uniq_path = 32;
    // set a temporary file location
    SURI dst(SFile::localTempDIR());
    // try to preserve file extension
    src.setSeparator(".");
    src.setURI(res.getLocation());
    if (src.depth() > 1) { // probably has an ext
      ext = src.getComponent(src.depth()-1);
      src.deleteComponent(src.depth()-1);
      if (ext == "gz") { // get rid of gz
        ext = "";
        if (src.depth() > 1) {
          ext = src.getComponent(src.depth()-1);
          src.deleteComponent(src.depth()-1);
        }
      }
    }
    // try to construct unique temp file
    std::string tmpuri = sysInfo::genGUIDString();
    if (tmpuri.size() > max_uniq_path) {
      tmpuri = tmpuri.substr(tmpuri.size() - max_uniq_path);
    }
    dst.addComponentBack("stmp-" + tmpuri  +"."+ext);
    // time to decompress
    try {
      res.decompress(dst.getURI());
      res.setLocation(dst.getURI());
      did_decom = true;
    }
    catch (std::exception &e) {
      SLogger::global().addMessage("SResourceAllocation: error handling \
decompression of file: " + res.getLocation() + " with apparent error: " +
      e.what());
      // cleanup the dirty file
      SFile cleanup(dst.getURI());
      cleanup.deleteFromDisk();
    }
  }
  return did_decom;
}

/* Try to guess what kind of file the filepath points to, instantiate it
 * and return a pointer to the newly allocated object.Return a null pointer
 * in the case of failure (possible consider throwing and exception?
 */
SResource* SResourceAllocator::SResourceFromPath(const std::string& filepath) {
  std::vector<SResource*> knownResources;
  SResource*              foundResource = nullptr;
  bool                    trieddecom    = false;
  std::string             respath       = filepath;
  /*----------------------------------------------------------------------------
   * Push instances of all auto-matically determinable SResources onto the
   * back of the vector. NOTE: precedence may be important (derived class last)
   *--------------------------------------------------------------------------*/
  // first plugins
  std::vector<std::string> resourceplugins =
                                  SPluginManager::global().plugins<SResource>();
  for (unsigned p=0; p < resourceplugins.size(); p++)
    knownResources.push_back(SPluginManager::global().getPluginInstance
                                               <SResource>(resourceplugins[p]));
  // now builtins
  knownResources.push_back((SResource*)new SDICOMArch());
  knownResources.push_back((SResource*)new SDICOM());
  knownResources.push_back((SResource*)new SPBM());
  knownResources.push_back((SResource*)new NNodeResource());
  knownResources.push_back((SResource*)new SJ2K());
  knownResources.push_back((SResource*)new SPNG());
  knownResources.push_back((SResource*)new SJPEG());
  /*--------------------------------------------------------------------------*/
  for (std::vector<SResource*>::iterator it = knownResources.begin();
       it != knownResources.end(); it++) {
    (*it)->setLocation(respath);
    if (!trieddecom) {
      if(handleDecompress(*(*it))) {
        respath      = (*it)->getLocation();
      }
      trieddecom = true;
    }
    if ((*it)->isValid()) {
      foundResource = *it;
      /* Now clean up the memory */
      for (std::vector<SResource*>::iterator it2 = ++it;
           it2 != knownResources.end(); it2++)
        delete *it2;
      break;
    }
    else {
      delete *it;
    }
  }
  /* and return the pointer */
  return foundResource;
}

// Convenience wrapper for the above -- will throw
void SResourceAllocator::loadInto(SSpace& targ, const std::string& uri,
                                  const std::string& respath) {
  // get a resource handler for the input
  SPtr<SResource> input(SResourceAllocator::SResourceFromPath(uri));
  // perform some checks (abusing the non-zero error catching of excepter)
  excepter(!input,"Could not instantiate resource from input");
  input->refresh(true); // for DICOM
  excepter(!input->hasSSpace(respath),"No useable data in input");
  // load data
  input->getSSpaceInto(targ);
}

