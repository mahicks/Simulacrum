/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>

#include "cmdProgress.h"

using namespace Simulacrum;

void cmdProgress::progressOut(int iprogress, int width) {
  static char spinner;
  static int progress;
  if (iprogress >= 0) {
    // adjust the spinner
    switch (spinner) {
      case '|':
        spinner = '/';
        break;
      case '/':
        spinner = '-';
        break;
      case '-':
        spinner = '\\';
        break;
      default:
        spinner = '|';
        break;
    }
    // update the spinner
    std::cout << '\r' << spinner << std::flush;
    // output progress, if it's new
    if (iprogress != progress) {
    progress = iprogress;
    if (progress > 100)
      progress = 100;
    // calc the progess chars
    int stepsout = (int)(((float)width) *((float)progress/100));
    // output the progress bar
    std::cout << " [";
    for (int i=0;i<stepsout;i++)
      std::cout << '#';
    for (int i=stepsout;i<width;i++)
      std::cout << ' ';
    std::cout << "] " << progress << "% ";
    }

  } else {
    // progess of < 0 indicates (de-)initialisation
    progress = 0;
    spinner = '|';
    std::cout << std::endl;
  }
}
