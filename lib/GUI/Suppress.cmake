# compile flags
if (SUPPRESS_EXTERNAL_WARNINGS)
  if (NOT MSVC)
    add_compile_options(-Wno-deprecated-declarations)
  endif (NOT MSVC)
endif (SUPPRESS_EXTERNAL_WARNINGS)
