/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::NetworkIO:
 * Interface specification for NetworkIO
 * Various individual file read/writers (PPM, DICOM ...)
 * Author: M.A.Hicks
 */

#ifndef SNIO
#define SNIO

#include <string>
#include <sstream>
#include <stdint.h>
#include <vector>
#include <memory>
#include <thread>
#include <cstdlib>
#include <iostream>

#include <Core/error.h>
#include <Core/definitions.h>
#include <Core/SPool/SPool.h>

namespace Simulacrum {

  class ssocket_t;
#define ssocket_t_impl class Simulacrum::ssocket_t: public boost::asio::ip::tcp::socket {public: ssocket_t(boost::asio::io_service &ios): boost::asio::ip::tcp::socket(ios) {}};

  /* SClient -- generic client class for open/closing and
   * writing to a connection */
  class SIMU_API SClient {
  public:
                       SClient();
                      ~SClient();
    bool               open   (const std::string& address, short port);
    bool               isOpen () const;
    void               close  ();
    const std::string& address() const;
    short              port   () const;
    ssocket_t&         socket ();
    int                read   (char* data, long long int length);
    int                write  (const char* data, long long int length);
  private:
    struct SClientPIMPL;
    SClientPIMPL *PrivateData;
  };

  /* SServer -- Generic connection reciever and handler-spawner */
  class SIMU_API SServer {
  public:
             SServer                 ();
    virtual ~SServer                 ();
    void     start                   ();
    void     stop                    ();
    void     setPort                 (short port);
    short    port                    ();
    void     setMaxActiveHandlers    (unsigned long);
    unsigned
    long     maxConnections          ();
    void     wait                    ();
    bool     isRunning               () const;
  protected:
    virtual
    void     handleConnection        (ssocket_t&)=0;
  private:
    class SServerPIMPL;
    SServerPIMPL *PrivateData;
  };

//-----------------------------------------------------------------------------


  typedef uint16_t SNetIDWidth_t;
  typedef uint32_t SNetDataWidth_t;

  /* SNetMessage -- The Simulacrum Network Message Model */
  class SIMU_API SNetMessage {
  public:
                       SNetMessage();
    virtual           ~SNetMessage();
    SNetIDWidth_t      getSessionID();
    SNetIDWidth_t      getItemID();
    SNetIDWidth_t      getCommand();
    SNetIDWidth_t      getDataType();
    SNetDataWidth_t    getDataLength();
    char*              getData();
    void               setSessionID(SNetIDWidth_t);
    void               setItemID(SNetIDWidth_t);
    void               setCommand(SNetIDWidth_t);
    void               setDataType(SNetIDWidth_t);
    void               setData(SNetDataWidth_t size, char* data,
                               bool copy=true);
    std::string        toString();
    void               copy(SNetMessage&,bool deep=true);
    void               write(ssocket_t &target);
    void               read(ssocket_t &source);
  private:
    class              SNetMessagePIMPL;
    SNetMessagePIMPL  *PrivateData;
  };

}

#endif
