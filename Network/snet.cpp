/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::NetworkIO:
 * Implemenation of NetworkIO
 * Various individual file read/writers (PPM, DICOM ...)
 * Author: M.A.Hicks
 */

#include "snet.h"

#include <Toolbox/SLogger/slogger.h>

#include <thread>
#include <mutex>
using namespace std;
#include <boost/asio.hpp>
using boost::asio::ip::tcp;

using namespace Simulacrum;
ssocket_t_impl // implementation for ssocket
static SNetDataWidth_t decode_header(char*, SNetMessage&);
static void            encode_header(char*, SNetMessage&);

/*------------------------------------------------------------------------------
 *                              SNet::SServer
 *------------------------------------------------------------------------------
 */

struct SClient::SClientPIMPL {
  std::string                         Address;
  short                               Port;
  shared_ptr<boost::asio::io_service> IO_Service;
  shared_ptr<ssocket_t>               IO_Socket;
};

SClient::SClient(): PrivateData(new SClientPIMPL) {
  PrivateData->IO_Service = shared_ptr<boost::asio::io_service>
                                                (new boost::asio::io_service());
  PrivateData->IO_Socket  = shared_ptr<ssocket_t>
                                  (new ssocket_t(*(PrivateData->IO_Service)));
}

SClient::~SClient() {
  close();
  delete PrivateData;
}

bool SClient::open(const std::string& address, short int port) {
  bool res = false;
  tcp::resolver::iterator iterator;
  close();
  PrivateData->Address = address;
  PrivateData->Port    = port;
  std::stringstream conv;
  conv << port;
  try {
    tcp::resolver resolver(*(PrivateData->IO_Service));
    tcp::resolver::query query(tcp::v4(), address, conv.str());
    iterator = resolver.resolve(query);
    PrivateData->IO_Socket =
        shared_ptr<ssocket_t>(new ssocket_t(*(PrivateData->IO_Service)));
  }
  catch (std::exception &e) {
    SLogger::global().addMessage("SClient::open: " + std::string(e.what()));
    close();
    return res;
  }
  try {
    PrivateData->IO_Socket->connect(*iterator);
  }
  catch (std::exception &e) {
    SLogger::global().addMessage("SClient::open: " + std::string(e.what()));
    close();
    return res;
  }
  if (!PrivateData->IO_Socket->is_open()) {
    PrivateData->IO_Socket->close();
  }
  else {
    res = true;
  }
  return res;
}

bool SClient::isOpen() const {
  bool res = false;
  try {
    if (PrivateData->IO_Socket != nullptr)
      if (PrivateData->IO_Socket->is_open())
        res = true;
  }
  catch (std::exception& e) {
    SLogger::global().addMessage("SClient:isOpen: " + std::string(e.what()));
  }
  return res;
}

void SClient::close() {
  if (isOpen()) {
    try {
      PrivateData->IO_Socket->shutdown
                                  (boost::asio::ip::tcp::socket::shutdown_both);
    }
    catch (std::exception&e) {
      SLogger::global().addMessage("SClient::close: " + std::string(e.what()));
    }
    try {
      PrivateData->IO_Socket->close();
    }
    catch (std::exception&e) {
      SLogger::global().addMessage("SClient::close: " + std::string(e.what()));
    }
  }
}

const std::string& SClient::address() const {
  return PrivateData->Address;
}

short int SClient::port() const {
  return PrivateData->Port;
}

ssocket_t& SClient::socket() {
  return *(PrivateData->IO_Socket.get());
}

int SClient::read(char* data, long long int length) {
  int readsize = 0;
  if (isOpen()) {
    readsize = boost::asio::read(*(PrivateData->IO_Socket),
                                 boost::asio::buffer(data,length));
  }
  return readsize;
}

int SClient::write(const char* data, long long int length) {
  int writesize = 0;
  if (isOpen()) {
    writesize = boost::asio::write(*(PrivateData->IO_Socket),
                                   boost::asio::buffer(data,length));
  }
  return writesize;
}

/*------------------------------------------------------------------------------
 *                              SNet::SServer
 *------------------------------------------------------------------------------
 */

class SServer::SServerPIMPL {
public:
  SServerPIMPL(SServer& parent): Parent(&parent),
                                 IO_Service(new boost::asio::io_service()) { };
  void     handleConnection_(shared_ptr<ssocket_t>);
  void     handle_accept    (shared_ptr<ssocket_t>,tcp::acceptor*);
  void     listen_accept    (tcp::acceptor*);
  void     listen_server    ();
  SServer *Parent;
  short    ListenPort;
  shared_ptr<boost::asio::io_service>
           IO_Service;
  thread   Server_Thread;
  SPool    Handlers;
};

void SServer::SServerPIMPL::handleConnection_
                                        (shared_ptr<ssocket_t> newsock) {
  Parent->handleConnection(*newsock);
}

void SServer::SServerPIMPL::handle_accept(shared_ptr<ssocket_t> newsock,
                            tcp::acceptor* Acceptor) {
  listen_accept(Acceptor);
  Handlers.addJob(bind
                      (&SServer::SServerPIMPL::handleConnection_,this,newsock));
}

void SServer::SServerPIMPL::listen_accept(tcp::acceptor* Acceptor) {
  shared_ptr<ssocket_t> newsock(new ssocket_t(*IO_Service));
  Acceptor->async_accept(*newsock,
        bind(&SServer::SServerPIMPL::handle_accept,this,newsock,Acceptor));
}

void SServer::SServerPIMPL::listen_server() {
  try {
    tcp::acceptor Acceptor(*IO_Service, tcp::endpoint(tcp::v4(), ListenPort));
    listen_accept(&Acceptor);
    IO_Service->run(); // blocks until IO_Server is stopped
    Acceptor.close();
  }
  catch (std::exception &e) {
    SLogger::global().addMessage(std::string("SServer: listening error -- ")
                                 + e.what(),SLogLevels::HIGH_SYNC);
    // join myself
    thread selfjoin( bind(&thread::join,&Server_Thread) );
    selfjoin.detach();
  }
}

SServer::SServer(): PrivateData(new SServerPIMPL(*this)) {
  setPort(1104);
}

SServer::~SServer() {
  stop();
  delete PrivateData;
}

void SServer::start() {
  std::stringstream msg;
  if (PrivateData->Server_Thread.joinable())
    return;
  msg << "SServer: Starting SServer on " << boost::asio::ip::host_name()
      << ", port " << PrivateData->ListenPort;
  SLogger::global().addMessage(msg.str());
  msg.str("");
  msg.clear();
  msg << "SServer: Maximum active handlers:  "
      << PrivateData->Handlers.poolSize();
  SLogger::global().addMessage(msg.str());
  PrivateData->Server_Thread =
  thread(bind(&SServer::SServerPIMPL::listen_server,PrivateData));
  SLogger::global().addMessage("SServer: Startup complete");
}

void SServer::stop() {
  std::stringstream msg;
  msg << "SServer: Stopping SServer on port " << PrivateData->ListenPort;
  SLogger::global().addMessage(msg.str());
  PrivateData->IO_Service->stop();
  wait();
  PrivateData->Handlers.wait();
  PrivateData->IO_Service =
        shared_ptr<boost::asio::io_service>(new boost::asio::io_service());
  SLogger::global().addMessage("SServer: Stopping complete");
}

void SServer::setPort(short int port) {
  PrivateData->ListenPort = port;;
}

short int SServer::port() {
  return PrivateData->ListenPort;
}

void SServer::setMaxActiveHandlers(long unsigned int mconn) {
  PrivateData->Handlers.setPoolSize(mconn);
}

long unsigned int SServer::maxConnections() {
  return PrivateData->Handlers.poolSize();
}

void SServer::wait() {
  if (PrivateData->Server_Thread.joinable())
    PrivateData->Server_Thread.join();
  PrivateData->Handlers.wait();
}

bool SServer::isRunning() const {
  bool res = false;
  try {
    if (PrivateData->Server_Thread.joinable())
      res = true;
  }
  catch (std::exception& e) {
    SLogger::global().addMessage("SClient:isRunning: " + std::string(e.what()));
  }
  return res;
}

/*The Simulacrum Network Model
* Packet Format:
* <-------------- 32-bit Little Endian ----------------->
* +-----------------------------------------------------+
* |  SPCH     | Version     |         Reserved          |
* |-----------------------------------------------------|
* |      SessionID          |          ItemID           |
* |-----------------------------------------------------|
* |        Command          |         DataType          |
* |-----------------------------------------------------|
* |                    DataLength                       |
* |-----------------------------------------------------|
* |                       Data                          |
* |     (Variable Length, determined by DataLength)     |
* +-----------------------------------------------------+
*
* SPCH       (Special Character)
* Version    (Protocol Version)
* Unused     (Currently Unused)
* SessionID  (SessionId Value, for use by session manager)
* ItemID     (Component-to-Component ID specification)
* Command    (Command for specified Item)
* DataType   (Data representation used in payload)
* DataLength (Length of the DataPayload)
* Data       (Variable length payload, specified by DataLength)
*/

/*------------------------------------------------------------------------------
 *                              SNet Helpers
 *------------------------------------------------------------------------------
 */

static const uint8_t       Version    = 1;
static const uint16_t      HeaderSize = 16;
static const uint8_t       SPCH       = 0xAA;
static const SNetIDWidth_t ReservedWord = 0xFF00;

const int SNetDefaultPort = 6060;

/* boost completion condition function, for sizes
static std::size_t do_size(std::size_t totransfer,
                                 const boost::system::error_code& error,
                                 std::size_t bytes_transferred) {
  if ((bytes_transferred >= totransfer) || error)
    return 0;
  else
    return 1;
}
*/

class SNetTunnelException : public SimulacrumIOException {
public:
 virtual const char* what() const throw(){
   return "Simulacrum: SNetTunnel Exception";
 }
};

class SNetMessageException : public SNetTunnelException {
public:
 virtual const char* what() const throw(){
   return "Simulacrum: SNetMessage Exception";
 }
};

class SNetTunnelClosed : public SNetTunnelException {
public:
 virtual const char* what() const throw(){
   return "Simulacrum: SNetTunnel Closed";
 }
};

/*------------------------------------------------------------------------------
 *                              SNet::SNetMessage
 *------------------------------------------------------------------------------
 */

class SNetMessage::SNetMessagePIMPL {
public:
  SNetIDWidth_t        SessionID;
  SNetIDWidth_t        ItemID;
  SNetIDWidth_t        Command;
  SNetIDWidth_t        DataType;
  SNetDataWidth_t      DataLength;
  char*                Data;
                       SNetMessagePIMPL(): SessionID(0), ItemID(0), Command(0),
                                           DataType(0), DataLength(0),
                                           Data(nullptr) {}
  virtual             ~SNetMessagePIMPL() {
    if (Data != nullptr)
      delete[] Data;
  }
};

SNetMessage::SNetMessage() {
  PrivateData = new SNetMessagePIMPL;
}

SNetMessage::~SNetMessage() {
  delete PrivateData;
}

SNetIDWidth_t SNetMessage::getSessionID() {
  return PrivateData->SessionID;
}

SNetIDWidth_t SNetMessage::getItemID() {
  return PrivateData->ItemID;
}

SNetIDWidth_t SNetMessage::getCommand() {
  return PrivateData->Command;
}

SNetIDWidth_t SNetMessage::getDataType() {
  return PrivateData->DataType;
}

SNetDataWidth_t SNetMessage::getDataLength() {
  return PrivateData->DataLength;
}

char* SNetMessage::getData() {
  return PrivateData->Data;
}

void SNetMessage::setSessionID(SNetIDWidth_t newsession) {
  PrivateData->SessionID = newsession;
}

void SNetMessage::setItemID(SNetIDWidth_t newid) {
  PrivateData->ItemID = newid;
}

void SNetMessage::setCommand(SNetIDWidth_t newcom) {
  PrivateData->Command = newcom;
}

void SNetMessage::setDataType(SNetIDWidth_t newtype) {
  PrivateData->DataType = newtype;
}

void SNetMessage::setData(SNetDataWidth_t newlen, char* newdata, bool copy) {
  PrivateData->DataLength = newlen;
  if (copy) {
    PrivateData->Data = new char[newlen];
    for (SNetDataWidth_t i=0;i<PrivateData->DataLength;i++) {
      PrivateData->Data[i] = newdata[i];
    }
  }
  else
    PrivateData->Data = newdata;
}

std::string SNetMessage::toString() {
  std::stringstream output;
  output << "SessionID: " << getSessionID() << std::endl
         << "ItemID: " << getItemID() << std::endl
         << "Command: " << getCommand() << std::endl
         << "DataType: " << getDataType() << std::endl
         << "DataLength: " << getDataLength() << std::endl
         << "Data: " << std::endl;
  for (unsigned i=0; i<getDataLength();i++)
    output << getData()[i];
  output << std::endl;
  return output.str();
}

void SNetMessage::copy(SNetMessage& copyme, bool deep) {
  setSessionID(copyme.getSessionID());
  setItemID(copyme.getItemID());
  setCommand(copyme.getCommand());
  setDataType(copyme.getDataType());
  setData(copyme.getDataLength(),copyme.getData(),deep);
}

static SNetDataWidth_t decode_header(char* header, SNetMessage& newmsg) {
  // For simplicity of reading, just initialise various pointers
  uint8_t         *spch   = (uint8_t*)         &(header[0]);
  uint8_t         *vers   = (uint8_t*)         &(header[1]);
  SNetIDWidth_t   *resvd  = (SNetIDWidth_t*)   &(header[2]);
  SNetIDWidth_t   *sesid  = (SNetIDWidth_t*)   &(header[4]);
  SNetIDWidth_t   *itmid  = (SNetIDWidth_t*)   &(header[6]);
  SNetIDWidth_t   *commd  = (SNetIDWidth_t*)   &(header[8]);
  SNetIDWidth_t   *dttyp  = (SNetIDWidth_t*)   &(header[10]);
  SNetDataWidth_t *dtsze  = (SNetDataWidth_t*) &(header[12]);
  // implicit synchronisation and endianness check
  if (*spch != SPCH) {
    DEBUG_OUT("Synchronisation check failed");
    throw SNetTunnelException();
  }
  if (*resvd != ReservedWord) {
    DEBUG_OUT("Endianness check failed");
    throw SNetTunnelException();
  }
  if (*vers != Version) {
    DEBUG_OUT("Version Mismatch");
    throw SNetTunnelException();
  }
  newmsg.setSessionID(*sesid);
  newmsg.setItemID(*itmid);
  newmsg.setCommand(*commd);
  newmsg.setDataType(*dttyp);
  return *dtsze;
}

static void encode_header(char* header,SNetMessage& newmsg) {
  // For simplicity of reading, just initialise various pointers
  uint8_t         *spch   = (uint8_t*)         &(header[0]);
  uint8_t         *vers   = (uint8_t*)         &(header[1]);
  SNetIDWidth_t   *resvd  = (SNetIDWidth_t*)   &(header[2]);
  SNetIDWidth_t   *sesid  = (SNetIDWidth_t*)   &(header[4]);
  SNetIDWidth_t   *itmid  = (SNetIDWidth_t*)   &(header[6]);
  SNetIDWidth_t   *commd  = (SNetIDWidth_t*)   &(header[8]);
  SNetIDWidth_t   *dttyp  = (SNetIDWidth_t*)   &(header[10]);
  SNetDataWidth_t *dtsze  = (SNetDataWidth_t*) &(header[12]);
  // initialise the header
  *spch    = SPCH;
  *vers    = Version;
  *resvd   = ReservedWord;
  *sesid   = newmsg.getSessionID();
  *itmid   = newmsg.getItemID();
  *commd   = newmsg.getCommand();
  *dttyp   = newmsg.getDataType();
  *dtsze   = newmsg.getDataLength();
}

/* Encode the message and transmit it */
void SNetMessage::write(ssocket_t &targ_sock) {
  char header[HeaderSize];
  size_t sentsize;
  encode_header(header,*this);
  //send header
  sentsize = boost::asio::write
                            (targ_sock,boost::asio::buffer(header,HeaderSize));
  if ((sentsize != HeaderSize)) {
    DEBUG_OUT("Error sending header block.");
    throw SNetTunnelException();
  }
  //send message
  sentsize = boost::asio::write
                    (targ_sock,boost::asio::buffer(getData(),getDataLength()));
  if ((sentsize != getDataLength())) {
    DEBUG_OUT("Error sending data block.");
    throw SNetTunnelException();
  }
}

/* Read new message from the Socket */
void SNetMessage::read(ssocket_t &src_sock) {
  char header[HeaderSize];
  char *data = nullptr;
  size_t readsize = 0;
  size_t datareadsize = 0;
  try {
    DEBUG_OUT("Waiting for new message...");
    readsize = boost::asio::read
                             (src_sock,boost::asio::buffer(header,HeaderSize));
    /* Check that we read the correct header size --  */
    if (readsize != HeaderSize) {
      DEBUG_OUT("General socket read error.");
      throw SNetTunnelException();
    }
    datareadsize = decode_header(header,*this);
    data = new char[datareadsize];
    setData(datareadsize,data,false); // do NOT copy data into message
    readsize = boost::asio::read
                             (src_sock,boost::asio::buffer(data,datareadsize));
    if (readsize != datareadsize) {
      DEBUG_OUT("Could not read indicated data size from socket.");
      throw SNetTunnelException();
    }
  }
  catch (std::exception &e) {
    DEBUG_OUT(std::string("SNetMessage listener exception captured: ") +
              e.what());
  }
}

