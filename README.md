# Simulacrum

The Simulacrum library. See the [website](http://mahicks.org/Simulacrum) and [this article](http://tinab.mahicks.org/simulacrum) for more information.

![Simulacrum Logo](Documentation/source/Images/slogo_small.png)

[![build status](http://gitlab.com/mahicks/Simulacrum/badges/master/pipeline.svg)](http://gitlab.com/mahicks/Simulacrum/commits/master)

Copyright (C) 2022 Michael A. Hicks
Distributed under the terms of the GNU Lesser General Public License.

# Overview

Simulacrum is a library suite for volume and image space interaction. It is designed to be lightweight, easy-to-use, portable, scalable and extensible. The core of Simulacrum is written in C++ with integrated Lua scripting, providing flexibility in programming solutions. A closely coupled tertiary library, Simulacrum-gui, provides a framework for visual tools, including advanced rendering techniques built around the cross-platform Qt libraries.

# Checkout

Simulacrum uses Git for revision control.

git clone https://gitlab.com/mahicks/Simulacrum

# Compiling

Simulacrum is composed of two libraries: Base and GUI. Only GUI is dependent on the Qt libraries.

Both libraries use CMake to compile binaries.

## Linux, MacOS X ...

From the cloned repository root:

> cmake -S . -B build

> cmake --build build

This will create a directory called 'build' and proceed to build Simulacrum
in that directory.

Dependencies:

CMake, Boost (system, filesystem, asio), libcurl, Swig. GUI: Qt 4 or 5.

## Windows

Using Qt Creator, open the top-level CMake project file.
Pressing the build button will then compile the project.

## Android

Currently possible but not yet documented here.

## CMake Options

Simulacrum's CMake configuration is sensitive to the following ON/OFF options:

ANDROID
 : Build for the Android NDK / Qt

CURL
 : Curl support (from system)

GUI
 : GUI Library

LUA
 : Lua support (built-in)

PNG
 : PNG support (built-in)

DICOM Transfer syntaxes:

J2K
 : JPEG2000 support (built-in)

JPEG
 : JPEG support (built-in)

LJPEG
 : Lossless JPEG support (built-in)

JPEGLS
 : JPEG-LS support (built-in)

DCMDIC
 : DICOM Data Dictionary (built-in)

SDEBUG
 : Extended runtime debug messages (default: OFF)

SUPPRESS_EXTERNAL_WARNINGS
 : Suppress warnings from external libraries

Options can be set using CMake, from the command line, as follows:

> cmake -D<optionname>=<ON/OFF>

For example:

> cmake -S . -B build -DJPEGLS=OFF

