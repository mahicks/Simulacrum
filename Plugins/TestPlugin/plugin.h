/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* A simple example of the Simulacrum Plugin framework */

#include <QLabel>

#include <Toolbox/SPlugins/splugins.h>
#include <Core/salgorithm.h>
#include <GUI/viewPort.h>

namespace Simulacrum {

  class SIMU_API ATestAlgorithmPlugin : public SAlgorithmCPU {
  public:
             ATestAlgorithmPlugin();
    virtual ~ATestAlgorithmPlugin();
    virtual
    void         init           ();
    virtual
    void         kernel         (SSpaceIterator *beginc,
                                 SSpaceIterator *endc);
    virtual
    SAlgorithm*  New            ();
  };

  class SIMU_API ATestQTWidget : public QLabel {
  public:
    explicit ATestQTWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~ATestQTWidget();
  };

  class SIMU_API ATestVPTool : public SViewPortTool {
  public:
                   ATestVPTool(QObject* parent = 0);
    virtual       ~ATestVPTool();
  public slots:
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

}