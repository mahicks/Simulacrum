/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* A simple example of the Simulacrum Plugin framework */

#include "plugin.h"

using namespace Simulacrum;

static void addPlugin(SPluginManager* plugman);

// definitions necessary for plugin use
SPLUGIN_DECL(addPlugin)

static void addPlugin(SPluginManager* plugman) {
  plugman->addPlugin((SPluginFactory*)
                        new SPluginConvenience<SAlgorithm,ATestAlgorithmPlugin>
                                                    ("EmptyAlgorithmTestPlugin")
                    );
  plugman->addPlugin((SPluginFactory*)
                                   new SPluginConvenience<QWidget,ATestQTWidget>
                                                           ("QWidgetTestPlugin")
                    );
  plugman->addPlugin((SPluginFactory*)
                               new SPluginConvenience<SViewPortTool,ATestVPTool>
                                                           ("SVPToolTestPlugin")
                    );
}

//----------------------

ATestAlgorithmPlugin::ATestAlgorithmPlugin() {
  reset();
}

ATestAlgorithmPlugin::~ATestAlgorithmPlugin() {

}

SAlgorithm* ATestAlgorithmPlugin::New() {
  return new ATestAlgorithmPlugin();
}

void ATestAlgorithmPlugin::init() {

}

void ATestAlgorithmPlugin::kernel(SSpaceIterator*, SSpaceIterator*) {

}

//----------------------

ATestQTWidget::ATestQTWidget(QWidget* parent, Qt::WindowFlags f): 
                                                             QLabel(parent, f) {
  resize(400,400);
  setText("A test widget!");
}

ATestQTWidget::~ATestQTWidget() {

}

//----------------------

ATestVPTool::ATestVPTool(QObject* parent): SViewPortTool(parent) {
  setToolTip("A test VP tool!");
}

ATestVPTool::~ATestVPTool() {

}

void ATestVPTool::leftClicked(SViewPort& vp, int x, int y) {
  SViewPortTool::leftClicked(vp,x,y);
  vp.showMessage("Test tool clicked!");
}

