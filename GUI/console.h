/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Connectable
 * An interface for a set of various console-like widgets
 * M. A. Hicks
 */
#ifndef SQCONSOLE
#define SQCONSOLE

#include <GUI/definitions.h>
#include "qconnectable.h"
#include <Toolbox/SLogger/slogger.h>

#include <QPlainTextEdit>

namespace Simulacrum {

  class SIMU_GUI_API SQLogView: public QPlainTextEdit, public SQConnectable {
    Q_OBJECT
  private:
    SLogger *SourceLog;
    void     addNewLogs();
  public:
             SQLogView(QWidget* parent = nullptr);
    virtual ~SQLogView();
    QSize    sizeHint () const;
  public slots:
    virtual
    void     sconnect(SConnectable&);
    virtual
    void     sdisconnect(SConnectable*);
    virtual
    void     refresh(bool);
    virtual
    void     setProgress(int);
    virtual
    void     setWaiting(bool);
    virtual
    void     showEvent(QShowEvent *event);
    virtual
    void     append(const QString&);
  signals:
    void     seriousEvent();
    void     active(bool);
    void     progress(int);
    void     updated(bool);
  };

}

#endif // SQCONSOLE
