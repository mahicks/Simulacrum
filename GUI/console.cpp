/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Console
 * A set of various console-like widgets
 * M. A. Hicks
 */

#include "console.h"

#include <QScrollBar>

using namespace Simulacrum;

SQLogView::SQLogView(QWidget* parent): QPlainTextEdit(parent), SourceLog(nullptr) {
  acceptCoreSignals();
  setReadOnly(true);
  setWindowFlags(Qt::Tool);
  setFont (QFont ("Courier New", 9));
  setMaximumBlockCount(8192);
}

SQLogView::~SQLogView() {

}

void SQLogView::addNewLogs() {
  if (SourceLog != nullptr)
    append(SourceLog->getNew().c_str());
}

QSize SQLogView::sizeHint() const {
  return QSize(800,400);
}

void SQLogView::sconnect(SConnectable& newlog) {
  SourceLog = dynamic_cast<SLogger*>(&newlog);
  if (SourceLog != nullptr) {
    setWindowTitle(SourceLog->getTitle().c_str());
    SQConnectable::sconnect(newlog);
  }
}

void SQLogView::sdisconnect(SConnectable *targ) {
  SQConnectable::sdisconnect(targ);
}

void SQLogView::refresh(bool serious) {
  SQConnectable::refresh(serious);
  if(serious) {
    show();
    emit seriousEvent();
  }
  if (isVisible())
    addNewLogs();
}

void SQLogView::showEvent(QShowEvent* event) {
  QPlainTextEdit::showEvent(event);
  refresh(false);
}

void SQLogView::setProgress(int prog) {
  SQConnectable::setProgress(prog);
}

void SQLogView::setWaiting(bool dowait) {
  SQConnectable::setWaiting(dowait);
}

void SQLogView::append(const QString& newlogtext) {
  // check if view is at the end
  bool bool_at_bottom = (verticalScrollBar()->value() ==
                                                verticalScrollBar()->maximum());
  // insert text at the end
  QTextCursor text_cursor = QTextCursor(document());
  text_cursor.movePosition(QTextCursor::End);
  text_cursor.insertText(newlogtext);
  // scroll if at the end
  if (bool_at_bottom)
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
}

