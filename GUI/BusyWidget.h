/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * BusyWidget
 * A simple overlay Widget for Qt4 to make another widget look busy.
 * M. A. Hicks
 */

#include <QWidget>
class QProgressBar;
class QLabel;
class QString;
class QPushButton;
class QTimer;
class QMovie;

#include <GUI/definitions.h>
#include <Core/sresource.h>
#include <GUI/qconnectable.h>



#ifndef BUSYWIDGET
#define BUSYWIDGET

namespace Simulacrum {

  class SIMU_GUI_API BusyWidget : public QWidget, public SQConnectable {
    Q_OBJECT
  private:
    static const int
                  progressWidth;
    static const int
                  progressHeight;
    QWidget      *busyTarget;
    QColor        busyColour;
    QProgressBar *busyProgress;
    QLabel       *busyLabel;
    QPushButton  *closeButton;
    QTimer       *repainter;
    QMovie       *busyAnim;
    QLabel       *busyAnimHolder;
    float         busyOpacity;
    bool          errorStatus;
    int           lockCount;
    bool          canClose;
    bool          disableInterface;
  public:
                  BusyWidget (QWidget *parent);
                 ~BusyWidget();
    bool          isBusy();
  public slots:
    void          setColor(const QColor&);
    void          setOpacity(float);
    void          hideIndicators();
    void          setBusy(bool busyval);
    void          setMessage(const QString&);
    void          showProgress(int progress, int max=100);
    void          oscillateProgress(bool = true);
    void          stopOscillateProgress();
    void          doError(const QString&, bool keepold = false);
    void          clear(bool skipnobusy = false);
    void          canCloseMessage(bool);
    void          setDisableInterface(bool);
    void          setBusyAnim(QString);
    void          dorepaint();
    void          refresh(bool);
    void          setProgress(int);
    void          setCloseButtonLabel(const QString&);
    void          sconnect(SConnectable*);
    void          sconnect(SConnectable&);
    void          sconnect(SQConnectable&);
    void          sdisconnect(SConnectable*);
    void          sdisconnect(SConnectable&);
    void          sdisconnect(SQConnectable&);
    void          setWaiting(bool);
  protected:
    void          paintEvent(QPaintEvent *event);
    void          resizeEvent (QResizeEvent*);
  signals:
    void          active      (bool);
    void          progress    (int);
    void          updated     (bool);
    void          closed      ();
  };

}
#endif
