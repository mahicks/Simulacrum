/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM_GUI_DEFINITIONS
#define SIMULACRUM_GUI_DEFINITIONS

#include <Core/definitions.h>
#include <Core/sabtree.h>
#include <Core/sresource.h>
#include <QString>

// DSO Exporting, Reference: http://gcc.gnu.org/wiki/Visibility
#define SIMU_GUI_API
#ifdef SHAREDGUILIB
  #ifdef _WIN32
    #undef  SIMU_GUI_API
    #define SIMU_GUI_API __declspec(dllexport)
  #else
    #undef  SIMU_GUI_API
    #define SIMU_GUI_API __attribute__ ((visibility ("default")))
  #endif
#endif

namespace Simulacrum {

  class SIMU_GUI_API SimulacrumGUILibrary {
  public:
    static bool init();
    static
    QString     toQString(SimulacrumLibrary::str_enc encoding,
                          std::string string);
  };
  
}

#endif //SIMULACRUM_GUI_DEFINITIONS
