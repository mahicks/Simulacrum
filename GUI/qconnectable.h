/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Connectable
 * An interface specification to allow components to be 'connected'
 * M. A. Hicks
 */
#ifndef SQCONNECTABLE
#define SQCONNECTABLE

#include <QObject>
#include <QMimeData>
#include <map>
#include <GUI/definitions.h>
#include <Core/sconnectable.h>
#include <Core/definitions.h>

namespace Simulacrum {

  class SQConnectable;

  class SIMU_GUI_API SConnectableWrapper : public QObject, public SConnectable {
    Q_OBJECT
  private:
    SQConnectable& SignalTarget;
  public:
         SConnectableWrapper(SQConnectable&);
    virtual
    bool sconnect(SConnectable&);
    bool sconnect_(SConnectable&);
    bool sdisconnect(SConnectable&);
    void refresh(bool);
    void progress(int);
    void signal(unsigned,void*);
  signals:
    void progupdate(int);
    void propupdate(bool);
    void propdisconnect(SConnectable*);
    void propwaiting(bool);
  };

  class SIMU_GUI_API SQConnectable {
  private:
    SConnectableWrapper CoreReceiver;
  public:
                 SQConnectable();
    virtual     ~SQConnectable();
  public:
    /* event 'receivers' */
    virtual void refresh(bool);
    virtual void sconnect(SQConnectable&);
    virtual void sdisconnect(SQConnectable&);
    virtual void sconnect(SConnectable&);
    virtual void sconnect(SConnectable&,bool);
    virtual void sdisconnect(SConnectable*);
    virtual void sconfigure(const QString&);
    virtual void setWaiting(bool);
    virtual void setProgress(int);
    virtual void ssignal(unsigned,void*);
    virtual void acceptCoreSignals();
    virtual SConnectableWrapper&
                 core();
    /* signals-to-be */
    virtual void active(bool)=0;
    virtual void progress(int)=0;
    virtual void updated(bool)=0;
  };

  class SMimeData: public QMimeData {
  public:
    void *ObjectPointer; 
  };

}

#endif
