/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*******************************************************************************
 *                          Standard Tools                                     *
 *                                                                             *
 ******************************************************************************/

#include "vpTools.h"

#include <functional>
#include <QMenu>
#include <QFileDialog>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QMouseEvent>
#include <QPainter>
#include <QColor>
#include <QApplication>
#include <QScreen>
#include <QScrollBar>
#include <QDial>
#include <QApplication>
#include <QDesktopWidget>
#include <QSlider>
#include <QAbstractSlider>
#include <QSpinBox>
#include <QCheckBox>
#include <QLabel>
#include <QInputDialog>
#include <QColorDialog>
#include <QComboBox>
#include <QGraphicsDropShadowEffect>
#include <qitemselectionmodel.h>

#include <Core/salgorithms.h>
#include <Resources/spng.h>
#include <GUI/tagBrowser.h>
#include "lighttable.h"
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/sysInfo/sysInfo.h>

using namespace Simulacrum;

#ifdef MOBILE
static unsigned goodIconSize() {        
#if QT_VERSION >= 0x050000
  return (unsigned)(QGuiApplication::primaryScreen()->logicalDotsPerInch()/1.3);
#else
  return 100;
#endif
}
#endif

//*************************** Generic Slider Tool ******************************

QAbstractSlider* newSlider(QWidget *parent) {
#ifdef MOBILE
  QScrollBar *newscrollbar = new QScrollBar(Qt::Vertical,parent);
  newscrollbar->setStyleSheet("QScrollBar:vertical { width: " + 
                               QString::number(goodIconSize()/2.5)  + 
                               "px; background: rgba(255, 255, 255, 30%);}");
  return newscrollbar;
#else
  QSlider *newslider = new QSlider(Qt::Vertical,parent);
  return newslider;
#endif
}

SSliderTool::SSliderTool(QObject* parent): SViewPortTool(parent), 
                                                                TargetVP(nullptr) {
  VPSlider = newSlider(parentWidget());
  VPSlider->hide();
  connect(VPSlider,SIGNAL(valueChanged(int)),this,SLOT(newSliderValue(int)));
  configureSlider();
}

SSliderTool::~SSliderTool() {

}

void SSliderTool::configureSlider() {
  VPSlider->setRange(sliderMin(),sliderMax());
}

void SSliderTool::selected(SViewPort& targetvp) {
  if (TargetVP != nullptr)
    disconnect(TargetVP,SIGNAL(destroyed(QObject*)),this,SLOT(VPGone()));
  TargetVP = &targetvp;
  viewportResized(targetvp);
  TargetVP->setEdgeWidget(VPSlider,3);
  configureSlider();
  VPSlider->show();
  connect(&targetvp,SIGNAL(destroyed(QObject*)),this,SLOT(VPGone()));
}

void SSliderTool::deSelected(SViewPort& targetvp) {
  if (&targetvp == TargetVP)
    disconnect(&targetvp,SIGNAL(destroyed(QObject*)),this,SLOT(VPGone()));
  TargetVP = nullptr;
  VPSlider->setParent(parentWidget());
  VPSlider->hide();
}

void SSliderTool::VPGone() {
  TargetVP = nullptr;
  VPSlider = newSlider(parentWidget());
  VPSlider->hide();
  connect(VPSlider,SIGNAL(valueChanged(int)),this,SLOT(newSliderValue(int)));
}

void SSliderTool::leftClicked(SViewPort& nvp, int, int) {
  SSliderTool::selected(nvp);
}

void SSliderTool::newSliderValue(int newpos) {
  if (sender() != VPSlider)
    VPSlider->setSliderPosition(newpos);
  doSliderValue(newpos);
}

void SSliderTool::doSliderValue(int newpos) {
  if (TargetVP != nullptr) {
    TargetVP->showMessage("Slider Position: " + QString::number(newpos));
  }
}

int SSliderTool::sliderMax() {
  return 100;
}

int SSliderTool::sliderMin() {
  return 0;
}

void SSliderTool::viewportResized(SViewPort& targ) {
#ifdef MOBILE
  static const int padd = 300;
#else
  static const int padd = 120;
#endif
  if (&targ == VPSlider->parentWidget())
    VPSlider->resize(VPSlider->sizeHint().width(),targ.height()-padd);
}

//*************************** MaxIP Slider Tool ********************************

SMaxIPTool::SMaxIPTool(QObject* parent): SSliderTool(parent), 
                                                            SourceSlicer(nullptr) {
  setToolTip(tr("Adjust Maximum Intensity Projection Depth"));
  setIcon(QIcon(":resources/max-icon"));
  setText(tr("Max. Intensity"));
  VPSlider->setToolTip(tr("Adjust projection depth"));
  connect(&ClearProjTimer,SIGNAL(timeout()),this,SLOT(clearProj()));
  ClearProjTimer.setSingleShot(true);
}

SMaxIPTool::~SMaxIPTool() {

}

void SMaxIPTool::configureReduction() {
  if (SourceSlicer != nullptr) {
    SourceSlicer->lock();
    SourceSlicer->setReductionFunction(std::bind(&SSlicer::MaxIPGrey,
                                                 std::placeholders::_1,
                                                 std::placeholders::_2,
                                                 std::placeholders::_3,
                                                 std::placeholders::_4,
                                                 std::placeholders::_5,
                                                 std::placeholders::_6,
                                                 std::placeholders::_7,
                                                 std::placeholders::_8));
    SourceSlicer->unlock();
  }
}

void SMaxIPTool::selected(SViewPort& nvp) {
  SourceSlicer = dynamic_cast<SSlicer*>(nvp.sourceSSpace());
  configureReduction();
  if (SourceSlicer != nullptr)
    newSliderValue(SourceSlicer->thickness());
  // source slider needs to be configured first
  SSliderTool::selected(nvp);
  setIcon(QIcon(":resources/max_selected-icon"));
}

void SMaxIPTool::deSelected(SViewPort& nvp) {
  SSliderTool::deSelected(nvp);
  setIcon(QIcon(":resources/max-icon"));
}

void SMaxIPTool::leftClicked(SViewPort& vp, int x, int y) {
  SSliderTool::leftClicked(vp, x, y);
  selected(vp);
}

void SMaxIPTool::doSliderValue(int nthickness) {
  if (SourceSlicer != nullptr) {
    if (SourceSlicer->sliceable()) {
      SourceSlicer->lock();
      SourceSlicer->setThickness((unsigned) nthickness);
      SourceSlicer->unlock();
      SourceSlicer->refresh(false);
      if (TargetVP != nullptr) {
        QString thicknessspacingstr;
        thicknessspacingstr =  
                         QString::number(SourceSlicer->thicknessSpacing(),'f',2)
                 + QString(" ") + QString(SourceSlicer->spacingUnits().c_str());
        TargetVP->showMessage("Thickness: " +
                               QString::number(SourceSlicer->thickness()) +
                               QString("[") + thicknessspacingstr + 
                               QString("]"));
        doProjections(*TargetVP,false,true);
        ClearProjTimer.start(1000);
      }
    }
  }
}

int SMaxIPTool::sliderMax() {
  if (SourceSlicer != nullptr)
    return SourceSlicer->depth();
  else
    return 1;
}

int SMaxIPTool::sliderMin() {
  return 1;
}

void SMaxIPTool::VPGone() {
  SSliderTool::VPGone();
  SourceSlicer = nullptr;
}

void SMaxIPTool::doProjections(SViewPort& vp, bool force, bool fast) {
  clearProj();
  QList<QWidget*> projs = SPointInfoTool::showSliceProjections(vp,force,fast);
  for (int p=0; p<projs.size(); p++) {
    connect(projs[p],SIGNAL(destroyed(QObject*)),
            this,SLOT(remProject(QObject*)));
  }
  Projections.append(projs);
}

void SMaxIPTool::clearProj() {
  for (int p=0; p<Projections.size(); p++) {
    Projections[p]->deleteLater();
  }
}

void SMaxIPTool::remProject(QObject* src) {
  QWidget *srcp = static_cast<QWidget*>(src);
  if (Projections.count(srcp)) {
    Projections.erase(Projections.begin()+Projections.indexOf(srcp));
  }
}

//*************************** CurveP Slider Tool *******************************

static void eCurvePlane(SSlicer *slice,const SCoordinate::Precision xx,
                        const SCoordinate::Precision yy,
                        const SPoint& point, const SVector& dir,
                        unsigned int depth, SElem&, SElem& target,
                        bool bothaxis, SCoordinate::Precision xp,
                        SCoordinate::Precision yp) {
  SCoordinate finalpoint;
  SVector     precisepoint;
  float expval   = ((float)depth)/500.0;
  float centdist =   SGeom::fabs(((float)xp) - (float)xx);
  if (bothaxis)
    centdist = sqrt((centdist*centdist) + 
                                 pow(SGeom::fabs(((float)yp) - (float)yy),2.0));
  precisepoint = point + ( dir  * (exp( expval * centdist )/2.0) );
  finalpoint.clamp_floor(precisepoint);
  if (slice->getSourceSSpace().extent().withinSpace(finalpoint)) {
    target.intensity(SSlicer::triLinearInterpolateIntensity(
                                 slice->getSourceSSpace(),precisepoint));
  }
  else {
    target.clear();
  }
}

SCurvePTool::SCurvePTool(QObject* parent): SMaxIPTool(parent), XP(0), YP(0) {
  setToolTip(tr("Adjust Curved-Plane Projection Surface"));
  setIcon(QIcon(":resources/curved-icon"));
  setText("Curved Plane");
  VPSlider->setToolTip(tr("Adjust projection curve"));
  ToolBox    = new QWidget(parentWidget());
  BothAxis   = new QCheckBox(ToolBox);
  CurveGraph = new SGrapher(ToolBox);
  ToolBox->hide();
  BothAxis->setText(tr("Curve about both axes"));
  BothAxis->setToolTip(
                      tr("By default, the plane curves about the Y-axis only"));
  CurveGraph->show();
  QVBoxLayout *toollayout = new QVBoxLayout(ToolBox);
  ToolBox->setLayout(toollayout);
  toollayout->addWidget(BothAxis);
  toollayout->addWidget(CurveGraph);
  connect(BothAxis,SIGNAL(stateChanged(int)),this,SLOT(updatePlane()));
}

SCurvePTool::~SCurvePTool() {

}

void SCurvePTool::selected(SViewPort& nvp) {
  SourceSlicer = dynamic_cast<SSlicer*>(nvp.sourceSSpace());
  configureReduction();
  if (SourceSlicer != nullptr)
    newSliderValue(SourceSlicer->thickness());
  // source slider needs to be configured first
  SSliderTool::selected(nvp);
  setIcon(QIcon(":resources/curved-selected-icon"));
  if (! ToolBox->isVisible()) {
    AdditionalWidget(ToolBox,tr("Curved Plane Configuration"));
    ToolBox->move(QCursor::pos());
    ToolBox->setMinimumHeight(400);
    ToolBox->resize(500,ToolBox->height());
    ToolBox->show();
  }
}

void SCurvePTool::deSelected(SViewPort& nvp) {
  SSliderTool::deSelected(nvp);
  setIcon(QIcon(":resources/curved-icon"));
  AdditionalWidgetHide(ToolBox);
}

int SCurvePTool::sliderMin() {
    return 0;
}

int SCurvePTool::sliderMax() {
    return 50;
}

void SCurvePTool::doSliderValue(int eval) {
  if (SourceSlicer != nullptr) {
    if (SourceSlicer->sliceable()) {
      SourceSlicer->lock();
      SourceSlicer->setThickness((unsigned) eval);
      SourceSlicer->unlock();
      SourceSlicer->refresh(false);
      if (TargetVP != nullptr) {
        QString thicknessspacingstr;
        thicknessspacingstr =  
                         QString::number(SourceSlicer->thicknessSpacing(),'f',2)
                 + QString(" ") + QString(SourceSlicer->spacingUnits().c_str());
        TargetVP->showMessage("Plane-curve: 0.5e^(" +
                      QString::number((float)SourceSlicer->thickness()/500.0)
                      + "x)");
        updateCurve();
      }
    }
  }
}

void SCurvePTool::leftClicked(SViewPort& vp, int x, int y) {
  SSliderTool::leftClicked(vp,x,y);
  SourceSlicer = dynamic_cast<SSlicer*>(vp.sourceSSpace());
  SVector curveabout = vp.coordToSourceSSpace({x,y});
  curveabout.setDim(2);
  XP = SGeom::sfloor(curveabout.x());
  YP = SGeom::sfloor(curveabout.y());
  vp.showMessage("Curving plane about: " + QString::number(XP) 
                  + "x" + QString::number(YP));
  configureReduction();
}

void SCurvePTool::updateCurve() {
  CurveGraph->clear();
  CurveGraph->setShowLabels(false);
  CurveGraph->setDrawGrid(true);
  CurveGraph->setSigFigs(4);
  if (SourceSlicer != nullptr) {
    const SCoordinate &slceext = SourceSlicer->extent();
    if (slceext.getDim() > 1) {
      SDataSet spanset;
      SCoordinate::Precision maxdepth = SourceSlicer->depth();
      spanset.push_back(std::make_pair(0,maxdepth));
      spanset.push_back(std::make_pair(slceext.x(),maxdepth));
      float stepsize = slceext.x();
      stepsize = stepsize / 30;
      float accum = 0.0;
      SDataSet graphpoints;
      const float expval = VPSlider->value() / 500.0;
      for (unsigned p=0; p<=30; p++) {
        float yval = exp(expval * SGeom::fabs((float)XP-accum))/2.0;
        if (yval > (float)maxdepth)
          yval = (float)maxdepth;
        graphpoints.push_back(std::make_pair(accum,yval-1));
        accum +=stepsize;
      }
      CurveGraph->addDataSet(spanset);
      CurveGraph->addDataSet(graphpoints);
    }
  }
}

void SCurvePTool::configureReduction() {
  if (SourceSlicer != nullptr) {
    SourceSlicer->lock();
    SourceSlicer->setReductionFunction( std::bind(&eCurvePlane,
                                                  std::placeholders::_1,
                                                  std::placeholders::_2,
                                                  std::placeholders::_3,
                                                  std::placeholders::_4,
                                                  std::placeholders::_5,
                                                  std::placeholders::_6,
                                                  std::placeholders::_7,
                                                  std::placeholders::_8,
                                                  BothAxis->isChecked(),
                                                  XP,
                                                  YP
                                                 )
                                      );
    SourceSlicer->unlock();
    SourceSlicer->refresh(false);
    updateCurve();
  }
}

void SCurvePTool::updatePlane() {
  configureReduction();
}

//*************************** MinIP Slider Tool ********************************

SMinIPTool::SMinIPTool(QObject* parent): SMaxIPTool(parent) {
  setToolTip(tr("Adjust Minimum Intensity Projection Depth"));
  setIcon(QIcon(":resources/min-icon"));
  setText(tr("Min. Intensity"));
}

SMinIPTool::~SMinIPTool() {

}

void SMinIPTool::configureReduction() {
  if (SourceSlicer != nullptr) {
    SourceSlicer->lock();
    SourceSlicer->setReductionFunction(std::bind(&SSlicer::MinIPGrey,
                                                 std::placeholders::_1,
                                                 std::placeholders::_2,
                                                 std::placeholders::_3,
                                                 std::placeholders::_4,
                                                 std::placeholders::_5,
                                                 std::placeholders::_6,
                                                 std::placeholders::_7,
                                                 std::placeholders::_8));
    SourceSlicer->unlock();
  }
}

void SMinIPTool::selected(SViewPort& nvp) {
  SMaxIPTool::selected(nvp);
  setIcon(QIcon(":resources/min_selected-icon"));
}

void SMinIPTool::deSelected(SViewPort& nvp) {
  SMaxIPTool::deSelected(nvp);
  setIcon(QIcon(":resources/min-icon"));
}

//*************************** AVGIP Slider Tool ********************************

SAVGIPTool::SAVGIPTool(QObject* parent): SMaxIPTool(parent) {
  setToolTip(tr("Adjust Average Intensity Projection Depth"));
  setIcon(QIcon(":resources/avg-icon"));
  setText(tr("Avg Intensity"));
}

SAVGIPTool::~SAVGIPTool() {

}

void SAVGIPTool::configureReduction() {
  if (SourceSlicer != nullptr) {
    SourceSlicer->lock();
    SourceSlicer->setReductionFunction(std::bind(&SSlicer::AVGIPGrey,
                                                 std::placeholders::_1,
                                                 std::placeholders::_2,
                                                 std::placeholders::_3,
                                                 std::placeholders::_4,
                                                 std::placeholders::_5,
                                                 std::placeholders::_6,
                                                 std::placeholders::_7,
                                                 std::placeholders::_8));
    SourceSlicer->unlock();
  }
}

void SAVGIPTool::selected(SViewPort& nvp) {
  SMaxIPTool::selected(nvp);
  setIcon(QIcon(":resources/avg_selected-icon"));
}

void SAVGIPTool::deSelected(SViewPort& nvp) {
  SMaxIPTool::deSelected(nvp);
  setIcon(QIcon(":resources/avg-icon"));
}

//**************************** OZIP Slider Tool ********************************

SOZIPTool::SOZIPTool(QObject* parent): SMaxIPTool(parent), zeroLevel(0),
                                                                LightSource(3) {
  setToolTip(tr("Adjust Surface Projection Depth"));
  setIcon(QIcon(":resources/surface-icon"));
  setText(tr("Surface Proj."));
  SecondVPSlider = newSlider(parentWidget());
  SecondVPSlider->hide();
  SecondVPSlider->setToolTip(tr("Adjust Zero-Level"));
  connect(SecondVPSlider,SIGNAL(valueChanged(int)),this,
          SLOT(newLevelValue(int)));
  LightSource.xyz(0,0,0);
  configureSecondSlider();
}

void SOZIPTool::configureSecondSlider() {
  SecondVPSlider->setRange(secondSliderMin(),secondSliderMax());
}

SOZIPTool::~SOZIPTool() {

}

int SOZIPTool::secondSliderMin() {
  int retval = 0;
  if (SourceSlicer != nullptr) {
    SSpace *sourcespc = dynamic_cast<SSpace*>(&(SourceSlicer->end()));
    if (sourcespc != nullptr)
      if (sourcespc->LUT().useWL())
        retval = sourcespc->LUT().getWLCentre() -
                                              (sourcespc->LUT().getWLWidth()/2);
  }
  return retval;
}

int SOZIPTool::secondSliderMax() {
  int retval = 255;
  if (SourceSlicer != nullptr) {
    SSpace *sourcespc = dynamic_cast<SSpace*>(&(SourceSlicer->end()));
    if (sourcespc != nullptr)
      if (sourcespc->LUT().useWL())
        retval = sourcespc->LUT().getWLCentre() +
                                              (sourcespc->LUT().getWLWidth()/2);
  }
  return retval;
}

void SOZIPTool::configureReduction() {
  if (SourceSlicer != nullptr) {
    SourceSlicer->lock();
    SourceSlicer->setReductionFunction(
      std::bind(&SSlicer::FirstOverGrey,std::placeholders::_1,
                                        std::placeholders::_2,
                                        std::placeholders::_3,
                                        std::placeholders::_4,
                                        std::placeholders::_5,
                                        std::placeholders::_6,
                                        std::placeholders::_7,
                                        std::placeholders::_8,
                                       (SElem::Precision) zeroLevel, true,
                                        LightSource)
                                       );
    SourceSlicer->unlock();
    SourceSlicer->refresh(false);
  }
}

void SOZIPTool::selected(SViewPort& nvp) {
  if (TargetVP != nullptr)
    TargetVP->setEdgeWidget(nullptr,1);
  nvp.setEdgeWidget(nullptr,1);
  SMaxIPTool::selected(nvp);
  setIcon(QIcon(":resources/surface_selected-icon"));
  SecondVPSlider->resize(VPSlider->sizeHint().width(),TargetVP->height()-200);
  TargetVP->setEdgeWidget(SecondVPSlider,1);
  configureSecondSlider();
  SecondVPSlider->show();
}

void SOZIPTool::deSelected(SViewPort& nvp) {
  SMaxIPTool::deSelected(nvp);
  setIcon(QIcon(":resources/surface-icon"));
  SecondVPSlider->hide();
  SecondVPSlider->setParent(parentWidget());
}

void SOZIPTool::newLevelValue(int newpos) {
  if (TargetVP != nullptr) {
    zeroLevel = newpos;
    TargetVP->showMessage("Zero-Level: " + QString::number(zeroLevel));
    configureReduction();
  }
}

void SOZIPTool::leftClicked(SViewPort& target,int x, int y) {
  selected(target);
  SSpace *sourcespace = target.sourceSSpace();
  if (sourcespace != nullptr) {
    try {
      SSlicer &sourceslice = dynamic_cast<SSlicer&>(*sourcespace);
      SSpace  *endspace    = target.endSourceSSpace();
      sourceslice.lock();
      // it's a slicer if we progress...
      if (sourceslice.sliceable() &&
          endspace != nullptr &&
          (endspace->extent().volume() != 0)
          ) {
        // get the point at which user clicked
        SCoordinate clickloc(2);
        SVector    clicklocplane;
        clickloc.xy(x,y);
        clicklocplane = target.coordToSourceSSpace(clickloc);
        clickloc = sourceslice.toSourceCoords
                                     (SCoordinate(3).clamp_floor(clicklocplane));
        LightSource = SVector(clickloc);
        LightSource = LightSource - (sourceslice.getPlane().normal() *
                                             ((float)sourceslice.depth()*0.08));
      }
      sourceslice.unlock();
      configureReduction();
    }
    catch (std::exception &)  {}
  }
}

void SOZIPTool::VPGone() {
  SMaxIPTool::VPGone();
  SecondVPSlider = newSlider(parentWidget());
  SecondVPSlider->hide();
  connect(SecondVPSlider,SIGNAL(valueChanged(int)),this,
          SLOT(newLevelValue(int)));
}

void SOZIPTool::viewportResized(SViewPort& nvp) {
  SSliderTool::viewportResized(nvp);
#ifdef MOBILE
  static const int padd = 300;
#else
  static const int padd = 120;
#endif
  if (&nvp == VPSlider->parentWidget())
    SecondVPSlider->resize(SecondVPSlider->sizeHint().width(),
                           nvp.height()-padd);
}

//*************************** Window Level Tool ********************************

static std::map<std::string, GPLUT::LUTGen_t> lutlist;

SWLTool::SWLTool(QObject* parent): SViewPortTool(parent), NewCoord(2),
                               HistAlg(new SBucketHistogram()), AddrCheck(nullptr),
                                         NoAdjust(false), ShowHistTools(true) {
  setToolTip(tr("Left-drag: Adjust Display LUT Right: recalc histogram, \
Middle: auto-wl"));
  setIcon(QIcon(":resources/contrast-icon"));
  setText(tr("LUT"));
  ToolPanel = new QWidget(parentWidget());
  BusyMaker = new BusyWidget(ToolPanel);
  QVBoxLayout *panellayout = new QVBoxLayout(ToolPanel);
  SkipEdges = new QCheckBox(tr("Skip outlier buckets"),ToolPanel);
  SkipEdges->setCheckState(Qt::Checked);
  panellayout->addWidget(SkipEdges);
  FullSource = new QCheckBox(tr("Use entire source space"),ToolPanel);
  panellayout->addWidget(FullSource);
  GraphTool = new SGrapher(ToolPanel);
  GraphTool->setProperty("makeDarker",true);
  panellayout->addWidget(GraphTool);
  AutoPercentile = new QSpinBox(ToolPanel);
  AutoPercentile->setMinimum(1);
  AutoPercentile->setMaximum(100);
  AutoPercentile->setValue(99);
  AutoPercentile->setPrefix(tr("Auto-LUT Percentile: "));
  panellayout->addWidget(AutoPercentile);
  ToolPanel->setLayout(panellayout);
  ToolPanel->hide();
  GraphTool->setShowLabels(false);
  GraphTool->setDrawGrid(true);
  std::vector<QColor> collist;
  collist.push_back(QColor(230,167,0));
  collist.push_back(Qt::white);
  collist.push_back(Qt::white);
  GraphTool->setDataSetColors(collist);
  ToolPanel->setMinimumHeight(350);
  connect(this,SIGNAL(histReady(SSpace*)),this,SLOT(updateHist(SSpace*)));
#ifdef MOBILE
  AutoPercentile->hide();
  FullSource->hide();
  SkipEdges->hide();
#endif
  // configure lut list
  QWidget *lutline = new QWidget(ToolPanel);
  QHBoxLayout *lutlayout = new QHBoxLayout(lutline);
  lutlayout->setContentsMargins(0,0,0,0);
  lutline->setLayout(lutlayout);
  LUTList = new QComboBox(lutline);
  lutlayout->addWidget(LUTList);
  LUTList->show();
  // configure apply button
  ApplyLUT = new QToolButton(lutline);
#ifdef MOBILE
  ApplyLUT->setStyleSheet("QToolButton { border: 1px solid #353535; \
background-color: rgba(0, 0, 0, 40%); color: #e5e5e5; border-radius: 5px; \
min-width: " + QString::number(goodIconSize()/1.5)  + "px; min-height: " + 
QString::number(goodIconSize()/1.5)  + "px; }");
  ApplyLUT->setIconSize(QSize(goodIconSize()/1.5,goodIconSize()/1.5));
#endif
  ApplyLUT->setToolTip("Apply LUT to ViewPorts");
  ApplyLUT->setCheckable(true);
  ApplyLUT->adjustSize();
  ApplyLUT->setFixedWidth(ApplyLUT->width());
  applyLUTChange(false);
  connect(ApplyLUT,SIGNAL(toggled(bool)),this,SLOT(applyLUTChange(bool)));
  lutlayout->addWidget(ApplyLUT);
  lutline->setMinimumHeight(ApplyLUT->height());
  lutline->setMaximumHeight(ApplyLUT->height());
  panellayout->addWidget(lutline);
  // init some basic luts
  if (lutlist.count("Standard Linear Grayscale") == 0)
    lutlist["Standard Linear Grayscale"] = nullptr;
  if (lutlist.count("Hot Iron") == 0) {
    std::vector<uint32_t> colors(4);
    RGBAI32SElem col((SElem::DataSource)&colors[0]);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[1]);
    col.red(255);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[2]);
    col.red(255);
    col.green(255);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[3]);
    col.red(255);
    col.green(255);
    col.blue(255);
    col.alpha(255);
    lutlist["Hot Iron"] = std::bind ( GPLUT::genColBlendLUT, colors,
                                      std::placeholders::_1,
                                      std::placeholders::_2,
                                      std::placeholders::_3,
                                      std::placeholders::_4,
                                      std::placeholders::_5 );
  }
  if (lutlist.count("Traffic Lights") == 0) {
    std::vector<uint32_t> colors(4);
    RGBAI32SElem col((SElem::DataSource)&colors[0]);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[1]);
    col.green(255);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[2]);
    col.red(255);
    col.green(255);
    col.alpha(255);
    col.source((SElem::DataSource)&colors[3]);
    col.red(255);
    col.alpha(255);
    lutlist["Traffic Lights"] = std::bind ( GPLUT::genColBlendLUT, colors,
                                            std::placeholders::_1,
                                            std::placeholders::_2,
                                            std::placeholders::_3,
                                            std::placeholders::_4,
                                            std::placeholders::_5 );
  }
}

void SWLTool::applyLUTChange(bool selected) {
  if (selected)
    ApplyLUT->setIcon(QIcon(":resources/contrast_selected-icon"));
  else
    ApplyLUT->setIcon(QIcon(":resources/contrast-icon"));
}

SWLTool::~SWLTool() {
  delete HistAlg;
}

void SWLTool::addLUT(GPLUT::LUTGen_t newlut, const QString &name) {
  lutlist[name.toStdString()] = newlut;
}

bool SWLTool::removeLUT(const QString& name) {
  bool retval = false;
  if (lutlist.count(name.toStdString())) {
    lutlist.erase(name.toStdString());
    retval = true;
  }
  return retval;
}

void SWLTool::selected(SViewPort& vp) {
  setIcon(QIcon(":resources/contrast_selected-icon"));
  // remove non-existant luts
  for (int i=0; i<LUTList->count();  i++) {
    if ( lutlist.count( LUTList->itemText(i).toStdString() ) == 0 ) {
      LUTList->removeItem(i);
      i = 0;
    }
  }
  // add missing luts
  for (std::map<std::string,GPLUT::LUTGen_t>::iterator it= lutlist.begin();
       it != lutlist.end(); it++) {
    if (LUTList->findText(it->first.c_str()) == -1) {
      LUTList->addItem(it->first.c_str());
    }
  }
  LUTList->setCurrentIndex(LUTList->findText("Standard Linear Grayscale"));
  if (ShowHistTools)
    AdditionalWidget(ToolPanel, tr("Intensity Histogram"));
  updateHist(vp.sourceSSpace());
}

void SWLTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/contrast-icon"));
  AdditionalWidgetHide(ToolPanel);
  ApplyLUT->setChecked(false);
}

void SWLTool::showHistTools(bool nhist) {
  ShowHistTools = nhist;
}

void SWLTool::leftClicked(SViewPort& source, int x, int y) {
  SViewPortTool::leftClicked(source,x,y);
  SSpace *sourcespace = source.sourceSSpace();
  if (sourcespace != nullptr) {
    bool islocked = sourcespace->try_lock();
    // abort if not locked; do not block GUI thread
    if (!islocked) return;
    // set the LUTgen method if needed
    if (ApplyLUT->isChecked() &&
        lutlist.count(LUTList->currentText().toStdString()) > 0) {
      if ( ! (lutlist[LUTList->currentText().toStdString()] == nullptr &&
              sourcespace->LUT().lutGen() == nullptr)
         ) {
        sourcespace->LUT().setLUTGen(
                                 lutlist[LUTList->currentText().toStdString()]);
      }
    }
    // dig one level deeper (in case it's a slicer etc)
    WLWidth    = sourcespace->LUT().getWLWidth();
    WLCentre   = sourcespace->LUT().getWLCentre();
    WLMax      = sourcespace->LUT().sizeWL();
    WLStepSize = (WLMax / source.width()) / 10;
    if (WLStepSize == 0)
      WLStepSize = 1;
    sourcespace->unlock();
    NoAdjust = false;
    dragged(source,x,y);
  }
}

void SWLTool::middleClicked(SViewPort& vp, int x, int y) {
  SSpace *target    = vp.sourceSSpace();
  SSpace *targetend = vp.endSourceSSpace();
  if ((target != nullptr) && (targetend != nullptr) &&
      (targetend->extent().volume() == target->extent().volume())
     ) {
    target = targetend; // use the end space anyway, since it's the same vol
  }
  if (FullSource->checkState() != 0)
    target = targetend;
  if ((target != nullptr)
#ifndef MOBILE // for doubleClicked gesture
       && ((&target->getSourceSSpace() != AddrCheck) ||
                         (target->getSourceSSpace().getName() != NameCheck) )
#endif
     ) {
    vp.showMessage("Computing auto-level...");
    connect(this,SIGNAL(histReady(SSpace*)),this,SLOT(doAutoWL(SSpace*)));
    rightClicked(vp,x,y);
  }
  else {
    doAutoWL(target);
  }
}

void SWLTool::doAutoWL(SSpace* target) {
  disconnect(this,SIGNAL(histReady(SSpace*)),this,SLOT(doAutoWL(SSpace*)));
  if ((target != nullptr)) {
    std::vector< std::pair < float, float > > tmp;
    tmp = HistAlg->histogram();
    if ((SkipEdges->checkState() != 0) && tmp.size() > 0)
      tmp[0].second = 0.0;
    bool islocked = target->try_lock();
    if (islocked) {
      target->autoLUT(AutoPercentile->value(),-1,-1,&tmp);
      target->unlock();
      target->getSourceSSpace().refresh(false);
      updateHist(target);
    }
  }
}

void SWLTool::rightClicked(SViewPort& vp, int, int) {
  SSpace *target    = vp.sourceSSpace();
  SSpace *targetend = vp.endSourceSSpace();
  if ((target != nullptr) && (targetend != nullptr) &&
      (targetend->extent().volume() == target->extent().volume())
     ) {
    target = targetend; // use the end space anyway, since it's the same vol
  }
  if (FullSource->checkState() != 0)
    target = targetend;
  if (target != nullptr && (BGJobs.jobs() == 0)) {
    bool islocked = target->try_lock();
    if (islocked) {
      BusyMaker->oscillateProgress(true);
      BGJobs.addJob( std::bind(&SWLTool::bgHistGen,this,target) );
    }
  }
  NoAdjust = true;
}

void SWLTool::bgHistGen(SSpace* srcspace) {
  if (srcspace != nullptr) {
    bool setprogress = false;
    if (srcspace->extent().volume() > (1920*1080)) {
      srcspace->progress(-1);
      setprogress = true;
    }
    HistAlg->setInput(0,*srcspace,false);
    if (srcspace->informationNode().hasChildNode("BitsStored")) {
      std::stringstream conv;
      float bits;
      conv <<
             srcspace->informationNode().getChildNode("BitsStored").NodeValue();
      conv >> bits;
      bits = pow(2.0,bits);
      HistAlg->setParameter("maximum",bits);
    }
    else {
      SElem::Precision bpp = srcspace->LUT().bppIn();
      if (bpp > 0) { // use LUT information about bpp
        HistAlg->setParameter("maximum",(double)pow(2.0,bpp));
      }
      else { // no LUT info
        HistAlg->setParameter("maximum",255);
      }
    }
    HistAlg->setParameter("buckets",256.0);
    HistAlg->execute();
    AddrCheck = &srcspace->getSourceSSpace();
    NameCheck = srcspace->getSourceSSpace().getName();
    if (setprogress)
      srcspace->progress(100);
    srcspace->unlock(); // must be locked before calling
  }
  emit (histReady(srcspace));
}

void SWLTool::updateHist(SSpace* addcheck) {
  GraphTool->removeDataSet(0);
  GraphTool->removeDataSet(0);
  GraphTool->removeDataSet(0);
  if ( (addcheck != nullptr) &&
       ((&(addcheck->getSourceSSpace()) == AddrCheck) &&
        (addcheck->getSourceSSpace().getName() == NameCheck)
       )
     ) {
    BusyMaker->setBusy(false);
    if (GraphTool->isVisible()) {
      std::vector< std::pair < float, float > > tmp, wll, wlh;
      float maxval = 0.0;
      tmp = HistAlg->histogram();
      if ((SkipEdges->checkState() != 0) && tmp.size() > 0)
        tmp[0].second = 0;
      for (unsigned i=0; i<tmp.size(); i++)
        if (tmp[i].second > maxval)
          maxval = tmp[i].second;
      tmp.push_back(std::make_pair(tmp[tmp.size()-1].first+1,0.0));
      GraphTool->addDataSet(tmp,SGrapherMode_t::SHistogram,false);
      if (addcheck->LUT().useWL()) {
        long long int WLWidthl  = addcheck->LUT().getWLWidth();
        long long int WLCentrel = addcheck->LUT().getWLCentre();
        wll.push_back(std::make_pair((float)(WLCentrel - (WLWidthl/2)),0.0));
        wll.push_back(std::make_pair((float)(WLCentrel - (WLWidthl/2)),maxval));
        wlh.push_back(std::make_pair((float)(WLCentrel + (WLWidthl/2)),0.0));
        wlh.push_back(std::make_pair((float)(WLCentrel + (WLWidthl/2)),maxval));
        GraphTool->addDataSet(wll,SGrapherMode_t::SLineGraph,false);
        GraphTool->addDataSet(wlh,SGrapherMode_t::SLineGraph,false);
      }
      GraphTool->drawGraph();
    }
  }
}

void SWLTool::dragged(SViewPort& source, int x, int y) {
  NewCoord.setDim(2);
  NewCoord.xy(x,y);
  bool usewl = false;
  SSpace *sourcespace = source.sourceSSpace();
  if (sourcespace != nullptr && !NoAdjust) {
    bool islocked = sourcespace->try_lock();
    bool changed = false;
    // abort if not locked; do not block GUI thread
    if (!islocked) return;
    long int newcentre, newwidth;
    if (sourcespace->LUT().useWL()) {
      usewl = true;
      SCoordinate delta(2);
      delta = NewCoord - ReferencePoint;
      newcentre = WLCentre+(delta.y()*WLStepSize);
      if (newcentre < 0)
        newcentre = 0;
      else if (newcentre > WLMax)
        newcentre = WLMax;
      newwidth = WLWidth+(delta.x()*WLStepSize);
      if (newwidth < 1)
        newwidth = 1;
      else if (newwidth > WLMax)
        newwidth = WLMax;
      sourcespace->LUT().adjLUT(newcentre,newwidth);
      // read back set values
      newcentre = sourcespace->LUT().getWLCentre();
      newwidth  = sourcespace->LUT().getWLWidth();
      changed = true;
    }
    if (changed)
      sourcespace->getSourceSSpace().emitRefresh(false);
    sourcespace->unlock();
    if (usewl) {
      QString message;
      message += "Window Centre: " + QString::number(newcentre) + "<br/>";
      message += "Window Width: " + QString::number(newwidth);
      source.showMessage(message);
    }
    else
      source.showMessage("No WL in use...");
  }
  else {
    leftClicked(source,x,y);
  }
  if (ShowHistTools)
    updateHist(source.sourceSSpace());
}

#ifdef MOBILE
void SWLTool::doubleClicked(SViewPort& vp, int x, int y) {
  middleClicked(vp,x,y);
}
#else
void SWLTool::doubleClicked(SViewPort&, int, int) { }
#endif

const float SWLTool::GammaStep = 0.05;

void SWLTool::doGammaeStep(SViewPort& source, float step) {
  SSpace *sourcespace = source.sourceSSpace();
  if (sourcespace != nullptr) {
    float newgamma = 1.0;
    bool islocked = sourcespace->try_lock();
    // abort if not locked; do not block GUI thread
    if (!islocked) return;
    bool usewl = sourcespace->LUT().useWL();
    if (usewl) {
      newgamma = sourcespace->LUT().getGamma() + step;
      sourcespace->LUT().setGamma(newgamma);
      // for regeneration of the LUT
      sourcespace->LUT().adjLUT(sourcespace->LUT().getWLCentre(),
                                          sourcespace->LUT().getWLWidth());
      sourcespace->emitRefresh(false);
    }
    sourcespace->unlock();
    if (usewl) {
      QString message;
      message += "Gamma: " + QString::number(newgamma) + "<br/>";
      source.showMessage(message);
    }
    else
      source.showMessage("No WL in use...");
  }
}

void SWLTool::keyPressed(SViewPort& source, int key) {
  switch (key) {
    case 'G':
      doGammaeStep(source,GammaStep);
      break;
    case 'B':
      doGammaeStep(source,0-GammaStep);
      break;
    case 0x1000013: // up arrow
      break;
    case 0x1000015: // down arrow
      break;
  }
}

//*************************** Panning Tool *************************************

SPanTool::SPanTool(QObject* parent) : SViewPortTool(parent), Orig(2) {
  setToolTip(tr("Pan"));
  setIcon(QIcon(":resources/pan-icon"));
  setText(tr("Pan"));
}

void SPanTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/pan_selected-icon"));
}

void SPanTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/pan-icon"));
}

void SPanTool::leftClicked(SViewPort& target, int x, int y) {
  SViewPortTool::leftClicked(target,x,y);
  target.setScaleFit(false);
  Orig.setDim(2);
  Orig.xy(target.renderSurfaceWidget().x(),target.renderSurfaceWidget().y());
}

void SPanTool::dragged(SViewPort& target, int x, int y) {
  int dx, dy;
  dx = x- ReferencePoint.x();
  dy = y - ReferencePoint.y();
  target.renderSurfaceWidget().move(Orig.x() + dx, Orig.y() + dy);
  QString message;
  SCoordinate VPCentre(2);
  VPCentre.xy(target.width()/2,target.height()/2);
  SVector pancentre;
  pancentre = target.coordToSourceSSpace(VPCentre);
  message += "Pan Centre: " + QString::number((int)pancentre.x()) 
                            + "," + QString::number((int)pancentre.y());
  target.showMessage(message);
}

//***************************** Zoom Tool **************************************

SZoomTool::SZoomTool(QObject* parent): SViewPortTool(parent),
                                                        Orig(2), ZoomFactor(2) {
  setToolTip(
            tr("Left: zoom in, Right: zoom out, Middle-drag: continuous zoom"));
  setIcon(QIcon(":resources/zoom-icon"));
  setText(tr("Zoom"));
}

void SZoomTool::setZoomFactor(int newz) {
  ZoomFactor = newz;
}

void SZoomTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/zoom_selected-icon"));
}

void SZoomTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/zoom-icon"));
}

void SZoomTool::showZoom(SViewPort& target) {
  float zoomratio = currentZoom(target);
  if (zoomratio != -1.0) {
    QString message, zoomnum;
    zoomnum = QString::number(zoomratio);
    zoomnum.truncate(4);
    message += "Zoom: " + zoomnum + "X";
    target.showMessage(message);
  }
}

const int SZoomTool::MaxZoom = 600;

void SZoomTool::doZoom (SViewPort& target, float scale, int cx, int cy,
                        int px, int py, bool relative) {
  if (target.sourceSSpace() != nullptr) {
    float czoom = currentZoom(target);
    if ((scale > 1.0) && (czoom > MaxZoom)) {
      target.showMessage("Maximum zoom exceeded");
      return;
    }
    if (czoom == -1.0)
      return;
    int clicklocx = cx - target.renderSurfaceWidget().x();
    int clicklocy = cy - target.renderSurfaceWidget().y();
    float posx    = (float)clicklocx /
                                    (float)target.renderSurfaceWidget().width();
    float posy    = (float)clicklocy /
                                  (float) target.renderSurfaceWidget().height();
    if (relative) {
      target.renderSurfaceWidget().resize(
        target.renderSurfaceWidget().width() * scale,
        target.renderSurfaceWidget().height() * scale
                                        );
    } else {
      bool gotlock = target.sourceSSpace()->try_lock();
      if (gotlock) {
        target.renderSurfaceWidget().resize(
          target.sourceSSpace()->extent().x() * scale,
          target.sourceSSpace()->extent().y() * scale
                                          );
        target.sourceSSpace()->unlock();
      }
      else {
        return;
      }
    }
    target.renderSurfaceWidget().move
      ((px) -((float)target.renderSurfaceWidget().width()  * posx),
      (py) -((float)target.renderSurfaceWidget().height() * posy));
    showZoom(target);
    target.setScaleFit(false);
  }
}

float SZoomTool::currentZoom(SViewPort &target) {
  bool islocked = target.sourceSSpace()->try_lock();
  if (islocked) {
    SCoordinate::Precision SourceVol = target.sourceSSpace()->extent().volume();
    target.sourceSSpace()->unlock();
    return ((float)target.renderSurfaceWidget().width() *
                      (float)target.renderSurfaceWidget().height()) /
                              SourceVol;
  }
  else {
    return -1.0;
  }
}

#ifdef MOBILE
void SZoomTool::middleClicked
#else
void SZoomTool::leftClicked
#endif
(SViewPort& target, int x, int y) {
  DoZoom = false;
  doZoom (target, 1.5, x, y, x, y);
}

void SZoomTool::rightClicked(SViewPort& target, int, int) {
  DoZoom = false;
  doZoom (target, 0.66667, target.width()/2, target.height()/2,
                           target.width()/2, target.height()/2 );
}

#ifdef MOBILE
void SZoomTool::leftClicked
#else
void SZoomTool::middleClicked
#endif
(SViewPort& target, int x, int y) {
  SViewPortTool::leftClicked(target, x, y);
  target.setScaleFit(false);
  Orig.setDim(2);
  Orig.xy(target.renderSurfaceWidget().width(),
          target.renderSurfaceWidget().height());
  OrigPos.setDim(2);
  OrigPos.xy(target.renderSurfaceWidget().x(),target.renderSurfaceWidget().y());
  DoZoom = true;
}

void SZoomTool::dragged(SViewPort& target, int, int y) {
  if (DoZoom && (Orig.getDim() > 1) && (OrigPos.getDim() > 1)) {
    int dy, nx, ny;
    dy = y - ReferencePoint.y();
    float czoom = currentZoom(target);
    if ((dy > 0.0) && (czoom > MaxZoom)) {
      target.showMessage("Maximum zoom exceeded");
      return;
    }
    if (czoom == -1.0)
      return;
    nx = Orig.x() + (dy * ZoomFactor);
    ny = Orig.y() + (dy * ZoomFactor);
    target.renderSurfaceWidget().setGeometry(OrigPos.x() - ((dy*ZoomFactor)/2),
                                             OrigPos.y() - ((dy*ZoomFactor)/2),
                                             nx,
                                             ny);
    showZoom(target);
    target.setScaleFit(false);
  }
}

//*************************** Stretch Tool *************************************

SFitStretchTool::SFitStretchTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Stretch-to-Fit"));
  setIcon(QIcon(":resources/fit-icon"));
  setText(tr("Scale/Fit"));
}

void SFitStretchTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/fit_selected-icon"));
}

void SFitStretchTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/fit-icon"));
}

void SFitStretchTool::leftClicked(SViewPort& target, int, int) {
  target.setScaleFit(true);
  target.showMessage("");
}

//**************************** Mirror Tool *************************************

SMirrorTool::SMirrorTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Invert Slice X-Axis"));
  setIcon(QIcon(":resources/flipx-icon"));
  setText(tr("Invert X-Axis"));
}

void SMirrorTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/flipx-selected-icon"));
}

void SMirrorTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/flipx-icon"));
}

void SMirrorTool::leftClicked(SViewPort& target, int, int) {
  SSpace *sourcespace = target.sourceSSpace();
  if (sourcespace != nullptr) {
    try {
      SSlicer &sourceslice = dynamic_cast<SSlicer&>(*sourcespace);
      bool islocked = sourceslice.try_lock();
      if (!islocked) return;
      sourceslice.setPlane(sourceslice.getPlane().point(),
                           !sourceslice.getXAxis(),
                           sourceslice.getYAxis());
      sourceslice.unlock();
      sourceslice.emitRefresh(false);
    }
    catch (std::exception &)  {}
  }
}

//**************************** Flip Tool *************************************

SFlipTool::SFlipTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Invert Slice Y-Axis"));
  setIcon(QIcon(":resources/flipy-icon"));
  setText(tr("Invert Y-Axis"));
}

void SFlipTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/flipy-selected-icon"));
}

void SFlipTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/flipy-icon"));
}

void SFlipTool::leftClicked(SViewPort& target, int, int) {
  SSpace *sourcespace = target.sourceSSpace();
  if (sourcespace != nullptr) {
    try {
      SSlicer &sourceslice = dynamic_cast<SSlicer&>(*sourcespace);
      bool islocked = sourceslice.try_lock();
      if (!islocked) return;
      sourceslice.setPlane(sourceslice.getPlane().point(),
                           sourceslice.getXAxis(),
                           !sourceslice.getYAxis());
      sourceslice.unlock();
      sourceslice.emitRefresh(false);
    }
    catch (std::exception &)  {}
  }
}

//***************************** 1to1 Tool **************************************

S1to1Tool::S1to1Tool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Zoom 1:1"));
  setIcon(QIcon(":resources/onetoone-icon"));
  setText(tr("Zoom 1:1"));
}

void S1to1Tool::selected(SViewPort&) {
  setIcon(QIcon(":resources/onetoone_selected-icon"));
}

void S1to1Tool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/onetoone-icon"));
}

void S1to1Tool::leftClicked(SViewPort& target, int x, int y) {
  if (target.sourceSSpace() != nullptr) {
    SViewPortTool::leftClicked(target, x, y);
    SVector clickloc = target.coordToSourceSSpace(SCoordinate(2).xy(x,y));
    // get the current clicked postion, on the renderSurface
    int nposx, nposy;
    nposx = (target.width()/2) - (int)clickloc.x();
    nposy = (target.height()/2) - (int)clickloc.y();
    SSpace *sourcesspace = target.sourceSSpace();
    bool islocked = sourcesspace->try_lock();
    if (!islocked) return;
    if (sourcesspace != nullptr) {
      if (sourcesspace->extent().getDim() > 1) {
        target.setScaleFit(false);
        target.renderSurfaceWidget().setGeometry(nposx,nposy,
                                                sourcesspace->extent().x(),
                                                sourcesspace->extent().y());
        QString message;
        message += "Centre: " + QString::number((int)clickloc.x()) + ","
                + QString::number((int)clickloc.y());
        target.showMessage(message);
      }
    }
    sourcesspace->unlock();
  }
}

//*************************** OrthoRotate Tool *********************************

enum OrthoStateVals { OrthoZ, OrthoX, OrthoY };

SOrthoRotateTool::SOrthoRotateTool(QObject* parent): SViewPortTool(parent), 
                                                            OrthoState(OrthoZ) {
  setToolTip(tr("Cycle Orthogonal Slice Planes (about a point)"));
  setIcon(QIcon(":resources/axes-icon"));
  setText(tr("Ortho. Planes"));
}

void SOrthoRotateTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/axes_selected-icon"));
}

void SOrthoRotateTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/axes-icon"));
}

void SOrthoRotateTool::leftClicked(SViewPort& target, int x, int y) {
  SSpace *sourcespace = target.sourceSSpace();
  if (sourcespace != nullptr) {
    try {
      SSlicer &sourceslice = dynamic_cast<SSlicer&>(*sourcespace);
      bool islocked = sourceslice.try_lock();
      if (!islocked) return;
      // it's a slicer if we progress...
      if (sourceslice.sliceable() &&
          (target.endSourceSSpace()->extent().volume() != 0)
          ) {
        // get the point at which user clicked
        SCoordinate clickloc(2);
        SVector     clicklocplane, normalv;
        clickloc.xy(x,y);
        clicklocplane = target.coordToSourceSSpace(clickloc);
        clickloc = sourceslice.toSourceCoords
                                    (SCoordinate(3).clamp_floor(clicklocplane));
        if (sourceslice.getSourceSSpace().extent().withinSpace(clickloc)) {
          normalv = sourceslice.getPlane().normal();
          // select a new slicer position based on the current orientation
          if (normalv.getDim() == 3) {
            if ( normalv[0] == 0 && normalv[1] == 0 && normalv[2] == 1 )
              OrthoState = OrthoZ;
            else
              if ( normalv[0] == 0 && normalv[1] == -1 && normalv[2] == 0 )
                OrthoState = OrthoY;
              else
                if ( normalv[0] == -1 && normalv[1] == 0 && normalv[2] == 0 )
                  OrthoState = OrthoX;
            else
              OrthoState = OrthoZ;
          }
          else
            OrthoState = OrthoZ;
          switch (OrthoState) {
            case OrthoZ:
              // step to X-orthoview
              target.showMessage
                       ("X-Ortho View <br/> X-Axis: 0,0,1 <br/> Y-Axis: 0,1,0");
              sourceslice.setPlane(SVector(clickloc),
                                  SVector(3).xyz(0,0,1),
                                  SVector(3).xyz(0,1,0));
              break;
            case OrthoX:
              // step to Y-othoview
              target.showMessage
                      ("Y-Ortho View <br/> X-Axis: 1,0,0 <br/> Y-Axis: 0,0,-1");
              sourceslice.setPlane(SVector(clickloc),
                                  SVector(3).xyz(1,0,0),
                                  SVector(3).xyz(0,0,1));
              break;
            case OrthoY:
            default:
              // step to Z-othoview
              target.showMessage
                       ("Z-Ortho View <br/> X-Axis: 1,0,0 <br/> Y-Axis: 0,1,0");
              sourceslice.setPlane(SVector(clickloc),
                                  SVector(3).xyz(1,0,0),
                                  SVector(3).xyz(0,1,0));
              break;
          }
        }
      }
      else
        target.showMessage("Not sliceable...");
      sourceslice.unlock();
      sourceslice.emitRefresh(true);
    }
    catch (std::exception &)  {}
  }
}

//***************************** Chroma Tool ************************************

SChromaTool::SChromaTool(QObject* parent): SViewPortTool(parent),
                                ColorChooser(new QColorDialog(parentWidget())) {
  setToolTip(tr("Select LUT Chroma"));
  setIcon(QIcon(":resources/palette-icon"));
  setText(tr("LUT Chroma"));
  ColorChooser->setWindowFlags(Qt::Widget);
  ColorChooser->setOptions(  QColorDialog::DontUseNativeDialog
                           | QColorDialog::NoButtons );
  ColorChooser->hide();
}

void SChromaTool::selected(SViewPort&) {
  //selected icon
  setIcon(QIcon(":resources/palette-selected-icon"));
  AdditionalWidget(ColorChooser, "Chroma Select");
}

void SChromaTool::deSelected(SViewPort&) {
  //unselected icon
  setIcon(QIcon(":resources/palette-icon"));
  AdditionalWidgetHide(ColorChooser);
}

void SChromaTool::leftClicked(SViewPort& target,int,int) {
  SSpace *targetspace = target.endSourceSSpace();
  if (targetspace != nullptr) {
    if (targetspace->LUT().useWL()) {
      targetspace->lock();
      QColor wlchroma = ColorChooser->currentColor();
      if (wlchroma.isValid()) {
        targetspace->LUT().setChroma
                              (wlchroma.red(),wlchroma.green(),wlchroma.blue());
        targetspace->LUT().useWLChroma(true);
        targetspace->emitRefresh(false);
      }
      targetspace->unlock();
    }
    else
      target.showMessage("No LUT in use");
  }
}

//***************************** Invert Tool ************************************

SInvertTool::SInvertTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Invert SSpace LUT"));
  setIcon(QIcon(":resources/invert-icon"));
  setText(tr("Invert LUT"));
}

void SInvertTool::selected(SViewPort&) {
  //selected icon
  setIcon(QIcon(":resources/invert-selected-icon"));
}

void SInvertTool::deSelected(SViewPort&) {
  //unselected icon
  setIcon(QIcon(":resources/invert-icon"));
}

void SInvertTool::leftClicked(SViewPort& target,int,int) {
  SSpace *targetspace = target.endSourceSSpace();
  if (targetspace != nullptr) {
    if (targetspace->LUT().useWL()) {
      targetspace->lock();
      targetspace->LUT().invertWL();
      targetspace->refresh(false);
      targetspace->unlock();
    }
    else
      target.showMessage("No LUT in use (cannot invert)");
  }
}

//***************************** Slice Tool *************************************

SSliceTool::SSliceTool(QObject* parent) : SSliderTool(parent), 
                          CurrentSlice(0), TmpSlice(0), HandleMouseScroll(true),
                              DoSync(false), FlipDir(false), ShowOptions(true) {
  setToolTip(
          tr("Left-drag: move Slice Along Normal, Middle-drag: synch slicing"));
  setIcon(QIcon(":resources/slice-icon"));
  setText(tr("Slicer"));
  QScrollBar *scrolltest = dynamic_cast<QScrollBar*>(VPSlider);
  if (scrolltest != nullptr)
    setFlipDir(true);
  Options = new QWidget(parentWidget());
  Options->hide();
  ProjectionOpt = new QCheckBox("Show Plane Projections",Options);
  ProjectionOpt->setCheckState(Qt::Checked);
  ProjectionForce = new QCheckBox("Force Plane Projections",Options);
  QVBoxLayout *optionslayout = new QVBoxLayout();
  optionslayout->addWidget(ProjectionOpt);
  optionslayout->addWidget(ProjectionForce);
  Options->setLayout(optionslayout);
}

void SSliceTool::outputSlicePosition(SViewPort& vp, SSlicer& slicer) {
 QString Message;
 Message += "Slice: " + QString::number(slicer.slice()+1) + "/" 
         + QString::number(slicer.depth()) + "<br> Orientation: " 
         + QString(slicer.getPlane().normal().toString().c_str());
 vp.showMessage(Message);
}

void SSliceTool::selected(SViewPort& tvp) {
  setIcon(QIcon(":resources/slice_selected-icon"));
  SSliderTool::selected(tvp);
  viewportUpdated(tvp);
  if (ShowOptions)
    AdditionalWidget(Options,"Slicing Options");
}

void SSliceTool::deSelected(SViewPort& tvp) {
  setIcon(QIcon(":resources/slice-icon"));
  SSliderTool::deSelected(tvp);
  clearProj();
  if (ShowOptions)
    AdditionalWidgetHide(Options);
}

SSliceTool* SSliceTool::doMouseScroll(bool nscroll) {
  HandleMouseScroll = nscroll;
  return this;
}

void SSliceTool::leftClicked(SViewPort& vp, int x, int y) {
  SSliderTool::leftClicked(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  SViewPortTool::leftClicked(vp,x,y);
  if (TargetSlicer != nullptr && TargetSlicer->try_lock()) {
    Scale = (float)TargetSlicer->depth() / ((float)vp.height()/2);
    CurrentSlice = TargetSlicer->slice();
    VPSlider->setValue(CurrentSlice);
    TargetSlicer->unlock();
    doProjections(vp,true);
  }
  else
    Scale = 0.0;
  DoSync = false;
}

void SSliceTool::middleClicked(SViewPort& vp, int x, int y) {
  SSliderTool::leftClicked(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  SViewPortTool::leftClicked(vp,x,y);
  if (TargetSlicer != nullptr && TargetSlicer->try_lock()) {
    Scale = (float)TargetSlicer->depth() / ((float)vp.height()/2);
    CurrentSlice = TargetSlicer->slice();
    VPSlider->setValue(CurrentSlice);
    TargetSlicer->unlock();
  }
  else
    Scale = 0.0;
  SPointInfoTool::synchVPs(vp,vp.width()/2,vp.height()/2,
                                          !ProjectionForce->checkState(),false);
  doProjections(vp,true);
  DoSync = true;
}

void SSliceTool::dragged(SViewPort& vp, int, int y) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    if (TargetSlicer->sliceable()) {
      int newslice;
      if (FlipDir)
        newslice = (int)((y - ReferencePoint.y())*Scale);
      else
        newslice = (int)((ReferencePoint.y() - y)*Scale);
      newslice = CurrentSlice + newslice;
      if ( newslice < 0 )
        newslice = 0;
      else if ( newslice >= (int)TargetSlicer->depth() )
        newslice = TargetSlicer->depth()-1;
      if (DoSync) {
        bool ready = TargetSlicer->try_lock();
        if (ready) {
          TargetSlicer->setSlice(newslice);
          TargetSlicer->unlock();
          disconnect(VPSlider,SIGNAL(valueChanged(int)),
                     this,SLOT(newSliderValue(int)));
          VPSlider->setValue(newslice);
          outputSlicePosition(vp,*TargetSlicer);
          TargetSlicer->emitRefresh(false);
          SPointInfoTool::synchVPs(vp,vp.width()/2,vp.height()/2,
                                   !ProjectionForce->checkState(),false);
          connect(VPSlider,SIGNAL(valueChanged(int)),
                  this,SLOT(newSliderValue(int)));
          doProjections(vp,true);
        }
      }
      else {
        newSliderValue(newslice);
      }
    }
    else
      vp.showMessage("Not sliceable...");
  }
}

void SSliceTool::wheelUp(SViewPort& vp, int x, int y) {
  if (HandleMouseScroll) {
    wheelUp_f(vp,x,y);
  }
}

void SSliceTool::wheelDown(SViewPort& vp, int x, int y) {
  if (HandleMouseScroll) {
    wheelDown_f(vp,x,y);
  }
}

void SSliceTool::wheelUp_f(SViewPort& vp, int, int) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    if (TargetSlicer->sliceable()) {
      bool islocked = TargetSlicer->try_lock();
      if (!islocked) return;
      TargetSlicer->sliceForwards();
      outputSlicePosition(vp,*TargetSlicer);
      VPSlider->setValue(TargetSlicer->slice());
      TargetSlicer->unlock();
      TargetSlicer->emitRefresh(false);
    }
    else
      vp.showMessage("Not sliceable...");
  }
}

void SSliceTool::wheelDown_f(SViewPort& vp, int, int) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    if (TargetSlicer->sliceable()) {
      bool islocked = TargetSlicer->try_lock();
      if (!islocked) return;
      TargetSlicer->sliceBackwards();
      outputSlicePosition(vp,*TargetSlicer);
      VPSlider->setValue(TargetSlicer->slice());
      TargetSlicer->unlock();
      TargetSlicer->emitRefresh(false);
    }
    else
      vp.showMessage("Not sliceable...");
  }
}

int SSliceTool::sliderMin() {
    return 0;
}

int SSliceTool::sliderMax() {
  int retval = sliderMin();
  if (TargetVP != nullptr) {
    SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(TargetVP->sourceSSpace());
    if (TargetSlicer != nullptr) {
      bool gotlock = TargetSlicer->try_lock();
      if (gotlock) {
        retval = TargetSlicer->depth()-1;
        TargetSlicer->unlock();
      }
      else
        retval = VPSlider->maximum();
    }
  }
  return retval;
}

void SSliceTool::doSliderValue(int sliceval) {
  if (TargetVP != nullptr) {
    SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(TargetVP->sourceSSpace());
    if (TargetSlicer != nullptr) {
      bool islocked = TargetSlicer->try_lock();
      if (!islocked) return;
      TargetSlicer->setSlice(sliceval);
      outputSlicePosition(*TargetVP,*TargetSlicer);
      TargetSlicer->unlock();
      doProjections(*TargetVP,true);
      TargetSlicer->emitRefresh(false);
    }
  }
}

void SSliceTool::viewportUpdated(SViewPort& vp) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool islocked = TargetSlicer->try_lock();
    if (islocked) {
      VPSlider->setValue(TargetSlicer->slice());
      TargetSlicer->unlock();
    }
    configureSlider();
  }
}

void SSliceTool::doProjections(SViewPort& vp, bool fast) {
  clearProj();
  if (ProjectionOpt->checkState()) {
    QList<QWidget*> projs = SPointInfoTool::showSliceProjections(vp,
                                            ProjectionForce->checkState(),fast);
    for (int p=0; p<projs.size(); p++) {
      connect(projs[p],SIGNAL(destroyed(QObject*)),
              this,SLOT(remProject(QObject*)));
    }
    Projections.append(projs);
  }
}

void SSliceTool::clearProj() {
  for (int p=0; p<Projections.size(); p++) {
    Projections[p]->deleteLater();
  }
}

void SSliceTool::remProject(QObject* src) {
  QWidget *srcp = static_cast<QWidget*>(src);
  if (Projections.count(srcp)) {
    Projections.erase(Projections.begin()+Projections.indexOf(srcp));
  }
}

void SSliceTool::setFlipDir(bool nflip) {
  FlipDir = nflip;
}

void SSliceTool::showOptions(bool nshow) {
  ShowOptions = nshow;
}

//************************* SVPProperties Tool *********************************

SVPProperties::SVPProperties(QObject *parent): SViewPortTool(parent) {
  setToolTip(tr("Viewport Properties"));
  setIcon(QIcon(":resources/settings-icon"));
  setText(tr("VP Properties"));
}

void SVPProperties::selected(SViewPort&) {
  setIcon(QIcon(":resources/settings-selected-icon"));
}

void SVPProperties::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/settings-icon"));
}

void SVPProperties::leftClicked(SViewPort& sourcevp, int, int) {
  // configure the context menu
  QMenu contextMenu;
  QAction setspatial("&Deform to spatial size",&contextMenu);
  setspatial.setCheckable(true);
  setspatial.setChecked(sourcevp.renderSurface().spatiallyDeform());
  connect(&setspatial,SIGNAL(triggered(bool)),
          &sourcevp.renderSurfaceWidget(),SLOT(enableSpatialDeform(bool)));
  contextMenu.addAction(&setspatial);
  QAction setshowhud("&Show HUD",&contextMenu);
  setshowhud.setCheckable(true);
  setshowhud.setChecked(sourcevp.isHUDVisible());
  connect(&setshowhud,SIGNAL(triggered(bool)),
          &sourcevp,SLOT(showHUD(bool)));
  contextMenu.addAction(&setshowhud);
  QAction setdownsample("&Performance downsampling",&contextMenu);
  setdownsample.setCheckable(true);
  setdownsample.setChecked(sourcevp.renderSurface().perfDownEnabled());
  connect(&setdownsample,SIGNAL(triggered(bool)),
          &sourcevp.renderSurfaceWidget(),SLOT(enablePerfDownsample(bool)));
  contextMenu.addAction(&setdownsample);
  QAction overlays("&Overlay data contexts",&contextMenu);
  overlays.setCheckable(true);
  overlays.setChecked(sourcevp.renderSurface().contextOverlays());
  connect(&overlays,SIGNAL(triggered(bool)),
          &sourcevp.renderSurfaceWidget(),SLOT(enableContextOverlays(bool)));
  contextMenu.addAction(&overlays);
  contextMenu.exec(QCursor::pos());
}

void SVPProperties::rightClicked(SViewPort& sourcevp, int x, int y) {
  leftClicked(sourcevp,x,y);
}

//**************************** SSnapshot Tool **********************************

SSnapShot::SSnapShot(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Left: Viewport Snapshot, Right: Snapshot all VPs"));
  setIcon(QIcon(":resources/snapshot-icon"));
  setText(tr("VP Snapshot"));
  Screenshot = new QPixmap();
}

SSnapShot::~SSnapShot() {
  delete Screenshot;
}

void SSnapShot::selected(SViewPort&) {
  setIcon(QIcon(":resources/snapshot-selected-icon"));
}

void SSnapShot::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/snapshot-icon"));
}

void SSnapShot::leftClicked(SViewPort& sourcevp, int, int) {
  sourcevp.clearFocus();
  (*Screenshot) = sourcevp.grab();
  SFileDialog *fileDialog = new SFileDialog(parentWidget());
  fileDialog->setWindowTitle(QString("Save Capture As..."));
  fileDialog->setNameFilter(tr("Images (*.png)"));
  fileDialog->setViewMode(QFileDialog::Detail);
  fileDialog->setFileMode(QFileDialog::AnyFile);
  fileDialog->setAcceptMode(QFileDialog::AcceptSave);
  fileDialog->setAttribute( Qt::WA_DeleteOnClose );
  connect(fileDialog,SIGNAL(fileSelected (const QString &)),
          this,SLOT(storeScreenshot(const QString&)));
  fileDialog->show();
}

void SSnapShot::rightClicked(SViewPort& sourcevp, int, int) {
  QWidget *parentw = sourcevp.parentWidget();
  while (true) {
    SDynamicLayout *parentl = dynamic_cast<SDynamicLayout*>(parentw);
    if (parentl == nullptr)
      break;
    else
      parentw = parentl->parentWidget();
  }
  if (parentw != nullptr) {
    sourcevp.clearFocus();
    (*Screenshot) = parentw->grab();
    SFileDialog *fileDialog = new SFileDialog(parentWidget());
    fileDialog->setWindowTitle(QString("Save Capture As..."));
    fileDialog->setNameFilter(tr("Images (*.png)"));
    fileDialog->setViewMode(QFileDialog::Detail);
    fileDialog->setFileMode(QFileDialog::AnyFile);
    fileDialog->setAcceptMode(QFileDialog::AcceptSave);
    fileDialog->setAttribute( Qt::WA_DeleteOnClose );
    connect(fileDialog,SIGNAL(fileSelected (const QString &)),
            this,SLOT(storeScreenshot(const QString&)));
    fileDialog->show();
  }
}

void SSnapShot::storeScreenshot(const QString& path) {
  Screenshot->save(path,"png",100);
}

#ifdef MOBILE
void SSnapShot::doubleClicked(SViewPort& vp, int x, int y) {
  rightClicked(vp, x, y);
}
#else
void SSnapShot::doubleClicked(SViewPort&,int,int) { }
#endif

//***************************** SExport Tool ***********************************

SExport::SExport(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Left: Export as PNG image"));
  setIcon(QIcon(":resources/export-icon"));
  setText(tr("Export PNG"));
}

SExport::~SExport() {

}

void SExport::selected(SViewPort&) {
  setIcon(QIcon(":resources/export-selected-icon"));
}

void SExport::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/export-icon"));
}

void SExport::leftClicked(SViewPort& vp, int, int) {
  SSpace *source = vp.sourceSSpace();
  if (source != nullptr && source->try_lock()) {
    std::string name;
    StoreIMG.lock();
    source->get2DRGBAInto(StoreIMG);
    name = source->getName();
    source->unlock();
    StoreIMG.unlock();
    SFileDialog *fileDialog = new SFileDialog(parentWidget());
    fileDialog->setWindowTitle(QString("Export As..."));
    fileDialog->setNameFilter(tr("Images (*.png)"));
    fileDialog->setViewMode(QFileDialog::Detail);
    fileDialog->setFileMode(QFileDialog::AnyFile);
    fileDialog->setAcceptMode(QFileDialog::AcceptSave);
    fileDialog->setDefaultSuffix("png");
    fileDialog->selectFile(name.c_str());
    fileDialog->setAttribute( Qt::WA_DeleteOnClose );
    connect(fileDialog,SIGNAL(fileSelected (const QString &)),
            this,SLOT(doExport(const QString&)));
    fileDialog->show();
  }
}

void SExport::doExport(const QString& path) {
  bgExports.addJob(std::bind(SExport::exportSSpace_noret,&StoreIMG,path));
}

bool SExport::exportSSpace(SSpace* src, const QString& path) {
  bool retval = false;
  SPNG exporter;
  exporter.setLocation(QDir::toNativeSeparators(path).toStdString());
  src->lock();
  src->progress(-1);
  try {
    exporter.storeSSpace(*src);
  }
  catch(std::exception &e) {
    SLogger::global().addMessage(std::string("SExport::exportSSpace: error: ")
                                 + e.what());
  }
  src->progress(100);
  src->unlock();
  return retval;
}

void SExport::exportSSpace_noret(SSpace* src, const QString& path) {
  exportSSpace(src, path);
}

//****************************** SInfo Tool ************************************

SInfoTool::SInfoTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("SSpace Meta-Information"));
  setIcon(QIcon(":resources/info-icon"));
  setText(tr("Vol. Information"));
  // build the singleton tag browser
  MainInfoWidget = new QWidget(parentWidget());
  SearchEdit = new QLineEdit(MainInfoWidget);
  MainTagBrowser = new tagBrowser(MainInfoWidget);
  connect(SearchEdit,SIGNAL(textChanged(QString)),
          MainTagBrowser,SLOT(newFilterString(QString)));
  QVBoxLayout *vertlayout = new QVBoxLayout(MainInfoWidget);
  vertlayout->setContentsMargins(1,1,1,1);
  vertlayout->addWidget(SearchEdit);
  vertlayout->addWidget(MainTagBrowser);
  MainInfoWidget->setLayout(vertlayout);
  MainInfoWidget->setWindowFlags(Qt::Tool);
  MainInfoWidget->hide();
  SearchEdit->setPlaceholderText("Search Information");
  SearchEdit->setInputMethodHints(Qt::ImhNoAutoUppercase|
                                  Qt::ImhNoPredictiveText);
  MainInfoWidget->setMinimumHeight(350);
}

void SInfoTool::selected(SViewPort&) {
  setIcon(QIcon(":resources/info_selected-icon"));
}

void SInfoTool::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/info-icon"));
  AdditionalWidgetHide(MainInfoWidget);
}

void SInfoTool::leftClicked(SViewPort& sourcevp, int, int) {
  if (sourcevp.sourceSSpace() != nullptr) {
    if (!MainInfoWidget->isVisible()) {
      MainInfoWidget->move(QCursor::pos());
      MainInfoWidget->resize(420,520);
    }
    MainTagBrowser->clear();
    SearchEdit->clear();
    MainTagBrowser->setSimple(true);
    MainTagBrowser->setBusyOpacity(0.0);
    MainTagBrowser->addTopLevelTag(&sourcevp.sourceSSpace()->informationNode());
    MainTagBrowser->refresh(false);
    MainTagBrowser->wait();
    MainTagBrowser->collapseAll();
    MainTagBrowser->resizeColumns(true);
#ifndef MOBILE
    MainInfoWidget->show();
#endif
    AdditionalWidget(MainInfoWidget, "Information");
    MainInfoWidget->show();
#ifndef _WIN32
    MainInfoWidget->adjustSize();
#endif
  }
}

void SInfoTool::middleClicked(SViewPort& sourcevp, int, int) {
  if (sourcevp.sourceSSpace() != nullptr) {
    QWidget *container = new QWidget(&sourcevp);
    container->setWindowTitle(sourcevp.sourceSSpace()->getName().c_str());
    container->setWindowFlags(Qt::Tool);
    container->setAttribute(Qt::WA_DeleteOnClose);
    QLineEdit *searcheedit = new QLineEdit(container);
    searcheedit->setPlaceholderText("Search Information");
    tagBrowser *newinfobrowser = new tagBrowser(container);
    connect(searcheedit,SIGNAL(textChanged(QString)),
            newinfobrowser,SLOT(newFilterString(QString)));
    QVBoxLayout *vertlayout = new QVBoxLayout(container);
    vertlayout->setContentsMargins(1,1,1,1);
    vertlayout->addWidget(searcheedit);
    vertlayout->addWidget(newinfobrowser);
    container->setLayout(vertlayout);
    newinfobrowser->setSimple(true);
    newinfobrowser->setBusyOpacity(0.0);
    container->move(QCursor::pos());
    container->resize(420,520);
    container->show();
    newinfobrowser->addTopLevelTag(&sourcevp.sourceSSpace()->informationNode());
    newinfobrowser->refresh(false);
    newinfobrowser->wait();
    newinfobrowser->collapseAll();
    newinfobrowser->resizeColumns(true);
  }
}

//****************************** SPivot Tool ***********************************

SPivotTool::SPivotTool(QObject* parent): SViewPortTool(parent),
                                                              PivotPoint(nullptr) {
  setToolTip(tr("Pivot Slice (left-click: about Y, right-click: about X, \
middle-click: free pivot)"));
  setIcon(QIcon(":resources/pivot-icon"));
  setText(tr("Pivot Plane"));
}

SPivotTool::~SPivotTool() {

}

void SPivotTool::selected(SViewPort& ) {
  setIcon(QIcon(":resources/pivot_selected-icon"));
}

void SPivotTool::deSelected(SViewPort& ) {
  setIcon(QIcon(":resources/pivot-icon"));
  clearProj();
}

#ifdef MOBILE
void SPivotTool::middleClicked
#else
void SPivotTool::leftClicked
#endif
(SViewPort& vp, int x, int y) {
  SViewPortTool::leftClicked(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      XAxisOrig  = TargetSlicer->getXAxis();
      YAxisOrig  = TargetSlicer->getYAxis();
      NormalOrig = TargetSlicer->getPlane().normal();
      SCoordinate clickpoint({x,y});
      clickpoint.clamp_floor(vp.coordToSourceSSpace(clickpoint));
      PointOrig.clamp_floor(TargetSlicer->toSourceCoords(clickpoint));
      if (PivotPoint == nullptr) {
        PivotPoint = new SPOIObject(&vp);
        PivotPoint->setSource(TargetSlicer->getSourceSSpace(),PointOrig);
        PivotPoint->setEditable(false);
        PivotPoint->setLabel("<b><font size=\"-1\" color=\"" 
                              + vp.activeColor().name() +"\">Pivot (" 
                              + QString(PointOrig.toString().c_str()) 
                              + ")</font></b>");
      }
      RotateAboutY    = true;
      RotateAboutBoth = false;
      TargetSlicer->setPlane(PointOrig,XAxisOrig,YAxisOrig);
      TargetSlicer->unlock();
      doProjections(vp,false,false);
    }
  }
}

void SPivotTool::rightClicked(SViewPort& vp, int x, int y) {
  leftClicked(vp,x,y);
  RotateAboutY = false;
}

#ifdef MOBILE
void SPivotTool::leftClicked(SViewPort& vp, int x, int y) {
  middleClicked(vp, x, y);
  RotateAboutBoth = true;
}
#else
void SPivotTool::middleClicked(SViewPort& vp, int x, int y) {
  leftClicked(vp, x, y);
  RotateAboutBoth = true;
}
#endif

void SPivotTool::dragged(SViewPort& vp, int x, int y) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr && TargetSlicer->sliceable()) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      if (RotateAboutY || RotateAboutBoth) {
        ReferencePoint.setDim(2);
        // handle x-pivot
        NormalOrig = TargetSlicer->getPlane().normal();
        XAxisOrig  = TargetSlicer->getXAxis();
        YAxisOrig  = TargetSlicer->getYAxis();
        float xscalepos = (float)(x - ReferencePoint.x()) /
                                    (float)(vp.renderSurfaceWidget().width()/2);
        xscalepos > 0.5  ? xscalepos = 0.5 : 0;
        xscalepos < -0.5 ? xscalepos = -0.5 : 0;
        SVector newxaxis(3);
        newxaxis = XAxisOrig - (XAxisOrig * SGeom::fabs(xscalepos));
        newxaxis = newxaxis + (NormalOrig * xscalepos);
        TargetSlicer->setPlane(PointOrig,newxaxis,YAxisOrig);
      }
      if (!RotateAboutY || RotateAboutBoth) {
        // handle y-pivot
        NormalOrig = TargetSlicer->getPlane().normal();
        XAxisOrig  = TargetSlicer->getXAxis();
        YAxisOrig  = TargetSlicer->getYAxis();
        float yscalepos = (float)(y - ReferencePoint.y()) /
                                    (float)(vp.renderSurfaceWidget().height()/2);
        yscalepos > 0.5  ? yscalepos = 0.5 : 0;
        yscalepos < -0.5 ? yscalepos = -0.5 : 0;
        SVector newyaxis(3);
        newyaxis = YAxisOrig - (YAxisOrig * SGeom::fabs(yscalepos));
        newyaxis = newyaxis + (NormalOrig * yscalepos);
        TargetSlicer->setPlane(PointOrig,XAxisOrig,newyaxis);
      }
      outputSlicePos(vp,*TargetSlicer);
      TargetSlicer->unlock();
      TargetSlicer->emitRefresh(false);
      ReferencePoint.xy(x,y);
      doProjections(vp,false,false);
    }
  }
}

void SPivotTool::buttonReleased(SViewPort& vp,int, int) {
  if (PivotPoint != nullptr) {
    PivotPoint->deleteLater();
    clearProj();
    PivotPoint = nullptr;
    SSlicer *slicer = dynamic_cast<SSlicer*>(vp.sourceSSpace());
    if (slicer != nullptr) {
      slicer->emitRefresh(true);
    }
  }
}

void SPivotTool::outputSlicePos(SViewPort& vp, SSlicer& slice) {
  QString posinfo;
  posinfo += "Point:  ";
  posinfo += slice.getPlane().point().toString().c_str();
  posinfo += "<br>X-Axis: ";
  posinfo += slice.getXAxis().toString().c_str();
  posinfo += "<br>Y-Axis: ";
  posinfo += slice.getYAxis().toString().c_str();
  posinfo += "<br>Normal: ";
  posinfo += slice.getPlane().normal().toString().c_str();
  vp.showMessage(posinfo);
}

void SPivotTool::doProjections(SViewPort& vp, bool force, bool fast) {
  clearProj();
  QList<QWidget*> projs = SPointInfoTool::showSliceProjections(vp,force,fast);
  for (int p=0; p<projs.size(); p++) {
    connect(projs[p],SIGNAL(destroyed(QObject*)),
            this,SLOT(remProject(QObject*)));
  }
  Projections.append(projs);
}

void SPivotTool::clearProj() {
  for (int p=0; p<Projections.size(); p++) {
    Projections[p]->deleteLater();
  }
}

void SPivotTool::remProject(QObject* src) {
  QWidget *srcp = static_cast<QWidget*>(src);
  if (Projections.count(srcp)) {
    Projections.erase(Projections.begin()+Projections.indexOf(srcp));
  }
}

//****************************** SRotate Tool **********************************

SRotateTool::SRotateTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Rotate Slice (left-click: CW, right-click: ACW, \
middle-click: free rotate)"));
  setIcon(QIcon(":resources/rotate-icon"));
  setText(tr("Rotate"));
}

SRotateTool::~SRotateTool() {

}

void SRotateTool::selected(SViewPort& ) {
  setIcon(QIcon(":resources/rotate_selected-icon"));
}

void SRotateTool::deSelected(SViewPort& ) {
  setIcon(QIcon(":resources/rotate-icon"));
}

void SRotateTool::configVars(SViewPort& vp, int x, int y) {
  SViewPortTool::leftClicked(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      XAxisOrig  = TargetSlicer->getXAxis();
      YAxisOrig  = TargetSlicer->getYAxis();
      SCoordinate clickpoint({x,y});
      clickpoint.clamp_floor(vp.coordToSourceSSpace(clickpoint));
      PointOrig.clamp_floor(TargetSlicer->toSourceCoords(clickpoint));
      TargetSlicer->unlock();
    }
  }
}

void SRotateTool::leftClicked(SViewPort& vp, int x, int y) {
  // rotate 90 degrees CW
  configVars(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      TargetSlicer->setPlane(PointOrig,SVector(3)-YAxisOrig,XAxisOrig);
      outputSlicePos(vp,*TargetSlicer);
      TargetSlicer->unlock();
      DragEnabled = false;
    }
  }
}

void SRotateTool::rightClicked(SViewPort& vp, int x, int y) {
  // rotate 90 degrees ACW
  configVars(vp,x,y);
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      TargetSlicer->setPlane(PointOrig,YAxisOrig,SVector(3)-XAxisOrig);
      outputSlicePos(vp,*TargetSlicer);
      TargetSlicer->unlock();
      DragEnabled = false;
    }
  }
}

void SRotateTool::middleClicked(SViewPort& vp, int x, int y) {
  configVars(vp,x,y);
  DragEnabled = true;
}

void SRotateTool::dragged(SViewPort& vp, int, int y) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr && DragEnabled) {
    bool locked = TargetSlicer->try_lock();
    if (locked) {
      // handle y-movement
      float yscalepos = (float)(y - ReferencePoint.y()) /
                                 (float)(vp.renderSurfaceWidget().height()/2);
      yscalepos > 1.0  ? yscalepos = 1.0 : 0;
      yscalepos < -1.0 ? yscalepos = -1.0 : 0;
      SVector newyaxis(3), newxaxis;
      newyaxis = YAxisOrig - (YAxisOrig * SGeom::fabs(yscalepos));
      newyaxis = newyaxis  + (XAxisOrig * yscalepos);
      newxaxis = XAxisOrig - (XAxisOrig * SGeom::fabs(yscalepos));
      newxaxis = newxaxis  + ((SVector(3)-YAxisOrig) * yscalepos);
      TargetSlicer->setPlane(PointOrig,newxaxis,newyaxis);
      outputSlicePos(vp,*TargetSlicer);
      TargetSlicer->unlock();
      TargetSlicer->emitRefresh(false);
    }
  }
}

void SRotateTool::buttonReleased(SViewPort& vp, int, int) {
  SSlicer *slicer = dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (slicer != nullptr) {
    slicer->emitRefresh(true);
  }
}

void SRotateTool::outputSlicePos(SViewPort& vp, SSlicer& slice) {
  QString posinfo;
  posinfo += "Point:  ";
  posinfo += slice.getPlane().point().toString().c_str();
  posinfo += "<br>X-Axis: ";
  posinfo += slice.getXAxis().toString().c_str();
  posinfo += "<br>Y-Axis: ";
  posinfo += slice.getYAxis().toString().c_str();
  posinfo += "<br>Normal: ";
  posinfo += slice.getPlane().normal().toString().c_str();
  vp.showMessage(posinfo);
}

//**************************** SPointInfo Tool *********************************

SPointInfoTool::SPointInfoTool(QObject* parent): SViewPortTool(parent),
                                                        DoReleaseUpdate(false) {
  setToolTip(tr("Point Information, localizer, projection"));
  setIcon(QIcon(":resources/marque-small-plain"));
  setText(tr("Point Information"));
  MainInfoWidget    = new QWidget(parentWidget());
  MainMessageWidget = new QLabel(MainInfoWidget);
  GraphWidget       = new SGrapher(MainInfoWidget);
  MainMessageWidget->setWordWrap(true);
  SynchVPs          = new QCheckBox("Project Location",MainInfoWidget);
  ForceSyncVPs      = new QCheckBox("Force Projection",MainInfoWidget);
  QVBoxLayout *infowidglatout = new QVBoxLayout(MainInfoWidget);
  infowidglatout->addWidget(SynchVPs);
  infowidglatout->addWidget(ForceSyncVPs);
  infowidglatout->addWidget(MainMessageWidget);
  infowidglatout->addWidget(GraphWidget);
  MainInfoWidget->setLayout(infowidglatout);
  MainInfoWidget->hide();
  GraphWidget->hide();
  GraphWidget->setDrawGrid(true);
  GraphWidget->setShowLabels(false);
  MainMessageWidget->setTextInteractionFlags(Qt::TextSelectableByMouse|
                                             Qt::TextSelectableByKeyboard);
  MainMessageWidget->setWordWrap(true);
  GraphWidget->setMinimumHeight(300);
  SynchVPs->setChecked(true);
  ForceSyncVPs->setChecked(false);
  ForceSyncVPs->setVisible((bool)SynchVPs->checkState());
  ForceSyncVPs->setToolTip(tr("Assume that all adjacent local spaces inhabit ")+
                           QString(tr("the same global space")));
  connect(SynchVPs,SIGNAL(stateChanged(int)),
          this,SLOT(showForceCheck(int)));
}

SPointInfoTool::~SPointInfoTool() {
  clearProjs();
}

void SPointInfoTool::showForceCheck(int checkstate) {
  ForceSyncVPs->setVisible((bool)checkstate);
}

void SPointInfoTool::selected(SViewPort& ) {
  setIcon(QIcon(":resources/marque-small-selected"));
  MainMessageWidget->setText("Select an SSpace point \
<br/><br/><br/><br/><br/><br/><br/>");
#ifndef MOBILE
  if (!MainInfoWidget->isVisible()) {
    AdditionalWidget(MainInfoWidget,"Point Information");
    MainInfoWidget->move(QCursor::pos());
  }
#endif
}

void SPointInfoTool::deSelected(SViewPort& ) {
  setIcon(QIcon(":resources/marque-small-plain"));
  AdditionalWidgetHide(MainInfoWidget);
  clearProjs();
}

static QString sanitlength(const QString& src) {
  QString result = src;
#ifdef MOBILE
  const int slimitl = 13;
#else
  const int slimitl = 40;
#endif
  if (result.size() > slimitl) {
    result = result.remove(0,result.size()-slimitl);
    result = "..." + result;
  }
  return result;
}

QString SPointInfoTool::genInfoString(SViewPort& vp, int x, int y) {
  QString result;
  result = "<table spacing=\"5\">";
  result += "<hr><tr><td>Viewport </td><td>" + sanitlength(vp.name())
          + "</td></tr>";
  SSpace *sourcespace = vp.sourceSSpace();
  if (sourcespace != nullptr) {
    SVector point;
    SCoordinate pointp, conv;
    SElem *pointpox = sourcespace->getNativeSElemP();
    SElem::DataSource tmpsource = pointpox->newDataSource();
    pointpox->source(tmpsource);
    point  = vp.coordToSourceSSpace({x,y});
    conv.clamp_round(point);
    if (sourcespace->extent().withinSpace(conv)) {
      sourcespace->SElemInto(conv,*pointpox);
      pointp = sourcespace->toSourceCoords(conv);
      result += "<tr><td>Space </td><td>"
          + sanitlength(QString(sourcespace->getName().c_str())) + "</td></tr>";
      result += "<tr><td>Point </td><td>" 
            + QString(point.toString().c_str()) + "</td></tr>";
      result += "<tr><td>Point' </td><td>" 
            + QString(pointp.toString().c_str()) + "</td></tr>";
      if (sourcespace->globalSpaceID().size() > 0) {
        SVector globalp = sourcespace->toGlobalSpace(point);
        result += "<tr><td>Global </td><td>" 
               + QString(globalp.toString().c_str()) + "</td></tr>";
        result += "<tr><td>Space ID</td><td>" 
               + sanitlength(QString(sourcespace->globalSpaceID().c_str())) +
               "</td></tr>";
      }
      for (CONTEXTS_t con=0; con < sourcespace->contexts(); con++) {
        if (sourcespace->contexts()>1) {
          result += "<tr><td>Context-" + QString::number(con) 
                + "</td><td><center>-------------</center><td></td></tr>";
        }
        result += "<tr><td>Type </td><td>"
              + QString((*pointpox).name().c_str()) + "</td></tr>";
        result += "<tr><td>Intensity </td><td>";
        if (pointpox->isSigned())
          result += QString::number(pointpox->intensitySigned())+"</td></tr>";
        else
          result += QString::number(pointpox->intensityUnsigned())+"</td></tr>";
        result += "<tr><td>RGBA </td><td>"
              + QString::number(pointpox->red()) + ", "
              + QString::number(pointpox->green()) + ", "
              + QString::number(pointpox->blue()) + "</td></tr>";
      }
    }
    else { // balance newlines for consistent height
      result += "<br/><br/><br/><br/><br/><br/><br/>";
    }
    result += "</table><hr>";
    delete   pointpox;
    delete[] tmpsource;
  }
  return result;
}

void SPointInfoTool::synchVPs(SViewPort& vp, int x, int y, bool checkspaceids,
                              bool rdepth, bool centerinvp) {
  SSpace *sourcespace = vp.sourceSSpace();
  if (sourcespace != nullptr) {
    std::vector<SConnectable*> reflist;
    sourcespace->lock();
    SDynamicLayout *parentlayout   = vp.layout();
    std::string     spaceid        = sourcespace->globalSpaceID();
    SVector         globalposition = sourcespace->
                                  toGlobalSpace(vp.coordToSourceSSpace({x,y}));
    if (parentlayout != nullptr) {
      QList<SViewPort*> vps= parentlayout->viewports();
      for (int vpn=0; vpn<vps.size(); vpn++) {
        if (vps[vpn] != &vp) {
          SSlicer *slicetest = dynamic_cast<SSlicer*>(vps[vpn]->sourceSSpace());
          if (slicetest != nullptr) {
            if ( ( (slicetest->globalSpaceID() == spaceid && spaceid.size()>0)||
                  ( (&(slicetest->getSourceSSpace()) ==
                    &(sourcespace->getSourceSSpace())) || (!checkspaceids) )
                ) && slicetest != sourcespace
              ) {
              SVector localpos(slicetest->fromGlobalSpace(globalposition));
              SCoordinate localclamped;
              localclamped.clamp_round(localpos);
              if (slicetest->getSourceSSpace().extent().
                                                  withinSpace(localclamped)) {
                if ( ! (slicetest->isOnSlice(localclamped)) ) {
                  slicetest->setPlane(localclamped,
                                      slicetest->getXAxis(),
                                      slicetest->getYAxis());
                  reflist.push_back(slicetest);
                }
                if (centerinvp) {
                  SCoordinate planeloc;
                  slicetest->fromSourceCoords(localclamped,planeloc);
                  vps[vpn]->centerOnSourceCoord(planeloc);
                }
              }
            }
          }
        }
      }
    }
    sourcespace->unlock();
    for (unsigned r=0;r<reflist.size();r++)
      reflist[r]->emitRefresh(rdepth);
  }
}

QList<QWidget*>
         SPointInfoTool::showSliceProjections(SViewPort& primaryvp, bool force,
                                              bool fast) {
  QList<QWidget*> result;
  SSlicer additonalslice;
  SSlicer *primaryslice = dynamic_cast<SSlicer*>(primaryvp.sourceSSpace());
  SDynamicLayout *parentlayout = primaryvp.layout();
  QList <SSlicer*> sourceplanes;
  sourceplanes.push_back(primaryslice);
  if (primaryslice->thickness() > 1) {
    // configure additional slicer to end depth
    additonalslice.sconnect(primaryslice->getSourceSSpace());
    additonalslice.setPlane(primaryslice->getPlane().point()+
                             (primaryslice->getPlane().normal()*
                              primaryslice->thickness()),
                            primaryslice->getXAxis(),primaryslice->getYAxis());
    sourceplanes.push_back(&additonalslice);
  }
  for (int s=0; s<sourceplanes.size(); s++) {
    SSlicer *primaryslicespace = sourceplanes[s];
    std::string  pspaceid = primaryslicespace->globalSpaceID();
    if ((primaryslicespace != nullptr) && (parentlayout != nullptr) ) {
      QList<SViewPort*> vps = parentlayout->viewports();
      SVector gp1, gp2; // global plane points (along plane normal)
      gp1 = gp2 = primaryslicespace->getPlane().point();
      gp1 = primaryslicespace->getSourceSSpace().toGlobalSpace(gp1);
      gp2 = gp2 + 
             primaryslicespace->getXAxis().cross(primaryslicespace->getYAxis());
      gp2 = primaryslicespace->getSourceSSpace().toGlobalSpace(gp2);
      for (int vp=0; vp<vps.size(); vp++) {
        SSlicer *targetslicespace =
                                dynamic_cast<SSlicer*>(vps[vp]->sourceSSpace());
        if ((targetslicespace != nullptr)&&
            (targetslicespace != primaryslicespace)&&
            (( (targetslicespace->globalSpaceID() == pspaceid) &&
                pspaceid.size() > 0 
             ) || force ||
              ((&(targetslicespace->getSourceSSpace())) ==
               (&(primaryslicespace->getSourceSSpace()))
              )
            )
          ) {
          // project the slice-intersection line onto this slice
          // map first plane to second space (for inter-space projections)
          SVector lp1, lp2;
          lp1 = targetslicespace->getSourceSSpace().fromGlobalSpace(gp1);
          lp2 = targetslicespace->getSourceSSpace().fromGlobalSpace(gp2);
          lp2 = (lp2-lp1).unit(); // make a plane normal
          SPlane planeintarg(lp1,lp2);
          SLine intersection = SGeom::planeplaneintersection
                                      (planeintarg,targetslicespace->getPlane());
          if ( (intersection.start().getDim()==3) &&
              (intersection.end().getDim()==3)) {
            SCoordinate p1, p2, pre1, pre2;
            SVector pdir;
            pre1.clamp_round(intersection.start());
            pre2.clamp_round(intersection.end()+(intersection.unit()*100));
            targetslicespace->fromSourceCoords(pre1,p1);
            targetslicespace->fromSourceCoords(pre2,p2);
            pdir = p2 - p1;
            pdir = pdir.unit();
            p1.clamp_round(vps[vp]->sourceSSpaceToCoord(p1)); // point on VP
            if ((p1.getDim()==2) && (p2.getDim()==2)) {
              // configure the line object
              SVector TLC, BRC, Scaledir, p1v;
              p1v = p1;
              Scaledir = pdir * (vps[vp]->width()*2);
              TLC = p1v - Scaledir;
              BRC = p1v + Scaledir;
              SLineDrawWidget *newprojline = new SLineDrawWidget(vps[vp],
                                                                fast||true);
              connect(vps[vp],SIGNAL(resized(SViewPort&)),
                      newprojline,SLOT(hide()));
              connect(vps[vp],SIGNAL(updated(bool)),
                      newprojline,SLOT(cleanup(bool)));
              if(s == 0) {
                newprojline->setColor(QColor(0,60,255,200));
#ifdef MOBILE
                newprojline->setThickness(2);
#endif
              }
              else
                newprojline->setColor(QColor(175,0,0,200));
              newprojline->setLine(QPoint(TLC.x(),TLC.y()),
                                  QPoint(BRC.x(),BRC.y()));
              newprojline->show();
              result.push_back(newprojline);
            }
          }
        }
      }
    }
  }
  return result;
}

QList<SPOIObject*> SPointInfoTool::showProjections(SViewPort& vp,
                                                   int x, int y,
                                                   bool checkspaceids,
                                                   bool fast) {
  QList<SPOIObject*> newprojections;
  SSpace *sourcespace = vp.endSourceSSpace();
  if (sourcespace != nullptr) {
    sourcespace->lock();
    SDynamicLayout *parentlayout   = vp.layout();
    std::string     spaceid        = sourcespace->globalSpaceID();
    SVector         globalposition = sourcespace->
                              toGlobalSpace(vp.coordToEndSourceSSpace({x,y}));
    sourcespace->unlock();
    if (parentlayout != nullptr) {
      QList<SViewPort*> vps= parentlayout->viewports();
      for (int vpl=0; vpl<vps.size(); vpl++) {
        SSpace *targetspace = vps[vpl]->endSourceSSpace();
        if (targetspace != nullptr) {
          targetspace->lock();
          if (((targetspace->globalSpaceID()==spaceid)
                    &&(spaceid.size()>0)) ||
                ( targetspace == sourcespace ) || (!checkspaceids)
            ) {
            SPOIObject *newprojpoint = new SPOIObject(vps[vpl],!fast);
            SVector projvect = targetspace->fromGlobalSpace(globalposition);
            newprojpoint->setSource(*targetspace,projvect);
            newprojpoint->setEditable(false);
            if (!fast) {
              newprojpoint->setLabel("<b><font size=\"-1\" color=\""
                                    + vp.activeColor().name() +"\">("
                                    + QString(projvect.toString().c_str())
                                    + ")</font></b>");
            }
            newprojections.push_back(newprojpoint);
          }
          targetspace->unlock();
        }
      }
    }
    else { // not in a layout; lone-ranger VP
      SPOIObject *newprojpoint = new SPOIObject(&vp,!fast);
      SVector projvect = vp.coordToEndSourceSSpace({x,y});
      newprojpoint->setSource(*sourcespace,projvect);
      newprojpoint->setEditable(false);
      if (!fast) {
        newprojpoint->setLabel("<b><font size=\"-1\" color=\""
                              + vp.activeColor().name() +"\">("
                              + QString(projvect.toString().c_str())
                              + ")</font></b>");
      }
      newprojections.push_back(newprojpoint);
    }
  }
  return newprojections;
}

std::vector< std::pair< float, float > > SPointInfoTool::getTimePoints
                                                 (SViewPort& vp, int x, int y) {
  std::vector< std::pair< float, float > > result;
  SSpace *sourcespace = vp.endSourceSSpace();
  if (sourcespace != nullptr) {
    SVector coord = vp.coordToEndSourceSSpace({x,y});
    SCoordinate lcoord;
    lcoord.clamp_round(coord);
    if (sourcespace->extent().withinSpace(lcoord)) {
      SElem *poxval = sourcespace->getNativeSElemP();
      for (SCoordinate::Precision t=0; t<sourcespace->extent().t(); t++) {
        lcoord.setDim(3);
        lcoord.append({t});
        poxval->source(sourcespace->SElemData_Passive(lcoord));
        result.push_back(std::make_pair<float,float>
                                                ((float)t,poxval->intensity()));
      }
      delete poxval;
    }
  }
  return result;
}

void SPointInfoTool::updateLocation(SViewPort& vp, int x, int y, bool fast) {
#ifdef MOBILE
  if (!fast) {
#endif
  MainMessageWidget->setText(genInfoString(vp,x,y));
  SSpace *sourcespace = vp.endSourceSSpace();
  if (sourcespace != nullptr) {
    sourcespace->lock();
    if (sourcespace->extent().getDim() > 3) {
      MainMessageWidget->setText(MainMessageWidget->text()
                       + "<br/><center><b>Intensity 4D</b></center>");
      // show time line
      if (GraphWidget->dataSets() > 0)
        GraphWidget->removeDataSet(0);
      GraphWidget->addDataSet(getTimePoints(vp,x,y));
      GraphWidget->show();
    }
    else {
      GraphWidget->hide();
    }
    sourcespace->unlock();
  }
  MainMessageWidget->adjustSize();
  if (GraphWidget->isVisible())
    MainInfoWidget->setMinimumHeight(MainMessageWidget->height()+
                                     GraphWidget->height() +
                                     SynchVPs->height() + 50);
  else
    MainInfoWidget->setMinimumHeight(MainMessageWidget->height()+
                                     SynchVPs->height() + 50);
#ifdef MOBILE
  }
#endif
  if (SynchVPs->isChecked()) {
    SPointInfoTool::synchVPs(vp,x,y,!((bool)ForceSyncVPs->checkState()),!fast);
    QList<QWidget*>    projlines = SPointInfoTool::showSliceProjections
                                   (vp,((bool)ForceSyncVPs->checkState()),fast);
    QList<SPOIObject*> newprojs  = SPointInfoTool::showProjections(vp,x,y,
                                      !((bool)ForceSyncVPs->checkState()),fast);
    for (int p=0;p<newprojs.size();p++)
      connect(newprojs[p],SIGNAL(destroyed(QObject*)),
              this,SLOT(removeProjP(QObject*)));
    for (int p=0;p<projlines.size();p++)
      connect(projlines[p],SIGNAL(destroyed(QObject*)),
              this,SLOT(removeProjP(QObject*)));
    clearProjs();
    Projections.append(projlines);
    for (int p=0; p<newprojs.size(); p++)
      Projections.push_back(newprojs[p]);
  }
}

void SPointInfoTool::leftClicked(SViewPort& vp, int x, int y) {
  updateLocation(vp,x,y,false);
  AdditionalWidget(MainInfoWidget,"Point Information");
}

void SPointInfoTool::buttonReleased(SViewPort& vp, int x, int y) {
 if (DoReleaseUpdate)
   updateLocation(vp,x,y,false);
}

void SPointInfoTool::middleClicked(SViewPort&, int, int) {

}

void SPointInfoTool::dragged(SViewPort& vp, int x, int y) {
  updateLocation(vp,x,y,true);
  DoReleaseUpdate = true;
}

void SPointInfoTool::removeProjP(QObject* delp) {
  for (int p=0; p<Projections.size(); p++) {
    if (Projections[p] == delp) {
      Projections.erase(Projections.begin()+p);
      break;
    }
  }
}

void SPointInfoTool::clearProjs() {
  for (QList<QWidget*>::iterator p = Projections.begin();
       p!= Projections.end(); p++) {
      (*p)->deleteLater();
  }
}

//**************************** TimeSlicer Tool *********************************

STimeSliceTool::STimeSliceTool(QObject* parent): SSliderTool(parent),
                                                              VPSelected(nullptr) {
  setToolTip(tr("Increment a slice through time"));
  setIcon(QIcon(":resources/timeslice-icon"));
  setText(tr("Time Point"));
  QSlider *isslider = dynamic_cast<QSlider*>(VPSlider);
  if (isslider != nullptr) {
    isslider->setTickInterval(1);
    isslider->setTickPosition(QSlider::TicksRight);
  }
}

STimeSliceTool::~STimeSliceTool() {

}

void STimeSliceTool::selected(SViewPort& nvp) {
  VPSelected = &nvp;
  setIcon(QIcon(":resources/timeslice_selected-icon"));
  viewportUpdated(nvp);
  leftClicked(nvp,0,0);
}

void STimeSliceTool::deSelected(SViewPort& nvp) {
  VPSelected = nullptr;
  setIcon(QIcon(":resources/timeslice-icon"));
  SSliderTool::deSelected(nvp);
}

void STimeSliceTool::leftClicked(SViewPort& vp, int x, int y) {
  VPSelected = &vp;
  SSliderTool::leftClicked(vp, x, y);
  viewportUpdated(vp);
}

void STimeSliceTool::viewportUpdated(SViewPort& vp) {
  SSlicer *TargetSlicer =  dynamic_cast<SSlicer*>(vp.sourceSSpace());
  if (TargetSlicer != nullptr) {
    bool islocked = TargetSlicer->try_lock();
    if (islocked) {
      SCoordinate coordsuff = TargetSlicer->coordinateSuffix();
      if (coordsuff.getDim() > 0) {
        VPSlider->setValue(coordsuff[0]);
      }
      TargetSlicer->unlock();
    }
    configureSlider();
  }
}

int STimeSliceTool::sliderMin() {
  return 1;
}

int STimeSliceTool::sliderMax() {
  int retval = 0;
  if (VPSelected != nullptr) {
    SSpace *sourcespace = VPSelected->endSourceSSpace();
    if (sourcespace != nullptr) {
      sysInfo::sleep(50);
      bool islocked = sourcespace->try_lock();
      if (islocked &&  (sourcespace->extent().getDim() > 3)) {
        retval = sourcespace->extent().t();
      }
      if (islocked)
        sourcespace->unlock();
    }
  }
  return retval;
}

void STimeSliceTool::doSliderValue(int pos) {
  if (VPSelected != nullptr) {
    SSlicer *slicersource = dynamic_cast<SSlicer*>(VPSelected->sourceSSpace());
    SSpace  *sourcespace  = VPSelected->endSourceSSpace();
    if (slicersource != nullptr && sourcespace != nullptr) {
      bool islocked = slicersource->try_lock();
      if (islocked) {
        if (sourcespace->extent().getDim() > 3) {
          slicersource->setCoordinateSuffix({pos-1});
          VPSelected->showMessage("Index: " + QString::number(pos));
        }
        else {
          VPSelected->showMessage("No timepoints...");
        }
        slicersource->unlock();
        slicersource->refresh(false);
      }
    }
    else {
      VPSelected->showMessage("Not sliceable...");
    }
  }
}

//**************************** SPOIObject Tool *********************************

SPOIObject::SPOIObject(SViewPort* parent, bool hq,
                       Qt::WindowFlags f): QLabel(parent, f),
 SourceImage(nullptr), Editable(true), Link(nullptr), LinkLine(nullptr), LabelSet(nullptr) {
  init(parent,hq);
}

SPOIObject::SPOIObject(SViewPort* parent, int vxx, int vyy, const QString &lb,
                       bool hq):
 QLabel(parent), SourceImage(nullptr), Editable(true), Link(nullptr), LinkLine(nullptr),
                                                                LabelSet(nullptr) {
  init(parent,hq);
  if (parent != nullptr) {
    SSpace *source = parent->endSourceSSpace();
    if (source != nullptr) {
      SPoint srcloc = parent->coordToEndSourceSSpace({vxx,vyy});
      setSource(*source,srcloc);
      setLabel(lb);
    }
  }
}

void SPOIObject::init(SViewPort* parent, bool hq) {
  setWriteDistance(false);
  hide();
  Label = new QLabel(parent);
  Label->hide();
  if (hq) {
    QGraphicsDropShadowEffect *newshadow =
                                      new QGraphicsDropShadowEffect(Label);
    newshadow->setColor(QColor(0,0,0));
    newshadow->setBlurRadius(5);
    newshadow->setOffset(0,0);
    Label->setGraphicsEffect(newshadow);
  }
  QPixmap defaultpixmap;
  defaultpixmap.load(":resources/marque-small-centre");
  setPixmap(defaultpixmap.scaledToWidth(32,Qt::SmoothTransformation));
  connect(parent,SIGNAL(updated(bool)),this,SLOT(refresh()));
  connect(parent,SIGNAL(resized(SViewPort&)),this,SLOT(refresh()));
}

SPOIObject::~SPOIObject() {
  Label->deleteLater();
  if (LinkLine != nullptr)
    LinkLine->deleteLater();
}

void SPOIObject::setSource(SSpace& newsrc, const SPoint& newpoint) {
  SourceImage   = &newsrc;
  SourceImageID = newsrc.globalSpaceID().c_str();
  Point         = newpoint;
  refresh();
  emit modified(this);
}

SSpace* SPOIObject::source() const {
  return SourceImage;
}

SPoint SPOIObject::sourcePoint() const {
  return Point;
}

void SPOIObject::setPixmap(const QPixmap& newpixmap) {
  QPixmap tmppix;
#ifdef MOBILE
  tmppix = newpixmap.scaled(goodIconSize(),goodIconSize(),Qt::KeepAspectRatio,
                            Qt::SmoothTransformation);
#else
  tmppix = newpixmap;
#endif
  QLabel::setPixmap(tmppix);
  adjustSize();
}

void SPOIObject::setLabel(const QString& newlabel) {
  Label->setText(newlabel);
  Label->adjustSize();
  refresh();
  emit modified(this);
}

void SPOIObject::setLabelFr(const QString& newlabel) {
  setLabel(QString("<font color=\"white\">") +
             newlabel +
             QString("</font>"));
}

void SPOIObject::setEditable(bool nmove) {
  Editable = nmove;
  if (!Editable) {
    setAttribute(Qt::WA_TransparentForMouseEvents);
  }
  else {
    setAttribute(Qt::WA_TransparentForMouseEvents,false);
  }
}

QString SPOIObject::label() const {
  return Label->text();
}

SPOIObject* SPOIObject::link() const {
  return Link;
}

void SPOIObject::mousePressEvent(QMouseEvent* event) {
  event->accept();
  QLabel::mousePressEvent(event);
  if ( (event->button() == Qt::RightButton) && Editable )
    deleteLater();
}

void SPOIObject::mouseReleaseEvent(QMouseEvent* event) {
  event->accept();
  QLabel::mouseReleaseEvent(event);
  if (Editable) {
    QPoint newpos = parentWidget()->mapFromGlobal(QCursor::pos());
    moveTo(newpos.x(),newpos.y());
    emit placed(newpos.x(),newpos.y());
  }
}

void SPOIObject::mouseDoubleClickEvent(QMouseEvent* event) {
  event->accept();
  QWidget::mouseDoubleClickEvent(event);
  if (Editable) {
    if (LabelSet == nullptr) {
      LabelSet = new QInputDialog(this);
      LabelSet->setWindowTitle("Modify Label");
      LabelSet->setInputMode(QInputDialog::TextInput);
      LabelSet->setLabelText("New Label:");
      LabelSet->setOkButtonText("&Set");
      connect(LabelSet,SIGNAL(textValueSelected(const QString&)),
              this,SLOT(setLabelFr(const QString&)));
    }
    LabelSet->show();
  }
}

void SPOIObject::mouseMoveEvent(QMouseEvent* event) {
  event->accept();
  QLabel::mouseMoveEvent(event);
  if (Editable) {
    QPoint newpos = parentWidget()->mapFromGlobal(QCursor::pos());
    move(newpos.x()-(width()/2),newpos.y()-(height()/2));
    Label->move(x()+width()-12,y());
  }
}

void SPOIObject::moveEvent(QMoveEvent* mev) {
  mev->accept();
  QWidget::moveEvent(mev);
  emit moved(x(),y());
  drawLink();
}

void SPOIObject::moveTo(int x, int y) {
  if (parentWidget() != nullptr) {
    // find the position on the source image
    SViewPort *vp = dynamic_cast<SViewPort*>(parentWidget());
    if (vp != nullptr) {
      bool found = false;
      SCoordinate newloc;
      newloc.clamp_round(vp->coordToSourceSSpace({x,y}));
      SSpace *curspace = vp->sourceSSpace();
      while(true) {
        if (curspace == nullptr)
          break;
        SSpace *nextspace = &(curspace->getSourceSSpace());
        if (nextspace == SourceImage) {
          newloc = curspace->toSourceCoords(newloc);
          found = true;
          break;
        } else {
          if (nextspace == curspace)
            break;
        }
        curspace = nextspace;
      }
      if (found) {
        setSource(*SourceImage,newloc);
        refresh();
      }
    }
  }
}

void SPOIObject::link(SPOIObject& newlnk) {
  unlink(Link);
  Link = &newlnk;
  connect(&newlnk,SIGNAL(destroyed(QObject*)),this,SLOT(unlink(QObject*)));
  connect(&newlnk,SIGNAL(moved(int,int)),this,SLOT(drawLink()));
  connect(&newlnk,SIGNAL(placed(int,int)),
          this,SIGNAL(placed(int,int)));
  connect(&newlnk,SIGNAL(placed(int,int)),SLOT(refresh()));
  LinkLine = new SLineDrawWidget(parentWidget());
  LinkLine->setThickness(1);
  SViewPort *testvp = dynamic_cast<SViewPort*>(parentWidget());
  if (testvp != nullptr) {
    LinkLine->setColor(testvp->activeColor());
  }
  drawLink();
  emit modified(this);
  emit placed(0,0);
  writeDistance();
}

void SPOIObject::unlink(QObject* ulnk) {
  if (ulnk == Link) {
    Link = nullptr;
    delete LinkLine;
    LinkLine = nullptr;
    emit modified(this);
  }
}

void SPOIObject::drawLink() {
  if ( (Link != nullptr) && (LinkLine != nullptr) ) {
    if (Link->isVisible() && isVisible()) {
      LinkLine->setVisible(true);
      LinkLine->setLine(pos()+QPoint(width()/2,(height()/2)+1),
                      Link->pos()+QPoint(Link->width()/2,(Link->height()/2)+1));
    }
    else {
      LinkLine->setVisible(false);
    }
  }
}

void SPOIObject::setWriteDistance(bool nwrite) {
  WriteDistance = nwrite;
  writeDistance(*this,Point,0.0);
}

void SPOIObject::writeDistance(SPOIObject &targ, SPoint lastpos, float accum) {
  if (WriteDistance) {
    float dist = 0.0;
    if (&targ != this) {
      QColor textcol(255,255,255);
      SViewPort *testvp = dynamic_cast<SViewPort*>(parentWidget());
      if (testvp != nullptr)
        textcol = testvp->activeColor();
      dist = (float)((targ.Point - lastpos) * SourceImage->spacing()).mag();
      targ.setLabel( QString("<font color=\"" + textcol.name() + "\">") +
                     QString::number(dist,'f',2) + 
                     QString(SourceImage->spacingUnits().c_str()) +  
                     QString(" (") + QString::number(accum+dist,'f',2) + 
                     QString(SourceImage->spacingUnits().c_str()) + 
                     QString(") </font>"));
    }
    if (targ.Link != nullptr) {
      writeDistance(*targ.Link,targ.Point,accum+dist);
    }
  }
}

void SPOIObject::writeDistance() {
  writeDistance(*this,Point,0.0);
}

void SPOIObject::refresh() {
  // Check if the point is visible on the surface and position; hide if not
  SViewPort *parentvp = dynamic_cast<SViewPort*>(parentWidget());
  if (parentvp != nullptr) {
    // first, find the source image in VP pipeline and build a stack
    bool found = false;
    std::vector<SSpace*> sspacestack;
    SSpace *curspace = parentvp->sourceSSpace();
    while (true) {
      if ( curspace == nullptr )
        break;
      if ( (curspace == SourceImage) && 
           (QString(SourceImage->globalSpaceID().c_str()) == SourceImageID) ) {
        found = true;
        break;
      }
      sspacestack.push_back(curspace);
      SSpace *newspace = &(curspace->getSourceSSpace());
      if ( curspace == newspace )
        break;
      else
        curspace = newspace;
    }
    if (found) {
      // project the point on the source image back through all of the SSpaces
      SCoordinate finalpoint;
      bool ismapped = true;
      finalpoint.clamp_round(Point);
      for(int s=(sspacestack.size()-1); s>=0; s--) {
        SCoordinate propcoord;
        ismapped = sspacestack[s]->fromSourceCoords(finalpoint, propcoord);
        if (ismapped) {
          finalpoint = propcoord;
        }
        else
          break;
      }
      // if the coordinates are present on the surface
      if (ismapped) {
        // determine where on the VP they are located
        finalpoint.setDim(2);
        SVector finalpointf = parentvp->sourceSSpaceToCoord(finalpoint);
        finalpoint.clamp_round(finalpointf);
        move(finalpoint.x()-(width()/2), finalpoint.y()-(height()/2));
        Label->move(x()+width()-12,y());
        show();
        Label->show();
              // write any distance
        writeDistance(*this,Point,0.0);
      }
      else { // not present on surface SSpace
        hide();
        Label->hide();
      }
      drawLink(); // update the link line
    }
    else { // SSpace not found
      // source image no longer present here
      deleteLater();
    }
  }
}

//************************* SLineDrawWidget Tool *******************************

SLineDrawWidget::SLineDrawWidget(QWidget* parent, bool fast): QWidget(parent),
                                     TB(true),Thickness(1), Color(255,255,255) {
  setAttribute(Qt::WA_TransparentForMouseEvents);
  setStyleSheet("background-color: rgba(0, 0, 0, 50%);");
  if (!fast) {
    QGraphicsDropShadowEffect *newshadow = new QGraphicsDropShadowEffect(this);
    newshadow->setColor(QColor(0,0,0));
    newshadow->setBlurRadius(5);
    newshadow->setOffset(0,0);
    setGraphicsEffect(newshadow);
  }
  SViewPort *testvp = dynamic_cast<SViewPort*>(parentWidget());
  if (testvp != nullptr) {
    connect(testvp,SIGNAL(updated(bool)),this,SLOT(update()));
  }
}

SLineDrawWidget::~SLineDrawWidget() {

}

void SLineDrawWidget::resizeEvent(QResizeEvent* rev) {
  QWidget::resizeEvent(rev);
  if (width() < Thickness)
    resize(Thickness,height());
  if (height() < Thickness)
    resize(width(),Thickness);
}

void SLineDrawWidget::paintEvent(QPaintEvent* pev) {
  QWidget::paintEvent(pev);
  // draw the line
  QPainter vppaint(this);
  vppaint.setPen(QPen(Color,
                      Thickness,
                      Qt::SolidLine,
                      Qt::RoundCap,
                      Qt::RoundJoin));
  int drawwidth  = width()-1;
  int drawheight = height()-1;
  if (TB) {
    vppaint.drawLine(0,0,drawwidth,drawheight);
  }
  else {
    vppaint.drawLine(0,drawheight,drawwidth,0);
  }
}

void SLineDrawWidget::setBT() {
  TB = false;
}

void SLineDrawWidget::setTB() {
  TB = true;
}

void SLineDrawWidget::setThickness(int nthick) {
  Thickness = nthick;
}

void SLineDrawWidget::setColor(QColor ncol) {
  Color = ncol;
}

void SLineDrawWidget::setLine(QPoint p1, QPoint p2) {
  QPoint TLC, BRC;
  // determine positons
  if (p1.x() < p2.x()) {
    TLC.setX(p1.x());
    BRC.setX(p2.x());
  }
  else {
    TLC.setX(p2.x());
    BRC.setX(p1.x());
  }
  if (p1.y() < p2.y()) {
    TLC.setY(p1.y());
    BRC.setY(p2.y());
  }
  else {
    TLC.setY(p2.y());
    BRC.setY(p1.y());
  }
  // configure line object position
  setGeometry(TLC.x(),TLC.y(),(BRC.x()-TLC.x())+1,(BRC.y()-TLC.y())+1);
  // configure the line orientation
  if (p1.x() < p2.x()) {
    if (p1.y() < p2.y())
      setTB();
    else
      setBT();
  }
  else {
    if (p1.y() > p2.y())
      setTB();
    else
      setBT();
  }
}

void SLineDrawWidget::cleanup(bool deep) {
  if (deep)
    deleteLater();
}

//************************** SMarkMeasure Tool *********************************

SMarkMeasure::SMarkMeasure(QObject* parent): SViewPortTool(parent), Counter(0),
                                                                 LastPOI(nullptr) {
  setToolTip(tr("Place a marker (right-click) or a measurer (left-click) or a \
polyline marker/measurer (right-then-middle click)"));
  setIcon(QIcon(":resources/measure-icon"));
  setText(tr("Mark/Measure"));
}

SMarkMeasure::~SMarkMeasure() {

}

void SMarkMeasure::selected(SViewPort&) {
  setIcon(QIcon(":resources/measure-selected-icon"));
}

void SMarkMeasure::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/measure-icon"));
}

void SMarkMeasure::leftClicked(SViewPort& targ, int x, int y) {
  SPOIObject *rbegin = new SPOIObject(&targ,x,y,"<b><font size=\"-1\" color=\""
                                              + targ.activeColor().name() +"\">"
                                              + QString(tr("measure "))
                                              + QString::number(Counter++) +
                                                 "</font></b>");
  SPOIObject *rend = new SPOIObject(&targ,x+(targ.width()/4),y,QString(" "));
  connect(rbegin,SIGNAL(destroyed(QObject*)),
          rend,SLOT(deleteLater()));
  connect(rend,SIGNAL(destroyed(QObject*)),
          rbegin,SLOT(deleteLater()));
  rbegin->link(*rend);
  rbegin->setWriteDistance(true);
  emit triggered(false);
}

void SMarkMeasure::middleClicked(SViewPort& targ, int x, int y) {
  if (LastPOI != nullptr) {
    SPOIObject *tmppoi = LastPOI;
    LastPOI = new SPOIObject(&targ,x,y,"");
    connect(LastPOI,SIGNAL(destroyed(QObject*)),
          this,SLOT(clearLastPOI(QObject*)));
    tmppoi->link(*LastPOI);
  }
}

void SMarkMeasure::rightClicked(SViewPort& targ, int x, int y) {
  LastPOI = new SPOIObject(&targ,x,y,"<b><font size=\"-1\" color=\""
                                              + targ.activeColor().name() +"\">"
                                              + QString("mark ")
                                              + QString::number(Counter++) +
                                                 "</font></b>");
  LastPOI->setWriteDistance(true);
  connect(LastPOI,SIGNAL(destroyed(QObject*)),
          this,SLOT(clearLastPOI(QObject*)));
}

void SMarkMeasure::clearLastPOI(QObject* rem) {
  if (rem == LastPOI)
    LastPOI = nullptr;
}

//**************************** SVPClose Tool ***********************************

SVPCloseTool::SVPCloseTool(QObject* parent): SViewPortTool(parent) {
  setToolTip(tr("Close a ViewPort"));
  setIcon(QIcon(":resources/closeflat-icon"));
  setText(tr("Close VP"));
}

SVPCloseTool::~SVPCloseTool() {

}

void SVPCloseTool::selected(SViewPort& ) {
  setIcon(QIcon(":resources/closeflat-selected-icon"));
}

void SVPCloseTool::deSelected(SViewPort& ) {
  setIcon(QIcon(":resources/closeflat-icon"));
}

void SVPCloseTool::leftClicked(SViewPort& vp, int , int ) {
  SDynamicLayout *parlayout = dynamic_cast<SDynamicLayout*>(vp.parent());
  if (parlayout != nullptr)
    parlayout->useWidget(nullptr);
}

//*************************** SReinterPret Tool ********************************

SReinterpret::SReinterpret(QObject* parent): SViewPortTool(parent) {
  Toolbox = new QWidget(parentWidget());
  Toolbox->hide();
  QVBoxLayout *layout = new QVBoxLayout(Toolbox);
  Toolbox->setLayout(layout);
  layout->addWidget(new QLabel(tr("Left-click applies new extent, right-click \
fetches current extent"),Toolbox));
  ExtentBox = new QLineEdit(Toolbox);
  layout->addWidget(ExtentBox);
  setToolTip(tr("Reinterpret SSpace Extent"));
  setIcon(QIcon(":resources/extent-icon"));
  setText(tr("Reinterpret Extent"));
  Message = new QLabel(Toolbox);
  layout->addWidget(Message);
}

SReinterpret::~SReinterpret() {

}

void SReinterpret::selected(SViewPort& vp) {
  setIcon(QIcon(":resources/extent-selected-icon"));
  AdditionalWidget(Toolbox,"Reinterpret SSpace Extent");
  Message->setText("Ready");
  rightClicked(vp,0,0);
}

void SReinterpret::deSelected(SViewPort&) {
  setIcon(QIcon(":resources/extent-icon"));
  AdditionalWidgetHide(Toolbox);
}

void SReinterpret::leftClicked(SViewPort& vp, int, int) {
  SSpace *endspace = vp.endSourceSSpace();
  bool    worked = false;
  if (endspace != nullptr && endspace->try_lock()) {
    QStringList components = ExtentBox->text().split
                              ("x",QString::KeepEmptyParts,Qt::CaseInsensitive);
    SCoordinate newext(components.size());
    for (int c=0; c<components.size(); c++) {
      newext[c] = components[c].toInt();
    }
    ExtentBox->setText(newext.toString().c_str());
    if (endspace->reinterpretExtent(newext)) {
      Message->setText(QString("Extent reinterpreted: ") +
                       endspace->extent().toString().c_str());
      worked = true;
    }
    else {
      Message->setText(
                     "<font color=\"red\">Extent volumes incompatible!</font>");
    }
    endspace->unlock();
    if (worked) {
      endspace->emitRefresh(true);
    }
  }
}

void SReinterpret::rightClicked(SViewPort& vp, int, int) {
  SSpace *endspace = vp.endSourceSSpace();
  if ((endspace != nullptr) && endspace->try_lock()) {
    ExtentBox->setText(endspace->extent().toString().c_str());
    endspace->unlock();
  }
}

//***************************** SFileDialog ************************************

#ifdef MOBILE
SFileDialog::SFileDialog(QWidget* parent, bool showfile, Qt::WindowFlags wf):
                                                        QFileDialog(parent,wf) {
  setViewMode(QFileDialog::Detail);
  setStyleSheet(
    "QScrollBar:vertical { width: " + QString::number(goodIconSize()/2.5)  + 
    "px; } QScrollBar:horizontal { height: " + 
     QString::number(goodIconSize()/2.5)+ "px; }");
  QObjectList childobjs = children();
  unsigned int toolc = 0;
  for (int i=0; i < childobjs.size(); i++) {
    QWidget *childwidg = dynamic_cast<QWidget*>(childobjs[i]);
    if (childwidg != nullptr) {
      childwidg->setInputMethodHints(Qt::ImhNoAutoUppercase|
                                     Qt::ImhNoPredictiveText);
    }
    QToolButton *button = dynamic_cast<QToolButton*>(childobjs[i]);
    if (button != nullptr) {
      if (toolc != 2) { // hide other buttons
        button->hide();
      }
      else { // fixup the back button
        button->setIconSize(QSize(goodIconSize()/1.6,goodIconSize()/1.6));
        button->setStyleSheet("QToolButton { border: 1px solid #353535; \
background-color: rgba(0, 0, 0, 40%); color: #e5e5e5; border-radius: 5px; \
min-width: " + QString::number(goodIconSize()/1.5)  + "px; min-height: " + 
QString::number(goodIconSize()/1.5)  + "px; }");
        button->adjustSize();
      }
      toolc++;
    }
    else {
      QSplitter *splitter = dynamic_cast<QSplitter*>(childobjs[i]);
      if (splitter != nullptr) { // hide the side pane
        QWidget *tohide =  splitter->widget(0);
        if (tohide != nullptr)
          tohide->hide();
      }
      else {
        QLineEdit *ledit = dynamic_cast<QLineEdit*>(childobjs[i]);
        if (ledit != nullptr) {
          if (showfile)
            ledit->setPlaceholderText("Filename");
          else
            ledit->hide();
        }
        else {
          QLabel *label = dynamic_cast<QLabel*>(childobjs[i]);
          if (label != nullptr) {
            label->hide();
          }
        }
      }
    }
  }
}
#else
SFileDialog::SFileDialog(QWidget* parent, bool, Qt::WindowFlags wf):
                                                        QFileDialog(parent,wf){}
#endif

SFileDialog::~SFileDialog() {

}

void SFileDialog::setToScreenSize() {
  QRect screensize = QGuiApplication::primaryScreen()->availableGeometry();
  setFixedSize(screensize.width(),screensize.height());
}

void SFileDialog::showEvent(QShowEvent* event) {
  QFileDialog::showEvent(event);
#ifdef MOBILE
  setToScreenSize();
#endif
}

void SFileDialog::resizeEvent(QResizeEvent* event) {
  QFileDialog::resizeEvent(event);
#ifdef MOBILE
  setToScreenSize();
#endif
}
