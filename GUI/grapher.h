/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:Grapher
 * A simple graphing drawer and widget
 * Author: M.A.Hicks
 */
#ifndef SGRAPHER
#define SGRAPHER

#include <QWidget>
#include <GUI/definitions.h>

namespace Simulacrum {

  typedef std::vector< std::pair<float, float> > SDataSet;
  enum SGrapherMode_t {SHistogram, SLineGraph, SScatterGraph};
  class SVector;
  class SGrapher: public QWidget {
  private:
    std::vector<SDataSet>
                   DataSets;
    std::vector<SGrapherMode_t>
                   DataSetModes;
    std::vector<QColor>
                   DataSetColours;
    QColor         BGColour;
    QColor         LabelColour;
    bool           Labels;
    int            PointSize;
    int            LineThickness;
    int            SigFigs;
    float          XScale, YScale, XMin, YMin;
    static const
    int            LabelBorder;
    static const
    int            LabelTextRatio;
    static const
    int            LabelMeterLength;
    int            LabelDensity;
    bool           DrawGrid;
    int            Border;
    QPixmap        GraphSurface;
    QPoint         PlotStart;
    void           drawLineGraph(const SDataSet&, bool withlines, QColor);
    void           drawHistogram(const SDataSet&, QColor);
    void           drawLabels();
    void           drawGridLines();
    void           setScales();
    QPoint         mapPoint(float,float);
    SVector        unmapPoint(int,int);
    int            effectiveBorder();
  public:
    explicit       SGrapher(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual       ~SGrapher();
    void           setShowLabels(bool);
    bool           showLabels();
    void           setLabelColor(QColor);
    int            sigFigs();
    void           setSigFigs(int);
    bool           drawGrid();
    void           setDrawGrid(bool);
    int            addDataSet(const SDataSet&,
                              SGrapherMode_t mode     = SLineGraph,
                              bool           doupdate = true);
    void           removeDataSet(int, bool doupdate = true);
    int            dataSets();
    void           setDataSetColors(const std::vector<QColor>&);
    void           setBGColor(QColor);
    void           clear();
    const QPixmap& pixmap();
    void           drawGraph();
  protected:
    virtual 
    void           paintEvent(QPaintEvent*);
    virtual
    void           resizeEvent(QResizeEvent*);
    virtual
    void           mouseMoveEvent(QMouseEvent*);
  };

}

#endif //SGRAPHER
