/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:
 * A parameterisable viewport matrix
 * Author: M.A.Hicks
 */
#ifndef SLIGHTTABLE
#define SLIGHTTABLE

#include <QListWidget>
#include <QWidget>
#include <QSplitter>

#include <GUI/definitions.h>
#include <GUI/BusyWidget.h>
#include <Core/SPool/SPool.h>
#include "viewPort.h"

class QHBoxLayout;
class QVBoxLayout;
class QStatusBar;
class QMenuBar;
class QInputDialog;

namespace Simulacrum {

  class SSpaceItem;
  class SIMU_GUI_API SSpaceCollection: public QListWidget {
    Q_OBJECT
  public:
             SSpaceCollection(QWidget* parent);
    virtual ~SSpaceCollection();
    QList<SSpaceItem*>
             selectedSSpaces();
    QList<SSpaceItem*>
             sspaces();
    virtual
    void     resizeEvent (QResizeEvent*);
  protected:
    SPool    LoadingPool;
    void     resourceLoader(SResource*, const std::string);
    QMimeData*
             mimeData(const QList<QListWidgetItem*> items) const;
    QStringList
             mimeTypes() const;
    void     dragEnterEvent ( QDragEnterEvent *);
    void     dropEvent      ( QDropEvent  * );
    void     duplicateBG(SSpace* source);
  signals:
    void     __newSSpace(SSpace*);
    void     newSSpace(SSpace&);
    void     newLayout(QString title,QString layoutxml);
    void     loading(bool);
    void     watchSSpace(SConnectable*);
    void     stopWatchSSpace(SConnectable*);
    void     message(QString,int);
  public slots:
    void     addSSpace(SSpace*);
    void     loadFromResource(SResource&,const std::string);
    void     reset(bool forceful = true);
    void     removeSelected();
    void     duplicateSelected();
    void     clear();
    void     clearUnconnected();
    void     standardAction(const QModelIndex &index);
  };

  class SIMU_GUI_API SSpaceItem: public QObject, public QListWidgetItem, 
                                                          public SQConnectable {
    Q_OBJECT
  private:
    SSpace  *SourceSpace;
  public:
    static
    unsigned
    short    IconRes();
             SSpaceItem    (SSpace*,SSpaceCollection* =nullptr);
    virtual ~SSpaceItem    ();
    void     setSourceSpace(SSpace*);
    SSpace*  sourceSpace   ();
    static
    QIcon    genQIconfromSSpace(SSpace&);
  public slots:
    void     refresh(bool);
    void     setProgress(int);
    void     sdisconnect(SConnectable*);
    bool     safeDelete();
    void     setWaiting(bool);
  signals:
    void     active(bool);
    void     progress(int);
    void     updated(bool);
  };

  class SIMU_GUI_API SCollapsibleToolBox: public QFrame {
    Q_OBJECT
  private:
    QVBoxLayout *MainLayout;
    QPushButton *MainLabel;
  protected:
    virtual
    void     childEvent(QChildEvent*);
  public:
             SCollapsibleToolBox(QWidget* parent = 0);
             SCollapsibleToolBox(QWidget* child, QString title,
                                 QWidget* parent = 0);
    virtual ~SCollapsibleToolBox();
  public slots:
    void     setTitle(const QString&);
    void     addWidget(QWidget*);
    void     collapse();
    void     expand();
    void     toggle();
  };

  class SIMU_GUI_API SDynamicLayout: public QSplitter {
    Q_OBJECT
  private:
    QWidget *MainWidget;
    QWidget *Selector;
    SViewPortEventHandler
            *EventHandler;
    SDynamicLayout*
             ParentLayout;
  public:
             SDynamicLayout (QWidget* parent = 0);
    virtual ~SDynamicLayout ();
    bool     isWidget       ();
    QWidget& mainWidget     ();
    QList<SDynamicLayout*>
             children       ();
    void     removeChild    (SDynamicLayout*);
    QString  toXML          ();
    void     toNode         (SAbsTreeNode&);
    QList<SViewPort*>
             viewports         (bool recurse=true);
  public slots:
    void     fromXML        (QString);
    void     fromNode       (SAbsTreeNode&);
    void     setEventHandler(SViewPortEventHandler*);
    void     split          (Qt::Orientation,
                             int stretch1 = 1, int stretch2 = 1, int sbd = 1);
    void     splitv         (int stretch1 = 1, int stretch2 = 1, int sbd = 1);
    void     splith         (int stretch1 = 1, int stretch2 = 1, int sbd = 1);
    void     useWidget      (QWidget*);
    void     makeVP         (QString predilection = "Custom",
                             QString vpconf = "", QString sspaceconf = "");
    void     makeSlicerVP   (QString predilection = "Custom",
                             QString vpconf = "", QString sspaceconf = "");
    void     doDestroy      ();
    void     clear          ();
  };

  class SIMU_GUI_API SLayoutStringAction: public QAction {
    Q_OBJECT
  private:
    QString ActionString;
  public:
    SLayoutStringAction(const QString& text, QObject* parent);
    void setStringEmission(const QString&);
  private slots:
    void doEmission();
  signals:
    void triggerString(QString);
    void triggerString(QString,QString);
  };

  class SIMU_GUI_API SLightTable: public QWidget {
    Q_OBJECT
  private:
    QSplitter   *mainLayout;
    QSplitter   *panelLayout;
    QTabWidget  *DynLayout;
    QVBoxLayout *WidgetStack;
    QStatusBar  *StatusBar;
    QMenuBar    *MainMenu;
    SSpaceCollection
                *Collection;
    SViewPortEventHandler
                *VPTools;
    QMenu       *LayoutMenu;
    QMenu       *CollectionMenu;
    QMenu       *CustomLayoutMenu;
    QMenu       *VPToolsMenu;
    std::vector<SViewPortTool*>
                 PluginTools;
    BusyWidget  *GeneralProgress;
    QList<QAction*>
                 Layouts;
    QInputDialog *StoreName;
  protected:
    void         resizeEvent(QResizeEvent*);
  public:
                 SLightTable(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual     ~SLightTable();
    SSpaceCollection&
                 collection();
    SDynamicLayout*
                 activeLayout();
    BusyWidget&
                 generalProgress();
    static
    std::string  layoutsPath();
  public slots:
    void         populateTools();
    void         showMessage(const QString);
    void         addPanelWidget(QWidget*, const QString& title);
    void         addLayout(const QString title = "Custom Layout",
                           const QString layoutxml = "");
    void         closeActiveLayout();
    void         closeLayout(int);
    void         reset();
    void         showCollectionMenu();
    void         showLayoutMenu();
    void         addToolWidget(QWidget*);
    void         newSSpaceAvailable(SSpace&);
    void         reloadPlugins();
    void         reloadLayouts();
    void         storeActiveLayout(QString name);
    void         loadPlugins();
    void         clearPlugins();
    void         showLayoutXML();
    void         showMenu(bool);
    void         mobileDevice();
  };

}

#endif //SLIGHTTABLE
