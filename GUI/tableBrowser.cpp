/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * TableBrowser
 * Author: M.A.Hicks
 */

#include "tableBrowser.h"
#include <QHeaderView>
#include <QScrollBar>
#include <iostream>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSet>
#include <QList>
#include <QLineEdit>
#include <QFormLayout>
#include <QClipboard>
#include <QApplication>
#include <QScreen>
#include <QWidget>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QSplitter>
#include <QLabel>
#include <QPushButton>

#include <Resources/DICOM/sarch.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/sysInfo/sysInfo.h>

using namespace Simulacrum;

#ifdef MOBILE
static unsigned goodIconSize() {
#if QT_VERSION >= 0x050000
  return (unsigned)(QGuiApplication::primaryScreen()->logicalDotsPerInch()/1.3);
#else
  return 100;
#endif
}
#endif

//-------------------------- DCMDictTool ---------------------------------------

DICOMDictTool::DICOMDictTool(QWidget* parent, Qt::WindowFlags f):
                                                            QWidget(parent, f) {
  setWindowFlags(Qt::Tool);
  setWindowTitle(tr("DICOM Tag"));
  TagID                    = new QLineEdit(this);
  TagVR                    = new QLineEdit(this);
  TagName                  = new QLineEdit(this);
  QFormLayout  *formLayout = new QFormLayout(this);
  acceptButton             = new QPushButton(this);
  acceptButton->setText(tr("Accept"));
  formLayout->addRow("&ID: ", TagID);
  formLayout->addRow("&VR: ", TagVR);
  formLayout->addRow("&Name: ", TagName);
  formLayout->addRow(acceptButton);
  TagID->setPlaceholderText(tr("Start lookup..."));
  TagName->setPlaceholderText(tr("Start lookup..."));
  setLayout(formLayout);
  connect(TagID,SIGNAL(textEdited(QString)),
          this,SLOT(updateBasedonID(QString)));
  connect(TagID,SIGNAL(textChanged(QString)),
          this,SIGNAL(contentsChanged()));
  connect(TagName,SIGNAL(textEdited(QString)),
          this,SLOT(updateBasedonName(QString)));
  connect(TagName,SIGNAL(textChanged(QString)),
          this,SIGNAL(contentsChanged()));
  connect(TagVR,SIGNAL(textChanged(QString)),
          this,SIGNAL(contentsChanged()));
  connect(acceptButton,SIGNAL(clicked(bool)),this,SLOT(done()));
  showAcceptButton(false);
  TagID->setInputMethodHints(Qt::ImhNoAutoUppercase|
                                   Qt::ImhNoPredictiveText);
  TagVR->setInputMethodHints(Qt::ImhNoAutoUppercase|
                                   Qt::ImhNoPredictiveText);
  TagName->setInputMethodHints(Qt::ImhNoAutoUppercase|
                                   Qt::ImhNoPredictiveText);
}

DICOMDictTool::~DICOMDictTool() {

}

QSize DICOMDictTool::minimumSizeHint() const {
  return QSize(400,this->height());
}

void DICOMDictTool::configure(const dcm_info_store& newentry) {
  DCMTag tmptag;
  tmptag.setID(newentry.ID);
  TagID  ->setText(tmptag.NodeID().c_str());
  QString VRStr;
  VRStr.resize(2);
  VRStr[0] = newentry.VR[0];
  VRStr[1] = newentry.VR[1];
  TagVR  ->setText(VRStr);
  TagName->setText(newentry.Name);
}

void DICOMDictTool::updateBasedonID(QString newid) {
  QStringList ids = newid.split(",");
  bool res = false;
  if (ids.length() == 2) {
    bool stat1 = false;
    bool stat2 = false;
    uint hex1  = ids[0].toUInt(&stat1,16);
    uint hex2  = ids[1].toUInt(&stat2,16);
    if (stat1 && stat2) {
      if(DCMDictionary.contains(hex1,hex2)) {
        configure(DCMDictionary.getEntry(hex1,hex2));
        res = true;
      }
    }
  }
  if (!res) {
    TagVR->clear();
    TagName->clear();
  }
}

void DICOMDictTool::updateBasedonName(QString newname) {
  if(DCMDictionary.contains(newname.toStdString(),false)) {
    configure(DCMDictionary.getEntry(newname.toStdString(),false));
  }
  else {
    TagVR->clear();
    TagID->clear();
  }
}

void DICOMDictTool::done() {
  emit selectedID(TagID->text());
}

QString DICOMDictTool::ID() {
  return TagID->text();
}

QString DICOMDictTool::VR() {
  return TagVR->text();
}

QString DICOMDictTool::Name() {
  return TagName->text();
}

void DICOMDictTool::fromTag(DCMTag& newtag) {
  TagID->setText(newtag.NodeID().c_str());
  TagVR->setText(newtag.NodeType().c_str());
  TagName->setText(newtag.NodeName().c_str());
}

DCMTag DICOMDictTool::toTag() {
  DCMTag retval;
  retval.NodeID(TagID->text().toStdString());
  retval.NodeType(TagVR->text().toStdString());
  return retval;
}

void DICOMDictTool::showAcceptButton(bool doshow) {
  acceptButton->setVisible(doshow);
}

/*******************************************************************************
 *
 *                             STableBrowser
 *
 ******************************************************************************/

SAbsTreeTableItem::SAbsTreeTableItem(SAbsTreeNode& newbase):
                                       QTableWidgetItem(UserType+1),
                                       BaseNode(newbase) {
  configure();
  refresh();
}

SAbsTreeTableItem::SAbsTreeTableItem(SAbsTreeNode& newbase,
                                     const QString& attribute):
                                       QTableWidgetItem(UserType+2),
                                       BaseNode(newbase),
                                       AttributeName(attribute) {
  configure();
  refresh();
}

SAbsTreeNode& SAbsTreeTableItem::getNode() {
  return BaseNode;
}

void SAbsTreeTableItem::configure() {

}

void SAbsTreeTableItem::refresh() {
  if (AttributeName.size() > 0 && (AttributeName != "SNodeKey")) {
    setText(SimulacrumGUILibrary::toQString(BaseNode.stringEncoding(),
                          BaseNode.getAttribute(AttributeName.toStdString())));
  }
  else
    setText(SimulacrumGUILibrary::toQString(BaseNode.stringEncoding(),
                                           BaseNode.NodeValue()));
}

STableBrowser::STableBrowser(QWidget* parent): QTableWidget(parent),
                                                               BusyMaker(this) {
  setAutoSelect(true);
  setShowGrid(false);
  setSortingEnabled(true);
  verticalHeader()->hide();
  setAlternatingRowColors(true);
  setCornerButtonEnabled(true);
  setWordWrap(false);
  clear(true);
#if QT_VERSION >= 0x050000
  horizontalHeader()->setSectionsMovable(true);
  setProperty("makeBorder",true);
#else
  horizontalHeader()->setMovable(true);
#endif
  connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(recalculateOutput()));
  connect(this,SIGNAL(_newTableData(QList< QList< SAbsTreeTableItem* > >*)),
          this,SLOT(__newTableData(QList< QList< SAbsTreeTableItem* > >*)));
#ifdef MOBILE
  unsigned int goodsize = goodIconSize();
  setStyleSheet(
    "QScrollBar:vertical { width: " + QString::number(goodsize/2.5)  + "px; }");
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  QFont tmpfont = font();
  tmpfont.setPointSize((int)((float)tmpfont.pointSize()*0.8));
  setFont(tmpfont);
#endif
}

STableBrowser::~STableBrowser() {

}

void STableBrowser::setParentNodes(QList< SAbsTreeNode* > newparentnodes) {
  QList<SAbsTreeNode*> newlist;
  for (int i=0; i < newparentnodes.size(); i++) {
    for (unsigned j = 0; j < newparentnodes[i]->NodeChildrenNum(true); j++)
      newlist.append(newparentnodes[i]->NodeChildren(true)[j]);
  }
  setBaseNodes(newlist);
}

void STableBrowser::setBaseNodes(QList<SAbsTreeNode*> newnodes) {
  NodesLock.lock();
  BaseNodes = newnodes;
  NodesLock.unlock();
  refresh();
}

QList< SAbsTreeNode* > STableBrowser::getSelectedNodes() {
  NodesLock.lock();
  QList<QTableWidgetItem *> newselection = selectedItems();
  QSet<SAbsTreeNode*>       tmpoutput;
  QList<SAbsTreeNode*>      output;
  for (int i=0; i<newselection.length(); i++)
    tmpoutput.insert(&(dynamic_cast<SAbsTreeTableItem*>
                                                 (newselection[i])->getNode()));
  QSetIterator<SAbsTreeNode *> i(tmpoutput);
  while (i.hasNext())
    output.append(i.next());
  NodesLock.unlock();
  return output;
}

void STableBrowser::addAttribute(QString newattribute) {
  NodesLock.lock();
  AttributeList.append(newattribute);
  setColumnCount(columnCount()+1);
  for (int i = 0; i < rowCount(); i++)
    setItem(i,columnCount()-1,genItemAt(i,columnCount()-1));
  setColumnHeaders();
  NodesLock.unlock();
  setRecommendedLayout();
}

void STableBrowser::removeAttribute(QString oldattribute) {
  int removed = -1;
  NodesLock.lock();
  for (int i = 0; i < AttributeList.size(); i++)
    if (AttributeList[i] == oldattribute) {
      AttributeList.removeAt(i);
      removed = i;
    }
  NodesLock.unlock();
  if (removed >= 0) {
    removeColumn(removed);
    setRecommendedLayout();
  }
}

void STableBrowser::removeAttribute(int column) {
  if (column >= 0 && column < AttributeList.size())
    removeAttribute(AttributeList[column]);
}

void STableBrowser::addNode(SAbsTreeNode& rownode) {
  NodesLock.lock();
  BaseNodes.append(&rownode);
  setRowCount(rowCount()+1);
  for (int i = 0; i < columnCount(); i++)
    setItem(rowCount()-1,i,genItemAt(rowCount()-1,i));
  resizeRowToContents(rowCount()-1);
  NodesLock.unlock();
}

SAbsTreeTableItem* STableBrowser::genItemAt(int row, int column) {
  SAbsTreeTableItem* newitem =
                   new SAbsTreeTableItem(*BaseNodes[row],AttributeList[column]);
  newitem->setTextAlignment(Qt::AlignCenter);
  if (EditMode)
    newitem->setFlags(newitem->flags() | Qt::ItemIsEditable);
  else {
    if ( newitem->flags() | Qt::ItemIsEditable )
      newitem->setFlags(newitem->flags() ^ Qt::ItemIsEditable);
  }
  return newitem;
}

QString STableBrowser::getFriendlyString(const std::string& keystr) {
  if (keystr.find(",") != std::string::npos) {
    // this might be a DICOM tag, in hex
    DCMTag convtag;
    convtag.setID(keystr);
    if (DataDictionary->contains(convtag.getID()))
      return std::string(DataDictionary->
                                        getEntry(convtag.getID()).Name).c_str();
    else
      return keystr.c_str();
  }
  else
    return keystr.c_str();
}

void STableBrowser::setColumnHeaders() {
  for (int c = 0; c < columnCount(); c++) {
    if (AttributeList.size() > c) {
      if (DoFriendlyNames)
        setHorizontalHeaderItem(c, new QTableWidgetItem(
                            getFriendlyString(AttributeList[c].toStdString())));
      else
        setHorizontalHeaderItem(c, new QTableWidgetItem(AttributeList[c]));
    }
  }
}

void STableBrowser::resizeEvent(QResizeEvent* event) {
  QTableWidget::resizeEvent(event);
  setRecommendedLayout();
}

void STableBrowser::genItems() {
  QList< QList< SAbsTreeTableItem* > >* newdat = new
                                           QList< QList< SAbsTreeTableItem* > >;
  NodesLock.lock();
  newdat->reserve(BaseNodes.size());
  for (int r = 0; r < BaseNodes.size(); r++) {
    QList< SAbsTreeTableItem * > irow;
    irow.reserve(AttributeList.size());
    for (int c = 0; c < AttributeList.size(); c++)
      irow.push_back(genItemAt(r,c));
    newdat->push_back(irow);
  }
  NodesLock.unlock();
  emit _newTableData(newdat);
}

void STableBrowser::__newTableData(QList< QList< SAbsTreeTableItem* > >*newdat){
  disconnect(this,SIGNAL(itemSelectionChanged()),
             this,SLOT(recalculateOutput()));
  clear(false);
  bool sorting = isSortingEnabled();
  setSortingEnabled(false);
  setRowCount(newdat->size());
  if (newdat->size() > 0)
    setColumnCount((*newdat)[0].size());
  for (int r = 0; r < newdat->size(); r++)
    for (int c = 0; c < (*newdat)[r].size(); c++)
      setItem(r,c,(*newdat)[r][c]);
  // perform a 1 item auto-select
  if (rowCount() == 1 && AutoSelect && isVisible()) {
    for (int c = 0; c < columnCount() ; c++)
      item(0,c)->setSelected(true);
  }
  else {
    clearSelection();
  }
  setSortingEnabled(sorting);
  configure();
  BusyMaker.clear();
  delete newdat;
  BusyMaker.clear();
  connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(recalculateOutput()));
  if (FilterStringCache.size() > 0)
    applyFilterString(FilterStringCache);
  // if this table is hidden, all rows should be selected, in order to trigger
  // a desirable behaviour (where a hidden table can be ignored and propagate
  // all nodes to the next table)
  if (!isVisible())
    setSelection(QRect(0,0,columnCount(),rowCount()),
                 QItemSelectionModel::Select|QItemSelectionModel::Columns);
  recalculateOutput();
  emit stateChanged();
}

void STableBrowser::refresh() {
  if (BaseNodes.size() > BusyThreshold) {
    BusyMaker.setBusy(true);
    BusyMaker.oscillateProgress();
  }
  BGJobs.addJob( std::bind(&STableBrowser::genItems,this) );
}

bool STableBrowser::isEditable() {
  return EditMode;
}

void STableBrowser::setEditable(bool doedit) {
  EditMode = doedit;
}

void STableBrowser::setFriendlyNames(bool dofriendly) {
  DoFriendlyNames = dofriendly;
}

void STableBrowser::setDataDictionary(DCMDataDic* newdic) {
  DataDictionary = newdic;
}

bool STableBrowser::applyFilterString(const QString& FilterString, bool force) {
  bool retfound = false;
  if (force || (FilterStringCache != FilterString)) {
    bool madebusy = false;
    if (rowCount() > BusyThreshold) {
      madebusy = true;
      BusyMaker.setBusy(true);
    }
    FilterStringCache = FilterString;
    disconnect(this,SIGNAL(itemSelectionChanged()),
              this,SLOT(recalculateOutput()));
    NodesLock.lock();
    bool sorting = isSortingEnabled();
    setSortingEnabled(false);
    for (int r=0 ; r <rowCount(); r++) {
      bool found = false;
      for (int c=0; c < columnCount(); c++) {
        if ( item(r,c)->text().contains(FilterString) ) {
          found    = true;
          retfound = true;
          break;
        }
      }
      setRowHidden(r,!found);
    }
    setSortingEnabled(sorting);
    NodesLock.unlock();
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(recalculateOutput()));
    recalculateOutput();
    if (madebusy)
      BusyMaker.setBusy(false);
  }
  return retfound;
}

void STableBrowser::copySelectionToClipboard() {
  QClipboard *clipboard = QApplication::clipboard();
  QList<QTableWidgetSelectionRange> selectionranges = selectedRanges();
  QString copystring;
  for (int sel=0; sel < selectionranges.size(); sel++) {
    for (int row=selectionranges[sel].topRow();
         row <= selectionranges[sel].bottomRow(); row++) {
      for (int column=selectionranges[sel].leftColumn();
         column <= selectionranges[sel].rightColumn(); column++) {
        copystring += item(row,column)->text();
        if (column != selectionranges[sel].rightColumn())
          copystring += ",";
      }
      copystring += QString("\n");
    }
  }
  copystring.replace('\0',' ');
  if (copystring.size() > 0)
    clipboard->setText(copystring);
}

void STableBrowser::configure() {
  if (EditMode)
    setSelectionBehavior(QAbstractItemView::SelectItems);
  else
    setSelectionBehavior(QAbstractItemView::SelectRows);
  resizeRowsToContents();
  resizeColumnsToContents();
  setRecommendedLayout();
  setColumnHeaders();
}

void STableBrowser::setRecommendedLayout() {
  if (columnCount() > 0) {
    int acolwidth = width();
    if (verticalScrollBar()->isVisible())
      acolwidth -= verticalScrollBar()->width();
    acolwidth = (acolwidth / columnCount()) - 5;
    for (int c = 0; c < columnCount(); c++)
      setColumnWidth(c, acolwidth);
  }
}

void STableBrowser::setAutoSelect(bool newsel) {
  AutoSelect = newsel;
}

bool STableBrowser::busy() {
  return (BGJobs.jobs() + BGJobs.activeThreads()) > 0;
}

void STableBrowser::wait() {
  BGJobs.wait();
}

void STableBrowser::clear(bool clearsource) {
  QTableWidget::clear();
  if (clearsource) {
    FilterStringCache.clear();
    NodesLock.lock();
    setFriendlyNames(true);
    setEditable(false);
    AttributeList.clear();
    BaseNodes.clear();
    DataDictionary = &InternalDataDictionary;
    NodesLock.unlock();
  }
}

void STableBrowser::recalculateOutput() {
  emit newSelectedNodes(getSelectedNodes());
}

/*******************************************************************************
 *
 *                          STableBrowserLabelled
 *
 ******************************************************************************/

STableBrowserLabelled::STableBrowserLabelled
       (QWidget* parent, Qt::WindowFlags f): QWidget(parent, f), OrigHeight(0) {
  MainLayout             = new QVBoxLayout(this);
  MainBrowser            = new STableBrowser(this);
  QWidget *hlay          = new QWidget(this);
  MainLabel              = new QLabel(hlay);
  QPushButton* minbutton = new QPushButton(QIcon(":resources/collapse-icon"),"",
                                           hlay);
  QHBoxLayout *hlayout   = new QHBoxLayout(hlay);
  hlayout->setContentsMargins(0,2,0,0);
  hlayout->addWidget(MainLabel);
  hlayout->addWidget(minbutton);
  hlay->setLayout(hlayout);
  minbutton->setToolTip("Collapse/Expand");
  MainLayout->setContentsMargins(0,0,0,0);
  MainLayout->addWidget(hlay);
  MainLayout->addWidget(MainBrowser);
  MainLabel->hide();
  setLayout(MainLayout);
  minbutton->setMaximumWidth(minbutton->height()-12);
  if (sysInfo::systemType() == "mobile")
    minbutton->hide();
  connect(MainBrowser,SIGNAL(stateChanged()),this,SLOT(browserUpdated()));
  connect(minbutton,SIGNAL(clicked(bool)),this,SLOT(toggleVisible()));
  connect(MainLabel,SIGNAL(linkActivated(const QString&)),
          this,SLOT(toggleVisible()));
}

STableBrowserLabelled::~STableBrowserLabelled() {

}

STableBrowser& STableBrowserLabelled::browser() {
  return *MainBrowser;
}

QString STableBrowserLabelled::infoString() {
  return QString::number(browser().rowCount());
}

void STableBrowserLabelled::setLabel(const QString& newtext) {
  MainLabelText = newtext;
  QString newsectiontitle = "<center>" + newtext;
  newsectiontitle += " (" + infoString() + ")"
                  +  "</center>";
  if (sysInfo::systemType() == "mobile") {
    MainLabel->setText("<a href=\"toggle\" style=\"color: black;\">" +
                       newsectiontitle + "</a>");
  }
  else {
    MainLabel->setText(newsectiontitle);
  }
  MainLabel->show();
}

void STableBrowserLabelled::toggleVisible() {
  MainBrowser->setVisible(!MainBrowser->isVisible());
  if (!MainBrowser->isVisible()) {
    OrigHeight = height();
    setMaximumHeight(MainLabel->height()+5);
  }
  else {
    setMaximumHeight(QWIDGETSIZE_MAX);
    resize(width(),OrigHeight);
  }
  MainBrowser->refresh();
}

void STableBrowserLabelled::browserUpdated() {
  setLabel(MainLabelText);
}

/*******************************************************************************
 *
 *                          STableBrowserStack
 *
 ******************************************************************************/

STableBrowserStack::STableBrowserStack
                      (bool vertical, QWidget* parent, Qt::WindowFlags f):
                      QWidget(parent, f), Vertical(vertical), TableStack(nullptr) {
  MainLayout = new QVBoxLayout(this);
  MainLayout->setContentsMargins(0,0,0,0);
  clear();
}

STableBrowserStack::~STableBrowserStack() {

}

void STableBrowserStack::applyFilterString(const QString& newfilter) {
  QString usefilter = newfilter;
  FilterCache = newfilter;
  for (int i = 0 ; i < TableList.length(); i++)
    if (TableList[i]->browser().isVisible())
      if (TableList[i]->browser().applyFilterString(usefilter))
        usefilter = "";
}

void STableBrowserStack::clear() {
  for (int i = 0 ; i < TableList.length(); i++)
    delete TableList[i];
  if (TableStack != nullptr) {
    MainLayout->removeWidget(TableStack);
    delete TableStack;
    TableStack = nullptr;
  }
  TableStack = new QSplitter(this);
  TableStack->setContentsMargins(2,2,2,2);
  if (Vertical)
    TableStack->setOrientation(Qt::Vertical);
  else
    TableStack->setOrientation(Qt::Horizontal);
  MainLayout->addWidget(TableStack);
  TableList.clear();
}

unsigned int STableBrowserStack::getDepth() {
  return TableStack->count();
}

STableBrowser& STableBrowserStack::getTableBrowser(int level) {
  if (level < TableList.size()) {
    return TableList[level]->browser();
  }
  else
    throw std::exception();
}

bool STableBrowserStack::busy() {
  bool res = false;
  for (unsigned i=0; i<getDepth(); i++) {
    if (getTableBrowser(i).busy())
      res = true;
  }
  return res;
}

void STableBrowserStack::wait() {
  for (unsigned i=0; i<getDepth(); i++) {
    getTableBrowser(i).wait();
  }
}

void STableBrowserStack::setDepth(unsigned int newdepth) {
  clear();
  for (int i=0; i < (int)newdepth; i++) {
    STableBrowserLabelled *newtable = new STableBrowserLabelled(this);
    if (i == (int)(newdepth-1))
      newtable->browser().setAutoSelect(false);
    TableList.append(newtable);
    TableStack->addWidget(newtable);
    if (i > 0){
      connect (&(TableList[i-1]->browser()),
               SIGNAL(newSelectedNodes(QList<SAbsTreeNode*>)),
               &(newtable->browser()),
               SLOT(setParentNodes(QList<SAbsTreeNode*>)));
      connect(&(TableList[i-1]->browser()),
              SIGNAL(itemPressed(QTableWidgetItem *)),
              &(newtable->browser()),SLOT(clearSelection ()));
    }
    connect (&(newtable->browser()),
             SIGNAL(itemActivated(QTableWidgetItem*)),
             this,
             SLOT(newSelectionHandler()));
    connect (&(newtable->browser()),
             SIGNAL(itemSelectionChanged()),
             this,
             SIGNAL(selectionChanged()));
  }
}

QList<std::string> STableBrowserStack::getSelectedPathsRaw(bool skiptop) {
  int lowestselectedlevel = 0;
  QList<std::string> retval;
  for (int i = TableList.size()-1; i >= 0; i--) {
    if (TableList[i]->browser().getSelectedNodes().size() > 0) {
      lowestselectedlevel = i;
      break;
    }
  }
  if (TableList.size() > lowestselectedlevel) {
    QList<SAbsTreeNode*> nodes = 
                    TableList[lowestselectedlevel]->browser().getSelectedNodes();
    for (int p = 0; p < nodes.size(); p++) {
      retval.append(nodes[p]->NodePath(skiptop));
    }
  }
  return retval;
}

QList<QString> STableBrowserStack::getSelectedPaths(bool skiptop) {
  int lowestselectedlevel = 0;
  QList<QString> retval;
  for (int i = TableList.size()-1; i >= 0; i--) {
    if (TableList[i]->browser().getSelectedNodes().size() > 0) {
      lowestselectedlevel = i;
      break;
    }
  }
  if (TableList.size() > lowestselectedlevel) {
    QList<SAbsTreeNode*> nodes = 
                    TableList[lowestselectedlevel]->browser().getSelectedNodes();
    for (int p = 0; p < nodes.size(); p++) {
      retval.append(SimulacrumGUILibrary::toQString(nodes[p]->stringEncoding(),
                                         nodes[p]->NodePath(skiptop).c_str()));
    }
  }
  return retval;
}

std::string STableBrowserStack::getSelectedPathRaw(bool skiptop) {
  std::string retval;
  auto paths = getSelectedPathsRaw(skiptop);
  if (paths.size() > 0) {
    retval = paths[0];
  }
  return retval;
}

QString STableBrowserStack::getSelectedPath(bool skiptop) {
  QString retval;
  auto paths = getSelectedPaths(skiptop);
  if (paths.size() > 0) {
    retval = paths[0];
  }
  return retval;
}

void STableBrowserStack::setAttributes
                                      (unsigned level, QStringList attributes) {
  if (level < (unsigned)TableList.size()) {
    for (int i=0; i < attributes.length(); i++)
      TableList[level]->browser().addAttribute(attributes[i]);
  }
}

void STableBrowserStack::setParentNodes(QList<SAbsTreeNode*> newparents) {
  if (TableList.size() > 0) {
    TableList[0]->browser().setParentNodes(newparents);
  }
  if (FilterCache.size() > 0) {
    applyFilterString(FilterCache);
  }
}

void STableBrowserStack::setLabel(unsigned int level, QString label) {
  if (level < (unsigned)TableList.size()) {
    TableList[level]->setLabel(label);
  }
}

void STableBrowserStack::newSelectionHandler() {
  emit newPathSelection(getSelectedPathRaw(true));
}

/*******************************************************************************
 *
 *                          SResourceTableBrowser
 *
 ******************************************************************************/

SResourceTableBrowser::SResourceTableBrowser
                                           (QWidget* parent, Qt::WindowFlags f):
                                  QWidget(parent, f), Depth(0), Resource(nullptr) {
  MainLayout = new QVBoxLayout(this);
  TableStack = new STableBrowserStack(true,this,f);
  connect(TableStack, SIGNAL(newPathSelection(std::string)), 
          this, SLOT(newPathSelection(std::string)));
  MainLayout->setContentsMargins(0,0,0,0);
  MainLayout->addWidget(TableStack);
  setContextMenuPolicy(Qt::CustomContextMenu);
}

SResourceTableBrowser::~SResourceTableBrowser() {
  if (Resource != nullptr)
    Resource->refDecr();
}

STableBrowserStack& SResourceTableBrowser::tableStack() {
  return *TableStack;
}

SResource* SResourceTableBrowser::resource() {
  return Resource;
}

QList<QString> SResourceTableBrowser::getSelectedPaths(bool skiptop) {
  return TableStack->getSelectedPaths(skiptop);
}

QList<std::string> SResourceTableBrowser::getSelectedPathsRaw(bool skiptop) {
  return TableStack->getSelectedPathsRaw(skiptop);
}

QString SResourceTableBrowser::getSelectedPath(bool skiptop) {
  return TableStack->getSelectedPath(skiptop);
}

std::string SResourceTableBrowser::getSelectedPathRaw(bool skiptop) {
  return TableStack->getSelectedPathRaw(skiptop);
}

void SResourceTableBrowser::setDepth(unsigned newdepth) {
  Depth = newdepth; 
}

void SResourceTableBrowser::refresh() {
  if (Resource != nullptr) {
    QList<SAbsTreeNode*> nodelist;
    SAbsTreeNode *root = rootNode();
    if (root != nullptr)
      nodelist.append(rootNode());
    TableStack->setDepth(Depth);
    TableStack->setParentNodes(nodelist);
  }
}

void SResourceTableBrowser::setResource(SResource& newres) {
  if (Resource != nullptr) {
    tableStack().wait();
    Resource->refDecr();
  }
  Resource = &newres;
  Resource->refIncr();
  refresh();
}

void SResourceTableBrowser::clearResource() {
  TableStack->clear();
  if(Resource != nullptr) {
    tableStack().wait();
    Resource->refDecr();
  }
  Resource = nullptr;
}

SAbsTreeNode* SResourceTableBrowser::rootNode() {
  SAbsTreeNode* res = nullptr;
  if (Resource != nullptr)
    res = &Resource->getRoot();
  return res;
}

void SResourceTableBrowser::newPathSelection(std::string path) {
  emit newResourcePathSelection(*Resource,path);
}

void SResourceTableBrowser::doOpenResourcePathSelection() {
  emit openResourcePathSelection(*Resource,getSelectedPathRaw());
}

/*******************************************************************************
 *
 *                          SDCMResourceTableBrowser
 *
 ******************************************************************************/

void SDCMResourceTableBrowser::refresh() {
  if (Resource != nullptr) {
    tableStack().clear();
    QList<SAbsTreeNode*> nodelist;
    SAbsTreeNode *root = rootNode();
    if (root != nullptr)
      nodelist.append(rootNode());
    std::string archpath;
    SDICOMArch *archcheck = dynamic_cast<SDICOMArch*>(Resource);
    if (archcheck != nullptr)
      archpath = archcheck->getImportDirective();
    SURI archpathuri(archpath);
    Depth = archpathuri.depth();
    tableStack().setDepth(Depth);
    doDICOMConfig(archpath);
    tableStack().setParentNodes(nodelist);
  }
}

SAbsTreeNode* SDCMResourceTableBrowser::rootNode() {
  if (Resource->getRoot().NodeChildrenNum() > 0)
    return Resource->getRoot().NodeChildren()[0];
  else
    return nullptr;
}

SDCMResourceTableBrowser::SDCMResourceTableBrowser
        (QWidget* parent, Qt::WindowFlags f): SResourceTableBrowser(parent, f) {

}

SDCMResourceTableBrowser::~SDCMResourceTableBrowser() {

}

void SDCMResourceTableBrowser::doDICOMConfig(const std::string &archpath) {
  DCMDataDic titledic;
  SURI archpathuri(archpath);
  for (unsigned i=0; i< archpathuri.depth(); i++) {
    DCMTag tmptag;
    QStringList attributes;
    tmptag.setID(archpathuri.getComponent(i));
      if (sysInfo::systemType() != "mobile") {
        switch (tmptag.getID()) {
          case PatientID_:
            attributes << "0010,0020" << "0010,0010" << "0010,0030"
                       << "0010,0040";
            break;
          case StudyUID_:
            attributes << "0010,0020" << "0010,0010" << "0008,1030"
                       << "0008,0020" << "0020,000d";
            break;
          case SeriesUID_:
            attributes << "0010,0020" << "0010,0010"  << "0008,103e"
                       << "0008,0060" <<  "0018,1030" << "0020,000e";
            break;
          default:
            attributes << "0010,0020" << "0010,0010" << "SNodeKey";
            break;
        }
    }
    else {
      switch (tmptag.getID()) {
        case PatientID_:
          attributes << "0010,0020" << "0010,0010" << "0010,0030";
          break;
        case StudyUID_:
          attributes << "0008,1030" << "0008,0020";
          break;
        case SeriesUID_:
          attributes << "0008,103e" << "0008,0060" <<  "0018,1030";
          break;
        default:
          attributes << "0010,0020" << "0010,0010" << "SNodeKey";
          break;
      }
    }
    if (titledic.contains(tmptag.getID()))
      tableStack().setLabel(i,titledic.getEntry(tmptag.getID()).Name);
    else
      tableStack().setLabel(i,tmptag.getIDstr().c_str());
    tableStack().setAttributes(i,attributes);
  }
}
