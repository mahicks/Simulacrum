/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:
 * A render area for Simulacrum Images in Qt
 * Author: M.A.Hicks
 */

#include "viewPort.h"
#include "lighttable.h"
#include <Core/error.h>
#include <Resources/DICOM/dcmtag.h>

#include <QPainter>
#include <QImage>
#include <QColor>
#include <QGridLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QWheelEvent>
#include <QEvent>
#include <stdint.h>
#include <qgraphicsitem.h>
#include <QWidget>
#include <QGraphicsDropShadowEffect>

#include <string.h>

using namespace Simulacrum;

/*******************************************************************************
 *                              SRenderSurface                                 *
 *                  A surface to which the image is drawn                      *
 ******************************************************************************/

BGSSpaceToQImage::BGSSpaceToQImage(QObject* parent): QThread(parent), 
    SourceSpace(nullptr), Downsample(false), PerfDown(0), DoPerfoDown(true), t1(0),
                                                 t2(0), t3(0), TargetRate(100) {
  rtimer.start();
}

BGSSpaceToQImage::~BGSSpaceToQImage() {

}

void BGSSpaceToQImage::updatePerfDown(uint64_t rtime) {
  if (DoPerfoDown) {
    t1 = t2;
    t2 = t3;
    t3 = rtime;
    unsigned long avgtime = (t1+t2+t3)/3;
    if (avgtime > TargetRate) {
      if (PerfDown > 0 || (avgtime > (TargetRate*2)) )
        PerfDown++;
    }
    else {
      if (PerfDown && avgtime < (TargetRate/1.5))
        PerfDown--;
    }
  }
}

void BGSSpaceToQImage::enablePerfDown(bool nperfdown) {
  DoPerfoDown = nperfdown;
  if (!nperfdown)
    PerfDown = 0;
}

uint64_t BGSSpaceToQImage::perfDown() {
  return PerfDown;
}

bool BGSSpaceToQImage::perfDownEnabled() {
  return DoPerfoDown;
}

void BGSSpaceToQImage::genRGBAFrom(SSpace& source, unsigned short downsample,
                                   bool merge, float opac, SCoordinate *cropb,
                                   SCoordinate *crope) {
  wait();
  SourceSpace = &source;
  Downsample  = downsample;
  DoMerge     = merge;
  MergeOpac   = opac;
  if ((cropb != nullptr) && (crope != nullptr)) {
    CropE       = *crope;
    CropB       = *cropb;
  }
  else {
    CropE.reset();
    CropB.reset();
  }
  start();
}

void BGSSpaceToQImage::run() {
  if (SourceSpace != nullptr) {
    short effectivedownsample = 0;
    if (Downsample)
      effectivedownsample = Downsample+PerfDown;
    SourceSpace->lock();
    SSpace *newrgba = new SSpace();
    rtimer.restart();
    SourceSpace->get2DRGBAInto(*newrgba,effectivedownsample,DoMerge,MergeOpac,
                               &CropB,&CropE);
    updatePerfDown(rtimer.elapsed());
    SourceSpace->unlock();
    emit newQImage(newrgba);
  }
}

SRenderSurface::~SRenderSurface() {

}

SCoordinate SRenderSurface::calcSpaciallyDeformedExtent(SSpace& source) {
  SCoordinate resextent = source.extent();
  SVector     spacing   = source.spacing();
  if ((unsigned)resextent.getDim() == spacing.getDim()
      && resextent.getDim() > 0 && (spacing.mag() > 0) ) {
    SVector::Precision mindimval = (SVector::Precision)0xFFFFFFFFFFFFFFFF;
    // find the largest dim
    for (unsigned d=0; d < spacing.getDim(); d++)
      if (spacing[d] < mindimval)
        mindimval = spacing[d];
    // normalise according to this
    for (unsigned d=0; d < spacing.getDim(); d++)
      spacing[d] /= mindimval;
    // scale the resultant extent
    for (unsigned d=0; d < (unsigned)resextent.getDim(); d++)
      resextent[d] =
                 SCoordinate::Precision((SVector::Precision)resextent[d] * spacing[d]);
  }
  return resextent;
}

SRenderSurfaceBasic::SRenderSurfaceBasic(QWidget* parent): QWidget(parent),
                                 SourceSpace(nullptr), DoDownsample(false),
                                 DoSpacialDeform(false), ContextOverlays(false),
                                 ContextOverlaysOpacity(0.4) {
  setOverlayOpacity(0.5);
  RGBAImage  = new SSpace();
  enableDownsample(false);
  enablePerfDownsample(true);
  enableSpatialDeform(true);
  enableContextOverlays(true);
  ResampleTimer.setSingleShot(true);
  setResampleTime(1000);
  SurfaceWidget = dynamic_cast<QWidget*>(parent);
  if (SurfaceWidget == nullptr)
    SurfaceWidget = this;
  connect(&BGRenderer,SIGNAL(newQImage(SSpace*)),this,SLOT(newImage(SSpace*)));
  connect(&ResampleTimer,SIGNAL(timeout()),this,SLOT(resample()));
}

SRenderSurfaceBasic::~SRenderSurfaceBasic() {
  BGRenderer.wait();
  DrawLock.lock();
  if (RGBAImage != nullptr)
    delete RGBAImage;
  DrawLock.unlock();
}

short unsigned int SRenderSurfaceBasic::downsampleScale(SSpace& source) {
  unsigned short res    = 1;
  const SCoordinate& sourcext = source.extent();
  if ( width() > 0 && height() > 0 && (sourcext.getDim() > 1 ) ) {
    int            xratio = sourcext.x() / width();
    int            yratio = sourcext.y() / height();
    if ((xratio > 1 || yratio > 1) && DoDownsample) {
      if (xratio > yratio)
        res = xratio;
      else
        res = yratio;
    }
  }
  return res;
}

void SRenderSurfaceBasic::setResampleTime(int newtime) {
  ResampleTimer.setInterval(newtime);
}

void SRenderSurfaceBasic::enableDownsample(bool ndown) {
  DoDownsample = ndown;
  enablePerfDownsample(ndown);
}

void SRenderSurfaceBasic::enablePerfDownsample(bool ndown) {
  BGRenderer.enablePerfDown(ndown);
}

bool SRenderSurfaceBasic::perfDownEnabled() {
  return BGRenderer.perfDownEnabled();
}

void SRenderSurfaceBasic::enableSpatialDeform(bool ndeform) {
  if (DoSpacialDeform != ndeform) {
    DoSpacialDeform = ndeform;
    if (SourceSpace != nullptr)
      SourceSpace->refresh(false);
  }
}

bool SRenderSurfaceBasic::spatiallyDeform() {
  return DoSpacialDeform;
}

void SRenderSurfaceBasic::newImage(SSpace* newimage) {
  DrawLock.lock();
  if (RGBAImage != nullptr)
    delete RGBAImage;
  RGBAImage = newimage;
  DrawSize = drawSize();
  DrawLock.unlock();
  SurfaceWidget->setCursor(RestoreCursor);
  update();
  emit updateSignal(false);
}

void SRenderSurfaceBasic::drawSImage(SSpace& newimg) {
  if (!BGRenderer.isRunning()) {
    SourceSpace = &newimg;
    const SCoordinate &SpaceExtent = SourceSpace->extent(); 
    QRect vissurf = visibleSurface();
    if ( SpaceExtent.getDim() > 1 &&
         ( (SpaceExtent.x() > vissurf.width()) ||
           (SpaceExtent.y() > vissurf.height())
         )
        ) {
      SCoordinate clipbegin, clipend;
      // buffer around the edges a bit
      const int edgebuffering = width()/20;
      // generate crop markers
      clipbegin.clamp_floor(coordToSSpace({vissurf.topLeft().x()-edgebuffering,
                                            vissurf.topLeft().y()-edgebuffering}
                                          )
                           );
      clipend.clamp_floor  (
                         coordToSSpace({vissurf.bottomRight().x()+edgebuffering,
                                        vissurf.bottomRight().y()+edgebuffering}
                                      )
                           );
      BGRenderer.genRGBAFrom(newimg,downsampleScale(newimg),ContextOverlays,
                             ContextOverlaysOpacity,&clipbegin,&clipend);
      ResampleTimer.start();
    }
    else {
      BGRenderer.genRGBAFrom(newimg,downsampleScale(newimg),ContextOverlays,
                             ContextOverlaysOpacity,nullptr,nullptr);
    }
    if (DoDownsample || (BGRenderer.perfDown() > 0))
      ResampleTimer.start();
  }
}

void SRenderSurfaceBasic::resample() {
  if ((SourceSpace != nullptr)) {
    if (BGRenderer.perfDown() >= 4) {
      RestoreCursor = SurfaceWidget->cursor();
      SurfaceWidget->setCursor(Qt::WaitCursor);
    }
    BGRenderer.genRGBAFrom(*SourceSpace,0,ContextOverlays,
                           ContextOverlaysOpacity);
  }
}

// very closely related to paint event below -- **centering on canvas**!
SVector SRenderSurfaceBasic::coordToSSpace(const SCoordinate& rcoord) {
  SCoordinate coordonpixels(2);
  SVector     retval(2);
  QSize       drawscale = drawSize();
  if ( SourceSpace != nullptr && (rcoord.getDim() == 2) ) {
    coordonpixels.x(rcoord.x() - ((width() - drawscale.width())/2) );
    coordonpixels.y(rcoord.y() - ((height() - drawscale.height())/2));
    SCoordinate sourceimgextent = SourceSpace->extent();
    if (sourceimgextent.getDim() > 1) {
      float xscale, yscale;
      xscale = ((float)sourceimgextent.x()) /
                                                ((float)drawscale.width());
      yscale = ((float)sourceimgextent.y()) /
                                               ((float)drawscale.height());
      retval[0] = ((double)(coordonpixels.x() * xscale));
      retval[1] = ((double)(coordonpixels.y() * yscale));
    }
  }
  return retval;
}

// very closely related to paint event below -- **centering on canvas**!
SVector SRenderSurfaceBasic::sspaceToCoord(const SVector& sspacecoord) {
  SVector result({-1.0,-1.0});
  if (SourceSpace != nullptr && sspacecoord.getDim() == 2) {
    QSize       drawsize        = drawSize();
    SCoordinate sourceimgextent = SourceSpace->extent();
    if (sourceimgextent.getDim() > 1) {
      // position on drawn pixels
      result.x((float)drawsize.width()*
                           ((float)sspacecoord.x()/(float)sourceimgextent.x()));
      result.y((float)drawsize.height()*
                           ((float)sspacecoord.y()/(float)sourceimgextent.y()));
      // offset of drawn pixels on render surface (centering)
      result.x(result.x() + ((width()  - drawsize.width() )/2) );
      result.y(result.y() + ((height() - drawsize.height())/2) );
    }
  }
  return result;
}

QSize SRenderSurfaceBasic::drawSize() {
  QSize result;
  float targetaspect = (float)width()/(float)height();
  if (RGBAImage != nullptr) {
    SCoordinate imgextent;
    if (DoSpacialDeform)
      imgextent = SRenderSurface::calcSpaciallyDeformedExtent(*RGBAImage);
    else
      imgextent = RGBAImage->extent();
    if (imgextent.getDim() == 2) {
      float   sourceaspect = (float)imgextent.x() / (float)imgextent.y();
      if ( (sourceaspect > targetaspect) ) {
        result.setWidth(width());
        result.setHeight((float)imgextent.y() * ((float)width() /
                                                 (float)imgextent.x()));
      }
      else {
        result.setWidth((float)imgextent.x() * ((float)height() /
                                                (float)imgextent.y()));
        result.setHeight(height());
      }
    }
  }
  return result;
}

QRect SRenderSurfaceBasic::visibleSurface() {
  QRect retval = QRect(0,0,width(),height());
  if (parentWidget() != nullptr) {
    int tlx,tly,vwidth,vheight, pwidth, pheight;
    // get parent information
    pwidth  = parentWidget()->width();
    pheight = parentWidget()->height();
    // calculate the visible TLC
    if (x() > 0)
      tlx = 0;
    else
      tlx = 0 - x();
    if (y() > 0)
      tly = 0;
    else
      tly = 0 - y();
    // calculate the visible BRC
    if ( (tlx + (width() - tlx)) > pwidth )
      vwidth = pwidth ;
    else
      vwidth = (width() - tlx);
    if ( (tly + (height() - tly)) > pheight )
      vheight = pheight ;
    else
      vheight = (height() - tly);
    retval = QRect(tlx,tly,vwidth,vheight);
  }
  return retval;
}

void SRenderSurfaceBasic::resizeEvent ( QResizeEvent* resev) {
  QWidget::resizeEvent(resev);
  DrawLock.lock();
  DrawSize = drawSize();
  DrawLock.unlock();
}

void SRenderSurfaceBasic::moveEvent(QMoveEvent* me) {
  QWidget::moveEvent(me);
  emit updateSignal(false);
}

void SRenderSurfaceBasic::showEvent(QShowEvent* se) {
  QWidget::showEvent(se);
  emit updateSignal(false);
}

void SRenderSurfaceBasic::paintEvent(QPaintEvent *pevent) {
  DrawLock.lock();
  QImage   drawimg;
  QPoint   drawxy(0,0);
  QPainter imagedrawer(this);
  imagedrawer.setClipRect(visibleSurface());
  imagedrawer.setRenderHint(QPainter::SmoothPixmapTransform);
  if (RGBAImage != nullptr) {
    if (RGBAImage->extent().getDim() > 1) {
      drawimg = QImage((uchar*)RGBAImage->selemDataStore()[0],
                               RGBAImage->extent().x(),
                               RGBAImage->extent().y(),
                               QImage::Format_ARGB32 );
    }
    drawxy.setX((width() - DrawSize.width())/2);
    drawxy.setY((height() - DrawSize.height())/2);
  }
  if (!drawimg.isNull()) {
    imagedrawer.drawImage(QRect(drawxy,DrawSize),drawimg);
  }
  else {
    QImage tmpimg = QImage(":resources/TestCard-image").scaled(width(),height(),
                                  Qt::KeepAspectRatio,Qt::SmoothTransformation);
    drawxy.setX((width() - tmpimg.width())/2);
    drawxy.setY((height() - tmpimg.height())/2);
    imagedrawer.drawImage(QRect(drawxy,tmpimg.size()),tmpimg);
  }
  DrawLock.unlock();
  QWidget::paintEvent(pevent);
}

bool SRenderSurfaceBasic::contextOverlays() {
  return ContextOverlays;
}

void SRenderSurfaceBasic::enableContextOverlays(bool nover) {
  if (ContextOverlays != nover) {
    ContextOverlays = nover;
    if (SourceSpace != nullptr)
      SourceSpace->refresh(false);
  }
}

void SRenderSurfaceBasic::setOverlayOpacity(float newopac) {
  if (ContextOverlaysOpacity != newopac) {
    ContextOverlaysOpacity = newopac;
    if (SourceSpace != nullptr)
      SourceSpace->refresh(false);
  }
}

/*******************************************************************************
 *                               SViewPortHUD                                  *
 *                       HeadsUpDisplay for the ViewPort                       *
 ******************************************************************************/

SViewPortHUD::SViewPortHUD(QWidget* parent): QWidget(parent) {
  clear();
  for (unsigned y = 0; y < LabelRows; y++)
    for (unsigned x = 0; x < LabelColumns; x++) {
      QLabel *newlabel = new QLabel(this);
      newlabel->setProperty("makeTrans",true);
      newlabel->setAttribute(Qt::WA_TransparentForMouseEvents);
      newlabel->setScaledContents(false);
      newlabel->setTextFormat(Qt::RichText);
#ifdef MOBILE
      QFont tmpfont = newlabel->font();
      tmpfont.setPointSize((int)((float)tmpfont.pointSize()*0.6));
      newlabel->setFont(tmpfont);
#endif
      //---------- Drop shadows -----------------------
      QGraphicsDropShadowEffect *newshadow =
                                     new QGraphicsDropShadowEffect(newlabel);
      newshadow->setColor(QColor(0,0,0));
      newshadow->setBlurRadius(ShadowRadius);
      newshadow->setOffset(ShadowOffset,ShadowOffset);
      newlabel->setGraphicsEffect(newshadow);
      //---------- End Drop shadows -------------------
      Qt::Alignment nalign;
      // horizontal alignment
      if (x == 0)
        nalign = Qt::AlignLeft;
      else if (x == (LabelColumns-1))
        nalign = Qt::AlignRight;
      else
        nalign = Qt::AlignHCenter;
      // vertical alignment
      if (y == 0)
        nalign |= Qt::AlignTop;
      else if (y == (LabelRows-1))
        nalign |= Qt::AlignBottom;
      else
        nalign |= Qt::AlignVCenter;
      newlabel->setAlignment(nalign);
      OverlayLabels.append(newlabel);
    }
    layoutLabels();
}

SViewPortHUD::~SViewPortHUD() {

}

void SViewPortHUD::clear() {
  BaseColor.setRgb(0,0,0,0);
  ActiveColor.setRgb(230,167,0);
  for ( int labelnum=0; labelnum < OverlayLabels.length(); labelnum++ )
    OverlayLabels[labelnum]->clear();
}

bool SViewPortHUD::isActive() {
  return Active;
}

void SViewPortHUD::setActive(bool nac) {
  Active = nac;
}

void SViewPortHUD::setTextColor(const QString& newcolor) {
  TextColour = QColor(newcolor);
}


void SViewPortHUD::setText(int position, const QString& text) {
  if (position < OverlayLabels.size()) {
    OverlayLabels[position]->setText("<font color=\"" + TextColour.name() +"\">"
                                     + text + "</font>");
    OverlayLabels[position]->adjustSize();
    layoutLabels();
  }
}

void SViewPortHUD::layoutLabels() {
  OverlayLabels[0]->move(ActiveBorderWidth,ActiveBorderWidth);
  OverlayLabels[1]->move((width()/2)-(OverlayLabels[1]->width()/2),ActiveBorderWidth);
  OverlayLabels[2]->move(width()-OverlayLabels[2]->width()-ActiveBorderWidth,ActiveBorderWidth);
  OverlayLabels[3]->move(ActiveBorderWidth,(height()/2)-(OverlayLabels[3]->height()/2));
  OverlayLabels[4]->move((width()/2)-(OverlayLabels[4]->width()/2),(height()/2)-(OverlayLabels[4]->height()/2));
  OverlayLabels[5]->move(width()-OverlayLabels[5]->width()-ActiveBorderWidth,(height()/2)-(OverlayLabels[5]->height()/2));
  OverlayLabels[6]->move(ActiveBorderWidth,height()-OverlayLabels[6]->height()-ActiveBorderWidth);
  OverlayLabels[7]->move((width()/2)-(OverlayLabels[7]->width()/2),height()-OverlayLabels[7]->height()-ActiveBorderWidth);
  OverlayLabels[8]->move(width()-OverlayLabels[8]->width()-ActiveBorderWidth,height()-OverlayLabels[8]->height()-ActiveBorderWidth);
}

void SViewPortHUD::paintEvent(QPaintEvent* pevent) {
  QWidget::paintEvent(pevent);
  QColor usecolor;
  if (Active)
    usecolor = ActiveColor;
  else
    usecolor = BaseColor;
  QPainter vppaint(this);
  vppaint.setPen(QPen(usecolor,
                      ActiveBorderWidth,
                      Qt::SolidLine,
                      Qt::RoundCap,
                      Qt::RoundJoin));
  vppaint.drawRect(0+(ActiveBorderWidth/2),
                    0+(ActiveBorderWidth/2),
                    width()-ActiveBorderWidth,
                    height()-ActiveBorderWidth);
}

void SViewPortHUD::resizeEvent(QResizeEvent* rev) {
  QWidget::resizeEvent(rev);
  layoutLabels();
}

/*******************************************************************************
 *                               SViewPort                                     *
 *                       The main viewport area                                *
 ******************************************************************************/

const unsigned SViewPort::BorderWidth = SViewPortHUD::ActiveBorderWidth;

SViewPort::SViewPort(QWidget* parent): QFrame(parent), ScaleFit(true),
                                       ScaleFitZoom(1.0), DoEmitMessage(false),
                                       SourceImage(nullptr), TW(nullptr), BW(nullptr),
                                       LW(nullptr), RW(nullptr) {
  RenderSurface = new SRenderSurfaceBasic(this);
  RenderSurfaceWidget = dynamic_cast<QWidget*>(RenderSurface);
  if (RenderSurfaceWidget == nullptr)
    RenderSurfaceWidget = this;
  else
    connect(RenderSurfaceWidget,SIGNAL(updateSignal(bool)),
            this,SIGNAL(updated(bool)));
  HUD = new SViewPortHUD(this);
  BusyDrawer = new BusyWidget(this);
  ClearMessageTimer = new QTimer(this);
  ClearMessageTimer->setSingleShot(true);
  connect(ClearMessageTimer, SIGNAL(timeout()), this, SLOT(showMessage()));
  setActive(false);
  HUD->setTextColor("#E6BA4B");
  clear();
  acceptCoreSignals();
  setAcceptDrops(true);
  setProperty("makeHardBG",true);
}

SViewPort::~SViewPort() {

}

bool SViewPort::isActive() {
  return HUD->isActive();
}

bool SViewPort::isHUDVisible() {
  return HUD->isVisible();
}

void SViewPort::setActive(bool nac) {
  HUD->setActive(nac);
}

void SViewPort::showHUD(bool nhud) {
  HUD->setVisible(nhud);
}

void SViewPort::setEdgeWidget(QWidget* newwidg, int position, bool restyle) {
  switch (position) {
    case 0:
      TW = newwidg;
      break;
    case 1:
      RW = newwidg;
      break;
    case 2:
      BW = newwidg;
      break;
    case 3:
      LW = newwidg;
      break;
    default:
      return;
      break;
  }
  if (newwidg != nullptr) {
    // check and disconnect from existing VPs
    SViewPort *parent = dynamic_cast<SViewPort*>(newwidg->parentWidget());
    if ((parent != nullptr)&&(parent!=this)) {
      if (parent->TW == newwidg)
        parent->setEdgeWidget(nullptr,0);
      if (parent->RW == newwidg)
        parent->setEdgeWidget(nullptr,1);
      if (parent->BW == newwidg)
        parent->setEdgeWidget(nullptr,2);
      if (parent->LW == newwidg)
        parent->setEdgeWidget(nullptr,3);
    }
    newwidg->setParent(this);
    if (restyle)
      newwidg->setStyleSheet("background-color: rgba(0, 0, 0, 40%);");
    resizeEvent(nullptr);
  }
}

void SViewPort::setHUDText(int position, const QString& text) {
  HUD->setText(position,text);
}

void SViewPort::showMessage(const QString& lmessage) {
  if (!DoEmitMessage)
    setHUDText(SViewPortHUD::ARBINFOPOS,lmessage);
  else
    emit message(lmessage);
  ClearMessageTimer->start(5000);
}

SSpace* SViewPort::sourceSSpace() {
  return SourceImage;
}

SSpace* SViewPort::endSourceSSpace() {
  SSpace *result = sourceSSpace();
  if (result != nullptr)
    result = dynamic_cast<SSpace*>(&(sourceSSpace()->end()));
  return result;
}

SSpace* SViewPort::penumSourceSSpace() {
  return dynamic_cast<SSpace*>(&(sourceSSpace()->penum()));
}

SRenderSurface& SViewPort::renderSurface() {
  return *RenderSurface;
}

QWidget& SViewPort::renderSurfaceWidget() {
  return *RenderSurfaceWidget;
}

SCoordinate SViewPort::coordToRenderSurface(const SCoordinate& reqcoord ) {
  SCoordinate retval(2);
  retval.x(reqcoord.x() - RenderSurfaceWidget->x());
  retval.y(reqcoord.y() - RenderSurfaceWidget->y());
  return retval;
}

SVector SViewPort::coordToSourceSSpace(const SCoordinate& reqcoord) {
  SCoordinate rcoord = coordToRenderSurface(reqcoord);
  return RenderSurface->coordToSSpace(rcoord);
}

SVector SViewPort::coordToEndSourceSSpace(const SCoordinate& reqcoord) {
  SSpace     *curspace = sourceSSpace();
  SCoordinate curloc;
  if (curspace != nullptr) {
    curloc.clamp_round(coordToSourceSSpace(reqcoord));
    while (true) {
      SSpace *nextsspace = &(curspace->getSourceSSpace());
      if (nextsspace != curspace) {
        curloc   = curspace->toSourceCoords(curloc);
        curspace = nextsspace;
      }
      else
        break;
    }
  }
  return curloc;
}

bool SViewPort::endSourceSSpaceToCoord(const SVector& sourcecoord,
                                                              SVector& result) {
  // build stack of sspaces through which to reverse project point
  SSpace *cursspace = sourceSSpace();
  SSpace *endspace  = endSourceSSpace();
  std::vector<SSpace*> sspacestack;
  result = sourcecoord;
  bool found = false;
  while (true) {
    if ( (cursspace == nullptr) || (endspace == nullptr) )
      break;
    sspacestack.push_back(cursspace);
    SSpace *nextspace = &(cursspace->getSourceSSpace());
    if ( (nextspace == cursspace) || (nextspace == endspace) )
      break;
    else
      cursspace = nextspace;
  }
  // project the point backwards throug the stack
  for (int i=(sspacestack.size()-1); i >= 0; i--) {
    SCoordinate newp, tmpp;
    tmpp.clamp_round(result);
    found = sspacestack[i]->fromSourceCoords(tmpp,newp);
    if (found)
      result = newp;
    else
      break;
  }
  return found;
}

SVector SViewPort::sourceSSpaceToCoord(const SVector& sspacecoord) {
  SCoordinate rendsurfpos;
  rendsurfpos.clamp_round(RenderSurface->sspaceToCoord(sspacecoord));
  if (rendsurfpos.getDim() == 2) {
    rendsurfpos.xy(rendsurfpos.x()+renderSurfaceWidget().x(),
                   rendsurfpos.y()+renderSurfaceWidget().y());
  }
  return rendsurfpos;
}

void SViewPort::centerOnSourceCoord(const SCoordinate& reqcoord,
                                    bool forcecenter) {
  if (reqcoord.getDim() == 2) {
    SVector curloc = sourceSSpaceToCoord(reqcoord);
    if ((curloc.getDim() == 2) && 
        (forcecenter || (curloc.x()<0) || (curloc.x()>=width()) ||
                        (curloc.y()<0) || (curloc.y()>=height()) )) {
      setScaleFit(false);
      QWidget &rendsur = renderSurfaceWidget();
      rendsur.move(rendsur.x() + ((width()/2) - curloc.x()),
                   rendsur.y() + ((height()/2) - curloc.y()));
    }
  }
}

const QString& SViewPort::name() {
  return VPName;
}

bool SViewPort::emitMessage() {
  return DoEmitMessage;
}

SDynamicLayout* SViewPort::layout(bool recurse) {
  SDynamicLayout *layoutres = nullptr;
  QWidget        *parsearch = parentWidget();
  while (parsearch != nullptr) {
    SDynamicLayout *layoutrest = dynamic_cast<SDynamicLayout*>(parsearch);
    if (layoutrest) {
      layoutres = layoutrest;
      if (!recurse)
        break;
    }
    parsearch = parsearch->parentWidget();
  }
  return layoutres;
}

QColor SViewPort::activeColor() {
  return HUD->ActiveColor;
}

void SViewPort::setName(const QString& npred) {
  VPName = npred;
}

void SViewPort::setEmitMessage(bool nemitm) {
  DoEmitMessage = nemitm;
}

void SViewPort::initLabels(SSpace& labelsource) {
  // extent information
  SSpace     &sourcespace   = labelsource.getSourceSSpace();
  SCoordinate actextent     = labelsource.extent();
  SCoordinate sourcextent   = sourcespace.extent();
  QString     standardlabel = actextent.toString().c_str();
  if (labelsource.spacing().getDim() > 1) {
    standardlabel += " (";
    standardlabel += (labelsource.spacing() * SVector(actextent))
                      .toString().c_str();
    standardlabel += " [";
    standardlabel += labelsource.spacingUnits().c_str();
    standardlabel += "])";
  }
  if (sourcextent != actextent) {
    standardlabel += "<br/>" + QString(sourcextent.toString().c_str()) + " ";
    // spatial information
    if (sourcespace.spacing().getDim() > 1) {
      standardlabel += "(";
      standardlabel += (sourcespace.spacing() * SVector(sourcextent))
                        .toString().c_str();
    standardlabel += " [";
      standardlabel += sourcespace.spacingUnits().c_str();
      standardlabel += "])";
    }
  }
  // sspace name
  standardlabel.append("<br/>");
  standardlabel.append(sourcespace.getName().c_str());
  setHUDText(6,standardlabel);
  // add direction label meanings, if present
  setHUDText(1,QString("<b>") +
               QString(labelsource.vectorMeaning({0,-1},false).c_str())
                + QString("</b>"));
  setHUDText(3,QString("<b>") +
   QString(labelsource.vectorMeaning({-1,0},false).c_str()).replace(',',"<br/>")
                + QString("</b>"));
  setHUDText(5,QString("<b>") +
    QString(labelsource.vectorMeaning({1,0},false).c_str()).replace(',',"<br/>")
                + QString("</b>"));
  setHUDText(7,QString("<b>") +
               QString(labelsource.vectorMeaning({0,1},false).c_str())
                + QString("</b>"));
}

void SViewPort::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  if (ScaleFit) {
    if (ScaleFitZoom == 1.0) {
      RenderSurfaceWidget->setGeometry(BorderWidth,BorderWidth,
                  width()-( (BorderWidth * 2) ),height()-( (BorderWidth * 2) ));
    }
    else {
      int newwidth  = ((int) ( width()  * ScaleFitZoom ));
      int newheight = ((int) ( height() * ScaleFitZoom ));
      RenderSurfaceWidget->setGeometry( 0-((newwidth - width())/2),
                                        0-((newheight - height())/2),
                                        newwidth,newheight);
    }
  }
  HUD->setGeometry(0,0,width(),height());
  emit resized(*this);
  updateWidgets();
}

void SViewPort::updateWidgets() {
  // centre embedded widgets
  if (TW != nullptr) {
    TW->move((width()/2)-(TW->width()/2),BorderWidth);
  }
  if (BW != nullptr) {
    BW->move((width()/2)-(BW->width()/2),height()-BW->height()-BorderWidth);
  }
  if (LW != nullptr) {
    LW->move(BorderWidth,(height()/2)-(LW->height()/2));
  }
  if (RW != nullptr) {
    RW->move(width()-(RW->width())-BorderWidth,(height()/2)-(RW->height()/2));
  }
}

void SViewPort::paintEvent(QPaintEvent* pevent) {
  QWidget::paintEvent(pevent);
}

void SViewPort::mousePressEvent(QMouseEvent* event) {
  event->accept();
  switch (event->button()) {
    case Qt::RightButton:
      emit rightClicked(*this, event->x(), event->y());
      break;
    case Qt::MiddleButton:
      emit middleClicked(*this, event->x(), event->y());
      break;
    default:
      emit leftClicked(*this, event->x(), event->y());
      break;
  }
}

void SViewPort::mouseMoveEvent(QMouseEvent* event) {
  event->accept();
#ifndef FASTDRAG
  static QTimer moveinterval;
  moveinterval.setSingleShot(true);
  if (!moveinterval.isActive()) {
    emit dragged(*this, event->x(), event->y());
    moveinterval.start(33);
  }
#else
  emit dragged(*this, event->x(), event->y());
#endif
}

void SViewPort::mouseDoubleClickEvent(QMouseEvent* event) {
  event->accept();
  emit doubleClicked(*this, event->x(), event->y());
}

void SViewPort::mouseReleaseEvent(QMouseEvent* event) {
  event->accept();
  emit buttonReleased(*this,event->x(),event->y());
}

void SViewPort::keyPressEvent(QKeyEvent* event) {
  event->accept();
  emit keyPressed(*this, event->key());
  QWidget::keyPressEvent(event);
}

void SViewPort::keyReleaseEvent(QKeyEvent* event) {
  event->accept();
  emit keyReleased(*this, event->key());
  QWidget::keyReleaseEvent(event);
}

void SViewPort::focusInEvent(QFocusEvent* event) {
  event->accept();
  QWidget::focusInEvent(event);
  setActive(true);
  emit activated(*this);
}

void SViewPort::focusOutEvent(QFocusEvent* event) {
  event->accept();
  QWidget::focusOutEvent(event);
  setActive(false);
  emit deActivated(*this);
}

void SViewPort::wheelEvent(QWheelEvent* event) {
  event->accept();
  if (event->delta() > 0)
    emit wheelUp(*this, event->x(), event->y());
  else
    emit wheelDown(*this, event->x(), event->y());
}

void SViewPort::dragEnterEvent(QDragEnterEvent* dragevent) {
  if (dragevent->mimeData()->hasFormat("sspace/pointer"))
    dragevent->acceptProposedAction();
}

void SViewPort::sconnect_end(SConnectable& targspace) {
  SConnectable &penumspace = core().penum();
  penumspace.lock();
  penumspace.sconnect(targspace);
  penumspace.unlock();
  penumspace.refresh(true);
}

void SViewPort::dropEvent(QDropEvent* dropevent) {
  const QMimeData *mimetmp = dropevent->mimeData();
  const SMimeData *targsspacedat = dynamic_cast<const SMimeData*>(mimetmp);
  if (targsspacedat != nullptr) {
    SSpace *targetspace = (SSpace*)targsspacedat->ObjectPointer;
    if (targetspace != nullptr) {
      sconnect_end(*targetspace);
    }
  }
}

void SViewPort::showEvent ( QShowEvent * event ) {
  QWidget::showEvent(event);
  emit updated(false);
}

void SViewPort::sconnect(SQConnectable& targ) {
  SQConnectable::sconnect(targ);
}

void SViewPort::sconnect(SConnectable& targ) {
  try {
    SSpace& tmptarg = dynamic_cast<SSpace&>(targ);
    if (tmptarg.try_lock()) {
      if (SourceImage != nullptr)
        sdisconnect(SourceImage);
      SourceImage = &tmptarg;
      showMessage(QString("Connected ")+
                                       QString(SourceImage->getName().c_str()));
      tmptarg.unlock();
      SQConnectable::sconnect(targ);
      targ.refresh(true);
      refresh(true);
    }
  }
  catch (std::exception &e) {
    // not an sspace -- probably shouldn't do anything
  }
}

void SViewPort::sdisconnect(SQConnectable& targ) {
  SQConnectable::sdisconnect(targ);
}

void SViewPort::sdisconnect(SConnectable* targ) {
  if (targ == SourceImage) {
    if (core().isConnected(targ)) {
      targ->lock();
      SourceImage = nullptr;
      targ->unlock();
    }
    else {
      SourceImage = nullptr;
    }
    refresh(true);
  }
  SQConnectable::sdisconnect(targ);
}

void SViewPort::sconfigure(const QString& confstrp) {
  SURI confparse;
  confparse.setSeparator(";");
  confparse.setURI(confstrp.toStdString());
  for (unsigned i=0; i<confparse.depth(); i++) {
    std::string confstr = confparse.getComponent(i);
    if (confstr == "nohud") {
      showHUD(false);
    } else if (confstr[confstr.size()-1] == 'x') {
      QString tmpstr = confstr.c_str();
      tmpstr.resize(confstr.size()-1);
      bool conv;
      float scaleval = tmpstr.toFloat(&conv);
      if (conv) {
        setScaleFit(true);
        setScaleZoom(scaleval);
      }
    }
  }
}

void SViewPort::setWaiting(bool waitstat) {
  if (waitstat) {
    BusyDrawer->setBusy(true);
    BusyDrawer->oscillateProgress();
  }
  else
    BusyDrawer->setBusy(false);
}

void SViewPort::setProgress(int prog) {
  BusyDrawer->showProgress(prog);
}

void SViewPort::showSImage(Simulacrum::SSpace& newsimage) {
  sconnect(newsimage);
  sdisconnect(&newsimage);
}

void SViewPort::refresh(bool deep) {
  if (SourceImage != nullptr) {
    if (deep) {
      SourceImage->lock();
      initLabels(*SourceImage);
      SourceImage->unlock();
      // handle VP stype efficiently -- set to black BG if not already
      if (SourceImage->extent().volume() > 1) {
        if (property("isTrans").toBool()) {
          setStyleSheet
                ("QWidget[makeHardBG=\"true\"] { background-color: #000000; }");
          setProperty("isTrans",false);
        }
      }
      else {
        setStyleSheet
            ("QWidget[makeHardBG=\"true\"] { background-color: transparent; }");
        setProperty("isTrans",true);
      }
    }
    RenderSurface->drawSImage(*SourceImage);
  }
  else {
    HUD->clear();
    RenderSurface->drawSImage(NullSpace);
    showMessage("Nothing connected");
    // make VP style transparent again
    setStyleSheet
           ("QWidget[makeHardBG=\"true\"] { background-color: transparent; }");
    setProperty("isTrans",true);
  }
  SQConnectable::refresh(deep);
}

void SViewPort::setScaleFit(bool doscale) {
  if (doscale != ScaleFit) {
    ScaleFit = doscale;
    resizeEvent(nullptr);
  }
  if (ScaleFit)
    setScaleZoom(1.0);
}

bool SViewPort::scaleFit() {
  return ScaleFit; 
}

void SViewPort::setScaleZoom(float nzoom) {
  if (nzoom != ScaleFitZoom) {
    ScaleFitZoom = nzoom;
    resizeEvent(nullptr);
  }
}

float SViewPort::scaleZoom() {
  return ScaleFitZoom;
}

void SViewPort::clear() {
  SourceImage = nullptr;
  HUD->clear();
  showHUD(true);
  BGColor.setRgb(0,0,0);
  QPalette bgpal;
  bgpal.setColor(QPalette::Background, BGColor);
  setAutoFillBackground(true);
  setPalette(bgpal);
  setFocusPolicy(Qt::StrongFocus);
  // render a temporary image
  showSImage(initIMG);
}

/*******************************************************************************
 *                          SDICOMViewPort                                     *
 *                  A Special Viewport for DICOM Files                         *
 ******************************************************************************/

SDCMViewPort::SDCMViewPort(QWidget* parent): SViewPort(parent) {
  clear();
}

SDCMViewPort::~SDCMViewPort() {

}

void SDCMViewPort::sconnect(SQConnectable& targ) {
  SViewPort::sconnect(targ);
}

void SDCMViewPort::sconnect(SConnectable& newimg) {
  SViewPort::sconnect(newimg);
  refresh(true);
}

void SDCMViewPort::sconnect(SConnectable& starget, bool dodel) {
    Simulacrum::SQConnectable::sconnect(starget, dodel);
}

void SDCMViewPort::initTagsDisplay(NNode& rootnode) {
  NNode& roottag           = rootnode;
  auto encoding            = SimulacrumLibrary::str_enc::Raw;
  const std::string encstr = "SpecificCharacterSet";
  // try to find the right charachter encoding
  if(roottag.hasChildNode(encstr)) {
    // now funnel it into the single point of conversion in a DCMTag
    DCMTag conv(0x0008,0x0005);
    conv.setVR('C','S');
    conv.fromString(roottag.getChildNode(encstr).NodeValue());
    encoding = conv.stringEncoding();
  }
  // now pull out the relevant items
  for (int labelord = 0; labelord < LabelProfile.size(); labelord++) {
    if (LabelProfile[labelord].size() > 0) {
      QString newlabel;
      for (int tagl = 0; tagl < LabelProfile[labelord].size(); tagl++) {
        if (roottag.hasChildNode(LabelProfile[labelord][tagl].toStdString())) {
          newlabel.append(SimulacrumGUILibrary::toQString(encoding,
              roottag.getChildNode(LabelProfile[labelord][tagl].toStdString()).
                                                           NodeValue()));
          newlabel.append("<br/>");
        }
      }
      setHUDText(labelord,newlabel);
    }
  }
}

void SDCMViewPort::setLabelProfile(const SDCMViewPortLabelProfile_t& newprof) {
  LabelProfile = newprof;
}

void SDCMViewPort::clear() {
  SViewPort::clear();
  // default label profile
  QList< QString > tl, tr, blank;
  SDCMViewPortLabelProfile_t cleanprofile;
  tl.push_back("PatientName");
  tl.push_back("PatientID");
  tl.push_back("SeriesDate");
  tr.push_back("InstitutionName");
  tr.push_back("Modality");
  tr.push_back("SeriesDescription");
  cleanprofile.append(tl);
  cleanprofile.append(blank);
  cleanprofile.append(tr);
  setLabelProfile(cleanprofile);
}

void SDCMViewPort::refresh(bool bval) {
  SViewPort::refresh(bval);
  if (SourceImage != nullptr) {
    try {
      SSpace &imgtmp = dynamic_cast<SSpace&>(*SourceImage).getSourceSSpace();
      if (bval) {
        imgtmp.lock();
        initTagsDisplay
                    (imgtmp.informationNode());
        imgtmp.unlock();
      }
    }
    catch (std::exception &e) {
      HUD->clear();
      DEBUG_OUT("Warning: Refreshing non-SSpace Viewport: ")
      DEBUG_OUT(e.what());
      return;
    }
  }
}

/*******************************************************************************
 *                          SViewPortEventHandler                              *
 *                  A an event handler for ViewPorts and Tools                 *
 ******************************************************************************/

SViewPortTool::SViewPortTool(QObject* parent): QAction(parent),
                                                             ReferencePoint(2) {
  setAdditionalWidgetF(std::bind(&SViewPortTool::doNullWidget,
                       this,
                       std::placeholders::_1,
                       std::placeholders::_2));
}

SViewPortTool::~SViewPortTool() {

}

void SViewPortTool::doNullWidget(QWidget* actwidg, QString) {
  actwidg->setParent(nullptr);
  actwidg->setWindowFlags(Qt::Tool);
}

void SViewPortTool::AdditionalWidgetHide(QWidget* actwidg) {
  actwidg->hide();
  actwidg->setParent(&Holder);
}

void SViewPortTool::connectViewPort(SViewPort& newvport) {
  connect (&newvport, SIGNAL(leftClicked(SViewPort&,int,int)),
           this, SLOT(leftClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(doubleClicked(SViewPort&,int,int)),
           this, SLOT(doubleClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(middleClicked(SViewPort&,int,int)),
           this, SLOT(middleClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(rightClicked(SViewPort&,int,int)),
           this, SLOT(rightClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(rightClicked(SViewPort&,int,int)),
           this, SLOT(rightClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(dragged(SViewPort&,int,int)),
           this, SLOT(dragged(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(wheelDown(SViewPort&,int,int)),
           this, SLOT(wheelDown(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(wheelUp(SViewPort&,int,int)),
           this, SLOT(wheelUp(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(keyPressed(SViewPort&,int)),
           this, SLOT(keyPressed(SViewPort&,int)));
}

bool SViewPortTool::oneShot() {
  return false;
}

void SViewPortTool::setAdditionalWidgetF(std::function<void(QWidget*,QString)>
                                                                       newadd) {
  AdditionalWidget = newadd;
}

void SViewPortTool::selected(SViewPort& ) { }

void SViewPortTool::deSelected(SViewPort& ) { }

void SViewPortTool::leftClicked(SViewPort&, int x, int y) {
  ReferencePoint.setDim(2);
  ReferencePoint.xy(x,y);
}

void SViewPortTool::middleClicked(SViewPort& , int, int) { }

void SViewPortTool::rightClicked(SViewPort& , int, int) { }

void SViewPortTool::doubleClicked(SViewPort& , int, int) { }

void SViewPortTool::buttonReleased(SViewPort& , int, int) { }

void SViewPortTool::dragged(SViewPort& , int, int) { }

void SViewPortTool::keyPressed(SViewPort& , int) { }

void SViewPortTool::keyReleased(SViewPort& , int) { }

void SViewPortTool::wheelUp(SViewPort& , int, int) { }

void SViewPortTool::wheelDown(SViewPort& , int, int) { }

void SViewPortTool::viewportResized(SViewPort& ) { }

void SViewPortTool::viewportUpdated(SViewPort& ) { }

// Action Event Handler Code

SViewPortEventHandler::SViewPortEventHandler
   (QWidget* parent): QToolBar(parent), ActiveTool(nullptr),
                                               ActiveViewPort(&SafetyViewPort) {
  setProperty("makeDarker",true);
}

SViewPortEventHandler::~SViewPortEventHandler() {

}

void SViewPortEventHandler::setActiveViewPort(SViewPort& newvp) {
  ActiveViewPort = &newvp;
}

void SViewPortEventHandler::clearActiveViewPort(QObject* vpgone) {
  if (vpgone == ActiveViewPort)
    ActiveViewPort = &SafetyViewPort;
}

void SViewPortEventHandler::addSVPTool(SViewPortTool* newtool) {
// Annoyingly, toolbar sizings are slightly different on Windows
#ifndef _WIN32
  addAction(newtool);
  resize(sizeHint());
#else
  addAction(newtool);
  resize(sizeHint());
  resize(width()+22,height());
#endif
  connect (newtool, SIGNAL(triggered(bool)), this, SLOT(newActiveTool()));
  newtool->setAdditionalWidgetF(std::bind(&SViewPortEventHandler::makeNewWidget,
                                          this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
}

void SViewPortEventHandler::connectViewPort(SViewPort& newvport) {
  connect (&newvport, SIGNAL(leftClicked(SViewPort&,int,int)),
           this, SLOT(leftClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(doubleClicked(SViewPort&,int,int)),
           this, SLOT(doubleClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(middleClicked(SViewPort&,int,int)),
           this, SLOT(middleClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(rightClicked(SViewPort&,int,int)),
           this, SLOT(rightClicked(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(buttonReleased(SViewPort&,int,int)),
           this, SLOT(buttonReleased(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(dragged(SViewPort&,int,int)),
           this, SLOT(dragged(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(wheelDown(SViewPort&,int,int)),
           this, SLOT(wheelDown(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(wheelUp(SViewPort&,int,int)),
           this, SLOT(wheelUp(SViewPort&,int,int)));
  connect (&newvport, SIGNAL(keyPressed(SViewPort&,int)),
           this, SLOT(keyPressed(SViewPort&,int)));
  connect (&newvport, SIGNAL(keyReleased(SViewPort&,int)),
           this, SLOT(keyReleased(SViewPort&,int)));
  connect (&newvport, SIGNAL(activated(SViewPort&)),
           this, SLOT(setActiveViewPort(SViewPort&)));
  connect (&newvport,SIGNAL(destroyed(QObject*)),
           this,SLOT(clearActiveViewPort(QObject*)));
  connect (&newvport, SIGNAL(resized(SViewPort&)),
           this, SLOT(viewportResized(SViewPort&)));
  connect (&newvport, SIGNAL(updated(bool)),
           this, SLOT(viewportUpdated(bool)));
}

void SViewPortEventHandler::newActiveTool() {
  SViewPortTool * tmptool = dynamic_cast< SViewPortTool* > (sender());
  if ( tmptool != nullptr) {
    if (tmptool->oneShot()) {
      if (ActiveViewPort != nullptr)
              tmptool->selected(*ActiveViewPort);
    }
    else {
      // deactivate currently selected tool button
      if (ActiveTool != nullptr) {
        try {
          QToolButton *stb = 
                    dynamic_cast < QToolButton* > (widgetForAction(ActiveTool));
          stb->setCheckable(true);
          stb->setChecked(false);
          if (ActiveViewPort !=nullptr)
            ActiveTool->deSelected(*ActiveViewPort);
        }
        catch (std::exception&) {  }
      }
      // set active tool
      if (ActiveTool == tmptool)
        ActiveTool = nullptr;
      else
        ActiveTool = tmptool;
      // activate currently tool button
      if (ActiveTool != nullptr) {
        try {
          QToolButton *stb = 
                    dynamic_cast < QToolButton* > (widgetForAction(ActiveTool));
          if (stb != nullptr) {
            stb->setCheckable(true);
            stb->setChecked(true);
            if (ActiveViewPort !=nullptr)
              ActiveTool->selected(*ActiveViewPort);
          }
        }
        catch (std::exception&) {  }
      }
    }
  }
}

void SViewPortEventHandler::leftClicked(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->leftClicked(source,x,y);
  }
}

void SViewPortEventHandler::rightClicked(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->rightClicked(source,x,y);
  }
}

void SViewPortEventHandler::doubleClicked(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->doubleClicked(source,x,y);
  }
}

void SViewPortEventHandler::middleClicked(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->middleClicked(source,x,y);
  }
}

void SViewPortEventHandler::buttonReleased(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->buttonReleased(source,x,y);
  }
}

void SViewPortEventHandler::dragged(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->dragged(source,x,y);
  }
}

void SViewPortEventHandler::wheelUp(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->wheelUp(source,x,y);
  }
}

void SViewPortEventHandler::wheelDown(SViewPort& source, int x, int y) {
  if (ActiveTool != nullptr) {
    ActiveTool->wheelDown(source,x,y);
  }
}

static bool CTRL_STAT = false;
static bool ALT_STAT  = false;
static bool SHFT_STAT = false;

void SViewPortEventHandler::keyPressed(SViewPort& source, int key) {
  // set status for special keys
  if (key == Qt::Key_Control)
    CTRL_STAT = true;
  if (key == Qt::Key_Alt)
    ALT_STAT = true;
  if (key == Qt::Key_Shift)
    SHFT_STAT = true;
  // handle function-key events
  if (SHFT_STAT) {
    // a list of tools from the actions
    // find the action (will be other kinds of actions in bar, but sorted)
    QList <QAction*> toolactions = actions();
    std::vector<SViewPortTool*> tools;
    for (int a=0; a<toolactions.size() ; a++) {
      SViewPortTool* tooltest = dynamic_cast<SViewPortTool*>(toolactions[a]);
      if (tooltest != nullptr) {
        tools.push_back(tooltest);
      }
    }
    // direct key access, based on tool order
    if ( (key >= Qt::Key_F1) && (key <= Qt::Key_F12)) {
      int toolpos = key - Qt::Key_F1;
      if (CTRL_STAT)
        toolpos += 12;
      else
        if (ALT_STAT)
          toolpos += 24;
      if (toolpos < (int)tools.size())
        tools[toolpos]->trigger();
    }
    // relative position step
    if ( (tools.size()>0) &&
         ((key == Qt::Key_Left) || (key == Qt::Key_Right)) ) {
      if (ActiveTool != nullptr) {
        SViewPortTool *prevt = nullptr;
        for (unsigned t=0; t<tools.size(); t++) {
          if (tools[t] == ActiveTool) {
            if (key == Qt::Key_Left) {
              if (prevt == nullptr && tools.size() > 0)
                tools[0]->trigger();
              else
                prevt->trigger();
            } else  {
              if (tools.size() > t+1)
                tools[t+1]->trigger();
              else {
                tools[0]->trigger();
              }
            }
            break;
          }
          prevt = tools[t];
        }
      }
      else {
        if (key == Qt::Key_Left)
          tools[tools.size()-1]->trigger();
        else
          tools[0]->trigger();
      }
    }
    if (key == Qt::Key_Escape) {
      if (ActiveTool != nullptr) {
        ActiveTool->trigger();
      }
    }
  }
  // finally, deliver the key to tool
  if (ActiveTool != nullptr) {
    ActiveTool->keyPressed(source,key);
  }
}

void SViewPortEventHandler::keyReleased(SViewPort& source, int key) {
  // set status for special keys
  if (key == Qt::Key_Control)
    CTRL_STAT = false;
  if (key == Qt::Key_Alt)
    ALT_STAT = false;
  if (key == Qt::Key_Shift)
    SHFT_STAT = false;
  if (ActiveTool != nullptr) {
    ActiveTool->keyReleased(source,key);
  }
}

void SViewPortEventHandler::viewportResized(SViewPort& source) {
  if (ActiveTool != nullptr) {
    ActiveTool->viewportResized(source);
  }
}

void SViewPortEventHandler::viewportUpdated(bool deep) {
  if (ActiveTool != nullptr && deep) {
    QObject   *senderp  = sender();
    SViewPort *sendervp = dynamic_cast<SViewPort*>(senderp);
    if (sendervp != nullptr) {
      ActiveTool->viewportUpdated(*sendervp);
    }
  }
}

void SViewPortEventHandler::makeNewWidget(QWidget* nwidg, const QString title) {
  nwidg->setParent(nullptr);
  nwidg->setWindowFlags(Qt::Tool);
  emit newWidget(nwidg,title);
}

