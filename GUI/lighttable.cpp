/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:
 * A parameterisable viewport matrix
 * Author: M.A.Hicks
 */

#include "vpTools.h"
#include "lighttable.h"
#include <Toolbox/SPlugins/splugins.h>
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/sysInfo/sysInfo.h>

#include <QPushButton>
#include <QHBoxLayout>
#include <QPixmap>
#include <QMimeData>
#include <QEvent>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QInputDialog>
#include <QDesktopWidget>
#include <QApplication>
#include <QScreen>
#include <QScrollBar>
#include <qgraphicsitem.h>
#include <QToolBar>
#include <QStatusBar>
#include <QMenuBar>
#include <QList>
#include <QStringList>
#include <QTabWidget>
#include <QGroupBox>
#include <QInputDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>

using namespace Simulacrum;

static unsigned goodIconSize() {
#if QT_VERSION >= 0x050000
  return (unsigned)(QGuiApplication::primaryScreen()->logicalDotsPerInch()/1.3);
#else
  return 100;
#endif
}

static QPushButton* newMobileButton(const QString &text, const QIcon & icon,
                                  QWidget *parent) {
  QPushButton *result;
  result = new QPushButton(text,parent);
  result->setStyleSheet("border: none; margin: 0px;padding:\
0px; background-color: transparent;");
  result->setFlat(true);
  result->setIcon(icon);
  QSize icsz = result->iconSize();
  icsz.scale(parent->size(),Qt::KeepAspectRatio);
  result->setIconSize(icsz);
  result->setProperty("makeMobileButton",true);
  result->setMinimumHeight(result->iconSize().height());
  return result;
}

// -----------------------------------------------------------------------------

SSpaceCollection::SSpaceCollection(QWidget* parent): QListWidget(parent) {
  setViewMode(QListView::ListMode);
  setFlow(QListView::TopToBottom);
  setIconSize(QSize(SSpaceItem::IconRes(),SSpaceItem::IconRes()));
  setResizeMode(QListView::Adjust);
  setMovement(QListView::Snap);
  setWordWrap(true);
  setTextElideMode(Qt::ElideMiddle);
  setSortingEnabled(true);
  setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
  setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this,SIGNAL(__newSSpace(SSpace*)),
          this,SLOT(addSSpace(SSpace*)),Qt::BlockingQueuedConnection);
#ifdef MOBILE
  unsigned int goodsize = goodIconSize();
  verticalScrollBar()->setStyleSheet(
    "QScrollBar:vertical { width: " + QString::number(goodsize/2.5)  + "px; }");
#endif
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  connect(this,SIGNAL(activated(const QModelIndex&)),
          this,SLOT(standardAction(const QModelIndex&)));
}

void SSpaceCollection::resizeEvent(QResizeEvent* event) {
  setGridSize(QSize(this->width(),SSpaceItem::IconRes()+2));
  QListView::resizeEvent(event);
}

SSpaceCollection::~SSpaceCollection() {

}

QList<SSpaceItem*> SSpaceCollection::selectedSSpaces() {
  QList<SSpaceItem*> retlist;
  QList<QListWidgetItem*> selitems = selectedItems();
  for (int i=0; i<selitems.size(); i++) {
    SSpaceItem* convitem = dynamic_cast<SSpaceItem*>(selitems[i]);
    if (convitem != nullptr)
      retlist.append(convitem);
  }
  return retlist;
}

QList<SSpaceItem*> SSpaceCollection::sspaces() {
  QList<SSpaceItem*> retlist;
  QList<QListWidgetItem*> allitems = findItems("", Qt::MatchContains);
  for (int i=0; i<allitems.size(); i++) {
    SSpaceItem* convitem = dynamic_cast<SSpaceItem*>(allitems[i]);
    if (convitem != nullptr)
      retlist.append(convitem);
  }
  return retlist;
}

void SSpaceCollection::addSSpace(SSpace* nspace) {
  QList<SSpaceItem*> contents = sspaces();
  bool exists = false;
  for (int i = 0;i<contents.size();i++) {
    if (contents[i]->sourceSpace() == nspace) {
      exists = true;
      break;
    }
  }
  if (!exists) {
    addItem( (QListWidgetItem*) new SSpaceItem(nspace,this) );
    emit newSSpace(*nspace);
  }
}

void SSpaceCollection::reset(bool forceful) {
  QList<SSpaceItem*> curspaces = sspaces();
  for (int s=0; s<curspaces.size(); s++) {
    if (forceful) {
      if(!curspaces[s]->sourceSpace()->try_lock())
        continue;
      curspaces[s]->sourceSpace()->unlock();
      curspaces[s]->sourceSpace()->disconnectListeners_Refresh();
    }
    curspaces[s]->safeDelete();
  }
}

void SSpaceCollection::clear() {
  reset();
  QListWidget::clear();
}

void SSpaceCollection::clearUnconnected() {
  reset(false);
}

void SSpaceCollection::removeSelected() {
  QList<SSpaceItem*> selitems = selectedSSpaces();
  for (int i=0; i<selitems.size();i++) {
    if(!selitems[i]->sourceSpace()->try_lock())
      continue;
    selitems[i]->sourceSpace()->unlock();
    selitems[i]->sourceSpace()->disconnectListeners_Refresh();
    selitems[i]->safeDelete();
  }
}

void SSpaceCollection::duplicateBG(SSpace* source) {
  source->try_lock();
  SSpace *duplicate = new SSpace();
  duplicate->setNativeSElemType(source->getNativeSElemP());
  source->copyInto(*duplicate);
  duplicate->setName(source->getName()+"_dup");
  duplicate->deleteOnDisconnect(true);
  emit __newSSpace(duplicate);
  emit message(std::string("Duplicated "+ source->getName()).c_str(),5000);
  source->unlock();
}

void SSpaceCollection::duplicateSelected() {
  QList<SSpaceItem*> selitems = selectedSSpaces();
  for (int i=0; i<selitems.size();i++) {
    if(!selitems[i]->sourceSpace()->try_lock())
      continue;
    LoadingPool.addJob(std::bind(&SSpaceCollection::duplicateBG,this,
                                 selitems[i]->sourceSpace()));
    emit message(std::string("Duplicating "+
                             selitems[i]->sourceSpace()->getName()
                            ).c_str(),5000);
  }
}

QMimeData* SSpaceCollection::mimeData
                                   (const QList<QListWidgetItem*> items) const {
  SMimeData *mimedata = new SMimeData();
  if (items.length() > 0) {
    SSpaceItem* tmpitem = dynamic_cast<SSpaceItem*>(items[0]);
    if (tmpitem !=nullptr) {
      SSpace *tmpsspace = tmpitem->sourceSpace();
      mimedata->setData("sspace/pointer",
                                QByteArray("Simulacrum Pointer Embedded"));
      mimedata->ObjectPointer = (void*)tmpsspace;
    }
  }
  return mimedata;
}

QStringList SSpaceCollection::mimeTypes() const {
  return QStringList("sspace/pointer");
}

void SSpaceCollection::dragEnterEvent(QDragEnterEvent* dragevent) {
  if (dragevent->mimeData()->hasFormat("sspace/pointer") ||
      dragevent->mimeData()->hasFormat("sspace/pointer/new"))
    dragevent->acceptProposedAction();
}

void SSpaceCollection::dropEvent(QDropEvent* dropevent) {
  const QMimeData *mimetmp = dropevent->mimeData();
  const SMimeData *targsspacedat = dynamic_cast<const SMimeData*>(mimetmp);
  if (targsspacedat != nullptr) {
    SSpace *targetspace = (SSpace*)targsspacedat->ObjectPointer;
    if (targetspace != nullptr) {
      addSSpace(targetspace);
    }
  }
}

void SSpaceCollection::standardAction(const QModelIndex&) {
  QList<SSpaceItem*> selspaces = selectedSSpaces();
  for (int s=0; s<selspaces.size(); s++) {
    SSpace &targspace = *(selspaces[s]->sourceSpace());
    QString sspacename = targspace.getName().c_str();
    QString layout = "<viewportslicer name=\""+sspacename+"\" />";
    if (targspace.extent().getDim() > 2)
      if ((targspace.extent().z() > 1) || (targspace.extent().getDim() > 3))
        layout = "<hsplit border=\"0\" size1=\"600\" size2=\"240\"><viewportslicer precedent=\"1\" name=\""+sspacename+"\" /><vsplit border=\"0\" size1=\"418\" size2=\"204\"><vsplit border=\"0\" precedent=\"1\" size1=\"207\" size2=\"207\"><viewportslicer precedent=\"1\" vpconf=\"nohud;\" name=\""+sspacename+"\" /><viewportslicer spconf=\"1;0;1;1;0;0;0;0;1;\" vpconf=\"nohud;\" name=\""+sspacename+"\" /></vsplit><viewportslicer spconf=\"0;1;1;0;0;1;0;1;0;\" vpconf=\"nohud;\" name=\""+sspacename+"\" /></vsplit></hsplit>";
    emit newLayout(sspacename,layout);
    emit newSSpace(targspace);
  }
}

void SSpaceCollection::resourceLoader(SResource* source,
                                      const std::string path) {
  source->lock();
  emit loading(true);
  try {
    QString newlayoutstr =
              source->loader().layoutRecommendation(path).c_str();
    if (newlayoutstr.size() > 0) {
      emit newLayout(source->loader().sspaceListTitle
                                    (path).c_str(),newlayoutstr);
    }
    std::vector<std::string> spacesavailable =
                                source->loader().sspaceList(path);
    for (unsigned s=0; s<spacesavailable.size();s++) {
      emit loading(true);
      SSpace *tmpspace = new SSpace();
      emit watchSSpace(tmpspace);
      emit message(QString(tr("Loading: ")) + spacesavailable[s].c_str(),0);
      try {
        source->loader().getSSpaceInto(*tmpspace,path,
                                      spacesavailable[s]);
      }
      catch (std::exception &e) {
        SLogger::global().addMessage(
                  std::string("SSpaceCollection::resourceLoader: @ ")
                         + path + std::string(" -- ") + e.what());
      }
      emit stopWatchSSpace(tmpspace);
      emit __newSSpace(tmpspace);
      tmpspace->deleteOnDisconnect(true);
      emit message(QString(tr("Loading complete: ")) +
                                               spacesavailable[s].c_str(),5000);
    }
  }
  catch (std::exception &e) {
    SLogger::global().addMessage(
                  std::string("SSpaceCollection::resourceLoader: ") + e.what());
  }
  source->unlock();
  emit loading(false);
}

void SSpaceCollection::loadFromResource(SResource& source,
                                        const std::string path) {
  LoadingPool.addJob(
               std::bind(&SSpaceCollection::resourceLoader, this, &source, path)
              );
}

// -----------------------------------------------------------------------------

short unsigned int SSpaceItem::IconRes() {
#ifdef MOBILE
  return goodIconSize();
#else
  return 64;
#endif
}

SSpaceItem::SSpaceItem(SSpace* nsource, SSpaceCollection* parent):
                                    QListWidgetItem(parent), SourceSpace(nullptr) {
  setSourceSpace(nsource);
  acceptCoreSignals();
}

SSpaceItem::~SSpaceItem() {

}

SSpace* SSpaceItem::sourceSpace() {
  return SourceSpace;
}

QIcon SSpaceItem::genQIconfromSSpace(SSpace& iconsource) {
  QIcon resultanticon;
  if (iconsource.extent().getDim() > 1) {
  long int scalesize = IconRes();
    long int imagesize = iconsource.extent().x();
    if (imagesize > scalesize)
      scalesize = imagesize / scalesize;
    else
      scalesize = 0;
    if (scalesize < 0 )
      scalesize = 0;
    SSpace icontmp;
    iconsource.get2DRGBAInto(icontmp,scalesize);
    QPixmap pixmapstore;
    pixmapstore.convertFromImage( QImage((uchar*)icontmp.selemDataStore()[0],
                                  icontmp.extent().x(),
                                  icontmp.extent().y(),
                                  QImage::Format_ARGB32 ).scaled(IconRes(),
                                                                 IconRes(),
                                                 Qt::KeepAspectRatioByExpanding,
                                                       Qt::SmoothTransformation
                                                                )
                                );
    resultanticon = QIcon(pixmapstore);
  }
  return resultanticon;
}

void SSpaceItem::refresh(bool deep) {
  SQConnectable::refresh(deep);
  if (SourceSpace != nullptr) {
    setText(SourceSpace->getName().c_str());
    setIcon(genQIconfromSSpace(*SourceSpace));
  }
}

void SSpaceItem::setProgress(int prog) {
  SQConnectable::setProgress(prog);
}

void SSpaceItem::sdisconnect(SConnectable* targ) {
  SourceSpace = nullptr;
  SQConnectable::sdisconnect(targ);
}

void SSpaceItem::setWaiting(bool dowait) {
  SQConnectable::setWaiting(dowait);
}

void SSpaceItem::setSourceSpace(SSpace* nsource) {
  if (SourceSpace != nullptr) {
    sdisconnect(SourceSpace);
  }
  sconnect(*nsource);
  SourceSpace = nsource;
  refresh(true);
}

bool SSpaceItem::safeDelete() {
  bool result = false;
  if (SourceSpace != nullptr && core().isConnected(SourceSpace) ) {
    if (SourceSpace->refCount() <= 1) {
      sdisconnect(SourceSpace);
      delete this;
      result = true;
    }
  }
  else
    delete this;
  return result;
}

// -----------------------------------------------------------------------------

SCollapsibleToolBox::SCollapsibleToolBox(QWidget* parent): QFrame(parent),
                                              MainLayout(new QVBoxLayout(this)),
                           MainLabel(newMobileButton("Untitled",QIcon(),this)) {
  MainLabel->setCheckable(true);
  MainLabel->setChecked(true);
  MainLabel->adjustSize();
  setLayout(MainLayout);
  MainLayout->setSpacing(1);
  setProperty("SToolBox",true);
}

SCollapsibleToolBox::SCollapsibleToolBox(QWidget* child, QString title,
                                         QWidget* parent): QFrame(parent),
                                              MainLayout(new QVBoxLayout(this)),
                           MainLabel(newMobileButton("Untitled",QIcon(),this)) {
  MainLabel->setCheckable(true);
  MainLabel->setChecked(true);
  MainLabel->adjustSize();
  setProperty("SToolBox",true);
  setLayout(MainLayout);
  MainLayout->setSpacing(1);
  MainLabel->show();
  MainLabel->setMaximumHeight(MainLabel->height());
  MainLayout->addWidget(MainLabel);
  addWidget(child);
  setTitle(title);
}

SCollapsibleToolBox::~SCollapsibleToolBox() {

}

void SCollapsibleToolBox::childEvent(QChildEvent* e) {
  QObject::childEvent(e);
  if ((e->type() == QEvent::ChildRemoved) && MainLayout->count() < 2)
    deleteLater();
}

void SCollapsibleToolBox::setTitle(const QString& newtitle) {
  MainLabel->setText(newtitle);
}

void SCollapsibleToolBox::addWidget(QWidget* newwidg) {
  connect(MainLabel,SIGNAL(clicked(bool)),newwidg,SLOT(setVisible(bool)));
  newwidg->setParent(this);
  MainLayout->addWidget(newwidg);
}

void SCollapsibleToolBox::collapse() {
  for (int i=1; i < MainLayout->count(); i++)
    MainLayout->itemAt(i)->widget()->hide();
}

void SCollapsibleToolBox::expand() {
  for (int i=1; i < MainLayout->count(); i++)
    MainLayout->itemAt(i)->widget()->show();
}

void SCollapsibleToolBox::toggle() {
  for (int i=1; i < MainLayout->count(); i++)
    MainLayout->itemAt(i)->widget()->setVisible
                                 (!MainLayout->itemAt(i)->widget()->isVisible());
}

// -----------------------------------------------------------------------------

SDynamicLayout::SDynamicLayout(QWidget* parent): QSplitter(parent),
                      MainWidget(nullptr), EventHandler(nullptr), ParentLayout(nullptr) {
#ifdef MOBILE
  long int goodsize = goodIconSize();
#else
  long int goodsize = 30;
#endif
  setProperty("makeVPSplit",true);
  // configure the main selector
  Selector   = new QWidget(this);
  QHBoxLayout *buttonlayout = new QHBoxLayout(Selector);
  QPushButton *dovsplit = new QPushButton("",Selector);
  dovsplit->setIcon(QIcon(":resources/splitv-icon"));
  if (dovsplit->iconSize().height() < goodsize)
    dovsplit->setIconSize(QSize(goodsize,goodsize));
  dovsplit->setToolTip(tr("Split this segment vertically"));
  connect(dovsplit,SIGNAL(clicked(bool)),this,SLOT(splitv()));
  QPushButton *dohsplit = new QPushButton("",Selector);
  dohsplit->setToolTip(tr("Split this segment horizontally"));
  dohsplit->setIcon(QIcon(":resources/splith-icon"));
  if (dohsplit->iconSize().height() < goodsize)
    dohsplit->setIconSize(QSize(goodsize,goodsize));
  connect(dohsplit,SIGNAL(clicked(bool)),this,SLOT(splith()));
  QPushButton *doslicervp     = new QPushButton("",Selector);
  doslicervp->setToolTip(tr("Make this segment a viewport"));
  doslicervp->setIcon(QIcon(":resources/simulacrum-logo"));
  if (doslicervp->iconSize().height() < goodsize)
    doslicervp->setIconSize(QSize(goodsize,goodsize));
  connect(doslicervp,SIGNAL(clicked(bool)),this,SLOT(makeSlicerVP()));
  QPushButton *doclose  = new QPushButton("",Selector);
  doclose->setToolTip(tr("Close this segment"));
  doclose->setIcon(QIcon(":resources/closeflat-black-icon"));
  if (doclose->iconSize().height() < goodsize)
    doclose->setIconSize(QSize(goodsize,goodsize));
  connect(doclose,SIGNAL(clicked(bool)),this,SLOT(doDestroy()));
  buttonlayout->addStretch();
  buttonlayout->addWidget(doslicervp);
  buttonlayout->addWidget(dovsplit);
  buttonlayout->addWidget(dohsplit);
  buttonlayout->addWidget(doclose);
  buttonlayout->addStretch();
  if (parent == nullptr)
    doclose->hide();
  Selector->setLayout(buttonlayout);
  Selector->show();
  addWidget(Selector);
  // try to set parent layout
  ParentLayout = dynamic_cast<SDynamicLayout*>(parent);
}

void SDynamicLayout::setEventHandler(SViewPortEventHandler* neh) {
  EventHandler = neh;
}

SDynamicLayout::~SDynamicLayout() {

}

bool SDynamicLayout::isWidget() {
  return (MainWidget != nullptr);
}

QWidget& SDynamicLayout::mainWidget() {
  if (isWidget())
    return *MainWidget;
  else
    return *this;
}

QList< SDynamicLayout* > SDynamicLayout::children() {
  QObjectList childrenl = QObject::children();
  QList< SDynamicLayout* > result;
  for (int c=0; c < childrenl.size(); c++) {
    SDynamicLayout *childlayout = dynamic_cast<SDynamicLayout*>(childrenl[c]);
    if (childlayout != nullptr)
      result.push_back(childlayout);
  }
  return result;
}

void SDynamicLayout::removeChild(SDynamicLayout* child) {
  QList< SDynamicLayout* > Children = children();
  for (int i = 0; i < Children.size(); i++)
    if (Children[i] == child)
      Children.removeAt(i);
}

void SDynamicLayout::fromXML(QString xmlstring) {
  NNode xmltree;
  std::stringstream xmlstream(xmlstring.toStdString());
  xmltree.loadFromXML(xmlstream);
  // must call from root node
  if (xmltree.NodeChildren(true).size() > 0)
    fromNode(*(xmltree.NodeChildren(true)[0]));
}

const std::string SPLITHSTR = "hsplit";
const std::string SPLITVSTR = "vsplit";
const std::string VPSTAND   = "viewport";
const std::string VPSLICE   = "viewportslicer";
const std::string PREDATTR  = "name";
const std::string PATTR     = "precedent";
const std::string STR1ATTR  = "size1";
const std::string STR2ATTR  = "size2";
const std::string BRATTR    = "border";
const std::string VPCATTR   = "vpconf";
const std::string SPCATTR   = "spconf";

void SDynamicLayout::fromNode(SAbsTreeNode& layout) {
  clear();
  std::string command = layout.NodeName();
  if (command == SPLITVSTR || command == SPLITHSTR) {
    int stretch1, stretch2, borderwidth;
    stretch1 = stretch2 = 1;
    borderwidth = 3;
    if (layout.hasAttribute(STR1ATTR)) {
      std::stringstream conv(layout.getAttribute(STR1ATTR));
      conv >> stretch1;
    }
    if (layout.hasAttribute(STR2ATTR)) {
      std::stringstream conv(layout.getAttribute(STR2ATTR));
      conv >> stretch2;
    }
    if (layout.hasAttribute(BRATTR)) {
      std::stringstream conv(layout.getAttribute(BRATTR));
      conv >> borderwidth;
    }
    if (command == SPLITHSTR)
      splith(stretch1,stretch2,borderwidth);
    else
      splitv(stretch1,stretch2,borderwidth);
  }
  else {
    if (command == VPSTAND || command == VPSLICE) {
      std::string pred;
      std::string configvp;
      std::string configsspace;
      if (layout.hasAttribute(PREDATTR))
        pred = layout.getAttribute(PREDATTR).c_str();
      if (layout.hasAttribute(VPCATTR))
        configvp = layout.getAttribute(VPCATTR).c_str();
      if (layout.hasAttribute(SPCATTR))
        configsspace = layout.getAttribute(SPCATTR).c_str();
      if (command == VPSLICE)
        makeSlicerVP(pred.c_str(),configvp.c_str(),configsspace.c_str());
      else
        makeVP(pred.c_str(),configvp.c_str(),configsspace.c_str());
    }
  }
  // now recurse, if necessary
  QList< SDynamicLayout* > Children = children();
  if (layout.NodeChildren(true).size() == 2
      && Children.size() == 2
     ) {
    if (layout.NodeChildren(true)[1]->hasAttribute(PATTR)) {
      Children[0]->fromNode(*layout.NodeChildren(true)[0]);
      Children[1]->fromNode(*layout.NodeChildren(true)[1]);
    }
    else {
      Children[0]->fromNode(*layout.NodeChildren(true)[1]);
      Children[1]->fromNode(*layout.NodeChildren(true)[0]);
    }
  }
}

void SDynamicLayout::toNode(SAbsTreeNode& target) {
  target.clear();
  // is this a splitter?
  QList< SDynamicLayout* > Children = children();
  if (Children.size() == 1) {
    Children[0]->toNode(target);
  }
  else {
    if (Children.size() > 1) {
      // set the name of this splitter
      if (orientation() == Qt::Vertical)
        target.NodeName(SPLITVSTR);
      else
        target.NodeName(SPLITHSTR);
      // set the size properties
      QList<int> splitsizes = sizes();
      if (splitsizes.size() > 2) {
        target.setAttribute(STR1ATTR,
                QString::number(splitsizes[splitsizes.size()-2]).toStdString());
        target.setAttribute(STR2ATTR,
                QString::number(splitsizes[splitsizes.size()-1]).toStdString());
      }
      // recurse on children
      SAbsTreeNode &pchild = target.NewChild();
      Children[1]->toNode(pchild);
      Children[0]->toNode(target.NewChild());
      pchild.setAttribute(PATTR,"1");
    }
    else {
      // or is it a VP
      if (MainWidget != nullptr) {
        SViewPort *mainvp = dynamic_cast<SViewPort*>(MainWidget);
        if (mainvp != nullptr) {
          if ( dynamic_cast<SSlicer*>(mainvp->sourceSSpace()) ) {
            target.NodeName(VPSLICE);
          }
          else {
            target.NodeName(VPSTAND);
          }
          target.setAttribute(PREDATTR,mainvp->name().toStdString());
        }
      }
    }
  }
}

QString SDynamicLayout::toXML() {
  NNode toxmlnode;
  std::stringstream res;
  toNode (toxmlnode);
  toxmlnode.storeToXML(res);
  return res.str().c_str();
}

void SDynamicLayout::split(Qt::Orientation norient,
                           int stretch1, int stretch2, int sbd) {
  QList< SDynamicLayout* > Children = children();
  for (int i=0; i<Children.size();i++)
    delete Children[i];
  Children.clear();
  setOrientation(norient);
  if (Selector != nullptr)
    Selector->hide();
  SDynamicLayout *l1 = new SDynamicLayout(this);
  SDynamicLayout *l2 = new SDynamicLayout(this);
  l1->setEventHandler(EventHandler);
  l2->setEventHandler(EventHandler);
  addWidget(l1);
  addWidget(l2);
  QList<int> sizesv;
  // hidden selector widget
  sizesv.push_back(0);
  sizesv.push_back(stretch1);
  sizesv.push_back(stretch2);
  setSizes(sizesv);
  setHandleWidth(sbd);
  if (sbd == 0) {
    setHandleWidth(1);
    for (int i = 0; i < count(); i++) {
      QSplitterHandle *hndl = handle(i);
      hndl->setEnabled(false);
    }
  }
}

QList< SViewPort* > SDynamicLayout::viewports(bool recurse) {
  QList< SViewPort* > result;
  SViewPort *vpcheck = dynamic_cast<SViewPort*>(MainWidget);
  if (vpcheck != nullptr)
    result.push_back(vpcheck);
  if (recurse) {
    QList< SDynamicLayout* > Children = children();
    for (int c=0; c<Children.size(); c++)
      result << Children[c]->viewports(recurse);
  }
  return result;
}

void SDynamicLayout::splitv(int stretch1, int stretch2, int spd) {
  split(Qt::Vertical,stretch1,stretch2,spd);
}

void SDynamicLayout::splith(int stretch1, int stretch2, int spd) {
  split(Qt::Horizontal,stretch1,stretch2,spd);
}

void SDynamicLayout::useWidget(QWidget* newwidget) {
  if (MainWidget != nullptr)
    MainWidget->deleteLater();
  MainWidget = newwidget;
  if (Selector != nullptr) {
    if (newwidget == nullptr)
      Selector->show();
    else
      Selector->hide();
  }
  if (newwidget != nullptr)
    addWidget(newwidget);
}

void SDynamicLayout::makeVP(QString predilection, QString vpconf,
                            QString) {
  SDCMViewPort* nvp = new SDCMViewPort(this);
  if (EventHandler != nullptr)
    EventHandler->connectViewPort(*nvp);
  nvp->setName(predilection);
  nvp->sconfigure(vpconf);
  useWidget(nvp);
}

void SDynamicLayout::makeSlicerVP(QString predilection, QString vpconf,
                                  QString sspaceconf) {
  SDCMViewPort *newvp     = new SDCMViewPort(this);
  SSlicer      *newslicer = new SSlicer();
  newvp->sconnect((SConnectable&) *(newslicer), true);
  SSliceTool *SliceController = new SSliceTool(newvp);
  /* connect some GUI events */
  QObject::connect(newvp,SIGNAL(wheelUp(SViewPort&, int,int)),
                   SliceController,SLOT(wheelUp(SViewPort&, int,int)));
  QObject::connect(newvp,SIGNAL(wheelDown(SViewPort&,int,int)),
                   SliceController,SLOT(wheelDown(SViewPort&, int,int)));
  if (EventHandler != nullptr)
    EventHandler->connectViewPort(*newvp);
  newvp->setName(predilection);
  newvp->sconfigure(vpconf);
  newslicer->sconfigure(sspaceconf.toStdString());
  useWidget(newvp);
}

void SDynamicLayout::doDestroy() {
  if (ParentLayout != nullptr)
    if (ParentLayout->children().size() > 1) {
      ParentLayout->removeChild(this);
      delete this;
    }
}

void SDynamicLayout::clear() {
  if (MainWidget != nullptr)
    delete MainWidget;
  QList< SDynamicLayout* > Children = children();
  for (int i=0; i<Children.size(); i++)
    delete Children[i];
  Children.clear();
  Selector->show();
}

// -----------------------------------------------------------------------------

SLayoutStringAction::SLayoutStringAction(const QString& text, QObject* parent):
                                                          QAction(text,parent) {
  connect(this,SIGNAL(triggered(bool)),this,SLOT(doEmission()));
}

void SLayoutStringAction::setStringEmission(const QString& newstr) {
  ActionString = newstr;
}

void SLayoutStringAction::doEmission() {
  emit triggerString(ActionString);
  emit triggerString(text(),ActionString);
}

// -----------------------------------------------------------------------------

SLightTable::SLightTable(QWidget* parent, Qt::WindowFlags f):
                                                            QWidget(parent, f) {
  // this should be its own window
  setWindowFlags(Qt::Window);
  // configure the main layout
  MainMenu    = new QMenuBar(this);
  StatusBar   = new QStatusBar(this);
  panelLayout = new QSplitter(this);
  panelLayout->setOrientation(Qt::Vertical);
  mainLayout  = new QSplitter(this);
  mainLayout->setOrientation(Qt::Horizontal);
  DynLayout   = new QTabWidget(mainLayout);
  DynLayout->setContextMenuPolicy(Qt::CustomContextMenu);
  Collection  = new SSpaceCollection(this);
  VPTools     = new SViewPortEventHandler(this);
  mainLayout->addWidget(DynLayout);
  mainLayout->addWidget(panelLayout);
  WidgetStack = new QVBoxLayout(this);
  VPToolsMenu = new QMenu(this);
  MainMenu->show();
  VPToolsMenu->setTitle(tr("Tools"));
  QPushButton* cornbutton = new QPushButton
                              (QIcon(":resources/collapse-icon"),"", DynLayout);
  cornbutton->setToolTip(tr("Layout options"));
  cornbutton->setMaximumWidth(cornbutton->height()-12);
  WidgetStack->addWidget(MainMenu);
  WidgetStack->addSpacing(2);
  WidgetStack->addWidget(VPTools);
  WidgetStack->addSpacing(2);
  WidgetStack->addWidget(mainLayout,10);
  WidgetStack->addWidget(StatusBar);
  setLayout(WidgetStack);
  WidgetStack->setSpacing(0);
  WidgetStack->setContentsMargins(0,0,0,0);
  addPanelWidget(Collection, tr("Space Collection"));
  addLayout(tr("&Custom Layout"));
  mainLayout->setStretchFactor(0,5);
  mainLayout->setStretchFactor(1,2);
  setWindowTitle(tr("Light Table"));
  connect(DynLayout,SIGNAL(tabCloseRequested(int)),this,SLOT(closeLayout(int)));
  connect(Collection,SIGNAL(customContextMenuRequested(QPoint)),
          this,SLOT(showCollectionMenu()));
  connect(cornbutton,SIGNAL(clicked()),this,SLOT(showLayoutMenu()));
  connect(Collection,SIGNAL(newLayout(QString,QString)),
          this,SLOT(addLayout(QString,QString)));
  connect(Collection,SIGNAL(newSSpace(SSpace&)),
          this,SLOT(newSSpaceAvailable(SSpace&)));
  connect(Collection,SIGNAL(message(QString,int)),
          StatusBar,SLOT(showMessage(QString,int)));
  // configure the menus -------------------------------------------------------
  LayoutMenu = new QMenu(MainMenu);
  LayoutMenu->setTitle(QString(tr("&Layouts")));
  MainMenu->addMenu(LayoutMenu);
  CollectionMenu = new QMenu(MainMenu);
  CollectionMenu->setTitle(QString(tr("Volume &Collection")));
  MainMenu->addMenu(CollectionMenu);
  MainMenu->addMenu(VPToolsMenu);
#ifndef MOBILE
  DynLayout->setCornerWidget(cornbutton,Qt::TopRightCorner);
  DynLayout->setTabsClosable(true);
  DynLayout->setMovable(true);
#else
  VPTools->setStyleSheet("QToolButton { border: 1px solid #353535; \
background-color: rgba(0, 0, 0, 40%); color: #e5e5e5; border-radius: 5px; \
min-width: " + QString::number(goodIconSize()/1.5)  + "px; min-height: " + 
QString::number(goodIconSize()/1.5)  + "px; }");
  VPTools->setIconSize(QSize(goodIconSize()/1.5,goodIconSize()/1.5));
  VPTools->adjustSize();
  LayoutMenu->addMenu(CollectionMenu);
  LayoutMenu->addMenu(VPToolsMenu);
#endif
  //--------------
  QAction *newtabaction = new QAction(QIcon(":resources/new-icon"),
                                    tr("&New Custom Layout"), LayoutMenu);
  connect(newtabaction,SIGNAL(triggered()),
          this,SLOT(addLayout()));
  LayoutMenu->addAction(newtabaction);
  //--------------
  CustomLayoutMenu = new QMenu(LayoutMenu);
  CustomLayoutMenu->setTitle(tr("Store&d Layouts"));
  LayoutMenu->addMenu(CustomLayoutMenu);
  //--------------
  StoreName = new QInputDialog(this);
  StoreName->setWindowTitle("Store Active Layout");
  StoreName->setInputMode(QInputDialog::TextInput);
  StoreName->setLabelText("Layout Name:");
  StoreName->setOkButtonText("&Store");
  connect(StoreName,SIGNAL(textValueSelected(const QString&)),
          this,SLOT(storeActiveLayout(QString)));
  //--------------
  QAction *storeaction = new QAction(QIcon(":resources/save-icon"),
                                    tr("&Store Layout"), LayoutMenu);
  connect(storeaction,SIGNAL(triggered()),
          StoreName,SLOT(show()));
  LayoutMenu->addAction(storeaction);
  //--------------
#ifdef SDEBUG
  QAction *showlayoutaction = new QAction(QIcon(""),
                                    tr("Show Layout &XML"), LayoutMenu);
  connect(showlayoutaction,SIGNAL(triggered()),
          this,SLOT(showLayoutXML()));
  LayoutMenu->addAction(showlayoutaction);
#endif
  //--------------
  QAction *closetabaction = new QAction(QIcon(":resources/close-icon"),
                                       tr("Close &Layout"), LayoutMenu);
  connect(closetabaction,SIGNAL(triggered()),this,SLOT(closeActiveLayout()));
  LayoutMenu->addAction(closetabaction);
  //--------------
  QAction *refreshpluaction = new QAction(tr("&Refresh Plugins"), LayoutMenu);
  connect(refreshpluaction,SIGNAL(triggered()),this,SLOT(reloadPlugins()));
  LayoutMenu->addAction(refreshpluaction);
  //--------------
  QAction *showsidebar = new QAction(tr("Show Side &Panel"),LayoutMenu);
  showsidebar->setCheckable(true);
  showsidebar->setChecked(true);
  connect(showsidebar,SIGNAL(toggled(bool)),panelLayout,SLOT(setVisible(bool)));
  LayoutMenu->addAction(showsidebar);
  //--------------
  QAction *closewindow = new QAction(tr("&Close ") + windowTitle(),LayoutMenu);
  connect(closewindow,SIGNAL(triggered()),this,SLOT(close()));
  LayoutMenu->addAction(closewindow);
  //--------------
  QAction *clrselaction = new QAction(QIcon(":resources/clear-icon"),
                                         tr("Remove &Selected"),CollectionMenu);
  connect(clrselaction,SIGNAL(triggered(bool)),
          Collection,SLOT(removeSelected()));
  CollectionMenu->addAction(clrselaction);
  //--------------
  QAction *dupselaction = new QAction(QIcon(":resources/plus-icon"),
                                      tr("&Duplicate Selected"),CollectionMenu);
  connect(dupselaction,SIGNAL(triggered(bool)),
          Collection,SLOT(duplicateSelected()));
  CollectionMenu->addAction(dupselaction);
  //--------------
  QAction *clearunconaction = new QAction(QIcon(":resources/clear-icon"),
                                       tr("Clear &Unconnected"),CollectionMenu);
  connect(clearunconaction,SIGNAL(triggered(bool)),
          Collection,SLOT(clearUnconnected()));
  CollectionMenu->addAction(clearunconaction);
  //--------------
  QAction *clearaction = new QAction(QIcon(":resources/clear-icon"),
                                        tr("Clear &Collection"),CollectionMenu);
  connect(clearaction,SIGNAL(triggered(bool)),Collection,SLOT(reset()));
  CollectionMenu->addAction(clearaction);
  //--------------
  connect(VPTools,SIGNAL(newWidget(QWidget*, const QString)),
          this,SLOT(addPanelWidget(QWidget*, const QString&)));
  populateTools();
  //----------------------------------------------------------------------------
  QWidget *genprogwidget
                  = new QWidget(StatusBar);
  GeneralProgress = new BusyWidget(genprogwidget);
  genprogwidget->setMinimumSize(200,StatusBar->height()-15);
  GeneralProgress->setOpacity(0.2);
  GeneralProgress->setBusyAnim(":resources/wait-small");
  genprogwidget->setToolTip(tr("No background jobs running"));
  GeneralProgress->setToolTip(tr("Progress of background job(s)"));
  StatusBar->addPermanentWidget(genprogwidget,0);
  //----------------------------------------------------------------------------
  connect(Collection,SIGNAL(loading(bool)),
          GeneralProgress,SLOT(setWaiting(bool)));
  connect(Collection,SIGNAL(watchSSpace(SConnectable*)),
          GeneralProgress,SLOT(sconnect(SConnectable*)));
  connect(Collection,SIGNAL(stopWatchSSpace(SConnectable*)),
          GeneralProgress,SLOT(sdisconnect(SConnectable*)));
  //----------------------------------------------------------------------------
  loadPlugins();
}

void SLightTable::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
}

void SLightTable::showCollectionMenu() {
  CollectionMenu->exec(QCursor::pos());
}

void SLightTable::showLayoutMenu() {
  LayoutMenu->exec(QCursor::pos());
}

void SLightTable::populateTools() {
  VPTools->clear();
  VPTools->addSeparator();
  SViewPortTool *newtool = new SPointInfoTool(VPTools);
  VPToolsMenu->addAction(newtool);
  VPTools->addSVPTool(newtool);
  newtool = new SMarkMeasure(VPTools);
  VPToolsMenu->addAction(newtool);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  VPTools->addSeparator();
  newtool = new SPanTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SZoomTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SFitStretchTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new S1to1Tool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SWLTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SInvertTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  VPTools->addSeparator();
  newtool = (new SSliceTool(VPTools))->doMouseScroll(false);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new STimeSliceTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SOrthoRotateTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SPivotTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SRotateTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SMirrorTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SFlipTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  VPTools->addSeparator();
  newtool = new SMaxIPTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SMinIPTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SAVGIPTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SOZIPTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SCurvePTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  VPTools->addSeparator();
  newtool = new SChromaTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SVPProperties(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SInfoTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SSnapShot(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SExport(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SVPCloseTool(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  newtool = new SReinterpret(VPTools);
  VPTools->addSVPTool(newtool);
  VPToolsMenu->addAction(newtool);
  VPTools->addSeparator();
  VPTools->show();
}

SLightTable::~SLightTable() {
  clearPlugins();
  delete VPTools;
  delete DynLayout;
  Collection->reset(true);
  delete Collection;
}

SSpaceCollection& SLightTable::collection() {
  return *Collection;
}

void SLightTable::addPanelWidget(QWidget* newpanelwidget, const QString &ttl) {
  panelLayout->addWidget(
                       new SCollapsibleToolBox(newpanelwidget,ttl,panelLayout));
  if (newpanelwidget->minimumHeight() < (panelLayout->height() / 8))
    newpanelwidget->setMinimumHeight(panelLayout->height() / 8);
  newpanelwidget->show();
}

void SLightTable::showMessage(const QString newmsg) {
  StatusBar->showMessage(newmsg);
}

void SLightTable::reset() {
  DynLayout->clear();
  Collection->clear();
  addLayout();
}

SDynamicLayout* SLightTable::activeLayout() {
  return dynamic_cast<SDynamicLayout*>(DynLayout->currentWidget());
}

BusyWidget& SLightTable::generalProgress() {
  return *GeneralProgress;
}

std::string SLightTable::layoutsPath() {
  return "Layouts";
}

void SLightTable::addLayout(const QString name, const QString layoutxml) {
  SDynamicLayout* nlo = new SDynamicLayout(DynLayout);
  nlo->setEventHandler(VPTools);
  DynLayout->setCurrentIndex(DynLayout->addTab(nlo,name));
  nlo->fromXML(layoutxml);
}

void SLightTable::closeActiveLayout() {
  if (DynLayout->count() > 0)
    closeLayout(DynLayout->currentIndex());
  if (DynLayout->count() < 1)
    addLayout();
}

void SLightTable::closeLayout(int index) {
  if (DynLayout->count() > 0) {
    QWidget *delwidget = DynLayout->widget(index);
    if (delwidget != nullptr) {
      DynLayout->removeTab(index);
      delwidget->deleteLater();
    }
  }
  if (DynLayout->count() < 1)
    addLayout();
}

void SLightTable::addToolWidget(QWidget* newwidg) {
  StatusBar->addPermanentWidget(newwidg);
}

void SLightTable::newSSpaceAvailable(SSpace& newsspace) {
  SDynamicLayout *layout = activeLayout();
  // try the active tab first
  if (layout != nullptr) {
    QList<SViewPort*> activevps = layout->viewports(true);
    for (int v=0; v < activevps.size(); v++) {
      if (activevps[v]->name().toStdString() == newsspace.getName()) {
        activevps[v]->sconnect_end(newsspace);
      }
    }
  }
  // then cycle through other tabs
  for (int t=0; t<DynLayout->count(); t++) {
    layout = dynamic_cast<SDynamicLayout*>(DynLayout->widget(t));
    if (layout != nullptr) {
      QList<SViewPort*> activevps = layout->viewports(true);
      for (int v=0; v < activevps.size(); v++) {
        if (activevps[v]->name().toStdString() == newsspace.getName()) {
          activevps[v]->sconnect_end(newsspace);
        }
      }
    }
  }
}

void SLightTable::loadPlugins() {
  std::vector<std::string> pluginsavailable =
                              SPluginManager::global().plugins<SViewPortTool>();
  for (unsigned p=0; p < pluginsavailable.size(); p++) {
    SViewPortTool* newplugin =
                       SPluginManager::global().getPluginInstance<SViewPortTool>
                                                          (pluginsavailable[p]);
    bool visibility = newplugin->isVisible();
    newplugin->setParent(VPTools); // will hide plugin
    newplugin->setVisible(visibility);
    PluginTools.push_back(newplugin);
    VPTools->addSVPTool(newplugin);
    VPToolsMenu->addAction(newplugin);
  }
  reloadLayouts();
}

void SLightTable::reloadLayouts() {
  // remove existing
  for (int i=0; i<Layouts.size(); i++)
    Layouts[i]->deleteLater();
  Layouts.clear();
  // now load layouts
  SURI layoutspath(sysInfo::simulacrumDIR());
  layoutspath.addComponentBack(SLightTable::layoutsPath());
  SFile layoutdir(layoutspath.getURI());
  try {
    if (layoutdir.isDIR()) {
      std::vector<std::string> layoutsl = layoutdir.getDIRContents();
      for (unsigned lfl=0; lfl < layoutsl.size(); lfl++) {
        if ((layoutsl[lfl].size() > 4) &&
            (layoutsl[lfl].find(".xml",layoutsl[lfl].size()-5) 
             != std::string::npos) ) {
          SFile loaderfile(layoutsl[lfl]);
          std::string layoutsrc = loaderfile.toString();
          SURI layoutfilepath(layoutsl[lfl]);
          QString layoutname;
          if (layoutfilepath.depth() > 0)
            layoutname =
                  layoutfilepath.getComponent(layoutfilepath.depth()-1).c_str();
          layoutname.replace(".xml","");
          SLayoutStringAction *newlayout = new SLayoutStringAction
                                       (layoutname,CustomLayoutMenu);
          connect(newlayout,SIGNAL(triggerString(QString,QString)),
                  this,SLOT(addLayout(QString,QString)));
          newlayout->setStringEmission(layoutsrc.c_str());
          CustomLayoutMenu->addAction(newlayout);
          Layouts.push_back(newlayout);
        }
      }
    }
  }
  catch (std::exception &e) {
    SLogger::global().addMessage("SLightTable::reloadLayouts: Error " +
                                 std::string("reading layouts: ") +
                                 std::string(e.what()));
  }
}

void SLightTable::storeActiveLayout(QString name) {
  SDynamicLayout *layoutl = activeLayout();
  if (layoutl != nullptr && name.size() > 0 ) {
    SURI layoutloc(sysInfo::simulacrumDIR());
    layoutloc.addComponentBack(SLightTable::layoutsPath());
    layoutloc.addComponentBack(name.toStdString()+".xml");
    SFile layoutfile(layoutloc.getURI());
    layoutfile.fromString(layoutl->toXML().toStdString());
    reloadLayouts();
  }
}

void SLightTable::clearPlugins() {
  for (unsigned i=0; i< PluginTools.size(); i++)
    delete PluginTools[i];
  PluginTools.clear();
}

void SLightTable::reloadPlugins() {
  clearPlugins();
  SPluginManager::global().loadAll();
  loadPlugins();
}

void SLightTable::showLayoutXML() {
  QLabel *newqlabel = new QLabel(this,Qt::Tool);
  newqlabel->setAttribute(Qt::WA_DeleteOnClose,true);
  SDynamicLayout *actlayout = dynamic_cast<SDynamicLayout*>
                                                   (DynLayout->currentWidget());
  newqlabel->setText(actlayout->toXML());
  newqlabel->setWordWrap(true);
  newqlabel->show();
  newqlabel->resize(640,480);
  newqlabel->move(x()+((width()/2)-(newqlabel->width()/2)),
                  y()+((height()/2)-(newqlabel->height()/2)));
  newqlabel->setTextInteractionFlags(Qt::TextSelectableByMouse|
                                     Qt::TextSelectableByKeyboard);
}

void SLightTable::showMenu(bool vis) {
  MainMenu->setVisible(vis);
}

void SLightTable::mobileDevice() {
  unsigned goodsize = goodIconSize();
  QWidget *MobileBar = new QWidget(this);
  MobileBar->setProperty("makeMobileBar",true);
  QHBoxLayout *mlayout = new QHBoxLayout(MobileBar);
  mlayout->setContentsMargins(0,0,0,0);
  MobileBar->setLayout(mlayout);
  MobileBar->setStyleSheet("QWidget[makeMobileBar=\"true\"]{ background: \
qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #333333, stop:1 #727272); }");
  MobileBar->setMinimumHeight(goodsize);
  MobileBar->setMaximumHeight(goodsize);
  QPushButton *mainbutton = newMobileButton(" ",
                                            QIcon(":resources/simulacrum-logo"),
                                            MobileBar);
  mainbutton->setMenu(LayoutMenu);
  MobileBar->layout()->addWidget(mainbutton);
  MobileBar->layout()->addWidget(VPTools);
  mlayout->addStretch();
  WidgetStack->insertWidget(0,MobileBar);
  MobileBar->setVisible(true);
}
