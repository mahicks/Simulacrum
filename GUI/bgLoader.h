/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:SIMAGEBGLOADER
 * A Qt background loader for SImages
 * Author: M.A.Hicks
 */
#ifndef SIMAGEBGLOADER
#define SIMAGEBGLOADER

#include <QThread>

#include <GUI/definitions.h>
#include <Core/sspace.h>
#include <Core/sresource.h>
#include <Core/definitions.h>

#include <GUI/viewPort.h>
#include <GUI/qconnectable.h>

namespace Simulacrum {

  class SIMU_GUI_API SImageBGLoader : public QThread, public SQConnectable {
      Q_OBJECT
  private:
    SResource        *Source;
    SSpace           *Image;
    std::string       Path;
    void doWork();
  public:
         SImageBGLoader();
    void run();
  public slots:
    void refresh(bool);
    void setResource(SResource&);
    void setResourcePath(const std::string&);
    void setSImage(SSpace&);
    void clear();
    void blank();
    void stop();
  signals:
    void active(bool);
    void progress(int);
    void updated(bool);
  };

} // Simulacrum

#endif //SIMAGEBGLOADER
