/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:VPTools
 * Tools for ViewPorts
 * Author: M.A.Hicks
 */
#ifndef SVIEWPORT_TOOLS
#define SVIEWPORT_TOOLS

#include <GUI/definitions.h>
#include "viewPort.h"
#include <GUI/grapher.h>
#include <Core/LUT/LUT.h>

#include <QFileDialog>
#include <QLabel>
class QLineEdit;
class QComboBox;
class QSlider;
class QAbstractSlider;
class QSpinBox;
class QCheckBox;
class QInputDialog;
class QColorDialog;
class QToolButton;
class QPixmap;

namespace Simulacrum {

/*******************************************************************************
 *                          Standard Tools                                     *
 *                                                                             *
 ******************************************************************************/

  class SPOIObject;

  class SIMU_GUI_API SSliderTool : public SViewPortTool {
    Q_OBJECT
  protected:
    QAbstractSlider
                  *VPSlider;
    SViewPort     *TargetVP;
    void           configureSlider ();
    virtual void   doSliderValue   (int);
    virtual int    sliderMax       ();
    virtual int    sliderMin       ();
  public:
                   SSliderTool     (QObject* parent = 0);
    virtual       ~SSliderTool     ();
  public slots:
    virtual void   leftClicked     (SViewPort&, int, int);
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
            void   newSliderValue  (int);
            void   viewportResized (SViewPort&);
    virtual void   VPGone          ();
  };

  class SIMU_GUI_API SMaxIPTool : public SSliderTool {
    Q_OBJECT
  protected:
    virtual void   doSliderValue      (int);
    virtual int    sliderMax          ();
    virtual int    sliderMin          ();
    virtual
    void           configureReduction ();
    SSlicer       *SourceSlicer;
    QList<QWidget*>
                   Projections;
    QTimer         ClearProjTimer;
  public:
                   SMaxIPTool         (QObject* parent = 0);
    virtual       ~SMaxIPTool();
  public slots:
    virtual void   selected           (SViewPort&);
    virtual void   deSelected         (SViewPort&);
    virtual void   leftClicked        (SViewPort&, int, int);
    virtual void   VPGone             ();
    virtual void   doProjections      (SViewPort& vp, bool force, bool fast);
    virtual void   clearProj          ();
    virtual void   remProject         (QObject*);
  };

  class SIMU_GUI_API SCurvePTool : public SMaxIPTool {
    Q_OBJECT
  protected:
    virtual void   doSliderValue      (int);
    virtual int    sliderMax          ();
    virtual int    sliderMin          ();
    virtual
    void           configureReduction ();
    SCoordinate::Precision
                   XP, YP;
    QWidget       *ToolBox;
    QCheckBox     *BothAxis;
    SGrapher      *CurveGraph;
    void           updateCurve();
  public:
                   SCurvePTool        (QObject* parent = 0);
    virtual       ~SCurvePTool();
  public slots:
    virtual void   selected           (SViewPort&);
    virtual void   deSelected         (SViewPort&);
    virtual void   leftClicked        (SViewPort&, int x, int y);
            void   updatePlane        ();
  };

  class SIMU_GUI_API SMinIPTool : public SMaxIPTool {
    Q_OBJECT
  protected:
    void           configureReduction ();
  public:
                   SMinIPTool(QObject* parent = 0);
    virtual       ~SMinIPTool();
  public slots:
    virtual void   selected           (SViewPort&);
    virtual void   deSelected         (SViewPort&);
  };

  class SIMU_GUI_API SAVGIPTool : public SMaxIPTool {
    Q_OBJECT
  protected:
    void           configureReduction ();
  public:
                   SAVGIPTool(QObject* parent = 0);
    virtual       ~SAVGIPTool();
  public slots:
    virtual void   selected           (SViewPort&);
    virtual void   deSelected         (SViewPort&);
  };

  class SIMU_GUI_API SOZIPTool : public SMaxIPTool {
    Q_OBJECT
  private:
    int            zeroLevel;
    SPoint         LightSource;
  protected:
    void           configureReduction   ();
    void           configureSecondSlider();
    QAbstractSlider
                  *SecondVPSlider;
    int            secondSliderMin      ();
    int            secondSliderMax      ();
  public:
                   SOZIPTool(QObject* parent = 0);
    virtual       ~SOZIPTool();
  public slots:
    virtual void   selected           (SViewPort&);
    virtual void   deSelected         (SViewPort&);
    virtual void   leftClicked        (SViewPort&, int x, int y);
            void   newLevelValue      (int);
    virtual void   VPGone             ();
    virtual void   viewportResized    (SViewPort&);
  };

  class SBucketHistogram;
  class SIMU_GUI_API SWLTool : public SViewPortTool {
    Q_OBJECT
  private:
    long long int  WLWidth, WLCentre, WLMax, WLStepSize;
    SCoordinate    NewCoord;
    BusyWidget    *BusyMaker;
    QCheckBox     *SkipEdges, *FullSource;
    QSpinBox      *AutoPercentile;
    QWidget       *ToolPanel;
    SGrapher      *GraphTool;
    static const
    float          GammaStep;
    void           doGammaeStep(SViewPort&, float step);
    SBucketHistogram
                  *HistAlg;
    SPool          BGJobs;
    void           bgHistGen(SSpace*);
    SSpace        *AddrCheck;
    std::string    NameCheck;
    bool           NoAdjust, DoAutoWL;
    bool           ShowHistTools;
    QComboBox     *LUTList;
    QToolButton   *ApplyLUT;
  public:
                   SWLTool       (QObject* parent = 0);
    virtual       ~SWLTool       ();
    static void    addLUT        (GPLUT::LUTGen_t, const QString &name);
    static bool    removeLUT     (const QString &name);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
    virtual void   middleClicked (SViewPort&, int x, int y);
    virtual void   rightClicked  (SViewPort&, int x, int y);
    virtual void   dragged       (SViewPort&, int x, int y);
    virtual void   doubleClicked (SViewPort&, int x, int y);
    virtual void   keyPressed    (SViewPort&, int key);
    virtual void   updateHist    (SSpace* addcheck);
    virtual void   doAutoWL      (SSpace*);
    virtual void   showHistTools (bool);
    virtual void   applyLUTChange(bool);
  signals:
    void           histReady     (SSpace*);
  };

  class SIMU_GUI_API SPanTool : public SViewPortTool {
    Q_OBJECT
  private:
    SCoordinate     Orig;
  public:
                   SPanTool      (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
    virtual void   dragged       (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SZoomTool : public SViewPortTool {
    Q_OBJECT
  private:
    SCoordinate     Orig;
    SCoordinate     OrigPos;
    int             ZoomFactor;
    bool            DoZoom;
    void            showZoom      (SViewPort&);
    const static
    int             MaxZoom;
  public:
                   SZoomTool     (QObject* parent = 0);
    void           doZoom        (SViewPort&, float scale,
                                  int centrex, int centrey,
                                  int atpx, int atpy, bool relative = true);
    float          currentZoom   (SViewPort &);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    void           setZoomFactor(int);
    virtual void   leftClicked   (SViewPort&, int x, int y);
    virtual void   rightClicked  (SViewPort&, int x, int y);
    virtual void   middleClicked (SViewPort&, int x, int y);
    virtual void   dragged       (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SFitStretchTool : public SViewPortTool {
    Q_OBJECT
  public:
                   SFitStretchTool (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SMirrorTool : public SViewPortTool {
    Q_OBJECT
  public:
                   SMirrorTool     (QObject* parent = 0);
  public slots:
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   leftClicked     (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SFlipTool : public SViewPortTool {
    Q_OBJECT
  public:
                   SFlipTool       (QObject* parent = 0);
  public slots:
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   leftClicked     (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API S1to1Tool : public SViewPortTool {
    Q_OBJECT
  public:
                   S1to1Tool       (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SOrthoRotateTool : public SViewPortTool {
    Q_OBJECT
  private:
    int OrthoState;
  public:
                   SOrthoRotateTool (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SChromaTool : public SViewPortTool {
    Q_OBJECT
  private:
    QColorDialog  *ColorChooser;
  public:
                   SChromaTool   (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SInvertTool : public SViewPortTool {
    Q_OBJECT
  public:
                   SInvertTool   (QObject* parent = 0);
  public slots:
    virtual void   selected      (SViewPort&);
    virtual void   deSelected    (SViewPort&);
    virtual void   leftClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SSliceTool : public SSliderTool {
    Q_OBJECT
  private:
    float          Scale;
    unsigned       CurrentSlice;
    unsigned       TmpSlice;
    bool           HandleMouseScroll;
    bool           DoSync;
    void           outputSlicePosition(SViewPort&,SSlicer&);
    bool           FlipDir;
    QList<QWidget*>
                   Projections;
    QWidget*       Options;
    QCheckBox*     ProjectionOpt;
    QCheckBox*     ProjectionForce;
    bool           ShowOptions;
  public:
                   SSliceTool     (QObject* parent = 0);
    virtual int    sliderMin      ();
    virtual int    sliderMax      ();
    virtual void   doSliderValue  (int );
  public slots:
     SSliceTool*   doMouseScroll  (bool);
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   middleClicked  (SViewPort&, int x, int y);
    virtual void   dragged        (SViewPort&, int x, int y);
    virtual void   wheelUp        (SViewPort&, int x, int y);
    virtual void   wheelDown      (SViewPort&, int x, int y);
    virtual void   wheelUp_f      (SViewPort&, int x, int y);
    virtual void   wheelDown_f    (SViewPort&, int x, int y);
    virtual void   viewportUpdated(SViewPort&);
    virtual void   doProjections  (SViewPort& vp, bool fast);
    virtual void   clearProj      ();
    virtual void   remProject     (QObject*);
    void           setFlipDir     (bool);
    void           showOptions    (bool);
  };

  class SIMU_GUI_API SVPProperties : public SViewPortTool {
    Q_OBJECT
  public:
                   SVPProperties  (QObject* parent = 0);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   rightClicked   (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SSnapShot : public SViewPortTool {
    Q_OBJECT
  public:
                   SSnapShot      (QObject* parent = 0);
    virtual       ~SSnapShot      ();
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   rightClicked   (SViewPort&, int x, int y);
    virtual void   doubleClicked  (SViewPort&, int x, int y);
  private slots:
    virtual void   storeScreenshot(const QString&);
  private:
    QPixmap        *Screenshot;
  };

  class SIMU_GUI_API SExport : public SViewPortTool {
    Q_OBJECT
  public:
                   SExport        (QObject* parent = 0);
    virtual       ~SExport        ();
    static bool    exportSSpace   (SSpace* src, const QString& path);
    static void    exportSSpace_noret
                                  (SSpace* src, const QString& path);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
  private slots:
    virtual void   doExport       (const QString&);
  private:
    SPool          bgExports;
    SSpace         StoreIMG;
  };

  class tagBrowser;
  class SIMU_GUI_API SInfoTool : public SViewPortTool {
    Q_OBJECT
  private:
    QWidget       *MainInfoWidget;
    tagBrowser    *MainTagBrowser;
    QLineEdit     *SearchEdit;
  public:
                   SInfoTool      (QObject* parent = 0);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   middleClicked  (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SPivotTool : public SViewPortTool {
    Q_OBJECT
  private:
    SVector        XAxisOrig, YAxisOrig, NormalOrig;
    SCoordinate    PointOrig;
    bool           RotateAboutY, RotateAboutBoth;
    SPOIObject    *PivotPoint;
    QList<QWidget*>
                   Projections;
  public:
                   SPivotTool     (QObject* parent = 0);
    virtual       ~SPivotTool     ();
    void           outputSlicePos (SViewPort&, SSlicer&);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   rightClicked   (SViewPort&, int x, int y);
    virtual void   middleClicked  (SViewPort&, int x, int y);
    virtual void   dragged        (SViewPort&, int x, int y);
    virtual void   buttonReleased (SViewPort&, int x, int y);
    virtual void   doProjections  (SViewPort& vp, bool force, bool fast);
    virtual void   clearProj      ();
    virtual void   remProject     (QObject*);
  };

  class SIMU_GUI_API SRotateTool : public SViewPortTool {
    Q_OBJECT
  private:
    SVector        XAxisOrig, YAxisOrig;
    SCoordinate    PointOrig;
    bool           DragEnabled;
  public:
                   SRotateTool    (QObject* parent = 0);
    virtual       ~SRotateTool    ();
    void           configVars     (SViewPort&, int x, int y);
    void           outputSlicePos (SViewPort&, SSlicer&);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   rightClicked   (SViewPort&, int x, int y);
    virtual void   middleClicked  (SViewPort&, int x, int y);
    virtual void   dragged        (SViewPort&, int x, int y);
    virtual void   buttonReleased (SViewPort&, int x, int y);
  };

  class SIMU_GUI_API SPointInfoTool : public SViewPortTool {
    Q_OBJECT
  private:
    QWidget       *MainInfoWidget;
    QLabel        *MainMessageWidget;
    QCheckBox     *SynchVPs, *ForceSyncVPs;
    QList<QWidget*>
                   Projections;
    SGrapher      *GraphWidget;
    bool           DoReleaseUpdate;
  private slots:
    void           showForceCheck(int);
    void           updateLocation(SViewPort &vp, int x, int y, bool fast);
  public:
                   SPointInfoTool (QObject* parent = 0);
    virtual       ~SPointInfoTool();
    static
    QString        genInfoString  (SViewPort&, int x, int y);
    static
    void           synchVPs       (SViewPort&, int x, int y,
                                   bool checkspaceids=true, bool rdepth = true,
                                   bool centerinvp = true);
    static
    QList<QWidget*>
                   showSliceProjections(SViewPort&primaryvp, bool force = false,
                                        bool fast=false);
    static
    QList<SPOIObject*>
                   showProjections(SViewPort&, int x, int y,
                                   bool checkspaceids=true,
                                   bool fast = false);
    static
    std::vector< std::pair<float,float> >
                   getTimePoints  (SViewPort&, int x, int y);
  public slots:
    virtual void   selected       (SViewPort&);
    virtual void   deSelected     (SViewPort&);
    virtual void   leftClicked    (SViewPort&, int x, int y);
    virtual void   buttonReleased (SViewPort&, int x, int y);
    virtual void   middleClicked  (SViewPort&, int x, int y);
    virtual void   dragged        (SViewPort&, int x, int y);
            void   removeProjP    (QObject*);
            void   clearProjs     ();
  };

  class SIMU_GUI_API STimeSliceTool : public SSliderTool {
    Q_OBJECT
  protected:
    SViewPort     *VPSelected;
    virtual void   doSliderValue   (int);
    virtual int    sliderMax       ();
    virtual int    sliderMin       ();
  public:
                   STimeSliceTool(QObject* parent = 0);
    virtual       ~STimeSliceTool();
  public slots:
    virtual void   leftClicked     (SViewPort&, int, int);
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   viewportUpdated (SViewPort&);
  };

  class SIMU_GUI_API SLineDrawWidget : public QWidget {
    Q_OBJECT
  private:
    bool     TB;
    int      Thickness;
    QColor   Color;
  protected:
    void     resizeEvent    ( QResizeEvent *);
    void     paintEvent     ( QPaintEvent  *);
  public:
             SLineDrawWidget(QWidget* parent, bool fast=false);
    virtual ~SLineDrawWidget();
  public slots:
    void     setTB          ();
    void     setBT          ();
    void     setThickness   (int);
    void     setColor       (QColor);
    void     setLine        (QPoint,QPoint);
    void     cleanup        (bool deep);
  };

  class SIMU_GUI_API SPOIObject : public QLabel {
    Q_OBJECT
  private:
    SSpace     *SourceImage;
    QString     SourceImageID;
    SPoint      Point;
    QLabel     *Label;
    bool        Editable;
    SPOIObject *Link;
    SLineDrawWidget
               *LinkLine;
    bool        WriteDistance;
    QInputDialog
               *LabelSet;
  protected:
    void   mousePressEvent       ( QMouseEvent *);
    void   mouseMoveEvent        ( QMouseEvent *);
    void   mouseReleaseEvent     ( QMouseEvent *);
    void   mouseDoubleClickEvent ( QMouseEvent *);
    void   moveEvent             ( QMoveEvent  *);
    void   init                  (SViewPort*, bool hq);
    void   writeDistance         (SPOIObject&, SPoint, float);
    void   writeDistance         ();
  public:
             SPOIObject  (SViewPort* parent, bool hq = true,
                          Qt::WindowFlags f = 0);
             SPOIObject  (SViewPort* parent, int vxx, int vyy, const QString&,
                          bool hq = true);
    virtual ~SPOIObject  ();
    SSpace*  source      () const;
    SPoint   sourcePoint () const;
    QString  label       () const;
    SPOIObject*
             link        () const;
  public slots:
    void     setSource   (SSpace &, const SPoint&);
    void     setPixmap   (const QPixmap&);
    void     setLabel    (const QString&);
    void     setLabelFr  (const QString&);
    void     setEditable (bool);
    void     refresh     ();
    void     moveTo      (int x, int y);
    void     link        (SPOIObject&);
    void     unlink      (QObject*);
    void     drawLink    ();
    void     setWriteDistance
                         (bool);
  signals:
    void     modified    (SPOIObject*);
    void     moved       (int x, int y);
    void     placed      (int x, int y);
  };
  
  class SIMU_GUI_API SMarkMeasure : public SViewPortTool {
    Q_OBJECT
  private:
    int            Counter;
    SPOIObject    *LastPOI;
  public:
                   SMarkMeasure(QObject* parent = 0);
    virtual       ~SMarkMeasure();
  public slots:
    virtual void   leftClicked     (SViewPort&, int, int);
    virtual void   middleClicked   (SViewPort&, int, int);
    virtual void   rightClicked    (SViewPort&, int, int);
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   clearLastPOI    (QObject*);
  };

  class SIMU_GUI_API SVPCloseTool : public SViewPortTool {
    Q_OBJECT
  public:
                   SVPCloseTool    (QObject* parent = 0);
    virtual       ~SVPCloseTool    ();
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   leftClicked     (SViewPort&, int, int);
  };

  class SIMU_GUI_API SReinterpret : public SViewPortTool {
    Q_OBJECT
  public:
                   SReinterpret    (QObject* parent = 0);
    virtual       ~SReinterpret    ();
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   leftClicked     (SViewPort&, int, int);
    virtual void   rightClicked    (SViewPort&, int, int);
  private:
    QWidget       *Toolbox;
    QLabel        *Message;
    QLineEdit     *ExtentBox;
  };

  class SIMU_GUI_API SFileDialog: public QFileDialog {
    Q_OBJECT
  protected:
    virtual
    void     showEvent   ( QShowEvent * event );
    void     resizeEvent ( QResizeEvent * event );
  public:
             SFileDialog(QWidget *parent, bool showfileinput=true,
                         Qt::WindowFlags = 0);
    virtual ~SFileDialog();
  public slots:
    void     setToScreenSize();
  };
  
}

#endif
