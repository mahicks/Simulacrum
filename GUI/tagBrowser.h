/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * Tag Browser for DICOM Objects
 * Author: M.A.Hicks
 */
#ifndef TAGBROWSER
#define TAGBROWSER

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QString>
#include <QList>
#include <QStringList>
#include <QThread>
#include <QMutex>

#include <GUI/definitions.h>
#include <Resources/DICOM/sdicom.h>
#include <Core/sresource.h>
#include <Core/definitions.h>
#include <Core/SPool/SPool.h>
#include "BusyWidget.h"

namespace Simulacrum {

  class SIMU_GUI_API tagBrowserWorker : public QThread {
    Q_OBJECT
  public:
    tagBrowserWorker();
    virtual ~tagBrowserWorker();
  private:
    bool stopNow;
  public slots:
    void safeStop();
    bool shouldStop();
    virtual void clear();
  signals:
    void doError(QString);
    void setBusy(bool);
    void setMessage(QString);
    void oscillateProgress();
    void showProgress(int, int);
    void refresh();
    void updateView();
  };

  class SIMU_GUI_API MultiFileHandler : public tagBrowserWorker {
    Q_OBJECT
  private:
    bool LoadPixelDataDefault;
    bool Recurse;
    QStringList
         FileList;
    void addFile(const QString& dicomfile);
    void doAddFiles();
  public:
    void run();
    void clear();
    void setParameters(const QStringList& filelist, bool recurse, bool pixdata);
  signals:
    void newResource(SResource*);
    void newTag(SAbsTreeNode*);
  };

  class SIMU_GUI_API STreeNodeItem : public QTreeWidgetItem {
  private:
    SAbsTreeNode &DataSource;
    static const unsigned
                  MaxDataLen = 4096;
    static const unsigned
                  MaxNameLen = 50;
    bool          Editable, SimpleView;
  public:
                  STreeNodeItem(STreeNodeItem* parent, SAbsTreeNode* source,
                                bool editable = false, bool simpleview = true);
    virtual      ~STreeNodeItem();
    void          setResourceURI(const QString&);
    void          refresh(bool deep = false);
    void          writeBack(int column);
    SAbsTreeNode& getNode();
    STreeNodeItem* getParentNodeItem();
    void          deleteNode();
    STreeNodeItem*
                  addChildNode();
    bool          isResourceItem();
  };

  class SIMU_GUI_API GenerateTreeItems : public tagBrowserWorker {
    Q_OBJECT
  private:
    SAbsTreeNodeList_t genTags;
    void generateTags(const SAbsTreeNodeList_t &ltagList,
                      STreeNodeItem *itemparent);
    bool               Editable;
    bool               SimpleView;
  public:
    void setParameters(const SAbsTreeNodeList_t, bool editable, bool simp=true);
    void run();
  signals:
    void newItem(QTreeWidgetItem*);
    void newItems(QList<QTreeWidgetItem*>*, bool cleanup);
    void configureView();
  };

  enum ColumnNames {NamePosition,
                    TagPosition,
                    TypePosition,
                    LengthPosition,
                    DataPosition,
                    ResourcePosition};
  static const unsigned
                    TBColumnNum = 6;

  class SIMU_GUI_API tagBrowser : public QTreeWidget {
    Q_OBJECT
  private:
    SAbsTreeNodeList_t        nodeList;
    QMutex                    nodeListLock;
    SAbsTreeNodeList_t        nodeListPending;
    QMutex                    nodeListPendingLock;
    typedef         std::map<std::string,SResource*> sresourcemap_t;
    bool            simpleView;
    static const bool
                    LoadPixelDataDefault = false;
    QString         ActiveFilter;
    sresourcemap_t
                    SResources;
    QMutex          SResourcesLock;
    SPool           GeneralPool;
    void            addTags(SAbsTreeNodeList_t&);
    BusyWidget      busyMaker;
    bool            multiResource;
    bool            filterItem(QTreeWidgetItem*, const QString&,
                                 bool recurse = false);
    MultiFileHandler
                    AddFilesThread;
    GenerateTreeItems
                    AddTagsThread;
    void            waitOnThreads();
    bool            Editable;
  protected:
    void            resizeEvent(QResizeEvent *);
  public:
                    tagBrowser(QWidget *parent = 0);
    virtual        ~tagBrowser();
    void            setDropTarget(bool);
    /* Qt Methods */
    QSize           sizeHint () const;
    bool            dropMimeData(QTreeWidgetItem *parent, int index,
                                 const QMimeData *data, Qt::DropAction action);
    QStringList     mimeTypes() const;
    bool            hasResource(const QString&);
    QTreeWidgetItem*getResourceItem(QTreeWidgetItem *item);
    QString         getResource(QTreeWidgetItem *item);
    SResource&      getResource(const QString&);
    std::string     getPathRaw(QTreeWidgetItem *item);
    QString         getPath(QTreeWidgetItem *item);
    sresourcemap_t  getResources();
    QStringList     getSelectedResources();
    QList<STreeNodeItem*>
                    getSelectedNodeItems();
    bool            isBusy();
    void            wait();
    bool            isEditable();
  public slots:
    void            addFiles(const QStringList&, bool recurse = true);
    void            addFilesRemoveExisting(const QStringList&,
                                           bool recurse = true);
    void            doError(const QString&);
    void            addTopLevelTag(SAbsTreeNode*);
    void            removeTopLevelTag(SAbsTreeNode*, bool stoponfirst=false,
                              bool dorefresh=true);
    void            addResource(SResource*);
    void            addResourceFull(SResource*);
    void            addResourceSafe(SResource*);
    void            addResourceFullSafe(SResource*);
    void            removeResource(SResource*, bool dorefresh = true);
    void            removeCurrentResource();
    void            addItem(QTreeWidgetItem*);
    void            addItems(QList<QTreeWidgetItem*>*, bool cleanup = false);
    void            newFilterString(const QString&);
    void            setSimple(bool);
    void            setMultiResource(bool);
    bool            isMultiResource();
    void            updateView();
    void            refresh(bool deep = false);
    void            saveCurrent();
    void            saveCurrentAs(const QString&);
    void            saveAll();
    void            configureView();
    void            clear();
    void            resizeColumns(bool todata = false);
    void            setSortable(bool);
    void            stopActions();
    void            itemModified(QTreeWidgetItem * item,int column);
    void            setEditable(bool);
    void            friendlyCollapseAll();
    void            friendlyExpandAll();
    void            copyNodesToClipboard();
    void            copyNodeValuesToClipboard();
    void            setBusyOpacity(float);
  signals:
    void            updated();
  };

}
#endif
