/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:
 * A render area for Simulacrum Images in Qt
 * Author: M.A.Hicks
 */
#ifndef SVIEWPORT
#define SVIEWPORT

#include <QThread>
#include <QAction>
#include <QToolBar>
#include <QToolButton>
#include <QTimer>
#include <QElapsedTimer>
#include <QFrame>

class QWidget;
class QLabel;
class QTimer;
class QCursor;
class QImage;
class QColor;
class QGridLayout;
class QString;

#include <functional>

#include <GUI/definitions.h>
#include <Core/sspace.h>
#include <Core/definitions.h>
#include <Core/slicer/slicer.h>
#include <Core/SPool/SPool.h>
#include "BusyWidget.h"
#include "qconnectable.h"

namespace Simulacrum {

/*******************************************************************************
 *                              SRenderSurface                                 *
 *                  A surface to which the image is drawn                      *
 ******************************************************************************/

  class SIMU_GUI_API BGSSpaceToQImage : public QThread {
    Q_OBJECT
  private:
    SSpace        *SourceSpace;
    unsigned short Downsample, PerfDown;
    bool           DoPerfoDown, DoMerge;
    float          MergeOpac;
    SCoordinate    CropE, CropB;
    uint64_t       t1,t2,t3,TargetRate;
    QElapsedTimer  rtimer;
  public:
             BGSSpaceToQImage(QObject* parent = 0);
    virtual ~BGSSpaceToQImage();
    void     updatePerfDown(uint64_t);
    void     enablePerfDown(bool);
    uint64_t perfDown();
    bool     perfDownEnabled();
    void     genRGBAFrom(SSpace& source, unsigned short downsample = 0,
                         bool mergecontexts = false, float mergeopacity = 0.3,
                         SCoordinate *cropb = nullptr, SCoordinate *crope = nullptr);
    void     run();
  signals:
    void     newQImage(SSpace*);
  };

  class SIMU_GUI_API SRenderSurface {
  public:
    virtual 
          ~SRenderSurface();
    virtual
    void   drawSImage(SSpace&)=0;
    virtual
    SVector
           coordToSSpace(const SCoordinate& redcoord)=0;
    virtual
    SVector
           sspaceToCoord(const SVector& sspacecoord)=0;
    virtual
    void   setResampleTime(int)=0;
    virtual
    void   enableDownsample(bool)=0;
    virtual
    bool   perfDownEnabled()=0;
    virtual
    void   enablePerfDownsample(bool)=0;
    static
    SCoordinate
           calcSpaciallyDeformedExtent(SSpace&);
    virtual
    bool   spatiallyDeform()=0;
    virtual
    void   enableSpatialDeform(bool)=0;
    virtual
    bool   contextOverlays()=0;
    virtual
    void   enableContextOverlays(bool)=0;
    virtual
    void   setOverlayOpacity(float)=0;
    virtual
    void   updateSignal(bool)=0;
  };

  class SIMU_GUI_API SRenderSurfaceBasic : public QWidget, public SRenderSurface {
    Q_OBJECT
  private:
    SSpace             *SourceSpace;
    SSpace             *RGBAImage;
    BGSSpaceToQImage    BGRenderer;
    SLockable           DrawLock;
    QTimer              ResampleTimer;
    bool                DoDownsample;
    bool                DoSpacialDeform;
    QWidget            *SurfaceWidget;
    QCursor             RestoreCursor;
    QSize               DrawSize;
    bool                ContextOverlays;
    float               ContextOverlaysOpacity;
  protected:
    QSize  drawSize();
    QRect  visibleSurface();
    void   callRender (short downsample);
    void   paintEvent (QPaintEvent*);
    void   resizeEvent(QResizeEvent*);
    void   moveEvent  (QMoveEvent *);
    void   showEvent  (QShowEvent *);
  public:
           SRenderSurfaceBasic(QWidget* parent = 0);
          ~SRenderSurfaceBasic();
    unsigned
    short  downsampleScale(SSpace&);
    void   drawSImage(SSpace&);
    
    SVector
           coordToSSpace(const SCoordinate& redcoord);
    SVector
           sspaceToCoord(const SVector& sspacecoord);
    bool   perfDownEnabled();
    bool   spatiallyDeform();
    bool   contextOverlays();
  public slots:
    void   setResampleTime(int);
    void   enableDownsample(bool);
    void   enablePerfDownsample(bool);
    void   enableSpatialDeform(bool);
    void   resample();
    void   enableContextOverlays(bool);
    void   setOverlayOpacity(float);
  protected slots:
    void   newImage(SSpace*);
  signals:
    void   updateSignal(bool);
  };

/*******************************************************************************
 *                               SViewPortHUD                                  *
 *                       HeadsUpDisplay for the ViewPort                       *
 ******************************************************************************/

  class SIMU_GUI_API SViewPortHUD : public QWidget {
    Q_OBJECT
    friend class SViewPort;
  private:
    bool            Active;
    QColor          BaseColor,ActiveColor;
    static
    const unsigned  ActiveBorderWidth = 2;
    static
    const unsigned  ShadowRadius = 5;
    static
    const unsigned  ShadowOffset = 0;
    static
    const unsigned  LabelColumns = 3;
    static
    const unsigned  LabelRows    = 3;
    QColor          TextColour;
    QList<QLabel*>  OverlayLabels;
  protected:
    void   paintEvent(QPaintEvent*);
    void   resizeEvent(QResizeEvent*);
    void   layoutLabels();
  public:
           SViewPortHUD(QWidget* parent = 0);
    virtual
          ~SViewPortHUD();
    void   clear();
    bool   isActive();
    void   setActive(bool);
    const  static int ARBINFOPOS = 8;

  public slots:
    void   setTextColor(const QString&);
    void   setText(int position, const QString& text);

  };

/*******************************************************************************
 *                               SViewPort                                     *
 *                       The main viewport area                                *
 ******************************************************************************/
  class SDynamicLayout;
  class SIMU_GUI_API SViewPort : public QFrame, public SQConnectable {
    Q_OBJECT
  private:
    void            initLabels(SSpace&);
    static
    const unsigned  BorderWidth;
    bool            ScaleFit;
    float           ScaleFitZoom;
    QString         VPName;
    bool            DoEmitMessage;
    SSpace          NullSpace;
    SCoordinate
                    coordToRenderSurface(const SCoordinate&);
  protected:
    SRenderSurface *RenderSurface;
    QWidget        *RenderSurfaceWidget;
    SSpace         *SourceImage;
    SViewPortHUD   *HUD;
    QColor          BGColor;
    BusyWidget     *BusyDrawer;
    SSpace          initIMG;
    QWidget        *TW,*BW,*LW,*RW;
    QTimer         *ClearMessageTimer;
    void   resizeEvent           ( QResizeEvent*);
    void   paintEvent            ( QPaintEvent *);
    void   mousePressEvent       ( QMouseEvent *);
    void   mouseMoveEvent        ( QMouseEvent *);
    void   mouseDoubleClickEvent ( QMouseEvent *);
    void   mouseReleaseEvent     ( QMouseEvent *);
    void   keyPressEvent         ( QKeyEvent   *);
    void   keyReleaseEvent       ( QKeyEvent   *);
    void   focusInEvent          ( QFocusEvent *);
    void   focusOutEvent         ( QFocusEvent *);
    void   wheelEvent            ( QWheelEvent *);
    void   dragEnterEvent        ( QDragEnterEvent *);
    void   dropEvent             ( QDropEvent  * );
    void   showEvent             ( QShowEvent * );
  public:
           SViewPort(QWidget* parent = 0);
    virtual
          ~SViewPort();
    bool   isActive();
    bool   isHUDVisible();
    void   setActive(bool);
    SSpace*
           sourceSSpace();
    SSpace*
           endSourceSSpace();
    SSpace*
           penumSourceSSpace();
    SRenderSurface&
           renderSurface();
    QWidget&
           renderSurfaceWidget();
    SVector
           coordToSourceSSpace(const SCoordinate& reqcoord);
    SVector
           coordToEndSourceSSpace(const SCoordinate& reqcoord);
    SVector
           sourceSSpaceToCoord(const SVector& sspacecoord);
    void   centerOnSourceCoord(const SCoordinate& reqcoord,
                               bool forcecenter=false);
    bool
           endSourceSSpaceToCoord(const SVector& sourcecoord, SVector &result);
    const QString&
           name();
    bool   emitMessage();
    SDynamicLayout*
           layout(bool recurse = true);
    QColor activeColor();
    bool   scaleFit      ();
    float  scaleZoom     ();
  public slots:
#ifdef MOBILE
    void   setEdgeWidget (QWidget*, int position=0, bool restyle=false);
#else
    void   setEdgeWidget (QWidget*, int position=0, bool restyle=true);
#endif
    void   updateWidgets ();
    void   showHUD       (bool);
    void   setHUDText    (int position, const QString& text);
    void   showMessage   (const QString& = "");
    void   setWaiting    (bool=true);
    void   setProgress   (int);
    void   showSImage    (SSpace&);
    virtual
    void   sconnect_end  (SConnectable&);
    virtual
    void   sconnect      (SQConnectable&);
    virtual
    void   sconnect      (SConnectable&);
    virtual
    void   sdisconnect   (SQConnectable&);
    virtual
    void   sdisconnect   (SConnectable*);
    virtual 
    void   sconfigure(const QString&);
    virtual
    void   refresh       (bool);
    void   clear         ();
    virtual
    void   setScaleFit   (bool);
    void   setScaleZoom  (float);
    void   setName       (const QString&);
    void   setEmitMessage(bool);
  signals:
    void   leftClicked   (SViewPort&, int x, int y);
    void   doubleClicked (SViewPort&, int x, int y);
    void   middleClicked (SViewPort&, int x, int y);
    void   rightClicked  (SViewPort&, int x, int y);
    void   buttonReleased(SViewPort&, int x, int y);
    void   keyPressed    (SViewPort&, int key);
    void   keyReleased   (SViewPort&, int key);
    void   dragged       (SViewPort&, int x, int y);
    void   wheelUp       (SViewPort&, int x, int y);
    void   wheelDown     (SViewPort&, int x, int y);
    void   activated     (SViewPort&);
    void   deActivated   (SViewPort&);
    void   active        (bool);
    void   progress      (int);
    void   updated       (bool);
    void   resized       (SViewPort&);
    void   message       (QString);
  };

/*******************************************************************************
 *                          SDICOMViewPort                                     *
 *                  A Special Viewport for DICOM Files                         *
 ******************************************************************************/

  typedef QList< QList< QString > > SDCMViewPortLabelProfile_t;

  class SIMU_GUI_API SDCMViewPort : public SViewPort {
    Q_OBJECT
  private:
    SDCMViewPortLabelProfile_t LabelProfile;
  public:
             SDCMViewPort(QWidget* parent = 0);
    virtual ~SDCMViewPort();
  public slots:
    void     initTagsDisplay(NNode&);
    virtual
    void     sconnect(SQConnectable&);
    virtual
    void     sconnect(SConnectable&);
    virtual
    void     sconnect(SConnectable&,bool);
    void     setLabelProfile(const SDCMViewPortLabelProfile_t&);
    void     clear();
    void     refresh(bool);
  };

/*******************************************************************************
 *                          SViewPortEventHandler                              *
 *                  A an event handler for ViewPorts and Tools                 *
 ******************************************************************************/

  class SIMU_GUI_API SViewPortTool : public QAction {
    Q_OBJECT
  private:
    void           doNullWidget(QWidget*, QString);
    QWidget        Holder;
  protected:
    SCoordinate    ReferencePoint;
    std::function<void(QWidget*,QString)>
                   AdditionalWidget;
    void           AdditionalWidgetHide(QWidget*);
  public:
                   SViewPortTool   (QObject* parent=nullptr);
    virtual       ~SViewPortTool();
    void           connectViewPort(SViewPort&);
    virtual
    bool           oneShot();
    void           setAdditionalWidgetF(std::function<void(QWidget*,QString)>);
  public slots:
    virtual void   selected        (SViewPort&);
    virtual void   deSelected      (SViewPort&);
    virtual void   leftClicked     (SViewPort&, int x, int y);
    virtual void   doubleClicked   (SViewPort&, int x, int y);
    virtual void   middleClicked   (SViewPort&, int x, int y);
    virtual void   rightClicked    (SViewPort&, int x, int y);
    virtual void   buttonReleased  (SViewPort&, int x, int y);
    virtual void   dragged         (SViewPort&, int x, int y);
    virtual void   wheelUp         (SViewPort&, int x, int y);
    virtual void   wheelDown       (SViewPort&, int x, int y);
    virtual void   keyPressed      (SViewPort&, int key);
    virtual void   keyReleased     (SViewPort&, int key);
    virtual void   viewportResized (SViewPort&);
    virtual void   viewportUpdated (SViewPort&);
  };

  class SIMU_GUI_API SViewPortEventHandler : public QToolBar {
    Q_OBJECT
  private:
    SViewPortTool *ActiveTool;
    SViewPort     *ActiveViewPort;
    SViewPort      SafetyViewPort;
  public:
                   SViewPortEventHandler(QWidget* parent = 0);
    virtual       ~SViewPortEventHandler();
  public slots:
    void           setActiveViewPort   (SViewPort&);
    void           clearActiveViewPort (QObject*);
    void           addSVPTool          (SViewPortTool*);
    void           connectViewPort     (SViewPort&);
    void           newActiveTool       ();
    void           leftClicked         (SViewPort&, int x, int y);
    void           doubleClicked       (SViewPort&, int x, int y);
    void           middleClicked       (SViewPort&, int x, int y);
    void           rightClicked        (SViewPort&, int x, int y);
    void           buttonReleased      (SViewPort&, int x, int y);
    void           dragged             (SViewPort&, int x, int y);
    void           wheelUp             (SViewPort&, int x, int y);
    void           wheelDown           (SViewPort&, int x, int y);
    void           keyPressed          (SViewPort&, int key);
    void           keyReleased         (SViewPort&, int key);
    void           viewportResized     (SViewPort&);
    void           viewportUpdated     (bool);
  protected slots:
    void           makeNewWidget       (QWidget*, const QString title);
  signals:
    void           newWidget           (QWidget*, const QString title);
  };

}
#endif //SVIEWPORT
