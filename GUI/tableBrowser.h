/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::FileIO:
 * TableBrowser
 * Author: M.A.Hicks
 */
#ifndef TABLEBROWSER
#define TABLEBROWSER

#include <QTableWidgetItem>
#include <QTableWidget>
#include <QList>
#include <QString>

class QLabel;
class QBoxLayout;
class QVBoxLayout;
class QSplitter;

#include <GUI/definitions.h>
#include <Core/sabtree.h>
#include <Core/SPool/SPool.h>
#include <Resources/DICOM/datadic.h>
#include <Resources/DICOM/dcmtag.h>
#include <GUI/BusyWidget.h>

namespace Simulacrum {

  class SIMU_GUI_API DICOMDictTool: public QWidget {
    Q_OBJECT
  private:
    QLineEdit   *TagID, *TagVR, *TagName;
    QPushButton *acceptButton;
    DCMDataDic   DCMDictionary;
    void       configure(const dcm_info_store&);
  public:
             DICOMDictTool(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~DICOMDictTool();
    QString  ID();
    QString  VR();
    QString  Name();
    void     fromTag(DCMTag&);
    DCMTag   toTag();
    virtual QSize
             minimumSizeHint () const;
  public slots:
    void     updateBasedonID  (QString);
    void     updateBasedonName(QString);
    void     done             ();
    void     showAcceptButton (bool);
  signals:
    void     contentsChanged();
    void     selectedID(QString);
  };

  class SIMU_GUI_API SAbsTreeTableItem : public QTableWidgetItem {
  private:
    SAbsTreeNode &BaseNode;
    QString       AttributeName;
  public:
                  SAbsTreeTableItem(SAbsTreeNode&);
                  SAbsTreeTableItem(SAbsTreeNode&,const QString &attribute);
    SAbsTreeNode& getNode();
    void          refresh();
    void          configure();
  };

  class SIMU_GUI_API STableBrowser : public QTableWidget {
    Q_OBJECT
  private:
    QList<SAbsTreeNode*> BaseNodes;
    QList<QString>       AttributeList;
    SAbsTreeTableItem*   genItemAt(int row,int column);
    void                 genItems();
    bool                 EditMode;
    bool                 DoFriendlyNames;
    QString              getFriendlyString(const std::string&);
    DCMDataDic           InternalDataDictionary;
    DCMDataDic*          DataDictionary;
    void                 setColumnHeaders();
    bool                 AutoSelect;
    BusyWidget           BusyMaker;
    SPool                BGJobs;
    SLockable            NodesLock;
    static const int     BusyThreshold = 20;
    QString              FilterStringCache;
  protected:
    void                 resizeEvent (QResizeEvent * event );
  public:
                         STableBrowser(QWidget* parent = 0);
    virtual             ~STableBrowser();
    QList<SAbsTreeNode*> getSelectedNodes();
    void                 refresh();
    bool                 isEditable();
    void                 setAutoSelect(bool);
    bool                 busy();
    void                 wait();
  public slots:
    void                 setParentNodes(QList<SAbsTreeNode*>);
    void                 setBaseNodes(QList<SAbsTreeNode*>);
    void                 addAttribute(QString);
    void                 addNode(SAbsTreeNode&);
    void                 removeAttribute(QString);
    void                 removeAttribute(int column);
    void                 clear(bool clearsource = true);
    void                 configure();
    void                 setRecommendedLayout();
    void                 setEditable(bool);
    void                 setFriendlyNames(bool);
    void                 setDataDictionary(DCMDataDic*);
    bool                 applyFilterString(const QString&, bool force=true);
    void                 copySelectionToClipboard();
  private slots:
    void                 recalculateOutput();
    void                 __newTableData(QList< QList<SAbsTreeTableItem*> >*);
  signals:
    void                 _newTableData(QList< QList<SAbsTreeTableItem*> >*);
    void                 newSelectedNodes(QList<SAbsTreeNode*>);
    void                 stateChanged();
  };

  class SIMU_GUI_API STableBrowserLabelled : public QWidget {
    Q_OBJECT
  private:
    STableBrowser       *MainBrowser;
    QVBoxLayout         *MainLayout;
    QLabel              *MainLabel;
    QString              MainLabelText;
    int                  OrigHeight;
  public:
                         STableBrowserLabelled
                                   (QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual             ~STableBrowserLabelled();
    STableBrowser&       browser();
    void                 setLabel(const QString&);
    virtual
    QString              infoString();
  public slots:
    void                 toggleVisible ();
    void                 browserUpdated();
  };

  class SIMU_GUI_API STableBrowserStack : public QWidget {
    Q_OBJECT
  private:
    bool                 Vertical;
    QSplitter           *TableStack;
    QVBoxLayout         *MainLayout;
    QList<STableBrowserLabelled*>
                         TableList;
    QString              FilterCache;
  public:
                         STableBrowserStack
                         (bool vertical = true, QWidget* parent = 0,
                          Qt::WindowFlags f = 0);
    virtual             ~STableBrowserStack();
    QList<QString>       getSelectedPaths(bool skiptop = true);
    QList<std::string>   getSelectedPathsRaw(bool skiptop = true);
    QString              getSelectedPath(bool skiptop = true);
    std::string          getSelectedPathRaw(bool skiptop = true);
    unsigned             getDepth();
    STableBrowser&       getTableBrowser(int level);
    bool                 busy();
    void                 wait();
  public slots:
    void                 setDepth(unsigned);
    void                 setLabel(unsigned level, QString label);
    void                 setAttributes(unsigned level, QStringList attributes);
    void                 setParentNodes(QList<SAbsTreeNode*>);
    void                 applyFilterString(const QString&);
    void                 clear();
    void                 newSelectionHandler();
  signals:
    void                 newPathSelection(std::string);
    void                 selectionChanged();
  };

  class SIMU_GUI_API SResourceTableBrowser : public QWidget {
    Q_OBJECT
  private:
    QLayout             *MainLayout;
    STableBrowserStack  *TableStack;
    QList<QString>       stripFrontPath(QList<QString>&);
  protected:
    unsigned             Depth;
    SResource           *Resource;
    virtual
    SAbsTreeNode*        rootNode();
  public:
                         SResourceTableBrowser
                                   (QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual             ~SResourceTableBrowser();
    STableBrowserStack&  tableStack();
    SResource*           resource();
    QList<QString>       getSelectedPaths(bool skiptop = true);
    QList<std::string>   getSelectedPathsRaw(bool skiptop = true);
    QString              getSelectedPath(bool skiptop = true);
    std::string          getSelectedPathRaw(bool skiptop = true);
  public slots:
    void                 setDepth(unsigned);
    virtual
    void                 refresh();
    void                 setResource(SResource&);
    void                 clearResource();
    void                 newPathSelection(std::string);
    void                 doOpenResourcePathSelection();
  signals:
    void                 newResourcePathSelection(SResource&,std::string);
    void                 openResourcePathSelection(SResource&,std::string);
  };

  class SIMU_GUI_API SDCMResourceTableBrowser : public SResourceTableBrowser {
    Q_OBJECT
  private:
    void                 doDICOMConfig(const std::string &path);
  protected:
    SAbsTreeNode*        rootNode();
  public:
                         SDCMResourceTableBrowser
                                    (QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual             ~SDCMResourceTableBrowser();
    void                 refresh();
  };

}

#endif //TABLEBROWSER
