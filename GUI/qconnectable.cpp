/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Simulacrum::Connectable
 * A partial implementation to allow components to be 'connected'
 * M. A. Hicks
 */

#include "qconnectable.h"

#include <iostream>

using namespace Simulacrum;

SConnectableWrapper::SConnectableWrapper(SQConnectable& targ):
                                            SConnectable(), SignalTarget(targ) {

}

bool SConnectableWrapper::sconnect(SConnectable& newtarg) {
  SignalTarget.sconnect(newtarg);
  return true;
}

bool SConnectableWrapper::sconnect_(SConnectable& newtarg) {
  return SConnectable::sconnect(newtarg);
}

bool SConnectableWrapper::sdisconnect(SConnectable& targ) {
  bool diddiscon = SConnectable::sdisconnect(targ);
  if (diddiscon)
    emit propdisconnect(&targ);
  return diddiscon;
}

void SConnectableWrapper::refresh(bool deep) {
  emit propupdate(deep);
}

void SConnectableWrapper::signal(unsigned int type, void* object) {
  SignalTarget.ssignal(type,object);
}

void SConnectableWrapper::progress(int prog) {
  if (prog < 0)
    emit propwaiting(true);
  else {
    if (prog > 99)
      emit propwaiting(false);
    if (prog <= 100)
      emit progupdate(prog);
  }
}

SQConnectable::SQConnectable() : CoreReceiver(*this) {

}

SQConnectable::~SQConnectable() {

}

void SQConnectable::acceptCoreSignals() {
  QObject *starget = dynamic_cast<QObject*>(this);
  QObject::connect(&CoreReceiver,SIGNAL(propupdate(bool)),
                   starget,SLOT(refresh(bool)));
  QObject::connect(&CoreReceiver,SIGNAL(progupdate(int)),
                   starget,SLOT(setProgress(int)));
  QObject::connect(&CoreReceiver,SIGNAL(propdisconnect(SConnectable*)),
                   starget,SLOT(sdisconnect(SConnectable*)));
  QObject::connect(&CoreReceiver,SIGNAL(propwaiting(bool)),
                   starget,SLOT(setWaiting(bool)));
}

void SQConnectable::sconfigure(const QString&) {

}

SConnectableWrapper& SQConnectable::core() {
  return CoreReceiver;
}

void SQConnectable::refresh(bool rval) {
  emit updated(rval);
}

void SQConnectable::sconnect(SQConnectable& stargetp) {
  QObject *starget = dynamic_cast<QObject*>(&stargetp);
  QObject *thiss   = dynamic_cast<QObject*>(this);
  QObject::connect(starget,SIGNAL(active(bool)),thiss,SLOT(setWaiting(bool)));
  QObject::connect(starget,SIGNAL(progress(int)),thiss,SLOT(setProgress(int)));
  QObject::connect(starget,SIGNAL(updated(bool)),thiss,SLOT(refresh(bool)));
}

void SQConnectable::sdisconnect(SQConnectable& stargetp) {
  QObject *starget = dynamic_cast<QObject*>(&stargetp);
  QObject *thiss   = dynamic_cast<QObject*>(this);
  QObject::disconnect(starget,SIGNAL(active(bool)),thiss,
                                                        SLOT(setWaiting(bool)));
  QObject::disconnect(starget,SIGNAL(progress(int)),thiss,
                                                        SLOT(setProgress(int)));
  QObject::disconnect(starget,SIGNAL(updated(bool)),thiss,SLOT(refresh(bool)));
}

void SQConnectable::sconnect(SConnectable& starget) {
  CoreReceiver.sconnect_(starget);
}

void SQConnectable::sconnect(SConnectable& starget, bool dodel) {
  sconnect(starget);
  starget.deleteOnDisconnect(dodel);
}

void SQConnectable::sdisconnect(SConnectable* starget) {
  if (CoreReceiver.isConnected(starget))
    CoreReceiver.sdisconnect(*starget);
}

void SQConnectable::setWaiting(bool) {
  //just a stub
}

void SQConnectable::setProgress(int nprog) {
  emit progress(nprog);
}

void SQConnectable::ssignal(unsigned int , void* ) {

}
