/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:Grapher
 * A simple graphing drawer and widget
 * Author: M.A.Hicks
 */

#include "grapher.h"
#include <Core/sprimitives.h>

#include <QPainter>
#include <QFont>
#include <QMouseEvent>
#include <QToolTip>
#include <iostream>
#include <limits>

using namespace Simulacrum;

const int SGrapher::LabelBorder      = 50;
const int SGrapher::LabelMeterLength = 10;
const int SGrapher::LabelTextRatio   = 3;

SGrapher::SGrapher(QWidget* parent, Qt::WindowFlags f): QWidget(parent, f) {
  setMouseTracking(true);
  clear();
}

SGrapher::~SGrapher() {

}

int SGrapher::addDataSet(const SDataSet& newset,
                         SGrapherMode_t newmode,
                         bool doupdate) {
  DataSets.push_back(newset);
  DataSetModes.push_back(newmode);
  if (doupdate)
    drawGraph();
  return (DataSets.size() - 1);
}

void SGrapher::removeDataSet(int remdat, bool doupdate) {
  if ((unsigned)remdat < DataSets.size()) {
    DataSets.erase(DataSets.begin() + remdat);
    DataSetModes.erase(DataSetModes.begin() + remdat);
    if (doupdate)
      drawGraph();
  }
}

int SGrapher::dataSets() {
  return (int)DataSets.size();
}

void SGrapher::setDataSetColors(const std::vector< QColor >& newcolours) {
  DataSetColours = newcolours;
}

void SGrapher::setBGColor(QColor newbg) {
  BGColour = newbg;
}

void SGrapher::setShowLabels(bool doshow) {
  Labels = doshow;
}

bool SGrapher::showLabels() {
  return Labels;
}

void SGrapher::setLabelColor(QColor newcol) {
  LabelColour = newcol;
}

void SGrapher::setDrawGrid(bool ngrid) {
  DrawGrid = ngrid;
}

bool SGrapher::drawGrid() {
  return DrawGrid;
}

int SGrapher::sigFigs() {
  return SigFigs;
}

void SGrapher::setSigFigs(int newsig) {
  SigFigs = newsig;
}

void SGrapher::clear() {
  setMinimumSize(200,200);
  DataSets.clear();
  DataSetModes.clear();
  Labels          = false;
  LabelDensity    = 30;
  DrawGrid        = false;
  Border          = 10;
  PointSize       = 4;
  LineThickness   = 1;
  XScale = YScale = 1.0;
  XMin = YMin     = 0.0;
  SigFigs         = 2;
  DataSetColours.clear();
  DataSetColours.resize(5);
  DataSetColours[0] = QColor(230,167,0);
  DataSetColours[1] = Qt::blue;
  DataSetColours[2] = Qt::green;
  DataSetColours[3] = Qt::red;
  DataSetColours[4] = Qt::black;
  BGColour = QColor(0,0,0,0);
  drawGraph();
}

const QPixmap& SGrapher::pixmap() {
  return GraphSurface;
}

void SGrapher::resizeEvent(QResizeEvent* event) {
  drawGraph();
  QWidget::resizeEvent(event);
}

void SGrapher::paintEvent(QPaintEvent* event) {
  QPainter graphdrawer(this);
  graphdrawer.setRenderHint(QPainter::SmoothPixmapTransform);
  graphdrawer.drawPixmap(QPoint(0,0),pixmap());
  QWidget::paintEvent(event);
}

void SGrapher::mouseMoveEvent(QMouseEvent* event) {
  if (event->pos().x() > effectiveBorder() &&
      event->pos().y() > effectiveBorder() &&
      event->pos().x() < (width() - effectiveBorder()) &&
      event->pos().y() < (height() - effectiveBorder()) ) {
    SVector realpos = unmapPoint(event->pos().x(),
                                GraphSurface.height() - event->pos().y());
    QToolTip::showText(event->globalPos(),
                        QString::number( realpos.x() ) + ", " +
                        QString::number( realpos.y() ),
                        this, rect() );
  }
  QWidget::mouseMoveEvent(event);
}

void SGrapher::setScales() {
  float XX, YY, XM, YM;
  XX = YY = 0.0;
  XM = YM = std::numeric_limits<float>::max();
  // find max vals
  for (unsigned dset=0; dset < DataSets.size(); dset++) {
    for (unsigned dpair=0; dpair < DataSets[dset].size(); dpair++) {
      if (DataSets[dset][dpair].first > XX)
        XX = DataSets[dset][dpair].first;
      if (DataSets[dset][dpair].second > YY)
        YY = DataSets[dset][dpair].second;
      if (DataSets[dset][dpair].first < XM)
        XM = DataSets[dset][dpair].first;
      if (DataSets[dset][dpair].second < YM)
        YM = DataSets[dset][dpair].second;
    }
  }
  if ( XX == 0.0)
    XX = 1.0;
  if ( YY == 0.0)
    YY = 1.0;
  if ( XM == std::numeric_limits<float>::max())
    XM = 0.0;
  if ( YM == std::numeric_limits<float>::max())
    YM = 0.0;
  if (width() > 0 && height() > 0) {
    XScale = (XX - XM) / (width()-(effectiveBorder()*2));
    YScale = (YY - YM) / (height()-(effectiveBorder()*2));
  }
  else {
    XScale = 1.0;
    YScale = 1.0;
  }
  XMin = XM;
  YMin = YM;
}

QPoint SGrapher::mapPoint(float xx, float yy) {
  QPoint retval;
  retval.setX(( (xx - XMin) / XScale)+effectiveBorder());
  retval.setY(( (yy - YMin) / YScale)+effectiveBorder());
  return retval;
}

SVector SGrapher::unmapPoint(int xx, int yy) {
  SVector result(2);
  result.x( ((xx-effectiveBorder()) * XScale) + XMin );
  result.y( ((yy-effectiveBorder()) * YScale) + YMin );
  return result;
}

int SGrapher::effectiveBorder() {
  int result = Border;
  if (Labels)
    result += LabelBorder;
  return result;
}

void SGrapher::drawGraph() {
  setScales();
  GraphSurface = QPixmap(size());
  GraphSurface.fill(BGColour);
  if (Labels)
    drawLabels();
  if (DrawGrid)
    drawGridLines();    
  for (unsigned dset=0; dset < DataSets.size(); dset++) {
    QColor usecol;
    if (dset < DataSetColours.size())
      usecol = DataSetColours[dset];
    else {
      if (DataSetColours.size() > 0)
        usecol = DataSetColours[DataSetColours.size()-1];
      else
        usecol = Qt::black;
    }
    switch (DataSetModes[dset]) {
      case SLineGraph:
        drawLineGraph(DataSets[dset],true,usecol);
        break;
      case SScatterGraph:
        drawLineGraph(DataSets[dset],false,usecol);
        break;
      case SHistogram:
        drawHistogram(DataSets[dset],usecol);
        break;
      default:
        drawLineGraph(DataSets[dset],true,usecol);
        break;
    }
  }
  update();
}

void SGrapher::drawLabels() {
  QPainter graphdrawer(&GraphSurface);
  graphdrawer.setPen(LabelColour);
  QPen newpen = graphdrawer.pen();
  newpen.setWidth(1);
  // draw X axis
  graphdrawer.drawLine(effectiveBorder(),
                       GraphSurface.height()-effectiveBorder(),
                       GraphSurface.width()-effectiveBorder(),
                       GraphSurface.height()-effectiveBorder());
  if (Labels) {
    int labelstep = (GraphSurface.width() - (effectiveBorder() * 2));
    labelstep /= LabelDensity;
    if (labelstep == 0)
      labelstep ++;
    for (int mpos = 0; mpos <= (GraphSurface.width()-(effectiveBorder() * 2));
        mpos+= labelstep) {
      graphdrawer.drawLine(effectiveBorder() + mpos,
                            GraphSurface.height()-effectiveBorder(),
                            effectiveBorder() + mpos,
                            (GraphSurface.height()-effectiveBorder()) 
                              + LabelMeterLength);
      if ( (mpos / labelstep) % 2 == 0 ) {
          graphdrawer.setFont(QFont("Verdana",LabelMeterLength));
          QString valuel = QString::number((mpos*XScale)+XMin,'g',SigFigs);
          QRect textbox(effectiveBorder() + (mpos-(500/2)),
                        GraphSurface.height()- 
                                         (effectiveBorder() - LabelMeterLength),
                        500, 500);
          graphdrawer.drawText(textbox,Qt::AlignTop|Qt::AlignHCenter,valuel);
      }
    }
  }
  // draw Y axis
  graphdrawer.drawLine(effectiveBorder(),
                       GraphSurface.height()-effectiveBorder(),
                       effectiveBorder(),
                       effectiveBorder());
  if (Labels) {
    int labelstep = (GraphSurface.height() - (effectiveBorder() * 2));
    labelstep /= LabelDensity;
    if (labelstep == 0)
      labelstep++;
    for (int mpos = 0; mpos <= (GraphSurface.height()-(effectiveBorder() * 2));
        mpos+= labelstep) {
      graphdrawer.drawLine(effectiveBorder(),
                           GraphSurface.height()-effectiveBorder() - mpos,
                           effectiveBorder() - LabelMeterLength,
                           GraphSurface.height()-effectiveBorder() - mpos);
      if ( (mpos / labelstep) % 2 == 0 ) {
          graphdrawer.setFont(QFont("Verdana",LabelMeterLength));
          QString valuel = QString::number((mpos*YScale)+YMin,'g',SigFigs);
          QRect textbox(Border,
                        GraphSurface.height()-effectiveBorder() 
                                                         - mpos - (labelstep/2),
                        LabelBorder-LabelMeterLength,500);
          graphdrawer.drawText(textbox,Qt::AlignTop|Qt::AlignRight,valuel);
      }
    }
  }
}

void SGrapher::drawGridLines() {
  QPainter graphdrawer(&GraphSurface);
  QPen newpen = graphdrawer.pen();
  newpen.setStyle(Qt::DashLine);
  newpen.setColor(Qt::darkGray);
  graphdrawer.setPen(newpen);
  // draw the x-grid lines
  int labelstep = (GraphSurface.width() - (effectiveBorder() * 2));
  labelstep /= LabelDensity;
  if (labelstep == 0)
    labelstep++;
  for (int mpos = labelstep;
        mpos <= (GraphSurface.width()-(effectiveBorder() * 2));
        mpos+= labelstep) {
    graphdrawer.drawLine(effectiveBorder() + mpos,
                        GraphSurface.height()-effectiveBorder(),
                        effectiveBorder() + mpos,
                        effectiveBorder());
  }
  // draw the y-grid
  for (int mpos = labelstep;
        mpos <= (GraphSurface.height()-(effectiveBorder() * 2));
        mpos+= labelstep) {
  graphdrawer.drawLine(effectiveBorder(),
                        GraphSurface.height()-effectiveBorder() - mpos,
                        effectiveBorder() + 
                              (GraphSurface.width()-(effectiveBorder() * 2)),
                        GraphSurface.height()-effectiveBorder() - mpos);
  }
}

void SGrapher::drawHistogram(const SDataSet& sourceset, QColor usecol) {
  QPainter graphdrawer(&GraphSurface);
  graphdrawer.setPen(Qt::transparent);
  QBrush newbrush = graphdrawer.brush();
  newbrush.setStyle(Qt::SolidPattern);
  newbrush.setColor(usecol);
  graphdrawer.setBrush(newbrush);
  for (unsigned dpair=0; dpair < sourceset.size(); dpair++) {
    QPen newpen = graphdrawer.pen();
    newpen.setWidth(1);
    graphdrawer.setPen(newpen);
    QPoint pointpos = mapPoint(sourceset[dpair].first,
                                 sourceset[dpair].second);
    int nextwidthpos = GraphSurface.width() - effectiveBorder();
    if (dpair < (sourceset.size()-1)) {
      nextwidthpos = mapPoint(sourceset[dpair+1].first,0.0).x();
    }
    pointpos.setY(GraphSurface.height() - pointpos.y());
    if (sourceset[dpair].second != 0.0) {
      graphdrawer.drawRect(pointpos.x(),
                          pointpos.y(),
                          nextwidthpos - pointpos.x(),
                          (GraphSurface.height() - pointpos.y()) 
                            - effectiveBorder() - 1);
    }
  }
}

void SGrapher::drawLineGraph(const SDataSet& sourceset, bool lines,
                             QColor usecol) {
  QPainter graphdrawer(&GraphSurface);
  graphdrawer.setPen(usecol);
  QPoint previouspoint;
  for (unsigned dpair=0; dpair < sourceset.size(); dpair++) {
    QPen newpen = graphdrawer.pen();
    newpen.setWidth(1);
    graphdrawer.setPen(newpen);
    QPoint pointpos = mapPoint(sourceset[dpair].first,
                                 sourceset[dpair].second);
    pointpos.setY(GraphSurface.height() - pointpos.y());
    graphdrawer.drawRect(pointpos.x()-(PointSize/2),
                         pointpos.y()-(PointSize/2),
                         PointSize,PointSize);
    if ((dpair > 0) && lines) {
      newpen.setWidth(LineThickness);
      graphdrawer.setPen(newpen);
      graphdrawer.drawLine(previouspoint,pointpos);
    }
    previouspoint = pointpos;
  }
}

