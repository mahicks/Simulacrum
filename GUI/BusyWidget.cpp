/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * BusyWidget
 * A simple overlay Widget for Qt4 to make another widget look busy.
 * M. A. Hicks
 */

#include <QPainter>
#include <QPen>
#include <QVariant>
#include <iostream>
#include <QProgressBar>
#include <QLabel>
#include <QString>
#include <QPushButton>
#include <QTimer>
#include <QMovie>

#include "BusyWidget.h"

using namespace Simulacrum;

#ifndef MOBILE
const int BusyWidget::progressWidth  = 300;
const int BusyWidget::progressHeight = 20;
#else
const int BusyWidget::progressWidth  = 600;
const int BusyWidget::progressHeight = 100;
#endif

BusyWidget::BusyWidget(QWidget* parent) : QWidget(parent), busyOpacity(0.4) {
  hide();
  busyTarget   = parent;
  busyProgress = new QProgressBar(this);
  busyLabel    = new QLabel(this);
  busyLabel->setText("");
  busyLabel->setWordWrap(true);
  busyLabel->setProperty("makeTrans",true);
  busyProgress->setTextVisible(false);
  busyColour   = QColor(0,0,0);
  lockCount    = 0;
  errorStatus  = false;
  closeButton  = new QPushButton(tr("&Hide"), this);
  connect(closeButton,SIGNAL(clicked()),this,SLOT(clear()));
  connect(closeButton,SIGNAL(clicked()),this,SIGNAL(closed()));
  repainter    = new QTimer(this);
  repainter->setSingleShot(true);
  connect(repainter, SIGNAL(timeout()), this, SLOT(dorepaint()));
  busyAnim       = new QMovie(this);
  busyAnimHolder = new QLabel(this);
  busyAnimHolder->setVisible(false);
  setBusyAnim(":resources/wait-large");
  clear();
  acceptCoreSignals();
}

BusyWidget::~BusyWidget() {

}

void BusyWidget::setBusy(bool busyval) {
  if (errorStatus) return;
  if (busyval){
    lockCount++;
    if (lockCount == 1){
      busyTarget->setEnabled(!disableInterface);
      show();
      raise();
      update();
    }
  }
  else {
    if(lockCount)
      lockCount--;
    if (!lockCount){
      busyTarget->setEnabled(true);
      hide();
      clear(true);
    }
  }
}

void BusyWidget::setColor(const QColor& newcolor){
  busyColour = QColor(newcolor);
  update();
}

void BusyWidget::setOpacity(float lopacity){
  busyOpacity = lopacity;
  update();
}

void BusyWidget::setMessage(const QString& lmessage){
  busyLabel->setText("<center>"+lmessage+"</center>");
}

void BusyWidget::showProgress(int progress, int lmax){
  if (errorStatus) return;
  busyProgress->setMaximum(lmax);
  busyProgress->setMinimum(0);
  busyProgress->setValue(progress);
  stopOscillateProgress();
  busyProgress->show();
}

void BusyWidget::hideIndicators(){
  busyProgress->hide();
  busyProgress->update();
}

void BusyWidget::oscillateProgress(bool start) {
  if (errorStatus) return;
  if (start) {
    if (!isBusy())
      setBusy(true);
    busyProgress->hide();
    busyAnim->start();
    busyAnimHolder->show();
  }
  else {
    stopOscillateProgress();
    if (isBusy()) {
      setBusy(false);
    }
  }
}

void BusyWidget::stopOscillateProgress() {
  busyAnimHolder->hide();
  busyAnim->stop();
}

bool BusyWidget::isBusy() {
  return lockCount;
}

void BusyWidget::canCloseMessage(bool newclose) {
  canClose = newclose;
  if (canClose)
    setDisableInterface(false);
}

void BusyWidget::setDisableInterface(bool dodis) {
  disableInterface = dodis;
}

void BusyWidget::doError(const QString &error, bool keepold) {
  QColor darkRed(96,0,0);
  QString errorstring;
  if (keepold){
    errorstring.append(busyLabel->text());
    errorstring.append("<br/>");
  }
  errorstring.append("<h3><font color=\"white\"><center>");
  errorstring.append(error);
  errorstring.append("</center></font></h3>");
  setColor(darkRed);
  setBusy(true);
  setMessage(errorstring);
  errorStatus = true;
  hideIndicators();
  update();
}

void BusyWidget::clear(bool skipnobusy){
  QColor defaultcolour(0,0,0);
  setColor(defaultcolour);
  lockCount   = 0;
  errorStatus = false;
  hideIndicators();
  setMessage("");
  if (!skipnobusy)
    setBusy(false);
  canCloseMessage(false);
  setDisableInterface(true);
  busyLabel->setOpenExternalLinks(true);
  setCloseButtonLabel(tr("&Close"));
  update();
}

void BusyWidget::setBusyAnim(QString animpath) {
  busyAnim->setFileName(animpath);
  busyAnimHolder->setMovie(busyAnim);
  busyAnim->start();
  busyAnimHolder->resize(busyAnim->frameRect().width()+10,
                         busyAnim->frameRect().height()+10);
  busyAnim->stop();
}

void BusyWidget::dorepaint() {
  update();
}

void BusyWidget::resizeEvent(QResizeEvent* newsizeevent) {
  QWidget::resizeEvent(newsizeevent);
  repainter->start(100);
}

void BusyWidget::paintEvent(QPaintEvent*) {
  const unsigned widgetspacing = 10;
  if ((width() != busyTarget->width()) || (height() != busyTarget->height())) {
    resize(busyTarget->width(),busyTarget->height());
  }
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setPen(QPen(busyColour.rgb()));
  painter.setBrush(QBrush(busyColour.rgb()));
  painter.setOpacity(busyOpacity);
  painter.drawRect(0,0,width(),height());
  painter.setOpacity(1);
  int lprogresswidth;
  if (width() < progressWidth)
    lprogresswidth = width();
  else
    lprogresswidth = progressWidth;
  busyLabel->resize(width(),busyLabel->sizeHint().height());
  int ystartpos = (height()/2)-
                  ((progressHeight + busyLabel->height())/2) -
                  (widgetspacing/2);
  if (canClose)
    ystartpos -= (closeButton->height() / 2) + (widgetspacing/2);
  if (ystartpos < 0)
    ystartpos = 0;
  if (busyAnimHolder->isVisible()) {
    busyAnimHolder->move((this->width()/2)-(busyAnimHolder->width()/2),
                         (this->height()/2)-(busyAnimHolder->height()/2));
    busyLabel->setGeometry((this->width()/2)-(busyLabel->width()/2),
                     busyAnimHolder->y()+busyAnimHolder->height()+widgetspacing,
                                                   width(),busyLabel->height());
  }
  else {
    busyProgress->setGeometry((this->width()/2)-(lprogresswidth/2),
                              ystartpos,
                              lprogresswidth,progressHeight);
    busyLabel->setGeometry((this->width()/2)-(busyLabel->width()/2),
                         busyProgress->y()+busyProgress->height()+widgetspacing,
                                                   width(),busyLabel->height());
  }
  closeButton->setGeometry((this->width()/2)-(closeButton->width()/2),
                           busyLabel->y() + busyLabel->height() + widgetspacing,
                           closeButton->width(), closeButton->height());
  closeButton->setHidden(!canClose);
}

void BusyWidget::refresh(bool nref) {
  SQConnectable::refresh(nref);
}

void BusyWidget::setProgress(int nprog) {
  SQConnectable::setProgress(nprog);
  if (nprog == -1)
    oscillateProgress(true);
  else
    showProgress(nprog);
}

void BusyWidget::setCloseButtonLabel(const QString& newlabel) {
  closeButton->setText(newlabel);
}

void BusyWidget::sconnect(SConnectable* newcon) {
  SQConnectable::sconnect(*newcon);
}

void BusyWidget::sconnect(SConnectable& newcon) {
  SQConnectable::sconnect(newcon);
}

void BusyWidget::sconnect(SQConnectable& ncon) {
  SQConnectable::sconnect(ncon);
}

void BusyWidget::sdisconnect(SConnectable* newdis) {
  SQConnectable::sdisconnect(newdis);
}

void BusyWidget::sdisconnect(SConnectable& newdis) {
  SQConnectable::sdisconnect(&newdis);
}

void BusyWidget::sdisconnect(SQConnectable& ndis) {
  SQConnectable::sdisconnect(ndis);
}

void BusyWidget::setWaiting(bool wait) {
  oscillateProgress(wait);
}
