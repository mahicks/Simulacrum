/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * A DICOM Tag Browser Widget for Qt
 * M. A. Hicks
 */
#include <QtGui>
#include <QPainter>
#include <QList>
#include <vector>
#include <string>
#include <QMetaType>
#include <QApplication>
#include <QScrollBar>

#include "tagBrowser.h"
#include <Toolbox/SFile/sfile.h>
#include <Core/definitions.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SResourceAllocator/sresourceallocator.h>
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/sysInfo/sysInfo.h>

using namespace Simulacrum;

#ifdef MOBILE
static unsigned goodIconSize() {
#if QT_VERSION >= 0x050000
  return (unsigned)(QGuiApplication::primaryScreen()->logicalDotsPerInch()/1.3);
#else
  return 100;
#endif
}
#endif

tagBrowserWorker::tagBrowserWorker() {
  clear();
}

tagBrowserWorker::~tagBrowserWorker() {

}

void tagBrowserWorker::clear() {
  stopNow = false;
}

void tagBrowserWorker::safeStop() {
  stopNow = true;
}

bool tagBrowserWorker::shouldStop() {
  return stopNow;
}

tagBrowser::tagBrowser(QWidget *parent) : QTreeWidget(parent),
                                          simpleView(false),
                                          busyMaker(this),
                                          multiResource(false),
                                          Editable(false) {
  clear();
  /* hook up the signals and slots for threaded communication with the GUI */
  connect (&AddFilesThread, SIGNAL(doError(QString)),
           &busyMaker,SLOT(doError(const QString&)));
  connect (&AddFilesThread, SIGNAL(setBusy(bool)),
           &busyMaker,SLOT(setBusy(bool)));
  connect (&AddFilesThread, SIGNAL(setMessage(QString)),
           &busyMaker,SLOT(setMessage(const QString&)));
  connect (&AddFilesThread, SIGNAL(oscillateProgress()),
           &busyMaker,SLOT(oscillateProgress()));
  connect (&AddFilesThread, SIGNAL(showProgress(int,int)),
           &busyMaker,SLOT(showProgress(int,int)));
  connect (&AddFilesThread, SIGNAL(newResource(SResource*)),
           this,SLOT(addResourceSafe(SResource*)));
  connect (&AddFilesThread, SIGNAL(newTag(SAbsTreeNode*)),
           this,SLOT(addTopLevelTag(SAbsTreeNode*)));
  connect (&AddFilesThread, SIGNAL(refresh()),
           this,SLOT(refresh()));
  connect (&AddFilesThread, SIGNAL(updateView()),
           this,SLOT(updateView()));

  connect (&AddTagsThread, SIGNAL(doError(QString)),
           &busyMaker,SLOT(doError(const QString&)));
  connect (&AddTagsThread, SIGNAL(setBusy(bool)),
           &busyMaker,SLOT(setBusy(bool)));
  connect (&AddTagsThread, SIGNAL(setMessage(QString)),
           &busyMaker,SLOT(setMessage(const QString&)));
  connect (&AddTagsThread, SIGNAL(oscillateProgress()),
           &busyMaker,SLOT(oscillateProgress()));
  connect (&AddTagsThread, SIGNAL(showProgress(int,int)),
           &busyMaker,SLOT(showProgress(int,int)));
  connect (&AddTagsThread, SIGNAL(refresh()),
           this,SLOT(refresh()));
  connect (&AddTagsThread, SIGNAL(newItem(QTreeWidgetItem*)),
           this,SLOT(addItem(QTreeWidgetItem*)));
  connect (&AddTagsThread, SIGNAL(newItems(QList<QTreeWidgetItem*>*,bool)),
           this,SLOT(addItems(QList<QTreeWidgetItem*>*, bool)));
  connect (&AddTagsThread, SIGNAL(configureView()),
           this,SLOT(configureView()));
  connect (this, SIGNAL(itemChanged(QTreeWidgetItem*,int)),
           this, SLOT(itemModified(QTreeWidgetItem*,int)));
#ifdef MOBILE
  unsigned int goodsize = goodIconSize();
  verticalScrollBar()->setStyleSheet(
    "QScrollBar:vertical { width: " + QString::number(goodsize/2.5)  + "px; }");
  horizontalScrollBar()->setStyleSheet(
   "QScrollBar:horizontal { height: " + QString::number(goodsize/2.5)+ "px; }");
  QFont tmpfont = font();
  tmpfont.setPointSize((int)((float)tmpfont.pointSize()*0.8));
  setFont(tmpfont);
#endif
}

tagBrowser::~tagBrowser() {
  AddFilesThread.wait();
  AddTagsThread.wait();
  clear();
}

void tagBrowser::resizeEvent(QResizeEvent* event) {
  QTreeWidget::resizeEvent(event);
#ifdef MOBILE
  resizeColumns(false);
#endif
}

void GenerateTreeItems::setParameters(const SAbsTreeNodeList_t newtagset,
                                      bool doeditable, bool simple) {
  if (isRunning()) return;
  tagBrowserWorker::clear();
  genTags = newtagset;
  Editable = doeditable;
  SimpleView = simple;
}

void GenerateTreeItems::run() {
  generateTags(genTags,nullptr);
}

void GenerateTreeItems::generateTags(const SAbsTreeNodeList_t& ltagList,
                                     STreeNodeItem* itemparent) {
  QList<QTreeWidgetItem*> *itemlist = new QList<QTreeWidgetItem*>;
  STreeNodeItem *newitem;
  /* Only perform these actions at the top-level */
  if (itemparent == nullptr){
    emit setBusy(true);
    emit oscillateProgress();
    emit setMessage("Constructing tree...");
  }
  /* Now add all of the tags to the list view */
  for (unsigned pos=0; pos < ltagList.size(); pos++) {
    SAbsTreeNode *newnode = ltagList[pos];
    newitem = new STreeNodeItem(itemparent, newnode, Editable);
    if ( itemparent == nullptr) {
      itemlist->push_back(static_cast<QTreeWidgetItem*>(newitem));
      // sets a hidden data element specifying the data resource
      newitem->setResourceURI(newnode->NodeValue().c_str());
    }
    if (shouldStop())
      break;
  }
  /* Only perform these actions at the top-level */
  if (itemparent == nullptr){
    emit newItems(itemlist,true);
    emit configureView();
    emit setBusy(false);
  }
  else
    delete itemlist;
}

void tagBrowser::addTags(SAbsTreeNodeList_t &ltagList){
  AddTagsThread.setParameters(ltagList, Editable, simpleView);
  AddTagsThread.start();
}

void MultiFileHandler::addFile(const QString& dicomfile){
  SResource* newresource =
                 SResourceAllocator::SResourceFromPath(dicomfile.toStdString());
  if (newresource != nullptr){
    try{
      newresource->refresh(LoadPixelDataDefault);
    }
    catch(SimulacrumException &e){
      delete newresource;
      throw e;
    }
    emit newResource((SResource*) newresource);
  }
}

void MultiFileHandler::setParameters(const QStringList& filelist,
                                     bool recurse, bool pixdata) {
  if (isRunning()) return;
  clear();
  FileList = filelist;
  Recurse  = recurse;
  LoadPixelDataDefault = pixdata;
}

void MultiFileHandler::run() {
  doAddFiles();
}

void MultiFileHandler::clear() {
  tagBrowserWorker::clear();
  FileList.clear();
  Recurse  = true;
  LoadPixelDataDefault = false;
}

void MultiFileHandler::doAddFiles() {
  emit setBusy(true);
  emit setMessage("Constructing file list...");
  emit oscillateProgress();
  if (Recurse) {
    foreach (QString afile, FileList) {
      SFile newfile(afile.toStdString());
      try {
        if (newfile.isDIR()) {
          std::vector<std::string> expandlist = newfile.getDIRContents(Recurse);
          for (unsigned i=0; i<expandlist.size(); i++) {
            FileList.append(QString(expandlist[i].c_str()));
          }
        }
      }
      catch (std::exception&) {}
    }
  }
  int NumFiles = FileList.length();
  int Counter  = 0;
  for (int filenum=0; filenum<FileList.length(); filenum++) {
    std::stringstream progress;
    progress << "Reading Resource " << filenum << " of " << FileList.length();
    emit setMessage(progress.str().c_str());
    try {
      addFile(FileList[filenum]);
    }
    catch (std::exception &e) {
      SLogger::global().addMessage(std::string(e.what()) + "<br/>" +
                              FileList[filenum].toStdString(),SLogLevels::HIGH);
    }
    Counter++;
    emit showProgress(Counter,NumFiles);
    if (shouldStop())
      break;
  }
  emit updateView();
  emit setBusy(false);
  clear();
}

void tagBrowser::addFiles(const QStringList& filelist, bool recurse){
  if (filelist.length() > 0) {
    AddFilesThread.wait();
    if (multiResource)
      AddFilesThread.setParameters(filelist,recurse,LoadPixelDataDefault);
    else
      AddFilesThread.setParameters(QStringList(filelist[0]),
                                  false,LoadPixelDataDefault);
    AddFilesThread.start();
  }
}

void tagBrowser::addFilesRemoveExisting(const QStringList& resl, bool recurse) {
  for (long int path=0; path < resl.size(); path++) {
    if (hasResource(resl[path])) {
      removeResource(&(getResource(resl[path])),false);
    }
  }
  addFiles(resl,recurse);
}

void tagBrowser::addTopLevelTag(SAbsTreeNode* tagadd) {
  nodeListLock.lock();
  if (!multiResource) nodeList.clear();
  nodeList.push_back(tagadd);
  nodeListLock.unlock();
  nodeListPendingLock.lock();
  nodeListPending.push_back(tagadd);
  nodeListPendingLock.unlock();
  if ((!multiResource) && (SResources.size() == 1))
    emit updated();
}

void tagBrowser::removeTopLevelTag(SAbsTreeNode* toremove, bool stoponfirst,
                                   bool dorefresh) {
  nodeListLock.lock();
  for (unsigned i=0; i<nodeList.size(); i++)
    if (nodeList[i] == toremove) {
      nodeList.erase(nodeList.begin()+i);
      if (stoponfirst)
        break;
    }
  nodeListLock.unlock();
  if (dorefresh)
    refresh(false);
}

void tagBrowser::addResource(SResource* newresource) {
  if (!multiResource) {
    clear();
  }
  SResourcesLock.lock();
  if ((newresource->getLocation().size() == 0) ||
      SResources.count(newresource->getLocation())) {
    // keys should be unique
    newresource->changeLocation(newresource->getLocation() + " "
                                + sysInfo::genGUIDString());
  }
  SResources.insert(std::pair<std::string,SResource*>
                     (newresource->getLocation(),newresource));
  addTopLevelTag(&newresource->getRoot());
  SResourcesLock.unlock();
}

void tagBrowser::addResourceFull(SResource* newresource) {
  addResource(newresource);
  refresh(false);
}

void tagBrowser::addResourceSafe(SResource* newresource) {
  if (!hasResource(newresource->getLocation().c_str())) {
    addResource(newresource);
  }
  else {
    delete newresource;
  }
}

void tagBrowser::addResourceFullSafe(SResource* newresource) {
  if (!hasResource(newresource->getLocation().c_str())) {
    addResourceFull(newresource);
  }
  else {
    delete newresource;
  }
}

void tagBrowser::removeResource(SResource* theResource, bool dorefresh) {
  if (theResource->refCount() == 0) {
    removeTopLevelTag(&(theResource->getRoot()),true,dorefresh);
    if (theResource->refCount() == 0) {
      theResource->lock();
      theResource->unlock();
      SResourcesLock.lock();
      if (SResources.count(theResource->getLocation())) {
        SResources.erase(theResource->getLocation());
      }
      else { // location may have changed since being added ;-(
        for ( sresourcemap_t::iterator it=SResources.begin();
              it != SResources.end(); it++ ) {
          if (it->second == theResource) {
            SResources.erase(it);
            break; //map, so unique
          }
        }
      }
      SResourcesLock.unlock();
      delete theResource;
    }
  }
}

void tagBrowser::removeCurrentResource() {
  QStringList selectedresources = getSelectedResources();
  QList<QTreeWidgetItem*> selecteditems = selectedItems();
  QSet<QTreeWidgetItem*> removeItems;
  clearSelection();
  foreach(QString resourcename,selectedresources) {
    removeResource(&getResource(resourcename),false);
  }
  foreach(QTreeWidgetItem* selitem,selecteditems) {
    removeItems.insert(getResourceItem(selitem));
  }
  foreach(QTreeWidgetItem* delitem,removeItems) {
    delete delitem;
  }
}

void tagBrowser::addItem(QTreeWidgetItem* newitem) {
  addTopLevelItem(newitem);
  resizeColumns(true);
}

void tagBrowser::addItems(QList< QTreeWidgetItem* >* newitems, bool cleanup) {
  addTopLevelItems(*newitems);
  if (cleanup)
    delete newitems;
  resizeColumns(true);
}

void tagBrowser::setSimple(bool dosimp){
  if (dosimp != simpleView) {
    simpleView = dosimp;
    setColumnHidden(TagPosition, simpleView);
    setColumnHidden(TypePosition, simpleView);
    setColumnHidden(LengthPosition, simpleView);
    refresh(false);
  }
}

void tagBrowser::setMultiResource(bool domul) {
  multiResource = domul;
}

bool tagBrowser::isMultiResource() {
  return multiResource;
}

void tagBrowser::setDropTarget(bool acceptdrops){
  setAcceptDrops(acceptdrops);
}

bool tagBrowser::filterItem(QTreeWidgetItem* theitem, const QString &filter,
                              bool recurse) {
  bool dohide = true;
  for ( int i = 0; i < theitem->columnCount(); i++)
    if ( theitem->text(i).indexOf(filter) != -1 &&
         theitem->text(i).length() > 0 )
      dohide = false;
  if (recurse && !dohide)
    for ( int c = 0; c < theitem->childCount(); c++)
      filterItem(theitem->child(c), "", true);
  else if (recurse && dohide)
    for ( int c = 0; c < theitem->childCount(); c++)
      if ( filterItem(theitem->child(c), filter, true) )
        dohide = false;
  theitem->setHidden(dohide);
  return !dohide;
}

void tagBrowser::newFilterString (const QString &newfilter) {
  busyMaker.setBusy(true);
  ActiveFilter = newfilter;
  bool   empty = true;
  for ( int i = 0; i< topLevelItemCount(); i++) {
    if (filterItem(topLevelItem(i),newfilter, true))
      empty = false;
  }
  if (newfilter.length() > 0 || (!multiResource))
    friendlyExpandAll();
  else
    friendlyCollapseAll();
  // ensure any selected item is in view after a change in the visible tree
  if (empty)
    clearSelection();
  else {
    QTreeWidgetItem *curitem = currentItem();
    expandItem(curitem);
    scrollToItem(curitem,QAbstractItemView::PositionAtCenter);
  }
  busyMaker.setBusy(false);
}

void tagBrowser::updateView() {
  nodeListPendingLock.lock();
  addTags(nodeListPending);
  nodeListPending.clear();
  nodeListPendingLock.unlock();
}

void tagBrowser::refresh(bool deep){
  waitOnThreads();
  QTreeWidget::clear();
  busyMaker.setBusy(true);
  nodeListPendingLock.lock();
  nodeListPending.clear();
  nodeListPendingLock.unlock();
  if (deep) {
    busyMaker.setMessage("Refreshing resources (building file list)...");
    sresourcemap_t::iterator it;
    unsigned                    counter = 0;
    QStringList                 readdlist;
    SResourcesLock.lock();
    for ( it=SResources.begin() ; it != SResources.end(); it++ ) {
      busyMaker.showProgress(counter,SResources.size());
      readdlist.push_back(it->second->getLocation().c_str());
      counter ++;
    }
    SResourcesLock.unlock();
    clear();
    addFiles(readdlist);
  }
  else {
    busyMaker.setMessage("Rendering resources...");
    nodeListLock.lock();
    addTags(nodeList);
    nodeListLock.unlock();
    busyMaker.setBusy(false);
  }
  emit updated();
}

void tagBrowser::saveCurrent() {
  busyMaker.setBusy(true);
  QStringList selectedresources = getSelectedResources();
  busyMaker.setMessage("Saving selected resources...");
  for (int i = 0; i < selectedresources.size(); i++) {
    busyMaker.showProgress(i,selectedresources.size());
    SResource &currentres = getResource(selectedresources[i]);
    currentres.loadMissingData();
    try {
      SDICOM::standardizeTag
                          (dynamic_cast<SDICOM&>(currentres).getRootTag(),true);
    }
    catch (std::exception &e) { }
    try {
      currentres.store();
    }
    catch (std::exception &e) { 
      SLogger::global().addMessage("Error saving resource!", SLogLevels::HIGH);
      SLogger::global() << e.what() << "\n";
    }
  }
  busyMaker.setBusy(false);
}

void tagBrowser::saveCurrentAs(const QString& newlocation) {
  busyMaker.setBusy(true);
  QStringList selectedresources = getSelectedResources();
  if (selectedresources.size() == 1) {
    SResource &selres = getResource(selectedresources[0]);
    selres.loadMissingData();
    selres.setLocation(newlocation.toStdString());
    saveCurrent();
    dynamic_cast<STreeNodeItem*>(getResourceItem(currentItem()))->refresh();
  }
  busyMaker.setBusy(false);
}

void tagBrowser::saveAll() {
  friendlyCollapseAll();
  selectAll();
  saveCurrent();
}

void tagBrowser::configureView() {
  newFilterString(ActiveFilter);
  if (ActiveFilter.length() > 0) {
    newFilterString(ActiveFilter);
  }
  else
    if (multiResource)
      friendlyCollapseAll();
    else
      friendlyExpandAll();
  emit updated();
}

void deleteItemList(std::vector<QTreeWidgetItem*>* dellist) {
  for (unsigned i=0; i<dellist->size(); i++)
    delete (*dellist)[i];
  delete dellist;
}

void deleteResourceList(std::vector<SResource*>* dellist) {
  for (unsigned i=0; i<dellist->size(); i++) {
    (*dellist)[i]->wait();
    delete (*dellist)[i];
  }
  delete dellist;
}

void tagBrowser::clear() {
  if (isBusy())
    return;
  busyMaker.setBusy(true);
  waitOnThreads();
  SResourcesLock.lock();
  std::vector<QTreeWidgetItem*>* cleanuplist = new std::vector<QTreeWidgetItem*>;
  cleanuplist->reserve(topLevelItemCount());
  while (topLevelItemCount() > 0)
    cleanuplist->push_back(takeTopLevelItem(0));
  // push the actual recursive delete job to the background
  GeneralPool.addJob( std::bind(deleteItemList,cleanuplist) );
  busyMaker.clear();
  nodeListLock.lock();
  nodeList.clear();
  nodeListLock.unlock();
  nodeListPendingLock.lock();
  nodeListPending.clear();
  nodeListPendingLock.unlock();
  sresourcemap_t::iterator it;
  std::vector<SResource*>* rescleanuplist = new std::vector<SResource*>;
  rescleanuplist->reserve(SResources.size());
  for ( it=SResources.begin() ; it != SResources.end();it != SResources.end()) {
    sresourcemap_t::iterator tmpit = it;
    tmpit++;
    if (it->second->refCount() == 0) {
      rescleanuplist->push_back(it->second);
      SResources.erase(it);
    }
    it = tmpit;
  }
  // background the resource deletion
  GeneralPool.addJob( std::bind(deleteResourceList,rescleanuplist) );
  SResourcesLock.unlock();
  setDropTarget(true);
  setWordWrap(true);
  // configure the columns
  setColumnCount(5);
  QStringList Headers;
  Headers << "Name" << "ID" << "Type" << "Length"
          << "Value" ;
  setHeaderLabels(Headers);
  setAlternatingRowColors(1);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  resizeColumns(false);
  setContextMenuPolicy(Qt::CustomContextMenu);
  busyMaker.setBusy(false);
}

QStringList tagBrowser::mimeTypes () const {
  QStringList qstrList;
  qstrList.append("text/uri-list");
  return qstrList;
}

bool tagBrowser::dropMimeData(QTreeWidgetItem *, int,
                              const QMimeData *data, Qt::DropAction) {
  QList<QUrl> urlList;
  QStringList fileList;
  urlList = data->urls(); // retrieve list of urls
  if (multiResource) {
    foreach(QUrl url, urlList) {
      fileList.append(url.toLocalFile());
    }
  }
  else
    if (urlList.length() > 0)
      fileList.append(urlList[0].toLocalFile());
  addFiles(fileList);
  return true;
}

QSize tagBrowser::sizeHint() const {
  int x = 900, y= 800;
  return (QSize(x,y));
}

bool tagBrowser::hasResource(const QString& respath) {
  bool result;
  SResourcesLock.lock();
  result = (SResources.count(respath.toStdString()) > 0);
  SResourcesLock.unlock();
  return result;
}

QTreeWidgetItem* tagBrowser::getResourceItem(QTreeWidgetItem* item) {
  if (item->text(ResourcePosition).length() == 0 && (item->parent() != nullptr)) {
    // head up the tree
    return getResourceItem(item->parent());
  }
  else {
    return item;
  }
}

QString tagBrowser::getResource(QTreeWidgetItem *item) {
  return getResourceItem(item)->text(ResourcePosition);
}

SResource& tagBrowser::getResource(const QString& resourcename) {
  SResource *retres;
  SResourcesLock.lock();
  if (SResources.count(resourcename.toStdString()) > 0) {
    retres = (SResources[resourcename.toStdString()]);
  }
  else {
    SResourcesLock.unlock();
    throw SimulacrumExceptionGeneric("Cannot find named resource: " +
                                     resourcename.toStdString());
  }
  SResourcesLock.unlock();
  return *retres;
}

std::string tagBrowser::getPathRaw(QTreeWidgetItem* item) {
  std::string retval;
  if (item) {
    SURI nodepath;
    auto *currentitem = dynamic_cast<STreeNodeItem*>(item);
    if (currentitem != nullptr) {
      while ( currentitem->getParentNodeItem() != nullptr ) {
        nodepath.addComponentFront(currentitem->getNode().NodeValue());
        currentitem = currentitem->getParentNodeItem();
      }
      retval = nodepath.getURI(true);
    }
  }
  return retval;
}

QString tagBrowser::getPath(QTreeWidgetItem* item) {
  QString retval;
  if (item) {
    SURI nodepath;
    QTreeWidgetItem *currentitem = item;
    while ( currentitem->parent() != nullptr ) {
      nodepath.addComponentFront(currentitem->text(TagPosition).toStdString());
      currentitem = currentitem->parent();
    }
    retval = QString(nodepath.getURI(true).c_str());
  }
  return retval;
}

tagBrowser::sresourcemap_t tagBrowser::getResources() {
  SResourcesLock.lock();
  tagBrowser::sresourcemap_t result = SResources;
  SResourcesLock.unlock();
  return result;
}

QStringList tagBrowser::getSelectedResources() {
  QSet<QString> selectedResourcesSet;
  QStringList   selectedResources;
  QList<QTreeWidgetItem*>  items = selectedItems();
  foreach(QTreeWidgetItem *item, items) {
    selectedResourcesSet.insert(getResource(item));
  }
  foreach(QString res,selectedResourcesSet) {
    selectedResources.append(res);
  }
  return selectedResources;
}

QList<STreeNodeItem*> tagBrowser::getSelectedNodeItems() {
  QList<QTreeWidgetItem*> selected = selectedItems();
  QList<STreeNodeItem*>    res;
  for (int i=0; i<selected.length(); i++) {
    try {
      res.push_back(dynamic_cast<STreeNodeItem*>(selected[i]));
    }
    catch (std::exception &) { }
  }
  return res;
}

void tagBrowser::doError(const QString& errorstring) {
  busyMaker.doError(errorstring);
}

bool tagBrowser::isBusy() {
  return (AddFilesThread.isRunning() || AddTagsThread.isRunning());
}

void tagBrowser::wait() {
  waitOnThreads();
}

void tagBrowser::waitOnThreads() {
  AddFilesThread.wait();
  AddTagsThread.wait();
}

void tagBrowser::stopActions() {
  if (AddFilesThread.isRunning())
    AddFilesThread.safeStop();
  if (AddTagsThread.isRunning())
    AddTagsThread.safeStop();
}

void tagBrowser::resizeColumns(bool todatal) {
  bool todata = todatal;
#ifdef MOBILE
  todata = false;
#endif
  unsigned visiblecolumns = 0;
  if (!todata)
    for (int acol = 0; acol < columnCount(); acol++)
      if ( ! isColumnHidden(acol) )
        visiblecolumns++;
  for (int acol = 0; acol < columnCount(); acol++)
    if (todata)
      resizeColumnToContents(acol);
    else
      setColumnWidth(acol,
                    ((width()-verticalScrollBar()->width())/visiblecolumns)-30);
}

void tagBrowser::setSortable (bool dosort) {
  setSortingEnabled(dosort);
}

void tagBrowser::itemModified(QTreeWidgetItem* item, int column) {
  disconnect (this, SIGNAL(itemChanged(QTreeWidgetItem*,int)),
              this, SLOT(itemModified(QTreeWidgetItem*,int)));
  dynamic_cast<STreeNodeItem*>(item)->writeBack(column);
  dynamic_cast<STreeNodeItem*>(item)->refresh();
  connect (this, SIGNAL(itemChanged(QTreeWidgetItem*,int)),
           this, SLOT(itemModified(QTreeWidgetItem*,int)));
}

void tagBrowser::setEditable(bool ised) {
  Editable = ised;
  refresh(false);
}

bool tagBrowser::isEditable() {
  return Editable;
}

void tagBrowser::friendlyCollapseAll() {
  collapseAll();
  resizeColumns(true);
}

void tagBrowser::friendlyExpandAll() {
  expandAll();
  resizeColumns(true);
}

void tagBrowser::copyNodesToClipboard() {
  QClipboard *clipboard = QApplication::clipboard();
  QString copystring;
  QList<STreeNodeItem*> selectednodes =  getSelectedNodeItems();
  for (long int i=0; i<selectednodes.size(); i++) {
    copystring += QString(selectednodes[i]->getNode().NodeName().c_str());
    copystring += ",";
    copystring += QString(selectednodes[i]->getNode().NodeID().c_str());
    copystring += ",";
    copystring += QString(selectednodes[i]->getNode().NodeType().c_str());
    copystring += ",";
    copystring += QString::number(selectednodes[i]->getNode().NodeSize());
    copystring += ",";
    copystring += QString(selectednodes[i]->getNode().NodeValue().c_str());
    if (selectednodes.size() > 1)
      copystring += QString("\n");
  }
  copystring.replace('\0',' ');
  if (copystring.size() > 0)
    clipboard->setText(copystring);
}

void tagBrowser::copyNodeValuesToClipboard() {
  QClipboard *clipboard = QApplication::clipboard();
  QString copystring;
  QList<STreeNodeItem*> selectednodes =  getSelectedNodeItems();
  for (long int i=0; i<selectednodes.size(); i++) {
    copystring += QString(selectednodes[i]->getNode().NodeValue().c_str());
    if (selectednodes.size() > 1)
      copystring += QString("\n");
  }
  copystring.replace('\0',' ');
  clipboard->setText(copystring);
}

void tagBrowser::setBusyOpacity(float nopac) {
  busyMaker.setOpacity(nopac);
}

STreeNodeItem::STreeNodeItem(STreeNodeItem      * parent,
                             SAbsTreeNode* source, bool iseditable,
                             bool simpleview):
                              QTreeWidgetItem(parent), DataSource(*source),
                              Editable(iseditable),SimpleView(simpleview) {
  if (iseditable)
    setFlags(flags()|Qt::ItemIsEditable);
  refresh(true);
}

STreeNodeItem::~STreeNodeItem() {

}

void STreeNodeItem::setResourceURI(const QString& newres) {
  refresh();
  setText(ResourcePosition,newres);
  if (flags() & Qt::ItemIsEditable)
    setFlags(flags()^Qt::ItemIsEditable);
}

void STreeNodeItem::refresh(bool deep) {
  std::string value;
  value = DataSource.NodeName();
  if (value.length() > MaxNameLen){
    value = &value[value.length()-MaxNameLen];
    value = "|...|" + value;
  }
  setText(NamePosition,value.c_str());
  setText(TagPosition,DataSource.NodeID().c_str());
  setText(TypePosition,DataSource.NodeType().c_str());
  setText(LengthPosition,QString::number(DataSource.NodeSize()));
  value = DataSource.NodeValue();
  if ((value.length() > MaxDataLen) && (!(flags() & Qt::ItemIsEditable)) ){
      value = &value[value.length()-MaxDataLen];
      value = "|...|" + value;
      setText(DataPosition,value.c_str());
  }
  else {
    setText(DataPosition,SimulacrumGUILibrary::toQString(
                                   DataSource.stringEncoding(),value.c_str()));
  }
  if (DataSource.NodeError()) {
    QBrush tmp;
    tmp.setColor(Qt::red);
    setForeground(NamePosition,tmp);
  }
  else if ( DataSource.NodeEmph() && (parent() != nullptr ) ) {
    QFont newfont  = font(NamePosition);
    newfont.setItalic(true);
    setFont(NamePosition,newfont);
  }
  if (deep) {
    for (int c=0; c<childCount(); c++)
      delete child(c);
    if (DataSource.NodeChildrenNum(false) > 0) {
      SAbsTreeNodeList_t childnodes = DataSource.NodeChildren(false,SimpleView);
      for (unsigned c=0; c<childnodes.size(); c++)
        new STreeNodeItem(this, childnodes[c], Editable, SimpleView);
    }
  }
}

void STreeNodeItem::writeBack(int column) {
  switch (column) {
    case NamePosition:
      DataSource.NodeName(text(NamePosition).toStdString());
      break;
    case TagPosition:
      DataSource.NodeID(text(TagPosition).toStdString());
      break;
    case TypePosition:
      DataSource.NodeType(text(TypePosition).toStdString());
      break;
    case LengthPosition:
      DataSource.NodeSize(text(LengthPosition).toStdString());
      break;
    case DataPosition:
      DataSource.NodeValue(text(DataPosition).toStdString());
      break;
  };
}

SAbsTreeNode& STreeNodeItem::getNode() {
  return DataSource;
}

STreeNodeItem* STreeNodeItem::getParentNodeItem() {
  QTreeWidgetItem* parentpoint = parent();
  if (parentpoint == nullptr)
    return nullptr;
  else
    return dynamic_cast<STreeNodeItem*>(parentpoint);
}

void STreeNodeItem::deleteNode() {
  // do not allow for the deletion of top level nodes, as they are special
  if (parent() != nullptr) {
    bool res = getNode().removeNode();
    if (res)
      delete this;
  }
}

STreeNodeItem* STreeNodeItem::addChildNode() {
  STreeNodeItem *newitem =
             new STreeNodeItem(this,&(getNode().NewChild()));
  newitem->setFlags(newitem->flags() | Qt::ItemIsEditable);
  return newitem;
}

bool STreeNodeItem::isResourceItem() {
  return (text(ResourcePosition).length() != 0);
}
