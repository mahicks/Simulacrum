/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Simulacrum::GUI:SIMAGEBGLOADER
 * A Qt background loader for SImages
 * Author: M.A.Hicks
 */

#include "bgLoader.h"
#include <QString>

using namespace Simulacrum;

SImageBGLoader::SImageBGLoader() {
  Source = nullptr;
  Image = nullptr;
  clear();
}

void SImageBGLoader::run() {
  doWork();
}

void SImageBGLoader::refresh(bool) {
  start();
}

void SImageBGLoader::setResource(SResource& newres) {
  Source = &newres;
}

void SImageBGLoader::setResourcePath(const std::string &newpath) {
  Path = newpath;
}

void SImageBGLoader::setSImage(SSpace& newimg) {
  Image = &newimg;
}

void SImageBGLoader::clear() {
  SSpace    *imgtmp = Image;
  SResource *srctmp = Source;
  if (imgtmp != nullptr)
    imgtmp->lock();
  if (srctmp != nullptr)
    srctmp->lock();
  Source = nullptr;
  Image = nullptr;
  Path.clear();
  if (imgtmp != nullptr)
    imgtmp->unlock();
  if (srctmp != nullptr)
    srctmp->unlock();
}

void SImageBGLoader::blank() {
  if (Image != nullptr) {
    Image->lock();
    Image->reset();
    Image->unlock();
  }
}

void SImageBGLoader::stop() {
  if ( (Source != nullptr) && isRunning())
    Source->doStop(true);
}

void SImageBGLoader::doWork() {
  if (Image == nullptr || Source == nullptr)
    return;
  emit active(true);
  if (Source == nullptr || Image == nullptr)
    return;
  Image->lock();
  Source->lock();
  Source->doStop(false);
  try {
    Source->getSSpaceInto(*Image,Path);
  }
  catch (std::exception &e) {
    // maintain name
    std::string nametmp = Image->getName();
    Image->reset();
    Image->setName(nametmp);
  }
  Source->unlock();
  Image->unlock();
  emit active(false);
}
