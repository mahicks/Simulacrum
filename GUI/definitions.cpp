/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QApplication>
#include "definitions.h"
#include <Core/slockable.h>

using namespace Simulacrum;

static bool      SIMU_GUI_INIT_DONE = false;
static SLockable SIMU_GUI_INIT_LOCK;

bool SimulacrumGUILibrary::init() {
  bool result;
  SIMU_GUI_INIT_LOCK.lock();
  if (!SIMU_GUI_INIT_DONE) {
    // perform the one-time library init process
    //Q_INIT_RESOURCE(resources);
    SIMU_GUI_INIT_DONE = true;
  }
  result = SIMU_GUI_INIT_DONE;
  SIMU_GUI_INIT_LOCK.unlock();
  return result;
}

QString SimulacrumGUILibrary::toQString(SimulacrumLibrary::str_enc encoding,
                                        std::string string) {
  QString result;
  if (encoding == SimulacrumLibrary::str_enc::Latin1) {
    result = QString::fromLatin1(string.c_str());
  }
  else {
    result = QString(string.c_str());
  }
  return result;
}
