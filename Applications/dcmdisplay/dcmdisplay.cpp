/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Resources/DICOM/sdicom.h>
#include <GUI/viewPort.h>
#include <GUI/bgLoader.h>
#include <GUI/vpTools.h>
#include <Toolbox/SLogger/slogger.h>
#include <GUI/console.h>

#include <iostream>
#include <qapplication.h>
#include <math.h>

#include <GUI/grapher.h>

using namespace Simulacrum;

int main(int argc, char **argv){
  if (!SimulacrumLibrary::init()) {
    std::cerr << "Simulacrum library initialisation failure!" << std::endl;
    exit(6);
  }
  if (argc < 2){
    std::cout << argv[0] << ": ERROR, insufficient parameters." << std::endl;
    std::cout << argv[0] << " <DICOM File>" << std::endl;
    return 1;
  }
  SDICOM     DCMFile;
  SSpace     DICOMImage;
  std::vector<SSpace>
             VolumeStore;
             VolumeStore.resize(argc-1);
  std::vector<SSpace*>
             VolLoadList;
  SSlicer    VolSlice(DICOMImage);
             VolSlice.setFrontSlice();

  /* scout the tags */
  DCMFile.setLocation(argv[1]);
  if (!DCMFile.isValid()) {
    std::cerr << "ERROR: not a DICOM file." << std::endl;
    return 1;
  }
  DCMFile.loadAllTags(false);

  /* make a Qt output application and SViewPort */
  QApplication   dcmdisp(argc, argv);
  if (!SimulacrumGUILibrary::init()) {
    std::cerr << "Simulacrum GUI library initialisation failure!" << std::endl;
    exit(7);
  }
  SDCMViewPort   vpwidg;
  SQLogView      LogViewer(&vpwidg);
  SImageBGLoader vpbgl;
  SSliceTool     SliceController;

  /* connect some GUI events */
  QObject::connect(&vpwidg,SIGNAL(wheelUp(SViewPort&, int,int)),
                   &SliceController,SLOT(wheelUp(SViewPort&, int,int)));
  QObject::connect(&vpwidg,SIGNAL(wheelDown(SViewPort&,int,int)),
                   &SliceController,SLOT(wheelDown(SViewPort&, int,int)));
  LogViewer.sconnect(SLogger::global());

  /* Scale the viewport, and show it */
  float aspect = ((float)DCMFile.getExtent().getCoord(0) /
                  (float)DCMFile.getExtent().getCoord(1));
  vpwidg.resize(800,800/aspect);
  vpwidg.sconnect(vpbgl);
  vpwidg.sconnect(VolSlice);
  vpwidg.setActive(true);

  SViewPortEventHandler toolhandler (&vpwidg);
  SViewPortEventHandler projhandler (&vpwidg);
  toolhandler.addSVPTool(new SPanTool(&toolhandler));
  toolhandler.addSVPTool(new SZoomTool(&toolhandler));
  toolhandler.addSVPTool(new SFitStretchTool(&toolhandler));
  toolhandler.addSVPTool(new S1to1Tool(&toolhandler));
  toolhandler.addSVPTool(new SWLTool(&toolhandler));
  toolhandler.addSVPTool(new SOrthoRotateTool(&toolhandler));
  toolhandler.addSVPTool(new SPivotTool(&toolhandler));
  toolhandler.addSVPTool(new SRotateTool(&toolhandler));
  toolhandler.addSVPTool((new SSliceTool(&toolhandler))->doMouseScroll(false));
  toolhandler.addSVPTool(new SMirrorTool(&toolhandler));
  toolhandler.addSVPTool(new SFlipTool(&toolhandler));
  toolhandler.addSVPTool(new SVPProperties(&toolhandler));
  projhandler.addSVPTool(new SMaxIPTool(&projhandler));
  projhandler.addSVPTool(new SMinIPTool(&projhandler));
  projhandler.addSVPTool(new SAVGIPTool(&projhandler));
  projhandler.addSVPTool(new SOZIPTool(&projhandler));
  vpwidg.setEdgeWidget(&toolhandler,2);
  vpwidg.setEdgeWidget(&projhandler,0);
  toolhandler.connectViewPort(vpwidg);
  toolhandler.show();
  projhandler.connectViewPort(vpwidg);
  projhandler.show();

  if (argc == 2) {
    /* configure the BG loader */
    vpbgl.setResource((SResource&)DCMFile);
    vpbgl.setSImage(DICOMImage);
    vpbgl.refresh(true);
  }
  else {
    /* start the loading process */
    std::cout << "Loading: " << (argc - 1) << " files..." << std::endl;
    for (int i = 1; i < argc; i++) {
      DCMFile.setLocation(argv[i]);
      if (!DCMFile.isValid()) {
        std::cerr << "ERROR - Not a valid file: " << argv[i] << std::endl;
      }
      else {
        DCMFile.loadSSpace(VolumeStore[i-1]);
        VolLoadList.push_back(&(VolumeStore[i-1]));
      }
    }
    std::cout << "Loading complete." << std::endl;
    // concatenate loaded images into single volume
    DICOMImage.progress(0);
    std::cout << "Combining " << VolLoadList.size() 
              << " object(s) into a single space..." << std::endl;
    DICOMImage.concatenate(VolLoadList);
    std::cout << "Final volume: " << DICOMImage.extent().toString() 
              << std::endl;
    // apply the appropriate DICOM view settings
    SDICOM::postLoadSSpaceConfiguration(DICOMImage,DCMFile);
    DICOMImage.refresh(true);
  }

  vpwidg.show();

  return dcmdisp.exec();
}
