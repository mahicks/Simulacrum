/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
/* XMLDump -- a simple program to write an XML tree to stdout
 * M.A. Hicks 2012
 */

#include <Core/NNode/nnode.h>

using namespace Simulacrum;

int main(int argc, char* argv[]) {
  int retval = 0;
  char *XMLFileName;
  if (argc < 2){
    std::cerr << "ERROR: insufficient paramters." << std::endl;
    std::cerr << argv[0] << " <XMLfile>" << std::endl;
    return 1;
  }
  else {
    XMLFileName = argv[1];
  }
  NNodeResource xmlresource;
  xmlresource.setLocation(XMLFileName);
  if (!xmlresource.isValid()) {
    std::cerr << "Not a valid XML file." << std::endl;
    retval = 1;
  }
  else {
    NNode &xmlfile = (NNode&)xmlresource.getRoot();
    NNode::XMLParseRes parseresult = xmlfile.loadFromXMLFile(XMLFileName);
    xmlfile.printTree();
    if(xmlfile.good())
      std::cout << "XML tree is good." << std::endl;
    else {
      std::cout << "XML tree is bad." << std::endl;
      //now output result info
      std::cout << "   Line: " << parseresult.Line + 1 << std::endl
                << " Column: " << parseresult.Column + 1 << std::endl
                << "  Token: " << parseresult.Token_str << std::endl
                << "  State: " << parseresult.State_str  << std::endl
                << " Messages:" << std::endl
                << "  " << parseresult.Messages << std::endl;
      retval = 1;
    }
    if (argc > 2) {
      xmlfile.xmlRootNode().storeToXMLFile(std::string(argv[2]));
    }
    xmlfile.clear();
  }
  return retval;
}
