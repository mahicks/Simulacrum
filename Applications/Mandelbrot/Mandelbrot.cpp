/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Toolbox/SLogger/slogger.h>
#include <Core/salgorithms.h>
#include <Resources/spng.h>

#include <iostream>
#include <math.h>

using namespace Simulacrum;

bool lutMethod(std::vector <luttype>& LUT, unsigned bppin,unsigned centre,
               unsigned width, bool inverted) {
  std::vector<uint32_t> colors(4);
  RGBAI32SElem col((SElem::DataSource)&colors[0]);
  col.alpha(255);
  col.source((SElem::DataSource)&colors[1]);
  col.green(255);
  col.alpha(255);
  col.source((SElem::DataSource)&colors[2]);
  col.red(255);
  col.green(255);
  col.alpha(255);
  col.source((SElem::DataSource)&colors[3]);
  col.red(255);
  col.alpha(255);
  return GPLUT::genColBlendLUT(colors,LUT,bppin,centre,width,inverted);
}

unsigned mandelbrotAt(const SComplex& base, const SComplex& pos,
                      unsigned depth, unsigned maxdepth) {
  if (depth >= maxdepth) {
    return maxdepth;
  }
  else {
    if ( (pos.mag() > 2.0) || (depth >= maxdepth) ) {
      return depth;
    }
    else {
      return mandelbrotAt(base,(pos*pos)+base,depth+1,maxdepth);
    }
  }
}

SComplex mandelbrotCoords(const SCoordinate& extent,
                          const SComplex&      tlc,
                          const SComplex&      brc,
                          const SCoordinate& pos) {
  SVector::Precision xinc, yinc;
  xinc = (brc.r() - tlc.r()) / extent.x();
  yinc = (tlc.i() - brc.i()) / extent.y();
  return SComplex( tlc.r() + (pos.x()*xinc), tlc.i() - (pos.y()*yinc) );
}

void drawMandelbrot(SSpace& target, const SComplex& tlc, const SComplex& brc,
                    unsigned depth) {
  SCoordinate extent = target.extent();
  // set the element data type
  target.setNativeSElemType(new BW16SElem(nullptr));
  // set a nice window LUT function
  target.LUT().setLUTGen(lutMethod);
  target.LUT().useWL(true);
  target.LUT().genLUT(16,depth/2,depth);
  // define an SFilter using a simple lamda kernel that calls mandelbrotAt
  SFilter mandelbrotfilter(
    [&extent,&tlc,&brc,depth] (auto& it, auto&, auto& out)
    {
       out.intensity(
         mandelbrotAt(
                      mandelbrotCoords(extent,tlc,brc,it.pos()),
                      {0,0}, 0, depth
                     )
                    );
    }
                         );
  // execute the algorithm
  target.progress(-1);
  mandelbrotfilter({target,target}); // input and output space are the same
  target.refresh(true);
  target.progress(100);
}

int main(int argc, char **argv){
  if (!SimulacrumLibrary::init()) {
    std::cerr << "Simulacrum library initialisation failure!" << std::endl;
    exit(6);
  }
  if (argc < 8){
    std::cout << argv[0] << ": ERROR, insufficient parameters." << std::endl;
    std::cout << argv[0]
      << " <xsize> <ysize> <tlc-x> <tlc-y> <brc-x> <brc-y> <iterations>"
      << std::endl;
    return 1;
  }

  /* get parameters */
  int xres, yres, depth;
  SComplex tlc(0,0), brc(0,0);
  SVector::Precision tmp;
  std::stringstream inp;
  inp << argv[1];
  inp >> xres;
  inp.clear();
  inp << argv[2];
  inp >> yres;
  inp.clear();
  inp << argv[3];
  inp >> tmp;
  tlc.r(tmp);
  inp.clear();
  inp << argv[4];
  inp >> tmp;
  tlc.i(tmp);
  inp.clear();
  inp << argv[5];
  inp >> tmp;
  brc.r(tmp);
  inp.clear();
  inp << argv[6];
  inp >> tmp;
  brc.i(tmp);
  inp.clear();
  inp << argv[7];
  inp >> depth;
  inp.clear();

  if ( (xres == 0) || (yres == 0) ) {
    std::cout << argv[0] << ": ERROR, output width/height cannot be zero!"
              << std::endl;
    return 2;
  }

  SSpace      RenderSpace;  /* a render area */
  SPNG        output;       /* a png image write */
  SPool       bgpool(1);    /* a thread pool to render in the background */
  output.setLocation("mandelbrot.png");

  /* configure the space */
  RenderSpace.resize({xres,yres});

  /* call the draw method */
  bgpool.addJob(  [&RenderSpace,&tlc, &brc, &depth] 
                  { drawMandelbrot(RenderSpace,tlc,brc,depth); }
               );
  bgpool.addJob( [&RenderSpace, &output] { output.storeSSpace(RenderSpace); });

  return 0;
}
