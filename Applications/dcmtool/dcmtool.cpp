/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdlib.h>
#include <Resources/DICOM/sdicom.h>
#include <Resources/DICOM/sarch.h>
#include <Resources/DICOM/sdicom-net.h>
#include <Core/slicer/slicer.h>
#include <Toolbox/parseArgs/parseArgs.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/cmdProgress/cmdProgress.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SLogger/slogger.h>
#include <Resources/spbm.h>
#include <Core/definitions.h>
#include <Core/sabtree.h>
#include <Core/error.h>

/* DICOM Tool
 */

#ifndef DCMTOOL_LIB_INC
#define DCMTOOL_LIB_INC

using namespace Simulacrum;

static std::string AppName = "dcmtool";

void doError(const std::string &error, int exitcode = 1) {
  std::cerr << AppName << ": " << error << std::endl;
  if (exitcode != 666)
    exit(exitcode);
}

void printTag(DCMTag& thetag,const std::string& filter,
              unsigned depth, bool showstructure, bool dataonly = false){
  if (!thetag.matches(filter,true))
    return;
  if (!dataonly) {
    SDICOM::printTag(thetag,true,true,true,depth,showstructure);
    tagset_t *TagChildren;
    TagChildren = &thetag.getTags();
    if (TagChildren->size() > 0){
      tagset_t::iterator it;
      for ( it=TagChildren->begin() ; it < TagChildren->end(); it++ )
        printTag(**it,filter,depth+1,showstructure);
    }
  }
  else {
    std::cerr << thetag.toString() << std::endl;
  }
}

void sniffDICOM(const std::string &potential, bool verbose) {
  SDICOM potentialDCM;
  potentialDCM.setLocation(potential);
  if (verbose) {
    std::cout << potential << '\t';
    if (potentialDCM.isValid())
      std::cout << "is";
    else
      std::cout << "is NOT";
    std::cout << " a DICOM file." << std::endl;
  }
  else {
    if (potentialDCM.isValid())
      std::cout << potentialDCM.getLocation() << std::endl;
  }
}

void sniffDICOMIdent(const std::string &potential, bool verbose) {
  SDICOM potentialDCM;
  potentialDCM.setLocation(potential);
  if (potentialDCM.isValid()) {
    potentialDCM.loadAllTags(false);
    bool deident = DCMDataDic::isTagTreeDeidentified(potentialDCM.getRootTag());
    if (!deident) {
      std::cout << potential;
      if (verbose) {
        std::string PatientData = "[";
        if (potentialDCM.getRootTag().hasTag(PatientName_)) {
          PatientData = PatientData +
               potentialDCM.getRootTag().getTag(PatientName_).toString() + ", ";
        }
        if (potentialDCM.getRootTag().hasTag(PatientSex_)) {
          PatientData = PatientData +
                potentialDCM.getRootTag().getTag(PatientSex_).toString() + ", ";
        }
        if (potentialDCM.getRootTag().hasTag(PatientDOB_)) {
          PatientData = PatientData +
                       potentialDCM.getRootTag().getTag(PatientDOB_).toString();
        }
        std::cout << "\t" << PatientData << "]";
      }
      std::cout << std::endl;
    }
  }
}

void doDump(parseArgs &Arguments, SDICOM &todump, bool verbose) {
  if (verbose)
    std::cout << todump.getLocation() << std::endl;
  std::vector<std::string> filters;
  filters = Arguments.getArg("FilterString").Gobbled;
  if (filters.size() == 0)
    filters.push_back("");
  for (unsigned tag = 0; tag < todump.getTags().size(); tag++ )
    for (unsigned fil=0; fil<filters.size();fil++)
      printTag(*(todump.getTags()[tag]),
              filters[fil],0,
              Arguments.getArg("ShowStructure").isSet);
}

void doArchList(SDCMArchNode &archnode, int depth, const std::string& filter) {
  std::string childfilter = filter;
  if ( (filter.size() == 0) ||
       (archnode.NodeName().find(filter)  != std::string::npos) ||
       (archnode.NodeValue().find(filter) != std::string::npos)
     ) {
    childfilter = "";
    for (int i=0; i<depth; i++)
      std::cout << " |";
    std::cout << archnode.NodeName() << "\t" << archnode.NodeValue()
              << std::endl;
  }
  SDCMArchNodeMap_t::iterator chit;
  for (chit  = archnode.getChildNodesMap().begin();
       chit != archnode.getChildNodesMap().end();
       chit++) {
    if (depth >= 0)
      doArchList(*(chit->second),depth+1,childfilter);
    else
      doArchList(*(chit->second),depth,childfilter);
  }
}

void doWrite(parseArgs &Arguments, SDICOM &towrite) {
  if (Arguments.getArg("XSyntax").isParsed)
    towrite.setTransferSyntax(Arguments.getArg("XSyntax").Name);
  SDICOM::standardizeTag(towrite.getRootTag(),true);
  if (Arguments.getArg("JPEGLS").isSet) {
    SLogger::global().addMessage(
                         "DCMTool::doWrite:: applying JPEGLS compression to: " +
                            towrite.getLocation());
    towrite.compressJPEGLS();
  }
  towrite.writeAllTags();
}

bool checkValid(SDICOM &DCMFile, bool verbose = true) {
  if (!DCMFile.isValid()) {
    if (verbose)
      doError("Error: not a DICOM file! (" +
              DCMFile.getLocation() +")",666);
    return false;
  }
  else
    return true;
}

const dcm_info_store& doLookup(DCMDataDic &dict,const std::string& lookupkey) {
  std::size_t seppos = lookupkey.find(',');
  if ( seppos == std::string::npos) {
    return dict.getEntry(lookupkey);
  }
  else {
    DCMTag tmptag;
    tmptag.setID(lookupkey);
    return dict.getEntry(tmptag.getID());
  }
}

std::string sanitizePath(std::string origpath, DCMDataDic& srcdatadic) {
  SURI orig(origpath);
  SURI targ;
  for (unsigned i=0; i<orig.depth(); i++) {
    DCMTag sanit;
    sanit.setID(doLookup(srcdatadic,orig.getComponent(i)).ID);
    targ.addComponentBack(sanit.getIDstr());
  }
  return targ.getURI();
}

void doGetPath(std::vector<std::string>&files,std::vector<std::string>&paths,
               std::vector<std::string>&filesout, bool verbose,
               DCMDataDic& datadic, bool dataonly = false){
  SDICOM DCMFile;
  bool storetofile = (filesout.size() == paths.size()) && (files.size() == 1);
  for (unsigned arg=0; arg < files.size(); arg++) {
    DCMFile.clear();
    DCMFile.setLocation(files[arg]);
    if (checkValid(DCMFile,verbose)){
      try {
        DCMFile.loadAllTags();
      }
      catch (std::exception &e) {
        doError("Reading ERROR: " + std::string(e.what()),666);
      }
      for (unsigned path=0; path < paths.size(); path++) {
        std::string actpath = sanitizePath(paths[path],datadic);
        if (DCMFile.getRootTag().pathExists(actpath)) {
          DCMTag &thetag = DCMFile.getRootTag().getByPath(actpath);
          if (storetofile) {
            SFile outfile(filesout[path]);
            std::string datatowrite(thetag.getData(),
                                    thetag.getDataLengthUnpadded());
            outfile.fromString(datatowrite);
          }
          else {
            printTag(thetag,"",0,true,dataonly);
          }
        }
      }
    }
  }
}

bool doArchiveImport(SDICOMArch& target,std::vector<std::string>&paths,
                     bool verbose) {
  bool errors = false;
  if (!verbose)
    cmdProgress::progressOut(-1);
  for (unsigned i=0; i<paths.size(); i++) {
    try {
      target.importFile(paths[i]);
      if (verbose)
        std::cout << paths[i] << std::endl;
      else {
        cmdProgress::progressOut((int)(((float)i/(float)paths.size())*100)+1,70);
      }
    }
    catch (std::exception &e) {
      errors = true;
      if (!verbose) std::cerr << std::endl;
      std::cerr << "ERROR: " << paths[i] << std::endl << "    " << e.what()
                << std::endl;
    }
  }
  if (!verbose) {
    cmdProgress::progressOut(100,70);
    cmdProgress::progressOut(-1);
  }
  return errors;
}

void doDicOut(DCMDataDic &dict,const std::string& lookupkey) {
  try {
    const dcm_info_store& entry =
    doLookup(dict,lookupkey);
    DCMTag tmptag; tmptag.setID(entry.ID);
    std::cout << lookupkey << ": " << std::endl
              << " Key:  " <<  std::hex << std::setfill('0')
                           <<  std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
                           << tmptag.getID1() << ","
                           <<  std::setfill('0')
                           <<  std::setw(sizeof(DICOM_ID_PART_LENGTH)*2)
                           << tmptag.getID2() << std::endl
              << " VR:   " << entry.VR << std::endl
              << " Name: " << entry.Name << std::endl;
  }
  catch (std::exception &e){
    doError("Key not found: " + lookupkey,666);
  }
}

void doModify(std::vector<std::string> &keys,std::vector<std::string> &values,
              std::vector<std::string> &files, bool doinsert, bool verbose,
              DCMDataDic& srcdatadic, bool valisfile = false) {
  SDICOM filereader;
  if (keys.size() == values.size()) {
    for (unsigned file=0; file <files.size(); file++) {
      filereader.clear();
      filereader.setLocation(files[file]);
      if (filereader.isValid()) {
        filereader.loadAllTags(true);
        int modcount = 0;
        // a valid DICOM file
        DCMTag &dcmroot = filereader.getRootTag();
        for (unsigned key=0;key<keys.size();key++) {
          std::string valtostore = values[key];
          if (valisfile) {
            SFile fromfile(valtostore);
            if (fromfile.exists())
              valtostore = fromfile.toString();
          }
          std::string lookupath = sanitizePath(keys[key],srcdatadic);
          if (!doinsert || dcmroot.pathExists(lookupath)) {
            // only modify an existing tag
            if (dcmroot.pathExists(lookupath)) {
              DCMTag &modtag= dcmroot.getByPath(lookupath);
              modtag.fromString(valtostore);
              modcount++;
            }
          }
          else {
            // find the parent tag, insert as necessary
            SURI path(lookupath);
            std::string actualkey;
            if (path.depth() > 0) {
              actualkey = path.getComponent(path.depth()-1);
              path.deleteComponent(path.depth()-1);
              if (dcmroot.pathExists(path.getURI())) {
                DCMTag &dcmins = dcmroot.getByPath(path.getURI());
                DCMTag *newtag = new DCMTag();
                newtag->setID(actualkey);
                newtag->setVR(doLookup(srcdatadic,actualkey).VR[0],
                              doLookup(srcdatadic,actualkey).VR[1]);
                newtag->fromString(valtostore);
                dcmins.addTag(newtag);
                modcount++;
              }
            }
          }
        }
        if (modcount > 0) {
          if (verbose)
            std::cout << "Modifying file: " << filereader.getLocation()
                      << std::endl; 
          filereader.writeAllTags();
        }
        else {
          if (verbose)
            std::cout << "NOT Modifying file: " << filereader.getLocation()
                      << std::endl;
        }
      }
      else {
        if (verbose)
          std::cerr << "WARNING: not a DICOM file - : " << files[file]
                    << std::endl;
      }
    }
  }
  else {
    doError("Number of keys does not match number of a values!");
  }
}

void doRemove(std::vector<std::string> &keys, std::vector<std::string> &files,
              bool verbose, DCMDataDic& srcdatadic) {
  SDICOM filereader;
  for (unsigned file=0; file <files.size(); file++) {
    filereader.clear();
    filereader.setLocation(files[file]);
    if (filereader.isValid()) {
      filereader.loadAllTags(true);
      int modcount = 0;
      DCMTag &dcmroot = filereader.getRootTag();
      for (unsigned key=0;key<keys.size();key++) {
        std::string lookupath = sanitizePath(keys[key],srcdatadic);
        if (dcmroot.pathExists(lookupath)) {
          dcmroot.getByPath(lookupath).removeNode();
          modcount++;
        }
      }
      if (modcount > 0) {
          if (verbose)
            std::cout << "Modifying file: " << filereader.getLocation()
                      << std::endl; 
          filereader.writeAllTags();
        }
        else {
          if (verbose)
            std::cout << "NOT Modifying file: " << filereader.getLocation()
                      << std::endl;
        }
    }
  }
}

static void smartResourceStore(SResource* target, int countval) {
  static int        counter;
  static SLockable  counterlock;
  static SLockable  countdownlock;
  counterlock.lock();
  counter = countval;
  counterlock.unlock();
  bool gotlock = countdownlock.try_lock();
  if (gotlock) {
    while(true) {
      sysInfo::sleep(1000);
      counterlock.lock();
      if (counter > 0) {
        counter--;
        counterlock.unlock();
      }
      else {
        try {
          SLogger::global().addMessage("DCMTool::smartResourceStore: Storing: "+
                                       target->getLocation());
          target->store();
        }
        catch (std::exception &e) {
          SLogger::global().addMessage("DCMTool::smartResourceStore: ERROR: "+
                                       std::string(e.what()));
        }
        counterlock.unlock();
        break;
      }
    }
    countdownlock.unlock();
  }
}

void doDCMCallback(std::string cmd, bool doimport, SDICOMArch* arch,
                   SDICOM* sourcedcm) {
  std::string path   = sourcedcm->getLocation();
  std::string actcmd = cmd;
  if (actcmd.size() > 0) {
    const std::string fileplaceholder("%f");
    std::size_t filerepref = actcmd.find(fileplaceholder);
    while (filerepref != std::string::npos) {
      actcmd.replace(filerepref,fileplaceholder.size(),path);
      filerepref = actcmd.find(fileplaceholder);
    }
    SLogger::global().addMessage("DCMTool::Executing: " + actcmd);
    if(system(actcmd.c_str()) != 0) {
      SLogger::global().addMessage
                               ("DCMTool::Executing: Reported ERROR:" + actcmd);
    }
  }
  if (doimport) {
    try {
      SLogger::global().addMessage("DCMTool::Importing: " +
                                                      sourcedcm->getLocation());
      arch->importFile(sourcedcm->getLocation());
      smartResourceStore(arch,5);
    }
    catch (std::exception &e) {
      SLogger::global().addMessage("DCMTool::Importing: ERROR: " +
                                                      sourcedcm->getLocation());
    }
  }
  delete sourcedcm; // must free object, by callback contract
}

void doWritePPM(std::vector<std::string>& dicomfiles,
                std::string &outfile, bool verbose, bool usewl = false,
                long int wlcentre=0, long int wlwidth=0) {
  for (unsigned file = 0; file < dicomfiles.size(); file++) {
    SDICOM  filesrc;
    SSpace  IMGsrc;
    SPBM    IMGout;
    SSlicer IMGSlice(IMGsrc);
    filesrc.setLocation(dicomfiles[file]);
    if (filesrc.isValid()) {
      filesrc.loadAllTags(true);
      filesrc.loadSSpace(IMGsrc);
      if (usewl)
        IMGsrc.LUT().adjLUT(wlcentre,wlwidth);
      for (unsigned slice=0; slice < IMGSlice.depth(); slice++) {
        std::stringstream slicestr;
        slicestr << "_" << std::setw(4) << std::setfill('0') << slice << "_" ;
        IMGSlice.setSlice(slice);
        if ( (dicomfiles.size() == 1) && (outfile.size() > 0) )
          IMGout.setLocation(outfile + slicestr.str());
        else
          IMGout.setLocation(dicomfiles[file] + slicestr.str() + ".ppm");
        if (IMGout.storeSSpace(IMGSlice) != 0 )
          std::cerr << "ERROR writing: " << IMGout.getLocation() << std::endl;
      }
    }
    else {
      if (verbose)
        std::cout << "Not DICOM: " << dicomfiles[file] << std::endl;
    }
  }
}

int doGetArgs(std::vector<std::string> &MainArgs, std::string output,
               bool verbose) {
  int errcnt = 0;
  for (unsigned arg=0; arg < MainArgs.size(); arg++) {
    SURII getfileuri(MainArgs[arg]);
    if (getfileuri.depth() > 0) {
      if (verbose)
        std::cout << "Retrieving file: " << getfileuri.getURI() << std::endl;
      if (output.size() > 0) {
        try {
          getfileuri.toFile(output);
          if (verbose)
            std::cout << "Stored as: " << output << std::endl;
        }
        catch (std::exception&) {
          std::cerr << "ERROR retrieving: " << getfileuri.getURI() << std::endl;
        }
        break;
      }
      else {
        try {
          getfileuri.toFile(getfileuri.getComponent(getfileuri.depth()-1));
          if (verbose)
            std::cout << "Stored as: "
                      << getfileuri.getComponent(getfileuri.depth()-1)
                      << std::endl;
        }
        catch (std::exception&) {
          std::cerr << "ERROR retrieving: " << getfileuri.getURI() << std::endl;
          errcnt++;
        }
      }
    }
  }
  return errcnt;
}

int doPutArgs(std::vector<std::string> &MainArgs, std::string target,
               bool verbose) {
  int errcnt = 0;
  for (unsigned arg=0; arg < MainArgs.size(); arg++) {
    SURII targeturi(target);
    if (targeturi.depth() > 0) {
      if (verbose)
        std::cout << AppName <<": Putting file: " << MainArgs[arg] << std::endl;
      if (target.size() > 0) {
        try {
          SFile source(MainArgs[arg]);
          std::stringstream memobj;
          source.toStream(memobj);
          targeturi.fromMem(memobj.str());
          if (verbose)
            std::cout << AppName <<": Put at: " << target << std::endl;
        }
        catch (std::exception &e) {
          std::cerr << "ERROR: " << MainArgs[arg] << " -- " << e.what()
                    << std::endl;
        }
        break;
      }
      else {
        std::cerr << "ERROR: no target specified" << std::endl;
      }
    }
  }
  return errcnt;
}

void doDCMRelay(std::string target, bool remove, bool verbose,
                SDICOM *srcdicom) {
  static SLockable oneatatime;
  oneatatime.lock();
  std::vector<std::string> putlist;
  putlist.push_back(srcdicom->getLocation());
  doPutArgs(putlist,target,verbose);
  if (remove) {
    SFile remfile(srcdicom->getLocation());
    try {
      remfile.deleteFromDisk();
    }
    catch (std::exception&) { }
  }
  delete srcdicom;
  oneatatime.unlock();
}

void inlinepseudon(SResource *storer, DCMDataDic *DCMDic, NNode *PseudonDB,
                   bool rempriv, bool force, SDICOM *DCMFile,
                   std::function<void(SDICOM*)> nextfunc) {
  storer->lock();
  if (!DCMDataDic::isTagTreeDeidentified(DCMFile->getRootTag()) ||
      force ) {
    DCMDic->deidentifyTagTree(DCMFile->getRootTag(),*PseudonDB,
                              rempriv,true);
    if (!DCMDataDic::isTagTreeDeidentified(DCMFile->getRootTag()))
      DCMDataDic::setTagTreeDeidentified(DCMFile->getRootTag(),true);
    DCMFile->store();
  }
  storer->store();
  storer->unlock();
  if (nextfunc != nullptr)
    nextfunc(DCMFile);
}

void doRecurseArgs(parseArgs &Arguments,
                   std::vector<std::string> &MainArgs,
                   bool recurse, bool verbose, bool dcmonly = false) {
  SDICOM DCMCheck;
  SURI   RemoteCheck;
  // construct recursed argument tree, if necessary
  if (verbose)
    std::cout << "Constructing file list... " << std::flush;
  for (unsigned gob=0;
         gob<Arguments.getArg("FileNames").Gobbled.size();
         gob++) {
    try {
      if (dcmonly) {
        RemoteCheck.setURI(Arguments.getArg("FileNames").Gobbled[gob]);
        DCMCheck.setLocation(Arguments.getArg("FileNames").Gobbled[gob]);
        if ( (!RemoteCheck.isLocal()) || checkValid(DCMCheck,false))
          MainArgs.push_back(Arguments.getArg("FileNames").Gobbled[gob]);
      }
      else
        MainArgs.push_back(Arguments.getArg("FileNames").Gobbled[gob]);
      if (SFile(Arguments.getArg("FileNames").Gobbled[gob]).isDIR()) {
        std::vector<std::string> recursed_args =
          SFile(Arguments.getArg("FileNames")
            .Gobbled[gob])
            .getDIRContents(recurse);
        for (unsigned rc=0; rc<recursed_args.size();rc++) {
          try {
            if (!SFile(recursed_args[rc]).isDIR()) {
              if (dcmonly) {
                DCMCheck.setLocation(recursed_args[rc]);
                if (checkValid(DCMCheck,false))
                  MainArgs.push_back(recursed_args[rc]);
              }
              else
                MainArgs.push_back(recursed_args[rc]);
            }
          }
          catch (std::exception &e) {
            std::cerr << "ERROR recursing at: " << recursed_args[rc]
                      << std::endl;
            std::cerr << e.what() << std::endl;
          }
        }
      }
    }
    catch (std::exception &e) {
      std::cerr << "ERROR recursing at: "
                << Arguments.getArg("FileNames").Gobbled[gob] << std::endl;
      std::cerr << e.what() << std::endl;
    }
  }
  if (verbose)
    std::cout << "Done. (" << MainArgs.size() << " files)" << std::endl;
}

// decode the specified action
int doAction(parseArgs& Arguments) {
  bool verbose = Arguments.getArg("Verbose").isSet;
  bool recurse = Arguments.getArg("Recurse").isSet;
  int errors = 0;
  SDICOM     DCMFile;
  DCMDataDic DCMDic;
  SURI       DCMURI;
  DCMFile.setDataDictionary(&DCMDic);
  std::vector<std::string> MainArgs;
  // re-route log messages to stdout in verbose mode
  if (verbose)
    SLogger::global().setStream(std::cerr);
  // load a custom datadictionary now
  if (Arguments.getArg("DataDictionary").isParsed)
    DCMDic.loadFromFile(Arguments.getArg("DataDictionary").Value);
  // process the various actions
  if (Arguments.getArg("Action").Value == "dump") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      DCMURI.clear();
      DCMURI.setURI(MainArgs[arg]);
      SFile dcmfileresolver(DCMURI.file());
      DCMFile.clear();
      DCMFile.setLocation(dcmfileresolver.getLocation());
      if (checkValid(DCMFile,verbose)){
        try {
          DCMFile.loadAllTags(false);
        }
        catch (std::exception &e) {
          doError("Reading ERROR: " + std::string(e.what()),666);
        }
        doDump(Arguments, DCMFile, verbose);
      }
      else errors++;
    }
  }
  else if (Arguments.getArg("Action").Value == "list") {
    // list the contents of an archive
    SDICOMArch  listarch;
    std::string filter;
    int         startdepth = 0;
    if (! Arguments.getArg("ShowStructure").isSet)
      startdepth = -1;
    if (Arguments.getArg("FilterString").isParsed) {
     if (Arguments.getArg("FilterString").Gobbled.size() > 0) {
       filter = Arguments.getArg("FilterString").Gobbled[0];
     }
     if (Arguments.getArg("FilterString").Gobbled.size() > 1) {
       doError("Only one filter allowed for archive listing");
     }
    }
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      listarch.setLocation(MainArgs[arg]);
      if (listarch.isValid()) {
        listarch.loadArchive();
        doArchList(listarch.getRootNodeChild(),startdepth,filter);
      }
      else {
        errors++;
      }
    }
  }
  else if (Arguments.getArg("Action").Value == "get") {
    MainArgs = Arguments.getArg("FileNames").Gobbled;
    errors += doGetArgs(MainArgs,
                        Arguments.getArg("OutputFile").Value,
                        verbose);
  }
  else if (Arguments.getArg("Action").Value == "put") {
    MainArgs = Arguments.getArg("FileNames").Gobbled;
    errors += doPutArgs(MainArgs,
                        Arguments.getArg("NetworkTarget").Value,
                        verbose);
  }
  else if (Arguments.getArg("Action").Value == "path") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    doGetPath(MainArgs,Arguments.getArg("DICOMKey").Gobbled,
              Arguments.getArg("FileValue").Gobbled,verbose,
              DCMDic,Arguments.getArg("Raw").isSet);
  }
  else if (Arguments.getArg("Action").Value == "sniff") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++)
          sniffDICOM(MainArgs[arg], verbose);
  }
  else if (Arguments.getArg("Action").Value == "sniffident") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++)
          sniffDICOMIdent(MainArgs[arg], verbose);
  }
  else if (Arguments.getArg("Action").Value == "rewrite") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      DCMFile.clear();
      DCMFile.setLocation(MainArgs[arg]);
      if (checkValid(DCMFile,verbose)) {
        DCMFile.loadAllTags(true); // (true loads pixel data)
        if (Arguments.getArg("OutputFile").isParsed)
          DCMFile.changeLocation(Arguments.getArg("OutputFile").Value);
        doWrite(Arguments,DCMFile);
      }
    }
  }
  else if (Arguments.getArg("Action").Value == "pseudon") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    NNodeResource PseudonDBResource;
    bool removeprivate = true;
    if (Arguments.getArg("KeepPrivate").isSet)
      removeprivate = false;
    if (Arguments.getArg("PseudoDB").isParsed) {
      PseudonDBResource.setLocation(Arguments.getArg("PseudoDB").Value);
      if (PseudonDBResource.isValid())
        PseudonDBResource.load();
    }
    NNode &PseudonDBRootNode = PseudonDBResource.getRootNode();
    NNode *PseudonDB;
    if ( PseudonDBRootNode.hasChildNode(DCMDataDic::pseudonDBName()) )
      PseudonDB = &PseudonDBRootNode.getChildNode(DCMDataDic::pseudonDBName());
    else {
      PseudonDB = new NNode;
      PseudonDB->setName("Simulacrum-PseudoDB");
      PseudonDBRootNode.addChildNode(PseudonDB);
    }
    cmdProgress::progressOut(-1);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      DCMFile.clear();
      DCMFile.setLocation(MainArgs[arg]);
      if (checkValid(DCMFile,verbose)) {
        DCMFile.loadAllTags(true); // (true loads pixel data)
        if (Arguments.getArg("OutputFile").isParsed)
          DCMFile.changeLocation(Arguments.getArg("OutputFile").Value);
        if (!DCMDataDic::isTagTreeDeidentified(DCMFile.getRootTag()) ||
             Arguments.getArg("Force").isSet ) {
          if (Arguments.getArg("Force").isSet)
            if(verbose &&
               DCMDataDic::isTagTreeDeidentified(DCMFile.getRootTag()))
              std::cerr <<  DCMFile.getLocation()
                        <<": Forced deidentification of (apparently) "
                        << "already deidentified DICOM object" << std::endl;
          DCMDic.deidentifyTagTree(DCMFile.getRootTag(),*PseudonDB,
                                   removeprivate,true);
          if (!DCMDataDic::isTagTreeDeidentified(DCMFile.getRootTag()))
            DCMDataDic::setTagTreeDeidentified(DCMFile.getRootTag(),true);
          DCMFile.store();
        }
        else
          if (verbose)
            std::cerr <<  DCMFile.getLocation()
                      <<  ": Already deidentified; skipping" << std::endl;
      }
      cmdProgress::progressOut
                          ((int)(((float)arg/(float)MainArgs.size())*100)+1,70);
    }
    if (Arguments.getArg("PseudoDB").isParsed)
      PseudonDBResource.store();
    cmdProgress::progressOut(100,70);
    cmdProgress::progressOut(-1);
  }
  else if (Arguments.getArg("Action").Value == "lookup") {
    for (unsigned arg=0;
         arg < Arguments.getArg("DICOMKey").Gobbled.size();
         arg++)
      doDicOut(DCMDic,Arguments.getArg("DICOMKey").Gobbled[arg]);
  }
  else if (Arguments.getArg("Action").Value == "import") {
    SDICOMArch TargetArch;
    bool       Exists;
    if (Arguments.getArg("SDCMArchive").isParsed)
      TargetArch.setLocation(Arguments.getArg("SDCMArchive").Value);
    else {
      doError("Error - No archive specified");
    }
    // load an existing archive
    if (TargetArch.isValid()) {
      TargetArch.loadArchive();
      Exists = true;
    }
    else {
      Exists = false;
    }
    if(Arguments.getArg("ArchiveTitle").isParsed || (!Exists)) {
      if (verbose)
        std::cout << "Setting archive title: "
                  << Arguments.getArg("ArchiveTitle").Value << std::endl;
      TargetArch.setTitle(Arguments.getArg("ArchiveTitle").Value);
    }
    if(Arguments.getArg("ArchivePath").isParsed  || (!Exists)) {
      if (verbose)
        std::cout << "Setting Archive Import Path Directive: "
                  << Arguments.getArg("ArchivePath").Value << std::endl;
      TargetArch.setImportDirective(Arguments.getArg("ArchivePath").Value);
    }
    if(Arguments.getArg("ArchiveBase").isParsed  || (!Exists)) {
      std::string archbasepath = "";
      if (Arguments.getArg("ArchiveBase").isParsed)
        archbasepath = Arguments.getArg("ArchiveBase").Value;
      else
        if (!Exists) {
          if (Arguments.positonals() > 0) {
            SURI  animppath(Arguments.getPositional(0).Value);
            SFile testfile (animppath.getURI());
            if (!testfile.isDIR())
              animppath.deleteComponent(animppath.depth()-1);
            archbasepath = animppath.getURI();
          }
        }
      if(verbose)
        std::cout << "Setting Archive Base: " << archbasepath << std::endl;
      TargetArch.setFSBase(archbasepath);
    }
    doRecurseArgs(Arguments,MainArgs,recurse,verbose,true);
    errors += doArchiveImport(TargetArch,MainArgs,verbose);
    if(verbose)
      std::cout << "Writing archive...";
    TargetArch.storeArchive();
    if(verbose)
      std::cout << " done." << std::endl;
  }
  else if (Arguments.getArg("Action").Value == "xml") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      DCMFile.clear();
      DCMFile.setLocation(MainArgs[arg]);
      if (checkValid(DCMFile,verbose)){
        try {
          DCMFile.loadAllTags(false);
        }
        catch (std::exception &e) {
          doError("Reading ERROR: " + std::string(e.what()),666);
          continue;
        }
        NNodeResource xmlout;
        NNode *tagsroot = new NNode();
        if (Arguments.getArg("OutputFile").isParsed)
          xmlout.setLocation(Arguments.getArg("OutputFile").Value);
        else
          xmlout.setLocation(MainArgs[arg] + ".xml");
        SAbsTreeNodeConversions::DCMTagToNNode
                               (DCMFile.getRootTag(),*tagsroot,true);
        xmlout.getRootNode().addChildNode(tagsroot);
        xmlout.store();
      }
      else errors++;
    }
  }
  else if (Arguments.getArg("Action").Value == "dumpdict") {
    std::string outfile = "dictionary.xml";
    if (Arguments.getArg("OutputFile").isParsed)
      outfile = Arguments.getArg("OutputFile").Value;
    DCMDic.storeToFile(outfile);
  }
  else if (Arguments.getArg("Action").Value == "sysinfo") {
    std::cout << sysInfo::sysInfoString() << std::endl;
  }
  else if (Arguments.getArg("Action").Value == "receive" ||
           Arguments.getArg("Action").Value == "relay" ) {
    SDICOM_Server DCMServer;
    SDICOMArch    DCMArchive;
    bool          doimport = false;
    bool          dorelay  = Arguments.getArg("Action").Value == "relay";
    bool          dopseudo = Arguments.getArg("NetPseudon").isSet;
    // inline pseudon stuff-----------------------------------------------------
    NNodeResource PseudonDBResource;
    bool          removeprivate = true;
    NNode        *PseudonDBRootNode, *PseudonDB;
    if (dopseudo) {
      if (Arguments.getArg("KeepPrivate").isSet)
        removeprivate = false;
      if (Arguments.getArg("PseudoDB").isParsed) {
        PseudonDBResource.setLocation(Arguments.getArg("PseudoDB").Value);
        if (PseudonDBResource.isValid())
          PseudonDBResource.load();
      }
      PseudonDBRootNode = &PseudonDBResource.getRootNode();
      if ( PseudonDBRootNode->hasChildNode(DCMDataDic::pseudonDBName()) )
        PseudonDB =
                &(PseudonDBRootNode->getChildNode(DCMDataDic::pseudonDBName()));
      else {
        PseudonDB = new NNode;
        PseudonDB->setName("Simulacrum-PseudoDB");
        PseudonDBRootNode->addChildNode(PseudonDB);
      }
    }
    // -------------------------------------------------------------------------
    // check to see if we should load and import to an archive
    if (Arguments.getArg("SDCMArchive").isParsed && (!dorelay) ) {
      DCMArchive.setLocation(Arguments.getArg("SDCMArchive").Value);
      if (DCMArchive.isValid()) {
        SLogger::global().addMessage("dcmtool: Loading existing archive: " +
                                     Arguments.getArg("SDCMArchive").Value);
        try {
          DCMArchive.load();
          doimport = true;
        }
        catch (std::exception &e) {
          doError(e.what());
        }
      }
      else {
        SLogger::global().addMessage("dcmtool: Creating new archive: " +
                                     Arguments.getArg("SDCMArchive").Value);
        DCMArchive.setTitle(Arguments.getArg("ArchiveTitle").Value);
        DCMArchive.setFSBase(Arguments.getArg("ArchiveBase").Value);
        DCMArchive.setImportDirective(Arguments.getArg("ArchivePath").Value);
        try {
          DCMArchive.store();
          doimport = true;
        }
        catch (std::exception &e) {
          doError(e.what());
        }
      }
    }
    std::stringstream conv;
    short port;
    conv << Arguments.getArg("NetworkPort").Value;
    conv >> port;
    if (Arguments.getArg("OutputFile").isParsed) {
      SFile dircheck(Arguments.getArg("OutputFile").Value);
      if (dircheck.isDIR())
        DCMServer.setTargetStoreDirectory(Arguments.getArg("OutputFile").Value);
      else
        doError(Arguments.getArg("OutputFile").Value + " is not a directory");
    }
    else
      DCMServer.setTargetStoreDirectory("./");
    DCMServer.setPort(port);
    DCMServer.setDiskStoreMode(true);
    DCMServer.setAETitle(Arguments.getArg("AETitle").Value);
    DCMServer.setRestrictedMode(true);
    std::function<void(SDICOM*)> callback = nullptr;
    if ( (Arguments.getArg("Exec").isParsed || doimport) & (!dorelay) ) {
      callback = std::bind(doDCMCallback,
                           Arguments.getArg("Exec").Value,
                           doimport,
                           &DCMArchive,
                           std::placeholders::_1);
    }
    else if (dorelay) {
      callback = std::bind(doDCMRelay,
                           Arguments.getArg("NetworkTarget").Value,
                           true,
                           verbose,
                           std::placeholders::_1);
    }
    if (!dopseudo) {
      DCMServer.setReceptionCallback(callback);
    }
    else {
      DCMServer.setReceptionCallback(
        std::bind(inlinepseudon,&PseudonDBResource,&DCMDic,
                  PseudonDB,removeprivate,
                  Arguments.getArg("Force").isSet, std::placeholders::_1,
                  callback)
      );
    }
    DCMServer.start();
    DCMServer.wait();
  }
  else if (Arguments.getArg("Action").Value == "send") {
    SDICOM_Client DCMClient;
    std::stringstream conv;
    short port;
    bool didassoc = false;
    conv << Arguments.getArg("NetworkPort").Value;
    conv >> port;
    DCMClient.setAETitle(Arguments.getArg("AETitle").Value);
    DCMClient.setTargetAETitle(Arguments.getArg("AETitleTarget").Value);
    DCMClient.setTargetPort(port);
    DCMClient.setTargetAddress(Arguments.getArg("NetworkTarget").Value);
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    SDICOM DCMSendFile;
    if (!verbose)
      cmdProgress::progressOut(-1,70);
    for (unsigned arg=0; arg < MainArgs.size(); arg++) {
      SFile afile(MainArgs[arg]);
      if ( (!afile.exists()) || afile.isDIR() ) {
        SLogger::global().addMessage("DCMTool: skipping non-existant file: "
                                     + MainArgs[arg]);
        continue;
      }
      DCMSendFile.clear();
      DCMSendFile.setLocation(MainArgs[arg]);
      if (!DCMSendFile.isValid()) {
        SLogger::global().addMessage("DCMTool: skipping non-DICOM file: "
                                     + MainArgs[arg]);
        continue;
      }
      DCMSendFile.refresh(true);
      std::stringstream msg;
      msg << "DCMTool: Transmitting file: " << DCMSendFile.getLocation();
      if (!didassoc) { // must associate on first attempt
        if(!DCMClient.associate(DCMSendFile))
          doError("Could not associate!");
        else
          didassoc = true;
      }
      SLogger::global().addMessage(msg.str());
      if (!DCMClient.sendDICOM(DCMSendFile)) {
        SLogger::global().addMessage("DCMTool: Error transmitting: " +
                                      DCMSendFile.getLocation());
        errors++;
      }
      if (!verbose)
        cmdProgress::progressOut
                            ((int)(((float)arg/(float)MainArgs.size())*100),70);
    }
    if (errors == 0) // don't wait for disassociation on a total screw-up
      DCMClient.disassociate();
    if (!verbose) {
      cmdProgress::progressOut(100,70);
      cmdProgress::progressOut(-1);
    }
  }
  else if (Arguments.getArg("Action").Value == "echo") {
    SDICOM_Client DCMClient;
    std::stringstream conv;
    SDICOM exemplardcm;
    DCMTag sopclass;
    short  port;
    // configure a LittleEndianImplit/VerificationSOPClass DICOM object
    // for association and echo request;
    exemplardcm.setTransferSyntax("1.2.840.10008.1.2");
    sopclass.setID(SOPClassUID);
    sopclass.setVR('U','I');
    sopclass.fromString("1.2.840.10008.1.1");
    exemplardcm.getRootTag().addTag(sopclass);
    conv << Arguments.getArg("NetworkPort").Value;
    conv >> port;
    DCMClient.setTargetPort(port);
    DCMClient.setTargetAddress(Arguments.getArg("NetworkTarget").Value);
    DCMClient.setAETitle(Arguments.getArg("AETitle").Value);
    DCMClient.setTargetAETitle(Arguments.getArg("AETitleTarget").Value);
    if (DCMClient.associate(exemplardcm)) {
      if (!DCMClient.echo(&exemplardcm))
        errors++;
    }
    else {
      errors++;
    }
    if ((errors > 0) || !DCMClient.disassociate())
      errors++;
  }
  else if (Arguments.getArg("Action").Value == "find") {
    SDICOM_Client     DCMClient;
    DCMTag            Querykeys, SOPClass;
    SDICOM            AssocDCM, Results;
    DCMTag           *resobj = new DCMTag();
    short             Port;
    int               Qlevel = SDICOM_Server::DCM_STUDY;
    std::stringstream conv;
    Results.addTag(resobj);
    // decode the required query level
    if (Arguments.getArg("DICOMLevel").isParsed &&
        Arguments.getArg("DICOMLevel").Value.size() == 1) {
      switch(Arguments.getArg("DICOMLevel").Value[0]) {
        case 'P':
          Qlevel = SDICOM_Server::DCM_PATIENT;
          break;
        default:
        case 'S':
          Qlevel = SDICOM_Server::DCM_STUDY;
          break;
        case 's':
          Qlevel = SDICOM_Server::DCM_SERIES;
          break;
        case 'I':
          Qlevel = SDICOM_Server::DCM_INSTANCE;
          break;
      }
    }
    // decode any query keys
    for (unsigned arg=0;
         arg < Arguments.getArg("DICOMKey").Gobbled.size();
         arg++) {
      try {
        DCMTag addkey;
        const dcm_info_store &lookupval = doLookup
                             (DCMDic,Arguments.getArg("DICOMKey").Gobbled[arg]);
        addkey.setID(lookupval.ID);
        addkey.setVR(lookupval.VR[0],lookupval.VR[1]);
        addkey.setName(lookupval.Name);
        // set a value if one was specified
        if ( arg < Arguments.getArg("DICOMValue").Gobbled.size() )
          addkey.fromString(Arguments.getArg("DICOMValue").Gobbled[arg]);
        Querykeys.addTag(addkey);
      }
      catch (std::exception&) {
        std::cerr << "Unknown key: " <<
                         Arguments.getArg("DICOMKey").Gobbled[arg] << std::endl;
      }
    }
    // configure the association object
    AssocDCM.setTransferSyntax("1.2.840.10008.1.2"); //LIM
    SOPClass.setID(SOPClassUID);
    SOPClass.setVR('U','I');
    SOPClass.fromString("1.2.840.10008.5.1.4.1.2.1.1"); // PatientRootFind
    AssocDCM.getRootTag().addTag(SOPClass);
    // now configure the query
    conv << Arguments.getArg("NetworkPort").Value;
    conv >> Port;
    DCMClient.setTargetPort(Port);
    DCMClient.setTargetAddress(Arguments.getArg("NetworkTarget").Value);
    DCMClient.setAETitle(Arguments.getArg("AETitle").Value);
    DCMClient.setTargetAETitle(Arguments.getArg("AETitleTarget").Value);
    if (DCMClient.associate(AssocDCM)) {
      if (!DCMClient.find(Querykeys, *resobj, Qlevel)) {
        SLogger::global().addMessage("DCMTool: General error during find");
        errors++;
      }
      else {
        if (Arguments.getArg("OutputFile").isParsed) {
          Results.changeLocation(Arguments.getArg("OutputFile").Value);
          Results.store();
        }
        else {
          doDump(Arguments,Results,false);
        }
      }
    }
    else {
      errors++;
    }
    if ((errors > 0) || !DCMClient.disassociate())
      errors++;
  }
  else if (Arguments.getArg("Action").Value == "move") {
    SDICOM_Client     DCMClient;
    DCMTag            Querykeys, SOPClass;
    SDICOM            AssocDCM;
    short             Port;
    int               Qlevel = SDICOM_Server::DCM_STUDY;
    std::stringstream conv;
    // decode the required query level
    if (Arguments.getArg("DICOMLevel").isParsed &&
        Arguments.getArg("DICOMLevel").Value.size() == 1) {
      switch(Arguments.getArg("DICOMLevel").Value[0]) {
        case 'P':
          Qlevel = SDICOM_Server::DCM_PATIENT;
          break;
        default:
        case 'S':
          Qlevel = SDICOM_Server::DCM_STUDY;
          break;
        case 's':
          Qlevel = SDICOM_Server::DCM_SERIES;
          break;
        case 'I':
          Qlevel = SDICOM_Server::DCM_INSTANCE;
          break;
      }
    }
    // decode any query keys
    for (unsigned arg=0;
         arg < Arguments.getArg("DICOMKey").Gobbled.size();
         arg++) {
      try {
        DCMTag addkey;
        const dcm_info_store &lookupval = doLookup
                             (DCMDic,Arguments.getArg("DICOMKey").Gobbled[arg]);
        addkey.setID(lookupval.ID);
        addkey.setVR(lookupval.VR[0],lookupval.VR[1]);
        addkey.setName(lookupval.Name);
        // set a value if one was specified
        if ( arg < Arguments.getArg("DICOMValue").Gobbled.size() )
          addkey.fromString(Arguments.getArg("DICOMValue").Gobbled[arg]);
        Querykeys.addTag(addkey);
      }
      catch (std::exception&) {
        std::cerr << "Unknown key: " <<
                         Arguments.getArg("DICOMKey").Gobbled[arg] << std::endl;
      }
    }
    // configure the association object
    AssocDCM.setTransferSyntax("1.2.840.10008.1.2"); //LIM
    SOPClass.setID(SOPClassUID);
    SOPClass.setVR('U','I');
    SOPClass.fromString("1.2.840.10008.5.1.4.1.2.1.2"); // PatientRootMove
    AssocDCM.getRootTag().addTag(SOPClass);
    // now configure the query
    conv << Arguments.getArg("NetworkPort").Value;
    conv >> Port;
    DCMClient.setTargetPort(Port);
    DCMClient.setTargetAddress(Arguments.getArg("NetworkTarget").Value);
    DCMClient.setAETitle(Arguments.getArg("AETitle").Value);
    DCMClient.setTargetAETitle(Arguments.getArg("AETitleTarget").Value);
    if (DCMClient.associate(AssocDCM)) {
      if (!DCMClient.move(Querykeys,Arguments.getArg("AETitle").Value, Qlevel)){
        SLogger::global().addMessage("DCMTool: General error during move");
        errors++;
      }
    }
    else {
      errors++;
    }
    if ((errors > 0) || !DCMClient.disassociate())
      errors++;
  }
  else if (Arguments.getArg("Action").Value == "modify" ||
           Arguments.getArg("Action").Value == "insert") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    bool doins = Arguments.getArg("Action").Value == "insert";
    bool dofromfile = false;
    std::vector<std::string> &keys   = Arguments.getArg("DICOMKey").Gobbled;
    std::vector<std::string> *values = &Arguments.getArg("DICOMValue").Gobbled;
    if (Arguments.getArg("FileValue").isParsed && 
        Arguments.getArg("DICOMValue").isParsed ) {
      doError("ERROR: cannot use both DICOMValues and FileValues");
    }
    else {
      if (Arguments.getArg("FileValue").isParsed) {
        dofromfile = true;
        values = &Arguments.getArg("FileValue").Gobbled;
      }
    }
    doModify(keys,*values,MainArgs,doins,verbose,DCMDic,dofromfile);
  }
  else if (Arguments.getArg("Action").Value == "remove") {
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    std::vector<std::string> keys = Arguments.getArg("DICOMKey").Gobbled;
    doRemove(keys,MainArgs,verbose,DCMDic);
  }
  else if (Arguments.getArg("Action").Value == "touch") {
    SDICOM toucher;
    for (unsigned i=0; i<Arguments.getArg("FileNames").Gobbled.size(); i++) {
      SFile filetest(Arguments.getArg("FileNames").Gobbled[i]);
      if (!filetest.exists()) {
        toucher.setLocation(Arguments.getArg("FileNames").Gobbled[i]);
        toucher.writeAllTags();
        if(verbose)
          std::cout << "Touched: " << Arguments.getArg("FileNames").Gobbled[i]
                    << std::endl;
      }
      else {
        if(verbose)
          std::cout << "Skipped: " << Arguments.getArg("FileNames").Gobbled[i]
                    << std::endl;
      }
    }
  }
  else if (Arguments.getArg("Action").Value == "toppm") {
    bool usewl   = false;
    long int wlc = 0;
    long int wlw = 0;
    if (Arguments.getArg("WLC").isParsed && Arguments.getArg("WLW").isParsed) {
      usewl = true;
      std::stringstream wlwidth(Arguments.getArg("WLW").Value);
      wlwidth >> wlw;
      std::stringstream wlcentre(Arguments.getArg("WLC").Value);
      wlcentre >> wlc;
      if (verbose)
        std::cout << "Using WLCentre,WLWidth: " << wlc << "," << wlw 
                  << std::endl;
    }
    doRecurseArgs(Arguments,MainArgs,recurse,verbose);
    doWritePPM(MainArgs,Arguments.getArg("OutputFile").Value, verbose,
               usewl, wlc, wlw);
  }
  else {
    doError("Unrecognised action ("+Arguments.getArg("Action").Value+")",666);
    errors++;
  }
  // output some general epilogue information
  if (verbose) {
    std::cout << "Total number of files considered: "
              << std::dec
              << MainArgs.size()
              << std::endl;
  }
  return errors;
}

#endif //libinclusion

#ifndef DCMTOOL_LIB

int main(int argc, char *argv[]){
  if (!SimulacrumLibrary::init()) {
    std::cerr << "Simulacrum library initialisation failure!" << std::endl;
    exit(6);
  }
  // set up arguments using parseArgs
  parseArgs Arguments;
  std::stringstream preamble;
  preamble << "The Simulacrum DICOM 'Swiss Army knife'." << std::endl
           << "Simulacrum Library V"
           << SimulacrumInformation::getVersionString();
  Arguments.setPreamble(preamble.str());
  Arguments.addArg("-a","Action","dump",false,false,false,false,
                   "The action to perform on FileNames");
  Arguments.addArg("-f","FilterString","",false,false,false,true,
                   "A string with which to locally filter DICOM tags");
  Arguments.addArg("-s","ShowStructure","",true,true,false,false,
                   "Show the structure of DICOM tags in output");
  Arguments.addArg("-o","OutputFile","",false,false,false,false,
                   "The name of the output file for various actions");
  Arguments.addArg("-p","NetworkPort","104",false,false,false,false,
                   "The network port number to use for various actions");
  Arguments.addArg("-t","NetworkTarget","",false,false,true,false,
                   "The target network address to use for various actions");
  Arguments.addArg("-aet","AETitle",sysInfo::systemName(),
                   false,false,false,false,
                   "Calling AETitle");
  Arguments.addArg("-aec","AETitleTarget","ANY",false,false,false,false,
                   "Called AETitle");
  Arguments.addArg("-l","DICOMLevel","S",false,false,false,false,
                   "DICOM level: [P]atient, [S]tudy, [s]eries, [I]mage");
  Arguments.addArg("-x","XSyntax","",false,false,false,false,
                   "The desired TransferSyntax of the output DICOM file, \
e.g. 1.2.840.10008.1.2.1");
  Arguments.addArg("-k","DICOMKey","",false,false,true,true, "A DICOM Key - \
either a pair of HHHH,HHHH values or a tag name");
  Arguments.addArg("-v","DICOMValue","",false,false,false,true,
                   "A DICOM tag value");
  Arguments.addArg("--file","FileValue","",false,false,false,true,
                   "A file path for disk value i/o");
  Arguments.addArg("-V","Verbose","",true,false,false,false,
                   "Verbose behaviour");
  Arguments.addArg("-R","Recurse","",true,false,false,false,
                   "Recurse the file hierarchy where possible");
  Arguments.addArg("-A","SDCMArchive","",false,false,false,false,
                   "The path to the Simulacrum DICOM Archive file");
  Arguments.addArg("-B","ArchiveBase","",false,false,false,false,
                   "The archive-base path");
  Arguments.addArg("-T","ArchiveTitle",SDICOMArch::defaultTitle(),false,false,
                   false,false,"Set the SDCMArchive title");
  Arguments.addArg("-D","ArchivePath",SDICOMArch::defaultPath(),false,false,
                   false,false,"An SDCMArchive path (various uses)");
  Arguments.addArg("--datadict","DataDictionary","",false,false,false,false,
                   "Use a specific DICOM DataDictionary");
  Arguments.addArg("--pseudondb","PseudoDB","",false,false,false,false,
                   "Use a specific DICOM PSeudoDB file");
  Arguments.addArg("--exec","Exec","",false,false,false,false,
                   "Specify a command to execute, e.g. for SCP node \
(%f <- file)");
  Arguments.addArg("--wlc","WLC","",false,false,false,false,
                   "Specify a window-level centre");
  Arguments.addArg("--wlw","WLW","",false,false,false,false,
                   "Specify a window-level width");
  Arguments.addArg("--keepprivate","KeepPrivate","",true,false,false,false,
                   "Keep private tags during pseudonymisation");
  Arguments.addArg("--force","Force","",true,false,false,false,
                   "Force various operations that might otherwise be disabled");
  Arguments.addArg("--raw","Raw","",true,false,false,false,
                   "Removes output formatting of certain operations");
  Arguments.addArg("--jpegls","JPEGLS","",true,false,false,false,
                   "Apply JPEGLS compression where possible (e.g. rewrite)");
  Arguments.addPositional("FileNames","",true, true,
                          "The name of a DICOM file on which to operate");
  Arguments.addArg("--netpseudon","NetPseudon","",true,false,false,false,
           "Perform inline pseudonimisation on network-received DICOM objects");
  std::stringstream HelpMessage;
  HelpMessage
  << "Actions:" << std::endl
  << "--------" << std::endl
  << " dump       -- Output the formated list of DICOM tags" << std::endl
  << " dumpdict   -- Dump the internal DataDictionary" << std::endl
  << " echo       -- DICOM ECHO SCU to a target SCP" << std::endl
  << " find       -- DICOM Find SCU" << std::endl
  << " get        -- Retrieve a remote file" << std::endl
  << " insert     -- Insert a key/value path" << std::endl
  << " lookup     -- Lookup a term in the DataDictionary" << std::endl
  << " modify     -- Modify an existing key/value path" << std::endl
  << " move       -- DICOM Move SCU" << std::endl
  << " path       -- Retrieve a DICOM tag by a path" << std::endl
  << " pseudon    -- Pseudonymise DICOM files, according to the data dictionary"
     <<  std::endl
  << " put        -- Put a file on a remote system (use target)" << std::endl
  << " receive    -- Start a DICOM SCP Node" << std::endl
  << " relay      -- Relay objects from an SCP to a target (like receive+put)"
     << std::endl
  << " remove     -- Remove an existing key path" << std::endl
  << " rewrite    -- Read in DICOM files, apply transformations, and write out"
     << std::endl
  << " send       -- DICOM SCU send" << std::endl
  << " sniff      -- Check for DICOM files" << std::endl
  << " sniffident -- Check for DICOM files with identity information" << std::endl
  << " sysinfo    -- Information regarding system resources" << std::endl
  << " toppm      -- Write PixelData to a (set of) PPM file(s)" << std::endl
  << " touch      -- Create an empty DICOM file" << std::endl
  << " xml        -- Write the DICOM tree as XML to the output file"
  << std::endl << std::endl
  << "Archive Actions:" << std::endl
  << "----------------" << std::endl
  << " import     -- Import DICOM file(s) to an archive" << std::endl
  << " list       -- List the contents of an archive" << std::endl;
  Arguments.setHelp(HelpMessage.str());
  try {
    Arguments.doParseArgs(argc,argv);
  }
  catch (std::exception &e) {
    doError(e.what(),1);
  }
  try {
    return doAction(Arguments);
  }
  catch (std::exception &e) {
    doError(e.what(),2);
  }
}

#endif //DCMTOOL_LIB
