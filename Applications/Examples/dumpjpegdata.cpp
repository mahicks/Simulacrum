/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <string>
#include "../../Resources/DICOM/sdicom.h"

using namespace Simulacrum;

int main(int argc, char *argv[]) {

  SDICOM dcmfile;

  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <dcmfile>" << std::endl;
    return 1;
  }
  else {
    dcmfile.setLocation(argv[1]);
    if (dcmfile.isValid()) {
      dcmfile.loadAllTags(true);
      if (dcmfile.getRootTag().hasTag(PixelData)) {
       DCMTag &PixelDataTag = dcmfile.getRootTag().getTag(PixelData);
       if (PixelDataTag.getTags().size() >= 2) {
         DCMTag &JPEGItemTag = *(PixelDataTag.getTags()[1]);
         if (JPEGItemTag.dataPresent()) { 
           // try to take the second item tag (which should be 
           const char *jpegdata = JPEGItemTag.getData(); 
           std::fstream outfile;
           std::string  outfilename(argv[1]);
           outfilename.append(".jpeg");
           outfile.open(outfilename.c_str(), std::ios::binary | std::ios::out);
           // write out the data char-by-char
           for (unsigned pos=0; pos<JPEGItemTag.getDataLength(); pos++)
             outfile.put(jpegdata[pos]);
           outfile.close();
         }
         else {
           std::cerr << "Error: data not present in item tag." << std::endl;
           return 4;
         }
       }
       else {
         std::cerr << "Error: could not fathom PixelData (not JPEG data?)." << std::endl;
         return 4;
       }
      } 
      else {
       std::cerr << "Error: no PixelData." << std::endl;
       return 3;
      }
    }
    else {
      std::cerr << "Error: not a DICOM file." << std::endl;
      return 2;
    }
  }

}
