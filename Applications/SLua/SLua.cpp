/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <cstdlib>
#include <Toolbox/SLua/slua.h>
#include <Toolbox/parseArgs/parseArgs.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SLogger/slogger.h>

using namespace Simulacrum;

static std::string AppName;

void doError(const std::string &error, int exitcode = 1) {
  std::cerr << AppName << ": " << error << std::endl;
  if (exitcode != 666)
    exit(exitcode);
}

int doAction(parseArgs& arguments) {
  SLua SLuaRuntime;
  int result = -1;
  std::vector<std::string> &cmdLine = arguments.getArg("cmdLine").Gobbled;
  if (cmdLine.size() > 0) {
    SFile sluafile(cmdLine[0]);
    if (sluafile.exists()) {
      SLuaRuntime.exceptError(SLuaRuntime.loadMem(sluafile.toString()));
      // push all command line arguments
      for (unsigned i=0; i< cmdLine.size(); i++)
        SLuaRuntime.push(cmdLine[i]);
      result = SLuaRuntime.exec();
      std::string poterror = SLuaRuntime.checkError(result);
      if (poterror.length() > 0)
        doError(poterror,result);
    }
    else {
      std::cerr << "File not found: " << sluafile.getLocation() << std::endl;
      result = 2;
    }
  }
  return result;
}

int main(int argc, char *argv[]){
  if (!SimulacrumLibrary::init()) {
    std::cerr << "Simulacrum library initialisation failure!" << std::endl;
    exit(6);
  }
  SURI AppNameU(argv[0]);
  AppName = AppNameU.getComponent(AppNameU.depth()-1);
  // set up arguments using parseArgs
  parseArgs Arguments;
  std::stringstream preamble;
  preamble << "The Simulacrum Lua Run-time Environment." << std::endl
           << "Simulacrum Library V"
           << SimulacrumInformation::getVersionString();
  Arguments.setPreamble(preamble.str());
  std::stringstream HelpMessage;
  HelpMessage
  << "The run-time environment launches an SLua program and provides "
  << "the wrapped Simulacrum library."
  << std::endl;
  Arguments.setHelp(HelpMessage.str());
  Arguments.addPositional("cmdLine","",true, true,
                          "The path of an SLua program and its arguments");
  Arguments.addArg("--writelog","WriteLog","",true,false,false,false,
                   "Write log entries to stderr");
  try {
    Arguments.doParseArgs(argc,argv);
  }
  catch (std::exception &e) {
    doError(e.what(),1);
  }
  try {
    if (Arguments.getArg("WriteLog").isSet) {
      SLogger::global().setStream(std::cerr);
    }
    return doAction(Arguments);
  }
  catch (std::exception &e) {
    doError(e.what(),2);
  }
}
