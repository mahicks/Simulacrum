==============
Get Simulacrum
==============

This page describes the resources providing the various distributables of
Simulacrum; precompiled binaries, headers, full source code and closely related
tools.

.. contents::
   :local:

.. _Disclaimer:

Disclaimer
----------

.. warning::
   **Simulacrum/Symmetry is provided "as is" and "with all faults."**
   There are no representations or warranties of any kind concerning the safety,
   suitability, lack of viruses, inaccuracies, typographical errors, or other
   harmful components of Simulacrum/Symmetry.
   There are inherent dangers in the use of any software, and you are solely
   responsible for determining whether Simulacrum/Symmetry is compatible with
   your equipment and other software installed on your equipment. You are also
   solely responsible for the protection of your equipment and backup of your
   data, and the provider will not be liable for any damages you may suffer in
   connection with using, modifying, or distributing Simulacrum/Symmetry.
   **Simulacrum/Symmetry is NOT suitable for primary diagnosis.
   It is neither a CE nor FDA cleared medical device.**

Stable Builds
-------------

.. _android-download:

Android
^^^^^^^

By downloading any of the available software, you are agreeing to the
:ref:`Disclaimer` displayed at the top of this page.

.. image:: Images/playstore.png
         :target: `simuplaystore`_

.. _simuplaystore: https://play.google.com/store/apps/details?id=org.mahicks.simulacrum.symmetry

The best way to get Symmetry and the latest precompiled libraries for the
Android platform is to install/extract them from the current APK on
Google Play. Alternatively, you may find the Android APKs
`here <http://mahicks.org/Simulacrum/files/android>`_.

.. _windows-download:

Windows (64-bit)
^^^^^^^^^^^^^^^^

By downloading any of the available software, you are agreeing to the
:ref:`Disclaimer` displayed at the top of this page.

Symmetry, dcmtool and SLua are currently, as of 2014-09-09, distributed
with Simulacrum in a single Windows installer

:Versions Supported: Windows 7 (64-bit), Windows 8 (64-bit Intel/AMD)

Windows distributables can be found at `the following URI <http://mahicks.org/Simulacrum/files/windows>`_:

.. code-block:: none

                http://mahicks.org/Simulacrum/files/windows

.. _linux-download:

Linux (Ubuntu 14.04)
^^^^^^^^^^^^^^^^^^^^

By downloading any of the available software, you are agreeing to the
:ref:`Disclaimer` displayed at the top of this page.

:Versions Supported: Ubuntu 14.04 (amd64), probably other amd64 Debian based
                     distributions

Details
$$$$$$$

The Linux distributables are decomposed into a set of discrete Debian pacakges:

 * libsimulacrum
 * libsimulacrum-dev
 * libsimulacrum-gui
 * libsimulacrum-gui-dev
 * symmetry
 * dcmtool
 * slua

These packages are provided in a convenient repository, making staying
up-to-date easy.

To use the repository, two actions must be performed:

 1. security signing public key must be added to the local keyring
 2. repository URI must be added to apt

The security public key can be found here:

.. code-block:: none

               http://mahicks.org/Simulacrum/repo/simulacrum.gpg.key

And the repository URI:

.. code-block:: none

                http://mahicks.org/Simulacrum/repo/apt/ubuntu/

Quick Start
$$$$$$$$$$$

`See here <https://help.ubuntu.com/community/Repositories/Ubuntu#Adding_Repositories_in_Ubuntu>`_
for a guide to adding third-party repositories using the Ubuntu GUI tools.

Run following commands from a console, to add the key and repository:

.. code-block:: none

               wget http://mahicks.org/Simulacrum/repo/simulacrum.gpg.key
               sudo apt-key add simulacrum.gpg.key
               sudo add-apt-repository 'deb http://mahicks.org/Simulacrum/repo/apt/ubuntu trusty main'
               sudo apt-get update

Now it should be possible to install one of the packages named above, for
example 'symmetry':

.. code-block:: none

               sudo apt-get install symmetry

Simulacrum libraries and applications will now be kept up-to-date by the
system's package manager, and/or by explicitly updating/upgrading the pacakges
installed on the system in the usual manner.

Revision History
----------------

**Version 1.2.2 -- 2015-12-24**

  * Core: switchable realloc behaviour for optimised memory use
    (mostly relevant for Android)
  * Symmetry: date restriction removed

**Version 1.2.1 -- 2015-06-07**

  * DICOM: improvements in handling certain obscure data
  * General: robustness/stability enhancements

**Version 1.2.0 -- 2015-02-08**

  * General stability and robustness improvements
  * DICOM:

    * Better handling of unknown TransferSyntax
    * Adding JPEGLS (r/w), JPEGLossless 8-bit and regular JPEGLossy

  * Full PNG (r/w) support
  * Algorithm performance improvements
  * API additions and improvements, for developers
  * Additional GUI tools (export images, raw data objects, reinterpret extent)

**Version 1.1.0 -- 2015-01-11**


 * Corrections to *many* serious bugs -- too many to enumerate here (upgrade!) 
 * Performance enhancements
 * DICOM

   * JPEG-8 bit lossless now handled correctly
   * Improved DICOM dictionary lookup tool

 * XML library improvements (now useful to general purpose XML editing/creation)
 * GUI:

   * Support for various LUT schemes in UI
   * ABI changes at this release

*Android Only*

 * UI improvements
 * Fullscreen mode -- no status bar
 * Window-Level LUT -- double-tap for auto levelling and histogram
 * Configurable DICOM data home location for quick indexing
 * Advanced tools now available in quick view tab
 * Memory optimisations

**Version 1.0.0 -- 2014-11-30**

Initial release.

.. _source-download:

Source Code
-----------

By downloading any of the available software, you are agreeing to the
:ref:`Disclaimer` displayed at the top of this page.

The Simulacrum source code is available on GitLab.

You can find the `project here <https://gitlab.com/mahicks/Simulacrum>`_.

You can download the latest snapshot of the development tree
`directly here <https://gitlab.com/mahicks/Simulacrum/-/archive/master/Simulacrum-master.zip>`_.

Development Builds
------------------

`The latest development builds can be obtained here
<https://tinab.mahicks.org/posts/simulacrum_builds/>`_.

Although not supported and potentially containing faults,
development builds provide the latest features and additions to Simulacrum.

