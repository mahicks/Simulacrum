=======
Welcome
=======

Welcome to the home page of the Simulacrum Project. Here you can find the
high-level documentation and help/guides for Simulacrum and several of its core
applications. *Documentation is currently a work in progress, with new items
added frequently!*

Want to get started immediately? Visit :doc:`GetSimulacrum`. Looking for the
Symmetry application? Visit :doc:`Symmetry`.

.. contents::
   :local:

What is Simulacrum?
-------------------

Simulacrum is a library suite for volume and image space interaction. It is
designed to be lightweight, easy-to-use, portable, scalable and extensible.
The core of Simulacrum is written in C++ with integrated Lua scripting,
providing flexibility in programming solutions. A closely coupled tertiary
library, Simulacrum-gui, provides a framework for visual tools,
including advanced rendering techniques built around the cross-platform Qt
libraries.

.. raw:: html

   <center><iframe src="https://docs.google.com/presentation/d/1qfkOdC-Bd3W-LauVYtYsMP-1d9_M8RHBTKG1n4D62gM/embed?start=false&loop=false&delayms=10000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></center>

Feature Overview
^^^^^^^^^^^^^^^^

Simulacrum is a libarary engineered to provide object-oriented abstractions over
many of the key aspects of a volume/image analysis toolkit. These include
generic frameworks to easily make use of the following:

 * N-Dimensional volume *Spaces*, including time-series, tensor fields,
   sparse domains etc, with run-time generic reimplementable *Element*
   structures
 * *Algorithms* operating over such spaces, including simplified parallelism
   with minimal parallel programming skills
 * Data *Resources*, including complex data/archive trees, from which to load
   the above spaces, with existing implementations for common data formats
   (particularly DICOM) but easily extendable to new data sources
 * Integrated *primitives* for library independence, including optimised
   vectors and basic linear algebra operations
 * *Connectable* components for building pipelines
 * Integrated off-screen *rendering* operations
 * Associated GUI Widgets for the above, including Viewports and advanced
   interactions tools, archive/data data browsers -- built around Qt
 * Toolbox of useful methods and structures for: run-time parallelism,
   file-system abstraction, logging, network transparency, command-line use
 * Plugin interface for all core components
 * Full and integrated Lua interface
 * Compatibility with existing libraries (VTK & ITK)
 * And many more, including:

   * A fast, C++, user-friendly implementation of large parts of the DICOM
     protocol:

     * Most common transfer syntaxes (including JPEG 2000, lossless JPEG...)
     * Disk object reading & writing
     * Networking (store/receive, query/retrieve, WADO ... )
     * Fast, parameterisable indexing, sorting and archiving of large
       DICOM data sets

On which platforms can Simulacrum be run?
-----------------------------------------

Currently there are binary builds of Simulacrum available for the following
platforms:

 * Windows (64-bit, Visual Studio 12.0)
 * Linux
 * Android
 * Raspberry Pi
 * OSX / iOS (coming soon)

However, Simulacrum can be built from source for almost any platform providing
a working C and C++ compiler; i.e. most modern operating systems. The tertiary
GUI library can be built for any platform on which the Qt 5.0 framework
operates. 

Who are Simulacrum's target users?
----------------------------------

Users likely to be interested in Simulacrum include, but are not limited to:

 * Developers looking for a free library on which to deploy their application to
   many platforms, with the possibility of proprietary components
 * Researchers, currently particularly in medical image analysis/visualisation
 
Under what licence is Simulacrum supplied?
------------------------------------------

Simulacrum is distrubuted under the "GNU Lesser General Public License (LGPL)"
version 3. This means you can use and extend the library in commercial
applications, so long as changes to the interior of the library are themselves
released under the same license. Combined with the plugin facilities of
Simulacrum, this provides developers with a high degree of flexibility.

Where to get Simulacrum?
------------------------

Visit :doc:`GetSimulacrum`.

Where is the Simulacrum API Reference?
--------------------------------------

The source-code level documentation can be
`found here <_static/source-docs/html/index.html>`_ .

.. _ContactDeveloper:

Contact the Developer
---------------------

The lead developer of Simulacrum is `Michael A. Hicks <http://mahicks.org>`_.
You can contact him `here <http://mahicks.org>`_.
