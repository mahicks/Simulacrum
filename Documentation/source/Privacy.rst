==============
Privacy Policy
==============

Symmetry and Simulacrum are designed to be entirely private.

* Neither Symmetry nor Simulacrum collect or store *any* user data.
* Neither Symmetry nor Simulacrum contain *any* automated connectivity
  for communicating or monitoring user data.

For privacy related questions, please :ref:`contact the developer <ContactDeveloper>`.

