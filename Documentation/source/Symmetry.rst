========
Symmetry
========

  *Symmetry is a powerful application for visualising, processing, organising
  and interacting with scientific/medical datasets*

.. raw:: html

  <center><iframe width="560" height="315" src="//www.youtube.com/embed/fIIUhe-I4co" frameborder="0" allowfullscreen></iframe></center><br/>

.. sidebar:: Download Symmetry

   * :ref:`Windows (64-bit) <windows-download>`
   * :ref:`Linux (64-bit) <linux-download>`
   * :ref:`Android (ARM) <android-download>`

     .. image:: Images/playstore.png
         :target: `simuplaystore`_
   * `Latest Development Builds <https://tinab.mahicks.org/posts/simulacrum_builds/>`_

.. _simuplaystore: https://play.google.com/store/apps/details?id=org.mahicks.simulacrum.symmetry

Based on the Simulacrum library, with a current focus on medical/DICOM,
Symmetry provides the following features:

* Simple single-pane viewing or advanced multi-pane light table configuration
* Standard image interaction tools: zoom, pan, window level, scrolling through
  volumes
* Advanced analysis tools

  * Multi Planar Reconstruction (MPR), including oblique planes
  * slab MIN/MIP/AVG intensity projections
  * surface rendering
  * cursor projections / registration
  * overlays & complex LUTs

* LightTable, with custom hangings/layouts and complex data loaders
  (e.g. MRI studies)
* Dynamic rendering performance/quality scaling to the hardware device
* Support for DICOM images/volumes (including Lossless-JPEG & JPEG 2000,
  enhanced & multi-frame)
* DICOM Tag viewing and editing
* Automatic DICOM data structuring and organisation
* DICOM/PACS integration (Query/Retrieve and WADO fulfilment, DICOM networking
  send/receive/echo)
* DICOM identity aware: 'Identity Shield' and de-identification tools
* Continuously extended to support new resource formats (now: NetPBM,
  JPEG2000 ...) and new algorithms
* User extendable though plugins and scripting interface
* Cross platform: Windows, Android, Linux, RaspberryPi and more...
