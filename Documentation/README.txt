##############################################
          Important Information
##############################################

This is a Sphinx document repository.

You can build this repository on most platforms.


##############################################
                Requirements
##############################################

On Ubuntu you need to the following packages:

1) python-sphinx 

For the graphs plug-in:

2) graphviz

3) python-pygraphviz

4) libgraphviz-dev

Install them easily, like this:

$ sudo apt-get install python-sphinx libgraphviz-dev python-pygraphviz graphviz

##############################################
         Building the Document Tree
##############################################

Show a list of available target types:

$ make

Build the two most common targets:

$ make html latexpdf
