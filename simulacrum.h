/*
  Copyright (C) 2017 Michael A. Hicks
  Distributed under the terms of the GNU Lesser General Public License.

  This file is part of Simulacrum.

  Simulacrum is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Simulacrum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Simulacrum.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMULACRUM
#define SIMULACRUM

// Core Simulacrum components
#include <Core/definitions.h>
#include <Core/sprimitives.h>
#include <Core/sspace.h>
#include <Core/sfileio.h>
#include <Core/error.h>
#include <Core/sconnectable.h>
#include <Core/slicer/slicer.h>
#include <Core/sresource.h>
#include <Core/slockable.h>
#include <Core/SPool/SPool.h>
#include <Core/sabtree.h>
#include <Core/LUT/LUT.h>
#include <Core/salgorithm.h>
#include <Core/salgorithms.h>

// NNode XML
#include <Core/NNode/nnode.h>

// Simulacrum GUI components
#ifdef GUI
#include <GUI/tagBrowser.h>
#include <GUI/BusyWidget.h>
#include <GUI/bgLoader.h>
#include <GUI/qconnectable.h>
#include <GUI/viewPort.h>
#include <GUI/tableBrowser.h>
#include <GUI/vpTools.h>
#include <GUI/console.h>
#include <GUI/lighttable.h>
#include <GUI/grapher.h>
#endif

// SNet
#include <Network/snet.h>

// Resources (images, databases etc)
#include <Resources/DICOM/datadic.h>
#include <Resources/DICOM/dcmtag.h>
#include <Resources/DICOM/sarch.h>
#include <Resources/DICOM/sdicom.h>
#include <Resources/DICOM/types.h>
#include <Resources/DICOM/sdicom-net.h>
#include <Resources/sjpegls.h>
#include <Resources/sjpeg.h>
#include <Resources/sljpeg.h>
#include <Resources/spng.h>
#include <Resources/spbm.h>
#include <Resources/sj2k.h>

// General Toolbox components
#include <Toolbox/parseArgs/parseArgs.h>
#include <Toolbox/SFile/sfile.h>
#include <Toolbox/SURI/suri.h>
#include <Toolbox/SResourceAllocator/sresourceallocator.h>
#include <Toolbox/cmdProgress/cmdProgress.h>
#include <Toolbox/sysInfo/sysInfo.h>
#include <Toolbox/SRing/sring.h>
#include <Toolbox/SLogger/slogger.h>
#include <Toolbox/SLua/slua.h>
#include <Toolbox/SPlugins/splugins.h>

namespace Simulacrum {


} //end namespace
#endif //SIMULACRUM

// -------------------------------Doxygen---------------------------------------
/*! \mainpage Simulacrum API Reference
 *
 * \section intro_sec Overview
 *
 * This site documents the Simulacrum Library API, including the Simulacrum GUI
 * Library. Simulacrum is written almost exclusively in C++, with all classes
 * contained within the Simulacrum namespace.
 *
 * \section install_sec Looking for the Simulacrum homepage?
 *
 * You can following the link to the Simulacrum Project pages from the footer
 * of any page in this documentation.
 *
 */
// -----------------------------------------------------------------------------
